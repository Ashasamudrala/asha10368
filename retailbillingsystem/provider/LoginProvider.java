package com.retailbillingsystem.provider;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.google.gson.Gson;
import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.services.LoginServices;

/**
 * 
 * @author IMVIZAG
 *
 */

@Path("/login")
public class LoginProvider {

	//creating gson  object
	private Gson gson = new Gson();
	
	/**
	 * this method is used to call  managerLogin method from LoginServices class
	 * @param username
	 * @param password
	 * @return JSON object
	 */
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/managerLogin")
	@GET
	public String managerLogin(String details) 
	{
		//calling managerLogin method inside a login services and storing the result into a boolean
		EmployeeDetails employeeDetails = gson.fromJson(details, EmployeeDetails.class);
		boolean managerLogin = new LoginServices().managerLogin(employeeDetails.getEmployeeName(), employeeDetails.getEmployeePassword());
		String res = managerLogin ? "Login successfull...." : "please enter valid details";
		//converting the result into JSON object
		String loginObject = this.gson.toJson(res);
		//returning the JSON object
		return loginObject;
	}
	
	/**
	 * this method is used to call  stockerLogin method from LoginServices class
	 * @param username
	 * @param password
	 * @return JSON object
	 */
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/stockerLogin")
	@GET
	public String stockerLogin(String details) {
		
		//calling stockerLogin method inside a login services and storing the result into a boolean
		EmployeeDetails employeeDetails = gson.fromJson(details, EmployeeDetails.class);
		boolean stockerLogin = new LoginServices().stockerLogin(employeeDetails.getEmployeeName(), employeeDetails.getEmployeePassword());
		String res = stockerLogin ? "Login successfull...." : "please enter valid details";
		
		//converting the result into JSON object
		String loginObject = this.gson.toJson(res);
		//returning the JSON object
		return loginObject;
	}
	
	/**
	 * this method is used to call  billerLogin method from LoginServices class
	 * @param username
	 * @param password
	 * @return JSON object
	 */
	@Produces("application/json")
	@Consumes("text/plain")
	@Path("/billerLogin")
	@GET
	public String billerLogin(String details) {
		
		//calling billerLogin method inside a login services and storing the result into a boolean
		EmployeeDetails employeeDetails = gson.fromJson(details, EmployeeDetails.class);
		boolean billerLogin = new LoginServices().billerLogin(employeeDetails.getEmployeeName(), employeeDetails.getEmployeePassword());
		String res = billerLogin ? "Login successfull...." : "please enter valid details";
		//converting the result into JSON object
		String loginObject =this.gson.toJson(res);
		//returning the JSON object
		return loginObject;
	}
	
}
