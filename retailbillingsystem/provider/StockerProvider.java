package com.retailbillingsystem.provider;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.google.gson.Gson;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.services.StockerServices;

@Path("/stocker")
public class StockerProvider{
	//initializing json object
		private Gson gson = new Gson();
	
	@Path("/addProducts")
	@Produces("application/json")
	@Consumes("text/plain")
	@POST
	/**
	 * this method is used to add prodcuts to products table
	 * @param details
	 * @return
	 */
	public String addProducts(String details)
	{
		Products products = gson.fromJson(details, Products.class);
		StockerServices stockerServices = new StockerServices();
		boolean res = stockerServices.addProduct(products);
		return (res ? "Added Successfully" : "Addition failed");
	}
	
	/**
	 * used to 
	 * 
	 * @param details
	 * @return
	 */
	@GET
	@Path("/delProducts")
	@Produces("application/json")
	@Consumes("text/plain")
	public String delProducts(String details) {
		
		Products products = gson.fromJson(details, Products.class);
		
		StockerServices stockerServices = new StockerServices();
		boolean res = stockerServices.deleteProduct(products.getProductname());
		return (res ? "Deleted Successfully" : "Deletion failed");
		
	}
	
	@GET
	@Path("/displayProducts")
	@Produces("application/json")
	public String displayProducts()
	{
		Gson gson = new Gson(); 
		String productJsonString ="";
		StockerServices stockerServices = new StockerServices();
		List<Products> productList = stockerServices.displayProducts();
		if(productList.size() != 0) {
   			
			productJsonString = gson.toJson(productList);

		}
   			
		return productJsonString;
	}
	
	@PUT
	@Path("/increaseQuantity")
	@Produces("application/json")
	@Consumes("text/plain")
	public String increaseQuantity(String details)
	{
		StockerServices stockerServices = new StockerServices();
		Gson gson = new Gson();
		Products products = gson.fromJson(details, Products.class);
		String productJsonString ="";
		String result = stockerServices.updateProductQuantity(products.getProductname(), products.getQuantity(), "1");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
	
	@PUT
	@Path("/decreaseQuantity")
	@Produces("application/json")
	@Consumes("text/plain")
	public String decreaseQuantity(String details)
	{
		StockerServices stockerServices = new StockerServices();
		Products products = gson.fromJson(details, Products.class);
		String productJsonString ="";
		String result = stockerServices.updateProductQuantity(products.getProductname(), products.getQuantity(), "2");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
	

	@PUT
	@Path("/increasePrice")
	@Produces("application/json")
	@Consumes("text/plain")
	public String increasePrice(String details)
	{
		StockerServices stockerServices = new StockerServices();
		Products products = gson.fromJson(details, Products.class);
		String productJsonString ="";
		String result = stockerServices.updateProductPrice(products.getProductname(),products.getPrice(), "1");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
	
	@PUT
	@Path("/decreasePrice")
	@Produces("application/json")
	@Consumes("text/plain")
	public String decreasePrice(String details)
	{
		StockerServices stockerServices = new StockerServices();
		Products products = gson.fromJson(details, Products.class);
		String productJsonString ="";
		String result = stockerServices.updateProductPrice(products.getProductname(),products.getPrice(), "2");
		productJsonString = gson.toJson(result);
		return productJsonString;
	}
}