package com.retailbillingsystem.provider;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.google.gson.Gson;
import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.services.ManagerService;

@Path("/manager")
public class ManagerProvider {
	//initializing json object
	private Gson gson = new Gson();
	
	
	@POST
	@Path("/addEmployee")
	@Produces("application/json")
	@Consumes("text/plain")
	public String addEmployee(String details)
	{
		EmployeeDetails employeeDetails = gson.fromJson(details, EmployeeDetails.class);
		ManagerService managerService = new ManagerService();
		boolean res = managerService.createEmployee(employeeDetails);
		String response = (res ? "Added Successfully" : "Addition failed");
		String employeejson = gson.toJson(response);
		return employeejson;
	}
	
	@GET
	@Path("/delEmployee")
	@Produces("application/json")
	public String delEmployee(String details) 
	{
		EmployeeDetails employeeDetails = gson.fromJson(details, EmployeeDetails.class);
		ManagerService managerService = new ManagerService();
		boolean res = managerService.deleteEmployee(employeeDetails.getEmployeeId(),employeeDetails.getRole());
		String response =  (res ? "Deleted Successfully" : "Deletion failed");
		String employeejson = gson.toJson(response);
		return employeejson;
	}
	
	@GET
	@Path("/displayEmployee")
	@Produces("application/json")
	public String displayEmployee()
	{
		ManagerService managerService = new ManagerService();
		List<EmployeeDetails> employeeList = managerService.displayEmployees();
		String employeejson = gson.toJson(employeeList);
		return employeejson;
	}
}