

import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
    render(){
        return(
            <div style={{fontSize:50}}>HelloWorld</div>
        )
    }
    
}

ReactDOM.render(<App/>,document.getElementById('app'))