/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import axios from 'axios';
import _ from 'lodash';

import history from '../history';
import actionTypes from '../actions/actionTypes';
import { standardizeError } from '../utils';

/**
 * Axios default configuration
 * 
 * @author Chandra Veerapaneni
 */
const axiosInstance = axios.create({
    baseURL: 'http://192.168.204.18:8082'
});

export default axiosInstance;

/**
 * Default export
 */
export const axiosWrapper = axiosInstance;

/**
* This method performs a 'POST' request invocation on the provided url and sends the provided payload.
* If any errors are encountered, this method raises and dispatches the error (via the error action creator).
* If the invocation was successful, the response would be dispatched via the action creator.
*
* To the options object, additional details can be provided. 'onSuccess' property (which is a function) can
* be provided in the options object and this method ensures that the function will be called after the data
* is successfully dispatched via the action creator.
* Likewise, a 'redirectUrl' can be provided, which will be used to redirect the user to, after the successful
* dispatching of the data via the action creator and successful callback invocation provided via 'onSuccess'
* property.
* 
* @param {*} url Url to which the POST request will be made.
* @param {*} data Payload that will be provided to this url
* @param {*} actionType The type of the action creator to which this response will be dispatched (upon success)
* @param {*} options Object that holds configuration option and supports the below properties:
*
* 'params' - Object that holds the parameters to be passed during API invocation
* 'onSuccess' - Callback function that will be called upon successful API invocation
* 'redirectUrl' - Url that the user will be redirected to, after the successful dispatching of the data via the 
* action creator and successful callback invocation provided via 'onSuccess' property.
*/
export const invokePost = (url, data, actionType, options) => {
    console.debug(`Payload to POST to the url - ${url} is ${JSON.stringify(data)}`);

    // Extract out the fields from options object.
    const { onSuccess = null, redirectUrl = '', params = {}, secured = false } = options || {};

    // Return a function from this action creator (i.e. using redux-thunk for asynchronous actions).
    // This method takes a dispatch function that will be used to dispatch the action instance when 
    // the data becomes available.
    // getState is the second parameter that gets passed by redux-thunk. This will enable us to fetch
    // any information from the redux store.
    return async (dispatch, getState) => {
        if (!isUrlValid(url)) {
            console.debug('Invalid url provided. Raising error.');
            dispatchError(dispatch, standardizeError(null, 'Url cannot be null / empty.'));
        } else if (secured && !isAuthenticated(getState())) {
            // Raise the error and redirect
            dispatchErrorAndRedirectToLoginUrl(dispatch, '/login');
        } else {
            console.debug(`Invoking POST request on the url - ${url}`);
            // Attempt to POST the data to the url.
            axiosInstance.post(url, data, { params: params , headers: {"Authorization" : `Bearer ${'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaXllckBpbm5vbWluZHMuY29tIiwiaWF0IjoxNTk5NTUxNzI1LCJleHAiOjE2MDAxNTY1MjV9.XZ_0xK3TyN8I64xH6v0iB-ObSuEGjW8pZoshKQyJfUj63ISRIosEV153R8pxJ7LfOTffTSzUsNX8AriLW0J5GQ'}`} }).then(response => {
                console.debug(`Received data from ${url}. Dispatching via action - ${actionType}`);
                // Dispatch the received response
                dispatch({
                    type: actionType,
                    payload: response.data
                });
                if (onSuccess instanceof Function) {
                    console.debug('Calling success callback function');
                    onSuccess(response);
                }
                // If redirect url is provided, send the user to the redirect url.
                if (redirectUrl) {
                    console.debug(`Redirecting to ${redirectUrl}`);
                    history.push(redirectUrl);
                }
            }).catch(error => {
                console.error(`Error encountered during POST request on url - ${url}`);
                console.error(error.response);
                dispatchError(dispatch, standardizeError(error.response));
            });
        }
    };
};

/**
* This method performs a 'PUT' request invocation on the provided url and sends the provided payload.
* If any errors are encountered, this method raises and dispatches the error (via the error action creator).
* If the invocation was successful, the response would be dispatched via the action creator.
*
* To the options object, additional details can be provided. 'onSuccess' property (which is a function) can
* be provided in the options object and this method ensures that the function will be called after the data
* is successfully dispatched via the action creator.
* Likewise, a 'redirectUrl' can be provided, which will be used to redirect the user to, after the successful
* dispatching of the data via the action creator and successful callback invocation provided via 'onSuccess'
* property.
* 
* @param {*} url Url to which the PUT request will be made.
* @param {*} data Payload that will be provided to this url
* @param {*} actionType The type of the action creator to which this response will be dispatched (upon success)
* @param {*} options Object that holds configuration option and supports the below properties:
*
* 'params' - Object that holds the parameters to be passed during API invocation
* 'onSuccess' - Callback function that will be called upon successful API invocation
* 'redirectUrl' - Url that the user will be redirected to, after the successful dispatching of the data via the
* action creator and successful callback invocation provided via 'onSuccess' property.
*/
export const invokePut = (url, data, actionType, options) => {
    console.debug(`Payload to PUT to the url - ${url} is ${JSON.stringify(data)}`);

    // Extract out the fields from options object.
    const { onSuccess = null, redirectUrl = '', params = {}, secured = false } = options || {};

    // Return a function from this action creator (i.e. using redux-thunk for asynchronous actions).
    // This method takes a dispatch function that will be used to dispatch the action instance when 
    // the data becomes available.
    // getState is the second parameter that gets passed by redux-thunk. This will enable us to fetch
    // any information from the redux store.
    return async (dispatch, getState) => {
        if (!isUrlValid(url)) {
            console.debug('Invalid url provided. Raising error.');
            dispatchError(dispatch, standardizeError(null, 'Url cannot be null / empty.'));
        } else if (secured && !isAuthenticated(getState())) {
            // Raise the error and redirect
            dispatchErrorAndRedirectToLoginUrl(dispatch, '/login');
        } else {
            console.debug(`Invoking PUT request on the url - ${url}`);
            // Attempt to PUT the data to the url.
            axiosInstance.put(url, data, { params: params , headers: {"Authorization" : `Bearer ${'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaXllckBpbm5vbWluZHMuY29tIiwiaWF0IjoxNTk5NTUxNzI1LCJleHAiOjE2MDAxNTY1MjV9.XZ_0xK3TyN8I64xH6v0iB-ObSuEGjW8pZoshKQyJfUj63ISRIosEV153R8pxJ7LfOTffTSzUsNX8AriLW0J5GQ'}`}}).then(response => {
                console.debug(`Received data from ${url}. Dispatching via action - ${actionType}`);
                // Dispatch the received response
                dispatch({
                    type: actionType,
                    payload: response.data
                });
                if (onSuccess instanceof Function) {
                    console.debug('Calling success callback function');
                    onSuccess(response);
                }
                // If redirect url is provided, send the user to the redirect url.
                if (redirectUrl) {
                    console.debug(`Redirecting to ${redirectUrl}`);
                    history.push(redirectUrl);
                }
            }).catch(error => {
                console.error(`Error encountered during PUT request on url - ${url}`);
                console.error(error.response);
                dispatchError(dispatch, standardizeError(error.response));
            });
        }
    };
};

/**
* This method performs a 'GET' request invocation on the provided url. 
* If any errors are encountered, this method raises and dispatches the error (via the error action creator). 
* If the invocation was successful, the response would be dispatched via the action creator.
*
* To the options object, additional details can be provided. 'onSuccess' property (which is a function) can
* be provided in the options object and this method ensures that the function will be called after the data
* is successfully dispatched via the action creator.
*
* @param {*} url Url to which the GET request will be made.
* @param {*} actionType The type of the action creator to which this response will be dispatched (upon success)
* @param {*} options Object that holds configuration option and supports the below properties:
*
* 'params' - Object that holds the parameters to be passed during API invocation
* 'onSuccess' - Callback function that will be called upon successful API invocation
*/
export const invokeGet = (url, actionType, options) => {
    // Extract out the fields from options object.
    const { onSuccess = null, params = {}, secured = false, responseType = '' } = options || {};

    // Return a function from this action creator (i.e. using redux-thunk for asynchronous actions).
    // This method takes a dispatch function that will be used to dispatch the action instance when 
    // the data becomes available.
    // getState is the second parameter that gets passed by redux-thunk. This will enable us to fetch
    // any information from the redux store.
    return async (dispatch, getState) => {
        if (!isUrlValid(url)) {
            console.debug('Invalid url provided. Raising error.');
            dispatchError(dispatch, standardizeError(null, 'Url cannot be null / empty.'));
        } else if (secured && !isAuthenticated(getState())) {
            // Raise the error and redirect
            dispatchErrorAndRedirectToLoginUrl(dispatch, '/login');
        } else {
            console.debug(`Invoking GET request on the url - ${url}`);
            // Attempt to GET the data from the url
            axiosInstance.get(url, { params: params, responseType: responseType , headers: {"Authorization" : `Bearer ${'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaXllckBpbm5vbWluZHMuY29tIiwiaWF0IjoxNTk5NTUxNzI1LCJleHAiOjE2MDAxNTY1MjV9.XZ_0xK3TyN8I64xH6v0iB-ObSuEGjW8pZoshKQyJfUj63ISRIosEV153R8pxJ7LfOTffTSzUsNX8AriLW0J5GQ'}`}}).then(response => {
                // Dispatch if and only if actionType is provided.
                if (actionType) {
                    console.debug(`Received data from ${url}. Dispatching via action - ${actionType}`);
                    // Dispatch the received response
                    dispatch({
                        type: actionType,
                        payload: response.data
                    });
                }

                // Is there a 'onSuccess' callback. If so, invoke it by passing the data.
                if (onSuccess instanceof Function) {
                    console.debug('Calling success callback function');
                    onSuccess(response);
                }
            }).catch(error => {
                console.error(`Error encountered during GET request on url - ${url}`);
                console.error(error.response);
                dispatchError(dispatch, standardizeError(error.response));
            });
        }
    };
};

/**
* This method performs a 'DELETE' request invocation on the provided url. 
* If any errors are encountered, this method raises and dispatches the error (via the error action creator). 
* If the invocation was successful, the response would be dispatched via the action creator.
*
* To the options object, additional details can be provided. 'onSuccess' property (which is a function) can
* be provided in the options object and this method ensures that the function will be called after the data
* is successfully dispatched via the action creator.
*
* @param {*} url Url to which the DELETE request will be made.
* @param {*} actionType The type of the action creator to which this response will be dispatched (upon success)
* @param {*} options Object that holds configuration option and supports the below properties:
*
* 'params' - Object that holds the parameters to be passed during API invocation
* 'onSuccess' - Callback function that will be called upon successful API invocation
*/
export const invokeDelete = (url, actionType, options) => {

    // Extract out the fields from options object.
    const { onSuccess = null, params = {}, secured = false } = options || {};

    // Return a function from this action creator (i.e. using redux-thunk for asynchronous actions).
    // This method takes a dispatch function that will be used to dispatch the action instance when 
    // the data becomes available.
    // getState is the second parameter that gets passed by redux-thunk. This will enable us to fetch
    // any information from the redux store.
    return async (dispatch, getState) => {
        if (!isUrlValid(url)) {
            console.debug('Invalid url provided. Raising error.');
            dispatchError(dispatch, standardizeError(null, 'Url cannot be null / empty.'));
        } else if (secured && !isAuthenticated(getState())) {
            // Raise the error and redirect
            dispatchErrorAndRedirectToLoginUrl(dispatch, '/login');
        } else {
            console.debug(`Invoking DELETE request on the url - ${url}`);
            // Attempt to DELETE the data from the url
            axiosInstance.delete(url, { params: params , headers: {"Authorization" : `Bearer ${'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaXllckBpbm5vbWluZHMuY29tIiwiaWF0IjoxNTk5NTUxNzI1LCJleHAiOjE2MDAxNTY1MjV9.XZ_0xK3TyN8I64xH6v0iB-ObSuEGjW8pZoshKQyJfUj63ISRIosEV153R8pxJ7LfOTffTSzUsNX8AriLW0J5GQ'}`}}).then(response => {
                console.debug(`Received data from ${url}. Dispatching via action - ${actionType}`);
                // Dispatch the received response
                dispatch({
                    type: actionType,
                    payload: response.data
                });

                // Is there a 'onSuccess' callback. If so, invoke it by passing the data.
                if (onSuccess instanceof Function) {
                    console.debug('Calling success callback function');
                    onSuccess(response);
                }
            }).catch(error => {
                console.error(`Error encountered during GET request on url - ${url}`);
                console.error(error.response);
                dispatchError(dispatch, standardizeError(error.response));
            });
        }
    };
};

/**
 * This method retrieves the authentication information from the redux store (via the provided state object)
 * and checks the authentication status of the user.
 * 
 * @param {*} state Redux state
 */
const isAuthenticated = (state) => {
    const { auth } = state;
    return auth && auth.isAuthenticated;
}

/**
 * This method dispatches the error via the Error action creator and redirects the user to the login url.
 * 
 * @param {*} dispatch Dispatch object that can be used to dispatch actions.
 * @param {*} loginUrl Url to redirect the user to after the successful dispatch.
 */
const dispatchErrorAndRedirectToLoginUrl = (dispatch, loginUrl) => {
    // Raise the error.
    dispatchError(dispatch, standardizeError(null, 'Please login to access this resource.'));
    history.push(loginUrl);
}

/**
 * This method checks if the provided url is valid.
 * 
 * @param {*} url Url to check.
 */
const isUrlValid = (url) => {
    return !(_.isNull(url) || url.length === 0)
}

/**
 * This method dispatches the error via the Error action creator.
 * 
 * @param {*} dispatch Dispatch function to asynchronously dispatch the error.
 * @param {*} payload Error payload.
 */
const dispatchError = (dispatch, payload) => {
    // Raise the error.
    dispatch({
        type: actionTypes.RAISE_ERROR,
        payload: payload
    });
}
