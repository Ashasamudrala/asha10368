/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

/**
 * This object contains the urls that are used across the application.
 * 
 * @author Chandra Veerapaneni
 */
const AppUrls = {
    auth: {
        login: '/login',
        logout: '/logout'
    },
    platforms: {
        getAllPlatforms: '/platforms?pageNumber=0&pageSize=100'
    },
    projects: {
        create: '/projects',
        getAllProjects: '/projects',
        getProject: '/projects/{projectId}',
        updateProject: '/projects/{projectId}',
        deleteProject: '/projects/{projectId}',
        generate: '/projects/generate'
    },
    jobs: {
        getAllJobs: '/projects/5d5801608a37cf5e626ada02/jobs',
    },
    categories: {
        create: '/categories',
        getAllCategories: '/categories'
    },
    accelerators: {
        getAllAccelerators: ''
    }
};

export default AppUrls;
