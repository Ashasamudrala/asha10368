/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import * as ApplicationApi from "../api/ApplicationApi";
import actionTypes from "./actionTypes";

// Platform API Endpoint
const PLATFORM_API_ENDPOINT = "/administration/platforms";

/**
 * This is the action creator that gets called whenever the user attempts to login to the system.
 *
 * @param {*} loginData Object that holds the login data i.e. email-address / username or password.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful login. User object is passed to this callback.
 * error: callback function that is called after login failure and the error message is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const loginAction = (loginData, options) => {
  console.debug("Attempting to login...");
  return ApplicationApi.invokePost(
    "/login",
    loginData,
    actionTypes.LOGIN,
    _.extend(options, { secured: false })
  );
};

/**
 * This is the action creator that gets called whenever the user attemmpts to close a specific error.
 *
 * @param {*} errorKey Key of the error message that needs to be cleared.
 */
export const clearError = errorKey => {
  console.debug(`Clearing error with the key - ${errorKey}`);

  // Return a function from this action creator (i.e. using redux-thunk for asynchronous actions).
  // This method takes a dispatch function that will be used to dispatch the action instance when
  // the data becomes available.
  // getState is the second parameter that gets passed by redux-thunk. This will enable us to fetch
  // any information from the redux store.
  return async (dispatch, getState) => {
    console.debug(
      "Attempting to dispatch the message to clear a specific error..."
    );
    dispatch({
      type: actionTypes.CLEAR_ERROR,
      payload: errorKey
    });
  };
};

/**
 * This is the action creator that gets called whenever an error occurs in the application.
 */
export const clearAllErrors = () => {
  console.debug("Clearing all errors from the redux store.");

  // Return a function from this action creator (i.e. using redux-thunk for asynchronous actions).
  // This method takes a dispatch function that will be used to dispatch the action instance when
  // the data becomes available.
  // getState is the second parameter that gets passed by redux-thunk. This will enable us to fetch
  // any information from the redux store.
  return async (dispatch, getState) => {
    console.debug("Attempting to dispatch the message to clear all errors...");
    dispatch({
      type: actionTypes.CLEAR_ALL_ERRORS
    });
  };
};

/**
 * This is the action creator that gets called whenever we need to retrieve the available platforms in the system.
 *
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful retrieval of the platforms in the system.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getAllPlatformsAction = options => {
  console.debug("Retrieving all the platforms in the system");
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 100 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 100;
  }

  return ApplicationApi.invokeGet(
    PLATFORM_API_ENDPOINT,
    actionTypes.GET_ALL_PLATFORMS,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called to create a new platform in the system.
 *
 * @param {*} payload Object that holds the details of the platform being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the platform.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createPlatformAction = (payload, options) => {
  console.debug(`Creating a new platform with payload as ${payload}`);
  return ApplicationApi.invokePost(
    PLATFORM_API_ENDPOINT,
    payload,
    actionTypes.CREATE_PLATFORM,
    options
  );
};

/**
 * This is the action creator that gets called to update the details of an existing platform.
 *
 * @param {*} platformId Unique identifier of the platform being updated.
 * @param {*} payload Object that holds the details of the platform being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successfully updating an existing platform.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updatePlatformAction = (platformId, payload, options) => {
  console.debug(
    `Updating the platform ${platformId} with payload ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePut(
    `${PLATFORM_API_ENDPOINT}/${platformId}`,
    payload,
    actionTypes.UPDATE_PLATFORM,
    options
  );
};

/**
 * This is the action creator that gets called to delete a specific platform.
 *
 * @param {*} platformId Unique identifier of the platform to be deleted.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the platform.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deletePlatformAction = (platformId, options) => {
  console.debug(`Deleting the platform - ${platformId}`);
  return ApplicationApi.invokeDelete(
    `${PLATFORM_API_ENDPOINT}/${platformId}`,
    actionTypes.DELETE_PLATFORM,
    options
  );
};

/**
 * This is the action creator that gets called to fetch the details of a specific platform.
 *
 * @param {*} platformId Unique identifier of the platform whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of a platform.
 */
export const getPlatformAction = (platformId, options) => {
  console.debug(`Retrieving details for platform - ${platformId}`);
  return ApplicationApi.invokeGet(
    `${PLATFORM_API_ENDPOINT}/${platformId}`,
    actionTypes.GET_PLATFORM,
    options
  );
};

/**
 * This is the action creator that gets called whenever we need to retrieve the available frameworks in the system.
 *
 * @param {*} platformId Unique identifier of the platform under which the frameworks have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful retrieval of the frameworks in the system.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getAllFrameworks = (platformId, options) => {
  console.debug(`Retrieving all frameworks for platform ${platformId}`);
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 100 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 100;
  }

  return ApplicationApi.invokeGet(
    `administration/platforms/${platformId}/frameworks`,
    actionTypes.GET_ALL_FRAMEWORKS,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called to create a new framework in the system.
 *
 * @param {*} platformId Unique identifier of the platform under which the framework is being created.
 * @param {*} payload Object that holds the details of the framework being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the framework.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createFrameworkAction = (platformId, payload, options) => {
  console.debug(`Creating a new framework with payload as ${payload}`);
  return ApplicationApi.invokePost(
    `/administration/platforms/${platformId}/frameworks`,
    payload,
    actionTypes.CREATE_FRAMEWORK,
    options
  );
};

/**
 * This is the action creator that gets called to update the details of an existing framework.
 *
 * @param {*} platformId Unique identifier of the platform under which the framework is being updated.
 * @param {*} frameworkId Unique identifier of the framework being updated.
 * @param {*} payload Object that holds the details of the framework being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successfully updating an existing framework.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updateFrameworkAction = (
  platformId,
  frameworkId,
  payload,
  options
) => {
  console.debug(
    `Updating the framework ${frameworkId} with payload ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePut(
    `/administration/platforms/${platformId}/frameworks/${frameworkId}`,
    payload,
    actionTypes.UPDATE_FRAMEWORK,
    options
  );
};

/**
 * This is the action creator that gets called to delete a specific framework.
 *
 * @param {*} platformId Unique identifier of the platform under which a specific framework has to be deleted.
 * @param {*} frameworkId Unique identifier of the framework to be deleted.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the framework.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deleteFrameworkAction = (platformId, frameworkId, options) => {
  console.debug(`Deleting the framework - ${frameworkId}`);
  return ApplicationApi.invokeDelete(
    `/administration/platforms/${platformId}/frameworks/${frameworkId}`,
    actionTypes.DELETE_FRAMEWORK,
    options
  );
};

/**
 * This is the action creator that gets called to fetch the details of a specific framework.
 *
 * @param {*} platformId Unique identifier of the platform under which the details of a specific framework have to be retrieved.
 * @param {*} frameworkId Unique identifier of the framework whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of a framework.
 */
export const getFrameworkAction = (platformId, frameworkId, options) => {
  console.debug(`Retrieving details for framework - ${frameworkId}`);
  return ApplicationApi.invokeGet(
    `/administration/platforms/${platformId}/frameworks/${frameworkId}`,
    actionTypes.GET_FRAMEWORK,
    options
  );
};
