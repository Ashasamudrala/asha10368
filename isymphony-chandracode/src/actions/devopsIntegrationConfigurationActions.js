/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import * as ApplicationApi from "../api/ApplicationApi";
import actionTypes from "./actionTypes";

// DevOps Integration Configuration endpoint.
const DEVOPS_INTEGRATION_CONFIGURATION_API_ENDPOINT =
  "/administration/devops/continuousIntegration/configurations";

/**
 * This is the action creator that gets called whenever the user attempts to create a new DevOps Integration Configuration from within the application.
 *
 * @param {*} payload Object that holds the details of the DevOps Delivery Configuration being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the DevOps Integration Configuration.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createDevOpsIntegrationConfigurationAction = (
  payload,
  options
) => {
  console.debug(
    `Creating a new DevOps Integration Configuration with payload as ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePost(
    DEVOPS_INTEGRATION_CONFIGURATION_API_ENDPOINT,
    payload,
    actionTypes.CREATE_DEVOPS_INTEGRATION_CONFIGURATION,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to update an existing DevOps Integration Configuration from within the application.
 *
 * @param {*} configurationId Unique identifier of the DevOps Delivery Configuration being updated.
 * @param {*} payload Object that holds the details of the DevOps Delivery Configuration being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful updates to the DevOps Integration Configuration.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updateDevOpsIntegrationConfigurationAction = (
  configurationId,
  payload,
  options
) => {
  console.debug(
    `Updating the DevOps Integration Configuration ${configurationId} with payload ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePut(
    `${DEVOPS_INTEGRATION_CONFIGURATION_API_ENDPOINT}/${configurationId}`,
    payload,
    actionTypes.UPDATE_DEVOPS_INTEGRATION_CONFIGURATION,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to delete an existing DevOps Integration Configuration from within the application.
 *
 * @param {*} configurationId Unique identifier of the DevOps Delivery Configuration being deleted.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the DevOps Integration Configuration.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deleteDevOpsIntegrationConfigurationAction = (
  configurationId,
  options
) => {
  console.debug(
    `Deleting the DevOps Integration Configuration - ${configurationId}`
  );
  return ApplicationApi.invokeDelete(
    `${DEVOPS_INTEGRATION_CONFIGURATION_API_ENDPOINT}/${configurationId}`,
    actionTypes.DELETE_DEVOPS_INTEGRATION_CONFIGURATION,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve all the DevOps Integration Configurations configured in the system.
 *
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of the DevOps Integration Configurations.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getAllDevOpsIntegrationConfigurationsAction = options => {
  console.debug(
    "Retrieving all DevOps Integration Configurations in the system"
  );
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 20 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 20;
  }
  return ApplicationApi.invokeGet(
    DEVOPS_INTEGRATION_CONFIGURATION_API_ENDPOINT,
    actionTypes.GET_ALL_DEVOPS_INTEGRATION_CONFIGURATIONS,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve the details of a specific DevOps Integration Configuration.
 *
 * @param {*} configurationId Unique identifier of the DevOps Delivery Configuration whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of the DevOps Integration Configuration details.
 */
export const getDevOpsIntegrationConfigurationAction = (
  configurationId,
  options
) => {
  console.debug(
    `Retrieving details for DevOps Integration Configuration - ${configurationId}`
  );
  return ApplicationApi.invokeGet(
    `${DEVOPS_INTEGRATION_CONFIGURATION_API_ENDPOINT}/${configurationId}`,
    actionTypes.GET_DEVOPS_INTEGRATION_CONFIGURATION,
    options
  );
};
