/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import * as ApplicationApi from "../api/ApplicationApi";
import actionTypes from "./actionTypes";

// Project API Endpoint
const PROJECT_API_ENDPOINT = "/projects";

/**
 * This is the action creator that gets called whenever the user attempts to create a new project from within the application.
 *
 * @param {*} payload Object that holds the details of the project being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the project. Project object is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createProjectAction = (payload, options) => {
  console.debug(`Creating a new project with payload as ${payload}`);
  return ApplicationApi.invokePost(
    PROJECT_API_ENDPOINT,
    payload,
    actionTypes.CREATE_PROJECT,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to update an existing project from within the application.
 *
 * @param {*} projectId Unique identifier of the project being updated.
 * @param {*} payload Object that holds the details of the project being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the project. Project object is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updateProjectAction = (projectId, payload, options) => {
  console.debug(
    `Updating the project ${projectId} with payload ${JSON.stringify(payload)}`
  );
  return ApplicationApi.invokePut(
    `${PROJECT_API_ENDPOINT}/${projectId}`,
    payload,
    actionTypes.UPDATE_PROJECT,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to delete an existing project from within the application.
 *
 * @param {*} projectId Unique identifier of the project being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the project.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deleteProjectAction = (projectId, options) => {
  console.debug(`Deleting the project - ${projectId}`);
  return ApplicationApi.invokeDelete(
    `${PROJECT_API_ENDPOINT}/${projectId}`,
    actionTypes.DELETE_PROJECT,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve their projects.
 *
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the project. Project object is passed to this callback.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getMyProjects = options => {
  console.debug("Retrieving my projects...");
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 20 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 20;
  }
  return ApplicationApi.invokeGet(
    PROJECT_API_ENDPOINT,
    actionTypes.GET_MY_PROJECTS,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve their projects.
 *
 * @param {*} projectId Unique identifier of the project whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the project. Project object is passed to this callback.
 */
export const getProjectAction = (projectId, options) => {
  console.debug(`Retrieving details for project - ${projectId}`);
  return ApplicationApi.invokeGet(
    `${PROJECT_API_ENDPOINT}/${projectId}`,
    actionTypes.GET_PROJECT,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve their projects.
 *
 * @param {*} projectId Unique identifier of the project whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the project. Project object is passed to this callback.
 */
export const downloadGeneratedCodeForProject = (projectId, options) => {
  console.debug("Downloading generated for project - " + projectId);
  return ApplicationApi.invokeGet(
    `${PROJECT_API_ENDPOINT}/${projectId}/download`,
    null,
    _.extend(options, { responseType: "blob" })
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve their jobs.
 *
 * @param {*} projectId Unique identifier of the project for which the jobs have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful retrieval of the user's jobs. Job objects are passed to the callback
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getMyJobs = (projectId, options) => {
  console.debug(`Retrieving the jobs for project - ${projectId}`);
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 100 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 100;
  }

  return ApplicationApi.invokeGet(
    `${PROJECT_API_ENDPOINT}/${projectId}/jobs`,
    actionTypes.GET_MY_JOBS,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve their jobs.
 *
 * @param {*} project Project details.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * redirectUrl: Url to be redirected to upon successful completion of the operation.
 */
export const createJob = (project, options) => {
  console.debug(`Initiating a job for the project - ${project.id}`);
  const payload = {
    name: `${project.name} - job`,
    description: `Job for project - ${project.name}`,
    projectId: project.id
  };
  return ApplicationApi.invokePost(
    `${PROJECT_API_ENDPOINT}/${project.id}/jobs`,
    payload,
    actionTypes.CREATE_JOB,
    options
  );
};
