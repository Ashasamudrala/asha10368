/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import * as ApplicationApi from "../api/ApplicationApi";
import actionTypes from "./actionTypes";

// Code Repository endpoint.
const REPOSITORY_API_ENDPOINT =
  "/administration/devops/repositories/configurations";

/**
 * This is the action creator that gets called whenever the user attempts to create a new code repository from within the application.
 *
 * @param {*} payload Object that holds the details of the code repository being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful creation of the code repository. Code Repository object is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createRepositoryAction = (payload, options) => {
  console.debug(
    `Creating a new code repository with payload as ${JSON.stringify(payload)}`
  );
  return ApplicationApi.invokePost(
    REPOSITORY_API_ENDPOINT,
    payload,
    actionTypes.CREATE_REPOSITORY,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to update an existing code repository from within the application.
 *
 * @param {*} repositoryId Unique identifier of the repository being updated.
 * @param {*} payload Object that holds the details of the repository being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful updates to the repository. Code Repository object is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updateRepositoryAction = (repositoryId, payload, options) => {
  console.debug(
    `Updating the code repository ${repositoryId} with payload ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePut(
    `${REPOSITORY_API_ENDPOINT}/${repositoryId}`,
    payload,
    actionTypes.UPDATE_REPOSITORY,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to delete an existing code repository from within the application.
 *
 * @param {*} repositoryId Unique identifier of the code repository being deleted.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the code repository.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deleteRepositoryAction = (repositoryId, options) => {
  console.debug(`Deleting the code repository - ${repositoryId}`);
  return ApplicationApi.invokeDelete(
    `${REPOSITORY_API_ENDPOINT}/${repositoryId}`,
    actionTypes.DELETE_REPOSITORY,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve all the code repositories configured in the system.
 *
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of the code repositories.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getAllRepositoriesAction = options => {
  console.debug("Retrieving all code repositories in the system");
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 20 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 20;
  }
  return ApplicationApi.invokeGet(
    REPOSITORY_API_ENDPOINT,
    actionTypes.GET_ALL_REPOSITORIES,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to retrieve the details of a specific code repository.
 *
 * @param {*} repositoryId Unique identifier of the code repository whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of the code repository details.
 */
export const getRepositoryAction = (repositoryId, options) => {
  console.debug(`Retrieving details for code repository - ${repositoryId}`);
  return ApplicationApi.invokeGet(
    `${REPOSITORY_API_ENDPOINT}/${repositoryId}`,
    actionTypes.GET_REPOSITORY,
    options
  );
};
