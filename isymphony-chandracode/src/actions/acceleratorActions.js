/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import * as ApplicationApi from "../api/ApplicationApi";
import actionTypes from "./actionTypes";

// Categories Endpoint
const CATEGORIES_API_ENDPOINT = "/administration/categories";

/**
 * This is the action creator that gets called to retrieve all the categories in the system.
 *
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful retrieval of the categories.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getAllCategoriesAction = options => {
  console.debug("Retrieving all the categories in the system.");
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 100 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 100;
  }

  return ApplicationApi.invokeGet(
    CATEGORIES_API_ENDPOINT,
    actionTypes.GET_ALL_CATEGORIES,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called during the category creation. To the options object, the caller
 * can provide 'onSuccess' and 'redirectUrl'.
 *
 * @param {*} newCategoryData Object that holds the details of the category being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful creation of the category. Category object is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createCategoryAction = (newCategoryData, options) => {
  console.debug(
    `Attempting to create a new category with payload - ${JSON.stringify(
      newCategoryData
    )}`
  );
  return ApplicationApi.invokePost(
    CATEGORIES_API_ENDPOINT,
    newCategoryData,
    actionTypes.CREATE_CATEGORY,
    options
  );
};

/**
 * This is the action creator that gets called whenever the user attempts to update an existing category from within the application.
 *
 * @param {*} categoryId Unique identifier of the category being updated.
 * @param {*} updatedCategoryData Object that holds the details of the category being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful update to the category. Category object is passed to this callback.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updateCategoryAction = (
  categoryId,
  updatedCategoryData,
  options
) => {
  console.debug(
    `Attempting to update the category ${categoryId} with payload - ${JSON.stringify(
      updatedCategoryData
    )}`
  );
  return ApplicationApi.invokePut(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}`,
    updatedCategoryData,
    actionTypes.UPDATE_CATEGORY,
    options
  );
};

/**
 * This is the action creator that gets called to delete a specific category.
 *
 * @param {*} categoryId Unique identifier of the category to be deleted.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the category.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deleteCategoryAction = (categoryId, options) => {
  console.debug(`Deleting the category - ${categoryId}`);
  return ApplicationApi.invokeDelete(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}`,
    actionTypes.DELETE_CATEGORY,
    options
  );
};

/**
 * This is the action creator that gets called to fetch the details of a specific category.
 *
 * @param {*} categoryId Unique identifier of the category whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of a category.
 */
export const getCategoryAction = (categoryId, options) => {
  console.debug(`Retrieving details for category - ${categoryId}`);
  return ApplicationApi.invokeGet(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}`,
    actionTypes.GET_CATEGORY,
    options
  );
};

/**
 * This is the action creator that gets called to retrieve the accelerators in the system.
 *
 * @param {*} categoryId Unique identifier of the category
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful retrieval of accelerators.
 * params: Object that holds the pagination settings i.e. pageNumber and pageSize
 */
export const getAllAcceleratorsAction = (categoryId, options) => {
  console.debug(`Retrieving all accelerators for category - ${categoryId}`);
  const { params } = options || {};
  const { pageNumber, pageSize } = params || { pageNumber: 0, pageSize: 100 };
  if (!_.isNumber(pageNumber) || pageNumber < 0) {
    params.pageNumber = 0;
  }
  if (!_.isNumber(pageSize) || pageSize <= 0) {
    params.pageSize = 100;
  }
  return ApplicationApi.invokeGet(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}/accelerators`,
    actionTypes.GET_ALL_ACCELERATORS,
    _.extend(options, { params: params })
  );
};

/**
 * This is the action creator that gets called to create a new accelerator in the system.
 *
 * @param {*} categoryId Unique identifier of the category.
 * @param {*} payload Object that holds the details of the accelerator being created.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful creation of the accelerator.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const createAcceleratorAction = (categoryId, payload, options) => {
  console.debug(
    `Attempting to create a new accelerator for category ${categoryId} with payload - ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePost(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}/accelerators`,
    payload,
    actionTypes.CREATE_ACCELERATOR,
    options
  );
};

/**
 * This is the action creator that gets called to update the details of an existing accelerator.
 *
 * @param {*} categoryId Unique identifier of the category.
 * @param {*} acceleratorId Unique identifier of the accelerator being updated.
 * @param {*} payload Object that holds the details of the accelerator being updated.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * onSuccess: callback function that is called after successful updates to the accelerator.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const updateAcceleratorAction = (
  categoryId,
  acceleratorId,
  payload,
  options
) => {
  console.debug(
    `Attempting to update the accelerator ${acceleratorId} under category ${categoryId} with payload - ${JSON.stringify(
      payload
    )}`
  );
  return ApplicationApi.invokePut(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}/accelerators/${acceleratorId}`,
    payload,
    actionTypes.UPDATE_ACCELERATOR,
    options
  );
};

/**
 * This is the action creator that gets called to delete a specific accelerator.
 *
 * @param {*} categoryId Unique identifier of the category.
 * @param {*} acceleratorId Unique identifier of the accelerator being deleted.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful deletion of the accelerator.
 * redirect: Url that the user will be re-directed to after delegating to the success callback.
 */
export const deleteAcceleratorAction = (categoryId, acceleratorId, options) => {
  console.debug(
    `Deleting the accelerator ${acceleratorId} belonging to category ${categoryId}`
  );
  return ApplicationApi.invokeDelete(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}/accelerators/${acceleratorId}`,
    actionTypes.DELETE_ACCELERATOR,
    options
  );
};

/**
 * This is the action creator that gets called to fetch the details of a specific accelerator.
 *
 * @param {*} categoryId Unique identifier of the category.
 * @param {*} acceleratorId Unique identifier of the accelerator whose details have to be retrieved.
 * @param {*} options Object that holds configuration options. This object supports below properties:
 *
 * success: callback function that is called after successful retrieval of the accelerator.
 */
export const getAcceleratorAction = (categoryId, acceleratorId, options) => {
  console.debug(
    `Retrieving details for the accelerator ${acceleratorId} belonging to category ${categoryId}`
  );
  return ApplicationApi.invokeGet(
    `${CATEGORIES_API_ENDPOINT}/${categoryId}/accelerators/${acceleratorId}`,
    actionTypes.GET_ACCELERATOR,
    options
  );
};
