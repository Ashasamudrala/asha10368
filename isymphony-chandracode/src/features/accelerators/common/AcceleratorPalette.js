/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import "./AcceleratorPalette.css";

import _ from "lodash";

import React from "react";
import {
  Checkbox,
  Container,
  Grid,
  Icon,
  Message,
  Table
} from "semantic-ui-react";
import { FieldArray as FikFieldArray } from "formik";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AcceleratorCard from "./AcceleratorCard";

/**
 * Component that is responsible to display the different accelerators available in the system.
 * This is visualized as a sidebar.
 *
 * @author Chandra Veerapaneni
 */
class AcceleratorPalette extends React.Component {
  // Empty row that will be inserted whenever a user checks / unchecks an accelerator in the palette
  static EMPTY_ROW = {
    acceleratorId: "",
    acceleratorVersion: "",
    metadata: {
      name: "",
      description: "",
      version: "1.0.0-SNAPSHOT",
      groupId: "",
      artifactId: "",
      packageName: ""
    }
  };
  // Internal component state.
  state = { selectedAccelerators: [] };

  /**
   * Constructor.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    if (props.selectedAccelerators) {
      this.state.selectedAccelerators = _.clone(props.selectedAccelerators);
    }
  }

  /**
   * This method is responsible to render the accelerators palette.
   */
  render() {
    const { t, accelerators, formikProps } = this.props;
    // Does this component have the necessary inputs provided?
    if (!accelerators) {
      return (
        <Message align="center">
          <Message.Header>
            {t("project.add.form.accelerator.section.empty.title")}
          </Message.Header>
          <Message.Content>
            {t("project.add.form.accelerator.section.palette.invalid.message")}
          </Message.Content>
        </Message>
      );
    }

    return (
      <FikFieldArray
        name="accelerators"
        render={arrayHelpers => (
          // Display a grid containing the accelerators palette and the instance
          // configuration panel.
          <Container fluid>
            <Grid>
              <Grid.Column width={6} className="accelerator-palette">
                {this.renderAcceleratorsPalette(formikProps, arrayHelpers)}
              </Grid.Column>
              <Grid.Column width={10}>
                {this.renderAcceleratorInstanceConfigurationPanels(formikProps)}
              </Grid.Column>
            </Grid>
          </Container>
        )}
      />
    );
  }

  /**
   * This method renders a palette containing the list of accelerators in the system.
   */
  renderAcceleratorsPalette = (formikProps, arrayHelpers) => {
    const { t, categories } = this.props;

    return (
      <Table
        className="accelerator-palette"
        celled
        compact
        structured
        selectable
      >
        {/* Table Header */}
        <Table.Header fullWidth>
          <Table.Row>
            <Table.HeaderCell width={5}>
              {t("category.field.name.2").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={10}>
              {t("accelerator.field.name.2").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={1} />
          </Table.Row>
        </Table.Header>

        {/* Table Body */}
        <Table.Body>
          {_.orderBy(categories, ["name"], "asc").map(category => {
            const catAccels = Object.values(category.accelerators || []);
            const length = catAccels.length === 0 ? 1 : catAccels.length;

            // Loop through each category and render the accelerators in a structured mode.
            return _.map(catAccels, (accelerator, index) => {
              return (
                <Table.Row key={accelerator.id}>
                  {/* Category to which the accelerator belongs */}
                  {index === 0 && (
                    <Table.Cell rowSpan={length}>
                      {category.metadata.icon && (
                        <Icon className={category.metadata.icon} />
                      )}
                      &nbsp;
                      <strong>{category.name}</strong>
                    </Table.Cell>
                  )}

                  {/* Accelerator name and description */}
                  <Table.Cell>
                    <p>
                      <strong>{accelerator.name}</strong>
                    </p>
                    <p>{accelerator.description}</p>
                  </Table.Cell>

                  {/* Checkbox */}
                  <Table.Cell collapsing>
                    <Checkbox
                      name={accelerator.id}
                      checked={this.isAcceleratedSelected(accelerator.id)}
                      onClick={(e, item) => {
                        this.onSelectionChangeHandler(
                          item,
                          formikProps,
                          arrayHelpers
                        );
                      }}
                    />
                  </Table.Cell>
                </Table.Row>
              );
            });
          })}
        </Table.Body>
      </Table>
    );
  };

  /**
   * This method renders the instance configuration panels allowing the developers
   * to provide customized settings for each of the selected accelerator.
   */
  renderAcceleratorInstanceConfigurationPanels = (
    formikProps,
    arrayHelpers
  ) => {
    const { t, accelerators, path } = this.props;
    const userSelectedAccelerators = formikProps.accelerators;

    if (userSelectedAccelerators && userSelectedAccelerators.length > 0) {
      return (
        <Container>
          {_.map(userSelectedAccelerators, (selectedAccelerator, index) => {
            return (
              <div key={selectedAccelerator.acceleratorId}>
                <AcceleratorCard
                  className="accelerator-card"
                  accelerator={accelerators[selectedAccelerator.acceleratorId]}
                  path={`${path}.${index}`}
                />
                <br />
              </div>
            );
          })}
        </Container>
      );
    }

    // No accelerators selected for this project.
    return (
      <Message align="center">
        <Message.Header>
          {t("project.add.form.accelerator.section.empty.title")}
        </Message.Header>
        <Message.Content>
          {t("project.add.form.accelerator.section.empty.message.2")}
        </Message.Content>
      </Message>
    );
  };

  /**
   * Handler method that gets called whenever user selects / deselects a specific accelerator in the palette.
   */
  onSelectionChangeHandler = (item, formikProps, arrayHelpers) => {
    const existingSelections = _.clone(this.state.selectedAccelerators);

    if (item.checked) {
      const acceleratorDefinition = this.props.accelerators[item.name];
      const acceleratorDefinitionMetadata =
        acceleratorDefinition.metadata || {};
      const { defaultArtifactId = "" } = acceleratorDefinitionMetadata;

      const { metadata } = formikProps;
      // Add a array to the collection
      arrayHelpers.insert(
        this.state.selectedAccelerators.length,
        _.cloneDeep(
          _.merge(_.clone(AcceleratorPalette.EMPTY_ROW), {
            acceleratorId: item.name,
            metadata: {
              name: defaultArtifactId,
              groupId: metadata && metadata.groupId ? metadata.groupId : "",
              packageName:
                metadata && metadata.packageName ? metadata.packageName : "",
              description: acceleratorDefinition.description,
              artifactId: defaultArtifactId
            }
          })
        )
      );
      // Update the local component state
      this.setState({
        selectedAccelerators: _.concat(existingSelections, item.name)
      });
    } else {
      // Remove the accelerator from the collection
      const indexToRemove = _.findIndex(
        this.state.selectedAccelerators,
        a => a === item.name
      );
      arrayHelpers.remove(indexToRemove);
      // Update the local component state
      _.remove(existingSelections, selection => selection === item.name);
      this.setState({ selectedAccelerators: existingSelections });
    }
  };

  /**
   * This method returns a boolean indicating if the provided accelerator id has already been selected.
   */
  isAcceleratedSelected = acceleratorId => {
    return _.includes(this.state.selectedAccelerators, acceleratorId);
  };
}

export default connect(null, {})(withTranslation()(AcceleratorPalette));
