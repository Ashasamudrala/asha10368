/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form, Message } from "semantic-ui-react";
import { Field as FikField } from "formik";
import { withTranslation } from "react-i18next";

import Panel from "../../../components/common/panel/Panel";
import getIcon from "../../../config/icons";
import {
  FikInput,
  FikTextArea,
  FikDropdown
} from "../../../components/common/formik-wrappers";

/**
 * Component that displays Accelerator information.
 */
class AcceleratorCard extends React.Component {
  /**
   * Method that is responsible to render the accelerator details on the screen.
   */
  render() {
    const { t, accelerator, path } = this.props;
    if (!accelerator) {
      return (
        <Message align="center">
          <Message.Header>
            {t(
              "project.add.form.accelerator.section.instance.section.invalid.title"
            )}
          </Message.Header>
          <Message.Content>
            {t(
              "project.add.form.accelerator.section.instance.section.invalid.message"
            )}
          </Message.Content>
        </Message>
      );
    }

    if (true) {
      const title = accelerator.name;
      const subtitle = accelerator.version;
      const icon = getIcon("accelerator.icon");
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {/* Dropdown for Accelerator version */}
                <FikField
                  id={`${path}.acceleratorVersion`}
                  label={t("project.field.accelerator.version")}
                  name={`${path}.acceleratorVersion`}
                  placeholder={t(
                    "project.add.form.accelerator.section.field.version.placeholder"
                  )}
                  options={_.map(accelerator.versions, version => {
                    return {
                      text: version.versionName,
                      value: version.versionName
                    };
                  })}
                  component={FikDropdown}
                  required
                />

                {/* Name for this accelerator instance. */}
                <FikField
                  id={`${path}.metadata.name`}
                  name={`${path}.metadata.name`}
                  label={t("project.field.accelerator.instance.metadata.name")}
                  placeholder={t(
                    "project.add.form.accelerator.section.field.instance.name.placeholder"
                  )}
                  config={{ dependentFields: [`${path}.metadata.artifactId`] }}
                  autoComplete="off"
                  component={FikInput}
                  required
                />

                {/* Field for accelerator description. */}
                <FikField
                  id={`${path}.metadata.description`}
                  name={`${path}.metadata.description`}
                  label={t(
                    "project.field.accelerator.instance.metadata.description"
                  )}
                  placeholder={t(
                    "project.add.form.accelerator.section.field.instance.description.placeholder"
                  )}
                  rows={5}
                  component={FikTextArea}
                />

                <Form.Group widths="equal">
                  {/* Group Id for this accelerator instance. */}
                  <FikField
                    id={`${path}.metadata.groupId`}
                    name={`${path}.metadata.groupId`}
                    label={t(
                      "project.field.accelerator.instance.metadata.groupId"
                    )}
                    placeholder={t(
                      "project.add.form.accelerator.section.field.instance.metadata.groupId.placeholder"
                    )}
                    config={{
                      dependentFields: [`${path}.metadata.packageName`]
                    }}
                    component={FikInput}
                    autoComplete="off"
                  />

                  {/* Artifact Id for this accelerator instance */}
                  <FikField
                    id={`${path}.metadata.artifactId`}
                    name={`${path}.metadata.artifactId`}
                    label={t(
                      "project.field.accelerator.instance.metadata.artifactId"
                    )}
                    placeholder={t(
                      "project.add.form.accelerator.section.field.instance.metadata.artifactId.placeholder"
                    )}
                    component={FikInput}
                    autoComplete="off"
                  />
                </Form.Group>

                <Form.Group widths="equal">
                  {/* Version identifier for this accelerator instance */}
                  <FikField
                    id={`${path}.metadata.version`}
                    name={`${path}.metadata.version`}
                    label={t(
                      "project.field.accelerator.instance.metadata.version"
                    )}
                    placeholder={t(
                      "project.add.form.accelerator.section.field.instance.version.placeholder"
                    )}
                    component={FikInput}
                    autoComplete="off"
                  />

                  {/* Package Name for this accelerator instance */}
                  <FikField
                    id={`${path}.metadata.packageName`}
                    name={`${path}.metadata.packageName`}
                    label={t(
                      "project.field.accelerator.instance.metadata.packageName"
                    )}
                    placeholder={t(
                      "project.add.form.accelerator.section.field.instance.metadata.packageName.placeholder"
                    )}
                    component={FikInput}
                    autoComplete="off"
                  />
                </Form.Group>
              </Container>
            )
          }}
        />
      );
    }
  }
}

export default withTranslation()(AcceleratorCard);
