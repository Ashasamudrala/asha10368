/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import {
  Card,
  Loader,
  Container,
  Pagination,
  Divider
} from "semantic-ui-react";
import { connect } from "react-redux";

import JobCard from "./JobCard";
import { getMyJobs } from "../../actions/projectActions";

/**
 * Component that is responsible to display user's jobs.
 *
 * @author Chandra Veerapaneni
 */
class MyJobs extends React.Component {
  /**
   * This method gets called as soon as the component is rendered on the screen.
   */
  componentDidMount() {
    this.props.getMyJobs({});
  }
  /**
   * This method renders the jobs in a card fashion.
   */
  renderJobCards = () => {
    return this.props.myJobs.data.map(job => {
      return <JobCard key={job.id} job={job} />;
    });
  };

  /**
   * This method is responsible to render the projecs on the screen.
   */
  render() {
    if (this.props.myJobs) {
      return (
        <Container>
          <Container align="center">
            <Pagination
              size="mini"
              boundaryRange={0}
              defaultActivePage={1}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              siblingRange={1}
              totalPages={this.props.myJobs.pagination.totalPages}
            />
          </Container>
          <Divider />
          <Card.Group itemsPerRow={3}>{this.renderJobCards()}</Card.Group>
        </Container>
      );
    } else {
      return (
        <Loader active inline="centered" size="medium">
          Loading my jobs...
        </Loader>
      );
    }
  }
}

/**
 * Maps the state to component properties.
 *
 * @param {*} state State
 * @param {*} ownProps Own properties
 */
const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth,
    myJobs: state.jobs.myJobs
  };
};

export default connect(mapStateToProps, {
  getMyJobs
})(MyJobs);
