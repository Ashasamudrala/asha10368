/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Card, Icon, Label, Popup, Button, Divider } from "semantic-ui-react";
import { NavLink } from "react-router-dom";

import { configurePopup } from "../../../utils";

/**
 * Component that renders the job details as a card.
 *
 * @author Chandra Veerapaneni
 */
class JobCard extends React.Component {
  /**
   * This method truncates the provided input upto the specified length and replaces the rest of the characters with ellipsis (...).
   */
  getTruncatedString = (input, length) => {
    return _.truncate(input, {
      length: length
    });
  };

  getStatusDetails = job => {
    const statusDetails = {
      color: "teal",
      message: "Not Started"
    };
    if (job && job.currentExecution && job.currentExecution.status) {
      const { status } = job.currentExecution;
      switch (status) {
        case "STARTED":
          statusDetails.color = "orange";
          statusDetails.message = "Running";
          break;
        case "COMPLETED":
          statusDetails.color = "teal";
          statusDetails.message = "Completed";
          break;
        case "FAILED":
          statusDetails.color = "red";
          statusDetails.message = "Failed";
          break;
        default:
          break;
      }
    }
    return statusDetails;
  };

  /**
   * Method that is responsible to render the card using the provided details.
   */
  render() {
    if (this.props.job) {
      const { job } = this.props;
      const statusDetails = this.getStatusDetails(job);
      return (
        <Card color="teal">
          <Card.Content>
            <Label
              size="mini"
              as="a"
              color={statusDetails.color}
              ribbon="right"
            >
              {statusDetails.message}
            </Label>
            <Popup
              trigger={
                <Card.Header>
                  {this.getTruncatedString(job.name, 48)}
                </Card.Header>
              }
              size="mini"
              wide
            >
              <Popup.Header>Job Name</Popup.Header>
              <Popup.Content>{job.name} </Popup.Content>
            </Popup>
            <Card.Meta>
              <Icon className="fa-address-card" />
              &nbsp;To Be Updated
            </Card.Meta>
            <Card.Description>
              {this.getTruncatedString(job.description, 100)}
              <Divider />
              <Label.Group size="mini">
                <Label image>
                  Java
                  <Label.Detail>1.8</Label.Detail>
                </Label>
                <Label image>
                  Spring Boot
                  <Label.Detail>2.1.7.RELEASE</Label.Detail>
                </Label>
                <Label image>
                  Spring Cloud
                  <Label.Detail>Greenwich.SR2</Label.Detail>
                </Label>
              </Label.Group>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <Popup
              trigger={
                <Button
                  size="mini"
                  icon="edit"
                  as={NavLink}
                  exact
                  to={"/"}
                  color="teal"
                  circular
                />
              }
              size="mini"
            >
              <Popup.Content>View this job</Popup.Content>
            </Popup>
            <Popup
              trigger={
                <Button
                  size="mini"
                  icon="download"
                  as={NavLink}
                  exact
                  to={"/"}
                  color="teal"
                  circular
                />
              }
              size="mini"
            >
              <Popup.Content>Download generated code</Popup.Content>
            </Popup>

            {/* Display the delete button. */}
            {configurePopup(
              <Button
                size="mini"
                icon="delete"
                as={NavLink}
                exact
                to={"/"}
                color="teal"
                circular
                floated="right"
              />,
              "",
              "Delete this job. Note that deleting a job deletes all the output generated as a result of the execution of this job."
            )}
          </Card.Content>
        </Card>
      );
    }

    return <div>Job Card.</div>;
  }
}

export default JobCard;
