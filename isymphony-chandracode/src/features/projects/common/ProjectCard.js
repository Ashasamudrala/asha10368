/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import "./ProjectCard.css";

import _ from "lodash";

import React from "react";
import {
  Button,
  Card,
  Container,
  Divider,
  Icon,
  Label,
  Message,
  Modal,
  Header,
  Image,
  List
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import getIcon from "../../../config/icons";
import { configurePopup } from "../../../utils";
import { createJob } from "../../../actions/projectActions";
import {
  deleteProjectAction,
  downloadGeneratedCodeForProject,
  getMyProjects
} from "../../../actions/projectActions";

/**
 * Component that renders the project details as a card.
 *
 * @author Chandra Veerapaneni
 */
class ProjectCard extends React.Component {
  // Size of the buttons in the button pane
  static DEFAULTS = {
    BUTTON_PANE_BUTTON_SIZE: "tiny",
    DEFAULT_PROJECT_IMAGE: "/assets/images/default.png",
    DEFAULT_PROJECT_STATUS: "NOT_STARTED",
    DEFAULT_PROJECT_STATUS_COLOR: "grey"
  };

  // Local component state
  state = { actionBeingPerformed: false, open: false };

  /**
   * Method that is responsible to render the card using the provided details.
   */
  render() {
    const { t, project, framework } = this.props;

    if (project) {
      const image =
        framework.metadata.image || ProjectCard.DEFAULTS.DEFAULT_PROJECT_IMAGE;
      return (
        <Card raised>
          <Image src={image} wrapped ui={false} />

          {/* Content */}
          <Card.Content>
            {this.renderJobStatus(this.props)}

            {/* Configure the popup on the header. */}
            {this.renderCardHeader(this.props)}

            {/* Customer details */}
            {this.renderCustomerDetails(this.props)}

            {this.drawDivider(
              getIcon("description.icon"),
              t("project.card.description.title").toUpperCase()
            )}

            {/* Project description */}
            {this.renderProjectDescriptionDetails(this.props)}

            {this.drawDivider(
              getIcon("version.icon"),
              t("project.card.versions.title").toUpperCase()
            )}

            {/* Platform and Framework versions */}
            {this.renderPlatformAndFrameworkDetails(this.props)}

            {this.drawDivider(
              getIcon("accelerator.icon"),
              t("project.card.accelerators.title").toUpperCase()
            )}

            {/* Accelerator Details */}
            {this.renderAcceleratorDetails(this.props)}

            {this.drawDivider(
              getIcon("money.icon"),
              t("project.card.effort.title").toUpperCase()
            )}

            {/* Render the effort and cost savings */}
            {this.renderEffort(this.props)}
          </Card.Content>

          {/* Extra content where buttons are hosted. */}
          <Card.Content extra>
            {/* Render the buttons pane */}
            {this.renderButtonsPane(this.props)}
          </Card.Content>
        </Card>
      );
    }
    return <Container fluid>{t("project.card.invalid")}</Container>;
  }

  /**
   * This method renders the card header using the project name.
   */
  renderCardHeader = ({ t, project }) => {
    // Build a card header and configure a popup over it.
    return configurePopup(
      <Card.Header>
        <br />
        {project.name}
      </Card.Header>,
      t("project.field.name.2"),
      project.name
    );
  };

  /**
   * This method renders the customer details in the card.
   */
  renderCustomerDetails = ({ project }) => {
    return (
      <Card.Meta>
        <Icon className={getIcon("customer.icon")} />
        &nbsp;{project.customer.name}
      </Card.Meta>
    );
  };

  /**
   * This method renders the project description details in the card.
   */
  renderProjectDescriptionDetails = ({ t, project }) => {
    return (
      <Card.Description>
        {project.description || t("project.card.description.none.message")}
      </Card.Description>
    );
  };

  /**
   * This method renders the platform and framework details on the card.
   */
  renderPlatformAndFrameworkDetails = ({
    platform,
    platformVersion,
    framework,
    frameworkVersion,
    color = "grey",
    size = "tiny"
  }) => {
    return (
      <Label.Group size={size}>
        <Label color={color} basic>
          {platform.name}
          <Label.Detail>{platformVersion}</Label.Detail>
        </Label>
        <Label color={color} basic>
          {framework.name}
          <Label.Detail>{frameworkVersion}</Label.Detail>
        </Label>
      </Label.Group>
    );
  };

  /**
   * This method renders the details of the accelerators used in the project.
   */
  renderAcceleratorDetails = ({
    t,
    project,
    acceleratorDefinitions,
    color = "grey",
    size = "tiny"
  }) => {
    const projectAccelerators = project.accelerators;
    if (projectAccelerators && projectAccelerators.length > 0) {
      const acceleratorDefinitionsMap = _.mapKeys(acceleratorDefinitions, "id");

      return (
        <Label.Group size={size}>
          {_.map(projectAccelerators, projectAccelerator => {
            return (
              <Label color={color} key={projectAccelerator.acceleratorId} basic>
                {
                  acceleratorDefinitionsMap[projectAccelerator.acceleratorId]
                    .name
                }
                <Label.Detail>
                  {projectAccelerator.metadata.version}
                </Label.Detail>
              </Label>
            );
          })}
        </Label.Group>
      );
    } else {
      return (
        <Container fluid>
          {t("project.card.accelerators.none.message")}
        </Container>
      );
    }
  };

  /**
   * This method draws a divider between various details in the card.
   */
  drawDivider = (icon, title, headerSize = "h6") => {
    return (
      <Divider horizontal className="left aligned">
        <Header as={headerSize}>
          <Icon className={icon} />
          {title}
        </Header>
      </Divider>
    );
  };

  /**
   * This method renders the efforts saved in this project.
   */
  renderEffort = ({ t, project, acceleratorDefinitions, billRate = 100 }) => {
    const { accelerators } = project;
    if (accelerators && accelerators.length > 0) {
      var effortSavings = 0;
      const acceleratorDefinitionsMap = _.mapKeys(acceleratorDefinitions, "id");
      _.each(accelerators, accelerator => {
        const acceleratorDefinition =
          acceleratorDefinitionsMap[accelerator.acceleratorId];
        if (acceleratorDefinition) {
          const acceleratorVersion = _.find(
            acceleratorDefinition.versions,
            version => version.versionName === accelerator.metadata.version
          );
          if (acceleratorVersion) {
            effortSavings =
              effortSavings +
              _.sum(
                _.map(
                  _.flatMap(acceleratorVersion.features),
                  "effortInPersonHours"
                )
              );
          }
        }
      });
      const costSavings = billRate * effortSavings;
      return (
        <Container textAlign="center">
          <Message size="small" info>
            {t("project.card.effort.message")
              .replace("{effortSavings}", effortSavings)
              .replace("{costSavings}", costSavings)
              .replace("{billRate}", billRate)}
          </Message>
        </Container>
      );
    }
    return (
      <Container textAlign="center">
        <Message size="small" success>
          {t("project.card.effort.none.message")}
        </Message>
      </Container>
    );
  };

  /**
   * This method renders the button pane containing the actions for edit, delete, etc.
   */
  renderButtonsPane = props => {
    return (
      <Container fluid>
        {/* Edit Project and popover */}
        {this.renderEditProjectButton(props)}

        {/* Edit Project and popover */}
        {this.renderDeleteProjectButton(props)}

        {/* Generate Project button */}
        {this.renderGenerateProjectButton(props)}

        {/* Download code button */}
        {this.renderDownloadButton(props)}

        {/* Delete confirmation modal dialog */}
        {this.renderDeleteConfirmationDialog(props)}
      </Container>
    );
  };

  /**
   * This method renders the edit project button.
   */
  renderEditProjectButton = ({
    t,
    buttonSize = ProjectCard.DEFAULTS.BUTTON_PANE_BUTTON_SIZE,
    project
  }) => {
    return configurePopup(
      <Button
        size={buttonSize}
        color="blue"
        as={NavLink}
        exact
        to={`/projects/edit/${project.id}`}
        basic
      >
        <Icon className={getIcon("edit.icon")} />
        {t("edit")}
      </Button>,
      "",
      t("project.actions.viewEdit")
    );
  };

  /**
   * This method renders the button to generate the code.
   */
  renderGenerateProjectButton = ({
    t,
    buttonSize = ProjectCard.DEFAULTS.BUTTON_PANE_BUTTON_SIZE,
    project
  }) => {
    const { latestJob } = project;
    const enableButton =
      _.isUndefined(latestJob) ||
      _.isNull(latestJob) ||
      (latestJob &&
        (latestJob.status === "COMPLETED" || latestJob.status === "FAILED"));

    // const completed = true;

    // Create the button and configure a popup on that button.
    return configurePopup(
      <Button
        size={buttonSize}
        color="purple"
        disabled={this.state.actionBeingPerformed || !enableButton}
        onClick={() => this.generateCode(project, this.props.createJob)}
        basic
      >
        <Icon className={getIcon("generate.code.icon")} />
        {t("project.actions.generate.code")}
      </Button>,
      "",
      t("project.actions.generate.code.message")
    );
  };

  /**
   * This method renders the download button on the card.
   */
  renderDownloadButton = ({
    t,
    buttonSize = ProjectCard.DEFAULTS.BUTTON_PANE_BUTTON_SIZE,
    project
  }) => {
    const codeGenerationTask = this.findCodeGenerationTask(project);
    const { metadata = {} } = codeGenerationTask || {};
    if (metadata && metadata.codeDownloadUrl) {
      return configurePopup(
        <Button
          size={buttonSize}
          color="teal"
          onClick={() => this.downloadCodeForProject(project)}
          basic
        >
          <Icon className={getIcon("download.icon")} />
          {t("download")}
        </Button>,
        "",
        t("project.actions.download.code.message")
      );
    }
  };

  /**
   * This method renders the delete project button.
   */
  renderDeleteProjectButton = ({
    t,
    buttonSize = ProjectCard.DEFAULTS.BUTTON_PANE_BUTTON_SIZE,
    project
  }) => {
    // Delete Project and popover
    return configurePopup(
      <Button
        size={buttonSize}
        color="red"
        floated="right"
        onClick={() => this.setState({ open: true })}
        basic
      >
        <Icon className={getIcon("delete.icon")} />
        {t("delete")}
      </Button>,
      "",
      t("project.actions.delete.message")
    );
  };

  /**
   * This method renders the job status.
   */
  renderJobStatus = ({ project }) => {
    const statusDetails = this.getJobStatus(project);
    const { latestJob } = project;
    if (latestJob) {
      const { latestTask } = latestJob;
      if (latestTask && latestTask.errors && latestTask.errors.length > 0) {
        return configurePopup(
          <Label
            size="mini"
            as="a"
            color={statusDetails.color}
            ribbon="right"
            basic
          >
            {statusDetails.message}
          </Label>,
          "",
          this.getErrors(latestTask.errors)
        );
      }
    }

    return (
      <Label
        size="mini"
        as="a"
        color={statusDetails.color}
        ribbon="right"
        basic
      >
        {statusDetails.message}
      </Label>
    );
  };

  getErrors = errors => {
    return (
      <List>
        {errors.map(error => {
          return <List.Item key={Math.random()}>{error.message}</List.Item>;
        })}
      </List>
    );
  };

  /**
   * This method renders the confirmation dialog before the user proceeds with the delete functionality.
   */
  renderDeleteConfirmationDialog = ({ t, project }) => {
    return (
      <div floated="right">
        <Modal
          size="mini"
          open={this.state.open}
          onClose={() => this.setState({ open: false })}
        >
          <Modal.Header>{t("project.actions.delete")}</Modal.Header>
          <Modal.Content>
            <p>{t("project.actions.delete.confirmation.message")}</p>
            <p>
              <strong>{project.name}</strong>
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button
              disabled={this.state.actionBeingPerformed}
              onClick={() =>
                this.setState({ actionBeingPerformed: false, open: false })
              }
              color="red"
              basic
            >
              <Icon className={getIcon("no.icon")} />
              {t("no")}
            </Button>

            <Button
              disabled={this.state.actionBeingPerformed}
              onClick={this.deleteProject}
              color="teal"
              basic
            >
              <Icon className={getIcon("yes.icon")} />
              {t("yes")}
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  };

  /**
   * This method triggers the code generation process by submitting the request to the backend.
   */
  generateCode = (project, createJob) => {
    this.setState({ actionBeingPerformed: true });
    createJob(project, {
      redirectUrl: "/projects"
    });
  };

  /**
   * This method returns the job status and the color to be used to render it.
   */
  getJobStatus = project => {
    const statusDetails = {
      color: ProjectCard.DEFAULTS.DEFAULT_PROJECT_STATUS_COLOR,
      message: ProjectCard.DEFAULTS.DEFAULT_PROJECT_STATUS.replace(/_/g, " ")
    };
    const { latestJob } = project;
    if (latestJob) {
      const { status } = latestJob;
      switch (status) {
        case "NOT_STARTED":
          statusDetails.color =
            ProjectCard.DEFAULTS.DEFAULT_PROJECT_STATUS_COLOR;
          break;
        case "FAILED":
          statusDetails.color = "red";
          break;
        case "COMPLETED":
          statusDetails.color = "green";
          break;
        default:
          statusDetails.color = "orange";
          break;
      }
      statusDetails.message = status.replace(/_/g, " ");
    }

    return statusDetails;
  };

  /**
   * For the provided project, this method attempts to find the code generation task
   * for the latest job.
   */
  findCodeGenerationTask = project => {
    const { latestJob } = project;
    if (latestJob && latestJob.tasks && latestJob.tasks.length > 0) {
      return _.find(latestJob.tasks, task => task.type === "CODE_GENERATION");
    }
    return null;
  };

  /**
   * This method downloads the generated code onto the client's machine.
   */
  downloadCodeForProject = project => {
    this.props.downloadGeneratedCodeForProject(project.id, {
      onSuccess: response => {
        const contentDispositionHeaderValue =
          response.headers["content-disposition"] || "";
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute(
          "download",
          contentDispositionHeaderValue
            ? contentDispositionHeaderValue.split(";")[1].split("=")[1]
            : project.name
        );
        document.body.appendChild(link);

        link.click();
      }
    });
  };

  /**
   * This method attempts to delete a project.
   */
  deleteProject = () => {
    this.setState({ actionBeingPerformed: true });
    this.props.deleteProjectAction(this.props.project.id, {
      redirectUrl: "/projects",
      onSuccess: () => {
        this.setState({ actionBeingPerformed: false, open: false });
      }
    });
  };
}

export default connect(null, {
  createJob,
  getMyProjects,
  downloadGeneratedCodeForProject,
  deleteProjectAction
})(withTranslation()(ProjectCard));
