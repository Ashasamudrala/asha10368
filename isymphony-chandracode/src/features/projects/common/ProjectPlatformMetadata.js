/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";

import Tag from "../../../components/common/tags/Tag";
import Tags from "../../../components/common/tags/Tags";

/**
 * Component class that is responsible to render the platform and framework metadata pertaining to a specific project.
 *
 * The data required to render this component is available on the 'this.props' object and the caller is assumed to pass
 * 'project', 'platform' and 'framework' objects.
 *
 * @author Chandra Veerapaneni
 */
class ProjectPlatformMetadata extends React.Component {
  /**
   * This method is responsible to render the platform and framework metadata on the UI.
   */
  render() {
    const dataToRender = [];
    // Destructure the required properties from the 'this.props' object.
    const { project, platform, framework } = this.props;
    if (project) {
      if (platform) {
        // Destructure the platform 'name' and 'version' properties
        const { name, version } = platform;
        dataToRender.push(
          <Tag key={platform.id} text={name} detail={version} />
        );
      }
      if (framework) {
        // Destructure the framework 'name' and 'version' properties
        const { name, version } = framework;
        dataToRender.push(
          <Tag key={framework.id} text={name} detail={version} />
        );

        // Add the modules detials.
        dataToRender.push(
          <Tags
            key={new Date().getTime()}
            content={framework.modules.map(module => {
              return { text: module.name, detail: module.version };
            })}
          />
        );
      }
    }
    return <div>{dataToRender}</div>;
  }
}

export default ProjectPlatformMetadata;
