/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import {
  Button,
  Card,
  Container,
  Divider,
  Icon,
  Message,
  Pagination
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import ProjectCard from "./common/ProjectCard";
import getIcon from "../../config/icons";
import { getAllCategoriesAction } from "../../actions/acceleratorActions";
import { getMyProjects } from "../../actions/projectActions";
import { getAllPlatformsAction } from "../../actions/coreActions";
import { renderPageTitle, renderLoader } from "../../utils";

/**
 * Component that is responsible to display the user's projects.
 *
 * @author Chandra Veerapaneni
 */
class MyProjectsHome extends React.Component {
  // Component state.
  state = {
    platformsDataLoaded: false,
    categoriesDataLoaded: false,
    myProjectsDataLoaded: false
  };

  /**
   * This method gets called as soon as the component is rendered on the screen.
   */
  componentDidMount() {
    this.props.getAllPlatformsAction({
      onSuccess: () => this.handlePlatformsDataLoaded()
    });
    this.props.getAllCategoriesAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.handleCategoriesDataLoaded()
    });
    this.loadMyProjects();
  }

  /**
   * This method is responsible to render the projecs on the screen.
   */
  render() {
    const { t } = this.props;
    const {
      platformsDataLoaded,
      categoriesDataLoaded,
      myProjectsDataLoaded
    } = this.state;

    // If either the platforms or categories or projects have not loaded, return.
    if (!platformsDataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    if (!categoriesDataLoaded) {
      return renderLoader(t("category.loading.message"));
    }

    if (!myProjectsDataLoaded) {
      return renderLoader(t("project.loading.message"));
    }

    const { projects, pagination } = this.props;
    if (projects && pagination && pagination.totalElements > 0) {
      return (
        <Container fluid>
          {this.renderPageTitle()}
          <Divider />
          <br />
          <Card.Group itemsPerRow={3}>{this.renderProjectCards()}</Card.Group>
          <br />
          <Divider />
          <Container align="right">{this.renderPaginationControl()}</Container>
        </Container>
      );
    } else {
      return (
        <Container fluid>
          {this.renderPageTitle()}
          <Divider />
          {this.renderNoProjectsContent()}
        </Container>
      );
    }
  }

  /**
   * This method renders the page title.
   */
  renderPageTitle = () => {
    const { t } = this.props;

    return renderPageTitle(
      t("project.home.title"),
      t("project.home.subtitle"),
      getIcon("project.icon"),
      <Container align="right">
        <Button
          floated="right"
          color="blue"
          onClick={this.loadMyProjects}
          basic
        >
          <Icon className={getIcon("refresh.icon")} />
          {t("refresh")}
        </Button>
        <Button
          floated="right"
          color="blue"
          as={NavLink}
          exact
          to={"/projects/new"}
          basic
        >
          <Icon className={getIcon("project.springboot.icon")} />
          {t("project.actions.springboot.add")}
        </Button>
      </Container>
    );
  };

  /**
   * This method renders the projects in a card fashion.
   */
  renderProjectCards = () => {
    if (this.props.projects) {
      // Destructure platforms and frameworks from props
      const { platforms, frameworks, accelerators } = this.props;
      return Object.values(this.props.projects).map(project => {
        const platformId = project.platform.id;
        const platformVersion = project.platform.version;
        const frameworkId = project.platform.framework.id;
        const frameworkVersion = project.platform.framework.version;
        const projectAccelerators = project.accelerators || [];
        // Find the accelerator id and versions used by this project.
        const projectAcceleratorVersions = _.mapValues(
          _.keyBy(projectAccelerators, "acceleratorId"),
          "acceleratorVersion"
        );
        // For these accelerator id and versions, extract the accelerator data.
        const acceleratorDefinitions = _.map(
          Object.keys(projectAcceleratorVersions),
          acceleratorId => accelerators[acceleratorId]
        );

        return (
          <ProjectCard
            key={project.id}
            platformId={platformId}
            platformVersion={platformVersion}
            frameworkId={frameworkId}
            frameworkVersion={frameworkVersion}
            platform={platforms[platformId]}
            framework={frameworks[frameworkId]}
            project={project}
            acceleratorDefinitions={acceleratorDefinitions}
          />
        );
      });
    }
  };

  /**
   * This method renderes the UI in situations when there are no projects created by the user.
   */
  renderNoProjectsContent = () => {
    const { t } = this.props;

    return (
      <Message align="center">
        <Message.Header>{t("project.empty.title")}</Message.Header>
        <Message.Content>
          <p>{t("project.empty.subtitle")}</p>
          <Button color="blue" as={NavLink} exact to={"/projects/new"} basic>
            <Icon className={getIcon("project.springboot.icon")} />
            {t("project.actions.springboot.add")}
          </Button>
        </Message.Content>
      </Message>
    );
  };

  /**
   * This method attempts to render the pagination control for the projects displayed on the screen.
   */
  renderPaginationControl = () => {
    const { pagination } = this.props;
    if (pagination && pagination.totalPages > 1) {
      return (
        <Pagination
          size="tiny"
          boundaryRange={0}
          defaultActivePage={1}
          ellipsisItem={null}
          firstItem={null}
          lastItem={null}
          siblingRange={1}
          totalPages={this.props.pagination.totalPages}
          onPageChange={(e, data) =>
            this.loadMyProjects(data.activePage, this.props.pagination.pageSize)
          }
        />
      );
    }
  };

  /**
   * This method attempts to load my projects in a paginated manner.
   */
  loadMyProjects = (pageNumber = 0, pageSize = 20) => {
    const pgNum = _.isNumber(pageNumber) ? pageNumber - 1 : 0;
    const pgSize = _.isNumber(pageSize) ? pageSize : 20;
    // Make the asynchronous call to retrieve the projects.
    this.props.getMyProjects({
      params: {
        pageNumber: pgNum,
        pageSize: pgSize
      },
      onSuccess: () => this.handleMyProjectsDataLoaded()
    });
  };

  /**
   * This is the callback function that gets called upon successful loading of platforms data.
   */
  handlePlatformsDataLoaded = () => {
    this.setState({ platformsDataLoaded: true });
  };

  /**
   * This is the callback function that gets called upon successful loading of categories data.
   */
  handleCategoriesDataLoaded = () => {
    this.setState({ categoriesDataLoaded: true });
  };

  /**
   * This is the callback function that gets called upon successful loading of my projects data.
   */
  handleMyProjectsDataLoaded = () => {
    this.setState({ myProjectsDataLoaded: true });
  };
}

/**
 * This method maps the state to component properties.
 *
 * @param {*} state Current state as retrieved from the redux store.
 */
const mapStateToProps = state => {
  return {
    auth: state.auth,
    platforms: state.application.platforms.content,
    frameworks: state.application.frameworks.content,
    categories: state.categories.content,
    accelerators: state.categories.accelerators.content,
    projects: state.projects.content,
    pagination: state.projects.pagination
  };
};

export default connect(mapStateToProps, {
  getMyProjects,
  getAllCategoriesAction,
  getAllPlatformsAction
})(withTranslation()(MyProjectsHome));
