/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import {
  Container,
  Divider,
  Form,
  Header,
  Icon,
  Message
} from "semantic-ui-react";
import { Field as FikField } from "formik";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import AcceleratorPalette from "../../accelerators/common/AcceleratorPalette";
import Panel from "../../../components/common/panel/Panel";
import PlatformFrameworkPanel from "../../common/PlatformFrameworkPanel";
import MetadataForm from "../../../components/common/form/vla/MetadataForm";
import getIcon from "../../../config/icons";
import { Formik } from "formik";
import {
  FikButton,
  FikInput,
  FikTextArea,
  FikDropdown
} from "../../../components/common/formik-wrappers";
import { renderPageTitle } from "../../../utils";

/**
 * An abstract form component that provides the Create / Update of projects within the application.
 *
 * @author Chandra Veerapaneni
 */
class AbstractProjectForm extends React.Component {
  // Defaults
  static DEFAULTS = {
    mode: "add",
    initialValues: {
      name: "",
      description: "",
      version: "1.0.0-SNAPSHOT",
      customer: {
        name: "",
        copyright: {
          period: ""
        }
      },
      author: {
        name: "",
        userName: ""
      },
      metadata: {
        groupId: "",
        artifactId: "",
        packageName: ""
      },
      accelerators: [],
      codeRepositoryConfigurationInstance: {
        codeRepositoryId: "",
        accountName: "",
        userName: "",
        personalAccessToken: "",
        branchName: "",
        metadata: []
      },
      continuousIntegrationConfigurationInstance: {
        configurationId: "",
        accountName: "",
        userName: "",
        personalAccessToken: "",
        metadata: []
      },
      continuousDeliveryConfigurationInstance: {
        configurationId: "",
        metadata: []
      }
    }
  };

  /**
   * This method gets called to render the form on the screen.
   */
  render() {
    const {
      t,
      title = t("project.add.form.title"),
      subTitle = t("project.add.form.subtitle"),
      icon = getIcon("project.icon.2"),
      initialValues = AbstractProjectForm.DEFAULTS.initialValues,
      mode = AbstractProjectForm.DEFAULTS.mode
    } = this.props;

    return (
      <Container fluid>
        {renderPageTitle(title, subTitle, icon)}

        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon name={icon} />
            {mode === "add"
              ? t("project.add.form.divider.title")
              : t("project.update.form.divider.title")}
          </Header>
        </Divider>
        <br />

        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the basic project details */}
              {this.renderBasicDetailsPanel()}

              <br />
              <br />

              {/* Render the fields to capture the customer details */}
              {this.renderCustomerDetailsPanel()}

              <br />
              <br />

              {/* Render the fields to capture the author details */}
              {this.renderAuthorDetailsPanel()}

              <br />
              <br />

              {/* Accelerators Section */}
              {this.renderAcceleratorsPalettePanel(props)}

              <br />
              <br />

              {/* Code Repository configuration Section */}
              {this.renderCodeRepositoryConfigurationPanel(props)}

              <br />
              <br />

              {/* Continuous Integration Section */}
              {this.renderContinuousIntegrationConfigurationPanel(props)}

              <br />
              <br />

              {/* Continuous Delivery Section */}
              {this.renderContinuousDeliveryConfigurationPanel(props)}

              <br />
              <br />

              {/* Render button panel */}
              {this.renderButtonPanel({ t, mode }, props)}
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic project details.
   */
  renderBasicDetailsPanel = () => {
    const { t, platforms, frameworks } = this.props;

    // Basic details for the project
    return (
      <Panel
        options={{
          title: t("project.add.form.details.section.title").toUpperCase(),
          subtitle: t("project.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container fluid>
              {/* Field for project name. */}
              <FikField
                id="project-field-name"
                name="name"
                label={t("project.field.name")}
                placeholder={t(
                  "project.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />

              {/* Field for project description. */}
              <FikField
                id="project-field-description"
                name="description"
                label={t("project.field.description")}
                placeholder={t(
                  "project.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />

              <br />
              {this.drawDivider(
                getIcon("version.icon"),
                t(
                  "project.add.form.details.section.platformAndFramework.section.title"
                ).toUpperCase()
              )}
              <br />

              {/* Display the dropdowns for Platform / Platform Version / Framework / Framework Version */}
              <PlatformFrameworkPanel
                path="platform"
                platforms={platforms}
                frameworks={frameworks}
              />

              <br />
              {this.drawDivider(
                getIcon("maven.icon"),
                t(
                  "project.add.form.details.section.maven.section.title"
                ).toUpperCase()
              )}
              <br />

              {/* Project Maven settings panel */}
              {this.renderProjectMavenSettingsPanel()}
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the maven settings for the project.
   */
  renderProjectMavenSettingsPanel = () => {
    const { t } = this.props;

    return (
      <Container fluid>
        <Form.Group widths="equal">
          {/* Field for project groupId. */}
          <FikField
            id="project-field-groupId"
            name="metadata.groupId"
            label={t("project.field.groupId")}
            placeholder={t(
              "project.add.form.details.section.maven.section.field.groupId.placeholder"
            )}
            config={{ dependentFields: ["metadata.packageName"] }}
            autoComplete="off"
            component={FikInput}
            required
          />

          {/* Field for project artifactId. */}
          <FikField
            id="project-field-artifactId"
            name="metadata.artifactId"
            label={t("project.field.artifactId")}
            placeholder={t(
              "project.add.form.details.section.maven.section.field.artifactId.placeholder"
            )}
            autoComplete="off"
            component={FikInput}
            required
          />

          {/* Field for project version. */}
          <FikField
            id="project-field-version"
            name="version"
            label={t("project.field.version")}
            placeholder={t(
              "project.add.form.details.section.maven.section.field.version.placeholder"
            )}
            autoComplete="off"
            component={FikInput}
            required
          />
        </Form.Group>

        <Form.Group widths="equal">
          {/* Field for project packageName. */}
          <FikField
            id="project-field-packageName"
            name="metadata.packageName"
            label={t("project.field.packageName")}
            placeholder={t(
              "project.add.form.details.section.maven.section.field.packageName.placeholder"
            )}
            autoComplete="off"
            component={FikInput}
            required
          />
        </Form.Group>
      </Container>
    );
  };

  /**
   * This method renders the fields to capture the basic project details.
   */
  renderCustomerDetailsPanel = () => {
    const { t } = this.props;

    // Customer details
    return (
      <Panel
        options={{
          title: t("project.add.form.customer.section.title").toUpperCase(),
          subtitle: t("project.add.form.customer.section.subtitle"),
          icon: getIcon("customer.icon"),
          content: (
            <Container fluid>
              {/* Field for customer name name. */}
              <FikField
                id="project-field-customer-name"
                name="customer.name"
                label={t("project.field.customer.name")}
                placeholder={t(
                  "project.add.form.customer.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for customer copyright period. */}
              <FikField
                id="project-field-customer-copyright-period"
                name="customer.copyright.period"
                label={t("project.field.customer.copyright.period")}
                placeholder={t(
                  "project.add.form.customer.section.field.copyright.period.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the author details.
   */
  renderAuthorDetailsPanel = () => {
    const { t } = this.props;

    // Author details
    return (
      <Panel
        options={{
          title: t("project.add.form.author.section.title").toUpperCase(),
          subtitle: t("project.add.form.author.section.subtitle"),
          icon: getIcon("author.icon"),
          content: (
            <Container fluid>
              {/* Field for author name. */}
              <FikField
                id="project-field-author-name"
                name="author.name"
                label={t("project.field.author.name")}
                placeholder={t(
                  "project.add.form.author.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for author username. */}
              <FikField
                id="project-field-author-username"
                name="author.userName"
                label={t("project.field.author.username")}
                placeholder={t(
                  "project.add.form.author.section.field.username.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the Accelerators palette
   */
  renderAcceleratorsPalettePanel = formikProps => {
    const { t, categories, accelerators, selectedAccelerators } = this.props;

    // Accelerators Palette and Instance Configuration panel.
    return (
      <Panel
        options={{
          title: t("project.add.form.accelerator.section.title").toUpperCase(),
          subtitle: t("project.add.form.accelerator.section.subtitle"),
          icon: getIcon("accelerator.icon"),
          content: categories ? (
            <AcceleratorPalette
              path="accelerators"
              categories={categories}
              accelerators={accelerators}
              formikProps={formikProps.values}
              selectedAccelerators={selectedAccelerators}
            />
          ) : (
            <Message align="center">
              <Message.Header>
                {t("project.add.form.accelerator.section.empty.title")}
              </Message.Header>
              <Message.Content>
                <p>{t("project.add.form.accelerator.section.empty.message")}</p>
              </Message.Content>
            </Message>
          )
        }}
      />
    );
  };

  /**
   * This method renders the code repository configuration panel.
   */
  renderCodeRepositoryConfigurationPanel = formikProps => {
    const { t, repositories } = this.props;
    // Code repository configuration panel.
    return (
      <Panel
        options={{
          title: t("project.add.form.repository.section.title").toUpperCase(),
          subtitle: t("project.add.form.repository.section.subtitle"),
          icon: getIcon("repository.icon"),
          content: (
            <Container fluid>
              <Form.Group widths="equal">
                {/* Dropdown for code repository names */}
                <FikField
                  id="codeRepositoryConfigurationInstance.codeRepositoryId"
                  name="codeRepositoryConfigurationInstance.codeRepositoryId"
                  label={t("project.field.repository.name")}
                  placeholder={t(
                    "project.add.form.repository.section.field.name.placeholder"
                  )}
                  options={_.map(repositories, repository => {
                    return { text: repository.name, value: repository.id };
                  })}
                  component={FikDropdown}
                />

                {/* Field for Repository account name. */}
                <FikField
                  id="codeRepositoryConfigurationInstance.accountName"
                  name="codeRepositoryConfigurationInstance.accountName"
                  label={t("project.field.repository.accountName")}
                  placeholder={t(
                    "project.add.form.repository.section.field.accountName.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Form.Group>

              <Form.Group widths="equal">
                {/* Field for Repository username. */}
                <FikField
                  id="codeRepositoryConfigurationInstance.userName"
                  name="codeRepositoryConfigurationInstance.userName"
                  label={t("project.field.repository.userName")}
                  placeholder={t(
                    "project.add.form.repository.section.field.userName.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />

                {/* Field for Repository PAT. */}
                <FikField
                  type="password"
                  id="codeRepositoryConfigurationInstance.personalAccessToken"
                  name="codeRepositoryConfigurationInstance.personalAccessToken"
                  label={t("project.field.repository.personalAccessToken")}
                  placeholder={t(
                    "project.add.form.repository.section.field.personalAccessToken.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Form.Group>

              <Form.Group widths="equal">
                {/* Field for Branch Name. */}
                <FikField
                  id="codeRepositoryConfigurationInstance.branchName"
                  name="codeRepositoryConfigurationInstance.branchName"
                  label={t("project.field.repository.branchName")}
                  placeholder={t(
                    "project.add.form.repository.section.field.branchName.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Form.Group>

              <br />
              {this.drawDivider(
                getIcon("metadata.icon"),
                t("project.field.repository.metadata").toUpperCase()
              )}
              <br />
              <MetadataForm
                displayHeader={false}
                resourceName="codeRepositoryInstance"
                path="codeRepositoryConfigurationInstance.metadata"
                metadata={formikProps.values}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the continuous integration configuration panel.
   */
  renderContinuousIntegrationConfigurationPanel = formikProps => {
    const { t, devopsIntegrationConfigurations } = this.props;
    // Continuous Integration configuration panel.
    return (
      <Panel
        options={{
          title: t(
            "project.add.form.devops.integration.section.title"
          ).toUpperCase(),
          subtitle: t("project.add.form.devops.integration.section.subtitle"),
          icon: getIcon("devops.integration.icon"),
          content: (
            <Container fluid>
              <Form.Group widths="equal">
                {/* Dropdown for supported continuous integration (CI) names */}
                <FikField
                  id="continuousIntegrationConfigurationInstance.configurationId"
                  name="continuousIntegrationConfigurationInstance.configurationId"
                  label={t("project.field.devops.integration.name")}
                  placeholder={t(
                    "project.add.form.devops.integration.section.field.name.placeholder"
                  )}
                  options={_.map(
                    devopsIntegrationConfigurations,
                    devopsIntegrationConfiguration => {
                      return {
                        text: devopsIntegrationConfiguration.name,
                        value: devopsIntegrationConfiguration.id
                      };
                    }
                  )}
                  component={FikDropdown}
                />

                {/* Field for CI system's account name. */}
                <FikField
                  id="continuousIntegrationConfigurationInstance.accountName"
                  name="continuousIntegrationConfigurationInstance.accountName"
                  label={t("project.field.devops.integration.accountName")}
                  placeholder={t(
                    "project.add.form.devops.integration.section.field.accountName.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Form.Group>

              <Form.Group widths="equal">
                {/* Field for CI system's username. */}
                <FikField
                  id="continuousIntegrationConfigurationInstance.userName"
                  name="continuousIntegrationConfigurationInstance.userName"
                  label={t("project.field.devops.integration.userName")}
                  placeholder={t(
                    "project.add.form.devops.integration.section.field.userName.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />

                {/* Field for CI system's PAT. */}
                <FikField
                  type="password"
                  id="continuousIntegrationConfigurationInstance.personalAccessToken"
                  name="continuousIntegrationConfigurationInstance.personalAccessToken"
                  label={t(
                    "project.field.devops.integration.personalAccessToken"
                  )}
                  placeholder={t(
                    "project.add.form.devops.integration.section.field.personalAccessToken.placeholder"
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Form.Group>

              <br />
              {this.drawDivider(
                getIcon("metadata.icon"),
                t("project.field.devops.integration.metadata").toUpperCase()
              )}
              <br />
              <MetadataForm
                displayHeader={false}
                resourceName="continuousIntegrationConfigurationInstance"
                path="continuousIntegrationConfigurationInstance.metadata"
                metadata={formikProps.values}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the continuous delivery configuration panel.
   */
  renderContinuousDeliveryConfigurationPanel = formikProps => {
    const { t, devopsDeliveryConfigurations } = this.props;
    // Continuous Delivery configuration panel.
    return (
      <Panel
        options={{
          title: t(
            "project.add.form.devops.delivery.section.title"
          ).toUpperCase(),
          subtitle: t("project.add.form.devops.delivery.section.subtitle"),
          icon: getIcon("devops.delivery.icon"),
          content: (
            <Container fluid>
              {/* Dropdown for supported continuous delivery (CD) names */}
              <FikField
                id="continuousDeliveryConfigurationInstance.configurationId"
                name="continuousDeliveryConfigurationInstance.configurationId"
                label={t("project.field.devops.delivery.name")}
                placeholder={t(
                  "project.add.form.devops.delivery.section.field.name.placeholder"
                )}
                options={_.map(
                  devopsDeliveryConfigurations,
                  devopsDeliveryConfiguration => {
                    return {
                      text: devopsDeliveryConfiguration.name,
                      value: devopsDeliveryConfiguration.id
                    };
                  }
                )}
                clearable
                component={FikDropdown}
              />

              <br />
              {this.drawDivider(
                getIcon("metadata.icon"),
                t("project.field.devops.delivery.metadata").toUpperCase()
              )}
              <br />
              <MetadataForm
                displayHeader={false}
                resourceName="continuousDeliveryConfigurationInstance"
                path="continuousDeliveryConfigurationInstance.metadata"
                metadata={formikProps.values}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the button panel.
   */
  renderButtonPanel = ({ t, mode }, formikProps) => {
    return (
      <div>
        {/* Add button */}
        <FikButton
          type="submit"
          icon={getIcon("project.add.icon")}
          label={
            mode === "add"
              ? t("project.actions.add")
              : t("project.actions.update")
          }
          loading={formikProps.isSubmitting}
          disabled={formikProps.isSubmitting}
          basic
          primary
        />

        {/* Clear button */}
        <FikButton
          type="reset"
          icon={getIcon("clear.icon")}
          label={t("clearForm")}
          onClick={formikProps.handleReset}
          color="purple"
          basic
        />

        {/* Close button */}
        <FikButton
          icon={getIcon("close.icon")}
          label={t("close")}
          as={NavLink}
          exact
          to="/projects"
          floated="right"
          basic
          secondary
        />
      </div>
    );
  };

  /**
   * This method draws a divider between various details in the card.
   */
  drawDivider = (icon, title, headerSize = "h5", headerColor = "grey") => {
    return (
      <Divider horizontal className="left aligned">
        <Header as={headerSize} color={headerColor}>
          <Icon className={icon} />
          {title}
        </Header>
      </Divider>
    );
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = formValues => {
    if (this.props.onSubmit) {
      const clonedFormValues = _.cloneDeep(formValues);
      const {
        codeRepositoryConfigurationInstance,
        continuousIntegrationConfigurationInstance,
        continuousDeliveryConfigurationInstance
      } = clonedFormValues;

      // Change from arrays to key-value pairs for repository metadata.
      const repoMetadata =
        codeRepositoryConfigurationInstance &&
        codeRepositoryConfigurationInstance.metadata;
      if (repoMetadata && repoMetadata.length >= 0) {
        clonedFormValues.codeRepositoryConfigurationInstance.metadata = _.mapValues(
          _.keyBy(repoMetadata, "key"),
          "value"
        );
      }

      // Change from arrays to key-value pairs for continuous integration metadata.
      const ciMetadata =
        continuousIntegrationConfigurationInstance &&
        continuousIntegrationConfigurationInstance.metadata;
      if (ciMetadata && ciMetadata.length >= 0) {
        clonedFormValues.continuousIntegrationConfigurationInstance.metadata = _.mapValues(
          _.keyBy(ciMetadata, "key"),
          "value"
        );
      }
      // Change from arrays to key-value pairs for continuous delivery metadata.
      const cdMetadata =
        continuousDeliveryConfigurationInstance &&
        continuousDeliveryConfigurationInstance.metadata;
      if (cdMetadata && cdMetadata.length >= 0) {
        clonedFormValues.continuousDeliveryConfigurationInstance.metadata = _.mapValues(
          _.keyBy(cdMetadata, "key"),
          "value"
        );
      }
      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

export default withTranslation()(AbstractProjectForm);
