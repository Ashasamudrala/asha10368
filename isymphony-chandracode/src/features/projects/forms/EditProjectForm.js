/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractProjectForm from "./AbstractProjectForm";
import { getAllCategoriesAction } from "../../../actions/acceleratorActions";
import { getAllDevOpsDeliveryConfigurationsAction } from "../../../actions/devopsDeliveryConfigurationActions";
import { getAllDevOpsIntegrationConfigurationsAction } from "../../../actions/devopsIntegrationConfigurationActions";
import { getAllPlatformsAction } from "../../../actions/coreActions";
import { getAllRepositoriesAction } from "../../../actions/repositoryActions";
import {
  getProjectAction,
  updateProjectAction
} from "../../../actions/projectActions";
import { renderLoader } from "../../../utils";

/**
 * A form component that is used to capture the project details during the project creation.
 *
 * @author Chandra Veerapaneni
 */
class EditProjectForm extends React.Component {
  // Component state
  state = {
    categoriesDataLoaded: false,
    cdDataLoaded: false,
    ciDataLoaded: false,
    platformsDataLoaded: false,
    projectDataLoaded: false,
    repositoriesDataLoaded: false
  };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    // Load the platforms
    this.props.getAllPlatformsAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ platformsDataLoaded: true })
    });

    // Load the categories
    this.props.getAllCategoriesAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ categoriesDataLoaded: true })
    });

    // Load the specific project
    this.props.getProjectAction(this.props.match.params.id, {
      onSuccess: () => this.setState({ projectDataLoaded: true })
    });

    // Load all the repositories in the system.
    this.props.getAllRepositoriesAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ repositoriesDataLoaded: true })
    });

    // Load all the continuous integration configurations in the system.
    this.props.getAllDevOpsIntegrationConfigurationsAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ ciDataLoaded: true })
    });

    // Load all the continuous delivery configurations in the system.
    this.props.getAllDevOpsDeliveryConfigurationsAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ cdDataLoaded: true })
    });
  }

  /**
   * This method gets called to render the new project form on the screen.
   */
  render() {
    const {
      categoriesDataLoaded,
      platformsDataLoaded,
      projectDataLoaded
    } = this.state;

    const { t } = this.props;
    // If either the platforms or categories or projects have not loaded, return.
    if (!platformsDataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    if (!categoriesDataLoaded) {
      return renderLoader(t("category.loading.message"));
    }

    if (!projectDataLoaded) {
      return renderLoader(t("project.loading.single.message"));
    }

    const {
      project,
      platforms,
      frameworks,
      categories,
      accelerators,
      repositories,
      devopsIntegrationConfigurations,
      devopsDeliveryConfigurations
    } = this.props;

    const clonedProject = _.cloneDeep(project);
    clonedProject.codeRepositoryConfigurationInstance = clonedProject.codeRepositoryConfigurationInstance || {
      codeRepositoryId: "",
      accountName: "",
      userName: "",
      personalAccessToken: "",
      branchName: "",
      metadata: {}
    };
    clonedProject.continuousIntegrationConfigurationInstance = clonedProject.continuousIntegrationConfigurationInstance || {
      configurationId: "",
      accountName: "",
      userName: "",
      personalAccessToken: "",
      metadata: {}
    };
    clonedProject.continuousDeliveryConfigurationInstance = clonedProject.continuousDeliveryConfigurationInstance || {
      configurationId: "",
      metadata: {}
    };

    const {
      codeRepositoryConfigurationInstance: repo,
      continuousIntegrationConfigurationInstance: ci,
      continuousDeliveryConfigurationInstance: cd
    } = clonedProject;

    // Transform the repository metadata
    if (repo.metadata) {
      repo.metadata = _.map(Object.keys(repo.metadata), key => {
        return {
          key: key,
          value: repo.metadata[key]
        };
      });
    }

    // Transform the CI metadata
    if (ci.metadata) {
      ci.metadata = _.map(Object.keys(ci.metadata), key => {
        return {
          key: key,
          value: ci.metadata[key]
        };
      });
    }

    // Transform the CD metadata
    if (cd.metadata) {
      cd.metadata = _.map(Object.keys(cd.metadata), key => {
        return {
          key: key,
          value: cd.metadata[key]
        };
      });
    }

    return (
      <Container>
        <AbstractProjectForm
          title={t("project.update.form.title")}
          subTitle={t("project.update.form.subtitle")}
          platforms={platforms}
          frameworks={frameworks}
          categories={categories}
          accelerators={accelerators}
          repositories={repositories}
          devopsIntegrationConfigurations={devopsIntegrationConfigurations}
          devopsDeliveryConfigurations={devopsDeliveryConfigurations}
          mode="update"
          onSubmit={this.onSubmit}
          selectedAccelerators={_.map(
            this.props.project.accelerators,
            "acceleratorId"
          )}
          initialValues={clonedProject}
        />
      </Container>
    );
  }

  /**
   * This method gets called whenever the form gets submitted by the user.
   */
  onSubmit = formValues => {
    this.props.updateProjectAction(this.props.project.id, formValues, {
      redirectUrl: "/projects"
    });
  };
}

/**
 * This method maps the state in the redux as component properties.
 *
 * @param {*} state Redux state.
 * @param {*} ownProps Component props.
 */
const mapStateToProps = (state, ownProps) => {
  return {
    platforms: state.application.platforms.content,
    frameworks: state.application.frameworks.content,
    categories: state.categories.content,
    accelerators: state.categories.accelerators.content,
    project: state.projects.content
      ? state.projects.content[ownProps.match.params.id]
      : null,
    repositories: state.repositories.content,
    devopsIntegrationConfigurations:
      state.devopsIntegrationConfigurations.content,
    devopsDeliveryConfigurations: state.devopsDeliveryConfigurations.content
  };
};

export default connect(mapStateToProps, {
  getAllCategoriesAction,
  getAllDevOpsDeliveryConfigurationsAction,
  getAllDevOpsIntegrationConfigurationsAction,
  getAllPlatformsAction,
  getAllRepositoriesAction,
  getProjectAction,
  updateProjectAction
})(withTranslation()(EditProjectForm));
