/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractProjectForm from "./AbstractProjectForm";
import { createProjectAction } from "../../../actions/projectActions";
import { getAllCategoriesAction } from "../../../actions/acceleratorActions";
import { getAllDevOpsDeliveryConfigurationsAction } from "../../../actions/devopsDeliveryConfigurationActions";
import { getAllDevOpsIntegrationConfigurationsAction } from "../../../actions/devopsIntegrationConfigurationActions";
import { getAllPlatformsAction } from "../../../actions/coreActions";
import { getAllRepositoriesAction } from "../../../actions/repositoryActions";
import { renderLoader } from "../../../utils";

/**
 * A form component that is used to capture the project details during the project creation.
 *
 * @author Chandra Veerapaneni
 */
class NewProjectForm extends React.Component {
  // Component state
  state = {
    categoriesDataLoaded: false,
    cdDataLoaded: false,
    ciDataLoaded: false,
    platformsDataLoaded: false,
    repositoriesDataLoaded: false
  };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    // Load all the platforms in the system.
    this.props.getAllPlatformsAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ platformsDataLoaded: true })
    });

    // Load all the categories in the system.
    this.props.getAllCategoriesAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ categoriesDataLoaded: true })
    });

    // Load all the repositories in the system.
    this.props.getAllRepositoriesAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ repositoriesDataLoaded: true })
    });

    // Load all the continuous integration configurations in the system.
    this.props.getAllDevOpsIntegrationConfigurationsAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ ciDataLoaded: true })
    });

    // Load all the continuous delivery configurations in the system.
    this.props.getAllDevOpsDeliveryConfigurationsAction({
      params: { pageNumber: 0, pageSize: 100 },
      onSuccess: () => this.setState({ cdDataLoaded: true })
    });
  }

  /**
   * This method gets called to render the new project form on the screen.
   */
  render() {
    const { t } = this.props;
    const {
      categoriesDataLoaded,
      platformsDataLoaded,
      repositoriesDataLoaded,
      ciDataLoaded,
      cdDataLoaded
    } = this.state;

    // If any of the pre-requisites (platforms, categories, repositories, ci, cd) are not loaded, return.
    if (!platformsDataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    if (!categoriesDataLoaded) {
      return renderLoader(t("category.loading.message"));
    }

    if (!repositoriesDataLoaded) {
      return renderLoader(t("repository.loading.message"));
    }

    if (!ciDataLoaded) {
      return renderLoader(t("devops.ci.loading.message"));
    }

    if (!cdDataLoaded) {
      return renderLoader(t("devops.cd.loading.message"));
    }

    const {
      platforms,
      frameworks,
      categories,
      accelerators,
      repositories,
      devopsIntegrationConfigurations,
      devopsDeliveryConfigurations
    } = this.props;

    return (
      <Container>
        <AbstractProjectForm
          platforms={platforms}
          frameworks={frameworks}
          categories={categories}
          accelerators={accelerators}
          repositories={repositories}
          devopsIntegrationConfigurations={devopsIntegrationConfigurations}
          devopsDeliveryConfigurations={devopsDeliveryConfigurations}
          onSubmit={this.onSubmit}
        />
      </Container>
    );
  }

  /**
   * This method gets called whenever the form gets submitted by the user.
   */
  onSubmit = formValues => {
    this.props.createProjectAction(formValues, {
      redirectUrl: "/projects"
    });
  };
}

/**
 * This method maps the state to component properties.
 *
 * @param {*} state Current state as retrieved from the redux store.
 */
const mapStateToProps = state => {
  return {
    platforms: state.application.platforms.content,
    frameworks: state.application.frameworks.content,
    categories: state.categories.content,
    accelerators: state.categories.accelerators.content,
    repositories: state.repositories.content,
    devopsIntegrationConfigurations:
      state.devopsIntegrationConfigurations.content,
    devopsDeliveryConfigurations: state.devopsDeliveryConfigurations.content
  };
};

export default connect(mapStateToProps, {
  createProjectAction,
  getAllCategoriesAction,
  getAllDevOpsDeliveryConfigurationsAction,
  getAllDevOpsIntegrationConfigurationsAction,
  getAllPlatformsAction,
  getAllRepositoriesAction
})(withTranslation()(NewProjectForm));
