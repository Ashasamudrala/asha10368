/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form } from "semantic-ui-react";
import { Field as FikField } from "formik";
import { withTranslation } from "react-i18next";

import { FikDropdown } from "../../components/common/formik-wrappers";

/**
 * Component that renders the fields allowing the users to choose a combination of platform and framework versions.
 *
 * @author Chandra Veerapaneni
 */
class PlatformFrameworkPanel extends React.Component {
  // Local component state.
  state = {
    platformId: "",
    frameworkId: ""
  };

  /**
   * This method renders the component on the UI.
   */
  render() {
    const { t, platforms, frameworks } = this.props;
    if (platforms && frameworks) {
      return (
        <Container fluid>
          {/* Render the panel containing the dropdowns for Platform and Platform Version */}
          {this.renderPlatformPanel(this.props)}

          {/* Render the panel containing the dropdowns for Framework and Framework Version */}
          {this.renderFrameworkPanel(this.props)}
        </Container>
      );
    }
    // Required data not provided.
    return (
      <Container fluid>
        {t(
          "project.add.form.details.section.platformAndFramework.section.invalid.message"
        )}
      </Container>
    );
  }

  /**
   * This method renders the dropdowns for Platform Name and Platform Version.
   */
  renderPlatformPanel = ({ t, path = "platform", platforms }) => {
    return (
      <Form.Group widths="equal">
        {/* Dropdown for platform names */}
        <FikField
          id={`${path}.id`}
          label={t("project.field.platform.name")}
          name={`${path}.id`}
          placeholder={t(
            "project.add.form.details.section.platformAndFramework.section.field.platform.name.placeholder"
          )}
          options={_.map(platforms, platform => {
            return { text: platform.name, value: platform.id };
          })}
          config={{
            onSelectionChange: item => {
              this.setState({ platformId: item.value });
            }
          }}
          component={FikDropdown}
          clearable
          required
        />

        {/* Dropdown for platform version */}
        <FikField
          id={`${path}.version`}
          label={t("project.field.platform.version")}
          name={`${path}.version`}
          placeholder={t(
            "project.add.form.details.section.platformAndFramework.section.field.platform.version.placeholder"
          )}
          options={this.getVersionsForSelectedPlatform()}
          component={FikDropdown}
          clearable
          required
        />
      </Form.Group>
    );
  };

  /**
   * This method renders the dropdowns for Framework Name and Framework Version.
   */
  renderFrameworkPanel = ({ t, path = "platform", frameworks }) => {
    return (
      <Form.Group widths="equal">
        {/* Dropdown for framework names */}
        <FikField
          id={`${path}.framework.id`}
          label={t("project.field.platform.framework.name")}
          name={`${path}.framework.id`}
          placeholder={t(
            "project.add.form.details.section.platformAndFramework.section.field.platform.framework.name.placeholder"
          )}
          options={this.getFrameworksForSelectedPlatform()}
          config={{
            onSelectionChange: item => {
              this.setState({ frameworkId: item.value });
            }
          }}
          component={FikDropdown}
          clearable
          required
        />

        {/* Dropdown for framework version */}
        <FikField
          id={`${path}.framework.version`}
          label={t("project.field.platform.framework.version")}
          name={`${path}.framework.version`}
          placeholder={t(
            "project.add.form.details.section.platformAndFramework.section.field.platform.framework.version.placeholder"
          )}
          options={this.getVersionsForSelectedFramework()}
          component={FikDropdown}
          clearable
          required
        />
      </Form.Group>
    );
  };

  /**
   * This method returns the platform object for the provided platform identifier.
   * It is expected that "platforms" property exists in the component properties (i.e. this.props).
   */
  getSelectedPlatformDetails = platformId => {
    if (platformId) {
      const { platforms } = this.props;
      return platforms[platformId] || null;
    }
    return null;
  };

  /**
   * This method returns the framework object for the provided framework identifier.
   * It is expected that "frameworks" property exists in the component properties (i.e. this.props).
   */
  getSelectedFrameworkDetails = frameworkId => {
    if (frameworkId) {
      const { frameworks } = this.props;
      return frameworks[frameworkId] || null;
    }
    return null;
  };

  /**
   * For the selected platform (which is available in the component state), this method attempts to
   * return the versions for the respective platform.
   *
   * If there are no versions or unable to find the respective platform, this method returns an empty
   * array.
   */
  getVersionsForSelectedPlatform = () => {
    const platform = this.getSelectedPlatformDetails(this.state.platformId);
    if (platform && platform.versions) {
      return _.map(platform.versions, version => {
        return { text: version, value: version };
      });
    }
    return [];
  };

  /**
   * For the selected platform (which is available in the component state), this method attempts to
   * return the frameworks for the respective platform.
   *
   * If there are no frameworks or unable to find the respective platform, this method returns an empty
   * array.
   */
  getFrameworksForSelectedPlatform = () => {
    const platform = this.getSelectedPlatformDetails(this.state.platformId);
    if (platform && platform.frameworks) {
      return _.map(Object.values(platform.frameworks), framework => {
        return { text: framework.name, value: framework.id };
      });
    }
    return [];
  };

  /**
   * For the selected framework (which is available in the component state), this method attempts to
   * return the versions for the respective framework.
   *
   * If there are no versions or unable to find the respective framework, this method returns an empty
   * array.
   */
  getVersionsForSelectedFramework = () => {
    const framework = this.getSelectedFrameworkDetails(this.state.frameworkId);
    if (framework && framework.versions) {
      return _.map(framework.versions, version => {
        return { text: version.versionName, value: version.versionName };
      });
    }
    return [];
  };
}

export default withTranslation()(PlatformFrameworkPanel);
