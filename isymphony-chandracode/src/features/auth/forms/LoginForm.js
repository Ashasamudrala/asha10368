/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from "react";

/**
 * A component that displays the login form onto the screen.
 *
 * @author Chandra Veerapaneni
 */
class LoginForm extends React.Component {
  /**
   * Main method that gets called to render the Login form on the screen.
   */
  render() {
    return <div>Login Form!</div>;
  }
}

export default LoginForm;
