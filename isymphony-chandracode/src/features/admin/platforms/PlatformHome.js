/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { NavLink } from "react-router-dom";
import { Container, Divider, Button, Icon } from "semantic-ui-react";
import { withTranslation } from "react-i18next";

import PlatformTable from "./common/PlatformTable";
import getIcon from "../../../config/icons";
import { renderPageTitle } from "../../../utils";

/**
 * This component is the Home page for the platforms section, where the administrative users
 * will be able to view / edit / delete the platforms available in the system.
 *
 * @author Chandra Veerapaneni
 */
class PlatformHome extends React.Component {
  /**
   * This method is responsible to render the platform home page details on the UI.
   */
  render() {
    const { t } = this.props;
    const platformIcon = getIcon("platform.icon");

    return (
      <Container fluid>
        {// Renders the page title.
        renderPageTitle(
          t("platform.home.title"),
          t("platform.home.subtitle"),
          platformIcon,
          <Container align="right">
            <Button
              as={NavLink}
              exact
              to={"/platforms/new"}
              floated="right"
              color="blue"
              basic
            >
              <Icon name={platformIcon} />
              {t("platform.actions.add")}
            </Button>
          </Container>
        )}

        <Divider />

        {/* Table that displays the platforms in the system. */}
        <PlatformTable />
      </Container>
    );
  }
}

export default withTranslation()(PlatformHome);
