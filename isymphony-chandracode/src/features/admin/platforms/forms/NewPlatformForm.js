/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";

import AbstractPlatformForm from "./AbstractPlatformForm";
import { createPlatformAction } from "../../../../actions/coreActions";

/**
 * This component is used for creation of a new platform.
 *
 * @author Chandra Veerapaneni
 */
class NewPlatformForm extends React.Component {
  /**
   * This method is responsible to render the UI on the screen.
   */
  render() {
    return (
      <Container>
        <AbstractPlatformForm mode="add" onSubmit={this.onFormSubmit} />
      </Container>
    );
  }

  /**
   * This method gets called upon submission of the form. The parameters passed to this method
   * is the form data.
   */
  onFormSubmit = formValues => {
    this.props.createPlatformAction(formValues, {
      redirectUrl: "/platforms"
    });
  };
}

export default connect(null, {
  createPlatformAction
})(NewPlatformForm);
