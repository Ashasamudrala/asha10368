/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractPlatformForm from "./AbstractPlatformForm";
import {
  getPlatformAction,
  updatePlatformAction
} from "../../../../actions/coreActions";
import { renderLoader } from "../../../../utils";

/**
 * This component is used for updating an existing platform.
 *
 * @author Chandra Veerapaneni
 */
class EditPlatformForm extends React.Component {
  // local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getPlatformAction(this.props.match.params.id, {
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  }

  /**
   * This method is responsible to render the UI on the screen.
   */
  render() {
    const { t } = this.props;
    if (!this.state.dataLoaded) {
      return renderLoader(t("platform.loading.single.message"));
    }

    const { name, description, versions, metadata } = this.props.platform;
    return (
      <Container>
        <AbstractPlatformForm
          mode="update"
          title={t("platform.update.form.title")}
          subtitle={t("platform.update.form.subtitle")}
          onSubmit={this.onFormSubmit}
          initialValues={{
            name: name,
            description: description,
            versions: versions,
            metadata: _.map(Object.keys(metadata), key => {
              return { key: key, value: metadata[key] };
            })
          }}
        />
      </Container>
    );
  }

  /**
   * This method gets called upon submission of the form. The parameters passed to this method
   * is the form data.
   */
  onFormSubmit = formValues => {
    this.props.updatePlatformAction(this.props.platform.id, formValues, {
      redirectUrl: "/platforms"
    });
  };
}

/**
 * This method extracts the required information from the redux store and makes them available as
 * component properties.
 *
 * @param {*} state Redux store
 * @param {*} ownProps Component's properties
 */
const mapStateToProps = (state, ownProps) => {
  return {
    platform: state.application.platforms.content[ownProps.match.params.id]
  };
};

export default connect(mapStateToProps, {
  getPlatformAction,
  updatePlatformAction
})(withTranslation()(EditPlatformForm));
