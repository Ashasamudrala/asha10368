/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import {
  Container,
  Pagination,
  Table,
  Button,
  List,
  Label
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import getIcon from "../../../../config/icons";
import { FikLabeledButton } from "../../../../components/common/formik-wrappers";
import { configurePopup, renderLoader } from "../../../../utils";
import { getAllPlatformsAction } from "../../../../actions/coreActions";

/**
 * This component is responsible to display the categories in a tabular fashion.
 *
 * @author Chandra Veerapaneni
 */
class PlatformTable extends React.Component {
  // Local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is loaded into the DOM.
   */
  componentDidMount() {
    this.loadPlatforms(0, 20);
  }

  /**
   * This method is responsible to render the table on the UI.
   */
  render() {
    return <Container fluid>{this.renderTable()}</Container>;
  }

  /**
   * This method renders the table.
   */
  renderTable = () => {
    const { dataLoaded } = this.state;
    const { t, platforms } = this.props;

    if (!dataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    return (
      <Table celled padded>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={3}>
              {t("platform.field.name").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={4}>
              {t("platform.field.description").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={4}>
              {t("platform.field.versions").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={3}>
              {t("platform.field.metadata").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={2}>
              {t("platform.field.actions").toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {platforms.map(platform => {
            return (
              <Table.Row key={platform.id}>
                {/* Platform Name */}
                {configurePopup(
                  <Table.Cell>
                    <strong>{platform.name}</strong>
                  </Table.Cell>,
                  t("platform.field.name"),
                  platform.name
                )}

                {/* Platform Description */}
                {configurePopup(
                  <Table.Cell>{platform.description}</Table.Cell>,
                  t("platform.field.description"),
                  platform.description
                )}

                {/* Version */}
                <Table.Cell>
                  <List horizontal>
                    {platform.versions.map(version => {
                      return (
                        <List.Item key={version}>
                          <List.Content>
                            <Label color="blue" basic>
                              {version}
                            </Label>
                          </List.Content>
                        </List.Item>
                      );
                    })}
                  </List>
                </Table.Cell>

                {/* Metadata */}
                <Table.Cell>
                  <List horizontal>
                    {Object.keys(platform.metadata).map(key => {
                      return (
                        <List.Item key={key}>
                          <List.Content>
                            <FikLabeledButton
                              size="tiny"
                              key={key}
                              content={key}
                              label={{
                                basic: true,
                                content: platform.metadata[key]
                              }}
                            />
                          </List.Content>
                        </List.Item>
                      );
                    })}
                  </List>
                </Table.Cell>

                {/* Actions */}
                <Table.Cell textAlign="center">
                  <Button
                    as={NavLink}
                    icon={getIcon("edit.icon")}
                    size="tiny"
                    exact
                    to={`/platforms/edit/${platform.id}`}
                    color="blue"
                    basic
                  />
                  &nbsp;
                  <Button
                    as={NavLink}
                    icon={getIcon("framework.icon")}
                    size="tiny"
                    exact
                    to={`/platforms/${platform.id}/frameworks`}
                    color="violet"
                    basic
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="16">
              {this.renderPaginationControl()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  };

  /**
   * This method renders the pagination control in the UI.
   */
  renderPaginationControl = () => {
    const { pagination } = this.props;
    if (pagination && pagination.totalPages > 1) {
      return (
        <Pagination
          size="tiny"
          boundaryRange={0}
          defaultActivePage={1}
          ellipsisItem={null}
          firstItem={null}
          lastItem={null}
          siblingRange={1}
          totalPages={pagination.totalPages}
          floated="right"
          onPageChange={(e, data) =>
            this.loadPlatforms(data.activePage, pagination.pageSize)
          }
        />
      );
    }
  };

  /**
   * This method attempts to load the platforms based on the provided pagination settings.
   */
  loadPlatforms = (pageNumber, pageSize) => {
    this.props.getAllPlatformsAction({
      params: {
        pageNumber: pageNumber - 1,
        pageSize: pageSize
      },
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  };
}

/**
 * This method extracts the required information from the redux store and makes it available
 * as the component properties.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {
    platforms: Object.values(state.application.platforms.content),
    pagination: state.application.platforms.pagination
  };
};

export default connect(mapStateToProps, {
  getAllPlatformsAction
})(withTranslation()(PlatformTable));
