/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Divider } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AcceleratorCategory from "./common/AcceleratorCategory";
import getIcon from "../../../config/icons";
import { getAllCategoriesAction } from "../../../actions/acceleratorActions";
import { getAllPlatformsAction } from "../../../actions/coreActions";
import { renderPageTitle, renderLoader } from "../../../utils";

/**
 * This component is the Home page for the accelerators section, where the administrative users
 * will be able to view / edit / delete the accelerators available in the system.
 *
 * @author Chandra Veerapaneni
 */
class AcceleratorsHome extends React.Component {
  // Locale state
  state = { platformsDataLoaded: false, categoriesDataLoaded: false };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getAllPlatformsAction({
      onSuccess: () => {
        this.setState({ platformsDataLoaded: true });
      }
    });

    this.props.getAllCategoriesAction({
      params: {
        pageNumber: 0,
        pageSize: 100
      },
      onSuccess: () => {
        this.setState({ categoriesDataLoaded: true });
      }
    });
  }

  /**
   * This method is responsible to render the accelerator home page details on the UI.
   */
  render() {
    const { t } = this.props;

    // If the categories and platforms are not loaded, return.
    if (!this.state.categoriesDataLoaded) {
      return renderLoader(t("accelerator.loading.message"));
    }
    if (!this.state.platformsDataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    const { categories, platforms, frameworks } = this.props;
    return (
      <Container fluid>
        {// Renders the page title.
        renderPageTitle(
          t("accelerator.home.title"),
          t("accelerator.home.subtitle"),
          getIcon("accelerator.icon")
        )}
        <Divider />

        {categories && categories.length > 0 ? (
          categories.map(category => {
            return (
              <AcceleratorCategory
                key={category.id}
                category={category}
                platforms={platforms}
                frameworks={frameworks}
              />
            );
          })
        ) : (
          <Container fluid>{t("category.empty.message")}</Container>
        )}
      </Container>
    );
  }
}

/**
 * This method extracts the required information from the redux store and makes it available as
 * the component properties.
 *
 * @param {*} state As-is state of the redux store.
 */
const mapStateToProps = state => {
  return {
    categories: Object.values(state.categories.content),
    accelerators: Object.values(state.categories.accelerators.content),
    platforms: state.application.platforms.content,
    frameworks: state.application.frameworks.content
  };
};

export default connect(mapStateToProps, {
  getAllCategoriesAction,
  getAllPlatformsAction
})(withTranslation()(AcceleratorsHome));
