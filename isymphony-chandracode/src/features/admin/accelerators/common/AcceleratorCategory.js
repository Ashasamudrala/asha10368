/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import "./AcceleratorCategory.css";

import _ from "lodash";

import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import getIcon from "../../../../config/icons";
import {
  Button,
  Icon,
  Container,
  Segment,
  Header,
  Label,
  Card,
  Divider,
  Message,
  List,
  Image
} from "semantic-ui-react";
import { configurePopup } from "../../../../utils";
import { getAllCategoriesAction } from "../../../../actions/acceleratorActions";
import { renderNoDataMessage } from "../../../../utils";

/**
 * This component renders the category and the accelerators contained within the category.
 * The category and the accelerators contained within the category are available on the
 * props object.
 *
 * @author Chandra Veerapaneni
 */
class AcceleratorCategory extends React.Component {
  // Default image.
  static DEFAULT_IMAGE = "/assets/images/default.png";

  /**
   * This method renders the UI on the screen.
   */
  render() {
    const { t, category } = this.props;
    if (!category) {
      return (
        <Container textAlign="center">
          {renderNoDataMessage(
            t("accelerator.category.empty.title"),
            t("accelerator.category.empty.message")
          )}
        </Container>
      );
    }

    // Render the category header and its accelerators.
    return this.renderCategoryHeader(this.props);
  }

  /**
   * This method renders the category section and within it, all the accelerators belonging to it.
   */
  renderCategoryHeader = ({ t, category, platforms, frameworks }) => {
    const { name, description, metadata } = category;
    const icon =
      metadata && metadata.icon ? metadata.icon : getIcon("accelerator.icon");

    return (
      <Container fluid>
        {/* Message that indicates the category and accelerators attached to the category. */}
        <Message warning icon attached>
          <Icon className={icon} />
          <Container align="left">
            <Message.Header>
              {t("accelerator.table.title").replace("{category.name}", name)}
            </Message.Header>
            <Message.Content>{description}</Message.Content>
          </Container>
          <Container align="right">
            <Button
              as={NavLink}
              exact
              to={`/categories/${category.id}/accelerators/new`}
              floated="right"
              color="blue"
              basic
            >
              <Icon className={icon} />
              {t("accelerator.actions.add")}
            </Button>
          </Container>
        </Message>
        <Segment attached>
          {this.renderAcceleratorsInCategory(
            t,
            category,
            platforms,
            frameworks
          )}
        </Segment>
        <br />
        <br />
      </Container>
    );
  };

  /**
   * This method attempts to render the accelerators in the provided category.
   */
  renderAcceleratorsInCategory = (t, category, platforms, frameworks) => {
    const tagColor = "grey";
    const { accelerators } = category;

    if (accelerators) {
      return (
        <Card.Group itemsPerRow={3}>
          {Object.values(accelerators).map(accelerator => {
            const {
              id,
              name,
              platform,
              description,
              versions,
              metadata
            } = accelerator;
            const {
              icon = getIcon("accelerator.icon"),
              image = AcceleratorCategory.DEFAULT_IMAGE
            } = metadata || {};
            const platformObj = platforms[platform.id];
            const frameworkObj = frameworks[platform.framework.id];

            return (
              <Card key={id} fluid raised>
                <Image src={image} wrapped ui={false} />
                <Card.Content>
                  <Card.Header>
                    <Icon className={icon} />
                    &nbsp;
                    {name}
                  </Card.Header>
                  <Card.Meta>
                    <br />
                    <Label size="tiny" color={tagColor} basic>
                      {t("accelerator.field.platform")}
                      <Label.Detail>{platformObj.name}</Label.Detail>
                    </Label>
                    <Label size="tiny" color={tagColor} basic>
                      {t("accelerator.field.framework")}
                      <Label.Detail>{frameworkObj.name}</Label.Detail>
                    </Label>
                  </Card.Meta>
                </Card.Content>
                <Card.Content>
                  <Card.Description>
                    {description}
                    <Divider horizontal className="left aligned">
                      <Header as="h5">
                        <Icon
                          size="small"
                          className={getIcon("version.icon")}
                        />
                        {t("accelerator.field.versions")}
                      </Header>
                    </Divider>
                    <Label.Group size="tiny">
                      {versions && versions.length > 0 ? (
                        versions.map(version => {
                          return configurePopup(
                            <Label
                              key={version.versionName}
                              color={tagColor}
                              basic
                            >
                              {version.versionName}
                              <Label.Detail>
                                $&nbsp;
                                {this.getSavingsInDollars(version)}
                              </Label.Detail>
                            </Label>,
                            t("accelerator.version.features.title").replace(
                              "{accelerator.version}",
                              version.versionName
                            ),
                            this.getFeatures(version),
                            { size: "tiny" }
                          );
                        })
                      ) : (
                        <div></div>
                      )}
                    </Label.Group>
                  </Card.Description>
                </Card.Content>
                <Card.Content extra>
                  <Button
                    size="tiny"
                    as={NavLink}
                    exact
                    to={`/categories/${category.id}/accelerators/edit/${accelerator.id}`}
                    color="blue"
                    basic
                  >
                    <Icon className={getIcon("edit.icon")} />
                    {t("edit")}
                  </Button>
                </Card.Content>
              </Card>
            );
          })}
        </Card.Group>
      );
    }

    // No accelerators in the category.
    return (
      <div>
        <br />
        {renderNoDataMessage(
          t("accelerator.empty.title"),
          t("accelerator.empty.message")
        )}
      </div>
    );
  };

  /**
   * This method attempts to load the categories based on the provided pagination settings.
   */
  loadCategories = (pageNumber, pageSize) => {
    this.props.getAllCategoriesAction({
      params: {
        pageNumber: pageNumber - 1,
        pageSize: pageSize
      },
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  };

  /**
   * This method retrieves the details of the features supported by this version.
   */
  getFeatures = version => {
    const { t } = this.props;
    const { features } = version;

    if (features && features.length > 0) {
      return (
        <List relaxed>
          <List.Item>
            <List.Header>
              {t("accelerator.savings.message").replace(
                "{effort}",
                _.sum(_.map(features, "effortInPersonHours"))
              )}
              <Divider />
            </List.Header>
          </List.Item>
          {features.map(feature => {
            return (
              <List.Item key={Math.random()}>{feature.description}</List.Item>
            );
          })}
        </List>
      );
    } else {
    }
  };

  /**
   * For the provided version, this method calculates the cost savings in dollars.
   */
  getSavingsInDollars = version => {
    const { features } = version;
    if (features && features.length > 0) {
      return _.sum(_.map(version.features, "effortInPersonHours")) * 100.0;
    }
    return 0;
  };
}

export default connect(null, {
  getAllCategoriesAction
})(withTranslation()(AcceleratorCategory));
