/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import {
  Container,
  Message,
  Table,
  Button,
  Divider,
  Header,
  Icon,
  Segment
} from "semantic-ui-react";
import { Field as FikField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import FeatureForm from "../feature/FeatureForm";
import Panel from "../../../../../components/common/panel/Panel";
import PlatformAndFrameworkVersion from "./PlatformAndFrameworkVersion";
import PrerequisiteForm from "../prerequisite/PrerequisiteForm";
import getIcon from "../../../../../config/icons";
import {
  FikButton,
  FikInput
} from "../../../../../components/common/formik-wrappers";

/**
 * Component that is responsible to render a form that captures accelerator version details including their dependencies.
 *
 * @author Chandra Veerapaneni
 */
class AcceleratorVersionForm extends React.Component {
  // Empty row.
  static EMPTY_ROW = {
    versionName: "1.0.0-SNAPSHOT",
    supportedPlatformAndFrameworkVersions: [],
    features: [],
    prerequisites: []
  };

  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { versions, path = "versions" } = this.props;

    return (
      // Render the dynamic fields to capture the version information
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderVersionForm(_.get(versions, path), arrayHelpers);
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the version details pertaining to the resource.
   */
  renderVersionForm = (versions, arrayHelpers) => {
    const {
      t,
      title = t("accelerator.add.form.versions.section.title"),
      subtitle = t("accelerator.add.form.versions.section.subtitle"),
      icon = getIcon("version.icon"),
      displayHeader = true
    } = this.props;

    // Metadata for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {versions && versions.length > 0
                  ? this.renderAcceleratorVersionsContent(
                      versions,
                      arrayHelpers
                    )
                  : this.renderNoAcceleratorVersionsContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {versions && versions.length > 0
            ? this.renderAcceleratorVersionsContent(versions, arrayHelpers)
            : this.renderNoAcceleratorVersionsContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders the UI that displays the accelerator version details.
   */
  renderAcceleratorVersionsContent = (versions, arrayHelpers) => {
    const {
      t,
      path = "versions",
      category,
      platformId,
      frameworkId,
      platforms,
      frameworks,
      title = t("accelerator.field.versionDetails")
    } = this.props;

    const color = "teal";
    const length = versions.length;
    return (
      <Container fluid>
        <Table celled compact selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="14">
                {title.toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width="2">
                {t("accelerator.field.actions").toUpperCase()}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {versions.map((member, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>
                    <br />
                    <Segment color={color}>
                      {/* Field for accelerator version name. */}
                      <FikField
                        id={`accelerator-field-version-versionName-${index}`}
                        label={t("accelerator.field.version.versionName")}
                        name={`${path}.${index}.versionName`}
                        placeholder={t(
                          "accelerator.add.form.versions.section.field.versionName.placeholder"
                        )}
                        autoComplete="off"
                        component={FikInput}
                        required
                      />

                      <br />
                      <Divider horizontal className="left aligned">
                        <Header as="h6">
                          <Icon className={getIcon("version.icon")} />
                          {t(
                            "accelerator.add.form.versions.section.supportedVersions.section.title"
                          ).toUpperCase()}
                        </Header>
                      </Divider>

                      {/* Platform and Framework Versions supported by this accelerator */}
                      <PlatformAndFrameworkVersion
                        displayHeader={false}
                        path={`${path}.${index}.supportedPlatformAndFrameworkVersions`}
                        platformId={platformId}
                        frameworkId={frameworkId}
                        platforms={platforms}
                        frameworks={frameworks}
                        category={category}
                        supportedVersions={this.props.versions}
                      />

                      <br />
                      <Divider horizontal className="left aligned">
                        <Header as="h6">
                          <Icon className={getIcon("feature.icon")} />
                          {t(
                            "accelerator.add.form.versions.section.features.section.title"
                          ).toUpperCase()}
                        </Header>
                      </Divider>

                      {/* Platform and Framework Versions supported by this accelerator */}
                      <FeatureForm
                        displayHeader={false}
                        path={`${path}.${index}.features`}
                        platformId={platformId}
                        frameworkId={frameworkId}
                        platforms={platforms}
                        frameworks={frameworks}
                        category={category}
                        features={this.props.versions}
                      />

                      <br />
                      <Divider horizontal className="left aligned">
                        <Header as="h6">
                          <Icon className={getIcon("prerequisite.icon")} />
                          {t(
                            "accelerator.add.form.versions.section.prerequisites.section.title"
                          ).toUpperCase()}
                        </Header>
                      </Divider>

                      {/* Platform and Framework Versions supported by this accelerator */}
                      <PrerequisiteForm
                        displayHeader={false}
                        path={`${path}.${index}.prerequisites`}
                        platformId={platformId}
                        frameworkId={frameworkId}
                        platforms={platforms}
                        frameworks={frameworks}
                        category={category}
                        prerequisites={this.props.versions}
                      />
                    </Segment>
                  </Table.Cell>

                  {/* Column for actions. */}
                  <Table.Cell textAlign="center">
                    {index === length - 1 && (
                      <Button
                        id={`accelerator-version-add-button-${index}`}
                        type="button"
                        size="tiny"
                        icon={getIcon("version.add.icon")}
                        onClick={e =>
                          arrayHelpers.insert(
                            index + 1,
                            AcceleratorVersionForm.EMPTY_ROW
                          )
                        }
                        color="blue"
                        basic
                      />
                    )}
                    &nbsp;
                    <Button
                      id={`accelerator-version-delete-button-${index}`}
                      type="button"
                      size="tiny"
                      icon={getIcon("delete.icon")}
                      onClick={e => arrayHelpers.remove(index)}
                      color="red"
                      basic
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </Container>
    );
  };

  /**
   * This method renders a message box indicating the absence of versions for this resource.
   */
  renderNoAcceleratorVersionsContent = arrayHelpers => {
    const { t, platformId, frameworkId } = this.props;

    return (
      <Message align="center">
        <Message.Header>{t("accelerator.versions.empty.title")}</Message.Header>
        <Message.Content>
          <p>{t("accelerator.versions.empty.message")}</p>
          <FikButton
            id={`accelerator-versions-add`}
            type="button"
            icon={getIcon("version.add.icon")}
            label={t("accelerator.actions.versions.add")}
            onClick={e =>
              arrayHelpers.insert(0, AcceleratorVersionForm.EMPTY_ROW)
            }
            color="blue"
            disabled={!platformId || !frameworkId}
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(AcceleratorVersionForm);
