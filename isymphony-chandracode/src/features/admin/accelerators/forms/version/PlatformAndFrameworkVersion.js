/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Button, Container, Message, Table } from "semantic-ui-react";
import { FieldArray as FikFieldArray, Field as FikField } from "formik";
import { withTranslation } from "react-i18next";

import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikButton,
  FikDropdown
} from "../../../../../components/common/formik-wrappers";

/**
 * Component that renders the fields allowing the users to choose a combination of platform and framework versions.
 *
 * @author Chandra Veerapaneni
 */
class PlatformAndFrameworkVersion extends React.Component {
  static EMPTY_ROW = {
    id: "",
    version: "",
    framework: {
      id: "",
      version: ""
    }
  };

  /**
   * This method renders the component on the UI.
   */
  render() {
    const {
      path = "supportedPlatformAndFrameworkVersions",
      supportedVersions
    } = this.props;
    return (
      // Render the dynamic fields to capture the version information
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderForm(_.get(supportedVersions, path), arrayHelpers);
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the version details pertaining to the resource.
   */
  renderForm = (supportedVersions, arrayHelpers) => {
    const {
      t,
      title = t(
        "accelerator.add.form.versions.section.supportedVersions.section.title"
      ),
      subtitle = t(
        "accelerator.add.form.versions.section.supportedVersions.section.title"
      ),
      icon = getIcon("version.icon"),
      displayHeader = true
    } = this.props;

    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {supportedVersions && supportedVersions.length > 0
                  ? this.renderSupportedPlatformFrameworkVersionsContent(
                      supportedVersions,
                      arrayHelpers
                    )
                  : this.renderNoVersionsContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {supportedVersions && supportedVersions.length > 0
            ? this.renderSupportedPlatformFrameworkVersionsContent(
                supportedVersions,
                arrayHelpers
              )
            : this.renderNoVersionsContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a control that displays the configured modules for this resource.
   */
  renderSupportedPlatformFrameworkVersionsContent = (
    supportedVersions,
    arrayHelpers
  ) => {
    const {
      t,
      platformId,
      platforms,
      frameworkId,
      frameworks,
      path = "supportedPlatformAndFrameworkVersions"
    } = this.props;

    const platform = _.find(platforms, platform => platform.id === platformId);
    const framework = _.find(
      frameworks,
      framework => framework.id === frameworkId
    );
    const length = supportedVersions.length;

    return (
      <Table celled compact selectable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="7">
              {t("accelerator.field.platform.version").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="7">
              {t("accelerator.field.framework.versions").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="2">
              {t("accelerator.field.actions").toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {supportedVersions.map((member, index) => {
            return (
              <Table.Row key={index}>
                <Table.Cell>
                  <FikField
                    id={`${path}.${index}.version`}
                    name={`${path}.${index}.version`}
                    placeholder={t(
                      "accelerator.add.form.versions.section.supportedVersions.section.field.platform.version.placeholder"
                    ).replace("{platform.name}", platform.name)}
                    options={_.map(platform.versions, version => {
                      return { text: version, value: version };
                    })}
                    component={FikDropdown}
                    required
                  />
                </Table.Cell>

                <Table.Cell>
                  <FikField
                    id={`${path}.${index}.framework.version`}
                    name={`${path}.${index}.framework.versions`}
                    placeholder={t(
                      "accelerator.add.form.versions.section.supportedVersions.section.field.framework.version.placeholder"
                    ).replace("{platform.name}", platform.name)}
                    options={_.map(framework.versions, version => {
                      return {
                        text: version.versionName,
                        value: version.versionName
                      };
                    })}
                    component={FikDropdown}
                    required
                    multiple
                  />
                </Table.Cell>

                {/* Column for actions. */}
                <Table.Cell textAlign="center">
                  {index === length - 1 && (
                    <Button
                      id={`${path}-${index}-add-button`}
                      type="button"
                      size="tiny"
                      icon={getIcon("version.add.icon")}
                      onClick={e =>
                        arrayHelpers.insert(
                          index + 1,
                          _.merge(
                            _.clone(PlatformAndFrameworkVersion.EMPTY_ROW),
                            {
                              id: platformId,
                              framework: { id: frameworkId }
                            }
                          )
                        )
                      }
                      color="blue"
                      basic
                    />
                  )}
                  &nbsp;
                  <Button
                    id={`${path}-${index}-delete-button`}
                    type="button"
                    size="tiny"
                    icon={getIcon("delete.icon")}
                    onClick={e => arrayHelpers.remove(index)}
                    color="red"
                    basic
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    );
  };

  /**
   * This method renders a segment control indicating the absence of metadata for this resource.
   */
  renderNoVersionsContent = arrayHelpers => {
    const { t, platformId, frameworkId } = this.props;

    return (
      <Message align="center">
        <Message.Header>
          {t(
            "accelerator.add.form.versions.section.supportedVersions.section.empty.title"
          )}
        </Message.Header>
        <Message.Content>
          <p>
            {t(
              "accelerator.add.form.versions.section.supportedVersions.section.empty.subtitle"
            )}
          </p>
          <FikButton
            id={`accelerator-versions-supportedVersions-add`}
            type="button"
            icon={getIcon("platform.add.icon")}
            label={t("accelerator.actions.versions.supportedVersions.add")}
            onClick={e =>
              arrayHelpers.insert(
                0,
                _.merge(_.clone(PlatformAndFrameworkVersion.EMPTY_ROW), {
                  id: platformId,
                  framework: { id: frameworkId }
                })
              )
            }
            color="blue"
            disabled={!platformId || !frameworkId}
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(PlatformAndFrameworkVersion);
