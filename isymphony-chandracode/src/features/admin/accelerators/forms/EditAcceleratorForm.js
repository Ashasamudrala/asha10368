/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractAcceleratorForm from "./AbstractAcceleratorForm";
import {
  updateAcceleratorAction,
  getCategoryAction,
  getAllCategoriesAction
} from "../../../../actions/acceleratorActions";
import { getAllPlatformsAction } from "../../../../actions/coreActions";

import { renderLoader } from "../../../../utils";

/**
 * A form component that is used to capture the project details during the project creation.
 *
 * @author Chandra Veerapaneni
 */
class EditAcceleratorForm extends React.Component {
  // local state
  state = { categoriesDataLoaded: false, platformsDataLoaded: false };

  /**
   * This method is called as soon as the component got rendered in the DOM.
   */
  componentDidMount() {
    this.props.getAllCategoriesAction({
      onSuccess: () => {
        this.setState({ categoriesDataLoaded: true });
      }
    });
    this.props.getAllPlatformsAction({
      onSuccess: () => {
        this.setState({ platformsDataLoaded: true });
      }
    });
  }

  /**
   * This method gets called to render the new project form on the screen.
   */
  render() {
    const { t } = this.props;
    if (!this.state.categoriesDataLoaded) {
      return renderLoader(t("category.loading.single.message"));
    }
    if (!this.state.platformsDataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    const { accelerator, category, platforms, frameworks } = this.props;

    const clonedAccelerator = _.cloneDeep(accelerator);
    clonedAccelerator.metadata = _.map(
      Object.keys(accelerator.metadata),
      key => {
        return { key: key, value: accelerator.metadata[key] };
      }
    );

    if (clonedAccelerator.versions && clonedAccelerator.versions.length > 0) {
      clonedAccelerator.versions.forEach((version, index) => {
        const { prerequisites } = version;
        if (prerequisites && prerequisites.length > 0) {
          const clonedPrerequisites = [];
          prerequisites.forEach((p, counter) => {
            clonedPrerequisites.push({
              order: p.order,
              metadata: _.map(Object.keys(p.metadata), key => {
                return { key: key, value: p.metadata[key] };
              })
            });
          });
          version.prerequisites = _.orderBy(
            clonedPrerequisites,
            "order",
            "asc"
          );
        }
      });
    }

    return (
      <Container>
        <AbstractAcceleratorForm
          category={category}
          platformId={clonedAccelerator.platform.id}
          frameworkId={clonedAccelerator.platform.framework.id}
          platforms={platforms}
          frameworks={frameworks}
          title={t("accelerator.update.form.title")}
          subTitle={t("accelerator.update.form.subtitle").replace(
            "{category.name}",
            category.name
          )}
          mode="update"
          initialValues={clonedAccelerator}
          onSubmit={this.onSubmit}
        />
      </Container>
    );
  }

  /**
   * This method gets called whenever the form gets submitted by the user.
   */
  onSubmit = formValues => {
    const { accelerator } = this.props;
    // Unset the values that are not required.
    formValues.platform.name = null;
    formValues.platform.framework.name = null;

    this.props.updateAcceleratorAction(
      accelerator.categoryId,
      accelerator.id,
      formValues,
      {
        redirectUrl: "/accelerators"
      }
    );
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = (state, ownProps) => {
  return {
    category: state.categories.content[ownProps.match.params.categoryId],
    accelerator:
      state.categories.accelerators.content[
        ownProps.match.params.acceleratorId
      ],
    platforms: Object.values(state.application.platforms.content),
    frameworks: Object.values(state.application.frameworks.content)
  };
};

export default connect(mapStateToProps, {
  getAllPlatformsAction,
  getAllCategoriesAction,
  getCategoryAction,
  updateAcceleratorAction
})(withTranslation()(EditAcceleratorForm));
