/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Message, Table, Button } from "semantic-ui-react";
import { Field as FikField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import MetadataForm from "../../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikButton,
  FikInput
} from "../../../../../components/common/formik-wrappers";

/**
 * Component that is responsible to render a form that captures accelerator version details including their dependencies.
 *
 * @author Chandra Veerapaneni
 */
class PrerequisiteForm extends React.Component {
  // Empty row that gets inserted when the user clicks on add button.
  static EMPTY_ROW = {
    order: 500,
    metadata: []
  };

  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { prerequisites, path = "prerequisites" } = this.props;

    return (
      // Render the dynamic fields to capture the version information
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderPrerequisitesForm(
            _.get(prerequisites, path),
            arrayHelpers
          );
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the version details pertaining to the resource.
   */
  renderPrerequisitesForm = (prerequisites, arrayHelpers) => {
    const {
      t,
      title = t("accelerator.add.form.versions.section.title"),
      subtitle = t("accelerator.add.form.versions.section.subtitle"),
      icon = getIcon("version.icon"),
      displayHeader = true
    } = this.props;

    // Metadata for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {prerequisites && prerequisites.length > 0
                  ? this.renderPrerequisitesContent(prerequisites, arrayHelpers)
                  : this.renderPrerequisitesNoContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {prerequisites && prerequisites.length > 0
            ? this.renderPrerequisitesContent(prerequisites, arrayHelpers)
            : this.renderPrerequisitesNoContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a segment control that displays the configured metadata for this resource.
   */
  renderPrerequisitesContent = (prerequisites, arrayHelpers) => {
    const { t, path = "prerequisites" } = this.props;
    const length = prerequisites.length;

    return (
      <Container fluid>
        <Table celled compact selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="14">
                {t(
                  "accelerator.field.version.prerequisite.details"
                ).toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width="2">
                {t(
                  "accelerator.field.version.prerequisite.actions"
                ).toUpperCase()}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {prerequisites.map((member, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>
                    {/* Field for pre-requisite order. */}
                    <FikField
                      id={`${path}.${index}.order`}
                      label={t("accelerator.field.version.prerequisite.order")}
                      name={`${path}.${index}.order`}
                      placeholder={t(
                        "accelerator.add.form.versions.section.prerequisites.section.field.order.placeholder"
                      )}
                      autoComplete="off"
                      component={FikInput}
                      required
                    />

                    {/* Pre-requisite Metadata */}
                    <MetadataForm
                      displayHeader={false}
                      resourceName="prerequisites"
                      path={`${path}.${index}.metadata`}
                      metadata={this.props.prerequisites}
                    />
                  </Table.Cell>

                  {/* Column for actions. */}
                  <Table.Cell textAlign="center">
                    {index === length - 1 && (
                      <Button
                        id={`${path}-${index}-button-add`}
                        type="button"
                        size="tiny"
                        icon={getIcon("prerequisite.add.icon")}
                        onClick={e =>
                          arrayHelpers.insert(
                            index + 1,
                            PrerequisiteForm.EMPTY_ROW
                          )
                        }
                        color="blue"
                        basic
                      />
                    )}
                    &nbsp;
                    <Button
                      id={`${path}-${index}-button-delete`}
                      type="button"
                      size="tiny"
                      icon={getIcon("delete.icon")}
                      onClick={e => arrayHelpers.remove(index)}
                      color="red"
                      basic
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </Container>
    );
  };

  /**
   * This method renders a UI indicating the absence of prerequisites for an accelerator.
   */
  renderPrerequisitesNoContent = arrayHelpers => {
    const { t } = this.props;

    return (
      <Message align="center">
        <Message.Header>
          {t(
            "accelerator.add.form.versions.section.prerequisites.section.empty.title"
          )}
        </Message.Header>
        <Message.Content>
          <p>
            {t(
              "accelerator.add.form.versions.section.prerequisites.section.empty.subtitle"
            )}
          </p>
          <FikButton
            id={`accelerator-version-prerequisite-add`}
            type="button"
            icon={getIcon("prerequisite.add.icon")}
            label={t("accelerator.actions.versions.prerequisite.add")}
            onClick={e => arrayHelpers.insert(0, PrerequisiteForm.EMPTY_ROW)}
            color="blue"
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(PrerequisiteForm);
