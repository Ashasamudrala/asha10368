/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Message, Table, Button } from "semantic-ui-react";
import { Field as FikField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikButton,
  FikInput,
  FikTextArea
} from "../../../../../components/common/formik-wrappers";

/**
 * Component that is responsible to render a form that captures the accelerator features.
 *
 * @author Chandra Veerapaneni
 */
class FeatureForm extends React.Component {
  static EMPTY_ROW = {
    description: "",
    effortInPersonHours: 0.0
  };

  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { features, path = "features" } = this.props;

    return (
      // Render the dynamic fields to capture the version information
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderFeaturesForm(_.get(features, path), arrayHelpers);
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the version details pertaining to the resource.
   */
  renderFeaturesForm = (features, arrayHelpers) => {
    const {
      t,
      title = t("accelerator.add.form.versions.section.features.section.title"),
      subtitle = t(
        "accelerator.add.form.versions.section.features.section.subtitle"
      ),
      icon = getIcon("feature.icon"),
      displayHeader = true
    } = this.props;

    // Metadata for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {features && features.length > 0
                  ? this.renderFeaturesContent(features, arrayHelpers)
                  : this.renderFeaturesNoContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {features && features.length > 0
            ? this.renderFeaturesContent(features, arrayHelpers)
            : this.renderFeaturesNoContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a segment control that displays the configured metadata for this resource.
   */
  renderFeaturesContent = (features, arrayHelpers) => {
    const { t, path = "versions" } = this.props;
    const length = features.length;

    return (
      <Container fluid>
        <Table celled compact selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="11">
                {t(
                  "accelerator.field.version.feature.description"
                ).toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width="3">
                {t(
                  "accelerator.field.version.feature.effortInPersonHours"
                ).toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width="2">
                {t("accelerator.field.version.feature.actions").toUpperCase()}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {features.map((member, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>
                    {/* Field for accelerator version name. */}
                    <FikField
                      id={`${path}.${index}.description`}
                      name={`${path}.${index}.description`}
                      placeholder={t(
                        "accelerator.add.form.versions.section.features.section.field.description.placeholder"
                      )}
                      autoComplete="off"
                      component={FikTextArea}
                      rows={2}
                      required
                    />
                  </Table.Cell>
                  <Table.Cell>
                    {/* Field for accelerator version name. */}
                    <FikField
                      id={`${path}.${index}.effortInPersonHours`}
                      name={`${path}.${index}.effortInPersonHours`}
                      placeholder={t(
                        "accelerator.add.form.versions.section.features.section.field.effortInPersonHours.placeholder"
                      )}
                      autoComplete="off"
                      component={FikInput}
                      required
                    />
                  </Table.Cell>

                  {/* Column for actions. */}
                  <Table.Cell textAlign="center">
                    {index === length - 1 && (
                      <Button
                        id={`${path}-${index}-feature-add-button`}
                        type="button"
                        size="tiny"
                        icon={getIcon("feature.add.icon")}
                        onClick={e =>
                          arrayHelpers.insert(index + 1, FeatureForm.EMPTY_ROW)
                        }
                        color="blue"
                        basic
                      />
                    )}
                    &nbsp;
                    <Button
                      id={`${path}-${index}-feature-delete-button`}
                      type="button"
                      size="tiny"
                      icon={getIcon("delete.icon")}
                      onClick={e => arrayHelpers.remove(index)}
                      color="red"
                      basic
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </Container>
    );
  };

  /**
   * This method renders a segment control indicating the absence of versions for this resource.
   */
  renderFeaturesNoContent = arrayHelpers => {
    const { t } = this.props;

    return (
      <Message align="center">
        <Message.Header>
          {t(
            "accelerator.add.form.versions.section.features.section.empty.title"
          )}
        </Message.Header>
        <Message.Content>
          <p>
            {t(
              "accelerator.add.form.versions.section.features.section.empty.subtitle"
            )}
          </p>
          <FikButton
            id={`accelerator-version-feature-add`}
            type="button"
            icon={getIcon("feature.add.icon")}
            label={t("accelerator.actions.versions.feature.add")}
            onClick={e => arrayHelpers.insert(0, FeatureForm.EMPTY_ROW)}
            color="blue"
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(FeatureForm);
