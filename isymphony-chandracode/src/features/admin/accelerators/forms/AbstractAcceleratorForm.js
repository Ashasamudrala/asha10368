/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import React from "react";
import { Container, Divider, Form, Header, Icon } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import AcceleratorVersionForm from "./version/AcceleratorVersionForm";
import MetadataForm from "../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../components/common/panel/Panel";
import getIcon from "../../../../config/icons";
import {
  FikButton,
  FikInput,
  FikTextArea,
  FikDropdown
} from "../../../../components/common/formik-wrappers";
import { Formik, Field as FikField } from "formik";
import { renderPageTitle } from "../../../../utils";

/**
 * An abstract form component that provides the Create / Update of accelerators within the application.
 *
 * @author Chandra Veerapaneni
 */
class AbstractAcceleratorForm extends React.Component {
  /**
   * Constructor.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);
    // Component state
    this.state = {
      dataLoaded: false,
      platformId: this.props.platformId || "",
      frameworkId: this.props.frameworkId || ""
    };
  }

  /**
   * This method gets called to render the form on the screen.
   */
  render() {
    const {
      t,
      mode = "add",
      icon = getIcon("accelerator.icon"),
      category,
      platforms,
      frameworks,
      title = t("accelerator.add.form.title"),
      subtitle = t("accelerator.add.form.subtitle").replace(
        "{category.name}",
        category.name
      ),
      initialValues = {
        name: "",
        description: "",
        categoryId: category.id,
        platform: {
          id: "",
          framework: {
            id: ""
          }
        },
        versions: []
      }
    } = this.props;
    return (
      <Container>
        {renderPageTitle(title, subtitle, icon)}
        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon className={icon} />
            {mode === "add"
              ? t("accelerator.add.form.divider.title").replace(
                  "{category.name}",
                  category.name
                )
              : t("accelerator.update.form.divider.title").replace(
                  "{category.name}",
                  category.name
                )}
          </Header>
        </Divider>
        <br />
        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the basic framework details */}
              {this.renderBasicDetails()}

              <br />
              <br />
              {/* Render the fields to capture the accelerator versions */}
              <AcceleratorVersionForm
                path="versions"
                platformId={this.state.platformId}
                frameworkId={this.state.frameworkId}
                category={category}
                platforms={platforms}
                frameworks={frameworks}
                versions={props.values}
              />

              <br />
              <br />
              {/* Metadata for the accelerator */}
              <MetadataForm
                path="metadata"
                resourceName="accelerator"
                metadata={props.values}
              />

              <br />
              <br />
              <Divider />
              <br />

              {/* Add button */}
              <FikButton
                type="submit"
                icon={getIcon("accelerator.add.icon")}
                label={
                  mode === "add"
                    ? t("accelerator.actions.add")
                    : t("accelerator.actions.update")
                }
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                basic
                primary
              />

              {/* Clear button */}
              <FikButton
                type="reset"
                icon={getIcon("clear.icon")}
                label={t("clearForm")}
                onClick={props.handleReset}
                color="purple"
                basic
              />

              {/* Close button */}
              <FikButton
                icon={getIcon("close.icon")}
                label={t("close")}
                as={NavLink}
                exact
                to={`/accelerators`}
                floated="right"
                basic
                secondary
              />
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic accelerator details.
   */
  renderBasicDetails = () => {
    const { t, platforms } = this.props;

    // Basic details for the accelerator
    return (
      <Panel
        options={{
          title: t("accelerator.add.form.details.section.title").toUpperCase(),
          subtitle: t("accelerator.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container>
              {/* Field for accelerator name. */}
              <FikField
                id="accelerator-field-name"
                name="name"
                label={t("accelerator.field.name")}
                placeholder={t(
                  "accelerator.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />

              {/* Field for accelerator description. */}
              <FikField
                id="accelerator-field-description"
                name="description"
                label={t("accelerator.field.description")}
                placeholder={t(
                  "accelerator.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />

              <Form.Group widths="equal">
                {/* Field for Platform. */}
                <FikField
                  id="accelerator-field-platform"
                  label={t("accelerator.field.platform")}
                  name="platform.id"
                  placeholder={t(
                    "accelerator.add.form.details.section.field.platform.placeholder"
                  )}
                  options={_.map(platforms, platform => {
                    return { text: platform.name, value: platform.id };
                  })}
                  config={{
                    onSelectionChange: item =>
                      this.handlePlatformSelectionChange(item.value)
                  }}
                  component={FikDropdown}
                  required
                />

                {/* Field for Framework */}
                <FikField
                  id="accelerator-field-framework"
                  label={t("accelerator.field.framework")}
                  name="platform.framework.id"
                  placeholder={t(
                    "accelerator.add.form.details.section.field.framework.placeholder"
                  )}
                  options={this.getFrameworksForSelectedPlatform()}
                  config={{
                    onSelectionChange: item =>
                      this.handleFrameworkSelectionChange(item.value)
                  }}
                  component={FikDropdown}
                  required
                />
              </Form.Group>
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method gets called after a user selects a platform in the Platform dropdown.
   */
  handlePlatformSelectionChange = platformId => {
    this.setState({ platformId });
  };

  /**
   * This method gets called after a user selects a framework in the Framework dropdown.
   */
  handleFrameworkSelectionChange = frameworkId => {
    this.setState({ frameworkId });
  };

  /**
   * This method attempts to retrieve the applicable frameworks for a selected platform.
   * The selected platform is retrieved from the component state.
   */
  getFrameworksForSelectedPlatform = () => {
    if (
      _.isNull(this.state.platformId) ||
      _.isUndefined(this.state.platformId)
    ) {
      return [];
    }
    const { frameworks } = this.props;
    const frameworksForPlatform = _.filter(
      frameworks,
      framework => framework.platformId === this.state.platformId
    );
    if (frameworksForPlatform && frameworksForPlatform.length > 0) {
      return _.map(frameworksForPlatform, framework => {
        return { text: framework.name, value: framework.id };
      });
    }
    return [];
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = formValues => {
    this.setState({ formBeingSubmitted: true });
    if (this.props.onSubmit) {
      const clonedFormValues = _.clone(formValues);
      const { versions, metadata } = clonedFormValues;

      if (metadata && metadata.length > 0) {
        clonedFormValues.metadata = _.mapValues(
          _.keyBy(metadata, "key"),
          "value"
        );
      }

      if (versions && versions.length > 0) {
        versions.forEach((version, index) => {
          const { features, prerequisites } = version;

          if (features && features.length > 0) {
            const clonedFeatures = _.clone(features);
            version.features = clonedFeatures.map(f => {
              f.effortInPersonHours = parseFloat(f.effortInPersonHours);
              return f;
            });
          }

          if (prerequisites && prerequisites.length > 0) {
            const modifiedPrerequisites = [];
            prerequisites.forEach((prerequisite, index) => {
              modifiedPrerequisites.push({
                order: parseInt(prerequisite.order),
                metadata: _.mapValues(
                  _.keyBy(prerequisite.metadata, "key"),
                  "value"
                )
              });
            });
            version.prerequisites = modifiedPrerequisites;
          }
        });
      }

      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

export default withTranslation()(AbstractAcceleratorForm);
