/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractAcceleratorForm from "./AbstractAcceleratorForm";
import {
  createAcceleratorAction,
  getCategoryAction
} from "../../../../actions/acceleratorActions";
import { getAllPlatformsAction } from "../../../../actions/coreActions";
import { renderLoader } from "../../../../utils";

/**
 * A form component that is used to capture the project details during the project creation.
 *
 * @author Chandra Veerapaneni
 */
class NewAcceleratorForm extends React.Component {
  // local state
  state = { categoriesDataLoaded: false, platformsDataLoaded: false };

  /**
   * This method is called as soon as the component got rendered in the DOM.
   */
  componentDidMount() {
    this.props.getCategoryAction(this.props.match.params.categoryId, {
      onSuccess: () => {
        this.setState({ categoriesDataLoaded: true });
      }
    });

    this.props.getAllPlatformsAction({
      onSuccess: () => {
        this.setState({ platformsDataLoaded: true });
      }
    });
  }

  /**
   * This method gets called to render the new project form on the screen.
   */
  render() {
    const { t } = this.props;
    if (!this.state.categoriesDataLoaded) {
      return renderLoader(t("category.loading.single.message"));
    }
    if (!this.state.platformsDataLoaded) {
      return renderLoader(t("platform.loading.message"));
    }

    const { category, platforms, frameworks } = this.props;

    return (
      <Container>
        <AbstractAcceleratorForm
          category={category}
          platforms={platforms}
          frameworks={frameworks}
          onSubmit={this.onSubmit}
        />
      </Container>
    );
  }

  /**
   * This method gets called whenever the form gets submitted by the user.
   */
  onSubmit = formValues => {
    this.props.createAcceleratorAction(formValues.categoryId, formValues, {
      redirectUrl: "/accelerators"
    });
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = (state, ownProps) => {
  return {
    category: state.categories.content[ownProps.match.params.categoryId],
    platforms: Object.values(state.application.platforms.content),
    frameworks: Object.values(state.application.frameworks.content)
  };
};

export default connect(mapStateToProps, {
  createAcceleratorAction,
  getAllPlatformsAction,
  getCategoryAction
})(withTranslation()(NewAcceleratorForm));
