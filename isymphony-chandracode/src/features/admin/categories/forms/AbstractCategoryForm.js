/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form, Divider, Icon, Header } from "semantic-ui-react";
import { Formik, Field as FikField } from "formik";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import MetadataForm from "../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../components/common/panel/Panel";
import getIcon from "../../../../config/icons";
import {
  FikInput,
  FikTextArea,
  FikButton
} from "../../../../components/common/formik-wrappers";
import { renderPageTitle } from "../../../../utils";

/**
 * This form abstracts out the functionality required to create a new category or update an existing category.
 *
 * @author Chandra Veerapaneni
 */
class AbstractCategoryForm extends React.Component {
  // Defaults
  static defaults = {
    mode: "add",
    initialValues: {
      name: "",
      description: "",
      metadata: []
    }
  };

  /**
   * This method is responsible to render the form on the UI screen.
   */
  render() {
    const {
      t,
      title = t("category.home.title"),
      subTitle = t("category.home.subtitle"),
      icon = getIcon("category.icon"),
      initialValues = AbstractCategoryForm.defaults.initialValues,
      mode = AbstractCategoryForm.defaults.mode
    } = this.props;

    return (
      <Container>
        {renderPageTitle(title, subTitle, icon)}

        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon name={icon} />
            {mode === "add"
              ? t("category.add.form.divider.title")
              : t("category.update.form.divider.title")}
          </Header>
        </Divider>
        <br />

        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the basic category details */}
              {this.renderBasicDetails()}

              <br />
              <br />

              {/* Render the dynamic fields to capture the metadata about the categories */}
              <MetadataForm
                metadata={props.values}
                title={t("category.add.form.metadata.section.title")}
                subtitle={t("category.add.form.metadata.section.subtitle")}
              />

              <br />
              <Divider />
              <br />

              {/* Add button */}
              <FikButton
                type="submit"
                icon={getIcon("category.add.icon")}
                label={
                  mode === "add"
                    ? t("category.actions.add")
                    : t("category.actions.update")
                }
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                basic
                primary
              />

              {/* Clear button */}
              <FikButton
                type="reset"
                icon={getIcon("clear.icon")}
                label={t("clearForm")}
                onClick={props.handleReset}
                color="purple"
                basic
              />

              {/* Close button */}
              <FikButton
                icon={getIcon("close.icon")}
                label={t("close")}
                as={NavLink}
                exact
                to="/categories"
                floated="right"
                basic
                secondary
              />
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic category details.
   */
  renderBasicDetails = () => {
    const { t } = this.props;

    // Basic details for the category
    return (
      <Panel
        options={{
          title: t("category.add.form.details.section.title"),
          subtitle: t("category.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container>
              {/* Field for category name. */}
              <FikField
                id="category-field-name"
                name="name"
                label={t("category.field.name")}
                placeholder={t(
                  "category.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />

              {/* Field for category description. */}
              <FikField
                id="category-field-description"
                name="description"
                label={t("category.field.description")}
                placeholder={t(
                  "category.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = (formValues, formikApi) => {
    if (this.props.onSubmit) {
      const clonedFormValues = _.clone(formValues);
      const { metadata } = clonedFormValues;
      if (metadata && metadata.length >= 0) {
        clonedFormValues.metadata = _.mapValues(
          _.keyBy(metadata, "key"),
          "value"
        );
      }
      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  {}
)(withTranslation()(AbstractCategoryForm));
