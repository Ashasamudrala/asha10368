/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Pagination, Table, Button, List } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import getIcon from "../../../../config/icons";
import { FikLabeledButton } from "../../../../components/common/formik-wrappers";
import { configurePopup, renderLoader } from "../../../../utils";
import { getAllCategoriesAction } from "../../../../actions/acceleratorActions";

/**
 * This component is responsible to display the categories in a tabular fashion.
 *
 * @author Chandra Veerapaneni
 */
class CategoriesTable extends React.Component {
  // Local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is loaded into the DOM.
   */
  componentDidMount() {
    this.loadCategories(0, 20);
  }

  /**
   * This method is responsible to render the table on the UI.
   */
  render() {
    return <Container align="center">{this.renderTable()}</Container>;
  }

  /**
   * This method renders the table.
   */
  renderTable = () => {
    const { t } = this.props;
    const { dataLoaded } = this.state;
    if (!dataLoaded) {
      return renderLoader(t("category.loading.message"));
    }

    const { categories } = this.props;
    return (
      <Table celled padded>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={3}>
              {t("category.field.name").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={8}>
              {t("category.field.description").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={4}>
              {t("category.field.metadata").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={1}>
              {t("category.field.actions").toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {categories.map(category => {
            return (
              <Table.Row key={category.id}>
                {/* Category Name */}
                {configurePopup(
                  <Table.Cell>
                    <strong>{category.name}</strong>
                  </Table.Cell>,
                  t("category.field.name"),
                  category.name
                )}

                {/* Category Description */}
                {configurePopup(
                  <Table.Cell>{category.description}</Table.Cell>,
                  t("category.field.description"),
                  category.description
                )}

                {/* Metadata */}
                <Table.Cell>
                  <List horizontal>
                    {Object.keys(category.metadata).map(key => {
                      return (
                        <List.Item key={key}>
                          <List.Content>
                            <FikLabeledButton
                              size="tiny"
                              key={key}
                              content={key}
                              label={{
                                basic: true,
                                content: category.metadata[key]
                              }}
                            />
                          </List.Content>
                        </List.Item>
                      );
                    })}
                  </List>
                </Table.Cell>

                {/* Actions */}
                <Table.Cell textAlign="center">
                  <Button
                    as={NavLink}
                    icon={getIcon("edit.icon")}
                    size="tiny"
                    exact
                    to={`/categories/edit/${category.id}`}
                    color="blue"
                    basic
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="4">
              {this.renderPaginationControl()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  };

  /**
   * This method renders the pagination control in the UI.
   */
  renderPaginationControl = () => {
    const { pagination } = this.props;
    if (pagination && pagination.totalPages > 1) {
      return (
        <Pagination
          size="tiny"
          boundaryRange={0}
          defaultActivePage={1}
          ellipsisItem={null}
          firstItem={null}
          lastItem={null}
          siblingRange={1}
          totalPages={pagination.totalPages}
          floated="right"
          onPageChange={(e, data) =>
            this.loadCategories(data.activePage, pagination.pageSize)
          }
        />
      );
    }
  };

  /**
   * This method attempts to load the categories based on the provided pagination settings.
   */
  loadCategories = (pageNumber, pageSize) => {
    this.props.getAllCategoriesAction({
      params: {
        pageNumber: pageNumber - 1,
        pageSize: pageSize
      },
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  };
}

/**
 * This method extracts the required information from the redux store and makes it available
 * as the component properties.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {
    categories: Object.values(state.categories.content),
    pagination: state.categories.pagination
  };
};

export default connect(mapStateToProps, {
  getAllCategoriesAction
})(withTranslation()(CategoriesTable));
