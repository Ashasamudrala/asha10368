/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Divider, Button, Icon } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import CategoriesTable from "./common/CategoriesTable";
import getIcon from "../../../config/icons";
import { renderPageTitle } from "../../../utils";

/**
 * This component is the Home page for the categories section, where the administrative users
 * will be able to view / edit / delete the categories available in the system.
 *
 * @author Chandra Veerapaneni
 */
class CategoriesHome extends React.Component {
  /**
   * This method is responsible to render the category home page details on the UI.
   */
  render() {
    const { t } = this.props;

    return (
      <Container>
        {// Renders the page title.
        renderPageTitle(
          t("category.home.title"),
          t("category.home.subtitle"),
          getIcon("category.icon"),
          <Container align="right">
            <Button
              as={NavLink}
              exact
              to={"/categories/new"}
              floated="right"
              color="blue"
              basic
            >
              <Icon name={getIcon("category.icon")} />
              {t("category.actions.add")}
            </Button>
          </Container>
        )}
        <Divider />
        <CategoriesTable />
      </Container>
    );
  }
}

export default withTranslation()(CategoriesHome);
