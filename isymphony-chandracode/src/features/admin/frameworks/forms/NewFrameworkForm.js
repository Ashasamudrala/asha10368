/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractFrameworkForm from "./AbstractFrameworkForm";
import {
  getPlatformAction,
  createFrameworkAction
} from "../../../../actions/coreActions";
import { renderLoader } from "../../../../utils";

/**
 * This component is used for creation of a new category.
 *
 * @author Chandra Veerapaneni
 */
class NewFrameworkForm extends React.Component {
  // local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getPlatformAction(this.props.match.params.platformId, {
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  }

  /**
   * This method is responsible to render the UI on the screen.
   */
  render() {
    const { t } = this.props;
    if (!this.state.dataLoaded) {
      return renderLoader(t("framework.loading.single.message"));
    }

    const { platform } = this.props;
    return (
      <Container>
        <AbstractFrameworkForm
          platform={platform}
          onSubmit={this.onFormSubmit}
        />
      </Container>
    );
  }

  /**
   * This method gets called upon submission of the form. The parameters passed to this method
   * is the form data.
   */
  onFormSubmit = formValues => {
    const { platform } = this.props;
    this.props.createFrameworkAction(platform.id, formValues, {
      redirectUrl: "/frameworks"
    });
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = (state, ownProps) => {
  return {
    platform:
      state.application.platforms.content[ownProps.match.params.platformId]
  };
};

export default connect(mapStateToProps, {
  getPlatformAction,
  createFrameworkAction
})(withTranslation()(NewFrameworkForm));
