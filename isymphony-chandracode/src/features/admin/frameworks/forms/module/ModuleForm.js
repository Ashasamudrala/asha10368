/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import {
  Container,
  Message,
  Table,
  Button,
  Segment,
  Form,
  Divider,
  Header,
  Icon
} from "semantic-ui-react";
import { Field as FikField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import MetadataForm from "../../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikButton,
  FikInput,
  FikTextArea
} from "../../../../../components/common/formik-wrappers";

/**
 * Component that is responsible to render a form that captures module information for a specific version.
 *
 * @author Chandra Veerapaneni
 */
class ModuleForm extends React.Component {
  // Empty row
  static EMPTY_ROW = {
    name: "",
    description: "",
    version: ""
  };

  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { modules, path = "modules" } = this.props;

    return (
      // Render the dynamic fields to capture the version information
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderModulesForm(_.get(modules, path), arrayHelpers);
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the module details pertaining to the resource.
   */
  renderModulesForm = (modules, arrayHelpers) => {
    const {
      t,
      title = t("framework.add.form.versions.section.modules.section.title"),
      subtitle = t(
        "framework.add.form.versions.section.modules.section.subtitle"
      ),
      icon = getIcon("version.icon"),
      displayHeader = true
    } = this.props;

    // Metadata for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {modules && modules.length > 0
                  ? this.renderModulesContent(modules, arrayHelpers)
                  : this.renderNoModulesContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {modules && modules.length > 0
            ? this.renderModulesContent(modules, arrayHelpers)
            : this.renderNoModulesContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a control that displays the configured modules for this resource.
   */
  renderModulesContent = (modules, arrayHelpers) => {
    const { t, path = "modules" } = this.props;

    const color = "orange";
    const length = modules.length;
    return (
      <Table celled compact selectable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="14">
              {t("framework.field.module.details").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="2">
              {t("framework.field.module.actions").toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {modules.map((member, index) => {
            return (
              <Table.Row key={index}>
                <Table.Cell>
                  <br />
                  <Segment color={color}>
                    <Form.Group widths="equal">
                      {/* Field for module name. */}
                      <FikField
                        id={`${path}-${index}-name`}
                        label={t("framework.field.module.name")}
                        name={`${path}.${index}.name`}
                        placeholder={t(
                          "framework.add.form.versions.section.modules.section.field.name.placeholder"
                        )}
                        autoComplete="off"
                        component={FikInput}
                        required
                      />

                      {/* Field for module version. */}
                      <FikField
                        id={`${path}-${index}-version`}
                        label={t("framework.field.module.version")}
                        name={`${path}.${index}.version`}
                        placeholder={t(
                          "framework.add.form.versions.section.modules.section.field.version.placeholder"
                        )}
                        autoComplete="off"
                        component={FikInput}
                        required
                      />
                    </Form.Group>
                    {/* Field for module description. */}
                    <FikField
                      id={`${path}-${index}-description`}
                      label={t("framework.field.module.description")}
                      name={`${path}.${index}.description`}
                      placeholder={t(
                        "framework.add.form.versions.section.modules.section.field.description.placeholder"
                      )}
                      autoComplete="off"
                      component={FikTextArea}
                      rows={3}
                    />

                    <br />
                    <Container fluid>
                      <Divider horizontal className="left aligned">
                        <Header as="h6">
                          <Icon className={getIcon("metadata.icon")} />
                          {t(
                            "framework.add.form.versions.section.modules.section.metadata.section.title"
                          ).toUpperCase()}
                        </Header>
                      </Divider>

                      {/* Column for framework modules. */}
                      <MetadataForm
                        displayHeader={false}
                        path={`${path}.${index}.metadata`}
                        metadata={this.props.modules}
                        resourceName="frameworkVersionModule"
                      />
                    </Container>
                  </Segment>
                </Table.Cell>

                {/* Column for actions. */}
                <Table.Cell textAlign="center">
                  {index === length - 1 && (
                    <Button
                      id={`${path}-${index}-add-button`}
                      type="button"
                      size="tiny"
                      icon={getIcon("version.add.icon")}
                      onClick={e =>
                        arrayHelpers.insert(index + 1, ModuleForm.EMPTY_ROW)
                      }
                      color="blue"
                      basic
                    />
                  )}
                  &nbsp;
                  <Button
                    id={`${path}-${index}-delete-button`}
                    type="button"
                    size="tiny"
                    icon={getIcon("delete.icon")}
                    onClick={e => arrayHelpers.remove(index)}
                    color="red"
                    basic
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    );
  };

  /**
   * This method renders a segment control indicating the absence of metadata for this resource.
   */
  renderNoModulesContent = arrayHelpers => {
    const { t } = this.props;

    return (
      <Message align="center">
        <Message.Header>{t("framework.modules.empty.title")}</Message.Header>
        <Message.Content>
          <p>{t("framework.modules.empty.message")}</p>
          <FikButton
            id={`framework-versions-modules-add`}
            type="button"
            icon={getIcon("version.add.icon")}
            label={t("framework.actions.modules.add")}
            onClick={e => arrayHelpers.insert(0, ModuleForm.EMPTY_ROW)}
            color="blue"
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(ModuleForm);
