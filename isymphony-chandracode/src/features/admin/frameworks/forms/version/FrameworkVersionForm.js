/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import {
  Container,
  Message,
  Table,
  Button,
  Form,
  Divider,
  Header,
  Icon,
  Segment
} from "semantic-ui-react";
import { Field as FikField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import MetadataForm from "../../../../../components/common/form/vla/MetadataForm";
import ModuleForm from "../module/ModuleForm";
import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikButton,
  FikInput,
  FikDropdown
} from "../../../../../components/common/formik-wrappers";

/**
 * Component that is responsible to render a form that captures version information including their dependencies.
 *
 * @author Chandra Veerapaneni
 */
class FrameworkVersionForm extends React.Component {
  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { versions, path = "versions" } = this.props;

    return (
      // Render the dynamic fields to capture the version information
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderVersionForm(_.get(versions, path), arrayHelpers);
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the version details pertaining to the resource.
   */
  renderVersionForm = (versions, arrayHelpers) => {
    const {
      t,
      title = t("framework.add.form.versions.section.title"),
      subtitle = t("framework.add.form.versions.section.subtitle"),
      icon = getIcon("version.icon"),
      displayHeader = true
    } = this.props;

    // Metadata for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {versions && versions.length > 0
                  ? this.renderVersionsContent(versions, arrayHelpers)
                  : this.renderNoVersionsContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {versions && versions.length > 0
            ? this.renderVersionsContent(versions, arrayHelpers)
            : this.renderNoVersionsContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a segment control that displays the configured metadata for this resource.
   */
  renderVersionsContent = (versions, arrayHelpers) => {
    const {
      t,
      path = "versions",
      platform,
      title = t("framework.field.versionDetails")
    } = this.props;

    const color = "teal";
    const length = versions.length;
    return (
      <Container fluid>
        <Table celled compact selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width="14">
                {title.toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width="2">
                {t("framework.field.actions").toUpperCase()}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {versions.map((member, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>
                    <br />
                    <Segment color={color}>
                      <Form.Group widths="equal">
                        {/* Field for framework version name. */}
                        <FikField
                          id={`framework-field-version-versionName-${index}`}
                          label={t("framework.field.version.versionName")}
                          name={`${path}.${index}.versionName`}
                          placeholder={t(
                            "framework.add.form.versions.section.field.versionName.placeholder"
                          )}
                          autoComplete="off"
                          component={FikInput}
                          required
                        />

                        {/* Field for supported platform versions. */}
                        <FikField
                          id={`framework-field-version-supportedPlatformVersions-${index}`}
                          label={t(
                            "framework.field.version.supportedPlatformVersions"
                          )}
                          name={`${path}.${index}.supportedPlatformVersions`}
                          placeholder={t(
                            "framework.add.form.versions.section.field.supportedPlatformVersions.placeholder"
                          ).replace("{platform.name}", platform.name)}
                          options={_.map(platform.versions, version => {
                            return { text: version, value: version };
                          })}
                          autoComplete="off"
                          component={FikDropdown}
                          required
                          multiple
                        />
                      </Form.Group>

                      <br />
                      <Container fluid>
                        <Divider horizontal className="left aligned">
                          <Header as="h6">
                            <Icon className={getIcon("metadata.icon")} />
                            {t(
                              "framework.add.form.versions.section.metadata.section.title"
                            ).toUpperCase()}
                          </Header>
                        </Divider>
                        {/* Column for framework modules. */}
                        <MetadataForm
                          displayHeader={false}
                          path={`${path}.${index}.metadata`}
                          metadata={this.props.versions}
                          resourceName="frameworkVersion"
                        />
                      </Container>

                      <br />
                      <Container fluid>
                        <Divider horizontal className="left aligned">
                          <Header as="h6">
                            <Icon className={getIcon("module.icon")} />
                            {t(
                              "framework.add.form.versions.section.modules.section.title"
                            ).toUpperCase()}
                          </Header>
                        </Divider>
                        {/* Column for framework modules. */}
                        <ModuleForm
                          path={`${path}.${index}.modules`}
                          displayHeader={false}
                          modules={this.props.versions}
                        />
                      </Container>
                    </Segment>
                  </Table.Cell>

                  {/* Column for actions. */}
                  <Table.Cell textAlign="center">
                    {index === length - 1 && (
                      <Button
                        id={`framework-version-add-button-${index}`}
                        type="button"
                        size="tiny"
                        icon={getIcon("version.add.icon")}
                        onClick={e =>
                          arrayHelpers.insert(index + 1, {
                            versionName: "",
                            supportedPlatformVersions: []
                          })
                        }
                        color="blue"
                        basic
                      />
                    )}
                    &nbsp;
                    <Button
                      id={`framework-version-delete-button-${index}`}
                      type="button"
                      size="tiny"
                      icon={getIcon("delete.icon")}
                      onClick={e => arrayHelpers.remove(index)}
                      color="red"
                      basic
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </Container>
    );
  };

  /**
   * This method renders a segment control indicating the absence of versions for this resource.
   */
  renderNoVersionsContent = arrayHelpers => {
    const { t } = this.props;

    return (
      <Message align="center">
        <Message.Header>{t("framework.versions.empty.title")}</Message.Header>
        <Message.Content>
          <p>{t("framework.versions.empty.message")}</p>
          <FikButton
            id={`framework-versions-add`}
            type="button"
            icon={getIcon("version.add.icon")}
            label={t("framework.actions.versions.add")}
            onClick={e =>
              arrayHelpers.insert(0, {
                versionName: "",
                supportedPlatformVersions: []
              })
            }
            color="blue"
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(FrameworkVersionForm);
