/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractFrameworkForm from "./AbstractFrameworkForm";
import {
  getPlatformAction,
  updateFrameworkAction
} from "../../../../actions/coreActions";
import { renderLoader } from "../../../../utils";

/**
 * This component is used for creation of a new category.
 *
 * @author Chandra Veerapaneni
 */
class EditFrameworkForm extends React.Component {
  // local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getPlatformAction(this.props.match.params.platformId, {
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  }

  /**
   * This method is responsible to render the UI on the screen.
   */
  render() {
    const { t } = this.props;

    if (!this.state.dataLoaded) {
      return renderLoader(t("framework.loading.single.message"));
    }

    const {
      name,
      description,
      channelConfiguration,
      versions,
      metadata
    } = this.props.framework;
    // Need to transform the metadata that is available in the nested properties
    if (versions && versions.length >= 0) {
      versions.forEach((version, index) => {
        const clonedVersion = version ? _.clone(version) : {};
        if (clonedVersion.metadata) {
          version.metadata = _.map(Object.keys(clonedVersion.metadata), key => {
            return { key: key, value: clonedVersion.metadata[key] };
          });
        }
        const { modules } = version;
        if (modules && modules.length >= 0) {
          modules.forEach((module, moduleIndex) => {
            const clonedModule = module ? _.clone(module) : {};
            if (clonedModule.metadata) {
              module.metadata = _.map(
                Object.keys(clonedModule.metadata),
                key => {
                  return { key: key, value: clonedModule.metadata[key] };
                }
              );
            }
          });
        }
      });
    }

    return (
      <Container>
        <AbstractFrameworkForm
          platform={this.props.platform}
          mode="update"
          title={t("framework.update.form.title")}
          subtitle={t("framework.update.form.subtitle")}
          onSubmit={this.onFormSubmit}
          initialValues={{
            name: name,
            description: description,
            channelConfiguration: channelConfiguration,
            versions: versions,
            metadata: _.map(Object.keys(metadata), key => {
              return { key: key, value: metadata[key] };
            })
          }}
        />
      </Container>
    );
  }

  /**
   * This method gets called upon submission of the form. The parameters passed to this method
   * is the form data.
   */
  onFormSubmit = formValues => {
    const { platform, framework } = this.props;
    this.props.updateFrameworkAction(platform.id, framework.id, formValues, {
      redirectUrl: "/frameworks"
    });
  };
}

/**
 * This method extracts the required information from the redux store and makes them available as
 * component properties.
 *
 * @param {*} state Redux store
 * @param {*} ownProps Component's properties
 */
const mapStateToProps = (state, ownProps) => {
  return {
    platform:
      state.application.platforms.content[ownProps.match.params.platformId],
    framework:
      state.application.frameworks.content[ownProps.match.params.frameworkId]
  };
};

export default connect(mapStateToProps, {
  getPlatformAction,
  updateFrameworkAction
})(withTranslation()(EditFrameworkForm));
