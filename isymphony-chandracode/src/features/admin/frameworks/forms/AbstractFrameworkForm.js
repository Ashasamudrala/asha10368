/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form, Divider, Icon, Header } from "semantic-ui-react";
import { Formik, Field as FikField } from "formik";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import FrameworkVersionForm from "./version/FrameworkVersionForm";
import MetadataForm from "../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../components/common/panel/Panel";
import getIcon from "../../../../config/icons";
import {
  FikInput,
  FikTextArea,
  FikButton
} from "../../../../components/common/formik-wrappers";
import { renderPageTitle } from "../../../../utils";

/**
 * This form abstracts out the functionality required to create a new framework or update an existing framework.
 *
 * @author Chandra Veerapaneni
 */
class AbstractFrameworkForm extends React.Component {
  /**
   * This method is responsible to render the form on the UI screen.
   */
  render() {
    const {
      t,
      platform,
      title = t("framework.add.form.title"),
      subtitle = t("framework.add.form.subtitle"),
      icon = getIcon("framework.icon"),
      mode = "add",
      initialValues = {
        name: "",
        description: "",
        channelConfiguration: {
          channelName: ""
        },
        versions: [],
        metadata: []
      }
    } = this.props;

    return (
      <Container>
        {renderPageTitle(title, subtitle, icon)}

        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon className={icon} />
            {mode === "add"
              ? t("framework.add.form.divider.title").replace(
                  "{platform.name}",
                  platform.name
                )
              : t("framework.update.form.divider.title").replace(
                  "{platform.name}",
                  platform.name
                )}
          </Header>
        </Divider>
        <br />

        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the basic framework details */}
              {this.renderBasicDetails(props)}
              <br />
              <br />

              {/* Render the fields to capture the channel configuration for the respective framework */}
              {this.renderChannelConfigurationSection()}

              <br />
              <br />

              {/* Render the dynamic fields to capture the metadata about the framework */}
              <MetadataForm resourceName="framework" metadata={props.values} />

              <br />
              <Divider />
              <br />

              {/* Add button */}
              <FikButton
                type="submit"
                icon={getIcon("framework.add.icon")}
                label={
                  mode === "add"
                    ? t("framework.actions.add")
                    : t("framework.actions.update")
                }
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                basic
                primary
              />

              {/* Clear button */}
              <FikButton
                type="reset"
                icon={getIcon("clear.icon")}
                label={t("clearForm")}
                onClick={props.handleReset}
                color="purple"
                basic
              />

              {/* Close button */}
              <FikButton
                icon={getIcon("close.icon")}
                label={t("close")}
                as={NavLink}
                exact
                to={`/frameworks`}
                floated="right"
                basic
                secondary
              />
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic framework details.
   */
  renderBasicDetails = formikProps => {
    const { t } = this.props;

    // Basic details for the framework
    return (
      <Panel
        options={{
          title: t("framework.add.form.details.section.title").toUpperCase(),
          subtitle: t("framework.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container>
              {/* Field for framework name. */}
              <FikField
                id="framework-field-name"
                name="name"
                label={t("framework.field.name")}
                placeholder={t(
                  "framework.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />
              {/* Field for framework description. */}
              <FikField
                id="framework-field-description"
                name="description"
                label={t("framework.field.description")}
                placeholder={t(
                  "framework.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("version.icon")} />
                  {t("framework.field.versions").toUpperCase()}
                </Header>
              </Divider>

              {/* Versions section for the framework */}
              <FrameworkVersionForm
                displayHeader={false}
                platform={this.props.platform}
                versions={formikProps.values}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the basic repository details.
   */
  renderChannelConfigurationSection = () => {
    const { t } = this.props;

    // Channel configuration section for the framework
    return (
      <Panel
        options={{
          title: t(
            "framework.add.form.channelConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t(
            "framework.add.form.channelConfiguration.section.subtitle"
          ),
          icon: getIcon("channel.icon"),
          content: (
            <Container>
              {/* Field for framework channel name. */}
              <FikField
                id="framework-field-channelConfiguration-channelName"
                name="channelConfiguration.channelName"
                label={t("framework.field.channelConfiguration.channelName")}
                placeholder={t(
                  "framework.add.form.channelConfiguration.section.field.channelName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = (formValues, formikApi) => {
    if (this.props.onSubmit) {
      const { platform } = this.props;

      const clonedFormValues = _.clone(formValues);
      clonedFormValues.platformId = platform.id;
      const { metadata, versions } = clonedFormValues;
      if (metadata && metadata.length >= 0) {
        clonedFormValues.metadata = _.mapValues(
          _.keyBy(metadata, "key"),
          "value"
        );
      }

      if (versions && versions.length >= 0) {
        versions.forEach((version, index) => {
          // Retrofit the version metadata.
          const versionMetadata = version.metadata;
          if (versionMetadata && versionMetadata.length >= 0) {
            version.metadata = _.mapValues(
              _.keyBy(versionMetadata, "key"),
              "value"
            );
          }

          // Module metadata
          const { modules } = version;
          if (modules && modules.length >= 0) {
            modules.forEach((module, index) => {
              const moduleMetadata = module.metadata;
              if (moduleMetadata && moduleMetadata.length >= 0) {
                module.metadata = _.mapValues(
                  _.keyBy(moduleMetadata, "key"),
                  "value"
                );
              }
            });
          }
        });
      }

      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

export default withTranslation()(AbstractFrameworkForm);
