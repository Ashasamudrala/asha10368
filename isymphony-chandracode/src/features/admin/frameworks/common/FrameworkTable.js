/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import {
  Container,
  Table,
  Button,
  Message,
  Icon,
  List,
  Label
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import getIcon from "../../../../config/icons";
import { configurePopup } from "../../../../utils";

/**
 * This component is responsible to display the categories in a tabular fashion.
 *
 * @author Chandra Veerapaneni
 */
class FrameworkTable extends React.Component {
  /**
   * This method is responsible to render the table on the UI.
   */
  render() {
    return <Container align="center">{this.renderTable()}</Container>;
  }

  /**
   * This method renders the table.
   */
  renderTable = () => {
    const { t, platform } = this.props;
    return (
      <Container>
        {/* Message that indicates the platform and attached to the respective framework table. */}
        <Message warning icon attached>
          <Icon
            className={platform.metadata.icon || getIcon("question.icon")}
          />
          <Container align="left">
            <Message.Header>{`${t("framework.table.title").replace(
              "{platform.name}",
              platform.name
            )}`}</Message.Header>
            <Message.Content>{`${t("framework.table.subtitle").replace(
              "{platform.name}",
              platform.name
            )}`}</Message.Content>
          </Container>
          <Container align="right">
            <Button
              as={NavLink}
              exact
              to={`/platforms/${platform.id}/frameworks/new`}
              floated="right"
              color="blue"
              basic
            >
              <Icon name={getIcon("framework.icon")} />
              {t("framework.actions.add")}
            </Button>
          </Container>
        </Message>

        {/* Table that displays the frameworks for a specific platform. */}
        <Table celled padded attached="bottom">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={3}>
                {t("framework.field.name").toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width={6}>
                {t("framework.field.description").toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width={2}>
                {t("framework.field.platform").toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width={3}>
                {t("framework.field.versions").toUpperCase()}
              </Table.HeaderCell>
              <Table.HeaderCell width={2}>
                {t("framework.field.actions").toUpperCase()}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {Object.values(platform.frameworks).map(framework => {
              return (
                <Table.Row key={framework.id}>
                  {/* Framework Name */}
                  {configurePopup(
                    <Table.Cell>
                      <strong>{framework.name}</strong>
                    </Table.Cell>,
                    t("framework.field.name"),
                    framework.name
                  )}

                  {/* Framework Description */}
                  {configurePopup(
                    <Table.Cell>{framework.description}</Table.Cell>,
                    t("framework.field.description"),
                    framework.description
                  )}

                  {/* Platform to which this framework is associated to. */}
                  {configurePopup(
                    <Table.Cell>{framework.platformName}</Table.Cell>,
                    t("framework.field.platform"),
                    framework.platformName
                  )}

                  {/* Versions supported by the framework. */}
                  <Table.Cell>
                    <List horizontal>
                      {framework.versions.map(version => {
                        return (
                          <List.Item key={version.versionName}>
                            <List.Content>
                              <Label color="blue" basic>
                                {version.versionName}
                              </Label>
                            </List.Content>
                          </List.Item>
                        );
                      })}
                    </List>
                  </Table.Cell>

                  {/* Actions */}
                  <Table.Cell textAlign="center">
                    <Button
                      as={NavLink}
                      icon="edit"
                      size="tiny"
                      exact
                      to={`/platforms/${framework.platformId}/frameworks/edit/${framework.id}`}
                      color="blue"
                      basic
                    />
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </Container>
    );
  };
}

export default withTranslation()(FrameworkTable);
