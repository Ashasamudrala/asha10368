/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Divider } from "semantic-ui-react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";

import FrameworkTable from "./common/FrameworkTable";
import getIcon from "../../../config/icons";
import { getAllPlatformsAction } from "../../../actions/coreActions";
import { renderPageTitle } from "../../../utils";

/**
 * This component is the Home page for the frameworks section, where the administrative users
 * will be able to view / edit / delete the frameworks available in the system.
 *
 * @author Chandra Veerapaneni
 */
class FrameworkHome extends React.Component {
  // local state
  state = { dataLoaded: false };

  /**
   * This method gets called when a component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getAllPlatformsAction({
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  }

  /**
   * This method is responsible to render the framework home page details on the UI.
   */
  render() {
    const { t, platforms } = this.props;

    return (
      <Container>
        {// Renders the page title.
        renderPageTitle(
          t("framework.home.title"),
          t("framework.home.subtitle"),
          getIcon("framework.icon")
        )}

        <Divider />

        {/* Display the frameworks on a per-platform basis. */}
        {platforms.map(platform => {
          return (
            <Container key={platform.id}>
              <FrameworkTable platform={platform} />
              <br />
              <br />
            </Container>
          );
        })}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    platforms: Object.values(state.application.platforms.content),
    pagination: state.application.platforms.pagination
  };
};

export default connect(mapStateToProps, { getAllPlatformsAction })(
  withTranslation()(FrameworkHome)
);
