/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Divider, Button, Icon } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import ContinuousDeliveryTable from "./common/ContinuousDeliveryTable";
import getIcon from "../../../../config/icons";
import { renderPageTitle } from "../../../../utils";

/**
 * This component is the Home page for the continuous delivery configuration section, where the administrative users
 * will be able to view / edit / delete the continuous delivery configurations available in the system.
 *
 * @author Chandra Veerapaneni
 */
class ContinuousDeliveryHome extends React.Component {
  /**
   * This method is responsible to render the continuous delivery home page details on the UI.
   */
  render() {
    const { t } = this.props;

    return (
      <Container>
        {// Renders the page title.
        renderPageTitle(
          t("devops.cd.home.title"),
          t("devops.cd.home.subtitle"),
          getIcon("devops.delivery.icon"),
          <Container align="right">
            <Button
              as={NavLink}
              exact
              to={"/devops/delivery/new"}
              floated="right"
              color="blue"
              basic
            >
              <Icon name={getIcon("devops.delivery.icon")} />
              {t("devops.cd.actions.add.2")}
            </Button>
          </Container>
        )}

        <Divider />

        {/* Table that displays the continuous delivery configurations in the system. */}
        <ContinuousDeliveryTable />
      </Container>
    );
  }
}

export default withTranslation()(ContinuousDeliveryHome);
