/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form, Divider, Icon, Header } from "semantic-ui-react";
import { Formik, Field as FikField } from "formik";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import ApiEndpointForm from "../../../../../components/common/form/vla/ApiEndpointForm";
import MetadataForm from "../../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikInput,
  FikTextArea,
  FikButton
} from "../../../../../components/common/formik-wrappers";
import { renderPageTitle } from "../../../../../utils";

/**
 * This form abstracts out the functionality required to create a new or update an existing code repository configuration.
 *
 * @author Chandra Veerapaneni
 */
class AbstractContinuousDeliveryForm extends React.Component {
  // Defaults
  static defaults = {
    mode: "add",
    initialValues: {
      name: "",
      description: "",
      apiConfiguration: {
        accountName: "",
        userName: "",
        personalAccessToken: "",
        endpoints: [],
        metadata: []
      },
      channelConfiguration: {
        channelName: ""
      },
      metadata: []
    }
  };

  /**
   * This method is responsible to render the form on the UI screen.
   */
  render() {
    const {
      t,
      title = t("devops.cd.add.form.title"),
      subtitle = t("devops.cd.add.form.subtitle"),
      icon = getIcon("devops.delivery.icon"),
      mode = AbstractContinuousDeliveryForm.defaults.mode,
      initialValues = AbstractContinuousDeliveryForm.defaults.initialValues
    } = this.props;

    return (
      <Container>
        {renderPageTitle(title, subtitle, icon)}

        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon name={icon} />
            {mode === "add"
              ? t("devops.cd.add.form.divider.title")
              : t("devops.cd.update.form.divider.title")}
          </Header>
        </Divider>
        <br />

        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the continuous delivery configuration details */}
              {this.renderBasicDetails()}

              <br />
              <br />

              {/* Render the fields to capture the API configuration for the respective continuous integration configuration */}
              {this.renderApiConfigurationSection(props)}

              <br />
              <br />

              {/* Render the fields to capture the channel configuration for the respective continuous delivery configuration */}
              {this.renderChannelConfigurationSection()}

              <br />
              <br />

              {/* Render the dynamic fields to capture the metadata about the continuous delivery configuration */}
              <MetadataForm
                metadata={props.values}
                title={t(
                  "devops.cd.add.form.metadata.section.title"
                ).toUpperCase()}
                subtitle={t("devops.cd.add.form.metadata.section.subtitle")}
              />

              <br />
              <Divider />
              <br />

              {/* Add button */}
              <FikButton
                type="submit"
                icon={getIcon("devops.delivery.add.icon")}
                label={
                  mode === "add"
                    ? t("devops.cd.actions.add")
                    : t("devops.cd.actions.update")
                }
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                basic
                primary
              />

              {/* Clear button */}
              <FikButton
                type="reset"
                icon={getIcon("clear.icon")}
                label={t("clearForm")}
                onClick={props.handleReset}
                color="purple"
                basic
              />

              {/* Close button */}
              <FikButton
                icon={getIcon("close.icon")}
                label={t("close")}
                as={NavLink}
                exact
                to="/devops/delivery"
                floated="right"
                basic
                secondary
              />
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic details for the continuous delivery configuration.
   */
  renderBasicDetails = () => {
    const { t } = this.props;

    // Basic details for the continuous delivery
    return (
      <Panel
        options={{
          title: t("devops.cd.add.form.details.section.title").toUpperCase(),
          subtitle: t("devops.cd.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container>
              {/* Field for Continuous Delivery name. */}
              <FikField
                id="devops-cd-field-name"
                name="name"
                label={t("devops.cd.field.name")}
                placeholder={t(
                  "devops.cd.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />

              {/* Field for continuous delivery configuration description. */}
              <FikField
                id="devops-cd-field-description"
                name="description"
                label={t("devops.cd.field.description")}
                placeholder={t(
                  "devops.cd.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the API details for the continuous delivery system.
   */
  renderApiConfigurationSection = formikProps => {
    const { t } = this.props;

    // API Configuration section for the continuous delivery system.
    return (
      <Panel
        options={{
          title: t(
            "devops.cd.add.form.apiConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t("devops.cd.add.form.apiConfiguration.section.subtitle"),
          icon: getIcon("api.icon"),
          content: (
            <Container>
              {/* Field for continuous delivery system's account name. */}
              <FikField
                id="devops-cd-field-apiConfiguration-accountName"
                name="apiConfiguration.accountName"
                label={t("devops.cd.field.apiConfiguration.accountName")}
                placeholder={t(
                  "devops.cd.add.form.apiConfiguration.section.field.accountName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for continuous delivery system's user-name. */}
              <FikField
                id="form-input-devops-delivery-username"
                name="apiConfiguration.userName"
                label={t("devops.cd.field.apiConfiguration.username")}
                placeholder={t(
                  "devops.cd.add.form.apiConfiguration.section.field.username.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for continuous delivery system's user's personal access token. */}
              <FikField
                type="password"
                id="devops-cd-field-apiConfiguration-personalAccessToken"
                name="apiConfiguration.personalAccessToken"
                label={t(
                  "devops.cd.field.apiConfiguration.personalAccessToken"
                )}
                placeholder={t(
                  "devops.cd.add.form.apiConfiguration.section.field.personalAccessToken.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              <br />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("api.endpoint.icon")} />
                  {t("default.api.endpoint.add.form.title").toUpperCase()}
                </Header>
              </Divider>

              <ApiEndpointForm
                path="apiConfiguration.endpoints"
                displayHeader={false}
                endpoints={formikProps.values}
              />

              <br />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("api.metadata.icon")} />
                  {t("api.metadata.add.form.title").toUpperCase()}
                </Header>
              </Divider>

              {/* Render the dynamic fields to capture the metadata about the API Configuration */}
              <MetadataForm
                resourceName="api"
                path="apiConfiguration.metadata"
                metadata={formikProps.values}
                displayHeader={false}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the channel configuration for the continuous delivery system.
   */
  renderChannelConfigurationSection = () => {
    const { t } = this.props;

    // Channel configuration section for the continuous delivery system.
    return (
      <Panel
        options={{
          title: t(
            "devops.cd.add.form.channelConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t(
            "devops.cd.add.form.channelConfiguration.section.subtitle"
          ),
          icon: getIcon("channel.icon"),
          content: (
            <Container>
              {/* Field for channel name for the respective continuous delivery system. */}
              <FikField
                id="devops-cd-field-channelConfiguration-channelName"
                name="channelConfiguration.channelName"
                label={t("devops.cd.field.channelConfiguration.channelName")}
                placeholder={t(
                  "devops.cd.add.form.channelConfiguration.section.field.channelName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = (formValues, formikApi) => {
    if (this.props.onSubmit) {
      const clonedFormValues = _.clone(formValues);
      const { metadata, apiConfiguration } = clonedFormValues;
      if (metadata && metadata.length >= 0) {
        clonedFormValues.metadata = _.mapValues(
          _.keyBy(metadata, "key"),
          "value"
        );
      }
      if (
        apiConfiguration &&
        apiConfiguration.metadata &&
        apiConfiguration.metadata.length >= 0
      ) {
        clonedFormValues.apiConfiguration.metadata = _.mapValues(
          _.keyBy(apiConfiguration.metadata, "key"),
          "value"
        );
      }
      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  {}
)(withTranslation()(AbstractContinuousDeliveryForm));
