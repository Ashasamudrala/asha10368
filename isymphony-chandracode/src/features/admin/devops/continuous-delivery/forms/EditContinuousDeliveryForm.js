/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractContinuousDeliveryForm from "./AbstractContinuousDeliveryForm";
import {
  getDevOpsDeliveryConfigurationAction,
  updateDevOpsDeliveryConfigurationAction
} from "../../../../../actions/devopsDeliveryConfigurationActions";
import { renderLoader } from "../../../../../utils";

/**
 * This component is used for updating an existing continuous delivery configuration instance.
 *
 * @author Chandra Veerapaneni
 */
class EditContinuousDeliveryForm extends React.Component {
  // local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getDevOpsDeliveryConfigurationAction(
      this.props.match.params.id,
      {
        onSuccess: () => {
          this.setState({ dataLoaded: true });
        }
      }
    );
  }

  /**
   * This method is responsible to render the UI on the screen.
   */
  render() {
    const { t } = this.props;

    if (!this.state.dataLoaded) {
      return renderLoader(t("devops.cd.loading.single.message"));
    }

    const {
      name,
      description,
      apiConfiguration,
      channelConfiguration,
      metadata
    } = this.props.devopsDeliveryConfiguration;
    // Clone the apiConfiguration and transform metadata property.
    const clonedApiConfiguration = apiConfiguration
      ? _.clone(apiConfiguration)
      : {};
    if (apiConfiguration.metadata) {
      clonedApiConfiguration.metadata = _.map(
        Object.keys(apiConfiguration.metadata),
        key => {
          return { key: key, value: apiConfiguration.metadata[key] };
        }
      );
    }

    return (
      <Container>
        <AbstractContinuousDeliveryForm
          title={t("devops.cd.update.form.title")}
          subtitle={t("devops.cd.update.form.subtitle")}
          mode="update"
          onSubmit={this.onFormSubmit}
          initialValues={{
            name: name,
            description: description,
            apiConfiguration: clonedApiConfiguration,
            channelConfiguration: channelConfiguration,
            metadata: _.map(Object.keys(metadata), key => {
              return { key: key, value: metadata[key] };
            })
          }}
        />
      </Container>
    );
  }

  /**
   * This method gets called upon submission of the form. The parameters passed to this method
   * is the form data.
   */
  onFormSubmit = formValues => {
    this.props.updateDevOpsDeliveryConfigurationAction(
      this.props.devopsDeliveryConfiguration.id,
      formValues,
      {
        redirectUrl: "/devops/delivery"
      }
    );
  };
}

/**
 * This method extracts the required information from the redux store and makes them available as
 * component properties.
 *
 * @param {*} state Redux store
 * @param {*} ownProps Component's properties
 */
const mapStateToProps = (state, ownProps) => {
  return {
    devopsDeliveryConfiguration:
      state.devopsDeliveryConfigurations.content[ownProps.match.params.id]
  };
};

export default connect(mapStateToProps, {
  getDevOpsDeliveryConfigurationAction,
  updateDevOpsDeliveryConfigurationAction
})(withTranslation()(EditContinuousDeliveryForm));
