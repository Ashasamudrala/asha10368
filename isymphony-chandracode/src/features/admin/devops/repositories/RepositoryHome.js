/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Divider, Button, Icon } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import RepositoryTable from "./common/RepositoryTable";
import getIcon from "../../../../config/icons";
import { renderPageTitle } from "../../../../utils";

/**
 * This component is the Home page for the code repositories section, where the administrative users
 * will be able to view / edit / delete the code repository configurations available in the system.
 *
 * @author Chandra Veerapaneni
 */
class RepositoryHome extends React.Component {
  /**
   * This method is responsible to render the repository home page details on the UI.
   */
  render() {
    const { t } = this.props;

    return (
      <Container>
        {// Renders the page title.
        renderPageTitle(
          t("repository.home.title"),
          t("repository.home.subtitle"),
          getIcon("repository.icon"),
          <Container align="right">
            <Button
              as={NavLink}
              exact
              to={"/devops/repositories/new"}
              floated="right"
              color="blue"
              basic
            >
              <Icon name={getIcon("repository.icon")} />
              {t("repository.actions.add")}
            </Button>
          </Container>
        )}

        <Divider />

        {/* Table that displays the repositories in the system. */}
        <RepositoryTable />
      </Container>
    );
  }
}

export default withTranslation()(RepositoryHome);
