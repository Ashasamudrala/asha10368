/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form, Divider, Icon, Header } from "semantic-ui-react";
import { Formik, Field as FikField } from "formik";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import ApiEndpointForm from "../../../../../components/common/form/vla/ApiEndpointForm";
import MetadataForm from "../../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikInput,
  FikTextArea,
  FikButton
} from "../../../../../components/common/formik-wrappers";
import { renderPageTitle } from "../../../../../utils";

/**
 * This form abstracts out the functionality required to create a new or update an existing code repository configuration.
 *
 * @author Chandra Veerapaneni
 */
class AbstractRepositoryForm extends React.Component {
  // Defaults
  static defaults = {
    mode: "add",
    initialValues: {
      name: "",
      description: "",
      apiConfiguration: {
        accountName: "",
        userName: "",
        personalAccessToken: "",
        endpoints: [],
        metadata: []
      },
      channelConfiguration: {
        channelName: ""
      },
      metadata: []
    }
  };

  /**
   * Constructor.
   *
   * @param {*} props Component properties.
   */
  constructor(props) {
    super(props);

    const {
      initialValues = AbstractRepositoryForm.defaults.initialValues
    } = this.props;
    this.state = {
      options: _.map(initialValues.versions, version => {
        return { text: version, value: version };
      })
    };
  }

  /**
   * This method is responsible to render the form on the UI screen.
   */
  render() {
    const {
      t,
      title = t("repository.add.form.title"),
      subtitle = t("repository.add.form.subtitle"),
      icon = getIcon("repository.icon"),
      mode = AbstractRepositoryForm.defaults.mode,
      initialValues = AbstractRepositoryForm.defaults.initialValues
    } = this.props;

    return (
      <Container>
        {renderPageTitle(title, subtitle, icon)}
        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon name={icon} />
            {mode === "add"
              ? t("repository.add.form.divider.title")
              : t("repository.update.form.divider.title")}
          </Header>
        </Divider>
        <br />
        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the basic repository details */}
              {this.renderBasicDetails()}

              <br />
              <br />

              {/* Render the fields to capture the API configuration for the respective repository */}
              {this.renderApiConfigurationSection(props)}

              <br />
              <br />

              {/* Render the fields to capture the channel configuration for the respective repository */}
              {this.renderChannelConfigurationSection()}

              <br />
              <br />

              {/* Render the dynamic fields to capture the metadata about the repository */}
              <MetadataForm
                metadata={props.values}
                title={t("repository.add.form.metadata.section.title")}
                subtitle={t("repository.add.form.metadata.section.subtitle")}
              />

              <br />
              <Divider />
              <br />

              {/* Add button */}
              <FikButton
                type="submit"
                icon={getIcon("repository.add.icon")}
                label={
                  mode === "add"
                    ? t("repository.actions.add")
                    : t("repository.actions.update")
                }
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                basic
                primary
              />

              {/* Clear button */}
              <FikButton
                type="reset"
                icon={getIcon("clear.icon")}
                label={t("clearForm")}
                onClick={props.handleReset}
                color="purple"
                basic
              />

              {/* Close button */}
              <FikButton
                icon={getIcon("close.icon")}
                label={t("close")}
                as={NavLink}
                exact
                to="/devops/repositories"
                floated="right"
                basic
                secondary
              />
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic repository details.
   */
  renderBasicDetails = () => {
    const { t } = this.props;

    // Basic details for the repository
    return (
      <Panel
        options={{
          title: t("repository.add.form.details.section.title").toUpperCase(),
          subtitle: t("repository.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container>
              {/* Field for repository name. */}
              <FikField
                id="form-input-repository-name"
                name="name"
                label={t("repository.field.name")}
                placeholder={t(
                  "repository.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />

              {/* Field for repository description. */}
              <FikField
                id="form-textarea-repository-description"
                name="description"
                label={t("repository.field.description")}
                placeholder={t(
                  "repository.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the basic repository details.
   */
  renderApiConfigurationSection = formikProps => {
    const { t } = this.props;

    // API Configuration section for the repository
    return (
      <Panel
        options={{
          title: t(
            "repository.add.form.apiConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t("repository.add.form.apiConfiguration.section.subtitle"),
          icon: getIcon("api.icon"),
          content: (
            <Container>
              {/* Field for repository Account name. */}
              <FikField
                id="form-input-repository-api-account-name"
                name="apiConfiguration.accountName"
                label={t("repository.field.apiConfiguration.accountName")}
                placeholder={t(
                  "repository.add.form.apiConfiguration.section.field.accountName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for repository user-name. */}
              <FikField
                id="form-input-repository-api-username"
                name="apiConfiguration.userName"
                label={t("repository.field.apiConfiguration.username")}
                placeholder={t(
                  "repository.add.form.apiConfiguration.section.field.username.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for repository user's personal access token. */}
              <FikField
                type="password"
                id="form-input-repository-api-personal-access-token"
                name="apiConfiguration.personalAccessToken"
                label={t(
                  "repository.field.apiConfiguration.personalAccessToken"
                )}
                placeholder={t(
                  "repository.add.form.apiConfiguration.section.field.personalAccessToken.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
              <br />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("api.endpoint.icon")} />
                  {t("default.api.endpoint.add.form.title").toUpperCase()}
                </Header>
              </Divider>

              <ApiEndpointForm
                path="apiConfiguration.endpoints"
                displayHeader={false}
                endpoints={formikProps.values}
              />

              <br />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("api.metadata.icon")} />
                  {t("api.metadata.add.form.title").toUpperCase()}
                </Header>
              </Divider>

              {/* Render the dynamic fields to capture the metadata about the API Configuration */}
              <MetadataForm
                resourceName="api"
                path="apiConfiguration.metadata"
                metadata={formikProps.values}
                displayHeader={false}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the basic repository details.
   */
  renderChannelConfigurationSection = () => {
    const { t } = this.props;

    // Channel configuration section for the repository
    return (
      <Panel
        options={{
          title: t(
            "repository.add.form.channelConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t(
            "repository.add.form.channelConfiguration.section.subtitle"
          ),
          icon: getIcon("channel.icon"),
          content: (
            <Container>
              {/* Field for repository channel name. */}
              <FikField
                id="form-input-repository-channel-name"
                name="channelConfiguration.channelName"
                label={t("repository.field.channelConfiguration.channelName")}
                placeholder={t(
                  "repository.add.form.channelConfiguration.section.field.channelName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = (formValues, formikApi) => {
    if (this.props.onSubmit) {
      const clonedFormValues = _.clone(formValues);
      const { metadata, apiConfiguration } = clonedFormValues;
      if (metadata && metadata.length >= 0) {
        clonedFormValues.metadata = _.mapValues(
          _.keyBy(metadata, "key"),
          "value"
        );
      }
      if (
        apiConfiguration &&
        apiConfiguration.metadata &&
        apiConfiguration.metadata.length >= 0
      ) {
        clonedFormValues.apiConfiguration.metadata = _.mapValues(
          _.keyBy(apiConfiguration.metadata, "key"),
          "value"
        );
      }
      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  {}
)(withTranslation()(AbstractRepositoryForm));
