/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Pagination, Table, Button, List } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import getIcon from "../../../../../config/icons";
import { FikLabeledButton } from "../../../../../components/common/formik-wrappers";
import { configurePopup, renderLoader } from "../../../../../utils";
import { getAllRepositoriesAction } from "../../../../../actions/repositoryActions";

/**
 * This component is responsible to display the repositories in a tabular fashion.
 *
 * @author Chandra Veerapaneni
 */
class RepositoryTable extends React.Component {
  // Local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is loaded into the DOM.
   */
  componentDidMount() {
    this.loadRepositories(0, 20);
  }

  /**
   * This method is responsible to render the table on the UI.
   */
  render() {
    return <Container align="center">{this.renderTable()}</Container>;
  }

  /**
   * This method renders the table.
   */
  renderTable = () => {
    const { t } = this.props;
    const { dataLoaded } = this.state;
    if (!dataLoaded) {
      return renderLoader(t("repository.loading.message"));
    }

    const { repositories } = this.props;
    return (
      <Table celled padded>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={3}>
              {t("repository.field.name").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={6}>
              {t("repository.field.description").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={5}>
              {t("repository.field.metadata").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={2}>
              {t("repository.field.actions").toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {repositories.map(repository => {
            return (
              <Table.Row key={repository.id}>
                {/* Repository Name */}
                {configurePopup(
                  <Table.Cell>
                    <strong>{repository.name}</strong>
                  </Table.Cell>,
                  t("repository.field.name"),
                  repository.name
                )}

                {/* Repository Description */}
                {configurePopup(
                  <Table.Cell>{repository.description}</Table.Cell>,
                  t("repository.field.description"),
                  repository.description
                )}

                {/* Metadata */}
                <Table.Cell>
                  <List horizontal>
                    {Object.keys(repository.metadata).map(key => {
                      return (
                        <List.Item key={key}>
                          <List.Content>
                            <FikLabeledButton
                              size="tiny"
                              key={key}
                              content={key}
                              label={{
                                basic: true,
                                content: repository.metadata[key]
                              }}
                            />
                          </List.Content>
                        </List.Item>
                      );
                    })}
                  </List>
                </Table.Cell>

                {/* Actions */}
                <Table.Cell textAlign="center">
                  <Button
                    as={NavLink}
                    icon={getIcon("edit.icon")}
                    size="tiny"
                    exact
                    to={`/devops/repositories/edit/${repository.id}`}
                    color="blue"
                    basic
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="5">
              {this.renderPaginationControl()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  };

  /**
   * This method renders the pagination control in the UI.
   */
  renderPaginationControl = () => {
    const { pagination } = this.props;
    if (pagination && pagination.totalPages > 1) {
      return (
        <Pagination
          size="tiny"
          boundaryRange={0}
          defaultActivePage={1}
          ellipsisItem={null}
          firstItem={null}
          lastItem={null}
          siblingRange={1}
          totalPages={pagination.totalPages}
          floated="right"
          onPageChange={(e, data) =>
            this.loadRepositories(data.activePage, pagination.pageSize)
          }
        />
      );
    }
  };

  /**
   * This method attempts to load the repositories based on the provided pagination settings.
   */
  loadRepositories = (pageNumber, pageSize) => {
    this.props.getAllRepositoriesAction({
      params: {
        pageNumber: pageNumber - 1,
        pageSize: pageSize
      },
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  };
}

/**
 * This method extracts the required information from the redux store and makes it available
 * as the component properties.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {
    repositories: Object.values(state.repositories.content),
    pagination: state.repositories.pagination
  };
};

export default connect(mapStateToProps, {
  getAllRepositoriesAction
})(withTranslation()(RepositoryTable));
