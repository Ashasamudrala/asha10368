/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import React from "react";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import AbstractContinuousIntegrationForm from "./AbstractContinuousIntegrationForm";
import {
  getDevOpsIntegrationConfigurationAction,
  updateDevOpsIntegrationConfigurationAction
} from "../../../../../actions/devopsIntegrationConfigurationActions";
import { renderLoader } from "../../../../../utils";

/**
 * This component is used for updating an existing continuous integration configuration instance.
 *
 * @author Chandra Veerapaneni
 */
class EditContinuousIntegrationForm extends React.Component {
  // local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is rendered in the DOM.
   */
  componentDidMount() {
    this.props.getDevOpsIntegrationConfigurationAction(
      this.props.match.params.id,
      {
        onSuccess: () => {
          this.setState({ dataLoaded: true });
        }
      }
    );
  }

  /**
   * This method is responsible to render the UI on the screen.
   */
  render() {
    const { t } = this.props;

    if (!this.state.dataLoaded) {
      return renderLoader(t("devops.ci.loading.single.message"));
    }

    const {
      name,
      description,
      apiConfiguration,
      pipelineConfiguration,
      channelConfiguration,
      metadata
    } = this.props.devopsIntegrationConfiguration;

    // Clone the apiConfiguration and transform metadata property.
    const clonedApiConfiguration = apiConfiguration
      ? _.clone(apiConfiguration)
      : {};
    if (apiConfiguration.metadata) {
      clonedApiConfiguration.metadata = _.map(
        Object.keys(apiConfiguration.metadata),
        key => {
          return { key: key, value: apiConfiguration.metadata[key] };
        }
      );
    }

    return (
      <Container>
        <AbstractContinuousIntegrationForm
          title={t("devops.ci.update.form.title")}
          subtitle={t("devops.ci.update.form.subtitle")}
          mode="update"
          onSubmit={this.onFormSubmit}
          initialValues={{
            name: name,
            description: description,
            apiConfiguration: clonedApiConfiguration,
            channelConfiguration: channelConfiguration,
            pipelineConfiguration: pipelineConfiguration,
            metadata: _.map(Object.keys(metadata), key => {
              return { key: key, value: metadata[key] };
            })
          }}
        />
      </Container>
    );
  }

  /**
   * This method gets called upon submission of the form. The parameters passed to this method
   * is the form data.
   */
  onFormSubmit = formValues => {
    this.props.updateDevOpsIntegrationConfigurationAction(
      this.props.devopsIntegrationConfiguration.id,
      formValues,
      {
        redirectUrl: "/devops/integration"
      }
    );
  };
}

/**
 * This method extracts the required information from the redux store and makes them available as
 * component properties.
 *
 * @param {*} state Redux store
 * @param {*} ownProps Component's properties
 */
const mapStateToProps = (state, ownProps) => {
  return {
    devopsIntegrationConfiguration:
      state.devopsIntegrationConfigurations.content[ownProps.match.params.id]
  };
};

export default connect(mapStateToProps, {
  getDevOpsIntegrationConfigurationAction,
  updateDevOpsIntegrationConfigurationAction
})(withTranslation()(EditContinuousIntegrationForm));
