/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Form, Divider, Icon, Header } from "semantic-ui-react";
import { Formik, Field as FikField } from "formik";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import ApiEndpointForm from "../../../../../components/common/form/vla/ApiEndpointForm";
import MetadataForm from "../../../../../components/common/form/vla/MetadataForm";
import Panel from "../../../../../components/common/panel/Panel";
import getIcon from "../../../../../config/icons";
import {
  FikInput,
  FikTextArea,
  FikButton
} from "../../../../../components/common/formik-wrappers";
import { renderPageTitle } from "../../../../../utils";

/**
 * This form abstracts out the functionality required to create a new or update an existing code repository configuration.
 *
 * @author Chandra Veerapaneni
 */
class AbstractContinuousIntegrationForm extends React.Component {
  // Defaults
  static defaults = {
    mode: "add",
    initialValues: {
      name: "",
      description: "",
      apiConfiguration: {
        accountName: "",
        userName: "",
        personalAccessToken: "",
        endpoints: [],
        metadata: []
      },
      pipelineConfiguration: {
        sonarConfiguration: {
          endpoint: "",
          userName: "",
          personalAccessToken: ""
        }
      },
      channelConfiguration: {
        channelName: ""
      },
      metadata: []
    }
  };

  /**
   * This method is responsible to render the form on the UI screen.
   */
  render() {
    const {
      t,
      title = t("devops.ci.add.form.title"),
      subtitle = t("devops.ci.add.form.subtitle"),
      icon = getIcon("devops.integration.icon"),
      mode = AbstractContinuousIntegrationForm.defaults.mode,
      initialValues = AbstractContinuousIntegrationForm.defaults.initialValues
    } = this.props;

    return (
      <Container>
        {renderPageTitle(title, subtitle, icon)}

        <br />
        <Divider horizontal>
          <Header as="h4" color="grey">
            <Icon name={icon} />
            {mode === "add"
              ? t("devops.ci.add.form.divider.title")
              : t("devops.ci.update.form.divider.title")}
          </Header>
        </Divider>
        <br />

        {/* Build the form. */}
        <Formik
          initialValues={initialValues}
          onSubmit={(formValues, formApi) => {
            this.onFormSubmit(formValues, formApi);
          }}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              {/* Render the fields to capture the continuous integration configuration details */}
              {this.renderBasicDetails()}

              <br />
              <br />

              {/* Render the fields to capture the API configuration for the respective continuous integration configuration */}
              {this.renderApiConfigurationSection(props)}

              <br />
              <br />

              {/* Render the fields to capture the pipeline configuration for the respective continuous integration configuration */}
              {this.renderPipelineConfigurationSection()}

              <br />
              <br />

              {/* Render the fields to capture the channel configuration for the respective continuous integration configuration */}
              {this.renderChannelConfigurationSection()}

              <br />
              <br />

              {/* Render the dynamic fields to capture the metadata about the continuous integration configuration */}
              <MetadataForm
                metadata={props.values}
                title={t(
                  "devops.ci.add.form.metadata.section.title"
                ).toUpperCase()}
                subtitle={t("devops.ci.add.form.metadata.section.subtitle")}
              />

              <br />
              <Divider />
              <br />

              {/* Add button */}
              <FikButton
                type="submit"
                icon={getIcon("devops.integration.add.icon")}
                label={
                  mode === "add"
                    ? t("devops.ci.actions.add")
                    : t("devops.ci.actions.update")
                }
                loading={props.isSubmitting}
                disabled={props.isSubmitting}
                basic
                primary
              />

              {/* Clear button */}
              <FikButton
                type="reset"
                icon={getIcon("clear.icon")}
                label={t("clearForm")}
                onClick={props.handleReset}
                color="purple"
                basic
              />

              {/* Close button */}
              <FikButton
                icon={getIcon("close.icon")}
                label={t("close")}
                as={NavLink}
                exact
                to="/devops/integration"
                floated="right"
                basic
                secondary
              />
            </Form>
          )}
        </Formik>
      </Container>
    );
  }

  /**
   * This method renders the fields to capture the basic details for the continuous integration configuration.
   */
  renderBasicDetails = () => {
    const { t } = this.props;

    // Basic details for the continuous integration
    return (
      <Panel
        options={{
          title: t("devops.ci.add.form.details.section.title").toUpperCase(),
          subtitle: t("devops.ci.add.form.details.section.subtitle"),
          icon: getIcon("info.icon"),
          content: (
            <Container>
              {/* Field for Continuous Integration name. */}
              <FikField
                id="devops-ci-field-name"
                name="name"
                label={t("devops.ci.field.name")}
                placeholder={t(
                  "devops.ci.add.form.details.section.field.name.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
                autoFocus
              />

              {/* Field for continuous integration configuration description. */}
              <FikField
                id="devops-ci-field-description"
                name="description"
                label={t("devops.ci.field.description")}
                placeholder={t(
                  "devops.ci.add.form.details.section.field.description.placeholder"
                )}
                rows={5}
                component={FikTextArea}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the API details for the continuous integration tool.
   */
  renderApiConfigurationSection = formikProps => {
    const { t } = this.props;

    // API Configuration section for the continuous integration tool
    return (
      <Panel
        options={{
          title: t(
            "devops.ci.add.form.apiConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t("devops.ci.add.form.apiConfiguration.section.subtitle"),
          icon: getIcon("api.icon"),
          content: (
            <Container>
              {/* Field for continuous integration system's account name. */}
              <FikField
                id="devops-ci-field-apiConfiguration-accountName"
                name="apiConfiguration.accountName"
                label={t("devops.ci.field.apiConfiguration.accountName")}
                placeholder={t(
                  "devops.ci.add.form.apiConfiguration.section.field.accountName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for continuous integration system's user-name. */}
              <FikField
                id="devops-ci-field-apiConfiguration-username"
                name="apiConfiguration.userName"
                label={t("devops.ci.field.apiConfiguration.username")}
                placeholder={t(
                  "devops.ci.add.form.apiConfiguration.section.field.username.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for continuous integration system's user's personal access token. */}
              <FikField
                type="password"
                id="devops-ci-field-apiConfiguration-personalAccessToken"
                name="apiConfiguration.personalAccessToken"
                label={t(
                  "devops.ci.field.apiConfiguration.personalAccessToken"
                )}
                placeholder={t(
                  "devops.ci.add.form.apiConfiguration.section.field.personalAccessToken.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              <br />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("api.endpoint.icon")} />
                  {t("default.api.endpoint.add.form.title").toUpperCase()}
                </Header>
              </Divider>

              <ApiEndpointForm
                path="apiConfiguration.endpoints"
                displayHeader={false}
                endpoints={formikProps.values}
              />

              <br />
              <br />
              <Divider horizontal>
                <Header as="h5">
                  <Icon name={getIcon("api.metadata.icon")} />
                  {t("api.metadata.add.form.title").toUpperCase()}
                </Header>
              </Divider>

              {/* Render the dynamic fields to capture the metadata about the API Configuration */}
              <MetadataForm
                resourceName="api"
                path="apiConfiguration.metadata"
                metadata={formikProps.values}
                displayHeader={false}
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the pipeline configuration details.
   */
  renderPipelineConfigurationSection = () => {
    const { t } = this.props;

    // Pipeline configuration section
    return (
      <Panel
        options={{
          title: t(
            "devops.ci.add.form.pipelineConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t(
            "devops.ci.add.form.pipelineConfiguration.section.subtitle"
          ),
          icon: getIcon("pipeline.icon"),
          content: (
            <Container>
              {/* Field for SonarQube url. */}
              <FikField
                id="devops-ci-field-pipelineConfiguration-sonarConfiguration-endpoint"
                name="pipelineConfiguration.sonarConfiguration.endpoint"
                label={t(
                  "devops.ci.field.pipelineConfiguration.sonarConfiguration.endpoint"
                )}
                placeholder={t(
                  "devops.ci.add.form.pipelineConfiguration.section.sonarConfiguration.field.endpoint.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for SonarQube username. */}
              <FikField
                id="devops-ci-field-pipelineConfiguration-sonarConfiguration-username"
                name="pipelineConfiguration.sonarConfiguration.userName"
                label={t(
                  "devops.ci.field.pipelineConfiguration.sonarConfiguration.username"
                )}
                placeholder={t(
                  "devops.ci.add.form.pipelineConfiguration.section.sonarConfiguration.field.username.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />

              {/* Field for SonarQube / SonarCloud user password / PAT (personal access token). */}
              <FikField
                id="devops-ci-field-pipelineConfiguration-sonarConfiguration-personalAccessToken"
                name="pipelineConfiguration.sonarConfiguration.personalAccessToken"
                type="password"
                label={t(
                  "devops.ci.field.pipelineConfiguration.sonarConfiguration.personalAccessToken"
                )}
                placeholder={t(
                  "devops.ci.add.form.pipelineConfiguration.section.sonarConfiguration.field.personalAccessToken.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method renders the fields to capture the channel configuration for the continuous integration tool.
   */
  renderChannelConfigurationSection = () => {
    const { t } = this.props;

    // Channel configuration section for the continuous integration tool.
    return (
      <Panel
        options={{
          title: t(
            "devops.ci.add.form.channelConfiguration.section.title"
          ).toUpperCase(),
          subtitle: t(
            "devops.ci.add.form.channelConfiguration.section.subtitle"
          ),
          icon: getIcon("channel.icon"),
          content: (
            <Container>
              {/* Field for channel name for the respective continuous integration tool. */}
              <FikField
                id="devops-ci-field-channelConfiguration-channelName"
                name="channelConfiguration.channelName"
                label={t("devops.ci.field.channelConfiguration.channelName")}
                placeholder={t(
                  "devops.ci.add.form.channelConfiguration.section.field.channelName.placeholder"
                )}
                autoComplete="off"
                component={FikInput}
                required
              />
            </Container>
          )
        }}
      />
    );
  };

  /**
   * This method gets called whenever a user submits the data displayed in the form. As this component
   * provides abstractions, it is the responsibility of the components using this component to provide
   * their own submit handlers via 'onSubmit' property. The handler would be called by this abstract
   * component whenever a user submits the form.
   */
  onFormSubmit = (formValues, formikApi) => {
    if (this.props.onSubmit) {
      const clonedFormValues = _.clone(formValues);
      const { metadata, apiConfiguration } = clonedFormValues;
      if (metadata && metadata.length >= 0) {
        clonedFormValues.metadata = _.mapValues(
          _.keyBy(metadata, "key"),
          "value"
        );
      }
      if (
        apiConfiguration &&
        apiConfiguration.metadata &&
        apiConfiguration.metadata.length >= 0
      ) {
        clonedFormValues.apiConfiguration.metadata = _.mapValues(
          _.keyBy(apiConfiguration.metadata, "key"),
          "value"
        );
      }
      this.props.onSubmit(clonedFormValues);
    } else {
      console.debug(
        "No onSubmit handler provided by the consuming application. Hence not submitting..."
      );
    }
  };
}

/**
 * This method extracts out the key information from the redux state and makes it available as component
 * props.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {};
};

export default connect(
  mapStateToProps,
  {}
)(withTranslation()(AbstractContinuousIntegrationForm));
