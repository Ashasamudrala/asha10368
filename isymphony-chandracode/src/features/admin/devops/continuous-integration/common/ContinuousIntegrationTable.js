/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container, Pagination, Table, Button, List } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import getIcon from "../../../../../config/icons";
import { FikLabeledButton } from "../../../../../components/common/formik-wrappers";
import { configurePopup, renderLoader } from "../../../../../utils";
import { getAllDevOpsIntegrationConfigurationsAction } from "../../../../../actions/devopsIntegrationConfigurationActions";

/**
 * This component is responsible to display the continuous integration configurations in a tabular fashion.
 *
 * @author Chandra Veerapaneni
 */
class ContinuousIntegrationTable extends React.Component {
  // Local state
  state = { dataLoaded: false };

  /**
   * This method gets called as soon as the component is loaded into the DOM.
   */
  componentDidMount() {
    this.loadDevOpsIntegrationConfigurations(0, 20);
  }

  /**
   * This method is responsible to render the table on the UI.
   */
  render() {
    return <Container fluid>{this.renderTable()}</Container>;
  }

  /**
   * This method renders the table.
   */
  renderTable = () => {
    const { dataLoaded } = this.state;
    const { t } = this.props;
    if (!dataLoaded) {
      return renderLoader(t("devops.ci.loading.message"));
    }

    const { devopsIntegrationConfigurations } = this.props;
    return (
      <Table celled padded>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={3}>
              {t("devops.ci.field.name").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={6}>
              {t("devops.ci.field.description").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={5}>
              {t("devops.ci.field.metadata").toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width={2}>
              {t("devops.ci.field.actions").toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {devopsIntegrationConfigurations.map(
            devopsIntegrationConfiguration => {
              return (
                <Table.Row key={devopsIntegrationConfiguration.id}>
                  {/* DevOps Integration Configuration Name */}
                  {configurePopup(
                    <Table.Cell>
                      <strong>{devopsIntegrationConfiguration.name}</strong>
                    </Table.Cell>,
                    t("devops.ci.field.name"),
                    devopsIntegrationConfiguration.name
                  )}

                  {/* DevOps Integration Configuration Description */}
                  {configurePopup(
                    <Table.Cell>
                      {devopsIntegrationConfiguration.description}
                    </Table.Cell>,
                    t("devops.ci.field.description"),
                    devopsIntegrationConfiguration.description
                  )}

                  {/* Metadata */}
                  <Table.Cell>
                    <List horizontal>
                      {Object.keys(devopsIntegrationConfiguration.metadata).map(
                        key => {
                          return (
                            <List.Item key={key}>
                              <List.Content>
                                <FikLabeledButton
                                  size="tiny"
                                  key={key}
                                  content={key}
                                  label={{
                                    basic: true,
                                    content:
                                      devopsIntegrationConfiguration.metadata[
                                        key
                                      ]
                                  }}
                                />
                              </List.Content>
                            </List.Item>
                          );
                        }
                      )}
                    </List>
                  </Table.Cell>

                  {/* Actions */}
                  <Table.Cell textAlign="center">
                    <Button
                      as={NavLink}
                      icon={getIcon("edit.icon")}
                      size="tiny"
                      exact
                      to={`/devops/integration/edit/${devopsIntegrationConfiguration.id}`}
                      color="blue"
                      basic
                    />
                  </Table.Cell>
                </Table.Row>
              );
            }
          )}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="5">
              {this.renderPaginationControl()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    );
  };

  /**
   * This method renders the pagination control in the UI.
   */
  renderPaginationControl = () => {
    const { pagination } = this.props;
    if (pagination && pagination.totalPages > 1) {
      return (
        <Pagination
          size="tiny"
          boundaryRange={0}
          defaultActivePage={1}
          ellipsisItem={null}
          firstItem={null}
          lastItem={null}
          siblingRange={1}
          totalPages={pagination.totalPages}
          floated="right"
          onPageChange={(e, data) =>
            this.loadDevOpsIntegrationConfigurations(
              data.activePage,
              pagination.pageSize
            )
          }
        />
      );
    }
  };

  /**
   * This method attempts to load the DevOps integration configurations based on the provided pagination settings.
   */
  loadDevOpsIntegrationConfigurations = (pageNumber, pageSize) => {
    this.props.getAllDevOpsIntegrationConfigurationsAction({
      params: {
        pageNumber: pageNumber - 1,
        pageSize: pageSize
      },
      onSuccess: () => {
        this.setState({ dataLoaded: true });
      }
    });
  };
}

/**
 * This method extracts the required information from the redux store and makes it available
 * as the component properties.
 *
 * @param {*} state As-is state in the redux store.
 */
const mapStateToProps = state => {
  return {
    devopsIntegrationConfigurations: Object.values(
      state.devopsIntegrationConfigurations.content
    ),
    pagination: state.devopsIntegrationConfigurations.pagination
  };
};

export default connect(mapStateToProps, {
  getAllDevOpsIntegrationConfigurationsAction
})(withTranslation()(ContinuousIntegrationTable));
