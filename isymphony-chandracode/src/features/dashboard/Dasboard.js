/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from "react";
import { Container, Segment } from "semantic-ui-react";

/**
 * Dashboard component that gets displayed when the users navigate to the root url.
 *
 * @author Chandra Veerapaneni
 */
class Dashboard extends React.Component {
  /**
   * This method gets called when rendering the component on the screen.
   */
  render() {
    return (
      <Container>
        <Segment color="grey" raised>
          Dashboard data goes here!
        </Segment>
      </Container>
    );
  }
}

export default Dashboard;
