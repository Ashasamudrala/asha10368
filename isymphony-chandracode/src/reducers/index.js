/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import projectReducer from "./projectReducer";
import acceleratorCategoryReducer from "./acceleratorCategoryReducer";
import applicationReducer from "./applicationReducer";
import repositoryReducer from "./repositoryReducer";
import devopsIntegrationConfigurationReducer from "./devopsIntegrationConfigurationReducer";
import devopsDeliveryConfigurationReducer from "./devopsDeliveryConfigurationReducer";

/**
 * Combine the reducers in the system.
 *
 * @author Chandra Veerapaneni
 */
export default combineReducers({
  form: formReducer,
  application: applicationReducer,
  projects: projectReducer,
  categories: acceleratorCategoryReducer,
  repositories: repositoryReducer,
  devopsIntegrationConfigurations: devopsIntegrationConfigurationReducer,
  devopsDeliveryConfigurations: devopsDeliveryConfigurationReducer
});
