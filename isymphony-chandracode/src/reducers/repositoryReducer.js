/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import actionTypes from "../actions/actionTypes";

/**
 * Repository reducer that deals with the different actions pertaining to repositories.
 *
 * @author Chandra Veerapaneni
 */
const INITIAL_STATE = {
  content: {},
  pagination: {}
};
export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case actionTypes.CREATE_REPOSITORY:
    case actionTypes.GET_REPOSITORY:
    case actionTypes.UPDATE_REPOSITORY: {
      return { ...state, content: { [payload.id]: payload } };
    }
    case actionTypes.DELETE_REPOSITORY: {
      return _.omit(state.content, payload);
    }
    case actionTypes.GET_ALL_REPOSITORIES: {
      const content = { ..._.mapKeys(payload.content, "id") };
      const pagination = {
        totalElements: payload.totalElements,
        totalPages: payload.totalPages,
        pageNumber: payload.pageable.pageNumber,
        pageSize: payload.pageable.pageSize,
        last: payload.last,
        first: payload.first,
        empty: payload.empty
      };
      return { ...state, content: content, pagination: pagination };
    }
    default:
      return state;
  }
};
