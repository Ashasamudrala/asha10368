/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import actionTypes from "../actions/actionTypes";

/**
 * Project reducer that deals with the different actions pertaining to projects, jobs, etc.
 *
 * @author Chandra Veerapaneni
 */
const INITIAL_STATE = {
  content: {},
  pagination: {},
  jobs: {
    content: {},
    pagination: {}
  }
};
export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case actionTypes.CREATE_PROJECT:
    case actionTypes.GET_PROJECT:
    case actionTypes.UPDATE_PROJECT: {
      return { ...state, content: { [payload.id]: payload } };
    }
    case actionTypes.DELETE_PROJECT: {
      return _.omit(state.content, payload);
    }
    case actionTypes.GET_MY_PROJECTS: {
      const content = { ..._.mapKeys(payload.content, "id") };
      const pagination = {
        totalElements: payload.totalElements,
        totalPages: payload.totalPages,
        pageNumber: payload.pageable.pageNumber,
        pageSize: payload.pageable.pageSize,
        last: payload.last,
        first: payload.first,
        empty: payload.empty
      };
      return { ...state, content: content, pagination: pagination };
    }
    case actionTypes.CREATE_JOB:
    case actionTypes.GET_JOB:
    case actionTypes.UPDATE_JOB: {
      return { ...state, jobs: { content: { [payload.id]: payload } } };
    }
    case actionTypes.DELETE_JOB: {
      return _.omit(state.jobs.content, payload);
    }
    case actionTypes.GET_MY_JOBS: {
      const content = { ..._.mapKeys(payload.content, "id") };
      const pagination = {
        totalElements: payload.totalElements,
        totalPages: payload.totalPages,
        pageNumber: payload.pageable.pageNumber,
        pageSize: payload.pageable.pageSize,
        last: payload.last,
        first: payload.first,
        empty: payload.empty
      };
      return { ...state, jobs: { content: content, pagination: pagination } };
    }
    default:
      return state;
  }
};
