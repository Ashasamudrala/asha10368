/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import actionTypes from "../actions/actionTypes";

/**
 * Core reducer that deals with the different actions pertaining to core functionality (authentication,
 * platforms, frameworks, etc.)
 *
 * @author Chandra Veerapaneni
 */

// Initial state
const INITIAL_STATE = {
  errors: {},
  auth: {
    isAuthenticated: null,
    user: null
  },
  platforms: {
    content: {},
    names: {},
    versions: {},
    pagination: {}
  },
  frameworks: {
    content: {},
    names: {},
    versions: {},
    modules: {
      versions: {}
    },
    pagination: {}
  }
};
export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case actionTypes.LOGIN: {
      return { ...state, auth: { isAuthenticated: true, user: payload } };
    }
    case actionTypes.LOGOUT: {
      return { ...state, auth: { isAuthenticated: false, user: null } };
    }
    case actionTypes.RAISE_ERROR: {
      return { ...state, errors: { [payload.key]: payload } };
    }
    case actionTypes.CLEAR_ERROR: {
      return _.omit(state.errors, payload);
    }
    case actionTypes.CLEAR_ALL_ERRORS: {
      return { ...state, errors: {} };
    }
    case actionTypes.CREATE_PLATFORM:
    case actionTypes.GET_PLATFORM:
    case actionTypes.UPDATE_PLATFORM: {
      const platforms = {
        content: { [payload.id]: payload },
        names: { [payload.id]: { text: payload.name, value: payload.id } },
        versions: {
          [payload.version]: { text: payload.version, value: payload.version }
        }
      };
      const frameworks = {
        content: { ..._.mapKeys(payload.frameworks, "id") }
      };
      return { ...state, platforms: platforms, frameworks: frameworks };
    }
    case actionTypes.DELETE_PLATFORM: {
      // Even if a platform is deleted, we should not be removing the frameworks
      // from the state because the same frameworks might be used by other platforms.
      return _.omit(state.platforms.content, payload);
    }
    case actionTypes.GET_ALL_PLATFORMS: {
      const platforms = {
        content: { ..._.mapKeys(payload.content, "id") },
        pagination: {
          totalElements: payload.totalElements,
          totalPages: payload.totalPages,
          pageNumber: payload.pageable.pageNumber,
          pageSize: payload.pageable.pageSize,
          last: payload.last,
          first: payload.first,
          empty: payload.empty
        },
        names: {
          ..._.mapKeys(
            _.map(payload.content, platform => {
              return { text: platform.name, value: platform.id };
            }),
            "value"
          )
        },
        versions: {
          ..._.mapKeys(
            _.map(payload.content, platform => {
              return { text: platform.version, value: platform.version };
            }),
            "value"
          )
        }
      };
      const allFrameworks = _.flatten(
        _.map(_.map(payload.content, "frameworks"), o => _.values(o))
      );
      const frameworks = {
        content: {
          ..._.mapKeys(allFrameworks, "id")
        },
        names: {
          ..._.mapKeys(
            _.map(allFrameworks, framework => {
              return { text: framework.name, value: framework.id };
            }),
            "value"
          )
        } /*,
                versions: {
                    ..._.mapKeys(_.map(_.flatMap(_.map(payload.content, 'frameworks')), framework => {
                        return { text: framework.version, value: framework.id };
                    }), 'value')
                },
                modules: {
                    versions: {
                        ..._.mapKeys(_.map(_.flatMap(_.map(payload.content, 'frameworks')), fw => {
                            return { text: fw.modules[0].version, value: fw.modules[0].version };
                        }), 'value')
                    }
                }*/
      };
      return { ...state, platforms: platforms, frameworks: frameworks };
    }
    case actionTypes.CREATE_FRAMEWORK:
    case actionTypes.GET_FRAMEWORK:
    case actionTypes.UPDATE_FRAMEWORK: {
      const frameworks = {
        content: { [payload.id]: payload },
        names: { [payload.id]: { text: payload.name, value: payload.id } }
      };
      return { ...state, frameworks: frameworks };
    }
    case actionTypes.DELETE_FRAMEWORK: {
      // What if the framework is used by multiple platforms?
      // TODO: Need to think about this use-case.
      return _.omit(state.frameworks.content, payload);
    }
    case actionTypes.GET_ALL_FRAMEWORKS: {
      const frameworks = {
        content: { ..._.mapKeys(payload.content, "id") },
        pagination: {
          totalElements: payload.totalElements,
          totalPages: payload.totalPages,
          pageNumber: payload.pageable.pageNumber,
          pageSize: payload.pageable.pageSize,
          last: payload.last,
          first: payload.first,
          empty: payload.empty
        },
        names: {
          ..._.mapKeys(
            _.map(payload.content, framework => {
              return { text: framework.name, value: framework.id };
            }),
            "value"
          )
        }
      };
      return { ...state, frameworks: frameworks };
    }
    default:
      return state;
  }
};
