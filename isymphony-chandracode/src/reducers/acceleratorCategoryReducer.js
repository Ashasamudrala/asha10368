/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import _ from "lodash";

import actionTypes from "../actions/actionTypes";

/**
 * Project reducer that deals with the different actions pertaining to categories.
 *
 * @author Chandra Veerapaneni
 */
const INITIAL_STATE = {
  content: {},
  categoryOptions: [],
  pagination: {},
  accelerators: {
    content: {},
    pagination: {}
  }
};
export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case actionTypes.CREATE_CATEGORY:
    case actionTypes.GET_CATEGORY:
    case actionTypes.UPDATE_CATEGORY: {
      const clonedOptions = _.clone(state.categoryOptions);
      _.remove(clonedOptions, { value: payload.id });
      clonedOptions.push({
        text: payload.name,
        value: payload.id,
        key: payload.id
      });
      return {
        ...state,
        content: { [payload.id]: payload },
        categoryOptions: { ...clonedOptions },
        accelerators: {
          content: {
            ...state.accelerators.content,
            ..._.mapKeys(
              _.flatten(
                _.map(_.map(payload.content, "accelerators"), o => _.values(o))
              ),
              "id"
            )
          }
        }
      };
    }
    case actionTypes.DELETE_CATEGORY: {
      // Should we delete the accelerators belonging to that category?
      // TODO: Fix this
      return _.omit(state.content, payload);
    }
    case actionTypes.GET_ALL_CATEGORIES: {
      const accelerators = _.flatten(
        _.map(_.map(payload.content, "accelerators"), o => _.values(o))
      );
      return {
        ...state,
        content: { ..._.mapKeys(payload.content, "id") },
        categoryOptions: _.orderBy(
          payload.content.map(category => {
            return {
              text: category.name,
              value: category.id,
              key: category.id
            };
          }),
          "text"
        ),
        pagination: {
          totalElements: payload.totalElements,
          totalPages: payload.totalPages,
          pageNumber: payload.pageable.pageNumber,
          pageSize: payload.pageable.pageSize,
          last: payload.last,
          first: payload.first,
          empty: payload.empty
        },
        accelerators: {
          content: { ..._.mapKeys(accelerators, "id") }
        }
      };
    }
    default:
      return state;
  }
};
