/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import iconConfigurations from "./iconConfigurations.json";

// All the icon configurations are exported.
const getIcon = name => {
  return iconConfigurations[name];
};

export default getIcon;
