/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// Import english messages
import messages_en from "./en/messages_en.json";

// Translations
const resources = {
  en: {
    translation: messages_en
  }
};

i18n
  // passes i18n down to react-i18next
  .use(initReactI18next)
  .init({
    resources,
    lng: "en",
    keySeparator: false,
    interpolation: {
      // react already safes from xss
      escapeValue: false
    }
  });

export default i18n;
