/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from "react";
import { Router, Route } from "react-router-dom";
import { Container } from "semantic-ui-react";
import { connect } from "react-redux";

import AcceleratorsHome from "../features/admin/accelerators/AcceleratorsHome";
import AppFooter from "./common/footer/AppFooter";
import AppHeader from "./common/header/AppHeader";
import CategoriesHome from "../features/admin/categories/CategoriesHome";
import ContinuousDeliveryHome from "../features/admin/devops/continuous-delivery/ContinuousDeliveryHome";
import ContinuousIntegrationHome from "../features/admin/devops/continuous-integration/ContinuousIntegrationHome";
import Dashboard from "../features/dashboard/Dasboard";
import EditAcceleratorForm from "../features/admin/accelerators/forms/EditAcceleratorForm";
import EditCategoryForm from "../features/admin/categories/forms/EditCategoryForm";
import EditContinuousDeliveryForm from "../features/admin/devops/continuous-delivery/forms/EditContinuousDeliveryForm";
import EditContinuousIntegrationForm from "../features/admin/devops/continuous-integration/forms/EditContinuousIntegrationForm";
import EditFrameworkForm from "../features/admin/frameworks/forms/EditFrameworkForm";
import EditPlatformForm from "../features/admin/platforms/forms/EditPlatformForm";
import EditProjectForm from "../features/projects/forms/EditProjectForm";
import EditRepositoryForm from "../features/admin/devops/repositories/forms/EditRepositoryForm";
import Errors from "../components/common/errors/Errors";
import FrameworkHome from "../features/admin/frameworks/FrameworkHome";
import Home from "./common/home/Home";
import LoginForm from "../features/auth/forms/LoginForm";
import MyProjectsHome from "../features/projects/MyProjectsHome";
import NewAcceleratorForm from "../features/admin/accelerators/forms/NewAcceleratorForm";
import NewCategoryForm from "../features/admin/categories/forms/NewCategoryForm";
import NewContinuousDeliveryForm from "../features/admin/devops/continuous-delivery/forms/NewContinuousDeliveryForm";
import NewContinuousIntegrationForm from "../features/admin/devops/continuous-integration/forms/NewContinuousIntegrationForm";
import NewFrameworkForm from "../features/admin/frameworks/forms/NewFrameworkForm";
import NewPlatformForm from "../features/admin/platforms/forms/NewPlatformForm";
import NewProjectForm from "../features/projects/forms/NewProjectForm";
import NewRepositoryForm from "../features/admin/devops/repositories/forms/NewRepositoryForm";
import PlatformHome from "../features/admin/platforms/PlatformHome";
import RepositoryHome from "../features/admin/devops/repositories/RepositoryHome";
import history from "../history";
import { getAllPlatformsAction } from "../actions/coreActions";

/**
 * Main application class.
 *
 * @author Chandra Veerapaneni
 */
class App extends React.Component {
  /**
   * Lifecycle method that gets called after this react component got rendered on the screen.
   */
  componentDidMount() {
    this.props.getAllPlatformsAction();
  }

  /**
   * This method gets called to render the main application component.
   */
  render() {
    return (
      <Container>
        <Router history={history}>
          <AppHeader />
          <Errors errors={this.props.errors} />
          <Route exact path="/" component={Home} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/login" component={LoginForm} />
          <Route exact path="/projects" component={MyProjectsHome} />
          <Route exact path="/projects/new" component={NewProjectForm} />
          <Route exact path="/projects/edit/:id" component={EditProjectForm} />
          <Route exact path="/categories" component={CategoriesHome} />
          <Route exact path="/categories/new" component={NewCategoryForm} />
          <Route
            exact
            path="/categories/edit/:id"
            component={EditCategoryForm}
          />
          <Route exact path="/accelerators" component={AcceleratorsHome} />
          <Route
            exact
            path="/categories/:categoryId/accelerators/new"
            component={NewAcceleratorForm}
          />
          <Route
            exact
            path="/categories/:categoryId/accelerators/edit/:acceleratorId"
            component={EditAcceleratorForm}
          />
          <Route exact path="/platforms" component={PlatformHome} />
          <Route exact path="/platforms/new" component={NewPlatformForm} />
          <Route
            exact
            path="/platforms/edit/:id"
            component={EditPlatformForm}
          />
          <Route exact path="/frameworks" component={FrameworkHome} />
          <Route
            exact
            path="/platforms/:platformId/frameworks/new"
            component={NewFrameworkForm}
          />
          <Route
            exact
            path="/platforms/:platformId/frameworks/edit/:frameworkId"
            component={EditFrameworkForm}
          />
          <Route exact path="/devops/repositories" component={RepositoryHome} />
          <Route
            exact
            path="/devops/repositories/new"
            component={NewRepositoryForm}
          />
          <Route
            exact
            path="/devops/repositories/edit/:id"
            component={EditRepositoryForm}
          />
          <Route
            exact
            path="/devops/integration"
            component={ContinuousIntegrationHome}
          />
          <Route
            exact
            path="/devops/integration/new"
            component={NewContinuousIntegrationForm}
          />
          <Route
            exact
            path="/devops/integration/edit/:id"
            component={EditContinuousIntegrationForm}
          />
          <Route
            exact
            path="/devops/delivery"
            component={ContinuousDeliveryHome}
          />
          <Route
            exact
            path="/devops/delivery/new"
            component={NewContinuousDeliveryForm}
          />
          <Route
            exact
            path="/devops/delivery/edit/:id"
            component={EditContinuousDeliveryForm}
          />
          <AppFooter />
        </Router>
      </Container>
    );
  }
}

/**
 * This method maps the state to the component properties.
 *
 * @param {*} state State from which the data that is required for this component needs to be extracted out as component properties.
 */
const mapStateToProps = state => {
  return {
    errors:
      state.application && state.application.errors
        ? Object.values(state.application.errors)
        : []
  };
};

export default connect(mapStateToProps, {
  getAllPlatformsAction
})(App);
