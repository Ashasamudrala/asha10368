/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Container } from "semantic-ui-react";

/**
 * Application footer.
 *
 * @author Chandra Veerapaneni
 */
class AppFooter extends React.Component {
  /**
   * This method is responsible to render the footer on the screen.
   */
  render() {
    return <Container></Container>;
  }
}

export default AppFooter;
