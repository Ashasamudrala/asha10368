/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Message, Table, Button } from "semantic-ui-react";
import { Field as FikField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import Panel from "../../panel/Panel";
import getIcon from "../../../../config/icons";
import { FikButton, FikInput } from "../../formik-wrappers";

/**
 * Component that is responsible to render API Endpoints that are generally expressed as key-values pairs.
 *
 * @author Chandra Veerapaneni
 */
class ApiEndpointForm extends React.Component {
  // Empty row that will be inserted when the users click on "Add Metadata" button.
  static EMPTY_ROW = {
    name: "",
    endpoint: ""
  };
  static DEFAULT_RESOURCE_NAME = "default";

  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { endpoints, path = "endpoints" } = this.props;

    return (
      // Render the dynamic fields to capture the API Endpoints. This can be used in different areas of the application.
      // For example: Repositories, Continuous Integration Configurations, etc.
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderApiEndpointsForm(
            _.get(endpoints, path),
            arrayHelpers
          );
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the endpoints pertaining to the resource.
   */
  renderApiEndpointsForm = (endpoints, arrayHelpers) => {
    const {
      t,
      resourceName = "default",
      title = t(
        `${resourceName}.api.endpoint.add.form.title`,
        t(
          `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.add.form.title`
        )
      ),
      subtitle = t(
        `${resourceName}.api.endpoint.add.form.subtitle`,
        t(
          `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.add.form.subtitle`
        )
      ),
      icon = getIcon("api.endpoint.icon"),
      displayHeader = true
    } = this.props;

    // API Endpoints for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {endpoints && endpoints.length > 0
                  ? this.renderApiEndpointsContent(endpoints, arrayHelpers)
                  : this.renderNoApiEndpointsContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {endpoints && endpoints.length > 0
            ? this.renderApiEndpointsContent(endpoints, arrayHelpers)
            : this.renderNoApiEndpointsContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a segment control that displays the configured endpoints for this resource.
   */
  renderApiEndpointsContent = (endpoints, arrayHelpers) => {
    const { t, resourceName = "default", path = "endpoints" } = this.props;

    const length = endpoints.length;
    return (
      <Table celled compact>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="4">
              {t(
                `${resourceName}.api.endpoint.field.name`,
                t(
                  `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.field.name`
                )
              ).toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="10">
              {t(
                `${resourceName}.api.endpoint.field.endpoint`,
                t(
                  `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.field.endpoint`
                )
              ).toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="2">
              {t(
                `${resourceName}.api.endpoint.field.actions`,
                t(
                  `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.field.actions`
                )
              ).toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {endpoints.map((endpoint, index) => (
            <Table.Row key={index}>
              <Table.Cell>
                {/* Field for endpoint name. */}
                <FikField
                  id={`${resourceName}-api-endpoint-field-name-${index}`}
                  name={`${path}.${index}.name`}
                  placeholder={t(
                    `${resourceName}.api.endpoint.field.name.placeholder`,
                    t(
                      `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.field.name.placeholder`
                    )
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Table.Cell>
              <Table.Cell>
                {/* Field for endpoint. */}
                <FikField
                  id={`${resourceName}-api-endpoint-field-endpoint-${index}`}
                  name={`${path}.${index}.endpoint`}
                  placeholder={t(
                    `${resourceName}.api.endpoint.field.endpoint.placeholder`,
                    t(
                      `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.field.endpoint.placeholder`
                    )
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Table.Cell>
              <Table.Cell textAlign="center">
                {index === length - 1 && (
                  <Button
                    id={`${resourceName}-api-endpoint-add-button-${index}`}
                    type="button"
                    size="tiny"
                    icon={getIcon("api.endpoint.add.icon")}
                    onClick={e =>
                      arrayHelpers.insert(index + 1, ApiEndpointForm.EMPTY_ROW)
                    }
                    color="blue"
                    basic
                  />
                )}
                &nbsp;
                <Button
                  id={`${resourceName}-api-endpoint-delete-button-${index}`}
                  type="button"
                  size="tiny"
                  icon={getIcon("delete.icon")}
                  onClick={e => arrayHelpers.remove(index)}
                  color="red"
                  basic
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    );
  };

  /**
   * This method renders a segment control indicating the absence of API endpoints for this resource.
   */
  renderNoApiEndpointsContent = arrayHelpers => {
    const { t, resourceName = "default" } = this.props;

    return (
      <Message align="center">
        <Message.Header>
          {t(
            `${resourceName}.api.endpoint.empty.title`,
            t(
              `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.empty.title`
            )
          )}
        </Message.Header>
        <Message.Content>
          <p>
            {t(
              `${resourceName}.api.endpoint.empty.message`,
              t(
                `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.empty.message`
              )
            )}
          </p>
          <FikButton
            id={`${resourceName}-api-endpoint-add`}
            type="button"
            icon={getIcon("api.endpoint.add.icon")}
            label={t(
              `${resourceName}.api.endpoint.actions.add`,
              t(
                `${ApiEndpointForm.DEFAULT_RESOURCE_NAME}.api.endpoint.actions.add`
              )
            )}
            onClick={e => arrayHelpers.insert(0, ApiEndpointForm.EMPTY_ROW)}
            color="blue"
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(ApiEndpointForm);
