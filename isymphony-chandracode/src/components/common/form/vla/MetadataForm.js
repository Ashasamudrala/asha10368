/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import { Container, Message, Table, Button } from "semantic-ui-react";
import { FastField as FikFastField, FieldArray as FikFieldArray } from "formik";
import { withTranslation } from "react-i18next";

import Panel from "../../panel/Panel";
import getIcon from "../../../../config/icons";
import { FikButton, FikInput } from "../../formik-wrappers";

/**
 * Component that is responsible to render metadata that is generally expressed as key-value pairs.
 *
 * @author Chandra Veerapaneni
 */
class MetadataForm extends React.Component {
  // Empty row that will be inserted when the users click on "Add Metadata" button.
  static EMPTY_ROW = {
    key: "",
    value: ""
  };
  static DEFAULT_RESOURCE_NAME = "default";

  /**
   * This method is responsible to render the UI.
   */
  render() {
    const { metadata, path = "metadata" } = this.props;

    return (
      // Render the dynamic fields to capture the metadata about the continuous integration configuration
      <FikFieldArray
        name={path}
        render={arrayHelpers => {
          return this.renderMetadataForm(_.get(metadata, path), arrayHelpers);
        }}
      />
    );
  }

  /**
   * This method renders the fields to capture the metadata pertaining to the resource.
   */
  renderMetadataForm = (metadata, arrayHelpers) => {
    const {
      t,
      resourceName = "default",
      title = t(
        `${resourceName}.metadata.add.form.title`,
        t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.add.form.title`)
      ),
      subtitle = t(
        `${resourceName}.metadata.add.form.subtitle`,
        t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.add.form.subtitle`)
      ),
      icon = getIcon("metadata.icon"),
      displayHeader = true
    } = this.props;

    // Metadata for the form.
    if (displayHeader) {
      return (
        <Panel
          options={{
            title: title.toUpperCase(),
            subtitle: subtitle,
            icon: icon,
            content: (
              <Container fluid>
                {metadata && metadata.length > 0
                  ? this.renderMetadataContent(metadata, arrayHelpers)
                  : this.renderNoMetadataContent(arrayHelpers)}
              </Container>
            )
          }}
        />
      );
    } else {
      return (
        <Container fluid>
          {metadata && metadata.length > 0
            ? this.renderMetadataContent(metadata, arrayHelpers)
            : this.renderNoMetadataContent(arrayHelpers)}
        </Container>
      );
    }
  };

  /**
   * This method renders a segment control that displays the configured metadata for this resource.
   */
  renderMetadataContent = (metadata, arrayHelpers) => {
    const { t, resourceName = "default", path = "metadata" } = this.props;

    const length = metadata.length;
    return (
      <Table celled compact>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="7">
              {t(
                `${resourceName}.metadata.field.key`,
                t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.field.key`)
              ).toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="7">
              {t(
                `${resourceName}.metadata.field.value`,
                t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.field.value`)
              ).toUpperCase()}
            </Table.HeaderCell>
            <Table.HeaderCell width="2">
              {t(
                `${resourceName}.metadata.field.actions`,
                t(
                  `${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.field.actions`
                )
              ).toUpperCase()}
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {metadata.map((member, index) => (
            <Table.Row key={index}>
              <Table.Cell>
                {/* Field for metadata key. */}
                <FikFastField
                  id={`${resourceName}-metadata-field-key-${index}`}
                  name={`${path}.${index}.key`}
                  placeholder={t(
                    `${resourceName}.metadata.field.key.placeholder`,
                    t(
                      `${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.field.key.placeholder`
                    )
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Table.Cell>
              <Table.Cell>
                {/* Field for metadata value. */}
                <FikFastField
                  id={`${resourceName}-metadata-field-value-${index}`}
                  name={`${path}.${index}.value`}
                  placeholder={t(
                    `${resourceName}.metadata.field.value.placeholder`,
                    t(
                      `${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.field.value.placeholder`
                    )
                  )}
                  autoComplete="off"
                  component={FikInput}
                />
              </Table.Cell>
              <Table.Cell textAlign="center">
                {index === length - 1 && (
                  <Button
                    id={`${resourceName}-metadata-add-button-${index}`}
                    type="button"
                    size="tiny"
                    icon={getIcon("metadata.add.icon")}
                    onClick={e =>
                      arrayHelpers.insert(index + 1, MetadataForm.EMPTY_ROW)
                    }
                    color="blue"
                    basic
                  />
                )}
                &nbsp;
                <Button
                  id={`${resourceName}-metadata-delete-button-${index}`}
                  type="button"
                  size="tiny"
                  icon={getIcon("delete.icon")}
                  onClick={e => arrayHelpers.remove(index)}
                  color="red"
                  basic
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    );
  };

  /**
   * This method renders a segment control indicating the absence of metadata for this resource.
   */
  renderNoMetadataContent = arrayHelpers => {
    const { t, resourceName = "default" } = this.props;

    return (
      <Message align="center">
        <Message.Header>
          {t(
            `${resourceName}.metadata.empty.title`,
            t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.empty.title`)
          )}
        </Message.Header>
        <Message.Content>
          <p>
            {t(
              `${resourceName}.metadata.empty.message`,
              t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.empty.message`)
            )}
          </p>
          <FikButton
            id={`${resourceName}-metadata-add`}
            type="button"
            icon={getIcon("metadata.add.icon")}
            label={t(
              `${resourceName}.metadata.actions.add`,
              t(`${MetadataForm.DEFAULT_RESOURCE_NAME}.metadata.actions.add`)
            )}
            onClick={e => arrayHelpers.insert(0, MetadataForm.EMPTY_ROW)}
            color="blue"
            basic
          />
        </Message.Content>
      </Message>
    );
  };
}

export default withTranslation()(MetadataForm);
