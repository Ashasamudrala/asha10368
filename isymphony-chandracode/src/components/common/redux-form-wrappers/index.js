/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from 'react';
import {
    Form,
    Input,
    Radio as RadioComponent,
    Checkbox as CheckboxComponent,
    Select as SelectComponent,
    TextArea,
    Dropdown,
    Label
} from 'semantic-ui-react';

/**
 * Wrapper class that wraps an input text field. The usage of this wrapper is limited to 'redux-form' Field component.
 * An example usage is shown below:
 * 
 * <Field name='title' label='Title' component={InputWrapper} required />
 * 
 * @param {*} param0 Object that has input field details.
 */
export const InputWrapper = ({ input,
    as,
    className,
    label,
    required,
    disabled,
    width,
    inline,
    meta: { touched, error },
    ...rest }) => {

    return (
        <Form.Field as={as}
            className={className}
            required={required}
            disabled={disabled}
            width={width}
            inline={inline}
            error={!!(touched && error)}>

            {label && <label>{label}</label>}
            <Input required={required} {...input} {...rest} />
            {touched && error ? (<Label basic color="red" pointing>{error}</Label>) : null}

        </Form.Field>
    );

};

/**
 * Wrapper class that wraps a Text-area widget. The usage of this wrapper is limited to 'redux-form' Field component.
 * An example usage is shown below:
 * 
 * <Field name='description' label='Description' component={TextAreaWrapper} required />
 * 
 * @param {*} param0 Object that has the text-area details. 
 */
export const TextAreaWrapper = ({ input,
    as,
    className,
    label,
    required,
    disabled,
    width,
    inline,
    meta: { touched, error },
    ...rest }) => {

    return (
        <Form.Field as={as}
            className={className}
            required={required}
            disabled={disabled}
            width={width}
            inline={inline}
            error={!!(touched && error)} >

            {label && <label>{label}</label>}
            <TextArea required={required} {...input} {...rest} />
            {touched && error ? (<Label basic color="red" pointing>{error}</Label>) : null}

        </Form.Field>
    );

};

/**
 * Wrapper class that wraps a dropdown component. The usage of this wrapper is limited to 'redux-form' Field component.
 * An example usage is shown below:
 * 
 * <Field name='songType' label='Song Type' component={DropdownWrapper} options={options} fluid search selection clearable /> 
 * 
 * @param {*} param0 Object that has the dropdown field details. 
 */
export const DropdownWrapper = ({ input,
    as,
    className,
    label,
    required,
    disabled,
    width,
    inline,
    options,
    meta: { touched, error },
    ...custom }) => {

    return (
        <Form.Field as={as}
            className={className}
            required={required}
            disabled={disabled}
            width={width}
            inline={inline}
            error={!!(touched && error)} >

            {label && <label>{label}</label>}
            <Dropdown as={as}
                className={className}
                value={input.value}
                disabled={disabled}
                required={required}
                options={options}
                onChange={(e, data) => input.onChange(data.value)}
                {...custom} />
            {touched && error ? (<Label basic color="red" pointing>{error}</Label>) : null}

        </Form.Field>
    );

};

export const CheckboxWrapper = ({ input, label, width, meta: { touched, error }, cb, ...custom }) => (
    <Form.Field
        control={CheckboxComponent}
        label={label}
        width={width}
        checked={!!input.value}
        onClick={(event, data) => {
            input.onChange(data.checked);
            if (cb) {
                cb(data);
            }
        }}
        {...custom} />
);

export const SelectField = ({ input, label, required, width, inline, options, meta: { touched, error }, ...custom }) => (
    <Form.Field error={!!(touched && error)}
        required={required}
        width={width}
        inline={inline}>

        {label && <label>{label}</label>}
        <SelectComponent
            search
            value={input.value}
            required={required}
            options={options}
            onChange={(event, data) => input.onChange(data.value)}
            {...custom} />
        {touched && error ? (<Label basic color="red" pointing>{error}</Label>) : null}

    </Form.Field>
);

export const Select = ({ input, required, options, meta: { touched, error }, ...rest }) => (
    <SelectComponent
        search
        value={input.value}
        required={required}
        options={options}
        onChange={(event, data) => input.onChange(data.value)}
        {...rest} />
);

export const ToggleField = ({ input, label, defaultChecked, width }) => (
    <Form.Field
        control={RadioComponent}
        toggle
        label={label}
        checked={!!input.value}
        defaultChecked={defaultChecked}
        onClick={(event, data) => input.onChange(data.checked)}
        width={width} />
);

export const Toggle = ({ input, label, defaultChecked }) => (
    <RadioComponent
        toggle
        label={label}
        checked={!!input.value}
        defaultChecked={defaultChecked}
        onClick={(event, data) => input.onChange(data.checked)} />
);


export const Checkbox = ({ input, label, meta: { touched, error }, ...custom }) => (
    <CheckboxComponent
        label={label}
        checked={!!input.value}
        onClick={(event, data) => input.onChange(data.checked)}
        {...custom} />
);

export const CheckboxField = ({ input, label, width, meta: { touched, error }, ...custom }) => (
    <Form.Field
        control={CheckboxComponent}
        label={label}
        width={width}
        checked={!!input.value}
        onClick={(event, data) => input.onChange(data.checked)}
        {...custom} />
);


export const RangeField = ({ input, label, width, inline, min, max, required, meta: { touched, error }, ...rest }) => (

    <Form.Field error={!!(touched && error)}
        required={required}
        width={width}
        inline={inline}>

        <label>{label} : {input.value}</label>
        <input type="range" required={required} min={min} max={max} {...input} {...rest} />
        {touched && error ? (<Label basic color="red" pointing>{error}</Label>) : null}

    </Form.Field>
);

export const Range = ({ input, min, max, required, meta: { touched, error }, ...rest }) => (
    <input type="range" required={required} min={min} max={max} {...input} {...rest} />
);

export const UploadField = ({ label, input, required, width, inline, meta: { touched, error }, ...rest }) => {
    delete input.value; //Delete value from input
    return (
        <Form.Field error={touched && error}
            required={required}
            width={width}
            inline={inline}>

            {label && <label>{label}</label>}
            <Input type="file" {...input} {...rest} />
            {touched && error ? (<Label basic color="red" pointing>{error}</Label>) : null}

        </Form.Field>
    )
};

export const Upload = ({ input, required, meta: { touched, error }, ...rest }) => {
    delete input.value;
    return (
        <Input required={required} type="file" {...input} {...rest} />
    )
};