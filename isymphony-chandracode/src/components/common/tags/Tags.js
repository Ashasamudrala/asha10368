/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Label } from "semantic-ui-react";

import Tag from "./Tag";

/**
 * Component to render a list of tags (i.e. Tag element). The list of tags to be rendered are available
 * as 'content' property in the 'props' object. The 'content' property is a collection of elements where
 * every element is an object that contains 'text' and 'detail' nested properties.
 *
 * @author Chandra Veerapaneni
 */
class Tags extends React.Component {
  /**
   * This method is responsible to render the list of tags on the UI.
   */
  render() {
    // Destructure the 'content' and other properties.
    const { content, color, circular } = this.props;
    if (content && content.length > 0) {
      return (
        <Label.Group>
          {content.map(element => (
            <Tag
              key={`${element.key}${Math.random()}` || new Date().getTime()}
              text={element.text}
              detail={element.detail}
              color={color || undefined}
              circular={circular || false}
            />
          ))}
        </Label.Group>
      );
    }
    // Empty data.
    return <div></div>;
  }
}

export default Tags;
