/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Label } from "semantic-ui-react";

/**
 * Component that renders the data as a tag i.e. a simple Label.
 *
 * The data to be rendered is available within the 'text' and 'detail' properties on the 'props' object.
 *
 * @author Chandra Veerapaneni
 */
class Tag extends React.Component {
  /**
   * Method that is responsible to render the UI on the screen.
   */
  render() {
    // Destructure the 'text', 'detail' and other properties.
    const { text, detail, color, circular } = this.props;
    if (text) {
      return (
        <Label color={color || undefined} circular={circular || false} image>
          {text}
          {detail ? <Label.Detail>{detail}</Label.Detail> : ""}
        </Label>
      );
    }
    return (
      // No data. Return empty.
      <div></div>
    );
  }
}

export default Tag;
