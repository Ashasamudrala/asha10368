/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import _ from "lodash";

import React from "react";
import {
  Form,
  // Radio as RadioComponent,
  // Checkbox,
  // Select as SelectComponent,
  Button,
  Icon
} from "semantic-ui-react";

/**
 * Wrapper class that wraps an input text field. An example usage is shown below:
 *
 * <FikInput name='title' label='Title' required />
 *
 * @param {*} param0 Object that has input field details.
 */
export const FikInput = ({ field, form, ...props }) => {
  const { setFieldValue } = form;
  const { config } = props;
  return (
    <Form.Input
      {...field}
      {...props}
      onChange={(e, field) => {
        setFieldValue(field.name, field.value);
        if (
          config &&
          config.dependentFields &&
          config.dependentFields.length > 0
        ) {
          _.each(config.dependentFields, depField => {
            setFieldValue(depField, field.value);
          });
        }
      }}
    />
  );
};

/**
 * Wrapper class that wraps an input text field. An example usage is shown below:
 *
 * <FikTextArea name='description' label='Description' rows={5} required />
 *
 * @param {*} param0 Object that has input field details.
 */
export const FikTextArea = ({ field, form, ...props }) => {
  return <Form.TextArea {...field} {...props} />;
};

/**
 * Wrapper class that wraps an input text field. An example usage is shown below:
 *
 * <FikTextArea name='description' label='Description' rows={5} required />
 *
 * @param {*} param0 Object that has input field details.
 */
export const FikButton = ({ ...props }) => {
  const { label, icon } = props;
  return (
    <Button {..._.omit(props, ["label", "icon"])}>
      {icon && <Icon name={icon} />}
      {label}
    </Button>
  );
};

/**
 * Wrapper class that wraps an input text field. An example usage is shown below:
 *
 * <FikTextArea name='description' label='Description' rows={5} required />
 *
 * @param {*} param0 Object that has input field details.
 */
export const FikLabeledButton = ({ ...props }) => {
  return <Button {...props} />;
};

/**
 * Component that displays a drop-down.
 *
 * @param {*} param0
 */
export const FikDropdown = ({
  field: { name, value },
  form: { touched, errors, setFieldValue },
  options,
  children: _,
  ...props
}) => {
  const { multiple = false, config } = props;
  const errorText = touched[name] && errors[name];
  const valueToSet = multiple ? value || [] : value || "";
  return (
    <Form.Dropdown
      selection
      clearable={true}
      options={options}
      value={valueToSet}
      onChange={(e, item) => {
        const values = item.value;
        setFieldValue(name, values);

        const { onSelectionChange } = config || {};
        if (onSelectionChange) {
          onSelectionChange(item);
        }
      }}
      error={errorText}
      {...props}
    />
  );
};


/**
 * Component that displays a Tag.
 *
 * @param {*} param0
 */
export const FikTag = ({
  field: { name, value },
  form: { touched, errors, setFieldValue },
  options,
  ...props
}) => {
  const errorText = touched[name] && errors[name];
  return (
    <Form.Dropdown
      options={options}
      value={value}
      onChange={(e, item) => {
        const values = item.value;
        // Update the selected values in the dropdown.
        setFieldValue(name, values);
        // Remove all the existing items in the options and re-add the current set of values.
        // This is being done to ensure that the removed items do not appear in the drop-down.
        options.splice(0, options.length);
        values.forEach(value => {
          options.push({ text: value, value: value });
        });
      }}
      error={errorText}
      noResultsMessage={null}
      onAddItem={(e, item) => {
        const tag = item.value;
        const matchingValue = _.find(value, element => element.key === tag);
        if (_.isNull(matchingValue) || _.isUndefined(matchingValue)) {
          options.push({ text: tag, value: tag });
        }
      }}
      search
      fluid
      selection
      multiple
      allowAdditions
      {...props}
    />
  );
};
