/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import {
  Container,
  Header,
  Segment,
  Grid,
  Label,
  List,
  Button,
  Icon,
  Divider,
  Statistic
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { withTranslation } from "react-i18next";

import getIcon from "../../../config/icons";

/**
 * This component represents the Home page.
 *
 * @author Chandra Veerapaneni
 */
class Home extends React.Component {
  /**
   * This method renders the Home page.
   */
  render() {
    const { t } = this.props;
    const effortInPersonHours = 1500;
    const effortInPersonMonths = Math.ceil(effortInPersonHours / (20 * 8));
    const costInDollars = effortInPersonHours * 25;

    return (
      <Container fluid>
        <br />
        <br />
        <Grid divided="vertically">
          <Grid.Row width={16}>
            <Grid.Column width={12}>
              <Segment color="orange" raised>
                <Header as="h2">Welcome!</Header>
                <br />
                <Label color="blue">iSymphony</Label>
                &nbsp;is a platform that{" "}
                <strong>
                  accelerates the development and devops activities
                </strong>{" "}
                that you would typically see in the development lifecycle.
                <br />
                <br />
                <br />
                <p>
                  Listed below are few items that this application helps you
                  with:
                </p>
                <List bulleted>
                  <List.Item>
                    Create and manage projects, where each project can be
                    comprised of one or more accelerators.
                  </List.Item>
                  <List.Item>Generate code for the project.</List.Item>
                  <List.Item>
                    Support for multiple technology stacks and frameworks (e.g.
                    Java / Spring Boot, .NET, Node.js, React, Angular, etc.)
                  </List.Item>
                  <List.Item>Download the generated code.</List.Item>
                  <List.Item>
                    Make changes to the project and re-generate the code any
                    number of times.
                  </List.Item>
                  <List.Item>Download code for the previous runs.</List.Item>
                  <List.Item>
                    Integrates with code repository systems like Azure DevOps
                    Git, GitHub, BitBucket, etc.
                  </List.Item>
                  <List.Item>
                    Create and configure a Build Pipeline in the continuous
                    integration systems like Azure DevOps, Jenkins, TravisCI,
                    etc.
                  </List.Item>
                  <List.Item>
                    Create and configure a continuous delivery pipeline and
                    deploy to AWS, Azure, GCP, etc.
                  </List.Item>
                  <List.Item>
                    Create conversational interfaces based on AWS Lex, Azure Bot
                    Service, Google Dialogflow
                  </List.Item>
                  <List.Item>
                    Administration console to manage and configure the settings
                    to perform the above activities.
                  </List.Item>
                </List>
              </Segment>
              <Divider />
              <Segment color="orange" raised>
                <Statistic.Group widths={5}>
                  <Statistic>
                    <Statistic.Value>
                      <Icon
                        color="green"
                        className={getIcon("project.springboot.icon")}
                      />{" "}
                      20
                    </Statistic.Value>
                    <Statistic.Label>Spring Boot Projects</Statistic.Label>
                  </Statistic>

                  <Statistic>
                    <Statistic.Value>
                      <Icon
                        color="blue"
                        className={getIcon("project.dotnet.icon")}
                      />{" "}
                      15
                    </Statistic.Value>
                    <Statistic.Label>.NET Projects</Statistic.Label>
                  </Statistic>

                  <Statistic>
                    <Statistic.Value>
                      <Icon
                        color="violet"
                        className={getIcon("project.react.icon")}
                      />{" "}
                      10
                    </Statistic.Value>
                    <Statistic.Label>React Projects</Statistic.Label>
                  </Statistic>

                  <Statistic>
                    <Statistic.Value>
                      <Icon
                        color="red"
                        className={getIcon("project.angular.icon")}
                      />{" "}
                      10
                    </Statistic.Value>
                    <Statistic.Label>Angular Projects</Statistic.Label>
                  </Statistic>

                  <Statistic>
                    <Statistic.Value>
                      <Icon color="brown" className={getIcon("robot.icon")} /> 2
                    </Statistic.Value>
                    <Statistic.Label>Chatbots</Statistic.Label>
                  </Statistic>
                </Statistic.Group>
              </Segment>
              <Divider />
              <Segment color="orange" raised>
                <Statistic.Group widths={3} color="teal">
                  <Statistic>
                    <Statistic.Value>
                      <Icon className={getIcon("clock.outline.icon")} />
                      {effortInPersonHours.toLocaleString()}
                    </Statistic.Value>
                    <Statistic.Label>
                      &nbsp;Savings in Person Hours
                    </Statistic.Label>
                  </Statistic>

                  <Statistic>
                    <Statistic.Value>
                      <Icon className={getIcon("calendar.alternate.icon")} />
                      {effortInPersonMonths.toLocaleString()}
                    </Statistic.Value>
                    <Statistic.Label>Savings in Person Months</Statistic.Label>
                  </Statistic>

                  <Statistic>
                    <Statistic.Value>
                      ${costInDollars.toLocaleString()}
                    </Statistic.Value>
                    <Statistic.Label>&nbsp;Savings in Cost</Statistic.Label>
                  </Statistic>
                </Statistic.Group>
              </Segment>
            </Grid.Column>
            <Grid.Column width={4}>
              <Segment color="orange" textAlign="center" raised>
                Begin by adding a project using the below button.
                <br />
                <br />
                <Button
                  color="blue"
                  as={NavLink}
                  exact
                  to={"/projects/new"}
                  basic
                >
                  <Icon className={getIcon("project.springboot.icon")} />
                  {t("project.actions.springboot.add")}
                </Button>
                <br />
                <br />
                <Divider />
                <br />
                Browse your projects using the below button.
                <br />
                <br />
                <Button color="blue" as={NavLink} exact to={"/projects"} basic>
                  <Icon className={getIcon("myprojects.icon")} />
                  {t("menu.projects.myProjects.title")}
                </Button>
                <br />
                <br />
                <Divider />
                <br />
                Browse the list of available accelerators in the system.
                <br />
                <br />
                <Button
                  color="blue"
                  as={NavLink}
                  exact
                  to={"/accelerators"}
                  basic
                >
                  <Icon className={getIcon("accelerator.icon")} />
                  {t(
                    "menu.projects.new.administration.accelerators.accelerators.title"
                  )}
                </Button>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default withTranslation()(Home);
