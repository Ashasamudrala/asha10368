/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";
import { Message, Container } from "semantic-ui-react";
import { connect } from "react-redux";

import { clearError } from "../../../actions/coreActions";

/**
 * Component that renders the errors encountered during the application usage.
 *
 * @author Chandra Veerapaneni
 */
class Errors extends React.Component {
  renderErrorMessages = errors => {
    return errors.map(error => {
      return (
        <div key={error.key}>
          <Message onDismiss={() => this.props.clearError(error.key)} error>
            {error.message}
          </Message>
          <br />
        </div>
      );
    });
  };

  /**
   * Renders the error messages on the screen.
   */
  render() {
    if (this.props.errors) {
      return (
        <Container fluid>
          {this.renderErrorMessages(this.props.errors)}
        </Container>
      );
    }
    return <div></div>;
  }
}

export default connect(null, {
  clearError
})(Errors);
