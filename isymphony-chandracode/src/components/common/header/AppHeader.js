/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import "./AppHeader.css";

import React from "react";
import { Menu, Image, Icon, Label, Dropdown, Divider } from "semantic-ui-react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";

import { loginAction } from "../../../actions/coreActions";
import getIcon from "../../../config/icons";

/**
 * Component that renders the header within the application.
 *
 * @author Chandra Veerapaneni
 */
class AppHeader extends React.Component {
  /**
   * Main method that gets called to render the Application Header on the UI.
   */
  render() {
    const { t } = this.props;

    return (
      <Menu className="app-header" stackable pointing inverted>
        <Menu.Item header>
          <Image
            className="logo"
            src="/assets/images/innominds-logo.png"
            size="tiny"
          />
          <Label circular color="orange" size="tiny">
            {t("application.title").toUpperCase()}
          </Label>
        </Menu.Item>
        <Menu.Item as={NavLink} exact to="/">
          <Icon name={getIcon("home.icon")} />
          {t("menu.home.title")}
        </Menu.Item>
        <Menu.Item as={NavLink} exact to="/dashboard">
          <Icon className={getIcon("dashboard.icon")} />
          {t("menu.dashboard.title")}
        </Menu.Item>

        <Menu.Menu>{this.renderProjectsMenu()}</Menu.Menu>
        <Menu.Menu>{this.renderSettingsMenu()}</Menu.Menu>

        {/* Render the user menu. */}
        <Menu.Menu position="right">{this.renderUserMenu()}</Menu.Menu>

        {/* Render auth-menu i.e. sign-in / sign-up / sign-out urls based on the user's authentication status. */}
        {/* <Menu.Menu position='right'>
                    <CognitoAuth />
                </Menu.Menu> */}
      </Menu>
    );
  }

  /**
   * This method renders the Projects menu.
   */
  renderProjectsMenu = () => {
    const { t } = this.props;

    return (
      <Dropdown text={t("menu.projects.title")} pointing className="link item">
        <Dropdown.Menu>
          <Dropdown item text={t("menu.projects.new.title")}>
            <Dropdown.Menu>
              <Dropdown.Header>
                {t("menu.projects.new.backend.title")}
              </Dropdown.Header>
              <Dropdown.Item as={NavLink} to="/projects/new">
                <Icon
                  className={getIcon("project.springboot.icon")}
                  color="teal"
                />
                {t("menu.projects.new.backend.spring.boot.title")}
              </Dropdown.Item>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon className={getIcon("project.dotnet.icon")} color="blue" />
                {t("menu.projects.new.backend.dotnet.title")}
              </Dropdown.Item>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon
                  className={getIcon("project.nodejs.icon")}
                  color="green"
                />
                {t("menu.projects.new.backend.nodejs.title")}
              </Dropdown.Item>
              <Divider />
              <Dropdown.Header>
                {t("menu.projects.new.frontend.title")}
              </Dropdown.Header>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon className={getIcon("project.react.icon")} color="blue" />
                {t("menu.projects.new.frontend.react.title")}
              </Dropdown.Item>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon className={getIcon("project.angular.icon")} color="red" />
                {t("menu.projects.new.frontend.angular.title")}
              </Dropdown.Item>
              <Divider />
              <Dropdown.Header>
                {t("menu.projects.new.chatbot.title")}
              </Dropdown.Header>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon className={getIcon("project.lex.icon")} color="orange" />
                {t("menu.projects.new.chatbot.lex.title")}
              </Dropdown.Item>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon
                  className={getIcon("project.dialogflow.icon")}
                  color="red"
                />
                {t("menu.projects.new.chatbot.dialogflow.title")}
              </Dropdown.Item>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon
                  className={getIcon("project.azurebot.icon")}
                  color="blue"
                />
                {t("menu.projects.new.chatbot.botservice.title")}
              </Dropdown.Item>
              <Divider />
              <Dropdown.Header>
                {t("menu.projects.new.devops.title")}
              </Dropdown.Header>
              <Dropdown.Item as={NavLink} to="" disabled>
                <Icon
                  className={getIcon("project.devops.pipeline.icon")}
                  color="red"
                />
                {t("menu.projects.new.devops.pipeline.title")}
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <Dropdown.Divider />
          <Dropdown.Item as={NavLink} exact to="/projects">
            <Icon className={getIcon("myprojects.icon")} color="blue" />
            {t("menu.projects.myProjects.title")}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  /**
   * This method renders the Projects menu.
   */
  renderSettingsMenu = () => {
    const { t } = this.props;
    return (
      <Dropdown
        text={t("menu.projects.administration.title")}
        pointing
        className="link item"
      >
        <Dropdown.Menu>
          <Dropdown.Header>
            {t("menu.projects.administration.core.title")}
          </Dropdown.Header>
          <Dropdown.Item as={NavLink} to="/platforms">
            <Icon className={getIcon("platform.icon")} color="brown" />
            {t("menu.projects.new.administration.core.platforms.title")}
          </Dropdown.Item>
          <Dropdown.Item as={NavLink} to="/frameworks">
            <Icon className={getIcon("framework.icon")} color="orange" />
            {t("menu.projects.new.administration.core.frameworks.title")}
          </Dropdown.Item>
          <Divider />
          <Dropdown.Header>
            {t("menu.projects.administration.devops.title")}
          </Dropdown.Header>
          <Dropdown.Item as={NavLink} to="/devops/repositories">
            <Icon className={getIcon("repository.icon")} color="black" />
            {t("menu.projects.new.administration.devops.repositories.title")}
          </Dropdown.Item>
          <Dropdown.Item as={NavLink} to="/devops/integration">
            <Icon className={getIcon("devops.integration.icon")} color="red" />
            {t(
              "menu.projects.new.administration.devops.continuousIntegration.title"
            )}
          </Dropdown.Item>
          <Dropdown.Item as={NavLink} to="/devops/delivery">
            <Icon className={getIcon("devops.delivery.icon")} color="green" />
            {t(
              "menu.projects.new.administration.devops.continuousDelivery.title"
            )}
          </Dropdown.Item>
          <Divider />
          <Dropdown.Header>
            {t("menu.projects.administration.accelerators.title")}
          </Dropdown.Header>
          <Dropdown.Item as={NavLink} to="/categories">
            <Icon className={getIcon("category.icon")} color="blue" />
            {t(
              "menu.projects.new.administration.accelerators.categories.title"
            )}
          </Dropdown.Item>
          <Dropdown.Item as={NavLink} to="/accelerators">
            <Icon className={getIcon("accelerator.icon")} color="red" />
            {t(
              "menu.projects.new.administration.accelerators.accelerators.title"
            )}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };

  /**
   * This method renders the User Profile menu and is generally visible only after a
   * user authenticates into the system.
   */
  renderUserMenu = () => {
    const { t } = this.props;

    return (
      <Dropdown
        text={t("welcome").replace("{user}", "User")}
        pointing
        className="item"
      >
        <Dropdown.Menu>
          <Dropdown.Item as={NavLink} exact to="/users/me/profile">
            <Icon className={getIcon("settings.icon")} />
            {t("menu.projects.user.settings.profile.title")}
          </Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown.Item as={NavLink} exact to="/logout">
            <Icon className={getIcon("logout.icon")} />
            {t("menu.projects.user.settings.logout.title")}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
  };
}

/**
 * Map the state data into properties on this component.
 *
 * @param {*} state State object as stored in the redux
 * @param {*} ownProps Properties that gets passed by redux
 */
const mapStateToProps = (state, ownProps) => {
  const { pagination } = state.projects;
  // What data on the state needs to be converted to properties on this component.
  return {
    isAuthenticated: state.isAuthenticated,
    currentUser: state.user,
    projectsCount: pagination ? pagination.totalElements : 0
  };
};

export default connect(mapStateToProps, {
  loginAction
})(withTranslation()(AppHeader));
