/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */
import React from "react";
import { Header, Icon, Container, Segment } from "semantic-ui-react";

/**
 * A React component that is responsible to display a Segment with a header.
 *
 * @author Chandra Veerapaneni
 */
class Panel extends React.Component {
  /**
   * Method that is responsible to render the segment.
   */
  render() {
    // Initialize the defaults.
    const defaults = this.getDefaults();

    // Destructure the required properties.
    const { title, subtitle, content, as, icon } = this.props.options;

    // return the data.
    return (
      <Container>
        <Header as={as || defaults.as} attached="top" block>
          <Icon className={icon || defaults.icon} />
          <Header.Content>
            {title || defaults.title}
            <Header.Subheader>{subtitle || defaults.subtitle}</Header.Subheader>
          </Header.Content>
        </Header>
        <Segment attached>{content || defaults.content}</Segment>
      </Container>
    );
  }

  /**
   * This method returns the default settings for this component.
   */
  getDefaults = () => {
    return {
      title: "No title provided",
      subtitle: "",
      as: "h4",
      icon: "question",
      content: "No content provided"
    };
  };
}

export default Panel;
