import {
  Popup,
  Message,
  Icon,
  Container,
  Loader,
  Divider
} from "semantic-ui-react";

/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import React from "react";

import _ from "lodash";

/**
 * This method attempts to add a popup on the component identified via the "trigger" parameter using the provided header and content.
 *
 * @param {*} trigger Component on which the popup needs to be displayed.
 * @param {*} header Header within the popup.
 * @param {*} content Content within the popup.
 * @param {*} options Options to configure the popup
 */
export const configurePopup = (trigger, header, content, options) => {
  const { size = "tiny", key = Math.random() } = options || {};

  return (
    <Popup trigger={trigger} size={size} key={key}>
      {header ? <Popup.Header>{header}</Popup.Header> : ""}
      {header && <Divider />}
      {content ? <Popup.Content>{content}</Popup.Content> : ""}
    </Popup>
  );
};

/**
 * This method renders a message box with the provided header and content.
 *
 * @param {*} header Header / title of the page
 * @param {*} content Subtitle
 * @param {*} icon Icon to be used
 * @param {*} rightContent Content that will be placed on the right
 */
export const renderPageTitle = (header, content, icon, rightContent) => {
  return (
    <Message icon info>
      <Icon name={icon ? icon : "question"} />
      <Container align="left">
        <Message.Header>{header}</Message.Header>
        <Message.Content>{content}</Message.Content>
      </Container>
      {rightContent && rightContent}
    </Message>
  );
};

/**
 * This method renders a loader with the provided message.
 *
 * @param {*} message Message to be displayed inside the loader.
 */
export const renderLoader = (message = "Loading...") => {
  return (
    <Message>
      <Loader inline="centered" size="large" active>
        {message}
      </Loader>
    </Message>
  );
};

/**
 * This method attempts to render a message window that is primarily meant to display no data messages.
 *
 * @param {*} header Header in the message window.
 * @param {*} message Actual message.
 */
export const renderNoDataMessage = (header, message) => {
  return (
    <Message align="center">
      <Message.Header>{header}</Message.Header>
      <Message.Content>
        <p>{message}</p>
      </Message.Content>
    </Message>
  );
};

/**
 * This method attempts to standardize the provided error into an object that consists of 'key', 'code' and 'message' fields.
 *
 * @param {*} error Error object that holds the error details.
 * @param {*} customErrorMessage Custom error message that will be used when there is no error message to display.
 */
export const standardizeError = (error, customErrorMessage = "") => {
  console.debug(error);
  const errorToUse = {
    key: new Date().getTime(),
    status: 500,
    code: "",
    message: customErrorMessage
  };

  if (error) {
    if (error instanceof Object) {
      errorToUse.code = error.data.error || "";
      errorToUse.message = error.data.message
        ? error.data.message
        : customErrorMessage
        ? customErrorMessage
        : JSON.stringify(error.data);
    } else {
      errorToUse.code = "ERROR";
      errorToUse.message = customErrorMessage || error.toString();
    }
  } else {
    errorToUse.code = "BAD_REQUEST";
    errorToUse.message = customErrorMessage || "No error message available";
  }
  return errorToUse;
};

/**
 * This method truncates the provided input upto the specified length and replaces the rest of the characters with ellipsis (...).
 *
 * @param {*} input Input to be truncated to the specified limit and adds ellipsis
 * @param {*} length Max length after which the input will be truncated.
 */
export const truncateString = (input, length) => {
  return _.truncate(input, {
    length: length
  });
};
