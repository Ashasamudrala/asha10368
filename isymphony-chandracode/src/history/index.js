/*
 * Copyright (c) 1998-2019 Innominds Software SEZ India Pvt Ltd. All rights reserved.
 *
 * This file is part of Digital Experience Platform.
 *
 * Digital Experience Platform project and associated code cannot be copied and/or
 * distributed without a written permission of Innominds Software SEZ India Pvt Ltd,
 * and/or its subsidiaries.
 */

import { createBrowserHistory as history } from "history";

/**
 * Expose browser history object so that it can be injected into other application parts.
 *
 * @author Chandra Veerapaneni
 */
export default history();
