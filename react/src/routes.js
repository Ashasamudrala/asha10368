import React from 'react';
import Clock from './components/clock';
import Register from './components/register';
import {Switch,Route} from 'react-router-dom';
import {BrowserRouter} from 'react-router-dom';
import Car from './components/car';
import CarDetail from './components/cardetails';
import Home from './components/home';

const Routes = () => (
    <BrowserRouter>
    <Switch>
    <Route exact path={'/'} component={Home} />
      <Route path={'/clock'} component={Clock} />
      <Route path={'/register'} component={Register} />
      <Route path={'/cars'} component={Car} exact/>
      <Route path={'/cars/:id'} component={CarDetail} exact/>

    </Switch>
    </BrowserRouter>
);

export default Routes;
