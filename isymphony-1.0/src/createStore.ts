import rootReducer from './rootReducer'
import { configureStore } from '@reduxjs/toolkit'
import { RootState } from './base.types'

const store = () =>
	configureStore<RootState>({
		reducer: rootReducer as any,
	})

export default store
