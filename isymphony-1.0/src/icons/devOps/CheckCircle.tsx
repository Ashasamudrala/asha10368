import React from 'react'

export type CheckCircleIconProps = React.SVGProps<SVGSVGElement>

export function CheckCircleIcon(props: CheckCircleIconProps) {
	return (
		<svg
			xmlns='http://www.w3.org/2000/svg'
			height='24'
			viewBox='0 0 24 24'
			width='24'
			{...props}
		>
			<g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'>
				<polygon points='0 0 24 0 24 24 0 24' />
				<path
					d='M12,4 C7.584,4 4,7.584 4,12 C4,16.416 7.584,20 12,20 C16.416,20 20,16.416 20,12 C20,7.584 16.416,4 12,4 Z'
					fill='#FFFFFF'
				/>
				<path
					d='M12,4 C7.584,4 4,7.584 4,12 C4,16.416 7.584,20 12,20 C16.416,20 20,16.416 20,12 C20,7.584 16.416,4 12,4 Z M9.832,15.432 L6.96,12.56 C6.648,12.248 6.648,11.744 6.96,11.432 C7.272,11.12 7.776,11.12 8.088,11.432 L10.4,13.736 L15.904,8.232 C16.216,7.92 16.72,7.92 17.032,8.232 C17.344,8.544 17.344,9.048 17.032,9.36 L10.96,15.432 C10.656,15.744 10.144,15.744 9.832,15.432 Z'
					fill='#008900'
					fill-rule='nonzero'
				/>
			</g>
		</svg>
	)
}
