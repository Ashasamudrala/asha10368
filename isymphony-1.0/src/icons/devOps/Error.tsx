import React from 'react'

export type ErrorIconProps = React.SVGProps<SVGSVGElement>

export function ErrorIcon(props: ErrorIconProps) {
	return (
		<svg
			xmlns='http://www.w3.org/2000/svg'
			height='24'
			viewBox='0 0 24 24'
			width='24'
			{...props}
		>
			<g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'>
				<polygon id='Path' opacity='0.87' points='0 0 24 0 24 24 0 24' />
				<path
					d='M12,4 C7.584,4 4,7.584 4,12 C4,16.416 7.584,20 12,20 C16.416,20 20,16.416 20,12 C20,7.584 16.416,4 12,4 Z'
					fill='#FFFFFF'
				/>
				<path
					d='M12,4 C7.576,4 4,7.576 4,12 C4,16.424 7.576,20 12,20 C16.424,20 20,16.424 20,12 C20,7.576 16.424,4 12,4 Z M15.44,15.44 C15.128,15.752 14.624,15.752 14.312,15.44 L12,13.128 L9.688,15.44 C9.376,15.752 8.872,15.752 8.56,15.44 C8.248,15.128 8.248,14.624 8.56,14.312 L10.872,12 L8.56,9.688 C8.248,9.376 8.248,8.872 8.56,8.56 C8.872,8.248 9.376,8.248 9.688,8.56 L12,10.872 L14.312,8.56 C14.624,8.248 15.128,8.248 15.44,8.56 C15.752,8.872 15.752,9.376 15.44,9.688 L13.128,12 L15.44,14.312 C15.744,14.616 15.744,15.128 15.44,15.44 Z'
					fill='#B12828'
					fill-rule='nonzero'
				/>
			</g>
		</svg>
	)
}
