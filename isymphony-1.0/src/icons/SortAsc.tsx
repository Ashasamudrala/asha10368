import React from 'react'

export type SortAscIconProps = React.SVGProps<SVGSVGElement>

export function SortAscIcon(props: SortAscIconProps) {
	return (
		<svg {...props}>
			<g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'>
				<polygon
					id='Path'
					fill='#666666'
					fill-rule='nonzero'
					points='2.5 4.5 3.205 5.205 5.5 2.915 5.5 11 6.5 11 6.5 2.915 8.795 5.21 9.5 4.5 6 1'
				/>
			</g>
		</svg>
	)
}
