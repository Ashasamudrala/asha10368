import React from 'react'

export type ManyToOneIconProps = React.SVGProps<SVGSVGElement>

export function ManyToOneIcon(props: ManyToOneIconProps) {
	return (
		<svg {...props}>
			<g strokeWidth='1' fill='none' fillRule='evenodd'>
				<path
					d='M9,16.3846154 L25,16.3846154 M9,21 L18.9047619,16.3846154 M9,11 L18.9047619,16.3846154'
					strokeWidth='1.6'
					strokeLinecap='round'
					strokeLinejoin='round'
				/>
			</g>
		</svg>
	)
}
