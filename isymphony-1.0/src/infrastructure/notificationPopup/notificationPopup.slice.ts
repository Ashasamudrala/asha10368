import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { NotificationPopupState } from './notificationPopup.types'
import { sliceName } from './notificationPopup.selectors'

const initialState: NotificationPopupState = {
	errorMessage: undefined,
	successMessage: undefined,
}

const slice = createSlice<
	NotificationPopupState,
	SliceCaseReducers<NotificationPopupState>,
	string
>({
	name: sliceName,
	initialState,
	reducers: {
		notifyError(state, action) {
			state.errorMessage = action.payload
		},
		notifySuccess(state, action) {
			state.successMessage = action.payload
		},
		resetError(state) {
			state.errorMessage = undefined
			state.successMessage = undefined
		},
		closePopup(state) {
			state.errorMessage = undefined
			state.successMessage = undefined
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
