export interface NotificationPopupState {
	errorMessage: any | undefined
	successMessage: any | undefined
}
