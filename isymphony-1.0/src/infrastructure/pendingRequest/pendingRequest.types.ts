export interface PendingRequestState {
	[key: string]: { turnSpinnerOff: boolean }
}
