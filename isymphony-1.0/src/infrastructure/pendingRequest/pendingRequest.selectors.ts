import buildCacheKey from '../buildCacheKey'
import { RootState } from '../../base.types'

export const sliceName = 'pendingRequest'

export const selectSlice = (state: RootState) => state[sliceName]

export const selectPendingRequest = (
	state: RootState,
	{ url, httpMethod }: { url: string; httpMethod: string }
) => selectSlice(state)[buildCacheKey({ url, httpMethod })]
