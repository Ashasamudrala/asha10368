import isEqual from 'lodash/isEqual'
import { RootState } from '../../base.types'
import buildCacheKey from '../buildCacheKey'

export const sliceName = 'httpCache'

export const selectSlice = (state: RootState) => state[sliceName]

export const CACHE_TIMEOUT = 900000

export const isExpired = (item: any) => {
	const currentTime = Date.now()
	return currentTime - item.createdAt > CACHE_TIMEOUT
}

export const getRequestCache = (state: RootState) => selectSlice(state)

export const tryToFindRequestInCache = (
	state: RootState,
	url: string,
	httpMethod: string,
	body: any
) => {
	const cacheKey = buildCacheKey({ url, httpMethod })
	const item = getRequestCache(state)[cacheKey]

	if (
		item &&
		(httpMethod.toLowerCase() === 'post' ||
			httpMethod.toLowerCase() === 'put') &&
		body
	) {
		if (!isEqual(item.body, body)) {
			return false
		}
	}

	if (!item) {
		return item
	}

	if (isExpired(item)) {
		// TODO: ryan - remove this as it's now mutating state
		// getRequestCache[cacheKey] = undefined;
		return false
	}

	return item
}
