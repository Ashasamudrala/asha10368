import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import buildCacheKey from '../buildCacheKey'
import { sliceName } from './httpCache.selectors'
import { HttpCacheState } from './httpCache.types'

const initialState: HttpCacheState = {}

const slice = createSlice<
	HttpCacheState,
	SliceCaseReducers<HttpCacheState>,
	string
>({
	name: sliceName,
	initialState,
	reducers: {
		addRequestToCache(state, action) {
			state[buildCacheKey(action.payload)] = {
				...action.payload.config,
				createdAt: action.payload.createdAt,
			}
		},
		deleteRequestFromCache(state, action) {
			if (
				action.payload.url &&
				action.payload.httpMethod &&
				!action.payload.patterns
			) {
				delete state[buildCacheKey(action.payload)]
			} else if (
				!(action.payload.url && action.payload.httpMethod) &&
				action.payload.patterns
			) {
				for (const cacheKey in state) {
					if (
						Object.prototype.hasOwnProperty.call(state, cacheKey) &&
						action.payload.patterns.some((p: any) => p.test(cacheKey))
					) {
						delete state[cacheKey]
					}
				}
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
