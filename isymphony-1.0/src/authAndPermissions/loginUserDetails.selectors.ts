import { isNil } from 'lodash'
import { RootState } from '../base.types'
import {
	HelpStatusProps,
	LoginUserDetailsState,
	LoginUserInfoState,
	PreferenceHelpStatusProps,
} from './loginUserDetails.types'

export const sliceName = 'loginUserDetails'

export const selectSlice = (state: RootState): LoginUserDetailsState =>
	state[sliceName]

export const selectGetLogInUserInfo = (
	state: RootState
): LoginUserInfoState | null => selectSlice(state).loginInfo

export const selectGetSelectedAppName = (state: RootState): string | null =>
	selectSlice(state).selectedAppName

export const selectGetSelectedAppId = (state: RootState): string | null =>
	selectSlice(state).selectedAppId

export const selectGetLoginUserId = (state: RootState): string | null => {
	const userInfo = selectGetLogInUserInfo(state)
	if (!isNil(userInfo)) {
		return userInfo.id
	}
	return null
}

export const selectGetLoginUserDisplayName = (state: RootState): string => {
	const userInfo = selectGetLogInUserInfo(state)
	if (!isNil(userInfo)) {
		return userInfo.displayName
	}
	return ''
}

export const selectGetLoginUserTitle = (state: RootState): string => {
	const userInfo = selectGetLogInUserInfo(state)
	if (!isNil(userInfo)) {
		return userInfo.title
	}
	return ''
}

export const selectGetAppPreferenceStatus = (
	state: RootState
): HelpStatusProps => {
	const preferences = selectSlice(state).preferences
	return preferences.apps
}

export const selectGetDatabasePreferenceStatus = (
	state: RootState
): HelpStatusProps => {
	const preferences = selectSlice(state).preferences
	return preferences.database
}

export const selectGetWebServicePreferenceStatus = (
	state: RootState
): HelpStatusProps => {
	const preferences = selectSlice(state).preferences
	return preferences.webServices
}

export const getUpdatedPreferences = (
	state: RootState
): PreferenceHelpStatusProps => {
	return selectSlice(state).preferences
}

export const selectGetAcceleratorsPreferenceStatus = (
	state: RootState
): HelpStatusProps => {
	const preferences = selectSlice(state).preferences
	return preferences.accelerators
}

export const selectGetSideNavbarPreferenceStatus = (
	state: RootState
): HelpStatusProps => {
	const preferences = selectSlice(state).preferences
	return preferences.sideNavbar
}
