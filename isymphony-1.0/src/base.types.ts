import { LoginUserDetailsState } from './authAndPermissions/loginUserDetails.types'
import { AcceleratorsBaseState } from './features/accelerators/base.types'
import { AdminBaseState } from './features/admin/base.types'
import { PlatformSettingState } from './features/admin/core/platform/platformSettings/platformSettings.types'
import { AppsBaseState } from './features/apps'
import { DatabaseBaseState } from './features/database'
import { EditProfileState } from './features/editProfile/editProfile.types'
import { LoginAndSignUpState } from './features/loginAndSignUp/loginAndSignUp.types'
import { TeamsBaseState } from './features/team/base.types'
import { WebServiceBaseState } from './features/webServices/base.types'
import { DialogsBaseState } from './features/dialogs/base.types'
import { HttpCacheState } from './infrastructure/httpCache/httpCache.types'
import { NotificationPopupState } from './infrastructure/notificationPopup/notificationPopup.types'
import { PendingRequestState } from './infrastructure/pendingRequest/pendingRequest.types'
import { IsyBusyIndicatorState } from './widgets/IsyBusyIndicator/isyBusyIndicator.types'
import { AppDevOpsState } from './features/appDevOps/appDevOps.types'

export interface RootState {
	apps: AppsBaseState
	databases: DatabaseBaseState
	loginUserDetails: LoginUserDetailsState
	editProfile: EditProfileState
	platformSettings: PlatformSettingState
	notificationPopup: NotificationPopupState
	httpCache: HttpCacheState
	pendingRequest: PendingRequestState
	dialogs: DialogsBaseState
	admin: AdminBaseState
	loginAndSignUp: LoginAndSignUpState
	acceleratorsBase: AcceleratorsBaseState
	webServiceBase: WebServiceBaseState
	teamBase: TeamsBaseState
	busyIndicator: IsyBusyIndicatorState
	appDevOps: AppDevOpsState
}
