import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

i18n
	.use(Backend)
	// detect user language
	// learn more: https://github.com/i18next/i18next-browser-languageDetector
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		debug: true,
		backend: {
			loadPath: '/locales/{{lng}}/{{ns}}.json',
			allowMultiLoading: true,
		},
		fallbackLng: {
			'en-US': ['en'],
			'fr-FR': ['fr'],
			default: ['en'],
		},
		interpolation: {
			escapeValue: false,
		},
	})
