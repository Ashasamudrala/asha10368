import { combineReducers } from 'redux'
import * as busyIndicator from './widgets/IsyBusyIndicator'
import * as dialogs from './features/dialogs'
import * as pendingRequest from './infrastructure/pendingRequest'
import * as notificationPopup from './infrastructure/notificationPopup'
import * as httpCache from './infrastructure/httpCache'
import * as loginAndSignUp from './features/loginAndSignUp'
import * as loginUserDetails from './authAndPermissions/loginUserDetails.slice'
import * as Team from './features/team'
import * as editProfile from './features/editProfile'
import * as apps from './features/apps'
import * as database from './features/database'
import * as accelerators from './features/accelerators'
import * as admin from './features/admin'
import * as webService from './features/webServices'
import * as appDevOps from './features/appDevOps/appDevOps.slice'

export default combineReducers({
	[busyIndicator.name]: busyIndicator.reducer,
	[appDevOps.name]: appDevOps.reducer,
	[loginAndSignUp.name]: loginAndSignUp.reducer,
	[dialogs.name]: dialogs.reducer,
	[loginUserDetails.name]: loginUserDetails.reducer,
	[apps.name]: apps.reducer,
	[pendingRequest.name]: pendingRequest.reducer,
	[notificationPopup.name]: notificationPopup.reducer,
	[httpCache.name]: httpCache.reducer,
	[Team.name]: Team.reducer,
	[editProfile.name]: editProfile.reducer,
	[database.name]: database.reducer,
	[accelerators.name]: accelerators.reducer,
	[admin.name]: admin.reducer,
	[webService.name]: webService.reducer,
})
