import { useTranslation } from 'react-i18next'
import {
	LOGIN_HEADER_TRANSLATION,
	ISYMPHONY_LOGO_ALT,
} from '../../utilities/constants'
import './isyLogoHeader.scss'

export function IsyLogoHeader() {
	const { t } = useTranslation(LOGIN_HEADER_TRANSLATION)
	return (
		<div className='logo-text'>
			<img src={`/images/iSymphonyLogo.svg`} alt={t(ISYMPHONY_LOGO_ALT)} />
		</div>
	)
}
