import React from 'react'
import { isArray, isEmpty, isFunction, isNil } from 'lodash'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import { IsyCheckbox } from '../IsyCheckbox/IsyCheckbox'
import './IsyDropDown.scss'

export interface IsyDropDownOptionProps<T> {
	id: T
	name?: string
	icon?: React.ReactNode
}

export interface IsyDropDownProps<T> {
	title?: string
	required?: boolean
	value: T
	options: Array<IsyDropDownOptionProps<T>>
	disabled?: boolean
	className?: string
	onChange: (value: any) => void
	isMultiSelect?: boolean
}

export function IsyDropDown<T>(props: IsyDropDownProps<T>) {
	const getValue = props.isMultiSelect
		? isArray(props.value)
			? props.value
			: [props.value]
		: props.value

	const getOptions = () => {
		if (isArray(props.options)) {
			return props.options
		}
		return []
	}

	const handleOnChange = (
		evt: React.ChangeEvent<{ name?: string; value: unknown }>
	) => {
		if (isFunction(props.onChange)) {
			evt.stopPropagation()
			props.onChange(evt.target.value)
		}
	}

	const renderCheckbox = (option: IsyDropDownOptionProps<T>) => {
		let selectedValues: any = getValue
		const handleCheckboxChange = (data: IsyDropDownOptionProps<T>) => {
			if (!props.isMultiSelect || !isArray(data)) {
				const values = [...selectedValues]
				const index = values.indexOf(data.id)
				if (index === -1) {
					values.push(data.id)
				} else {
					values.splice(index, 1)
				}
				props.onChange(values)
			}
		}
		return (
			<IsyCheckbox
				value={selectedValues.indexOf(option.id) > -1}
				onChange={() => handleCheckboxChange(option)}
			/>
		)
	}

	const renderTitle = () => {
		if (isEmpty(props.title)) {
			return null
		}
		return (
			<label className='isy-dropdown-label'>
				{props.title}
				{props.required && <span className='isy-dropdown-required'> *</span>}
			</label>
		)
	}

	const handleRenderValue = (selected: unknown) => {
		let selectedValues: any = isArray(selected)
			? (selected as T[])
			: [selected as string]
		return (getOptions() as { id: T; name: T }[] | [])
			.filter((x) => selectedValues.indexOf(x.id) > -1)
			.map((x) => x.name)
			.join(', ')
	}

	const renderOption = (option: IsyDropDownOptionProps<T>) => {
		return (
			<MenuItem
				key={option.id as any}
				value={option.id as any}
				className={`${props.className || ''} isy-dropdown-option`}
			>
				{props.isMultiSelect && renderCheckbox(option)}
				{!isNil(option.icon) && option.icon}
				{!isNil(option.name) && (
					<span className='isy-dropdown-option-label'>{option.name}</span>
				)}
			</MenuItem>
		)
	}

	const renderSelect = () => {
		return (
			<Select
				onChange={handleOnChange}
				className={`isy-dropdown-control ${props.className || ''}`}
				value={getValue}
				disabled={props.disabled}
				multiple={props.isMultiSelect}
				renderValue={props.isMultiSelect ? handleRenderValue : undefined}
			>
				{getOptions().map(renderOption)}
			</Select>
		)
	}

	return (
		<div className='isy-dropdown'>
			{renderTitle()}
			{renderSelect()}
		</div>
	)
}
