import { IsyTour } from '../IsySteps/IsyTour/IsyTour'
import { isFunction } from 'lodash'
import { IsyStepItemProps, IsyStepsOptionProps } from './IsyTour/Steps'

export interface IsyStepsProps {
	isEnabled: boolean
	steps: IsyStepItemProps[]
	initialStep: number
	updateSteps?: boolean
	options?: IsyStepsOptionProps
	onAfterChange?: (currentStep: number) => void
	onBeforeExit?: (currentStep: number) => boolean
	onBeforeChange?: (currentStep: number) => boolean
	onComplete?: () => void
	getCustomButtonData?: (currentStep: number, type: string) => void
	onExit?: (currentStep: number) => void
}

export function IsySteps(props: IsyStepsProps) {
	const handleOnAfterChange = (currentStep: number) => {
		if (isFunction(props.onAfterChange)) {
			props.onAfterChange(currentStep)
		}
	}

	const handleOnBeforeExit = (currentStep: number) => {
		if (isFunction(props.onBeforeExit)) {
			return props.onBeforeExit(currentStep)
		}
		return true
	}

	const handleOnBeforeChange = (currentStep: number) => {
		if (isFunction(props.onBeforeChange)) {
			return props.onBeforeChange(currentStep)
		}
		return true
	}

	const handleOnComplete = () => {
		if (isFunction(props.onComplete)) {
			props.onComplete()
		}
	}

	const handleCustomButton = (currentStep: number, type: string) => {
		if (isFunction(props.getCustomButtonData)) {
			props.getCustomButtonData(currentStep, type)
		}
	}

	const handleExit = (currentStep: number) => {
		if (isFunction(props.onExit)) {
			props.onExit(currentStep)
		}
	}

	const render = () => {
		return (
			<IsyTour
				onExit={handleExit}
				initialStep={props.initialStep}
				steps={props.steps}
				isEnabled={props.isEnabled}
				updateSteps={props.updateSteps}
				onChange={handleOnAfterChange}
				onBeforeExit={handleOnBeforeExit}
				onBeforeChange={handleOnBeforeChange}
				onComplete={handleOnComplete}
				getCustomButtonData={handleCustomButton}
				options={props.options}
			/>
		)
	}
	return render()
}
