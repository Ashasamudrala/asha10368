import { isEqual, isFunction } from 'lodash'
import { isNil } from 'lodash'
import React, { isValidElement } from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import {
	IsyStepItemProps,
	isySteps,
	IsySteps,
	IsyStepsOptionProps,
} from './Steps'

export interface IsyTourProps {
	steps: IsyStepItemProps[]
	options?: IsyStepsOptionProps
	onExit: (currentStep: number) => void
	onBeforeExit?: (currentStep: number) => boolean
	onBeforeChange?: (currentStep: number) => boolean
	onPreventChange?: (currentStep: number) => void
	onAfterChange?: (currentStep: number) => void
	onChange?: (currentStep: number) => void
	onComplete?: () => void
	onStart?: (currentStep: number) => void
	getCustomButtonData?: (currentStep: number, type: string) => void
	isEnabled: boolean
	initialStep: number
	updateSteps?: boolean
}

export class IsyTour extends React.Component<IsyTourProps> {
	isyStepsRef: IsySteps | null
	isConfigured: boolean
	isVisible: boolean
	constructor(props: IsyTourProps) {
		super(props)
		this.isyStepsRef = null
		this.isConfigured = false
		this.isVisible = false

		this.initializationSteps()
	}

	componentDidMount() {
		if (this.props.isEnabled) {
			this.configureStepsJs()
			this.renderSteps()
		}
	}

	componentDidUpdate(prevProps: IsyTourProps) {
		const { isEnabled, steps, options, initialStep } = this.props
		if (
			!this.isConfigured ||
			!isEqual(prevProps.steps, steps) ||
			!isEqual(prevProps.options, options)
		) {
			this.configureStepsJs()
			this.renderSteps()
		}

		if (prevProps.isEnabled !== isEnabled) {
			this.renderSteps()
		}

		if (
			this.props.updateSteps &&
			isEnabled &&
			!isNil(this.isyStepsRef) &&
			prevProps.initialStep !== initialStep
		) {
			if (initialStep !== this.isyStepsRef.currentStep) {
				this.isyStepsRef.updateStep(initialStep)
			}
		}
		if (
			!isEqual(prevProps.steps, steps) &&
			isEnabled &&
			!isNil(this.isyStepsRef)
		) {
			this.isyStepsRef.updateStepsData(steps)
		}
	}

	componentWillUnmount() {
		if (!isNil(this.isyStepsRef)) {
			this.isyStepsRef.exit()
		}
	}

	configureStepsJs() {
		const { options, steps } = this.props

		const sanitizedSteps = steps.map((step) => {
			if (isValidElement(step.description)) {
				return {
					...step,
					intro: renderToStaticMarkup(step.description),
				}
			}
			return step
		})

		if (!isNil(this.isyStepsRef)) {
			this.isyStepsRef.setOptions({ ...options, steps: sanitizedSteps })
		}

		this.isConfigured = true
	}

	renderSteps() {
		const { isEnabled, initialStep, steps } = this.props
		if (
			isEnabled &&
			steps.length > 0 &&
			!this.isVisible &&
			!isNil(this.isyStepsRef)
		) {
			this.isyStepsRef.start()

			this.isVisible = true

			this.isyStepsRef.goToStepNumber(initialStep + 1)

			if (this.props.onStart && !isNil(this.isyStepsRef)) {
				this.props.onStart(this.isyStepsRef.currentStep)
			}
		} else if (!isEnabled && this.isVisible) {
			this.isVisible = false
			if (!isNil(this.isyStepsRef)) {
				this.isyStepsRef.exit()
			}
		}
	}

	initializationSteps = () => {
		this.isyStepsRef = isySteps()

		this.isyStepsRef.onExit(this.handleExit)
		this.isyStepsRef.onBeforeExit(this.handleBeforeExit)
		this.isyStepsRef.onBeforeChange(this.handleBeforeChange)
		this.isyStepsRef.onAfterChange(this.handleAfterChange)
		this.isyStepsRef.onChange(this.handleOnChange)
		this.isyStepsRef.onComplete(this.handleOnComplete)
		this.isyStepsRef.onCustomButtonClick(this.handleCustomButton)
	}

	handleCustomButton = () => {
		if (
			isFunction(this.props.getCustomButtonData) &&
			!isNil(this.isyStepsRef) &&
			!isNil(this.isyStepsRef.buttonType)
		) {
			this.props.getCustomButtonData(
				this.isyStepsRef.currentStep,
				this.isyStepsRef.buttonType
			)
		}
	}

	handleBeforeChange = () => {
		if (!this.isVisible) {
			return true
		}
		if (isFunction(this.props.onBeforeChange) && !isNil(this.isyStepsRef)) {
			if (isFunction(this.props.onPreventChange) && !isNil(this.isyStepsRef)) {
				setTimeout(() => {
					!isNil(this.isyStepsRef) &&
						isFunction(this.props.onPreventChange) &&
						this.props.onPreventChange(this.isyStepsRef.currentStep)
				}, 0)
			}
			return this.props.onBeforeChange(this.isyStepsRef.currentStep)
		} else {
			return true
		}
	}

	handleExit = () => {
		this.isVisible = false
		if (isFunction(this.props.onExit) && !isNil(this.isyStepsRef))
			this.props.onExit(this.isyStepsRef.currentStep)
	}

	handleBeforeExit = () => {
		if (isFunction(this.props.onBeforeExit) && !isNil(this.isyStepsRef)) {
			return this.props.onBeforeExit(this.isyStepsRef?.currentStep)
		}
		return true
	}

	handleAfterChange = () => {
		if (!this.isVisible) {
			return
		}
		if (isFunction(this.props.onAfterChange) && !isNil(this.isyStepsRef)) {
			this.props.onAfterChange(this.isyStepsRef.currentStep)
		}
	}

	handleOnChange = () => {
		if (!this.isVisible) {
			return
		}
		if (
			isFunction(this.props.onChange) &&
			!isNil(this.isyStepsRef) &&
			isFunction(this.props.onAfterChange)
		) {
			this.props.onAfterChange(this.isyStepsRef.currentStep)
		}
	}

	handleOnComplete = () => {
		if (isFunction(this.props.onComplete)) {
			this.props.onComplete()
		}
	}

	render() {
		return null
	}
}
