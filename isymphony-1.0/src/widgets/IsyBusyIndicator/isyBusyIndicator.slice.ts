import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { isString } from 'lodash'
import { Action } from '../../common.types'
import { IsyBusyIndicatorState } from './isyBusyIndicator.types'

const initialState: IsyBusyIndicatorState = {
	global: 0,
}

export default createSlice<
	IsyBusyIndicatorState,
	SliceCaseReducers<IsyBusyIndicatorState>,
	'busyIndicator'
>({
	name: 'busyIndicator',
	initialState,
	reducers: {
		incrementBusyIndicator(state, action: Action<string | null>) {
			if (isString(action.payload)) {
				if (!action.payload) {
					state[action.payload] = 1
				} else {
					state[action.payload]++
				}
				return
			}

			state.global++
		},
		decrementBusyIndicator(state, action: Action<string | null>) {
			if (isString(action.payload)) {
				if (!action.payload) {
					throw new Error('Attempted to decrement an empty busy indicator')
				} else {
					state[action.payload]--
				}
				return
			}

			if (!state.global) {
				throw new Error('Attempted to decrement an empty busy indicator')
			}

			state.global--
		},
	},
})
