import { RootState } from '../../base.types'
import { IsyBusyIndicatorState } from './isyBusyIndicator.types'
import isyBISlice from './isyBusyIndicator.slice'
const selectSlice = (state: RootState): IsyBusyIndicatorState =>
	state[isyBISlice.name]
export const getNamedBusyIndicator = (state: RootState, name: string) =>
	!!selectSlice(state)[name]

export const getGlobalBusyIndicator = (state: RootState) =>
	getNamedBusyIndicator(state, 'global')
