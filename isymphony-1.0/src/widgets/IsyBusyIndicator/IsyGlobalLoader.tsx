import React from 'react'
import { useSelector } from 'react-redux'
import { getGlobalBusyIndicator } from './IsyBusyIndicator.selectors'
import CircularProgress from '@material-ui/core/CircularProgress'
import './IsyBusyIndicator.scss'

export function IsyGlobalLoader() {
	const showLoader = useSelector(getGlobalBusyIndicator)

	const render = () => {
		if (showLoader) {
			return (
				<div className='global-loader-holder'>
					<CircularProgress className='loader' />
				</div>
			)
		}
		return null
	}

	return <>{render()}</>
}
