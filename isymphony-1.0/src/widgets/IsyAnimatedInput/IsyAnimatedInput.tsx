import React from 'react'
import {
	FormControl,
	IconButton,
	InputLabel,
	InputAdornment,
} from '@material-ui/core'
import InputText from '@material-ui/core/Input'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import './isyAnimatedInput.scss'
import { isNil, isFunction } from 'lodash'

export interface IsyAnimatedInputProps<T> {
	type?: 'text' | 'password' | 'number'
	placeholder?: string
	onChange: (data: T) => void
	onKeyUp?: (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => void
	label?: string
	autoFocus?: boolean
	disabled?: boolean
	value: T
	className?: string
}
export default function IsyAnimatedInput<T>(props: IsyAnimatedInputProps<T>) {
	const [showPassword, setShowPassword] = React.useState(false)

	const handleClickShowPassword = () => {
		setShowPassword(!showPassword)
	}

	const getType = () => {
		if (isNil(props.type)) {
			return 'text'
		}
		if (props.type === 'password') {
			return showPassword ? 'text' : 'password'
		}
		return props.type
	}

	const handleOnKeyUp = (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		if (isFunction(props.onKeyUp)) {
			props.onKeyUp(event)
		}
	}

	const handleOnChange = (
		event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		if (isFunction(props.onChange)) {
			props.onChange(event.target.value as any)
		}
	}

	const renderEndAdornment = () => {
		if (props.type === 'password' && (props.value as any).length > 0) {
			return (
				<InputAdornment component='div' position='end'>
					<IconButton onClick={handleClickShowPassword}>
						{showPassword ? <Visibility /> : <VisibilityOff />}
					</IconButton>
				</InputAdornment>
			)
		}
		return null
	}

	const renderInput = () => {
		return (
			<FormControl className={'isy-animated-input ' + props.className}>
				<InputLabel>{props.label}</InputLabel>
				<InputText
					type={getType()}
					value={props.value}
					onChange={handleOnChange}
					disabled={props.disabled}
					autoFocus={props.autoFocus}
					endAdornment={renderEndAdornment()}
					onKeyUp={handleOnKeyUp}
					placeholder={props.placeholder}
				/>
			</FormControl>
		)
	}
	return renderInput()
}
