import React from 'react'
import { isFunction, isString, get, isNil, isEmpty } from 'lodash'
import moment from 'moment'
import CircularProgress from '@material-ui/core/CircularProgress'
import './isyGrid.scss'
import { SortDescIcon } from '../../icons/SortDesc'
import { SortAscIcon } from '../../icons/SortAsc'
import IsyAccordion from '../IsyAccordion/IsyAccordion'

export enum IsyGridType {
	FLAT_LIST,
	ACCORDION_HIERARCHICAL_LIST,
}

export enum IsyGridColumnTypes {
	DATE,
	STRING,
	STRING_ARRAY,
}

export interface IsyGridColumnProps {
	header: string | (() => React.ReactNode)
	type?: IsyGridColumnTypes
	format?: string
	separator?: string
	sortRef?: string
	dataRef:
		| string
		| ((
				row: any,
				column: IsyGridColumnProps,
				rowIndex: number,
				columnIndex: number
		  ) => React.ReactNode)
}

export interface IsyGridHierarchicalListItemProps {
	id?: string
	name: string
	children: any[]
}

export interface IsyGridProps {
	type?: IsyGridType
	data: any[] | IsyGridHierarchicalListItemProps[]
	config: IsyGridColumnProps[]
	isLoading?: boolean
	emptyMessage?: string
	onRowClick?: (
		data: any,
		e: React.MouseEvent<HTMLDivElement, MouseEvent>
	) => void
	rowCustomClass?: string | ((row: any) => string)
	hideHeader?: boolean
	sortBy?: string
	isSortDesc?: boolean
	onSortChange?: (sortBy: string, isDesc: boolean) => void
}

export function IsyGrid(props: IsyGridProps) {
	const { config, data, isLoading, emptyMessage } = props

	const getEmptyMessage = () => {
		if (isString(emptyMessage)) {
			return emptyMessage
		}
		return ''
	}

	const getRowCustomClassName = (row: Object) => {
		if (isFunction(props.rowCustomClass)) {
			return props.rowCustomClass(row)
		}
		if (isString(props.rowCustomClass)) {
			return props.rowCustomClass
		}
		return ''
	}

	const getHeaderCell = (column: IsyGridColumnProps) => {
		if (isFunction(column.header)) {
			return column.header()
		}
		if (isString(column.header)) {
			return column.header
		}
		return ''
	}

	const getDataCell = (
		row: Object,
		column: IsyGridColumnProps,
		rowIndex: number,
		columnIndex: number
	) => {
		if (isFunction(column.dataRef)) {
			return column.dataRef(row, column, rowIndex, columnIndex)
		}
		switch (column.type) {
			case IsyGridColumnTypes.DATE: {
				const data = get(row, column.dataRef)
				return moment(data).format(
					isNil(column.format) ? 'DD/MM/YYYY, H:mm a' : column.format
				)
			}
			case IsyGridColumnTypes.STRING_ARRAY:
				return (get(row, column.dataRef) || []).join(column.separator)
			case IsyGridColumnTypes.STRING:
			default:
				return get(row, column.dataRef)
		}
	}

	const handleOnRowClick = (
		data: Object,
		e: React.MouseEvent<HTMLDivElement, MouseEvent>
	) => {
		if (isFunction(props.onRowClick)) {
			props.onRowClick(data, e)
		}
	}

	const handleSortBy = (column: IsyGridColumnProps) => {
		if (!isNil(column.sortRef) && isFunction(props.onSortChange)) {
			if (column.sortRef === props.sortBy) {
				props.onSortChange(column.sortRef, !props.isSortDesc)
			} else {
				props.onSortChange(column.sortRef, false)
			}
		}
	}

	const renderLoadingOrEmptyMessage = () => {
		if (isLoading) {
			return (
				<div className='isy-grid-loader-holder'>
					<CircularProgress className='isy-grid-loader' />
				</div>
			)
		} else if (data.length === 0) {
			return (
				<div className='isy-grid-empty-container'>
					<div className='isy-grid-empty-message'>{getEmptyMessage()}</div>
				</div>
			)
		}
	}

	const renderDataCell = (
		row: Object,
		column: IsyGridColumnProps,
		rowIndex: number,
		columnIndex: number
	) => {
		return (
			<div
				className={'isy-grid-row-cell column-' + (columnIndex + 1)}
				key={columnIndex}
			>
				{getDataCell(row, column, rowIndex, columnIndex)}
			</div>
		)
	}

	const renderRow = (row: Object, index: number) => {
		return (
			<div
				className={
					'isy-grid-row row-' + (index + 1) + ' ' + getRowCustomClassName(row)
				}
				key={index}
				onClick={(e) => handleOnRowClick(row, e)}
				tabIndex={isFunction(props.onRowClick) ? 1 : undefined}
			>
				{config.map((column, cIndex) =>
					renderDataCell(row, column, index, cIndex)
				)}
			</div>
		)
	}

	const renderAccordion = (
		row: IsyGridHierarchicalListItemProps,
		index: number
	) => {
		return (
			<IsyAccordion
				key={index}
				header={row.name + '(' + row.children.length + ')'}
				id={row.id}
				isExpandByDefault={true}
			>
				{(row.children as any[]).map(renderRow)}
			</IsyAccordion>
		)
	}

	const renderDataByType = () => {
		switch (props.type) {
			case IsyGridType.ACCORDION_HIERARCHICAL_LIST:
				return (data as IsyGridHierarchicalListItemProps[]).map(renderAccordion)
			case IsyGridType.FLAT_LIST:
			default:
				return (data as any[]).map(renderRow)
		}
	}

	const renderData = () => {
		return <div className='isy-grid-body'>{renderDataByType()}</div>
	}

	const renderSort = (column: IsyGridColumnProps) => {
		if (!isNil(column.sortRef) && column.sortRef === props.sortBy) {
			if (props.isSortDesc) {
				return <SortDescIcon />
			}
			return <SortAscIcon />
		}
		return null
	}

	const renderHeaderCell = (column: IsyGridColumnProps, index: number) => {
		return (
			<div
				className={
					(!isEmpty(column.sortRef) ? 'sort-by' : '') +
					' isy-grid-header-cell column-' +
					(index + 1)
				}
				key={index}
				onClick={() => handleSortBy(column)}
			>
				{getHeaderCell(column)}
				{renderSort(column)}
			</div>
		)
	}

	const renderHeader = () => {
		return (
			<div
				className='isy-grid-header'
				tabIndex={
					isFunction(config[0].header) || !isNil(config[0].sortRef)
						? 1
						: undefined
				}
			>
				{config.map(renderHeaderCell)}
			</div>
		)
	}

	const renderGrid = () => {
		return (
			<div className='isy-grid-container'>
				{!props.hideHeader && renderHeader()}
				{renderData()}
				{renderLoadingOrEmptyMessage()}
			</div>
		)
	}
	return renderGrid()
}
