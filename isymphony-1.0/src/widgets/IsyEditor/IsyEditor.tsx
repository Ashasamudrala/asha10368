import React from 'react'
import { UnControlled as CodeMirror } from 'react-codemirror2'
import 'codemirror/lib/codemirror.css'
import 'codemirror/addon/fold/foldgutter.css'
import { isFunction } from 'lodash'

require('codemirror/addon/fold/foldcode')
require('codemirror/addon/fold/foldgutter')
require('codemirror/addon/fold/brace-fold')
require('codemirror/addon/fold/xml-fold')
require('codemirror/addon/fold/indent-fold')
require('codemirror/addon/fold/markdown-fold')
require('codemirror/addon/fold/comment-fold')
require('codemirror/mode/javascript/javascript')
require('codemirror/mode/xml/xml')
require('codemirror/mode/htmlmixed/htmlmixed')
require('codemirror/mode/python/python')
require('codemirror/mode/markdown/markdown')

export interface IsyEditorProps {
	value: string
	options: CodeMirror.EditorConfiguration

	onChange: (value: string) => void
}

export class IsyEditor extends React.Component<IsyEditorProps> {
	handleBlur = (editor: CodeMirror.Editor) => {
		const value = editor.getValue()
		if (isFunction(this.props.onChange)) {
			this.props.onChange(value)
		}
	}

	render() {
		return (
			<CodeMirror
				value={this.props.value}
				options={{
					lineNumbers: true,
					lineWrapping: true,
					tabSize: 2,
					gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
					...this.props.options,
				}}
				onBlur={this.handleBlur}
			/>
		)
	}
}
