import React from 'react'
import { Switch, withStyles } from '@material-ui/core'
import { isBoolean, isFunction } from 'lodash'
import './IsyCheckbox.scss'

export interface IsySwitchProps {
	label?: string
	value: boolean
	disabled?: boolean
	onChange: (value: boolean) => void
}

const CustomSwitch = withStyles({
	switchBase: {
		color: '#e34d28',
		float: 'right',
		'&$checked': {
			color: '#e34d28',
		},
		'&$checked + $track': {
			backgroundColor: '#e34d28',
		},
	},
	checked: {},
	track: {},
})(Switch)

export function IsySwitch(props: IsySwitchProps) {
	const getCheckedState = () => {
		if (isBoolean(props.value)) {
			return props.value
		}
		return false
	}

	const handleClick = (
		_: React.ChangeEvent<HTMLInputElement>,
		checked: boolean
	) => {
		if (isFunction(props.onChange)) {
			props.onChange(checked)
		}
	}

	const renderSwitch = () => {
		return (
			<div className='isy-switch'>
				<CustomSwitch
					className='isy-switch-control'
					checked={getCheckedState()}
					disabled={props.disabled}
					onChange={handleClick}
				/>
				<span className='isy-switch-label'>{props.label}</span>
			</div>
		)
	}
	return renderSwitch()
}
