declare module 'multiselect-react-dropdown' {
	import * as React from 'react'
	export interface MultiselectState {
		toggleOptionsList: boolean
	}
	export interface MultiselectOptionProps {
		id: string
		name: string
		[key: string]: any
	}
	export interface MultiselectProps {
		className?: string
		options?: Array<string | MultiselectOptionProps>
		onSelect: (selectedList: string[], selectedItem: string) => void
		onRemove: (selectedList: string[], removedItem: string) => void
		singleSelect?: boolean
		selectedValues: string[]
		showCheckbox?: boolean
		selectionLimit?: number
		placeholder?: string
		disablePreSelectedValues?: boolean
		isObject?: boolean
		displayValue?: string
		emptyRecordMsg?: string
		groupBy?: string
		closeIcon?: 'circle' | 'circle2' | 'cancel' | 'close'
		style?: React.CSSProperties
		caseSensitiveSearch?: boolean
		closeOnSelect?: boolean
		id?: string
		avoidHighlightFirstOption?: boolean
		hidePlaceholder?: boolean
		disable?: boolean
		onSearch?: () => void
		loading?: boolean
		loadingMessage?: string | React.ReactNode
	}
	export class Multiselect extends React.Component<
		MultiselectProps,
		MultiselectState
	> {
		searchBox = React.createRef<HTMLInputElement>()
	}
}
