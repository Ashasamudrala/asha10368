import React from 'react'
import { isFunction, isBoolean, isArray, isNil, isEmpty } from 'lodash'
import './IsyFileInput.scss'

export interface IsyInputProps {
	value: FileList | File
	disabled?: boolean
	className?: string
	accept?: string
	onChange?: (value: FileList | File) => void
	multiple?: boolean
}

export class IsyFileInput extends React.Component<IsyInputProps> {
	inputRef: HTMLInputElement | null = null

	getInputValue = (): string => {
		if (isArray(this.props.value)) {
			if (this.props.multiple) {
				const names = this.props.value.map((f) => f.name)
				return names.join(', ')
			}
			return this.props.value[0].name
		} else if (!isNil(this.props.value)) {
			return (this.props.value as File).name
		}
		return ''
	}

	getIsDisabled = () => {
		if (isBoolean(this.props.disabled)) {
			return this.props.disabled
		}
		return false
	}

	handleRef = (instance: HTMLInputElement | null) => {
		this.inputRef = instance
	}

	handleOnChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
		const files = evt.target.files
		if (isFunction(this.props.onChange) && !isNil(files) && files.length > 0) {
			this.props.onChange(this.props.multiple ? files : files[0])
		}
	}

	handleOnClick = () => {
		if (!isNil(this.inputRef)) {
			this.inputRef.click()
		}
	}

	render() {
		const value = this.getInputValue()
		return (
			<div className='isy-file-input' onClick={this.handleOnClick}>
				<span className='select-label' title={value}>
					{isEmpty(value) ? 'Select file' : value}
				</span>
				<input
					type='file'
					ref={this.handleRef}
					className={'isy_file_input ' + this.props.className}
					accept={this.props.accept}
					disabled={this.getIsDisabled()}
					onChange={this.handleOnChange}
					multiple={this.props.multiple || false}
					hidden
				/>
			</div>
		)
	}
}
