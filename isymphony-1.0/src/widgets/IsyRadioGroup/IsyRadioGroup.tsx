import React from 'react'
import { isArray, isEmpty, isFunction } from 'lodash'
import './IsyRadioGroup.scss'
import { IsyRadio } from '../IsyRadio/IsyRadio'

export interface IsyRadioGroupItemProps<T> {
	value: T
	label: string
}

export interface IsyRadioGroupProps<T> {
	title?: string
	required?: boolean
	value: T
	options: Array<IsyRadioGroupItemProps<T>>
	disabled?: boolean
	onChange: (value: T) => void
}

export function IsyRadioGroup<T>(props: IsyRadioGroupProps<T>) {
	const getOptions = () => {
		if (isArray(props.options)) {
			return props.options
		}
		return []
	}

	const handleOnChange = (newValue: T) => {
		if (
			props.value !== newValue &&
			isFunction(props.onChange) &&
			!props.disabled
		) {
			props.onChange(newValue)
		}
	}

	const renderRadio = (item: IsyRadioGroupItemProps<T>) => {
		return (
			<IsyRadio
				label={item.label}
				key={item.value as any}
				onChange={() => handleOnChange(item.value)}
				selected={props.value === item.value}
				disabled={props.disabled}
			/>
		)
	}

	const renderTitle = () => {
		if (isEmpty(props.title)) {
			return null
		}
		return (
			<label className='isy-radio-group-label'>
				{props.title}
				{props.required && <span className='isy-radio-group-required'> *</span>}
			</label>
		)
	}

	return (
		<div className='isy-radio-group'>
			{renderTitle()}
			{getOptions().map(renderRadio)}
		</div>
	)
}
