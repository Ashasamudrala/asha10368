import React from 'react'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

export interface IsyAccordionProps {
	id?: string
	header: string | React.ReactNode
	children?: React.ReactNode | React.ReactNode[]
	isExpandByDefault?: boolean
	isDisable?: boolean
}
export default function IsyAccordion(props: IsyAccordionProps) {
	const renderExpandIcon = () => {
		return <ExpandMoreIcon className='isy-accordion-summary-icon' />
	}

	return (
		<Accordion
			id={props.id}
			className='isy-accordion'
			defaultExpanded={props.isExpandByDefault}
			disabled={props.isDisable}
		>
			<AccordionSummary
				className='isy-accordion-summary'
				expandIcon={renderExpandIcon()}
			>
				{props.header}
			</AccordionSummary>
			{props.children}
		</Accordion>
	)
}
