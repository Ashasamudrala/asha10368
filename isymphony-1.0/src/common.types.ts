export interface Action<T> {
	type: string
	payload: T
}

export interface RouteProps {
	id: string
	path: string
	title?: string
	includeInTopBar?: boolean
	hasBreadCrumb?: boolean
	containerClass?: string
	hasSideBar?: boolean
	isSideBar?: boolean
	children?: RouteProps[]
}

export interface RouteConfigProps {
	default: string
	list: RouteProps[]
}
