import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Switch, Route, Redirect, useHistory } from 'react-router-dom'
import { Container } from '@material-ui/core'
import routesConfig from './config/routes.json'
import { NotificationPopup } from './infrastructure/notificationPopup/NotificationPopup'
import { TopNavigation } from './features/topNavigation/TopNavigation'
import { IsySideBar } from './widgets/IsySideBar/IsySideBar'
import { selectGetLoginUserId } from './authAndPermissions/loginUserDetails.selectors'
import { isNil } from 'lodash'
import {
	logInUserOnRefresh,
	loadUserProfileInfo,
} from './authAndPermissions/loginUserDetails.asyncActions'
import { Dialogs } from './features/dialogs/Dialogs'
import { getComponentsBasedOnRouteId } from './utilities/routes'
import { IsyBreadCrumbPage } from './widgets/IsyBreadCrumbPage/IsyBreadCrumbPage'
import { RouteConfigProps, RouteProps } from './common.types'
import { IsyGlobalLoader } from './widgets/IsyBusyIndicator/IsyGlobalLoader'

function PageRoute({
	children,
	className,
	...rest
}: {
	children: any
	className?: string
	path: string
}) {
	return (
		<Route {...rest}>
			<Container className={className} fixed={false}>
				{children}
			</Container>
		</Route>
	)
}

export function Routes() {
	const logInUserId = useSelector(selectGetLoginUserId)
	const history = useHistory()
	const currentPath = window.location.href
	const dispatch = useDispatch()

	useEffect(() => {
		if (isNil(logInUserId)) {
			const jwtToken = sessionStorage.getItem('accessToken')
			if (isNil(jwtToken)) {
				if (
					currentPath.indexOf('activeInviteSignUp') === -1 &&
					currentPath.indexOf('resetpassword') === -1
				) {
					history.push('/login')
				}
			} else {
				;(dispatch(logInUserOnRefresh()) as any).then(() => {
					setTimeout(() => {
						// timeout for dispatch flow to complete
						dispatch(loadUserProfileInfo())
					})
				})
			}
		}
	})

	const renderTopNavigation = () => {
		if (!isNil(logInUserId)) {
			return <TopNavigation routes={(routesConfig as RouteConfigProps).list} />
		}
	}

	const renderRoute = (route: RouteProps) => {
		return (
			<PageRoute
				className={'content-container ' + route.containerClass}
				path={route.path}
				key={route.id}
			>
				{getComponentsBasedOnRouteId(route.id)}
			</PageRoute>
		)
	}

	const renderSideBar = (route: RouteProps) => {
		if (!isNil(route.children)) {
			return (
				<Route path={route.path} key={route.id}>
					<IsySideBar routes={route.children} />
				</Route>
			)
		}
		return renderRoute(route)
	}

	const renderBreadCrumb = (route: RouteProps) => {
		return (
			<Route path={route.path} key={route.id}>
				<IsyBreadCrumbPage route={route} />
			</Route>
		)
	}

	const renderRoutesList = () => {
		return (routesConfig as RouteConfigProps).list.map((route) => {
			if (route.hasSideBar) {
				return renderSideBar(route)
			} else if (route.hasBreadCrumb) {
				return renderBreadCrumb(route)
			}
			return renderRoute(route)
		})
	}

	const render = () => {
		return (
			<>
				{renderTopNavigation()}
				<Switch>
					<Redirect
						exact
						from='/'
						to={(routesConfig as RouteConfigProps).default}
					/>
					{renderRoutesList()}
				</Switch>
				<Dialogs />
				<IsyGlobalLoader />
				<div className={'notification-popup-holder'}>
					<NotificationPopup />
				</div>
			</>
		)
	}

	return render()
}
