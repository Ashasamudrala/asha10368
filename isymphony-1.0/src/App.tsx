import React, { Suspense, useState } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Container } from '@material-ui/core'
import { Routes } from './Routes'
import './app.scss'

function App() {
	const themeNames = { dark: `dark-theme`, light: `light-theme` }
	const [themeName] = useState(themeNames.light)

	return (
		<div className={themeName}>
			<div className='main-container'>
				<Suspense fallback='loading'>
					<Router>
						<Container className='page'>
							<Routes />
						</Container>
					</Router>
				</Suspense>
			</div>
		</div>
	)
}

export default App
