import React, { useState } from 'react'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import { useTranslation } from 'react-i18next'
import {
	ACCELERATOR_NO_AI_FOUND,
	ACCELERATOR_TRANSLATIONS,
	ACCELERATOR_VENDOR_NAME,
	DATE_CREATED,
	DELETE,
	DESCRIPTION,
	DUPLICATE,
	LAST_MODIFIED,
	OWNER,
} from '../../../../utilities/constants'
import {
	AcceleratorInstanceProps,
	AcceleratorsCardMoreOver,
	CategoriesProps,
	CategoriesToAcceleratorInstancesMap,
} from '../../accelerators.types'
import {
	IsyGrid,
	IsyGridColumnTypes,
	IsyGridHierarchicalListItemProps,
	IsyGridType,
} from '../../../../widgets/IsyGrid/IsyGrid'
import {
	AcceleratorSortBy,
	OrderBy,
} from '../../../../utilities/apiEnumConstants'
import { getAcceleratorsIconUrl } from '../../accelerators.utilities'
import { isNil } from 'lodash'
import { IsyPopover } from '../../../../widgets/IsyPopover/IsyPopover'
import './acceleratorsTableView.scss'

export interface AcceleratorsTableViewProps {
	categories: CategoriesProps[]
	instancesMap: CategoriesToAcceleratorInstancesMap
	orderBy: OrderBy
	highlightAppId: string | null
	sortBy: AcceleratorSortBy
	includeDuplicate: boolean
	onSelect: (data: AcceleratorInstanceProps) => void
	onDelete: (data: AcceleratorInstanceProps) => void
	onDuplicate: (data: AcceleratorInstanceProps) => void
	onOrderAndSortByChange: (key: OrderBy, sortBy: AcceleratorSortBy) => void
}

export function AcceleratorsTableView(props: AcceleratorsTableViewProps) {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)
	const [
		settingsData,
		setSettingsData,
	] = useState<AcceleratorInstanceProps | null>(null)

	const getPopoverActions = () => {
		const list = []
		if (props.includeDuplicate) {
			list.push({
				name: t(DUPLICATE),
				key: AcceleratorsCardMoreOver.DUPLICATE,
			})
		}
		list.push({
			name: t(DELETE),
			key: AcceleratorsCardMoreOver.DELETE,
		})
		return list
	}

	const gridConfig = () => [
		{
			header: '',
			dataRef: renderIcon,
		},
		{
			header: t(ACCELERATOR_VENDOR_NAME),
			type: IsyGridColumnTypes.STRING,
			dataRef: renderName,
			sortRef: AcceleratorSortBy.NAME,
		},
		{
			header: '',
			dataRef: renderMoreIcon,
		},
		{
			header: t(OWNER),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'createdByUserDisplayName',
		},
		{
			header: t(LAST_MODIFIED),
			type: IsyGridColumnTypes.DATE,
			dataRef: 'lastModifiedTimestamp',
			sortRef: AcceleratorSortBy.LAST_MODIFIED,
		},
		{
			header: t(DATE_CREATED),
			type: IsyGridColumnTypes.DATE,
			dataRef: 'creationTimestamp',
			sortRef: AcceleratorSortBy.CREATED_ON,
		},
		{
			header: t(DESCRIPTION),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'description',
		},
	]

	const getGridData = (): IsyGridHierarchicalListItemProps[] => {
		const data: IsyGridHierarchicalListItemProps[] = []
		for (let i = 0, iLen = props.categories.length; i < iLen; i++) {
			const category = props.categories[i]
			const list = props.instancesMap[category.id] || []
			if (list.length > 0) {
				data.push({
					id: category.name.replace(/ /g, '_'),
					name: category.name,
					children: list,
				})
			}
		}
		return data
	}

	const handleAppsSortChange = (sortBy: string, isDesc: boolean) => {
		props.onOrderAndSortByChange(
			isDesc ? OrderBy.DESC : OrderBy.ASC,
			sortBy as AcceleratorSortBy
		)
	}

	const handleShowMenu = (
		e: React.MouseEvent<SVGSVGElement>,
		item: AcceleratorInstanceProps
	) => {
		e.stopPropagation()
		setSettingsData(item)
		setPopoverAnchorEl(e.currentTarget)
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleViewItem = (item: AcceleratorInstanceProps) => {
		props.onSelect(item)
	}

	const handleMenuItems = (
		e: React.MouseEvent<HTMLLIElement>,
		selectedItem: string
	) => {
		e.stopPropagation()
		if (!isNil(settingsData)) {
			switch (selectedItem) {
				case AcceleratorsCardMoreOver.DUPLICATE: {
					props.onDuplicate(settingsData)
					break
				}
				case AcceleratorsCardMoreOver.DELETE: {
					props.onDelete(settingsData)
					break
				}
			}
		}
		setPopoverAnchorEl(null)
		setSettingsData(null)
	}

	const renderName = (item: AcceleratorInstanceProps) => {
		return (
			<>
				<span className='card-header-title' title={item.name}>
					{item.name}
				</span>
				{item.id === props.highlightAppId && (
					<img
						alt='new'
						id='new-icon'
						className='new-icon'
						src='/images/acceleratorsIcons/newBadge.svg'
					/>
				)}
			</>
		)
	}

	const renderIcon = (item: AcceleratorInstanceProps) => {
		return (
			<img
				src={getAcceleratorsIconUrl(item.imageRef)}
				alt={item.imageRef}
				className='image'
			/>
		)
	}

	const renderMoreIcon = (item: AcceleratorInstanceProps) => {
		return (
			<MoreVertIcon
				className='more-icon'
				onClick={(e) => handleShowMenu(e, item)}
			/>
		)
	}

	const renderPopover = () => {
		if (isNil(popoverAnchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={getPopoverActions()}
				className={'pop-over-menu-list'}
				onClose={handlePopoverClose}
				onSelect={handleMenuItems}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			/>
		)
	}

	return (
		<div className='accelerators-table-view'>
			<IsyGrid
				type={IsyGridType.ACCORDION_HIERARCHICAL_LIST}
				data={getGridData()}
				config={gridConfig()}
				emptyMessage={t(ACCELERATOR_NO_AI_FOUND)}
				onRowClick={handleViewItem}
				sortBy={props.sortBy}
				isSortDesc={props.orderBy === OrderBy.DESC}
				onSortChange={handleAppsSortChange}
			/>
			{renderPopover()}
		</div>
	)
}
