import React from 'react'
import { CategoriesProps } from '../../accelerators.types'
import {
	ACCELERATOR_NO_CATEGORIES_TO_VIEW,
	ACCELERATOR_TRANSLATIONS,
	CATEGORIES,
} from '../../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import { isFunction } from 'lodash'

export interface AcceleratorsCategoriesProps {
	categories: CategoriesProps[]
	activeCategoryId: string
	onSelectedCategory: (id: string) => void
	updateSteps?: (stepNumber: number) => void
}

export function AcceleratorsCategories(props: AcceleratorsCategoriesProps) {
	const { categories, activeCategoryId } = props
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)

	const handleSelectedCategory = (category: CategoriesProps) => {
		props.onSelectedCategory(category.id)
		if (isFunction(props.updateSteps)) {
			props.updateSteps(2)
		}
	}

	const renderCategory = (category: CategoriesProps, index: number) => {
		const name = `${category.name.replace(/ /g, '_')}`
		return (
			<a
				onClick={() => handleSelectedCategory(category)}
				className={`${
					activeCategoryId === category.id
						? 'list-selected isy-steps-selected-category '
						: ''
				}`}
				href={'#' + name}
				key={index}
			>
				{category.name}
			</a>
		)
	}

	return (
		<>
			<span className='category-filter'>{t(CATEGORIES)}</span>
			{categories.length === 0 ? (
				<span className='no-categories'>
					{t(ACCELERATOR_NO_CATEGORIES_TO_VIEW)}
				</span>
			) : (
				<div className='list-container'>{categories.map(renderCategory)}</div>
			)}
		</>
	)
}
