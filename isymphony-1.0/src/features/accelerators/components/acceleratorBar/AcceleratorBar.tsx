import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { isNil } from 'lodash'
import {
	ACCELERATOR_TRANSLATIONS,
	DESCENDING,
	LAST_MODIFIED,
	ALPHABETICAL,
	ASCENDING,
	DATE_CREATED,
	OWNER,
} from '../../../../utilities/constants'
import { CardViewIcon } from '../../../../icons/CardView'
import { IsyPopover } from '../../../../widgets/IsyPopover/IsyPopover'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import CheckIcon from '@material-ui/icons/Check'
import DehazeIcon from '@material-ui/icons/Dehaze'
import {
	AcceleratorSortBy,
	OrderBy,
} from '../../../../utilities/apiEnumConstants'
import { AcceleratorsViewType } from '../../accelerators.types'
import './acceleratorBar.scss'

export interface AcceleratorBarProps {
	orderBy: OrderBy
	sortBy: AcceleratorSortBy
	viewType: AcceleratorsViewType
	onViewTypeChange: (viewType: AcceleratorsViewType) => void
	onOrderByChange: (key: OrderBy) => void
	onSortByChange: (key: AcceleratorSortBy) => void
	onCloseSortBy: () => void
}

export function AcceleratorBar(props: AcceleratorBarProps) {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)

	const sortOptions = () => [
		{
			key: AcceleratorSortBy.LAST_MODIFIED,
			hasIcon: props.sortBy === AcceleratorSortBy.LAST_MODIFIED,
			icon: <CheckIcon />,
			name: t(LAST_MODIFIED),
		},
		{
			key: AcceleratorSortBy.CREATED_ON,
			hasIcon: props.sortBy === AcceleratorSortBy.CREATED_ON,
			icon: <CheckIcon />,
			name: t(DATE_CREATED),
		},
		{
			key: AcceleratorSortBy.OWNER,
			hasIcon: props.sortBy === AcceleratorSortBy.OWNER,
			icon: <CheckIcon />,
			name: t(OWNER),
		},
		{
			key: AcceleratorSortBy.NAME,
			hasIcon: props.sortBy === AcceleratorSortBy.NAME,
			icon: <CheckIcon />,
			name: t(ALPHABETICAL),
			menuClass: 'border-bottom',
		},
		{
			key: OrderBy.DESC,
			hasIcon: props.orderBy === OrderBy.DESC,
			icon: <CheckIcon />,
			name: t(ASCENDING),
		},
		{
			key: OrderBy.ASC,
			hasIcon: props.orderBy === OrderBy.ASC,
			icon: <CheckIcon />,
			name: t(DESCENDING),
		},
	]

	const getSortByDisplayName = () => {
		switch (props.sortBy) {
			case AcceleratorSortBy.LAST_MODIFIED:
				return t(LAST_MODIFIED)
			case AcceleratorSortBy.CREATED_ON:
				return t(DATE_CREATED)
			case AcceleratorSortBy.OWNER:
				return t(OWNER)
			case AcceleratorSortBy.NAME:
				return t(ALPHABETICAL)
			default:
				return null
		}
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setPopoverAnchorEl(null)
		props.onCloseSortBy()
	}

	const handlePopoverShow = (event: any) => {
		setPopoverAnchorEl(event.currentTarget)
	}

	const handleViewTypeChange = (type: AcceleratorsViewType) => {
		props.onViewTypeChange(type)
	}

	const handleMenuItems = (_: React.MouseEvent<HTMLLIElement>, key: string) => {
		switch (key) {
			case OrderBy.ASC:
			case OrderBy.DESC:
				props.onOrderByChange(key)
				break
			default:
				props.onSortByChange(key as AcceleratorSortBy)
		}
	}

	const renderPopover = () => {
		if (isNil(popoverAnchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={sortOptions()}
				className={'accelerator-bar-popover'}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
			/>
		)
	}
	const renderSortOptions = () => {
		return (
			<>
				<button onClick={handlePopoverShow} className='pop-btn'>
					{getSortByDisplayName()}{' '}
					{props.orderBy === OrderBy.DESC ? (
						<ArrowDownwardIcon />
					) : (
						<ArrowUpwardIcon />
					)}
				</button>
				<span className='border-right'></span>
				{renderPopover()}
			</>
		)
	}
	const renderViewByOptions = () => {
		return (
			<>
				<span
					className={
						'list-view ' +
						(props.viewType === AcceleratorsViewType.LIST ? 'select-icon' : '')
					}
					onClick={() => handleViewTypeChange(AcceleratorsViewType.LIST)}
				>
					<DehazeIcon />
				</span>
				<span className='border-right'></span>
				<span
					className={
						'card-view ' +
						(props.viewType === AcceleratorsViewType.GRID ? 'select-icon' : '')
					}
					onClick={() => handleViewTypeChange(AcceleratorsViewType.GRID)}
				>
					<CardViewIcon />
				</span>
			</>
		)
	}
	return (
		<div className='accelerator-bar'>
			<span className='flex-grow'></span>
			{props.viewType === AcceleratorsViewType.GRID && renderSortOptions()}
			{renderViewByOptions()}
		</div>
	)
}
