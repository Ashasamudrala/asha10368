import React from 'react'
import { CardViewSecondIcon } from '../../../../icons/CardViewSecond'
import { MarketPlaceCard } from './MarketPlaceCard/MarketPlaceCard'
import { useTranslation } from 'react-i18next'
import {
	ACCELERATOR_TRANSLATIONS,
	NO_ACCELERATORS,
} from '../../../../utilities/constants'
import './marketPlaceGrid.scss'
import { MarketplaceProps } from '../../accelerators.types'
import { isFunction } from 'lodash'

export interface MarketPlaceGridProps {
	items: MarketplaceProps[]
	className?: string
	onSelect: (data: MarketplaceProps) => void
	updateSteps?: (stepNumber: number) => void
}

export function MarketPlaceGrid(props: MarketPlaceGridProps) {
	const { items, className } = props
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)

	const handleUpdateSteps = (stepNumber: number) => {
		if (isFunction(props.updateSteps)) {
			props.updateSteps(stepNumber)
		}
	}

	const renderNoCardsView = () => {
		return (
			<div className='empty-card-view'>
				<CardViewSecondIcon className='icon' />
				<div className='text'>{t(NO_ACCELERATORS)}</div>
			</div>
		)
	}

	const renderList = () => {
		return items.map((data: MarketplaceProps, i: number) => (
			<MarketPlaceCard
				className={i === 0 ? className : ''}
				onSelect={props.onSelect}
				data={data}
				key={i}
				updateSteps={handleUpdateSteps}
			/>
		))
	}

	return (
		<div className='accelerators-grid-card-container'>
			{items.length === 0 ? renderNoCardsView() : renderList()}
		</div>
	)
}
