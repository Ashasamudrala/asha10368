import { AcceleratorInstanceState } from './acceleratorInstance/acceleratorInstance.types'
import { AcceleratorsState } from './accelerators.types'

export enum AcceleratorsSubReducersNames {
	ACCELERATORS = 'accelerators',
	ACCELERATOR_INSTANCE = 'accelerator_instance',
}

export interface AcceleratorsBaseState {
	[AcceleratorsSubReducersNames.ACCELERATORS]: AcceleratorsState
	[AcceleratorsSubReducersNames.ACCELERATOR_INSTANCE]: AcceleratorInstanceState
}
