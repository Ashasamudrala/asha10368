import {
	AppType,
	AcceleratorSortBy,
	OrderBy,
} from '../../utilities/apiEnumConstants'

export enum AcceleratorsViewType {
	GRID = 'GRID',
	LIST = 'LIST',
}

export interface CategoriesProps {
	id: string
	name: string
}

export interface CategoriesToMarketPlaceMap {
	[key: string]: MarketplaceProps[]
}

export interface MarketplaceMetadataProps {
	imageRef: string
}
export interface MarketplaceConfigurationUiConfigurationMetadataProps {
	renderAs: string
	placeholderValue: string
	outOfForm?: boolean
	optionsRef?: string
}

export interface MarketplaceConfigurationUiConfigurationProps {
	metadata: MarketplaceConfigurationUiConfigurationMetadataProps
}

export interface MarketplaceConfigurationFieldConstraintsProps {
	length: number
	required: 'true' | 'false' | boolean
	min: number
	max: number
	regex?: string
	errorMessage?: string
}
export interface MarketplaceConfigurationFieldProps {
	name: string
	displayName: string
	type: string
	fields: MarketplaceConfigurationFieldProps[]
	uiConfiguration: MarketplaceConfigurationUiConfigurationProps
	constraints: MarketplaceConfigurationFieldConstraintsProps
}

export interface MarketplaceConfigurationSectionProps {
	name: string
	displayName: string
	uiConfiguration: MarketplaceConfigurationUiConfigurationProps
	fields: MarketplaceConfigurationFieldProps[]
}

export interface MarketplaceConfigurationProps {
	sections: MarketplaceConfigurationSectionProps[]
}

export interface MarketplaceProps {
	id: string
	name: string
	description: string
	categoryId: string
	metadata: MarketplaceMetadataProps
	configuration: MarketplaceConfigurationProps
}

export interface AcceleratorConfigurationProps {
	sections: any
}

export interface AcceleratorInstanceProps {
	id: string
	name: string
	description: string
	acceleratorId: string
	acceleratorName: string
	applicationId: string
	categoryId: string
	categoryName: string
	configuration: AcceleratorConfigurationProps
	imageRef: string
	createdByUserDisplayName: string
	createdUserProfilePic: string
	creationTimestamp: string
	lastModifiedTimestamp: string
}
export interface CategoriesToAcceleratorInstancesMap {
	[key: string]: AcceleratorInstanceProps[]
}

export interface AcceleratorsAppItemProps {
	id: string
	name: string
	type: AppType
}

export interface AcceleratorsState {
	activeTab: number
	activeCategoryId: string
	categoriesData: CategoriesProps[]
	categoriesToMarketPlaceMap: CategoriesToMarketPlaceMap
	categoriesToAcceleratorInstancesMap: CategoriesToAcceleratorInstancesMap
	appsList: AcceleratorsAppItemProps[]
	selectedApps: string[]
	searchString: string
	sortBy: AcceleratorSortBy
	orderBy: OrderBy
	viewType: AcceleratorsViewType
	highlightApp: string | null
}

export enum AcceleratorsCardMoreOver {
	DUPLICATE = 'duplicate',
	DELETE = 'delete',
}
