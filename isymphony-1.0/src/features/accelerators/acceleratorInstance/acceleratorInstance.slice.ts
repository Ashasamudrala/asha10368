import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { AcceleratorsSubReducersNames } from '../base.types'
import { Action } from '../../../common.types'
import * as asyncActions from './acceleratorInstance.asyncActions'
import {
	AcceleratorConfigurationValuesProps,
	AcceleratorInstanceAppOption,
	AcceleratorInstanceSetIdsActionState,
	AcceleratorInstanceState,
	AcceleratorInstanceViewState,
	AcceleratorLockStatus,
} from './acceleratorInstance.types'
import { cloneDeep, isArray, isEmpty, isNil } from 'lodash'
import {
	buildDataObjectFromBackendConfig,
	clearDropDownOptionsData,
	filterDeep,
	mergeDeep,
} from './acceleratorInstance.utilities'
import i18n from 'i18next'
import { ACCELERATOR_DUPLICATE_NAME } from '../../../utilities/constants'

const initialState: AcceleratorInstanceState = {
	categoryId: null,
	acceleratorId: null,
	acceleratorInstanceId: null,
	acceleratorInstanceAppId: null,
	state: AcceleratorInstanceViewState.NONE,
	acceleratorData: null,
	optionsObject: {},
	configurationValues: {},
	appOptions: [],
	userDetails: null,
	prevName: null,
	prevAssignTo: null,
	savedInstanceData: null,
	errorList: {},
	isDuplicate: false,
	attributeTypes: [],
	lockStatus: AcceleratorLockStatus.UN_KNOWN,
	lockedBy: null,
	prevConfigurationValues: {},
	prevAcceleratorInstanceId: null,
}

const slice = createSlice<
	AcceleratorInstanceState,
	SliceCaseReducers<AcceleratorInstanceState>,
	AcceleratorsSubReducersNames.ACCELERATOR_INSTANCE
>({
	name: AcceleratorsSubReducersNames.ACCELERATOR_INSTANCE,
	initialState,
	reducers: {
		setErrorList(state, action: Action<{ [key: string]: string }>) {
			state.errorList = action.payload
		},
		discardChanges(state) {
			state.configurationValues = cloneDeep(state.prevConfigurationValues)
		},
		clearOptionsObject(state) {
			const optionsToLoad = filterDeep(state.acceleratorData, 'optionsRef')
			for (let i = 0, iLen = optionsToLoad.length; i < iLen; i++) {
				state.optionsObject[optionsToLoad[i]] = null as any
			}
			if (
				!isNil(state.acceleratorData) &&
				!isNil(state.acceleratorData.configuration)
			) {
				state.configurationValues = cloneDeep(
					clearDropDownOptionsData(
						state.acceleratorData.configuration,
						state.configurationValues
					)
				)
			}
		},
		showAcceleratorInstance(
			state,
			action: Action<AcceleratorInstanceSetIdsActionState>
		) {
			state.categoryId = action.payload.categoryId
			state.acceleratorId = action.payload.acceleratorId
			state.acceleratorInstanceId = action.payload.acceleratorInstanceId
			state.prevAcceleratorInstanceId = action.payload.acceleratorInstanceId
			state.acceleratorInstanceAppId = action.payload.acceleratorInstanceAppId
			state.isDuplicate = action.payload.isDuplicate
			state.state =
				isNil(action.payload.acceleratorInstanceId) ||
				action.payload.isDuplicate
					? AcceleratorInstanceViewState.EDIT
					: AcceleratorInstanceViewState.VIEW
		},
		setViewCurrentState(state, action: Action<AcceleratorInstanceViewState>) {
			state.state = action.payload
		},
		setConfigurationValues(
			state,
			action: Action<AcceleratorConfigurationValuesProps>
		) {
			state.configurationValues = {
				...state.configurationValues,
				...action.payload,
			}
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(
			asyncActions.fetchAttributeTypes.fulfilled,
			(state, action) => {
				state.attributeTypes = action.payload.content
			}
		)
		builder.addCase(
			asyncActions.createNewAcceleratorInstance.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					state.state = AcceleratorInstanceViewState.SUCCESS
					state.savedInstanceData = action.payload
					state.prevConfigurationValues = cloneDeep(state.configurationValues)
				}
			}
		)
		builder.addCase(
			asyncActions.updateAcceleratorInstance.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					state.state =
						state.lockStatus === AcceleratorLockStatus.LOCK_ACQUIRED
							? AcceleratorInstanceViewState.EDIT
							: AcceleratorInstanceViewState.VIEW
					state.savedInstanceData = action.payload
					state.prevConfigurationValues = cloneDeep(state.configurationValues)
				}
			}
		)
		builder.addCase(
			asyncActions.addDeleteAcceleratorInstance.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					if (!action.meta.arg) {
						state.lockStatus = AcceleratorLockStatus.LOCK_PRESENT
					}
					state.acceleratorInstanceId = action.payload.id
					state.prevAssignTo = action.payload.applicationId
					state.acceleratorInstanceAppId = action.payload.applicationId
					state.state =
						state.lockStatus === AcceleratorLockStatus.LOCK_ACQUIRED
							? AcceleratorInstanceViewState.EDIT
							: AcceleratorInstanceViewState.VIEW
					state.savedInstanceData = action.payload
					state.prevConfigurationValues = cloneDeep(state.configurationValues)
				}
			}
		)
		builder.addCase(asyncActions.checkLockStatus.fulfilled, (state, action) => {
			if (isEmpty(action.payload)) {
				state.lockStatus = AcceleratorLockStatus.LOCK_PRESENT
				state.state = AcceleratorInstanceViewState.VIEW
			} else {
				const loggedInUserId = action.meta.arg
				if (loggedInUserId === action.payload.userId) {
					state.lockStatus = AcceleratorLockStatus.LOCK_ACQUIRED
					state.state = AcceleratorInstanceViewState.EDIT
				} else {
					state.lockStatus = AcceleratorLockStatus.LOCK_NOT_PRESENT
					state.lockedBy = action.payload.displayName
					state.state = AcceleratorInstanceViewState.VIEW
				}
			}
		})
		builder.addCase(asyncActions.acquireLock.fulfilled, (state, action) => {
			if (action.payload.response === 'ACQUIRED_LOCK') {
				state.lockStatus = AcceleratorLockStatus.LOCK_ACQUIRED
				state.state = AcceleratorInstanceViewState.EDIT
			} else {
				state.lockStatus = AcceleratorLockStatus.LOCK_NOT_PRESENT
				state.lockedBy = action.payload.displayName
				state.state = AcceleratorInstanceViewState.VIEW
			}
		})
		builder.addCase(asyncActions.releaseLock.fulfilled, (state, action) => {
			if (!isNil(action.payload)) {
				state.lockStatus = AcceleratorLockStatus.LOCK_PRESENT
				state.state =
					state.state === AcceleratorInstanceViewState.NONE
						? AcceleratorInstanceViewState.NONE
						: AcceleratorInstanceViewState.VIEW
			}
		})

		builder.addCase(
			asyncActions.loadDropDownOption.fulfilled,
			(state, action) => {
				if (action && action.payload && isArray(action.payload.content)) {
					state.optionsObject[action.meta.arg] = action.payload.content.map(
						(d: any) => ({
							id: d.name,
							name: isEmpty(d.displayName) ? d.name : d.displayName,
						})
					)
				} else {
					state.optionsObject[action.meta.arg] = []
				}
			}
		)

		builder.addCase(
			asyncActions.loadAcceleratorMetadata.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					state.acceleratorData = action.payload
					const optionsToLoad = filterDeep(action.payload, 'optionsRef')
					for (let i = 0, iLen = optionsToLoad.length; i < iLen; i++) {
						state.optionsObject[optionsToLoad[i]] = null as any
					}
					state.configurationValues = buildDataObjectFromBackendConfig(
						action.payload.configuration,
						state.appOptions
					)
				}
			}
		)
		builder.addCase(
			asyncActions.loadAcceleratorInstance.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					state.acceleratorData = {
						id: action.payload.acceleratorId,
						name: action.payload.acceleratorName,
						description: '',
						categoryId: action.payload.categoryId,
						metadata: {
							imageRef: action.payload.imageRef,
						},
						configuration: action.payload.configuration,
					}
					state.userDetails = {
						createdByUserDisplayName: action.payload.createdByUserDisplayName,
						createdUserProfilePic: action.payload.createdUserProfilePic,
						creationTimestamp: action.payload.creationTimestamp,
						lastModifiedTimestamp: action.payload.lastModifiedTimestamp,
					}
					const defaultConfigValues = buildDataObjectFromBackendConfig(
						action.payload.configuration,
						state.appOptions
					)
					const configValues = {
						...action.payload.configurationValues,
						name: action.payload.name,
						description: action.payload.description,
					}
					if (!state.isDuplicate) {
						configValues.assignToApp = action.payload.applicationId
						state.prevName = action.payload.name
						state.prevAssignTo = action.payload.applicationId
					} else {
						state.acceleratorInstanceId = null
						state.acceleratorInstanceAppId = null
						configValues.name = ''
					}
					state.configurationValues = mergeDeep(
						{},
						defaultConfigValues,
						configValues
					)
					state.prevConfigurationValues = cloneDeep(state.configurationValues)
				}
			}
		)
		builder.addCase(
			asyncActions.loadCurrentInstanceAppData.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					state.appOptions = [
						{
							id: action.payload.id,
							name: action.payload.name,
							type: action.payload.type,
						},
						...state.appOptions,
					]
				}
			}
		)
		builder.addCase(
			asyncActions.loadAcceleratorSupportApps.fulfilled,
			(state, action) => {
				if (action && action.payload && isArray(action.payload.content)) {
					const options: AcceleratorInstanceAppOption[] = []
					for (let i = 0, iLen = action.payload.content.length; i < iLen; i++) {
						options.push({
							id: action.payload.content[i].id,
							name: action.payload.content[i].name,
							type: action.payload.content[i].type,
						})
					}
					if (
						state.configurationValues.hasOwnProperty('assignToApp') &&
						isNil(state.configurationValues.assignToApp)
					) {
						state.configurationValues.assignToApp =
							options.length > 0 ? options[0].id : ''
					}
					state.appOptions = [...options, ...state.appOptions]
				}
			}
		)
		builder.addCase(
			asyncActions.validateAcceleratorInstance.fulfilled,
			(state, action) => {
				if (isEmpty(action.payload)) {
					let errorList = state.errorList
					if (errorList.name) {
						delete state.errorList.name
					}
				} else {
					state.errorList = Object.assign({}, state.errorList, {
						name: i18n.t(ACCELERATOR_DUPLICATE_NAME, {
							name: action.payload.categoryName,
						}),
					})
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
