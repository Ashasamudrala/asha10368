import { createAsyncThunk } from '@reduxjs/toolkit'
import { isEmpty } from 'lodash'
import { RootState } from '../../base.types'
import doAsync from '../../infrastructure/doAsync'
import {
	ACCELERATORS_GET_ACCELERATOR_INSTANCE,
	ACCELERATORS_GET_ALL_APPS,
	ACCELERATORS_GET_APPS_ACCELERATORS,
	ACCELERATORS_GET_APPS_ACCELERATORS_SEARCH,
	ACCELERATORS_GET_CATEGORIES,
	ACCELERATORS_GET_CATEGORIES_WITH_MARKET_PLACE,
	ACCELERATORS_GET_CATEGORIES_WITH_MARKET_PLACE_WITH_SEARCH,
} from '../../utilities/apiEndpoints'
import {
	getSearchString,
	getSelectedApps,
	selectOrderBy,
	selectSortBy,
} from './accelerators.selectors'
import { actions } from '../../authAndPermissions/loginUserDetails.slice'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { DialogTypes, showDialog } from '../dialogs'
import i18n from 'i18next'
import {
	HELP_ACCELERATORS_DESCRIPTION,
	HELP_ACCELERATORS_TITLE,
	HELP_SKIP_DESCRIPTION_SECTION,
	OK_BUTTON_LABEL,
	SKIP_TOUR,
	START_TOUR,
} from '../../utilities/constants'

export const loadAcceleratorApps = createAsyncThunk(
	'accelerators/loadAcceleratorApps',
	async (_: undefined, thunkArgs) => {
		return await doAsync({
			url: ACCELERATORS_GET_ALL_APPS(),
			...thunkArgs,
		})
	}
)

export const fetchCategoriesWithMarketPlace = createAsyncThunk(
	'accelerators/fetchCategoriesWithMarketPlace',
	async (_: undefined, thunkArgs) => {
		const searchString = getSearchString(thunkArgs.getState() as RootState)
		if (isEmpty(searchString)) {
			return await doAsync({
				url: ACCELERATORS_GET_CATEGORIES_WITH_MARKET_PLACE,
				...thunkArgs,
			})
		} else {
			return await doAsync({
				url: ACCELERATORS_GET_CATEGORIES_WITH_MARKET_PLACE_WITH_SEARCH(
					searchString
				),
				...thunkArgs,
			})
		}
	}
)

export const fetchCategories = createAsyncThunk(
	'accelerators/fetchCategories',
	async (_: undefined, thunkArgs) => {
		return await doAsync({
			url: ACCELERATORS_GET_CATEGORIES,
			...thunkArgs,
		})
	}
)

export const deleteAcceleratorInstance = createAsyncThunk(
	'accelerators/deleteAcceleratorInstance',
	async (data: { appId: string; acceleratorInstanceId: string }, thunkArgs) => {
		await doAsync({
			url: ACCELERATORS_GET_ACCELERATOR_INSTANCE(
				data.appId,
				data.acceleratorInstanceId
			),
			httpMethod: 'delete',
			...thunkArgs,
		})
	}
)

export const loadAcceleratorsForSelectedApps = createAsyncThunk(
	'accelerators/loadAcceleratorsForSelectedApps',
	async (_: undefined, thunkArgs) => {
		const selectedApps = getSelectedApps(thunkArgs.getState() as RootState)
		const sortBy = selectSortBy(thunkArgs.getState() as RootState)
		const orderBy = selectOrderBy(thunkArgs.getState() as RootState)
		const searchString = getSearchString(thunkArgs.getState() as RootState)
		if (selectedApps.length > 0) {
			if (isEmpty(searchString)) {
				return await doAsync({
					url: ACCELERATORS_GET_APPS_ACCELERATORS(
						selectedApps,
						sortBy,
						orderBy
					),
					...thunkArgs,
				})
			} else {
				return await doAsync({
					url: ACCELERATORS_GET_APPS_ACCELERATORS_SEARCH(
						selectedApps,
						sortBy,
						orderBy,
						searchString
					),
					...thunkArgs,
				})
			}
		}
	}
)

export const showAcceleratorHelpLandingPage = createAsyncThunk(
	'isyTour/showAcceleratorLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateAcceleratorsPreferences({
					landingPage: false,
				})
			)
			thunkArgs.dispatch(updatePreferences()).then(() =>
				thunkArgs.dispatch(
					actions.updateAcceleratorsPreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			)
		}
		const handleCancel = () => {
			thunkArgs.dispatch(showAcceleratorSkipHelpLandingPage())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				onCancel: handleCancel,
				data: {
					message: i18n.t(HELP_ACCELERATORS_DESCRIPTION),
					okayButtonLabel: i18n.t(START_TOUR),
					cancelButtonLabel: i18n.t(SKIP_TOUR),
				},
				title: i18n.t(HELP_ACCELERATORS_TITLE),
			})
		)
	}
)

export const showAcceleratorSkipHelpLandingPage = createAsyncThunk(
	'isyTour/showAcceleratorSkipHelpLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateAcceleratorsPreferences({
					landingPage: false,
					status: HelpModuleStatus.COMPLETED,
				})
			)
			thunkArgs.dispatch(updatePreferences())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				data: {
					message: i18n.t(HELP_SKIP_DESCRIPTION_SECTION),
					okayButtonLabel: i18n.t(OK_BUTTON_LABEL),
				},
				title: i18n.t(HELP_ACCELERATORS_TITLE),
			})
		)
	}
)
