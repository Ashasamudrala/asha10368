export const getAcceleratorsIconUrl = (imageRef: string): string => {
	switch (imageRef) {
		case 'dbAuth':
			return '/images/acceleratorsIcons/dbAuth.svg'
		case 'ldapAuth':
			return '/images/acceleratorsIcons/ldapAuth.svg'
		case 'azureReposGit':
		case 'azureDevops':
			return '/images/acceleratorsIcons/azureDevops.svg'
		case 'gitHub':
			return '/images/acceleratorsIcons/gitHub.svg'
		default:
			return '/images/acceleratorsIcons/Database.svg'
	}
}
