import { RootState } from '../../base.types'
import { AcceleratorsBaseState } from './base.types'

export const name = 'acceleratorsBase'
export const selectSlice = (state: RootState): AcceleratorsBaseState => {
	return state[name]
}
