import { combineReducers } from 'redux'
import { Accelerators } from './Accelerators'
import * as acceleratorInstance from './acceleratorInstance/acceleratorInstance.slice'
import * as accelerators from './accelerators.slice'

import * as baseSelector from './base.selector'
import { AcceleratorsBaseState } from './base.types'

export const reducer = combineReducers<AcceleratorsBaseState>({
	[accelerators.name]: accelerators.reducer,
	[acceleratorInstance.name]: acceleratorInstance.reducer,
})

export const { name } = baseSelector
export * from './base.types'
export default Accelerators
