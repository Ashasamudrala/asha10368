import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import { RootState } from '../../base.types'
import { selectDialogDataById } from './dialogs.selectors'
import { DialogTypes } from './dialogs.types'

import { actions as connectForeignKeyActions } from './connectForeignKey/connectForeignKey.slice'
import { actions as createDatabaseActions } from './createDatabase/createDatabase.slice'
import { actions as inviteUsersActions } from './inviteUsers/inviteUsers.slice'
import { actions as selectThirdPartyServiceActions } from './selectThirdPartyService/selectThirdPartyService.slice'

export const clearSubReducersData = createAsyncThunk(
	'dialog/clearData',
	async (id: string, { dispatch, getState }) => {
		const dialogData = selectDialogDataById(getState() as RootState, id)
		if (!isNil(dialogData)) {
			switch (dialogData.type) {
				case DialogTypes.CONNECT_FOREIGN_KEY:
					dispatch(connectForeignKeyActions.clearData(null))
					break
				case DialogTypes.CREATE_DATABASE:
					dispatch(createDatabaseActions.clearData(null))
					break
				case DialogTypes.INVITE_USERS:
					dispatch(inviteUsersActions.clearData(null))
					break
				case DialogTypes.SELECT_WEB_SERVICE:
					dispatch(selectThirdPartyServiceActions.clearData(null))
					break
				default:
			}
		}
	}
)
