import slice from './dialogs.slice'
import { selectSlice as dialogsSelect } from './base.selector'
import { RootState } from '../../base.types'
import { DialogProps, DialogsMapState, DialogsState } from './dialogs.types'
import { isNil } from 'lodash'

export const selectSlice = (state: RootState): DialogsState =>
	dialogsSelect(state)[slice.name]

export const selectOrderedDialogs = (state: RootState): string[] =>
	selectSlice(state).ordered

export const selectDialogsMap = (state: RootState): DialogsMapState =>
	selectSlice(state).map

export const selectDialogDataById = (
	state: RootState,
	id: string
): DialogProps | null => {
	const map = selectDialogsMap(state)
	if (!isNil(map[id])) {
		return map[id]
	}
	return null
}
