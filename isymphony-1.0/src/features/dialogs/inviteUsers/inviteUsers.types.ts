import { UserRoleProps } from '../../team/team.types'

export interface InviteUsersReturnProps {
	members: InviteUserProps[]
}

export interface InviteUsersDataProps {
	roles: UserRoleProps[]
}

export interface InviteUserProps {
	email: string
	roleId: string
}

export interface InviteUsersState {
	members: InviteUserProps[]
}
