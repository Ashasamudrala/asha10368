import slice from './inviteUsers.slice'
import { selectSlice as dialogsSelect } from '../base.selector'
import { RootState } from '../../../base.types'
import { InviteUserProps, InviteUsersState } from './inviteUsers.types'

export const selectSlice = (state: RootState): InviteUsersState =>
	dialogsSelect(state)[slice.name]

export const getInviteUsersMembers = (state: RootState): InviteUserProps[] =>
	selectSlice(state).members
