import React from 'react'
import { DialogContentText } from '@material-ui/core'
import { ConfirmationDataProps } from './confirmation.types'

export interface ConfirmationContentProps {
	dialogData: ConfirmationDataProps
}

export function ConfirmationContent(props: ConfirmationContentProps) {
	const { dialogData } = props
	return (
		<DialogContentText>
			<span className='content'>{dialogData.message}</span>
		</DialogContentText>
	)
}
