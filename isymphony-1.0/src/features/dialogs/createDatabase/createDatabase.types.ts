export interface CreateDatabaseReturnProps {
	name: string
}

export enum CreateDatabaseErrorType {
	EMPTY,
	INVALID,
	INVALID_STARTS,
	LENGTH_EXCEEDS,
	NONE,
}

export interface CreateDatabaseState {
	name: string
	error: CreateDatabaseErrorType
}
