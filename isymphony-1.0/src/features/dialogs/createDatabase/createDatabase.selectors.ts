import slice from './createDatabase.slice'
import { selectSlice as dialogsSelect } from '../base.selector'
import { RootState } from '../../../base.types'
import {
	CreateDatabaseErrorType,
	CreateDatabaseState,
} from './createDatabase.types'

export const selectSlice = (state: RootState): CreateDatabaseState =>
	dialogsSelect(state)[slice.name]

export const getName = (state: RootState): string => selectSlice(state).name

export const getError = (state: RootState): CreateDatabaseErrorType =>
	selectSlice(state).error
