import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { DialogsSubReducersNames } from '../base.types'
import {
	CreateDatabaseErrorType,
	CreateDatabaseState,
} from './createDatabase.types'

const initialState = {
	name: '',
	error: CreateDatabaseErrorType.NONE,
}

const slice = createSlice<
	CreateDatabaseState,
	SliceCaseReducers<CreateDatabaseState>,
	DialogsSubReducersNames.CREATE_DATABASE
>({
	name: DialogsSubReducersNames.CREATE_DATABASE,
	initialState,
	reducers: {
		// synchronous actions
		setName(state: CreateDatabaseState, action: Action<string>) {
			state.name = action.payload
		},
		setError(
			state: CreateDatabaseState,
			action: Action<CreateDatabaseErrorType>
		) {
			state.error = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
