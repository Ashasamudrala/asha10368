import React from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import { Typography } from '@material-ui/core'
import {
	DATABASE_NAME,
	DATABASE_TRANSLATIONS,
	DATABASE_NAME_PLACEHOLDER,
	VALIDATE_NAME,
	VALIDATE_DATABASE_NAME_LENGTH,
	INVALID_DATABASE_NAME,
	INVALID_DATABASE_NAME_START,
} from '../../../utilities/constants'
import { getName, getError } from './createDatabase.selectors'
import { actions } from './createDatabase.slice'
import { IsyInput } from '../../../widgets/IsyInput/IsyInput'
import './createDatabase.scss'
import { CreateDatabaseErrorType } from './createDatabase.types'

export function CreateDatabaseContent() {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const name = useSelector(getName)
	const error = useSelector(getError)
	const dispatch = useDispatch()

	const getErrorLabel = () => {
		switch (error) {
			case CreateDatabaseErrorType.EMPTY:
				return t(VALIDATE_NAME)
			case CreateDatabaseErrorType.INVALID:
				return t(INVALID_DATABASE_NAME)
			case CreateDatabaseErrorType.INVALID_STARTS:
				return t(INVALID_DATABASE_NAME_START)
			case CreateDatabaseErrorType.LENGTH_EXCEEDS:
				return t(VALIDATE_DATABASE_NAME_LENGTH)
			default:
				return ''
		}
	}

	const handleNameChange = (value: string) => {
		dispatch(actions.setName(value))
	}

	return (
		<div className='createDataBase'>
			<Typography>{t(DATABASE_NAME)}</Typography>
			<IsyInput
				type='text'
				className={error === CreateDatabaseErrorType.NONE ? '' : 'error-input'}
				placeholder={t(DATABASE_NAME_PLACEHOLDER)}
				onChange={handleNameChange}
				value={name}
			/>
			<span className='error-label'>{getErrorLabel()}</span>
		</div>
	)
}
