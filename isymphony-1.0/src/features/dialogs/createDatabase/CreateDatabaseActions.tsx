import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	BUTTON_CANCEL,
	BUTTON_CREATE,
} from '../../../utilities/constants'
import { isEmpty } from 'lodash'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { getName } from './createDatabase.selectors'
import { actions } from './createDatabase.slice'
import {
	CreateDatabaseErrorType,
	CreateDatabaseReturnProps,
} from './createDatabase.types'
const regex = /^[a-zA-Z]+[a-zA-Z0-9_#@$ ]*$/i

export interface CreateDatabaseActionsProps {
	onOkay: (data: CreateDatabaseReturnProps) => void
	onCancel: () => void
}

export function CreateDatabaseActions(props: CreateDatabaseActionsProps) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const name = useSelector(getName)
	const dispatch = useDispatch()

	const handleCancel = () => {
		props.onCancel()
	}

	const handleOkay = () => {
		if (isEmpty(name)) {
			return dispatch(actions.setError(CreateDatabaseErrorType.EMPTY))
		}
		if (!regex.test(name)) {
			return dispatch(actions.setError(CreateDatabaseErrorType.INVALID))
		}
		if (name.indexOf('ii') === 0) {
			return dispatch(actions.setError(CreateDatabaseErrorType.INVALID_STARTS))
		}
		if (name.length < 3 || name.length >= 32) {
			return dispatch(actions.setError(CreateDatabaseErrorType.LENGTH_EXCEEDS))
		}
		dispatch(actions.setError(CreateDatabaseErrorType.NONE))
		props.onOkay({
			name,
		})
	}

	return (
		<>
			<IsyButton onClick={handleCancel} className='standard-btn'>
				{t(BUTTON_CANCEL)}
			</IsyButton>
			<IsyButton
				disabled={isEmpty(name) ? true : false}
				className='primary-btn'
				onClick={handleOkay}
			>
				{t(BUTTON_CREATE)}
			</IsyButton>
		</>
	)
}
