export interface HelpLandingDataProps {
	message: string
	okayButtonLabel?: string
	cancelButtonLabel?: string
}
