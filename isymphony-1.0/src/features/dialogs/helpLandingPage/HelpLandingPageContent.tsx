import React from 'react'
import { useTranslation } from 'react-i18next'
import {
	ISYMPHONY_LOGO_ALT,
	ISY_TOUR_TRANSLATIONS,
} from '../../../utilities/constants'
import './helpLanding.scss'
import { HelpLandingDataProps } from './helpLanding.types'

export interface HelpLandingPageContentProps {
	dialogData: HelpLandingDataProps
}

export function HelpLandingPageContent(props: HelpLandingPageContentProps) {
	const { t } = useTranslation(ISY_TOUR_TRANSLATIONS)
	const { dialogData } = props
	const render = () => {
		return (
			<div className='help-section'>
				<div className='help-section-icon'>
					<img
						src={`/images/iSymphonyLogo.svg`}
						alt={t(ISYMPHONY_LOGO_ALT)}
						className='help-logo'
					/>
				</div>
				<div className='help-section-description'>{dialogData.message}</div>
			</div>
		)
	}
	return render()
}
