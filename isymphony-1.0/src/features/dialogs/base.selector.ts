import { RootState } from '../../base.types'
import { DialogsBaseState } from './base.types'

export const sliceName = 'dialogs'
export const selectSlice = (state: RootState): DialogsBaseState =>
	state[sliceName]
