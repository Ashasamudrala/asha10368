import { AlertDataProps } from './alert/alert.types'
import { ConfirmationDataProps } from './confirmation/confirmation.types'
import {
	ConnectForeignKeyDataProps,
	ConnectForeignKeyReturnProps,
} from './connectForeignKey/connectForeignKey.types'
import { CreateDatabaseReturnProps } from './createDatabase/createDatabase.types'
import { HelpLandingDataProps } from './helpLandingPage/helpLanding.types'
import {
	InviteUsersDataProps,
	InviteUsersReturnProps,
} from './inviteUsers/inviteUsers.types'
import { SelectedThirdServiceReturnProps } from './selectThirdPartyService/selectThirdPartyService.types'

export enum DialogTypes {
	CONFIRMATION_DIALOG,
	CONNECT_FOREIGN_KEY,
	ALERT,
	CREATE_DATABASE,
	INVITE_USERS,
	SELECT_WEB_SERVICE,
	HELP_LANDING_PAGE,
	ABOUT,
	TERMS_AND_CONDITIONS,
}

export type DialogReturnTypes =
	| CreateDatabaseReturnProps
	| ConnectForeignKeyReturnProps
	| InviteUsersReturnProps
	| SelectedThirdServiceReturnProps

export interface RemoveDialogProps {
	id: string
	eventType: string
	data?: DialogReturnTypes
}

export interface AddDialogProps extends DialogProps {
	onOkay?: (data?: DialogReturnTypes) => void
	onCancel?: () => void
}

export interface DialogProps {
	type: DialogTypes
	data?:
		| AlertDataProps
		| ConfirmationDataProps
		| ConnectForeignKeyDataProps
		| InviteUsersDataProps
		| HelpLandingDataProps
	title?: string
	className?: string
}

export interface DialogsMapState {
	[key: string]: DialogProps
}

export interface DialogsState {
	ordered: string[]
	map: DialogsMapState
}
