import { DialogsState } from './dialogs.types'
import { CreateDatabaseState } from './createDatabase/createDatabase.types'
import { ConnectForeignKeyState } from './connectForeignKey/connectForeignKey.types'
import { InviteUsersState } from './inviteUsers/inviteUsers.types'
import { SelectedServiceState } from './selectThirdPartyService/selectThirdPartyService.types'

export enum DialogsSubReducersNames {
	DIALOGS = 'dialogs',
	CREATE_DATABASE = 'createDatabase',
	CONNECT_FOREIGN_KEY = 'connectForeignKey',
	INVITE_USERS = 'inviteUsers',
	THIRD_PARTY_SERVICE = 'thirdPartyService',
}

export interface DialogsBaseState {
	[DialogsSubReducersNames.DIALOGS]: DialogsState
	[DialogsSubReducersNames.CREATE_DATABASE]: CreateDatabaseState
	[DialogsSubReducersNames.CONNECT_FOREIGN_KEY]: ConnectForeignKeyState
	[DialogsSubReducersNames.INVITE_USERS]: InviteUsersState
	[DialogsSubReducersNames.THIRD_PARTY_SERVICE]: SelectedServiceState
}
