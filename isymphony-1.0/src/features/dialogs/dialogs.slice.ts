import { v4 } from 'uuid'
import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { omit, filter, isFunction } from 'lodash'
import { Action } from '../../common.types'
import {
	AddDialogProps,
	DialogReturnTypes,
	DialogsState,
	RemoveDialogProps,
} from './dialogs.types'
import { DialogsSubReducersNames } from './base.types'

const okayFns: { [id: string]: (data?: DialogReturnTypes) => void } = {}
const cancelFns: { [id: string]: () => void } = {}

const initialState = {
	ordered: [],
	map: {},
}

const slice = createSlice<
	DialogsState,
	SliceCaseReducers<DialogsState>,
	DialogsSubReducersNames.DIALOGS
>({
	name: DialogsSubReducersNames.DIALOGS,
	initialState,
	reducers: {
		// synchronous actions
		showDialog(state: DialogsState, action: Action<AddDialogProps>) {
			// action.payload must contain type, data, onCancel, onOkay, title
			const id = v4()
			if (isFunction(action.payload.onOkay)) {
				okayFns[id] = action.payload.onOkay
			}
			if (isFunction(action.payload.onCancel)) {
				cancelFns[id] = action.payload.onCancel
			}
			state.ordered = [id, ...state.ordered]
			state.map = {
				[id]: omit(action.payload, 'onCancel', 'onOkay'),
				...state.map,
			}
		},
		removeDialog(state, action: Action<RemoveDialogProps>) {
			const id = action.payload.id
			const eventType = action.payload.eventType
			setTimeout(() => {
				if (eventType === 'OKAY' && isFunction(okayFns[id])) {
					okayFns[id](action.payload.data)
				} else if (eventType === 'CANCEL' && isFunction(cancelFns[id])) {
					cancelFns[id]()
				}
				delete okayFns[id]
				delete cancelFns[id]
			})
			state.ordered = filter((innerId: string) => id !== innerId)
			state.map = omit(state.map, id)
		},
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
