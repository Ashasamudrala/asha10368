import slice from './selectThirdPartyService.slice'
import { selectSlice as dialogsSelect } from '../base.selector'
import { RootState } from '../../../base.types'
import { SelectedServiceState } from './selectThirdPartyService.types'

export const selectSlice = (state: RootState): SelectedServiceState =>
	dialogsSelect(state)[slice.name]

export const getSelectedService = (state: RootState): string | null =>
	selectSlice(state).selectedService
