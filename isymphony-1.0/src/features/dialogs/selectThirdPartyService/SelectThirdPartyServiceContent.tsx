import React from 'react'
import './selectThirdPartyService.scss'
import { actions } from './selectThirdPartyService.slice'
import { useSelector, useDispatch } from 'react-redux'
import { getSelectedService } from './selectThirdPartyService.selectors'

export function SelectThirdPartyServiceContent() {
	const dispatch = useDispatch()
	const selectedThirdPartyServiceId = useSelector(getSelectedService)

	const getServiceList = () => [
		{
			id: 'rest',
			alt: 'rest icon',
			imagePath: '/images/thirdPartyIcons/restService.svg',
			label: 'REST',
			isDisabled: false,
		},
		{
			id: 'soap',
			alt: 'SOAP',
			imagePath: '/images/thirdPartyIcons/soapService.svg',
			label: 'SOAP',
			isDisabled: true,
		},
		{
			id: 'websocket',
			alt: 'WebSocket',
			imagePath: '/images/thirdPartyIcons/webSocketService.svg',
			label: 'WebSocket',
			isDisabled: true,
		},
	]

	const handleSelect = (selectedService: string, isDisabled: boolean) => {
		if (!isDisabled) {
			dispatch(actions.setSelectedService({ selectedService }))
		}
	}

	const renderDialog = () => {
		return (
			<div className='container'>
				{getServiceList().map((serviceItem, index) => {
					return (
						<div
							className={`service-icon ${
								selectedThirdPartyServiceId === serviceItem.id
									? 'active-service'
									: ''
							} ${serviceItem.isDisabled ? 'disabled' : ''}`}
							key={index}
							onClick={() =>
								handleSelect(serviceItem.id, serviceItem.isDisabled)
							}
						>
							<img alt={serviceItem.alt} src={serviceItem.imagePath} />
							<span className='service-text'>{serviceItem.label}</span>
						</div>
					)
				})}
			</div>
		)
	}

	return <div className='select-web-service'>{renderDialog()}</div>
}
