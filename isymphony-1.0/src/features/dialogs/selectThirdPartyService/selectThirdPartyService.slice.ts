import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { DialogsSubReducersNames } from '../base.types'
import { SelectedServiceState } from './selectThirdPartyService.types'

const initialState: SelectedServiceState = {
	selectedService: null,
}

const slice = createSlice<
	SelectedServiceState,
	SliceCaseReducers<SelectedServiceState>,
	DialogsSubReducersNames.THIRD_PARTY_SERVICE
>({
	name: DialogsSubReducersNames.THIRD_PARTY_SERVICE,
	initialState,
	reducers: {
		// synchronous actions
		setSelectedService(
			state: SelectedServiceState,
			action: Action<{ selectedService: string }>
		) {
			state.selectedService = action.payload.selectedService
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
