import React from 'react'
import { isString } from 'lodash'
import { useTranslation } from 'react-i18next'
import { BUTTON_OK, PLATFORM_TRANSLATIONS } from '../../../utilities/constants'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { AlertDataProps } from './alert.types'

export interface AlertActionsProps {
	dialogData: AlertDataProps
	onOkay: (data?: any) => void
}

export function AlertActions(props: AlertActionsProps) {
	const { dialogData } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const getYesButtonLabel = () => {
		if (isString(dialogData.okayButtonLabel)) {
			return dialogData.okayButtonLabel
		}
		return t(BUTTON_OK)
	}

	const handleOkay = () => {
		props.onOkay()
	}

	return (
		<IsyButton onClick={handleOkay} className='primary-btn'>
			{getYesButtonLabel()}
		</IsyButton>
	)
}
