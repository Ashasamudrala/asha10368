export interface AlertDataProps {
	message: string
	okayButtonLabel?: string
}
