import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { LoginAndSignUpSubReducersNames } from '../loginAndSignUp.types'
import * as asyncActions from './activeInviteSignUp.asyncActions'
import {
	ActiveInviteSignUpState,
	ActiveInviteUserDetailsErrorProps,
	ActiveInviteUserDetailsProps,
	ActiveInviteUserPageStatus,
} from './activeInviteSignUp.types'

const userDetails: ActiveInviteUserDetailsProps = {
	firstName: '',
	lastName: '',
	title: '',
	email: '',
	password: '',
	conformationPassword: '',
	invitationId: '',
	isTermsAgreed: false,
}

const initialState: ActiveInviteSignUpState = {
	details: userDetails,
	status: ActiveInviteUserPageStatus.INITIAL,
	errors: {},
}

const slice = createSlice<
	ActiveInviteSignUpState,
	SliceCaseReducers<ActiveInviteSignUpState>,
	LoginAndSignUpSubReducersNames.ACTIVE_INVITE_SIGN_UP
>({
	name: LoginAndSignUpSubReducersNames.ACTIVE_INVITE_SIGN_UP,
	initialState,
	reducers: {
		setInvitationId(state, action: Action<string>) {
			state.details.invitationId = action.payload
		},
		toggleIsTermsAgreed(state) {
			state.details.isTermsAgreed = !state.details.isTermsAgreed
		},
		updateDetails(state, action: Action<ActiveInviteUserDetailsProps>) {
			state.details = action.payload
		},
		setErrorList(state, action: Action<ActiveInviteUserDetailsErrorProps>) {
			state.errors = action.payload
		},
	},
	extraReducers: (builder) => {
		// asynchronous actions
		builder.addCase(
			asyncActions.validateInvitationId.fulfilled,
			(state, action) => {
				if (action.payload) {
					state.details = {
						...state.details,
						email: action.payload.member.email,
					}
				}
			}
		)
		builder.addCase(
			asyncActions.signUpUserDetails.fulfilled,
			(state, action) => {
				if (action.payload && state.details.isTermsAgreed) {
					state.status = ActiveInviteUserPageStatus.SUCCESS
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
