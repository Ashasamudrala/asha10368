import slice from './activeInviteSignUp.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUp.selector'
import { RootState } from '../../../base.types'
import {
	ActiveInviteSignUpState,
	ActiveInviteUserDetailsErrorProps,
	ActiveInviteUserDetailsProps,
	ActiveInviteUserPageStatus,
} from './activeInviteSignUp.types'

export const selectSlice = (state: RootState): ActiveInviteSignUpState =>
	loginAndRecoverySlice(state)[slice.name]

export const selectSignUpDetails = (
	state: RootState
): ActiveInviteUserDetailsProps => selectSlice(state).details

export const selectSignUpStatus = (
	state: RootState
): ActiveInviteUserPageStatus => selectSlice(state).status

export const selectSignUpErrors = (
	state: RootState
): ActiveInviteUserDetailsErrorProps => selectSlice(state).errors
