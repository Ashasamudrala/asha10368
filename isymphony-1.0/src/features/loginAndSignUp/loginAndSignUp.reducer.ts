import { combineReducers } from 'redux'
import * as login from './login'
import * as forgotPassword from './forgotPassword'
import * as resetPassword from './resetPassword'
import * as activeInviteSignUp from './activeInviteSignUp'
import { LoginAndSignUpState } from './loginAndSignUp.types'

export default combineReducers<LoginAndSignUpState>({
	[login.name]: login.reducer,
	[forgotPassword.name]: forgotPassword.reducer,
	[resetPassword.name]: resetPassword.reducer,
	[activeInviteSignUp.name]: activeInviteSignUp.reducer,
})
