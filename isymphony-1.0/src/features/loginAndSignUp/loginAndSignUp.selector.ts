import { RootState } from '../../base.types'
import { LoginAndSignUpState } from './loginAndSignUp.types'

export const name = 'loginAndSignUp'
export const selectSlice = (state: RootState): LoginAndSignUpState =>
	state[name]
