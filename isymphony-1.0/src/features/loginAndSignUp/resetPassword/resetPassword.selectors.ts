import slice from './resetPassword.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUp.selector'
import { RootState } from '../../../base.types'
import {
	ResetPasswordDetailsErrorState,
	ResetPasswordDetailsState,
	ResetPasswordState,
} from './resetPassword.types'

export const selectSlice = (state: RootState): ResetPasswordState =>
	loginAndRecoverySlice(state)[slice.name]

export const selectResetPasswordDetails = (
	state: RootState
): ResetPasswordDetailsState => selectSlice(state).details

export const selectResetPasswordErrors = (
	state: RootState
): ResetPasswordDetailsErrorState => selectSlice(state).errors
