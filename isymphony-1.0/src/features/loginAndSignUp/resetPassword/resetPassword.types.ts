export interface ResetPasswordDetailsState {
	email: string
	newPassword: string
	conformationPassword: string
	token: string
}

export interface ResetPasswordDetailsErrorState {
	newPassword?: string
	conformationPassword?: string
}

export interface ResetPasswordState {
	details: ResetPasswordDetailsState
	errors: ResetPasswordDetailsErrorState
}
