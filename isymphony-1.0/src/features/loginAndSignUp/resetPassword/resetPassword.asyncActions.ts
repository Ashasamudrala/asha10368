import { createAsyncThunk } from '@reduxjs/toolkit'
import { omit } from 'lodash'
import { RootState } from '../../../base.types'
import doAsync from '../../../infrastructure/doAsync'
import {
	GET_TOKEN_FOR_RESET_PASSWORD,
	PASSWORD_RESET_PASSWORD,
} from '../../../utilities/apiEndpoints'
import { selectResetPasswordDetails } from './resetPassword.selectors'

export const validateResetToken = createAsyncThunk(
	'resetPassword/validateToken',
	async (tokenFromCurrentPath: string, thunkArgs) =>
		await doAsync({
			url: GET_TOKEN_FOR_RESET_PASSWORD(tokenFromCurrentPath),
			...thunkArgs,
		})
)

export const postEmailAndToken = createAsyncThunk(
	'resetPassword/post',
	async (_: undefined, thunkArgs) => {
		const details = selectResetPasswordDetails(
			thunkArgs.getState() as RootState
		)
		await doAsync({
			url: PASSWORD_RESET_PASSWORD,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(omit(details, 'conformationPassword', 'email')),
			},
			successMessage: 'Password changed successfully',
			...thunkArgs,
		})
	}
)
