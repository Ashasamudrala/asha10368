import { createAsyncThunk } from '@reduxjs/toolkit'
import { RootState } from '../../../base.types'
import doAsync from '../../../infrastructure/doAsync'
import { PASSWORD_RESET_REQUESTS } from '../../../utilities/apiEndpoints'
import { selectForgotPasswordDetails } from './forgotPassword.selectors'

export const sendRequestForForgotPassword = createAsyncThunk(
	'forgotPassword/sendRequest',
	async (_: undefined, thunkArgs) => {
		const details = selectForgotPasswordDetails(
			thunkArgs.getState() as RootState
		)
		return await doAsync({
			url: PASSWORD_RESET_REQUESTS,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(details),
			},
			successMessage: 'Reset link sent successfully',
			...thunkArgs,
		})
	}
)
