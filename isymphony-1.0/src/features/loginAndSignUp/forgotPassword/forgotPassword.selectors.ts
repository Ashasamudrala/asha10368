import slice from './forgotPassword.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUp.selector'
import { RootState } from '../../../base.types'
import {
	ForgotPasswordDetailsErrorState,
	ForgotPasswordDetailsState,
	ForgotPasswordState,
} from './forgotPassword.types'

export const selectSlice = (state: RootState): ForgotPasswordState =>
	loginAndRecoverySlice(state)[slice.name]

export const selectForgotPasswordDetails = (
	state: RootState
): ForgotPasswordDetailsState => selectSlice(state).details

export const selectForgotPasswordErrors = (
	state: RootState
): ForgotPasswordDetailsErrorState => selectSlice(state).errors
