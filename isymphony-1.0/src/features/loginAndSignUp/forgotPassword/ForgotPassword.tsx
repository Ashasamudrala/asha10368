import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { sendRequestForForgotPassword } from './forgotPassword.asyncActions'
import { actions } from './forgotPassword.slice'
import { isEmpty } from 'lodash'
import './forgotPassword.scss'
import {
	PLATFORM_TRANSLATIONS,
	RESET_PASSWORD,
	REMEMBER_YOUR_PASSWORD,
	LOGIN,
	VALIDATE_EMAIL,
	VALIDATE_EMAIL_PATTERN,
	EMAIL,
	FORGOT_PASSWORD,
} from '../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import { NeedAnAccount } from '../needAnAccount/NeedAnAccount'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { Link } from 'react-router-dom'
import {
	ForgotPasswordDetailsErrorState,
	ForgotPasswordDetailsState,
} from './forgotPassword.types'
import {
	selectForgotPasswordDetails,
	selectForgotPasswordErrors,
} from './forgotPassword.selectors'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { IsyLogoHeader } from '../../../widgets/IsyLogoHeader/IsyLogoHeader'

export function ForgotPassword() {
	const details = useSelector(selectForgotPasswordDetails)
	const errors = useSelector(selectForgotPasswordErrors)
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()

	const getFormBuilderConfig = (): IsyFormBuilderFormItemProps[] => [
		{
			type: IsyFormBuilderFormTypes.SECTION,
			title: t(FORGOT_PASSWORD),
			forms: [
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'email',
					title: t(EMAIL),
				},
			],
		},
	]

	const emailIsValid = (email: string) => {
		return email.match(/^[\w-.]+@([\w-]+\.)+[\w-]{2,3}$/)
	}

	const handleValidation = () => {
		const errors: ForgotPasswordDetailsErrorState = {}
		if (isEmpty(details.email)) {
			errors.email = t(VALIDATE_EMAIL)
		} else if (!emailIsValid(details.email)) {
			errors.email = t(VALIDATE_EMAIL_PATTERN)
		}
		dispatch(actions.setErrorList(errors))
		return isEmpty(errors)
	}

	const handleUpdateData = (data: ForgotPasswordDetailsState) => {
		dispatch(actions.updateDetails(data))
	}

	const sendResetLinkToMail = () => {
		if (handleValidation()) {
			dispatch(sendRequestForForgotPassword())
		}
	}

	return (
		<div
			className='forgot-password-container'
			style={{ background: 'url(/images/Login.png)' }}
		>
			<IsyLogoHeader />
			<div className='form'>
				<div className='forgot-password-form'>
					<IsyFormBuilder<ForgotPasswordDetailsState>
						data={details}
						forms={getFormBuilderConfig()}
						onChange={handleUpdateData}
						errors={errors}
					/>
					<IsyButton className='primary-btn' onClick={sendResetLinkToMail}>
						{t(RESET_PASSWORD)}
					</IsyButton>
					<div className='remember-password'>
						<div className='message-class'>
							<div className='remember-tag'>
								<span>{t(REMEMBER_YOUR_PASSWORD)}</span>
							</div>
						</div>
						<Link className='forgot-text' to='/login'>
							{t(LOGIN)}
						</Link>
					</div>
				</div>
				<div className='footer'>
					<NeedAnAccount />
				</div>
			</div>
		</div>
	)
}
