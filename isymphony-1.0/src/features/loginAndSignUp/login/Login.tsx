import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	logInUser,
	loadUserProfileInfo,
} from '../../../authAndPermissions/loginUserDetails.asyncActions'
import { actions } from './login.slice'
import {
	PLATFORM_TRANSLATIONS,
	REMEMBER_ME,
	FORGOT_PASSWORD,
	LOGIN,
	EMAIL,
	PASSWORD,
	VALIDATE_EMAIL,
	VALIDATE_EMAIL_PATTERN,
	VALIDATE_PASSWORD,
	VALIDATE_PASSWORD_PATTERN,
} from '../../../utilities/constants'
import { isEmpty, isNil } from 'lodash'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { IsyLogoHeader } from '../../../widgets/IsyLogoHeader/IsyLogoHeader'
import { NeedAnAccount } from '../needAnAccount/NeedAnAccount'
import './login.scss'
import { useHistory, Link } from 'react-router-dom'
import { selectGetLogInUserInfo } from '../../../authAndPermissions/loginUserDetails.selectors'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { selectLoginDetails, selectLoginErrors } from './login.selectors'
import { LoginDetailsErrorState, LoginDetailsState } from './login.types'

/** Representation of login page */
export function Login() {
	const login = useSelector(selectGetLogInUserInfo)
	const details = useSelector(selectLoginDetails)
	const errors = useSelector(selectLoginErrors)
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()

	const [isChecked, setIsChecked] = useState(false)
	const history = useHistory()

	useEffect(() => {
		const storedEmail = localStorage.getItem('username')
		if (!isNil(storedEmail)) {
			dispatch(actions.setEmail(storedEmail))
			setIsChecked(true)
		}
	}, [dispatch])

	useEffect(() => {
		redirectToHome()
	}, [login]) // eslint-disable-line react-hooks/exhaustive-deps

	const redirectToHome = () => {
		!isNil(login) && login.accessToken && history.push('/apps')
	}

	const getFormBuilderConfig = (): IsyFormBuilderFormItemProps[] => [
		{
			type: IsyFormBuilderFormTypes.SECTION,
			title: t(LOGIN),
			forms: [
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'email',
					title: t(EMAIL),
					onKeyUp: handleKeyUp,
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_PASSWORD,
					dataRef: 'password',
					title: t(PASSWORD),
					onKeyUp: handleKeyUp,
				},
			],
		},
	]

	const passwordIsValid = (password: string) => {
		return password.match(/^(?=.*[\da-zA-Z]).{8,}$/)
	}

	const emailIsValid = (email: string) => {
		return email.match(/^[\w-.]+@([\w-]+\.)+[\w-]{2,3}$/)
	}

	// handle validations of required fields
	const handleValidation = () => {
		const errors: LoginDetailsErrorState = {}
		if (isEmpty(details.email)) {
			errors.email = t(VALIDATE_EMAIL)
		} else if (!emailIsValid(details.email)) {
			errors.email = t(VALIDATE_EMAIL_PATTERN)
		}
		if (isEmpty(details.password)) {
			errors.password = t(VALIDATE_PASSWORD)
		} else if (!passwordIsValid(details.password)) {
			errors.password = t(VALIDATE_PASSWORD_PATTERN)
		}
		dispatch(actions.setErrorList(errors))
		return isEmpty(errors)
	}

	const sendLoginData = () => {
		if (handleValidation()) {
			;(dispatch(
				logInUser({
					username: details.email,
					password: details.password,
				})
			) as any).then(() => {
				setTimeout(() => {
					dispatch(loadUserProfileInfo())
				})
			})
			// Saving username in sessionStorage if user can click on "remember me" check.
			if (isChecked) {
				localStorage.setItem('username', details.email)
			}
		}
	}

	const handleChangeCheckbox = () => {
		setIsChecked(!isChecked)
	}
	const handleKeyUp = (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		if (event.keyCode === 13) {
			event.preventDefault()
			event.stopPropagation()
			sendLoginData()
		}
	}

	const handleUpdateData = (data: LoginDetailsState) => {
		dispatch(actions.updateDetails(data))
	}

	return (
		<div
			className='login-container'
			style={{ background: 'url(/images/Login.png)' }}
		>
			<IsyLogoHeader />
			<div className='form'>
				<div className='login-form'>
					<IsyFormBuilder<LoginDetailsState>
						data={details}
						forms={getFormBuilderConfig()}
						onChange={handleUpdateData}
						errors={errors}
					/>
					<IsyButton className='primary-btn' onClick={sendLoginData}>
						{t(LOGIN)}
					</IsyButton>
					<div className='remember-forgot'>
						<div className='check-remember'>
							<input
								type='checkbox'
								id='checkbox'
								checked={isChecked}
								onChange={handleChangeCheckbox}
								className='checkbox'
							/>
							<label htmlFor='checkbox'>
								<span>{t(REMEMBER_ME)}</span>
							</label>
						</div>
						<div>
							<Link className='forgot-text' to='/forgotPassword'>
								{t(FORGOT_PASSWORD)}
							</Link>
						</div>
					</div>
				</div>
				<NeedAnAccount />
			</div>
		</div>
	)
}
