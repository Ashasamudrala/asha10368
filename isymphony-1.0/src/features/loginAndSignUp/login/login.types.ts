export interface LoginDetailsState {
	email: string
	password: string
}

export interface LoginDetailsErrorState {
	email?: string
	password?: string
}

export interface LoginState {
	details: LoginDetailsState
	errors: LoginDetailsErrorState
}
