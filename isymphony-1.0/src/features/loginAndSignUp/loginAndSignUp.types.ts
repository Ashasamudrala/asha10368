import { ActiveInviteSignUpState } from './activeInviteSignUp/activeInviteSignUp.types'
import { ForgotPasswordState } from './forgotPassword/forgotPassword.types'
import { LoginState } from './login/login.types'
import { ResetPasswordState } from './resetPassword/resetPassword.types'

export enum LoginAndSignUpSubReducersNames {
	LOGIN = 'Login',
	FORGOT_PASSWORD = 'forgotPassword',
	RESET_PASSWORD = 'resetPassword',
	ACTIVE_INVITE_SIGN_UP = 'activeInviteSignUp',
}

export interface LoginAndSignUpState {
	[LoginAndSignUpSubReducersNames.LOGIN]: LoginState
	[LoginAndSignUpSubReducersNames.FORGOT_PASSWORD]: ForgotPasswordState
	[LoginAndSignUpSubReducersNames.RESET_PASSWORD]: ResetPasswordState
	[LoginAndSignUpSubReducersNames.ACTIVE_INVITE_SIGN_UP]: ActiveInviteSignUpState
}
