import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import { RootState } from '../../base.types'
import doAsync from '../../infrastructure/doAsync/doAsync'
import {
	APP_DEV_OPS_JOB,
	APP_DEV_OPS_JOBS,
	APP_DEV_OPS_JOB_DOWNLOAD_CODE,
	APP_DEV_OPS_LATEST,
} from '../../utilities/apiEndpoints'
import { selectJobId } from './appDevOps.selectors'
import { AppDevOpsJobStatus } from './appDevOps.types'

export const loadAppLatestJob = createAsyncThunk(
	'appDevOps/loadAppLatestJob',
	async (appId: string, thunkArgs) => {
		return await doAsync({
			url: APP_DEV_OPS_LATEST(appId),
			noBusySpinner: true,
			...thunkArgs,
		}).then((data: any) => {
			if (
				data.status === AppDevOpsJobStatus.IN_PROGRESS ||
				data.status === AppDevOpsJobStatus.NEW
			) {
				setTimeout(() => {
					thunkArgs.dispatch(loadAppCurrentJobStatus(appId))
				}, 1000)
			}
			return data
		})
	}
)

export const loadAppCurrentJobStatus = createAsyncThunk(
	'appDevOps/loadAppCurrentJobStatus',
	async (appId: string, thunkArgs) => {
		const jobId = selectJobId(thunkArgs.getState() as RootState)
		if (isNil(jobId)) {
			return
		}
		return await doAsync({
			url: APP_DEV_OPS_JOB(appId, jobId),
			noBusySpinner: true,
			...thunkArgs,
		}).then((data: any) => {
			if (
				data.status === AppDevOpsJobStatus.IN_PROGRESS ||
				data.status === AppDevOpsJobStatus.NEW
			) {
				setTimeout(() => {
					thunkArgs.dispatch(loadAppCurrentJobStatus(appId))
				}, 15000)
			}
			return data
		})
	}
)

export const startDevOpsJob = createAsyncThunk(
	'appDevOps/startDevOpsJob',
	async (appId: string, thunkArgs) => {
		return await doAsync({
			url: APP_DEV_OPS_JOBS(appId),
			noBusySpinner: true,
			httpMethod: 'post',
			...thunkArgs,
		}).then((data: any) => {
			setTimeout(() => {
				thunkArgs.dispatch(loadAppCurrentJobStatus(appId))
			}, 100)
			return data
		})
	}
)

export const downloadCode = createAsyncThunk(
	'appDevOps/loadAppCurrentJobSdownloadCodetatus',
	async (data: { appId: string; fileName: string }, thunkArgs) => {
		const jobId = selectJobId(thunkArgs.getState() as RootState)
		if (isNil(jobId)) {
			return
		}
		return await doAsync({
			url: APP_DEV_OPS_JOB_DOWNLOAD_CODE(data.appId, jobId),
			...thunkArgs,
		}).then((dataString: string) => {
			const a = document.createElement('a')
			a.href = URL.createObjectURL(dataString)
			a.target = '_blank'
			a.download = data.fileName
			a.click()
		})
	}
)
