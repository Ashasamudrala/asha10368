import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../common.types'
import { AppDevOpsJobStatus, AppDevOpsState } from './appDevOps.types'
import * as asyncActions from './appDevOps.asyncActions'

const initialState: AppDevOpsState = {
	isDialogOpen: false,
	jobId: null,
	tasks: [],
	status: AppDevOpsJobStatus.NONE,
	endTime: null,
}

const slice = createSlice<
	AppDevOpsState,
	SliceCaseReducers<AppDevOpsState>,
	'appDevOps'
>({
	name: 'appDevOps',
	initialState,
	reducers: {
		// synchronous actions
		setDialogState(state, action: Action<boolean>) {
			state.isDialogOpen = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(asyncActions.startDevOpsJob.fulfilled, (state, action) => {
			if (action.payload) {
				state.jobId = action.payload.id
				state.tasks = action.payload.tasks
				state.status = action.payload.status
				state.endTime = action.payload.endTime
			}
		})
		builder.addCase(
			asyncActions.loadAppCurrentJobStatus.fulfilled,
			(state, action) => {
				if (action.payload) {
					state.jobId = action.payload.id
					state.tasks = action.payload.tasks
					state.status = action.payload.status
					state.endTime = action.payload.endTime
				}
			}
		)
		builder.addCase(
			asyncActions.loadAppLatestJob.fulfilled,
			(state, action) => {
				if (action.payload !== 'JOB_DOES_NOT_EXIST_FOR_APPLICATION') {
					state.jobId = action.payload.id
					state.tasks = action.payload.tasks
					state.status = action.payload.status
					state.endTime = action.payload.endTime
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
