import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import {
	APP_DEV_OPS_GENERATE_CODE,
	APP_DEV_OPS_TRANSLATIONS,
	APP_DEV_OPS_CODE_GENERATION_IN_PROGRESS,
	APP_DEV_OPS_CODE_GENERATION_COMPLETED,
	APP_DEV_OPS_DOWNLOAD_CODE,
	APP_DEV_OPS_VERSION_CONTROL,
	APP_DEV_OPS_CONTINUOUS_INTEGRATION,
	APP_DEV_OPS_VERSION_CONTROL_IN_PROGRESS,
	APP_DEV_OPS_CONTINUOUS_INTEGRATION_IN_PROGRESS,
	APP_DEV_OPS_VERSION_CONTROL_COMPLETED,
	APP_DEV_OPS_CONTINUOUS_INTEGRATION_COMPLETED,
	APP_DEV_OPS_NOT_CONFIGURED,
	APP_DEV_OPS_VIEW_FILES_IN_GITHUB,
	APP_DEV_OPS_VIEW_FILES_IN_AZURE,
} from '../../utilities/constants'
import {
	AppDevOpsJobTaskPros,
	AppDevOpsJobTaskStatus,
	AppDevOpsJobTaskType,
} from './appDevOps.types'
import { useTranslation } from 'react-i18next'
import { CheckCircleIcon } from '../../icons/devOps/CheckCircle'
import { NotConfigIcon } from '../../icons/devOps/NotConfig'
import { ErrorIcon } from '../../icons/devOps/Error'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import { isArray, isNil } from 'lodash'

export interface AppDevOpsTaskItemProps {
	item: AppDevOpsJobTaskPros
	index: number
	onDownloadCode: (fileName: string) => void
}

export function AppDevOpsTaskItem(props: AppDevOpsTaskItemProps) {
	const { t } = useTranslation(APP_DEV_OPS_TRANSLATIONS)

	const getLabel = () => {
		switch (props.item.type) {
			case AppDevOpsJobTaskType.CODE_GENERATION:
				return t(APP_DEV_OPS_GENERATE_CODE)
			case AppDevOpsJobTaskType.CODE_COMMIT:
				return t(APP_DEV_OPS_VERSION_CONTROL)
			case AppDevOpsJobTaskType.CI:
				return t(APP_DEV_OPS_CONTINUOUS_INTEGRATION)
			default:
				return props.item.type as string
		}
	}

	const getProgressSubText = () => {
		switch (props.item.type) {
			case AppDevOpsJobTaskType.CODE_GENERATION:
				return t(APP_DEV_OPS_CODE_GENERATION_IN_PROGRESS)
			case AppDevOpsJobTaskType.CODE_COMMIT:
				return t(APP_DEV_OPS_VERSION_CONTROL_IN_PROGRESS)
			case AppDevOpsJobTaskType.CI:
				return t(APP_DEV_OPS_CONTINUOUS_INTEGRATION_IN_PROGRESS)
			default:
				return ''
		}
	}

	const getDoneSubText = () => {
		switch (props.item.type) {
			case AppDevOpsJobTaskType.CODE_GENERATION:
				return t(APP_DEV_OPS_CODE_GENERATION_COMPLETED)
			case AppDevOpsJobTaskType.CODE_COMMIT:
				return t(APP_DEV_OPS_VERSION_CONTROL_COMPLETED)
			case AppDevOpsJobTaskType.CI:
				return t(APP_DEV_OPS_CONTINUOUS_INTEGRATION_COMPLETED)
			default:
				return ''
		}
	}

	const getButtonLabel = () => {
		switch (props.item.type) {
			case AppDevOpsJobTaskType.CODE_GENERATION:
				return t(APP_DEV_OPS_DOWNLOAD_CODE)
			case AppDevOpsJobTaskType.CODE_COMMIT: {
				switch (props.item.channelName) {
					case 'azureReposGitRequests':
						return t(APP_DEV_OPS_VIEW_FILES_IN_AZURE)
					case 'githubRequests ':
						return t(APP_DEV_OPS_VIEW_FILES_IN_GITHUB)
				}
				break
			}
			case AppDevOpsJobTaskType.CI:
				switch (props.item.channelName) {
					case 'azureCiPipelineRequests':
						return t(APP_DEV_OPS_VIEW_FILES_IN_AZURE)
				}
				break
			default:
		}
		return 'Open'
	}

	const handleButtonClick = () => {
		switch (props.item.type) {
			case AppDevOpsJobTaskType.CODE_GENERATION:
				if (!isNil(props.item.metadata.generatedCodeZipPath)) {
					const arraySplit = props.item.metadata.generatedCodeZipPath.split('/')
					const fileName = arraySplit[arraySplit.length - 1]
					props.onDownloadCode(fileName)
				}
				break
			case AppDevOpsJobTaskType.CODE_COMMIT:
				if (!isNil(props.item.metadata.repositoryURL)) {
					window.open(props.item.metadata.repositoryURL)
				}
				break
			case AppDevOpsJobTaskType.CI:
				if (!isNil(props.item.metadata.pipelineUrl)) {
					window.open(props.item.metadata.pipelineUrl)
				}
				break
			default:
		}
	}

	const renderTitle = () => {
		return <span className='label'>{getLabel()}</span>
	}

	const renderInitialState = () => {
		return (
			<div className='initial'>
				<span className='badge'>{props.index}</span>
				{renderTitle()}
			</div>
		)
	}

	const renderInProgressState = () => {
		return (
			<div className='in-progress'>
				<div className='circular-progress'>
					<CircularProgress
						variant='determinate'
						className='track'
						size={20}
						thickness={5}
						value={100}
					/>
					<CircularProgress
						variant='indeterminate'
						className='path'
						size={20}
						thickness={5}
					/>
					<span className='badge'>{props.index}</span>
				</div>
				{renderTitle()}
				<span className='sub-text'>{getProgressSubText()}</span>
			</div>
		)
	}

	const renderDoneState = () => {
		return (
			<div className='done'>
				<CheckCircleIcon className='success icon' />
				{renderTitle()}
				<IsyButton className='secondary-btn' onClick={handleButtonClick}>
					{getButtonLabel()}
				</IsyButton>
				<span className='sub-text'>{getDoneSubText()}</span>
			</div>
		)
	}

	const renderNotConfigState = () => {
		return (
			<div className='not-config'>
				<NotConfigIcon className='not-config icon' />
				{renderTitle()}
				<span className='sub-text'>{t(APP_DEV_OPS_NOT_CONFIGURED)}</span>
			</div>
		)
	}

	const renderErrorState = () => {
		return (
			<div className='error'>
				<ErrorIcon className='error icon' />
				{renderTitle()}
				{isArray(props.item.errors) && props.item.errors.length > 0 && (
					<span className='sub-text'>{props.item.errors[0].message}</span>
				)}
			</div>
		)
	}

	const renderSection = () => {
		switch (props.item.status) {
			case AppDevOpsJobTaskStatus.NEW:
				return renderInitialState()
			case AppDevOpsJobTaskStatus.NOT_CONFIG:
				return renderNotConfigState()
			case AppDevOpsJobTaskStatus.IN_PROGRESS:
				return renderInProgressState()
			case AppDevOpsJobTaskStatus.DONE:
				return renderDoneState()
			case AppDevOpsJobTaskStatus.ERROR:
				return renderErrorState()
			default:
				return null
		}
	}

	const render = () => {
		return <div className='task-item'>{renderSection()}</div>
	}

	return render()
}
