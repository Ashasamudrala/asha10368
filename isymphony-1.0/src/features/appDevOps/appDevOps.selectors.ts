import { RootState } from '../../base.types'
import { name } from './appDevOps.slice'
import {
	AppDevOpsJobStatus,
	AppDevOpsJobTaskPros,
	AppDevOpsState,
} from './appDevOps.types'

export const selectSlice = (state: RootState): AppDevOpsState => {
	return state[name]
}

export const selectIsDialogOpen = (state: RootState): boolean => {
	return selectSlice(state).isDialogOpen
}

export const selectJobStatus = (state: RootState): AppDevOpsJobStatus => {
	return selectSlice(state).status
}

export const selectJobTasks = (state: RootState): AppDevOpsJobTaskPros[] => {
	return selectSlice(state).tasks
}

export const selectJobId = (state: RootState): string | null => {
	return selectSlice(state).jobId
}

export const selectEndTime = (state: RootState): number | null => {
	return selectSlice(state).endTime
}
