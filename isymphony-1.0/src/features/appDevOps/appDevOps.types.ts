export enum AppDevOpsJobStatus {
	NONE = 'NONE',
	NEW = 'NEW',
	IN_PROGRESS = 'RUNNING',
	FAILED = 'FAILED',
	DONE = 'COMPLETED',
	ERROR = 'FAILED',
}

export enum AppDevOpsJobTaskStatus {
	NEW = 'New',
	IN_PROGRESS = 'Running',
	DONE = 'Completed',
	ERROR = 'Failed',
	NOT_CONFIG = 'NotConfig',
}

export enum AppDevOpsJobTaskType {
	CODE_GENERATION = 'CodeGeneration',
	CODE_COMMIT = 'CodeCommit',
	CI = 'CIConfiguration',
}

export interface AppDevOpsJobTaskMetadataProps {
	generatedCodeZipPath?: string
	repositoryURL?: string
	pipelineUrl?: string
}

export interface AppDevOpsJobTaskErrorPros {
	code: string
	message: string
}

export interface AppDevOpsJobTaskPros {
	status: AppDevOpsJobTaskStatus
	type: AppDevOpsJobTaskType
	metadata: AppDevOpsJobTaskMetadataProps
	channelName?: string
	errors?: AppDevOpsJobTaskErrorPros[]
}

export interface AppDevOpsState {
	isDialogOpen: boolean
	jobId: string | null
	tasks: AppDevOpsJobTaskPros[]
	status: AppDevOpsJobStatus
	endTime: number | null
}
