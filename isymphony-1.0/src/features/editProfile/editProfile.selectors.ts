import { RootState } from '../../base.types'
import { EditProfileState } from './editProfile.types'

export const sliceName = 'editProfile'

const selectSlice = (state: RootState) => state[sliceName]

export const selectEditProfileState = (state: RootState): EditProfileState =>
	selectSlice(state)
