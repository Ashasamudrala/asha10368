import { createAsyncThunk } from '@reduxjs/toolkit'
import {
	fetchAllDatabases,
	fetchAttributeTypes,
	releaseLock,
	saveDatabase,
} from './database.asyncActions'
import { selectDatabase, selectIsDBModified } from './database.selectors'
import { actions as dbActions } from './database.slice'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import i18n from 'i18next'

import { actions as notificationActions } from '../../infrastructure/notificationPopup/notificationPopup.slice'
import { isNil } from 'lodash'
import { RootState } from '../../base.types'
import {
	BUTTON_DISCARD,
	BUTTON_SAVE,
	DATABASE_DISCARD_MESSAGE,
	COMMON_CONFORMATION_UPDATE_MESSAGE_LOCK,
} from '../../utilities/constants'
import { DialogTypes, showDialog } from '../dialogs'
import { selectGetDatabasePreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'

export const initDatabaseService = createAsyncThunk(
	'webService/initWebService',
	async (id: string, { dispatch }) => {
		dispatch(dbActions.setAppId(id))
		dispatch(fetchAttributeTypes())
		dispatch(fetchAllDatabases())
	}
)

export const releaseLockConformation = createAsyncThunk(
	'database/releaseLockConformation',
	async (_: undefined, { dispatch, getState }) => {
		const selectedDatabase = selectDatabase(getState() as RootState)
		if (isNil(selectedDatabase)) {
			return
		}
		const databasePreferenceData = selectGetDatabasePreferenceStatus(
			getState() as RootState
		)
		const dbName = selectedDatabase.name
		const isDBModified = selectIsDBModified(getState() as RootState)
		const conformationCallBack = () => {
			dispatch(saveDatabase()).then(dispatch(releaseLock()) as any)
		}
		const handleOnClose = () => {
			dispatch(
				notificationActions.notifySuccess(
					i18n.t(DATABASE_DISCARD_MESSAGE, { name: dbName })
				)
			)
			dispatch(dbActions.updateDatabaseOnCancel(null))
			dispatch(releaseLock())
		}
		if (!isDBModified) {
			if (
				databasePreferenceData &&
				databasePreferenceData.status === HelpModuleStatus.INPROGRESS
			) {
				dispatch(
					preferenceActions.updateDatabasePreferences({
						status: HelpModuleStatus.COMPLETED,
					})
				)
			}
			dispatch(releaseLock())
		} else {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(COMMON_CONFORMATION_UPDATE_MESSAGE_LOCK, {
							name: dbName,
						}),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		}
	}
)
