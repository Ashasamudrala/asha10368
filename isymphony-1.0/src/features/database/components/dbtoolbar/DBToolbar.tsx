import React from 'react'
import {
	IsyToolbar,
	IsyToolbarItemProps,
	IsyToolbarItemTypes,
} from '../../../../widgets/IsyToolbar/IsyToolbar'
import { useDispatch, useSelector } from 'react-redux'
import './dbtoolbar.scss'
import { DatabaseLockStatus, DatabaseTableProps } from '../../database.types'
import {
	ADD_NEW_TABLE,
	BUTTON_SAVE,
	DATABASE_TRANSLATIONS,
	SEARCH_TABLE,
	COMMON_REQUEST_LOCK,
	COMMON_RELEASE_LOCK,
	COMMON_LOCKED_BY,
	DATABASE_RELEASE_LOCK_INFO,
	DATABASE_REQUEST_LOCK_INFO,
} from '../../../../utilities/constants'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined'
import { useTranslation } from 'react-i18next'
import { actions } from '../../database.slice'
import { getSearchTerm } from '../../database.selectors'
import { IsySearchSuggestionProps } from '../../../../widgets/IsySearch/IsySearch'
import { IsyButton } from '../../../../widgets/IsyButton/IsyButton'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined'
import CachedOutlinedIcon from '@material-ui/icons/CachedOutlined'

export interface DBToolbarProps {
	tablesData: DatabaseTableProps[]
	isDBModified: boolean
	isEnabledSteps?: boolean
	lockStatus: DatabaseLockStatus
	lockedBy: string | null
	onSaveDatabase: () => void
	onTableSelect: (tableIndex: number) => void
	onAddNewTable: () => void
	onAcquireLock: () => void
	onReleaseLock: () => void
	onRefreshLock: () => void
}

export function DBToolbar(props: DBToolbarProps) {
	const searchSuggestions = props.tablesData.map((table, index) => ({
		value: index,
		name: table.name,
	}))
	const searchTerm = useSelector(getSearchTerm)

	const dispatch = useDispatch()
	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	const getToolbarConfig = (): IsyToolbarItemProps[] => [
		{
			type: IsyToolbarItemTypes.SEARCH,
			value: searchTerm,
			onCancel: handleSearchChange,
			onChange: handleSearchChange,
			suggestions: getFilterSuggestions(),
			onSuggestionSelect: handleTableSelect,
			placeholder: t(SEARCH_TABLE),
		},
		{
			type: IsyToolbarItemTypes.FLEX_GROW,
		},
		{
			type: IsyToolbarItemTypes.BUTTON,
			name: t(ADD_NEW_TABLE),
			onClick: props.onAddNewTable,
			disabled: props.lockStatus !== DatabaseLockStatus.LOCK_ACQUIRED,
			icon: <AddOutlinedIcon />,
			className: props.isEnabledSteps ? 'isy-btn-new-table ' : '',
		},
		{
			type: IsyToolbarItemTypes.SEPARATOR,
		},
		{
			type: IsyToolbarItemTypes.ICON,
			icon: <SaveOutlinedIcon />,
			tooltip: t(BUTTON_SAVE),
			disabled:
				!props.isDBModified ||
				props.lockStatus !== DatabaseLockStatus.LOCK_ACQUIRED,
			onClick: props.onSaveDatabase,
			className: 'save-icon ' + props.isEnabledSteps && 'isy-steps-save-icon',
		},
		{
			type: IsyToolbarItemTypes.SEPARATOR,
		},
		{
			type: IsyToolbarItemTypes.COMPONENT,
			render: renderEditLock,
		},
	]

	const getFilterSuggestions = () => {
		const searchString = searchTerm.toLowerCase()
		return searchSuggestions.filter(
			(option) => option.name.toLowerCase().indexOf(searchString) > -1
		)
	}

	const handleSearchChange = (val: string) => {
		dispatch(actions.updateSearchTerm(val))
	}

	const handleTableSelect = (item: IsySearchSuggestionProps) => {
		dispatch(actions.updateSearchTerm(item.name))
		props.onTableSelect(item.value as number)
	}

	const handleReloadLockStatus = () => {
		props.onRefreshLock()
	}

	const renderEditLock = () => {
		switch (props.lockStatus) {
			case DatabaseLockStatus.LOCK_PRESENT: {
				return (
					<span className='edit-lock'>
						<IsyButton className='request-button' onClick={props.onAcquireLock}>
							<LockOutlinedIcon className='icon' />
							{t(COMMON_REQUEST_LOCK)}
						</IsyButton>
						<span className='lock-tooltip'>
							{t(DATABASE_REQUEST_LOCK_INFO)}
						</span>
					</span>
				)
			}
			case DatabaseLockStatus.LOCK_ACQUIRED: {
				return (
					<span className='edit-lock'>
						<IsyButton className='release-button' onClick={props.onReleaseLock}>
							<LockOpenOutlinedIcon className='icon' />
							{t(COMMON_RELEASE_LOCK)}
						</IsyButton>
						<span className='lock-tooltip'>
							{t(DATABASE_RELEASE_LOCK_INFO)}
						</span>
					</span>
				)
			}
			case DatabaseLockStatus.LOCK_NOT_PRESENT: {
				return (
					<div className='locked-by'>
						<span className='info'>
							{t(COMMON_LOCKED_BY, { name: props.lockedBy })}
						</span>
						<CachedOutlinedIcon
							className='icon'
							onClick={handleReloadLockStatus}
						/>
					</div>
				)
			}
			default:
				return null
		}
	}

	return (
		<div className='db-tool-bar'>
			<IsyToolbar items={getToolbarConfig()} />
		</div>
	)
}
