import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import './dBAccordion.scss'
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Accordion from '@material-ui/core/Accordion'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import Typography from '@material-ui/core/Typography'
// import Tooltip from '@material-ui/core/Tooltip'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
// import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
// import IconButton from '@material-ui/core/IconButton'
import {
	DATABASE_TRANSLATIONS,
	DATABASES,
	SETTINGS,
	DELETE,
	// CREATE_DATABASE,
} from '../../../../utilities/constants'
import { actions } from '../../databaseSettings/databaseSettings.slice'
import { useTranslation } from 'react-i18next'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined'
import { IsyPopover } from '../../../../widgets/IsyPopover/IsyPopover'
import {
	DatabaseProps,
	DatabaseSchemasProps,
	DatabaseTableProps,
} from '../../database.types'
import { isNil } from 'lodash'

export interface DBAccordionProps {
	databases: DatabaseProps[]
	selectedDatabaseIndex: number | null
	selectedTableIndex: number | null
	setSelectedDatabaseIndex: (index: number) => void
	onSelectedTableIndexAndAttrIndex: (
		tableIndex: number,
		attrIndex: null
	) => void
	onDeleteDatabase: (databaseId: string) => void
	onAddDatabase: () => void
	isEditable: boolean
}

export function DBAccordion(props: DBAccordionProps) {
	const {
		databases,
		selectedDatabaseIndex,
		selectedTableIndex,
		isEditable,
	} = props
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)
	const [
		selectedPopoverDbDetails,
		setPopoverDbDetails,
	] = useState<DatabaseProps | null>(null)
	const dispatch = useDispatch()

	const handleChange = (index: number, isExpanded: boolean) => {
		if (isExpanded) {
			props.setSelectedDatabaseIndex(index)
		}
	}
	const handleListItem = (index: number) => {
		props.onSelectedTableIndexAndAttrIndex(index, null)
	}

	const accordionExpansion = {
		backgroundColor: `#faf9f8`,
	}

	const handleClick = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>,
		database: DatabaseProps
	) => {
		event.stopPropagation()
		setPopoverDbDetails(database)
		setPopoverAnchorEl(event.currentTarget)
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleMenuItems = (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		selectedItem: string
	) => {
		if (selectedItem === 'settings' && !isNil(selectedPopoverDbDetails)) {
			// todo
			dispatch(
				actions.setDatabaseIdForSettings({
					tableId: selectedPopoverDbDetails.id,
					tableName: selectedPopoverDbDetails.name,
				})
			)
		} else if (selectedItem === 'delete' && !isNil(selectedPopoverDbDetails)) {
			props.onDeleteDatabase(selectedPopoverDbDetails.id)
		}
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const popoverActions = [
		{
			name: t(SETTINGS),
			key: 'settings',
		},
		{
			name: t(DELETE),
			key: 'delete',
		},
	]

	const renderPopover = () => {
		if (isNil(popoverAnchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={
					isEditable
						? popoverActions
						: popoverActions.splice(0, popoverActions.length - 1)
				}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				className={'pop-over-menu-list'}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			/>
		)
	}

	const renderDataBasesAccordions = () => {
		return databases.map((database, index) => {
			return (
				<div
					className={`menu-header ${
						index === selectedDatabaseIndex ? 'selected' : ''
					}`}
					key={index}
				>
					<Accordion
						expanded={index === selectedDatabaseIndex}
						onChange={(_, isExpanded) => handleChange(index, isExpanded)}
					>
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls={`panel${index}bh-content`}
							id={`panel${index}bh-header`}
							className='accordion-parent'
						>
							<Typography title={database.name} className='database-name'>
								{database.name}
							</Typography>
							{index === selectedDatabaseIndex && (
								<div className='hide-settings-icon'>
									<MoreHorizIcon
										onClick={(event) => handleClick(event, database)}
									/>
								</div>
							)}
						</AccordionSummary>
						<AccordionDetails style={accordionExpansion}>
							{renderSubMenuAccordionDetails(database, index)}
						</AccordionDetails>
					</Accordion>
				</div>
			)
		})
	}

	const renderAccordionDetails = () => {
		return (
			<div className='database-parent'>
				<div className='database-accordion'>
					<div className='database-parent-accordion'>
						<div className='database'>{t(DATABASES)}</div>
						{/* <Tooltip title={t(CREATE_DATABASE) as string}>
							<IconButton
								aria-label='Add Database'
								onClick={props.onAddDatabase}
								component='span'
							>
								<AddOutlinedIcon />
							</IconButton>
						</Tooltip> */}
					</div>
				</div>
				<div className='database-accordion-overflow'>
					{renderDataBasesAccordions()}
				</div>
			</div>
		)
	}

	const renderSubListDetails = (
		schemaData: DatabaseSchemasProps,
		dbIndex: number
	) => {
		return (
			<List>
				{schemaData.tables.map(
					(tableData: DatabaseTableProps, index: number) => {
						return (
							<ListItem
								button
								key={index}
								onClick={() => handleListItem(index)}
								className={`${
									dbIndex === selectedDatabaseIndex &&
									selectedTableIndex === index
										? 'list-selected'
										: ''
								} list-item`}
							>
								<ListItemIcon>
									<TableChartOutlinedIcon />
								</ListItemIcon>
								<ListItemText
									className='list-item-text'
									primary={tableData.name}
									title={tableData.name}
								/>
							</ListItem>
						)
					}
				)}
			</List>
		)
	}

	const renderSubMenuAccordionDetails = (
		database: DatabaseProps,
		index: number
	) => {
		return database.schemas.map(
			(schemaData: DatabaseSchemasProps, id: number) => {
				return (
					<div className={`subMenu-menu-header`} key={id}>
						<Accordion defaultExpanded={true}>
							<AccordionSummary expandIcon={<ExpandMoreIcon />}>
								<div className='account-tree-icon'>
									<AccountTreeOutlinedIcon />
								</div>
								<Typography className='schema-display-name'>
									{schemaData.displayName}
								</Typography>
							</AccordionSummary>
							<AccordionDetails className='tables-list-container'>
								{renderSubListDetails(schemaData, index)}
							</AccordionDetails>
						</Accordion>
						<div className={'popover-class'}>{renderPopover()}</div>
					</div>
				)
			}
		)
	}
	return renderAccordionDetails()
}
