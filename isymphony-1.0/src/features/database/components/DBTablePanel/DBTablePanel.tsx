import React, { useState, useEffect } from 'react'
import { isEmpty } from 'lodash'
import './DBTablePanel.scss'
import { IsyInput } from '../../../../widgets/IsyInput/IsyInput'
import Tooltip from '@material-ui/core/Tooltip'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import SearchIcon from '@material-ui/icons/Search'
import Divider from '@material-ui/core/Divider'
import { Typography } from '@material-ui/core'
import {
	COLUMNS,
	TABLE_NAME,
	DATABASE_TRANSLATIONS,
	SEARCH_COLUMN,
	ADD_NEW_COLUMN,
	DELETE_TABLE,
} from '../../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import { DBTablePanelColumn } from './DBTablePanelColumn'
import {
	AttributeTypeProps,
	DatabaseTableAttributeProps,
	DatabaseTableConstraintsProps,
	DatabaseTableProps,
} from '../../database.types'
import { TableAttributeKeyTypes } from '../../tables/tables.types'
import { IsySearch } from '../../../../widgets/IsySearch/IsySearch'

export interface DBTablePanelProps {
	tableData: DatabaseTableProps
	attributeTypes: AttributeTypeProps[]
	onTableChange: (data: Partial<DatabaseTableProps>) => void
	onTableDelete: () => void
	onAddAttribute: () => void
	onAttributeChange: (
		data: Partial<DatabaseTableAttributeProps>,
		attrIndex: number
	) => void
	onAttributeConstraintChange: (
		d: Partial<DatabaseTableConstraintsProps>,
		attrIndex: number
	) => void
	onDeleteTableAttribute: (index: number) => void
	onAttributeKeyChange: (key: TableAttributeKeyTypes, attrIndex: number) => void
	isEditable: boolean
}

export function DBTablePanel(props: DBTablePanelProps) {
	const { tableData, attributeTypes, isEditable } = props
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const [searchEnabled, setSearchEnabled] = useState<boolean>(false)
	const [searchTerm, setSearchTerm] = useState<string>('')
	useEffect(() => {
		setSearchEnabled(false)
		setSearchTerm('')
	}, [props.tableData])

	const handleTableNameChange = (value: string) => {
		props.onTableChange({ name: value })
	}

	const handleSearchChange = (value: string) => {
		setSearchTerm(value)
	}

	const handleSearchColumn = () => {
		setSearchEnabled(true)
	}

	const handleSearchClose = () => {
		if (isEmpty(searchTerm)) {
			setSearchEnabled(false)
		} else {
			setSearchTerm('')
		}
	}

	const handleDeleteTable = () => {
		props.onTableDelete()
	}

	const handleAddAttribute = () => {
		props.onAddAttribute()
	}

	const renderTableHeader = () => {
		return (
			<div className='table-heading'>
				<TableChartOutlinedIcon className='table-icon' />
				<label className='form-heading' title={tableData && tableData.name}>
					{tableData && tableData.name}
				</label>
				<Tooltip title={t(DELETE_TABLE) as string}>
					<DeleteOutlinedIcon
						className={isEditable ? 'delete-icon' : 'hide-delete-icon'}
						onClick={handleDeleteTable}
					/>
				</Tooltip>
			</div>
		)
	}

	const renderTableName = () => {
		return (
			<div className='table-section'>
				<Typography>{t(TABLE_NAME)}</Typography>
				<IsyInput<string>
					type='text'
					onBlur={handleTableNameChange}
					value={tableData.name}
					disabled={!isEditable}
				/>
			</div>
		)
	}
	const renderAttribute = (
		data: DatabaseTableAttributeProps,
		index: number
	) => {
		return (
			<DBTablePanelColumn
				data={data}
				attributeTypes={attributeTypes}
				onUpdateTableAttribute={(data: Partial<DatabaseTableAttributeProps>) =>
					props.onAttributeChange(data, index)
				}
				onChangeOfTableAttributeConstraint={(
					data: Partial<DatabaseTableConstraintsProps>
				) => props.onAttributeConstraintChange(data, index)}
				onDeleteTableAttribute={() => props.onDeleteTableAttribute(index)}
				onAttributeKeyChange={(key: TableAttributeKeyTypes) =>
					props.onAttributeKeyChange(key, index)
				}
				isEditable={isEditable}
			/>
		)
	}

	const renderAttributesHeader = () => {
		return (
			<>
				<label className='section'>{t(COLUMNS)}</label>
				{isEditable && (
					<Tooltip title={t(ADD_NEW_COLUMN) as string}>
						<AddOutlinedIcon
							className={'add-icon'}
							onClick={handleAddAttribute}
						></AddOutlinedIcon>
					</Tooltip>
				)}
				<SearchIcon className='searchIcon' onClick={handleSearchColumn} />
			</>
		)
	}

	const renderAttributesSearch = () => {
		return (
			<IsySearch
				value={searchTerm}
				onChange={handleSearchChange}
				onCancel={handleSearchClose}
				hideSearchIcon={true}
				placeholder={t(SEARCH_COLUMN)}
				autoFocus={true}
			/>
		)
	}

	const renderAttributes = () => {
		const children = []
		const searchString = searchTerm.toLowerCase()
		for (let i = 0, iLen = tableData.attributes.length; i < iLen; i++) {
			const attr = tableData.attributes[i]
			if (
				isEmpty(searchString) ||
				attr.name.toLowerCase().indexOf(searchString) !== -1
			) {
				children.push(renderAttribute(attr, i))
			}
		}
		return children
	}

	const renderAttributesSection = () => {
		return (
			<div className='table-second-section'>
				{renderAttributesHeader()}
				{searchEnabled && renderAttributesSearch()}
				<Divider className='divider' />
				{renderAttributes()}
			</div>
		)
	}

	return (
		<div className='table-properties-panel'>
			{renderTableHeader()}
			<div className='table-scrollContent'>
				{renderTableName()}
				<Divider className='divider' />
				{renderAttributesSection()}
				<Divider />
			</div>
		</div>
	)
}
