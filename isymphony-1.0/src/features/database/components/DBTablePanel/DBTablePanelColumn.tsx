import React, { useState, useEffect } from 'react'
import './DBTablePanel.scss'
import { isNil, find } from 'lodash'
import { IsyInput } from '../../../../widgets/IsyInput/IsyInput'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import { DBColumnPopOver } from './DBColumnPopOver/DbColumnPopOver'
import {
	AttributeTypeProps,
	DatabaseTableAttributeProps,
	DatabaseTableConstraintsProps,
} from '../../database.types'
import { TableAttributeKeyTypes } from '../../tables/tables.types'

export interface DBTablePanelColumnProps {
	data: DatabaseTableAttributeProps
	attributeTypes: AttributeTypeProps[]
	onUpdateTableAttribute: (data: Partial<DatabaseTableAttributeProps>) => void
	onChangeOfTableAttributeConstraint: (
		data: Partial<DatabaseTableConstraintsProps>
	) => void
	onDeleteTableAttribute: () => void
	onAttributeKeyChange: (key: TableAttributeKeyTypes) => void
	isEditable: boolean
}

export function DBTablePanelColumn(props: DBTablePanelColumnProps) {
	const { data, attributeTypes, isEditable } = props
	const [anchorEl, setAnchorEl] = useState<Element | null>(null)

	useEffect(() => {
		setAnchorEl(null)
	}, [data.name])

	const handleClosePopOver = () => {
		setAnchorEl(null)
	}

	const handleClickOptions = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>
	) => {
		event.stopPropagation()
		setAnchorEl(event.currentTarget)
	}

	const handleNameChange = (value: string) => {
		props.onUpdateTableAttribute({ name: value })
	}

	const handleTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		props.onUpdateTableAttribute({ type: e.target.value })
	}

	const getAttributeTypeConstraints = () => {
		const attributeType = find(attributeTypes, (a) => a.value === data.type)
		if (!isNil(attributeType)) {
			return attributeType.constraints
		}
		return []
	}

	const renderOptions = () => {
		return (
			<div className='moreContainer'>
				<MoreHorizIcon onClick={handleClickOptions} className='more' />
				{!isNil(anchorEl) && (
					<DBColumnPopOver
						isLimitedView={false}
						anchorEl={anchorEl}
						constraints={data.constraints}
						reference={data.reference}
						constraintsConfig={getAttributeTypeConstraints()}
						onClose={handleClosePopOver}
						onChangeOfConstraint={props.onChangeOfTableAttributeConstraint}
						onDelete={props.onDeleteTableAttribute}
						onKeyChange={props.onAttributeKeyChange}
						isViewMode={!isEditable}
					/>
				)}
			</div>
		)
	}

	const renderType = () => {
		return (
			<div className='dataTypeContainer'>
				<select
					value={data.type}
					className='dataType'
					onChange={handleTypeChange}
					disabled={!isNil(data.reference) || !isEditable}
				>
					<option disabled selected hidden value=''>
						Type
					</option>
					{attributeTypes.map((item) => (
						<option key={item.value} value={item.value}>
							{item.value}
						</option>
					))}
				</select>
			</div>
		)
	}

	const renderName = () => {
		return (
			<div className='dataNameContainer'>
				<IsyInput<string>
					type='text'
					onBlur={handleNameChange}
					value={data.name}
					disabled={!isEditable}
				/>
			</div>
		)
	}

	return (
		<div className='columnData' key={data as any}>
			{renderName()}
			{renderType()}
			{renderOptions()}
		</div>
	)
}
