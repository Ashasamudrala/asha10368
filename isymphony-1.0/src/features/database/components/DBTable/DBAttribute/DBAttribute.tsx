import React, { Fragment, useState, useEffect } from 'react'
import { IsyPopover } from '../../../../../widgets/IsyPopover/IsyPopover'
import { useTranslation } from 'react-i18next'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import Tooltip from '@material-ui/core/Tooltip'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import { IsyInput } from '../../../../../widgets/IsyInput/IsyInput'
import { isNil, find } from 'lodash'
import {
	DATABASE_TRANSLATIONS,
	NONE,
	PRIMARY_KEY,
	FOREIGN_KEY,
	UNIQUE_KEY,
} from '../../../../../utilities/constants'
import './DBAttribute.scss'
import {
	AttributeTypeProps,
	DatabaseTableAttributeProps,
} from '../../../database.types'
import { TableAttributeKeyTypes } from '../../../tables/tables.types'
import { isFunction } from 'lodash'

export interface DBAttributeProps {
	data: DatabaseTableAttributeProps
	index: number
	attributeTypes: AttributeTypeProps[]
	isEditMode: boolean
	isLimitedView: boolean
	onChange: (attr: Partial<DatabaseTableAttributeProps>, index: number) => void
	onSetKey: (index: number, key: TableAttributeKeyTypes) => void
	onDelete: (index: number) => void
	onSetStepKey?: () => void
}

export function DBAttribute(props: DBAttributeProps) {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const { data, index, attributeTypes, isEditMode } = props
	const [anchorEl, setAnchorEl] = useState<Element | null>(null)

	useEffect(() => {
		setAnchorEl(null)
	}, [isEditMode])

	const getSvgTypeClassName = (canNull: boolean) => {
		if (!isNil(data.reference)) {
			return 'foreign_key_svg'
		}
		if (!isNil(data.constraints)) {
			if (data.constraints.primary) {
				return 'primary_key_svg'
			}
			if (data.constraints.unique) {
				return 'unique_key_svg'
			}
		}
		if (!canNull) {
			return 'none_key_svg'
		}
		return 'no_key_svg'
	}
	const getTooltipName = () => {
		if (!isNil(data.reference)) {
			return t(FOREIGN_KEY)
		}
		if (!isNil(data.constraints)) {
			if (data.constraints.primary) {
				return t(PRIMARY_KEY)
			}
			if (data.constraints.unique) {
				return t(UNIQUE_KEY)
			}
		}
		return ''
	}

	const getAttributeTypeConstraints = () => {
		const attributeType = find(attributeTypes, (a) => a.value === data.type)
		if (!isNil(attributeType)) {
			return attributeType.constraints.map((c) => c.name)
		}
		return []
	}

	const getPopoverActions = () => {
		const options = []
		const constraints = getAttributeTypeConstraints()
		if (
			constraints.indexOf('primary') !== -1 &&
			(!props.isLimitedView || data.constraints.primary)
		) {
			options.push({
				key: TableAttributeKeyTypes.PRIMARY_KEY,
				hasIcon: true,
				icon: <VpnKeyIcon className='key-icon primary_key_svg' />,
				name: t(PRIMARY_KEY),
				labelClass: 'key_label',
			})
		}
		if (!data.constraints.primary) {
			options.unshift({
				key: TableAttributeKeyTypes.NONE,
				hasIcon: true,
				icon: <VpnKeyIcon className='key-icon none_key_svg' />,
				name: t(NONE),
				labelClass: 'key_label',
			})
			if (constraints.indexOf('primary') !== -1 && !props.isLimitedView) {
				options.push({
					key: TableAttributeKeyTypes.FOREIGN_KEY,
					hasIcon: true,
					icon: <VpnKeyIcon className='key-icon foreign_key_svg' />,
					name: t(FOREIGN_KEY),
					labelClass: 'key_label',
					menuClass: 'isy-foreign-key',
				})
			}
			if (constraints.indexOf('unique') !== -1) {
				options.push({
					key: TableAttributeKeyTypes.UNIQUE_KEY,
					hasIcon: true,
					icon: <VpnKeyIcon className='key-icon unique_key_svg' />,
					name: t(UNIQUE_KEY),
					labelClass: 'key_label',
				})
			}
		}
		return options
	}

	const handleOnClick = (
		e: React.MouseEvent<HTMLTableRowElement, MouseEvent>
	) => {
		e.stopPropagation()
	}

	const handleOnNameChange = (name: string) => {
		props.onChange({ name: name }, index)
	}

	const handleOnTypeChange = (evt: React.ChangeEvent<HTMLSelectElement>) => {
		props.onChange({ type: evt.target.value }, index)
	}

	const handleClickOnKeyIcon = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>
	) => {
		event.stopPropagation()
		if (isFunction(props.onSetStepKey)) {
			props.onSetStepKey()
		}
		setAnchorEl(event.currentTarget)
	}
	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setAnchorEl(null)
	}
	const handleMenuItems = (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		key: string
	) => {
		event.stopPropagation()
		props.onSetKey(index, key as TableAttributeKeyTypes)
		setAnchorEl(null)
	}
	const renderPopover = () => {
		if (isNil(anchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={anchorEl}
				actions={getPopoverActions()}
				className={'key-popover'}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			/>
		)
	}

	return (
		<Fragment>
			{isEditMode ? (
				<tr onClick={handleOnClick}>
					<td>
						{!props.isLimitedView && (
							<span className='icon'>
								<DeleteOutlineIcon
									className='delete-icon'
									onClick={() => props.onDelete(index)}
								/>
							</span>
						)}
						<IsyInput<string>
							type='text'
							onBlur={handleOnNameChange}
							value={data.name}
							disabled={props.isLimitedView}
						/>
					</td>

					<td>
						<select
							name='type'
							value={data.type}
							onChange={handleOnTypeChange}
							disabled={!isNil(data.reference)}
						>
							<option disabled selected hidden value=''>
								Type
							</option>
							{attributeTypes.map((type) => (
								<option key={type.value} value={type.value}>
									{type.value}
								</option>
							))}
						</select>
						<span className='icon key-icon'>
							<VpnKeyIcon
								className={getSvgTypeClassName(false)}
								onClick={handleClickOnKeyIcon}
							/>
						</span>
						{renderPopover()}
					</td>
				</tr>
			) : (
				<tr key={index}>
					<td>
						<span title={data.name} className='attribute-content'>
							{data.name}
						</span>
					</td>
					<td>
						<span>{data.type}</span>
						<Tooltip title={getTooltipName()} className='tooltip-icon'>
							<span className='icon key-icon'>
								<VpnKeyIcon className={getSvgTypeClassName(true)} />
							</span>
						</Tooltip>
					</td>
				</tr>
			)}
		</Fragment>
	)
}
