import React from 'react'
import './DBRelationPanel.scss'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import Autocomplete, {
	AutocompleteRenderInputParams,
} from '@material-ui/lab/Autocomplete'
import {
	TableRow,
	TableBody,
	TableCell,
	Table,
	TextField,
	Typography,
} from '@material-ui/core'
import { RelationshipIcon } from '../../../../icons/Relationship'
import { OneToOneIcon } from '../../../../icons/OneToOne'
import { OneToManyIcon } from '../../../../icons/OneToMany'
import { ManyToOneIcon } from '../../../../icons/ManyToOne'
import { ManyToManyIcon } from '../../../../icons/ManyToMany'
import {
	RELATIONSHIP,
	FROM,
	TO,
	DATABASE_TRANSLATIONS,
	ONE_TO_ONE,
	ONE_TO_MANY,
	MANY_TO_MANY,
	MANY_TO_ONE,
	RELATIONSHIP_TYPE,
	FOREIGN_KEY_DIALOG_CONTAINMENT,
	FOREIGN_KEY_DIALOG_CONTAINMENT_COMPOSITION,
	FOREIGN_KEY_DIALOG_CONTAINMENT_AGGREGATION,
} from '../../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import { IsyRadioGroup } from '../../../../widgets/IsyRadioGroup/IsyRadioGroup'
import { isNil, findIndex } from 'lodash'
import {
	DatabaseTableAttributeProps,
	DatabaseTableProps,
} from '../../database.types'
import {
	DatabaseContainmentType,
	DatabaseRelationShipType,
} from '../../../../utilities/apiEnumConstants'
import { IsyDropDown } from '../../../../widgets/IsyDropDown/IsyDropDown'

export interface DBRelationPanelToProps {
	tableIndex: number
	tableName: string
	attributeName: string
	attributeIndex: number
	dataType: string
}

export interface DBRelationPanelProps {
	tablesData: DatabaseTableProps[]
	tableIndex: number
	attrIndex: number
	onChange: (data: Partial<DatabaseTableAttributeProps>) => void
	isEditable: boolean
}

export function DBRelationPanel(props: DBRelationPanelProps) {
	const { tablesData, tableIndex, attrIndex, isEditable } = props
	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	const getTableOptions = tablesData.map(
		(table: DatabaseTableProps, index: number) => ({
			id: index,
			name: table.name,
		})
	)

	const relationshipOptions = [
		{
			id: DatabaseRelationShipType.ONE_TO_ONE,
			name: t(ONE_TO_ONE),
			hasIcon: true,
			icon: <OneToOneIcon className='relation-ship-icons-panel' />,
			labelClass: 'relation-ship-labels-panel',
		},
		{
			id: DatabaseRelationShipType.ONE_TO_MANY,
			name: t(ONE_TO_MANY),
			hasIcon: true,
			icon: <OneToManyIcon className='relation-ship-icons-panel' />,
			labelClass: 'relation-ship-labels-panel',
		},
		{
			id: DatabaseRelationShipType.MANY_TO_ONE,
			name: t(MANY_TO_ONE),
			hasIcon: true,
			icon: <ManyToOneIcon className='relation-ship-icons-panel' />,
			labelClass: 'relation-ship-labels-panel',
		},
		{
			id: DatabaseRelationShipType.MANY_TO_MANY,
			name: t(MANY_TO_MANY),
			hasIcon: true,
			icon: <ManyToManyIcon className='relation-ship-icons-panel' />,
			labelClass: 'relation-ship-labels-panel',
		},
	]

	const containmentOptions = [
		{
			value: DatabaseContainmentType.COMPOSITION,
			label: t(FOREIGN_KEY_DIALOG_CONTAINMENT_COMPOSITION),
		},
		{
			value: DatabaseContainmentType.AGGREGATION,
			label: t(FOREIGN_KEY_DIALOG_CONTAINMENT_AGGREGATION),
		},
	]

	const getAttributesOptions = tablesData[tableIndex].attributes.map(
		(attr, index) => ({
			id: index,
			name: attr.name,
		})
	)

	const getRelationShipType = () => {
		const ref = tablesData[tableIndex].attributes[attrIndex].reference
		if (!isNil(ref)) {
			return ref.relation.association
		}
		return '' as any
	}

	const getContainmentType = () => {
		const ref = tablesData[tableIndex].attributes[attrIndex].reference
		if (!isNil(ref)) {
			return ref.relation.containment.type
		}
		return ''
	}

	const getOptionsAndSelectedElement = () => {
		console.log(tablesData, tableIndex, attrIndex, tablesData[tableIndex])
		const sourceAttrType = tablesData[tableIndex].attributes[attrIndex].type
		let targetModelTableName = null
		const ref = tablesData[tableIndex].attributes[attrIndex].reference
		if (!isNil(ref)) {
			targetModelTableName = ref.targetModel
		}
		const options = []
		let selectedElement = null
		for (let i = 0, iLen = tablesData.length; i < iLen; i++) {
			if (i !== tableIndex) {
				const table = tablesData[i]
				const primaryKeyIndex = findIndex(table.attributes, (attr) => {
					return (
						attr.type === sourceAttrType && (attr.constraints.primary || false)
					)
				})
				if (primaryKeyIndex !== -1) {
					options.push({
						tableIndex: i,
						tableName: table.name,
						attributeName: table.attributes[primaryKeyIndex].name,
						attributeIndex: primaryKeyIndex,
						dataType: sourceAttrType,
					})
					if (targetModelTableName === table.name) {
						selectedElement = options[options.length - 1]
					}
				}
			}
		}
		return { options, selectedElement }
	}

	const getLabelValue = (option: DBRelationPanelToProps) => {
		const labelData = `${option.tableName}: ${option.attributeName} [${option.dataType}]`
		return labelData
	}

	const handleDeleteRelation = () => {
		props.onChange({ reference: null })
	}

	const handleSelectedValue = (_: any, option: any, reason: any) => {
		option = option as DBRelationPanelToProps
		if (reason === 'select-option') {
			const ref = tablesData[tableIndex].attributes[attrIndex].reference
			const reference = Object.assign({}, ref, {
				targetModel: option.tableName,
				column: option.attributeName,
			})
			props.onChange({ reference })
		}
	}

	const handleRelationshipTypeChange = (value: DatabaseRelationShipType) => {
		const ref = tablesData[tableIndex].attributes[attrIndex].reference
		if (!isNil(ref)) {
			const reference = Object.assign({}, ref, {
				relation: Object.assign({}, ref.relation, {
					association: value,
				}),
			})
			props.onChange({ reference })
		}
	}

	const handleContainmentChange = (value: string) => {
		const ref = tablesData[tableIndex].attributes[attrIndex].reference
		if (!isNil(ref)) {
			const reference = Object.assign({}, ref, {
				relation: Object.assign({}, ref.relation, {
					containment: Object.assign({}, ref.relation.containment, {
						type: value,
					}),
				}),
			})
			props.onChange({ reference })
		}
	}

	const renderListOfOptions = (option: DBRelationPanelToProps) => {
		return (
			<Table className='auto-complete-table'>
				<TableBody>
					<TableRow>
						<TableCell className='table-cell'>{option.tableName}</TableCell>
						<TableCell className='table-cell'>
							{option.attributeName} [{option.dataType}]
						</TableCell>
					</TableRow>
				</TableBody>
			</Table>
		)
	}

	const renderTextField = (params: AutocompleteRenderInputParams) => {
		return <TextField {...params} variant='outlined' />
	}

	const renderTo = () => {
		const optionsAndSelectedElement = getOptionsAndSelectedElement()
		return (
			<div className='relation-section relation-section-second'>
				<Typography>{t(TO)}</Typography>
				<Autocomplete<DBRelationPanelToProps>
					value={optionsAndSelectedElement.selectedElement}
					className='auto-search-input'
					options={optionsAndSelectedElement.options}
					getOptionLabel={getLabelValue}
					closeIcon={null}
					onChange={handleSelectedValue}
					renderOption={renderListOfOptions}
					renderInput={renderTextField}
					disabled={!isEditable}
				/>
			</div>
		)
	}

	const renderFrom = () => {
		return (
			<div className='relation-section'>
				<Typography>{t(FROM)}</Typography>
				<IsyDropDown<number>
					options={getTableOptions}
					disabled={true}
					value={tableIndex}
					className='input-base'
					onChange={() => {
						/* nothing to do */
					}}
				/>
				<IsyDropDown<number>
					options={getAttributesOptions}
					disabled={true}
					value={attrIndex}
					className='input-base'
					onChange={() => {
						/* nothing to do */
					}}
				/>
			</div>
		)
	}

	const renderContainment = () => {
		return (
			<div className='relation-section relation-section-second'>
				<Typography>{t(FOREIGN_KEY_DIALOG_CONTAINMENT)}</Typography>
				<IsyRadioGroup
					value={getContainmentType()}
					options={containmentOptions}
					onChange={handleContainmentChange}
					disabled={!isEditable}
				/>
			</div>
		)
	}

	const renderRelations = () => {
		return (
			<div className='relation-section relation-section-second'>
				<Typography>{t(RELATIONSHIP_TYPE)}</Typography>
				<IsyDropDown<DatabaseRelationShipType>
					options={relationshipOptions}
					onChange={handleRelationshipTypeChange}
					value={getRelationShipType()}
					className='input-base'
					disabled={!isEditable}
				/>
			</div>
		)
	}

	const renderHeader = () => {
		return (
			<div className='relation-heading'>
				<RelationshipIcon className='relation-icon' />
				<label className='form-heading'>{t(RELATIONSHIP)}</label>
				<DeleteOutlinedIcon
					className={isEditable ? 'delete-icon' : 'hide-delete-icon'}
					onClick={handleDeleteRelation}
				/>
			</div>
		)
	}

	return (
		<div className='relation-properties-panel'>
			{renderHeader()}
			<div className='relation-scroll-content'>
				{renderFrom()}
				{renderTo()}
				{renderRelations()}
				{renderContainment()}
			</div>
		</div>
	)
}
