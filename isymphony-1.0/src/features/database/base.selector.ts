import { RootState } from '../../base.types'
import { DatabaseBaseState } from './base.types'

export const name = 'databases'
export const selectSlice = (state: RootState): DatabaseBaseState => {
	return state[name]
}
