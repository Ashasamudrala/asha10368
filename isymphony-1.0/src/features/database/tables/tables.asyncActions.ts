import { createAsyncThunk } from '@reduxjs/toolkit'
import { showDialog, DialogTypes } from '../../dialogs'
import { actions as dbActions } from '../database.slice'
import { actions as preferenceActions } from '../../../authAndPermissions/loginUserDetails.slice'
import {
	CONFIRM_MESSAGE_DELETE_TABLE,
	CONFIRM_MESSAGE_DELETE_ATTRIBUTE,
	FOREIGN_KEY_NO_PRIMARY_KEY,
	PRIMARY_KEY_UPDATE_ERROR,
	PRIMARY_KEY_CHANGED_TO_OTHER,
	PRIMARY_KET_DELETE_ERROR,
	PRIMARY_KEY_UPDATE_TYPE,
	EMPTY_TABLE_NAME,
	PRIMARY_KEY_UPDATE_TYPE_ERROR,
	UNIQUE_KEY_UPDATE_TYPE_CONFIRMATION,
	FOREIGN_KEY_UPDATE_TYPE_CONFIRMATION,
	DATABASE_UPDATE_MESSAGE,
	BUTTON_DISCARD,
	BUTTON_SAVE,
	INVALID_TABLE_NAME_MESSAGE,
	INVALID_TABLE_NAME,
	EMPTY_TABLE_NAME_MESSAGE,
	DUPLICATE_TABLE_NAME,
	DUPLICATE_TABLE_NAME_MESSAGE,
	INVALID_COLUMN_NAME,
	EMPTY_COLUMN_NAME,
	DUPLICATE_COLUMN_NAME,
	INVALID_COLUMN_NAME_MESSAGE,
	EMPTY_COLUMN_NAME_MESSAGE,
	DUPLICATE_COLUMN_NAME_MESSAGE,
	UNIQUE_KEY_CHANGED_TO_NONE_CONFORMATION,
	FOREIGN_KEY_CHANGED_TO_NONE_CONFORMATION,
	CONFIRM_MESSAGE_CHANGING_TARGET_MODEL,
	DATABASE_DISCARD_MESSAGE,
	CONFIRM_MESSAGE_CHANGING_TARGET_RELATIONSHIP_TYPE,
	CONFIRM_MESSAGE_CHANGING_TARGET_CONTAINMENT,
	BUTTON_CANCEL,
	DELETE,
} from '../../../utilities/constants'
import i18n from 'i18next'
import {
	selectDatabaseTables,
	selectDbConstraintsByAttributeType,
	selectDatabase,
	selectIsDBModified,
} from '../database.selectors'
import {
	saveDatabase,
	createDatabase,
	fetchAllDatabases,
	releaseLock,
	checkLockStatus,
} from '../database.asyncActions'
import { findIndex, isNil, isEmpty, isNull } from 'lodash'
import { RootState } from '../../../base.types'
import { TableAttributeKeyTypes } from './tables.types'
import {
	UpdateTableActionProps,
	UpdateTableAttributeActionProps,
} from '../database.types'
import {
	ConnectForeignKeyDataProps,
	ConnectForeignKeyReturnProps,
} from '../../dialogs/connectForeignKey/connectForeignKey.types'
import { CreateDatabaseReturnProps } from '../../dialogs/createDatabase/createDatabase.types'
import { actions as notificationActions } from '../../../infrastructure/notificationPopup/notificationPopup.slice'
import {
	selectGetDatabasePreferenceStatus,
	selectGetLoginUserId,
} from '../../../authAndPermissions/loginUserDetails.selectors'
import { HelpModuleStatus } from '../../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../../authAndPermissions/loginUserDetails.asyncActions'
const nameRegex = /^[a-zA-Z_]+[a-zA-Z0-9_#@$ ]*$/i

export const conformTableDelete = createAsyncThunk(
	'tables/confirmTableDelete',
	async (data: { tableIndex: number }, { dispatch, getState }) => {
		const tables = selectDatabaseTables(getState() as RootState)
		const tableName = tables[data.tableIndex].name
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => {
					dispatch(dbActions.deleteTable(data))
				},
				data: {
					message: i18n.t(CONFIRM_MESSAGE_DELETE_TABLE, { name: tableName }),
				},
			})
		)
	}
)

export const conformTableAttributeDelete = createAsyncThunk(
	'tables/confirmTableAttributeDelete',
	async (
		data: { tableIndex: number; attrIndex: number },
		{ dispatch, getState }
	) => {
		const tables = selectDatabaseTables(getState() as RootState)
		const attr = tables[data.tableIndex].attributes[data.attrIndex]
		if (attr.constraints.primary) {
			return await dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(PRIMARY_KET_DELETE_ERROR),
					},
				})
			)
		}
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => dispatch(dbActions.deleteTableAttribute(data)),
				data: {
					message: i18n.t(CONFIRM_MESSAGE_DELETE_ATTRIBUTE, {
						name: attr.name,
					}),
				},
			})
		)
	}
)

const setAttributeForeignKey = createAsyncThunk(
	'tables/setAttributeForeignKey',
	async (
		data: {
			tableIndex: number
			attrIndex: number
			updateCurrentStep: (confirm: boolean) => void
		},
		{ dispatch, getState }
	) => {
		const tables = selectDatabaseTables(getState() as RootState)
		const primaryKeyIndex: number = findIndex(
			tables[data.tableIndex].attributes,
			(attr) => attr.constraints.primary || false
		)
		const databasePreferenceData = selectGetDatabasePreferenceStatus(
			getState() as RootState
		)
		if (primaryKeyIndex === -1) {
			return await dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(FOREIGN_KEY_NO_PRIMARY_KEY),
					},
				})
			)
		} else {
			const okayCallBack = (result: ConnectForeignKeyReturnProps) => {
				dispatch(
					dbActions.setTableAttributeForeignKey({
						...result,
						...data,
						primaryKeyIndex,
					})
				)
				data.updateCurrentStep(true)
			}
			const handleOnClose = () => {
				data.updateCurrentStep(false)
			}

			return await dispatch(
				showDialog({
					type: DialogTypes.CONNECT_FOREIGN_KEY,
					className:
						databasePreferenceData.status === HelpModuleStatus.INPROGRESS &&
						'isy-step-connect-foreign-key',
					onOkay: okayCallBack,
					onCancel: handleOnClose,
					data: {
						tablesData: tables,
						...data,
					} as ConnectForeignKeyDataProps,
				})
			)
		}
	}
)

export const setAttributeKey = createAsyncThunk(
	'tables/setAttributeKey',
	async (
		data: {
			keyType: TableAttributeKeyTypes
			tableIndex: number
			attrIndex: number
			updateCurrentStep: (confirm: boolean) => void
		},
		{ dispatch, getState }
	) => {
		const key = data.keyType
		const tables = selectDatabaseTables(getState() as RootState)
		const attributes = tables[data.tableIndex].attributes
		const oldPrimaryKey = findIndex(
			attributes,
			(a) => a.constraints.primary || false
		)
		if (
			oldPrimaryKey === data.attrIndex &&
			key !== TableAttributeKeyTypes.PRIMARY_KEY
		) {
			return await dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(PRIMARY_KEY_UPDATE_ERROR),
					},
				})
			)
		}

		switch (key) {
			case TableAttributeKeyTypes.NONE: {
				const okayFunction = () => {
					dispatch(
						dbActions.updateTableAttributeConstraints({
							tableIndex: data.tableIndex,
							attrIndex: data.attrIndex,
							constraints: {
								unique: false,
								primary: false,
							},
						})
					)
				}
				const attribute = attributes[data.attrIndex]
				if (attribute.constraints.unique) {
					// has unique key
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(UNIQUE_KEY_CHANGED_TO_NONE_CONFORMATION),
							},
						})
					)
				}
				if (!isNil(attribute.reference)) {
					// has foreign key
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(FOREIGN_KEY_CHANGED_TO_NONE_CONFORMATION, {
									columnName: attribute.name,
								}),
								okayButtonLabel: i18n.t(DELETE),
								cancelButtonLabel: i18n.t(BUTTON_CANCEL),
							},
						})
					)
				}
				return okayFunction()
			}
			case TableAttributeKeyTypes.PRIMARY_KEY:
				if (oldPrimaryKey !== data.attrIndex) {
					const okayFunction = () =>
						dispatch(
							dbActions.updateTableAttributeConstraints({
								tableIndex: data.tableIndex,
								attrIndex: data.attrIndex,
								constraints: {
									unique: false,
									primary: true,
									required: true,
								},
							})
						)
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(PRIMARY_KEY_CHANGED_TO_OTHER),
							},
						})
					)
				}
				break
			case TableAttributeKeyTypes.UNIQUE_KEY:
				return dispatch(
					dbActions.updateTableAttributeConstraints({
						tableIndex: data.tableIndex,
						attrIndex: data.attrIndex,
						constraints: {
							unique: true,
							primary: false,
						},
					})
				)
			case TableAttributeKeyTypes.FOREIGN_KEY:
				return dispatch(
					setAttributeForeignKey({
						tableIndex: data.tableIndex,
						attrIndex: data.attrIndex,
						updateCurrentStep: data.updateCurrentStep,
					})
				)
			default:
		}
	}
)

export const updateTableAsync = createAsyncThunk(
	'tables/updateTableAsync',
	async (data: UpdateTableActionProps, { dispatch, getState }) => {
		const tables = selectDatabaseTables(getState() as RootState)
		const okayFunction = () => dispatch(dbActions.updateTable(data))
		if (!isNil(data.table.name)) {
			if (isEmpty(data.table.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(EMPTY_TABLE_NAME),
						data: {
							message: i18n.t(EMPTY_TABLE_NAME_MESSAGE),
						},
					})
				)
			}
			if (!nameRegex.test(data.table.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(INVALID_TABLE_NAME),
						data: {
							message: i18n.t(INVALID_TABLE_NAME_MESSAGE),
						},
					})
				)
			}
			const sameNameIndex = findIndex(tables, (t) => t.name === data.table.name)
			if (sameNameIndex !== -1 && sameNameIndex !== data.tableIndex) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(DUPLICATE_TABLE_NAME),
						data: {
							message: i18n.t(DUPLICATE_TABLE_NAME_MESSAGE),
						},
					})
				)
			}
		}
		okayFunction()
	}
)

export const updateTableAttributeAsync = createAsyncThunk(
	'tables/updateTableAttributeAsync',
	async (data: UpdateTableAttributeActionProps, { dispatch, getState }) => {
		const tables = selectDatabaseTables(getState() as RootState)
		const attribute = tables[data.tableIndex].attributes[data.attrIndex]
		const okayFunction = () => dispatch(dbActions.updateTableAttribute(data))

		if (!isNil(data.attr.name)) {
			if (isEmpty(data.attr.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(EMPTY_COLUMN_NAME),
						data: {
							message: i18n.t(EMPTY_COLUMN_NAME_MESSAGE),
						},
					})
				)
			}
			if (!nameRegex.test(data.attr.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(INVALID_COLUMN_NAME),
						data: {
							message: i18n.t(INVALID_COLUMN_NAME_MESSAGE),
						},
					})
				)
			}
			const sameNameIndex = findIndex(
				tables[data.tableIndex].attributes,
				(a) => a.name === data.attr.name
			)
			if (sameNameIndex !== -1 && sameNameIndex !== data.attrIndex) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(DUPLICATE_COLUMN_NAME),
						data: {
							message: i18n.t(DUPLICATE_COLUMN_NAME_MESSAGE),
						},
					})
				)
			}
		}

		if (!isNil(data.attr.type)) {
			const constraintsConfig = selectDbConstraintsByAttributeType(
				getState() as RootState,
				data.attr.type
			)
			let hasPrimary = false
			let hasUnique = false
			for (let i = 0, iLen = constraintsConfig.length; i < iLen; i++) {
				if (constraintsConfig[i].name === 'primary') {
					hasPrimary = true
				} else if (constraintsConfig[i].name === 'unique') {
					hasUnique = true
				}
			}

			if (attribute.constraints.primary) {
				if (!hasPrimary) {
					return await dispatch(
						showDialog({
							type: DialogTypes.ALERT,
							data: {
								message: i18n.t(PRIMARY_KEY_UPDATE_TYPE_ERROR),
							},
						})
					)
				}
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(PRIMARY_KEY_UPDATE_TYPE),
						},
					})
				)
			}
			if (attribute.constraints.unique) {
				if (!hasUnique) {
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: () => {
								dispatch(
									dbActions.updateTableAttributeConstraints({
										tableIndex: data.tableIndex,
										attrIndex: data.attrIndex,
										constraints: {
											unique: false,
										},
									})
								)
								okayFunction()
							},
							data: {
								message: i18n.t(UNIQUE_KEY_UPDATE_TYPE_CONFIRMATION),
							},
						})
					)
				}
			}
			if (!isNil(attribute.reference)) {
				if (!hasPrimary) {
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: () => {
								data.attr.reference = null
								okayFunction()
							},
							data: {
								message: i18n.t(FOREIGN_KEY_UPDATE_TYPE_CONFIRMATION),
							},
						})
					)
				}
			}
		}

		if (isNull(data.attr.reference) && !isNil(attribute.reference)) {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: okayFunction,
					data: {
						message: i18n.t(FOREIGN_KEY_CHANGED_TO_NONE_CONFORMATION, {
							columnName: attribute.name,
						}),
						okayButtonLabel: i18n.t(DELETE),
						cancelButtonLabel: i18n.t(BUTTON_CANCEL),
					},
				})
			)
		}

		if (!isNil(attribute.reference) && !isNil(data.attr.reference)) {
			if (data.attr.reference.targetModel !== attribute.reference.targetModel) {
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(CONFIRM_MESSAGE_CHANGING_TARGET_MODEL, {
								targetTableName: data.attr.reference.targetModel,
							}),
						},
					})
				)
			}
			if (
				data.attr.reference.relation.association !==
				attribute.reference.relation.association
			) {
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(
								CONFIRM_MESSAGE_CHANGING_TARGET_RELATIONSHIP_TYPE,
								{
									targetRelationShipType:
										data.attr.reference.relation.association,
								}
							),
						},
					})
				)
			}
			if (
				data.attr.reference.relation.containment.type !==
				attribute.reference.relation.containment.type
			) {
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(CONFIRM_MESSAGE_CHANGING_TARGET_CONTAINMENT, {
								targetContainment:
									data.attr.reference.relation.containment.type,
							}),
						},
					})
				)
			}
		}

		okayFunction()
	}
)

export const confirmAccordionChange = createAsyncThunk(
	'tables/confirmAccordionChange',
	async (data: { index: number; appid: string }, { dispatch, getState }) => {
		const selectedDatabase = selectDatabase(getState() as RootState)
		if (isNil(selectedDatabase)) {
			return
		}
		const dbName = selectedDatabase.name
		const isDBModified = selectIsDBModified(getState() as RootState)
		const conformationCallBack = () => {
			dispatch(saveDatabase()).then(() => {
				dispatch(dbActions.updateSearchTerm('')) as any
				dispatch(releaseLock()).then(() => {
					dispatch(dbActions.updateSelectedDatabaseIndex(data.index)) as any
					dispatch(
						checkLockStatus(selectGetLoginUserId(getState() as RootState))
					)
				})
			})
		}
		const handleOnClose = () => {
			dispatch(
				notificationActions.notifySuccess(
					i18n.t(DATABASE_DISCARD_MESSAGE, { name: dbName })
				)
			)
			dispatch(dbActions.updateDatabaseOnCancel(null))
			dispatch(dbActions.updateSearchTerm(''))
			dispatch(releaseLock()).then(() => {
				dispatch(dbActions.updateSelectedDatabaseIndex(data.index))
				dispatch(checkLockStatus(selectGetLoginUserId(getState() as RootState)))
			})
		}
		if (!isDBModified) {
			dispatch(dbActions.updateSearchTerm(''))
			dispatch(releaseLock()).then(() => {
				dispatch(dbActions.updateSelectedDatabaseIndex(data.index))
				dispatch(checkLockStatus(selectGetLoginUserId(getState() as RootState)))
			})
		} else {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(DATABASE_UPDATE_MESSAGE, { name: dbName }),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		}
	}
)

export const confirmOnAddDatabase = createAsyncThunk(
	'tables/confirmOnAddDatabase',
	async (data: { appId: string }, { dispatch, getState }) => {
		const selectedDatabase = selectDatabase(getState() as RootState)
		if (isNil(selectedDatabase)) {
			return
		}
		const isDBModified = selectIsDBModified(getState() as RootState)
		const dbName = selectedDatabase.name
		const conformationCallBack = () => {
			dispatch(saveDatabase()).then(() =>
				dispatch(createDatabaseAsync({ appId: data.appId }))
			)
		}
		const handleOnClose = () => {
			dispatch(createDatabaseAsync({ appId: data.appId }))
			dispatch(dbActions.updateDatabaseOnCancel(null))
		}
		if (!isDBModified) {
			dispatch(createDatabaseAsync({ appId: data.appId }))
		} else {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(DATABASE_UPDATE_MESSAGE, { name: dbName }),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		}
	}
)

export const createDatabaseAsync = createAsyncThunk(
	'table/createDatabaseAsync',
	async (
		data: { appId: string; onCreateDatabaseStep?: (goToNext: any) => void },
		{ dispatch, getState }
	) => {
		const databasePreferenceData = selectGetDatabasePreferenceStatus(
			getState() as RootState
		)
		const okayFunction = (name: CreateDatabaseReturnProps) => {
			dispatch(createDatabase({ appId: data.appId, ...name })).then(
				(response) => {
					if (response && response.payload) {
						dispatch(fetchAllDatabases()).then(() => {
							if (data.onCreateDatabaseStep) {
								data.onCreateDatabaseStep(response.payload)
							}
						})
					}
				}
			)
		}
		const handleCancel = () => {
			if (databasePreferenceData.status === HelpModuleStatus.INPROGRESS) {
				dispatch(
					preferenceActions.updateDatabasePreferences({
						status: HelpModuleStatus.INCOMPLETE,
						currentStep: 1,
					})
				)
				dispatch(updatePreferences())
			}
		}
		return await dispatch(
			showDialog({
				type: DialogTypes.CREATE_DATABASE,
				className:
					databasePreferenceData.status === HelpModuleStatus.INPROGRESS &&
					'isy-steps-create-database',
				onOkay: okayFunction,
				onCancel: handleCancel,
				data: {},
			})
		)
	}
)
