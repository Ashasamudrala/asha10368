import slice from './tables.slice'
import { selectSlice as baseSelector } from '../base.selector'
import { RootState } from '../../../base.types'
import { TablesState } from './tables.types'

export const selectSlice = (state: RootState): TablesState =>
	baseSelector(state)[slice.name]

export const selectEditItem = (state: RootState): string | null =>
	selectSlice(state).editItem

export const getSelectedTableIndexForPosition = (
	state: RootState
): number | null => selectSlice(state).selectedTableIndexForPosition

export const getShowPropertiesPanel = (state: RootState): boolean =>
	selectSlice(state).showPropertiesPanel
