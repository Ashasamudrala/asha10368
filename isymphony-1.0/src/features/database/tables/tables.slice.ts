import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { DatabaseSubReducersNames } from '../base.types'
import { TablesState } from './tables.types'

const initialState: TablesState = {
	editItem: null,
	selectedTableIndexForPosition: null,
	showPropertiesPanel: true,
}

const slice = createSlice<
	TablesState,
	SliceCaseReducers<TablesState>,
	DatabaseSubReducersNames.TABLES
>({
	name: DatabaseSubReducersNames.TABLES,
	initialState,
	reducers: {
		// synchronous actions
		setEditTableView(state: TablesState, action: Action<string | null>) {
			state.editItem = action.payload
		},
		setSelectedTableIndexForPosition(
			state: TablesState,
			action: Action<number | null>
		) {
			state.selectedTableIndexForPosition = action.payload
		},
		toggleShowPropertiesPanel(state: TablesState) {
			state.showPropertiesPanel = !state.showPropertiesPanel
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
