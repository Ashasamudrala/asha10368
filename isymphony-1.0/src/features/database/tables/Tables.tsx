import React, { useEffect } from 'react'
import { v4 } from 'uuid'
import { isNil, findIndex, find } from 'lodash'
import { DBToolbar } from '../components/dbtoolbar/DBToolbar'
import { useTranslation } from 'react-i18next'
import {
	DATABASE_TRANSLATIONS,
	PROPERTIES_EMPTY_TEXT,
	EXPAND,
	COLLAPSE,
} from '../../../utilities/constants'
import { useSelector, useDispatch } from 'react-redux'
import {
	selectEditItem,
	getSelectedTableIndexForPosition,
	getShowPropertiesPanel,
} from './tables.selectors'
import { actions as tbActions } from './tables.slice'
import { actions as dbActions } from '../database.slice'
import {
	selectDatabaseTables,
	selectDbAttributeTypes,
	selectIsDBModified,
	selectSelectedAttrIndex,
	selectSelectedTableIndex,
	selectSelectedDatabaseId,
	getLockedBy,
	selectPreviousAllDatabase,
} from '../database.selectors'
import { DBTable } from '../components/DBTable/DBTable'
import {
	DbCanvas,
	DbCanvasItemLinkProps,
	DbCanvasItemProps,
} from '../components/canvas/DBCanvas'
import { DBTablePanel } from '../components/DBTablePanel/DBTablePanel'
import { DBRelationPanel } from '../components/DBRelationPanel/DBRelationPanel'
import { isParentUntil } from '../../../utilities/utilities'
import {
	saveDatabase,
	loadDatabaseCanvas,
	acquireLock,
	refreshLockStatus,
} from '../database.asyncActions'
import {
	conformTableDelete,
	conformTableAttributeDelete,
	setAttributeKey,
	updateTableAttributeAsync,
	updateTableAsync,
} from './tables.asyncActions'
import './tables.scss'
import { OneToOneIcon } from '../../../icons/OneToOne'
import { OneToManyIcon } from '../../../icons/OneToMany'
import { ManyToOneIcon } from '../../../icons/ManyToOne'
import { ManyToManyIcon } from '../../../icons/ManyToMany'
import LastPageIcon from '@material-ui/icons/LastPage'
import FirstPageIcon from '@material-ui/icons/FirstPage'
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined'
import {
	DatabaseLockStatus,
	DatabaseTableAttributeProps,
	DatabaseTableConstraintsProps,
	DatabaseTableProps,
} from '../database.types'
import { TableAttributeKeyTypes } from './tables.types'
import { releaseLockConformation } from '../database.controller'

export interface TablesProps {
	appId: string
	lockStatus: DatabaseLockStatus
	currentStep: number
	isTableStepsEnabled: boolean
	isDatabaseLoaded?: boolean
	onUpdateCurrentStep: (stepNumber: number, updateType?: string) => void
	onUpdateRelationShipDialogStep: (confirm: boolean) => void
	onSelectedTableIndexAndAttrIndex: (
		tableIndex: number | null,
		attrIndex: number | null
	) => any
}

export function Tables(props: TablesProps) {
	const selectedTableIndex = useSelector(selectSelectedTableIndex)
	const selectedDatabaseId = useSelector(selectSelectedDatabaseId)
	const selectedAttrIndex = useSelector(selectSelectedAttrIndex)
	const editItem = useSelector(selectEditItem)
	const attributeTypes = useSelector(selectDbAttributeTypes)
	const tablesData = useSelector(selectDatabaseTables)
	const showPropertiesPanel = useSelector(getShowPropertiesPanel)
	const lockedBy = useSelector(getLockedBy)
	const selectedTableIndexForPosition = useSelector(
		getSelectedTableIndexForPosition
	)
	const previousDatabases = useSelector(selectPreviousAllDatabase)
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const { lockStatus, currentStep, isTableStepsEnabled } = props
	const isDBModified = useSelector(selectIsDBModified)
	const dispatch = useDispatch()

	useEffect(() => {
		const listener = (event: MouseEvent) => {
			let eventListener = [
				'MuiPopover-root',
				'database-container',
				'database-container-edit',
				'MuiDialog-root',
				'input-base',
				'select-class',
				'property-panel-toggle-icon',
				'isy-toolbar-button',
				'add-new-table',
				'steps-tooltipReferenceLayer',
				'steps-helperLayer',
				'steps-parts-overlay',
				'standard-btn',
			]
			if (
				!isNil(event.srcElement) &&
				!isParentUntil(event.srcElement as Element, eventListener)
			) {
				dispatch(tbActions.setEditTableView(null))
			}
		}
		document.addEventListener('click', listener)
		return () => {
			document.removeEventListener('click', listener)
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	// empty array is important to say its should happen on mount.

	useEffect(() => {
		dispatch(loadDatabaseCanvas())
	}, [selectedDatabaseId, dispatch])

	useEffect(() => {
		if (
			previousDatabases &&
			isTableStepsEnabled &&
			(props.currentStep === 15 || props.currentStep === 13)
		) {
			if (props.isDatabaseLoaded) {
				props.onUpdateCurrentStep(15)
			} else {
				props.onUpdateCurrentStep(13)
			}
		}
	}, [previousDatabases]) // eslint-disable-line react-hooks/exhaustive-deps

	const getSelectedTableId = () => {
		if (!isNil(selectedTableIndex)) {
			return tablesData[selectedTableIndex].id
		}
		return null
	}

	const getTableHeight = (table: DatabaseTableProps) => {
		const attrLength = table.attributes.length > 8 ? 8 : table.attributes.length
		const rowsHeight = attrLength * 31
		const headerHeight = 33
		const buttonHeight = 32 + (attrLength - 1)
		if (editItem === table.id) {
			return rowsHeight + headerHeight + buttonHeight
		}
		return rowsHeight + headerHeight
	}

	const getLinksOfTable = (
		table: DatabaseTableProps
	): DbCanvasItemLinkProps[] => {
		const links: DbCanvasItemLinkProps[] = []
		for (let i = 0, iLen = table.attributes.length; i < iLen; i++) {
			const ref = table.attributes[i].reference
			if (!isNil(ref)) {
				const targetTable = find(tablesData, (t) => t.name === ref.targetModel)
				if (!isNil(targetTable)) {
					const targetAttrIndex = findIndex(
						targetTable.attributes,
						(a) => a.name === ref.column
					)
					const targetTableId = targetTable.id
					links.push({
						targetTableId,
						targetAttrIndex,
						sourceTableId: table.id,
						sourceAttrIndex: i,
						targetTableName: targetTable.name,
						targetAttrName: targetTable.attributes[targetAttrIndex].name,
						sourceTableName: table.name,
						sourceAttrName: table.attributes[i].name,
						association: ref.relation.association,
					})
				}
			}
		}
		return links
	}

	const getAssociationIcon = (
		data: DbCanvasItemLinkProps,
		x: number,
		y: number
	): React.ReactNode => {
		const classes =
			'DbCanvasLine ' + (getIsLinkSelected(data) ? 'DbCanvasLineSelected' : '')
		switch (data.association) {
			case 'OneToOne':
				return (
					<OneToOneIcon
						x={x - 11 + 'px'}
						y={y - 15.5 + 'px'}
						className={classes}
					/>
				)
			case 'OneToMany':
				return (
					<OneToManyIcon
						x={x - 9 + 'px'}
						y={y - 16.3846154 + 'px'}
						className={classes}
					/>
				)
			case 'ManyToOne':
				return (
					<ManyToOneIcon
						x={x - 9 + 'px'}
						y={y - 16.3846154 + 'px'}
						className={classes}
					/>
				)
			case 'ManyToMany':
				return (
					<ManyToManyIcon
						x={x - 9 + 'px'}
						y={y - 16.3846154 + 'px'}
						className={classes}
					/>
				)
			default:
				return null
		}
	}

	const getTooltipForRelation = (data: DbCanvasItemLinkProps) => {
		return (
			data.sourceTableName +
			':' +
			data.sourceAttrName +
			' -> ' +
			data.targetTableName +
			':' +
			data.targetAttrName
		)
	}

	const getIsLinkSelected = (data: DbCanvasItemLinkProps) => {
		return (
			data.sourceTableId === getSelectedTableId() &&
			data.sourceAttrIndex === selectedAttrIndex
		)
	}

	const getCanvasTables = () => {
		return tablesData.map((item, tableIndex) => {
			return {
				id: item.id,
				width: 230,
				height: getTableHeight(item),
				x: isNil(item.position) ? (tableIndex + 1) * 300 : item.position.x,
				y: isNil(item.position) ? 20 : item.position.y,
				links: getLinksOfTable(item),
				data: item,
				index: tableIndex,
			}
		})
	}

	/**
	 * Delete attributes from database table when we click delete icon.
	 */
	const handleDeleteAttribute = (tableIndex: number, attrIndex: number) => {
		dispatch(conformTableAttributeDelete({ tableIndex, attrIndex }))
	}

	/**
	 * Add attributes to the database table based on selecting.
	 */
	const handleAddAttribute = (tableIndex: number) => {
		dispatch(dbActions.addAttribute({ tableIndex }))
	}

	const handleAcquireLock = () => {
		dispatch(acquireLock())
		if (isTableStepsEnabled && lockStatus === DatabaseLockStatus.LOCK_PRESENT) {
			props.onUpdateCurrentStep(currentStep)
		}
	}

	const handleReleaseLock = () => {
		dispatch(releaseLockConformation())
	}

	const handleRefreshLock = () => {
		dispatch(refreshLockStatus())
	}

	const handleTableChange = (
		table: Partial<DatabaseTableProps>,
		tableIndex: number
	) => {
		// dispatch update table
		dispatch(updateTableAsync({ table, tableIndex }))
	}

	const onTableAttributeChange = (
		attr: Partial<DatabaseTableAttributeProps>,
		tableIndex: number,
		attrIndex: number
	) => {
		// dispatch update table attribute
		dispatch(updateTableAttributeAsync({ attr, tableIndex, attrIndex }))
	}

	const handleSelectTable = (tb: DbCanvasItemProps) => {
		props.onSelectedTableIndexAndAttrIndex(tb.index, null)
	}

	const handleSelectTableIndex = (tableIndex: number) => {
		props.onSelectedTableIndexAndAttrIndex(tableIndex, null)
		setTimeout(() => {
			dispatch(tbActions.setSelectedTableIndexForPosition(null))
		}, 10)
		dispatch(tbActions.setSelectedTableIndexForPosition(tableIndex))
	}

	const handleSelectRelation = (data: DbCanvasItemLinkProps) => {
		const index = findIndex(
			tablesData,
			(table) => table.id === data.sourceTableId
		)
		props.onSelectedTableIndexAndAttrIndex(index, data.sourceAttrIndex)
	}

	const handleCanvasClick = () => {
		dispatch(tbActions.setEditTableView(null))
		props.onSelectedTableIndexAndAttrIndex(null, null)
	}

	const handleSetEditTable = (id: string | null) => {
		dispatch(tbActions.setEditTableView(id))
	}

	const handleAddNewTable = () => {
		const id = v4()
		dispatch(dbActions.addTable({ id: id }))
		dispatch(tbActions.setEditTableView(id))
		setTimeout(() => {
			dispatch(tbActions.setSelectedTableIndexForPosition(null))
		}, 10)
		dispatch(tbActions.setSelectedTableIndexForPosition(tablesData.length))

		if (isTableStepsEnabled) {
			props.onSelectedTableIndexAndAttrIndex(null, null)
			if (
				currentStep === 2 ||
				currentStep === 4 ||
				currentStep === 6 ||
				currentStep === 8
			) {
				setTimeout(() => {
					props.onUpdateCurrentStep(currentStep)
				}, 700)
			}
		}
	}

	const handleUpdateSteps = () => {
		if (isTableStepsEnabled) {
			setTimeout(() => {
				props.onUpdateCurrentStep(currentStep)
			}, 700)
		}
	}

	const updateCurrentStep = (confirm: boolean) => {
		props.onUpdateRelationShipDialogStep(confirm)
	}

	const handleDeleteTable = (tableIndex: number) => {
		dispatch(conformTableDelete({ tableIndex }))
	}

	const handleSetKey = (
		tableIndex: number,
		attrIndex: number,
		keyType: TableAttributeKeyTypes
	) => {
		dispatch(
			setAttributeKey({ tableIndex, attrIndex, keyType, updateCurrentStep })
		)
		if (isTableStepsEnabled) {
			setTimeout(() => {
				props.onUpdateCurrentStep(12, 'key')
			}, 300)
		}
	}

	const handleTableReposition = (
		data: DbCanvasItemProps,
		x: number,
		y: number
	) => {
		dispatch(dbActions.updateTablePosition({ tableIndex: data.index, x, y }))
	}

	const handleSaveDatabase = () => {
		dispatch(saveDatabase())
	}

	const handleUpdateTableAttributeConstraints = (
		constraints: Partial<DatabaseTableConstraintsProps>,
		tableIndex: number,
		attrIndex: number
	) => {
		dispatch(
			dbActions.updateTableAttributeConstraints({
				constraints,
				tableIndex,
				attrIndex,
			})
		)
	}

	const handleToggleProperties = () => {
		dispatch(tbActions.toggleShowPropertiesPanel(null))
	}

	const handleDeleteKeyDownInCanvas = () => {
		if (
			!isNil(selectedTableIndex) &&
			props.lockStatus === DatabaseLockStatus.LOCK_ACQUIRED
		) {
			if (!isNil(selectedAttrIndex)) {
				onTableAttributeChange(
					{ reference: null },
					selectedTableIndex,
					selectedAttrIndex
				)
			} else {
				handleDeleteTable(selectedTableIndex)
			}
		}
	}

	const renderToolbarComponent = () => {
		return (
			<DBToolbar
				isEnabledSteps={isTableStepsEnabled}
				lockStatus={props.lockStatus}
				lockedBy={lockedBy}
				onAddNewTable={handleAddNewTable}
				tablesData={tablesData}
				isDBModified={isDBModified}
				onSaveDatabase={handleSaveDatabase}
				onTableSelect={handleSelectTableIndex}
				onAcquireLock={handleAcquireLock}
				onReleaseLock={handleReleaseLock}
				onRefreshLock={handleRefreshLock}
			/>
		)
	}

	const renderTableComponent = (table: DbCanvasItemProps, index: number) => {
		return (
			<foreignObject
				width={table.width + 'px'}
				height={table.height + 'px'}
				x={'0px'}
				y={'0px'}
			>
				<DBTable
					table={table.data}
					tableIndex={index}
					isSelected={index === selectedTableIndex && isNil(selectedAttrIndex)}
					isLastIndex={tablesData.length - 1 === index}
					attributeTypes={attributeTypes}
					isEditMode={editItem === table.id}
					isEditable={props.lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					isLimitedView={false}
					setEditMode={handleSetEditTable}
					onChange={handleTableChange}
					onAttrChange={onTableAttributeChange}
					onAddAttr={handleAddAttribute}
					onDeleteAttr={handleDeleteAttribute}
					onDelete={handleDeleteTable}
					onSetKey={handleSetKey}
					onSetStepKey={handleUpdateSteps}
				/>
			</foreignObject>
		)
	}

	const renderCanvas = () => {
		return (
			<div className='DbCanvasParentContainer'>
				<DbCanvas
					list={getCanvasTables()}
					selected={getSelectedTableId()}
					canReposition={props.lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					positionCenterToRef={selectedTableIndexForPosition}
					onSelectNode={handleSelectTable}
					onSelectLink={handleSelectRelation}
					onCanvasClick={handleCanvasClick}
					onReposition={handleTableReposition}
					onRenderItem={renderTableComponent}
					getLinkTooltip={getTooltipForRelation}
					getIsLinkSelected={getIsLinkSelected}
					getLineIcon={getAssociationIcon}
					onDeleteKeyDown={handleDeleteKeyDownInCanvas}
				/>
			</div>
		)
	}

	const renderRelationProperties = () => {
		if (!isNil(selectedTableIndex) && !isNil(selectedAttrIndex)) {
			return (
				<DBRelationPanel
					tablesData={tablesData}
					isEditable={props.lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					tableIndex={selectedTableIndex}
					attrIndex={selectedAttrIndex}
					onChange={(data: Partial<DatabaseTableAttributeProps>) =>
						onTableAttributeChange(data, selectedTableIndex, selectedAttrIndex)
					}
				/>
			)
		}
		return null
	}

	const renderTableProperties = () => {
		if (!isNil(selectedTableIndex)) {
			return (
				<DBTablePanel
					tableData={tablesData[selectedTableIndex]}
					attributeTypes={attributeTypes}
					isEditable={props.lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					onTableDelete={() => handleDeleteTable(selectedTableIndex)}
					onTableChange={(data: Partial<DatabaseTableProps>) =>
						handleTableChange(data, selectedTableIndex)
					}
					onAttributeChange={(
						data: Partial<DatabaseTableAttributeProps>,
						attrIndex: number
					) => onTableAttributeChange(data, selectedTableIndex, attrIndex)}
					onAddAttribute={() => handleAddAttribute(selectedTableIndex)}
					onAttributeConstraintChange={(
						d: Partial<DatabaseTableConstraintsProps>,
						attrIndex: number
					) =>
						handleUpdateTableAttributeConstraints(
							d,
							selectedTableIndex,
							attrIndex
						)
					}
					onDeleteTableAttribute={(attrIndex: number) =>
						handleDeleteAttribute(selectedTableIndex, attrIndex)
					}
					onAttributeKeyChange={(
						key: TableAttributeKeyTypes,
						attrIndex: number
					) => handleSetKey(selectedTableIndex, attrIndex, key)}
				/>
			)
		}
		return null
	}

	const renderEmptyProperties = () => {
		return (
			<div className='empty-properties'>
				<div className='icon-holder'>
					<SettingsOutlinedIcon className='icon' />
				</div>
				<div className='text'>{t(PROPERTIES_EMPTY_TEXT)}</div>
			</div>
		)
	}

	const renderProperties = () => {
		if (isNil(selectedTableIndex)) {
			return renderEmptyProperties()
		}
		if (!isNil(selectedAttrIndex)) {
			return renderRelationProperties()
		}
		return renderTableProperties()
	}

	const renderMainArea = () => {
		return (
			<div
				className={
					showPropertiesPanel ? 'tables-main-view-props' : 'tables-main-view'
				}
			>
				{renderCanvas()}
			</div>
		)
	}

	const renderPropertiesToggleIcon = () => {
		return (
			<div
				className='property-panel-toggle-icon'
				onClick={handleToggleProperties}
				title={showPropertiesPanel ? t(COLLAPSE) : t(EXPAND)}
			>
				{showPropertiesPanel ? (
					<LastPageIcon className='icon' />
				) : (
					<FirstPageIcon className='icon' />
				)}
			</div>
		)
	}

	const renderTablesView = () => {
		return (
			<div className='table-view'>
				{renderToolbarComponent()}
				<div className='canvas-properties-container'>
					{renderMainArea()}
					{showPropertiesPanel && renderProperties()}
					{renderPropertiesToggleIcon()}
				</div>
			</div>
		)
	}

	const renderView = () => {
		return renderTablesView()
	}

	return renderView()
}
