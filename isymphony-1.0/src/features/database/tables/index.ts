import { Tables } from './Tables'
import slice from './tables.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export default Tables
