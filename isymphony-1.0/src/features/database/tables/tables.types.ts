export enum TableAttributeKeyTypes {
	PRIMARY_KEY = 'PrimaryKey',
	NONE = 'None',
	FOREIGN_KEY = 'ForeignKey',
	UNIQUE_KEY = 'UniqueKey',
}

export interface TablesState {
	editItem: string | null
	selectedTableIndexForPosition: number | null
	showPropertiesPanel: boolean
}
