import { combineReducers } from 'redux'
import database from './database.slice'
import tables from './tables/tables.slice'
import databaseSettings from './databaseSettings/databaseSettings.slice'
import * as baseSelector from './base.selector'
import { DatabaseBaseState, DatabaseSubReducersNames } from './base.types'

export const reducer = combineReducers<DatabaseBaseState>({
	[database.name]: database.reducer,
	[tables.name as DatabaseSubReducersNames.TABLES]: tables.reducer,
	[databaseSettings.name as DatabaseSubReducersNames.DATABASE_SETTINGS]: databaseSettings.reducer,
})

export const { name } = baseSelector

export * from './base.types'
