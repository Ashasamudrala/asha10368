import { DatabaseVendorOptionsTypes } from '../../../utilities/apiEnumConstants'

export enum ErrorTypes {
	NONE = 1,
	EMPTY = 2,
	INVALID = 3,
	DUPLICATE = 4,
	INVALID_LENGTH = 5,
}

export interface DatabaseSettingState {
	password: string
	userName: string
	port: number | string
	type: string
	host: string
	platform: DatabaseVendorOptionsTypes
	name: string
	environmentType: string
	provider: string
}

export interface DatabaseSettingErrorState {
	password?: ErrorTypes
	userName?: ErrorTypes
	port?: ErrorTypes
	host?: ErrorTypes
	platform?: ErrorTypes
	name?: ErrorTypes
	environmentType?: ErrorTypes
}

export interface DatabaseSettingsState {
	dbConfiguration: DatabaseSettingState | null
	databaseIdForSettings: string | null
	name: string | null
	errorList: DatabaseSettingErrorState
}
