import slice from './databaseSettings.slice'
import { selectSlice as baseSelector } from '../base.selector'
import { RootState } from '../../../base.types'
import {
	DatabaseSettingState,
	DatabaseSettingsState,
	DatabaseSettingErrorState,
} from './databaseSettings.types'

export const selectSlice = (state: RootState): DatabaseSettingsState => {
	return baseSelector(state)[slice.name]
}

export const selectDatabaseIdForSettings = (
	state: RootState
): string | null => {
	return selectSlice(state).databaseIdForSettings
}

export const selectedDConfiguration = (
	state: RootState
): DatabaseSettingState | null => {
	return selectSlice(state).dbConfiguration
}

export const selectedErrorList = (
	state: RootState
): DatabaseSettingErrorState => {
	return selectSlice(state).errorList
}
