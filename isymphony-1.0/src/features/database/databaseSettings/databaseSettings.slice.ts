import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { isNil, pick } from 'lodash'
import {
	DatabaseSettingsState,
	DatabaseSettingState,
	DatabaseSettingErrorState,
	ErrorTypes,
} from './databaseSettings.types'
import * as asyncActions from './databaseSettings.asyncActions'
import { DatabaseSubReducersNames } from '../base.types'
import { Action } from '../../../common.types'
import { DatabaseVendorOptionsTypes } from '../../../utilities/apiEnumConstants'

const defaultErrorList: DatabaseSettingErrorState = {
	password: ErrorTypes.NONE,
	userName: ErrorTypes.NONE,
	port: ErrorTypes.NONE,
	host: ErrorTypes.NONE,
	platform: ErrorTypes.NONE,
	name: ErrorTypes.NONE,
	environmentType: ErrorTypes.NONE,
}

const defaultConfig: DatabaseSettingState = {
	password: '',
	userName: '',
	port: '',
	type: 'RDBMS',
	host: '',
	platform: DatabaseVendorOptionsTypes.POSTGRESQL,
	name: '',
	environmentType: 'Development',
	provider: 'Container',
}

const initialState: DatabaseSettingsState = {
	dbConfiguration: null,
	databaseIdForSettings: null,
	name: null,
	errorList: defaultErrorList,
}

const slice = createSlice<
	DatabaseSettingsState,
	SliceCaseReducers<DatabaseSettingsState>,
	DatabaseSubReducersNames.DATABASE_SETTINGS
>({
	name: DatabaseSubReducersNames.DATABASE_SETTINGS,
	initialState,
	reducers: {
		// synchronous actions
		setDatabaseIdForSettings(
			state: DatabaseSettingsState,
			action: Action<{ tableId: string; tableName: string }>
		) {
			state.databaseIdForSettings = action.payload.tableId
			state.name = action.payload.tableName
			state.dbConfiguration = null
			state.errorList = Object.assign({}, defaultErrorList)
		},
		updateConfigurationData(
			state: DatabaseSettingsState,
			action: Action<DatabaseSettingState>
		) {
			state.dbConfiguration = Object.assign(
				{},
				state.dbConfiguration,
				action.payload
			)
		},
		updateErrorList(
			state: DatabaseSettingsState,
			action: Action<DatabaseSettingErrorState>
		) {
			state.errorList = Object.assign({}, defaultErrorList, action.payload)
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(asyncActions.fetchDBConfiguration.fulfilled, (state, action) => {
				const list = [
					'password',
					'userName',
					'port',
					'host',
					'platform',
					'name',
					'environmentType',
					'type',
					'provider',
				]
				const configurations = action.payload.configurations
				if (!isNil(configurations.Development)) {
					state.dbConfiguration = pick(
						configurations.Development,
						list
					) as DatabaseSettingState
				} else {
					state.dbConfiguration = Object.assign({}, defaultConfig, {
						name: state.name,
					})
				}
			})
			.addCase(
				asyncActions.saveDbConfiguration.fulfilled,
				(state: DatabaseSettingsState, action) => {
					// if (action && action.payload) {
					state.databaseIdForSettings = null
					state.name = null
					state.dbConfiguration = null
					state.errorList = Object.assign({}, defaultErrorList)
					// }
				}
			)
	},
})

export default slice

export const { name, actions, reducer } = slice
