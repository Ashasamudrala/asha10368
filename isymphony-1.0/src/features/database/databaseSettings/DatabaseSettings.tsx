import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	selectDatabaseIdForSettings,
	selectedDConfiguration,
	selectedErrorList,
} from './databaseSettings.selectors'
import { actions } from './databaseSettings.slice'
import { isNil, isEmpty } from 'lodash'
import {
	fetchDBConfiguration,
	saveDbConfiguration,
} from './databaseSettings.asyncActions'
import Drawer from '@material-ui/core/Drawer'
import { Typography } from '@material-ui/core'
import { IsyInput } from '../../../widgets/IsyInput/IsyInput'
import { IsyDropDown } from '../../../widgets/IsyDropDown/IsyDropDown'
import {
	CONNECT_TO_DB,
	DATABASE_TRANSLATIONS,
	BUTTON_CONNECT,
	BUTTON_CANCEL,
	DATABASE_NAME,
	DATABASE_VENDOR,
	MYSQL,
	ORACLE,
	POSTGRE_SQL,
	SQL_SERVER,
	DB2,
	HOST,
	PORT,
	PASSWORD,
	USERNAME,
	PORT_EMPTY,
	PORT_INVALID,
	HOST_EMPTY,
	HOST_INVALID,
	PASSWORD_INVALID,
	USERNAME_INVALID,
	USERNAME_EMPTY,
	PASSWORD_EMPTY,
} from '../../../utilities/constants'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { IsyBusyIndicator } from '../../../widgets/IsyBusyIndicator/IsyBusyIndicator'
import { useTranslation } from 'react-i18next'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import {
	DatabaseSettingState,
	DatabaseSettingErrorState,
	ErrorTypes,
} from './databaseSettings.types'
import { DatabaseVendorOptionsTypes } from '../../../utilities/apiEnumConstants'
import './databaseSettings.scss'

const hostValidRegex = /^(?!(?:[0-9]+|\s+)$).[-a-zA-z]+$/
const portValidRegex = /^\d{4}$/
const dbPasswordRegex = /^[a-zA-Z0-9]{8,}$/

export interface DatabaseSettingProps {
	appId: string
	isEditable: boolean
}

export default function DatabaseSettings(props: DatabaseSettingProps) {
	const { appId, isEditable } = props
	const selectedDatabaseId = useSelector(selectDatabaseIdForSettings)
	const configData = useSelector(selectedDConfiguration)
	const errorList = useSelector(selectedErrorList)
	const dispatch = useDispatch()
	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	useEffect(() => {
		if (!isNil(selectedDatabaseId)) {
			dispatch(
				fetchDBConfiguration({
					appId,
					databaseId: selectedDatabaseId,
				})
			)
		}
	}, [selectedDatabaseId, appId, dispatch])

	const getDatabaseVendorOptions = [
		{
			id: DatabaseVendorOptionsTypes.MYSQL,
			name: t(MYSQL),
		},
		{
			id: DatabaseVendorOptionsTypes.ORACLE,
			name: t(ORACLE),
		},
		{
			id: DatabaseVendorOptionsTypes.POSTGRESQL,
			name: t(POSTGRE_SQL),
		},
		{
			id: DatabaseVendorOptionsTypes.SQLSERVER,
			name: t(SQL_SERVER),
		},
		{
			id: DatabaseVendorOptionsTypes.DB2,
			name: t(DB2),
		},
	]

	const getHostErrorLabel = () => {
		switch (errorList.host) {
			case ErrorTypes.EMPTY:
				return t(HOST_EMPTY)
			case ErrorTypes.INVALID:
				return t(HOST_INVALID)
			default:
				return ''
		}
	}

	const getPortErrorLabel = () => {
		switch (errorList.port) {
			case ErrorTypes.EMPTY:
				return t(PORT_EMPTY)
			case ErrorTypes.INVALID:
				return t(PORT_INVALID)
			default:
				return ''
		}
	}

	const getUserNameErrorLabel = () => {
		switch (errorList.userName) {
			case ErrorTypes.EMPTY:
				return t(USERNAME_EMPTY)
			case ErrorTypes.INVALID:
				return t(USERNAME_INVALID)
			default:
				return ''
		}
	}

	const getPasswordErrorLabel = () => {
		switch (errorList.password) {
			case ErrorTypes.EMPTY:
				return t(PASSWORD_EMPTY)
			case ErrorTypes.INVALID:
				return t(PASSWORD_INVALID)
			default:
				return ''
		}
	}

	const isFieldEmpty = (value: string) => {
		return isEmpty(value)
	}

	const isDataValid = (value: string, regex: RegExp) => {
		return regex.test(value)
	}

	const getValidationObject = (data: DatabaseSettingState | null) => {
		const errors: DatabaseSettingErrorState = {}
		if (isFieldEmpty(isNil(data) ? '' : data.host)) {
			errors.host = ErrorTypes.EMPTY
		} else if (!isDataValid(isNil(data) ? '' : data.host, hostValidRegex)) {
			errors.host = ErrorTypes.INVALID
		}

		if (isFieldEmpty(isNil(data) ? '' : data.port + '')) {
			errors.port = ErrorTypes.EMPTY
		} else if (
			!isDataValid(
				isNil(data) ? '' : ((data.port + '') as string),
				portValidRegex
			)
		) {
			errors.port = ErrorTypes.INVALID
		}

		if (isFieldEmpty(isNil(data) ? '' : data.userName)) {
			errors.userName = ErrorTypes.EMPTY
		}
		if (isFieldEmpty(isNil(data) ? '' : data.password)) {
			errors.password = ErrorTypes.EMPTY
		} else if (
			!isDataValid(isNil(data) ? '' : data.password, dbPasswordRegex)
		) {
			errors.password = ErrorTypes.INVALID
		}
		return errors
	}

	const handleClose = () => {
		dispatch(
			actions.setDatabaseIdForSettings({ tableId: null, tableName: null })
		)
	}

	const handleOkay = () => {
		const errors = getValidationObject(configData)
		if (Object.keys(errors).length > 0) {
			// has errors
			return dispatch(actions.updateErrorList(errors))
		} else {
			// no errors
			dispatch(actions.updateErrorList({}))
			if (!isNil(configData)) {
				dispatch(
					saveDbConfiguration({
						appId,
						databaseId: !isNil(selectedDatabaseId) ? selectedDatabaseId : '',
						configData: configData,
					})
				)
			}
		}
	}
	const handleDatabaseVendorChange = (value: string) => {
		dispatch(actions.updateConfigurationData({ platform: value }))
	}
	const handleHostChange = (value: string) => {
		dispatch(actions.updateConfigurationData({ host: value }))
	}
	const handlePortChange = (value: number | string) => {
		dispatch(actions.updateConfigurationData({ port: value }))
	}
	const handleUserNameChange = (value: string) => {
		dispatch(actions.updateConfigurationData({ userName: value }))
	}
	const handlePasswordChange = (value: string) => {
		dispatch(actions.updateConfigurationData({ password: value }))
	}

	const renderHeader = () => {
		return (
			<div className='header-section'>
				<span className='header-title'>{t(CONNECT_TO_DB)}</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
				/>
			</div>
		)
	}

	const renderDatabaseName = () => {
		return (
			<div className='relation-section'>
				<Typography>{t(DATABASE_NAME)}</Typography>
				<IsyInput<string>
					type='text'
					disabled={true}
					value={isNil(configData) ? '' : configData.name}
				/>
			</div>
		)
	}

	const renderDatabaseVendor = () => {
		return (
			<div className='relation-section'>
				<Typography>{t(DATABASE_VENDOR)}</Typography>
				<IsyDropDown<string>
					options={getDatabaseVendorOptions}
					value={isNil(configData) ? '' : configData.platform}
					className='input-base'
					onChange={handleDatabaseVendorChange}
					disabled={!isEditable}
				/>
			</div>
		)
	}

	const renderHost = () => {
		return (
			<div className='relation-section'>
				<Typography>
					{t(HOST)}
					<span className='form-label-required'>*</span>
				</Typography>
				<IsyInput<string>
					type='text'
					onChange={handleHostChange}
					value={isNil(configData) ? '' : configData.host}
					className={errorList.host === ErrorTypes.NONE ? '' : 'error-input'}
					disabled={!isEditable}
				/>
				<span className='error-label'>{getHostErrorLabel()}</span>
			</div>
		)
	}

	const renderPost = () => {
		return (
			<div className='relation-section'>
				<Typography>
					{t(PORT)}
					<span className='form-label-required'>*</span>
				</Typography>
				<IsyInput<number | string>
					type='number'
					onChange={handlePortChange}
					value={isNil(configData) ? '' : configData.port}
					className={errorList.port === ErrorTypes.NONE ? '' : 'error-input'}
					disabled={!isEditable}
				/>
				<span className='error-label'>{getPortErrorLabel()}</span>
			</div>
		)
	}

	const renderUsername = () => {
		return (
			<div className='relation-section'>
				<Typography>
					{t(USERNAME)}
					<span className='form-label-required'>*</span>
				</Typography>
				<IsyInput<string>
					type='text'
					onChange={handleUserNameChange}
					value={isNil(configData) ? '' : configData.userName}
					className={
						errorList.userName === ErrorTypes.NONE ? '' : 'error-input'
					}
					disabled={!isEditable}
				/>
				<span className='error-label'>{getUserNameErrorLabel()}</span>
			</div>
		)
	}

	const renderPassword = () => {
		return (
			<div className='relation-section'>
				<Typography>
					{t(PASSWORD)}
					<span className='form-label-required'>*</span>
				</Typography>
				<IsyInput<string>
					type='password'
					onChange={handlePasswordChange}
					value={isNil(configData) ? '' : configData.password}
					className={
						errorList.password === ErrorTypes.NONE ? '' : 'error-input'
					}
					disabled={!isEditable}
				/>
				<span className='error-label'>{getPasswordErrorLabel()}</span>
			</div>
		)
	}

	const renderForm = () => {
		if (isNil(configData)) {
			return ''
		}
		return (
			<>
				{renderDatabaseName()}
				{renderDatabaseVendor()}
				{renderHost()}
				{renderPost()}
				{renderUsername()}
				{renderPassword()}
			</>
		)
	}

	const renderMiddle = () => {
		return (
			<div className='middle-section'>
				<IsyBusyIndicator doNotShowNoText={true}>
					{renderForm()}
				</IsyBusyIndicator>
			</div>
		)
	}

	const renderBottom = () => {
		return (
			<div className='bottom-section'>
				<IsyButton onClick={handleClose} className='standard-btn'>
					{t(BUTTON_CANCEL)}
				</IsyButton>
				<IsyButton onClick={handleOkay} className='primary-btn'>
					{t(BUTTON_CONNECT)}
				</IsyButton>
			</div>
		)
	}

	return (
		<Drawer
			className={'database-settings'}
			open={!isNil(selectedDatabaseId)}
			anchor='right'
		>
			{renderHeader()}
			{renderMiddle()}
			{renderBottom()}
		</Drawer>
	)
}
