import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import i18n from 'i18next'
import {
	DATABASE_LOADING_MESSAGE_ERROR,
	DATABASE_CONFIG_SUCCESS,
} from '../../../utilities/constants'
import { UPDATE_DB_CONFIG } from '../../../utilities/apiEndpoints'
import { DatabaseSettingState } from './databaseSettings.types'

export const fetchDBConfiguration = createAsyncThunk(
	'databaseSettings/fetch',
	async (data: { databaseId: string; appId: string }, thunkArgs) =>
		await doAsync({
			url: UPDATE_DB_CONFIG(data.appId, data.databaseId),
			errorMessage: i18n.t(DATABASE_LOADING_MESSAGE_ERROR),
			...thunkArgs,
		})
)

export const saveDbConfiguration = createAsyncThunk(
	'databaseSettings/update',
	async (
		data: {
			appId: string
			databaseId: string
			configData: DatabaseSettingState
		},
		thunkArgs
	) =>
		await doAsync({
			url: UPDATE_DB_CONFIG(data.appId, data.databaseId),
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					configurations: {
						[data.configData.environmentType]: data.configData,
					},
				}),
			},
			successMessage: i18n.t(DATABASE_CONFIG_SUCCESS),
			noBusySpinner: true,
			...thunkArgs,
		})
)
