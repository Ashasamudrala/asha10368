import DatabaseSettings from './DatabaseSettings'
import slice from './databaseSettings.slice'

export const { name, reducer } = slice

// we export the component most likely to be desired by default
export default DatabaseSettings
