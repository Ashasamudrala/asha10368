import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import {
	GET_ALL_DATABASES,
	CREATE_DATABASE,
	SAVE_DATABASE,
	SAVE_DATABASE_CANVAS,
	DELETE_DATABASE,
	GET_ATTRIBUTE_TYPES,
	DATABASE_CHECK_IN_USER,
	DATABASE_CHECK_IN,
	DATABASE_CHECK_OUT,
} from '../../utilities/apiEndpoints'
import {
	getLockStatus,
	selectAllDatabases,
	selectAppId,
	selectDatabase,
	selectSelectedDatabase,
} from './database.selectors'
import {
	DATABASE_DELETE_MESSAGE,
	DATABASE_DELETE_MESSAGE_SUCCESS,
	DATABASE_SAVE_MESSAGE_SUCCESS,
	DATABASE_CREATE_MESSAGE_SUCCESS,
	START_TOUR,
	SKIP_TOUR,
	HELP_SKIP_DESCRIPTION_SECTION,
	OK_BUTTON_LABEL,
	HELP_DATABASE_DESCRIPTION,
	HELP_DATABASE_TITLE,
	COMMON_REQUEST_LOCK_GRANTED,
	BUTTON_OK,
	DATABASE_ACQUIRE_LOCK_MESSAGE,
	COMMON_LOCK_NOT_PRESENT_MESSAGE,
	COMMON_REQUEST_LOCK_REJECTED,
} from '../../utilities/constants'
import { showDialog, DialogTypes } from '../dialogs'
import i18n from 'i18next'
import { find, isNil, omit } from 'lodash'
import { RootState } from '../../base.types'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { actions } from '../../authAndPermissions/loginUserDetails.slice'
import {
	DatabaseLockStatus,
	DatabaseTablePositionProps,
} from './database.types'
import {
	selectGetDatabasePreferenceStatus,
	selectGetLoginUserId,
} from '../../authAndPermissions/loginUserDetails.selectors'

export const fetchAttributeTypes = createAsyncThunk(
	'database/dataType',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: GET_ATTRIBUTE_TYPES,
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const fetchAllDatabasesHelper = createAsyncThunk(
	'database/getAllHelper',
	async (appId: string, thunkArgs) =>
		await doAsync({
			url: GET_ALL_DATABASES(appId),
			...thunkArgs,
		})
)

export const fetchAllDatabases = createAsyncThunk(
	'database/getAll',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		if (isNil(appId)) {
			return
		}
		thunkArgs
			.dispatch(fetchAllDatabasesHelper(appId))
			.then(() =>
				thunkArgs.dispatch(
					checkLockStatus(
						selectGetLoginUserId(thunkArgs.getState() as RootState)
					)
				)
			)
	}
)

export const refreshLockStatus = createAsyncThunk(
	'database/refreshLockStatus',
	async (_: undefined, thunkArgs) => {
		thunkArgs.dispatch(
			checkLockStatus(selectGetLoginUserId(thunkArgs.getState() as RootState))
		)
	}
)

export const checkLockStatus = createAsyncThunk(
	'database/checkLockStatus',
	async (loggedInUserId: string | null, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const selectedDatabase = selectSelectedDatabase(
			thunkArgs.getState() as RootState
		)
		if (isNil(appId) || isNil(selectedDatabase) || isNil(loggedInUserId)) {
			return
		}
		return await doAsync({
			url: DATABASE_CHECK_IN_USER(
				appId,
				selectedDatabase.id,
				selectedDatabase.schemas[0].id
			),
			...thunkArgs,
		})
	}
)

export const acquireLock = createAsyncThunk(
	'database/acquireLock',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const databasePreferenceData = selectGetDatabasePreferenceStatus(
			thunkArgs.getState() as RootState
		)
		const selectedDatabase = selectSelectedDatabase(
			thunkArgs.getState() as RootState
		)
		if (isNil(appId) || isNil(selectedDatabase)) {
			return
		}
		return await doAsync({
			url: DATABASE_CHECK_IN(
				appId,
				selectedDatabase.id,
				selectedDatabase.schemas[0].id
			),
			httpMethod: 'put',
			...thunkArgs,
		}).then((data: any) => {
			if (data.response === 'ACQUIRED_LOCK') {
				const conformationCallBack = () => {
					thunkArgs.dispatch(fetchAllDatabasesHelper(appId)).then((data) => {
						thunkArgs.dispatch(loadDatabaseCanvas())
						return data
					})
				}
				if (
					databasePreferenceData &&
					databasePreferenceData.status !== HelpModuleStatus.INPROGRESS
				) {
					thunkArgs.dispatch(
						showDialog({
							type: DialogTypes.ALERT,
							onOkay: conformationCallBack,
							onCancel: conformationCallBack,
							title: i18n.t(COMMON_REQUEST_LOCK_GRANTED),
							data: {
								message: i18n.t(DATABASE_ACQUIRE_LOCK_MESSAGE, {
									name: selectedDatabase.name,
								}),
								okayButtonLabel: i18n.t(BUTTON_OK),
							},
						})
					)
				}
			} else {
				if (
					databasePreferenceData &&
					databasePreferenceData.status === HelpModuleStatus.INPROGRESS
				) {
					thunkArgs.dispatch(
						actions.updateDatabasePreferences({
							currentStep: 1,
						}) as any
					)
				} else {
					thunkArgs.dispatch(
						showDialog({
							type: DialogTypes.ALERT,
							title: i18n.t(COMMON_REQUEST_LOCK_REJECTED),
							data: {
								message: i18n.t(COMMON_LOCK_NOT_PRESENT_MESSAGE, {
									name: data.displayName,
								}),
								okayButtonLabel: i18n.t(BUTTON_OK),
							},
						})
					)
				}
			}
			return data
		})
	}
)

export const releaseLock = createAsyncThunk(
	'database/releaseLock',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const selectedDatabase = selectSelectedDatabase(
			thunkArgs.getState() as RootState
		)
		const lockStatus = getLockStatus(thunkArgs.getState() as RootState)
		if (
			isNil(appId) ||
			isNil(selectedDatabase) ||
			lockStatus !== DatabaseLockStatus.LOCK_ACQUIRED
		) {
			return
		}
		return await doAsync({
			url: DATABASE_CHECK_OUT(
				appId,
				selectedDatabase.id,
				selectedDatabase.schemas[0].id
			),
			httpMethod: 'put',
			...thunkArgs,
		})
	}
)

export const createDatabase = createAsyncThunk(
	'database/createDatabase',
	async (data: { appId: string; name: string }, thunkArgs) =>
		await doAsync({
			url: CREATE_DATABASE(data.appId),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name: data.name,
				}),
			},
			successMessage: i18n.t(DATABASE_CREATE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const loadDatabaseCanvas = createAsyncThunk(
	'database/loadCanvas',
	async (_: undefined, thunkArgs) => {
		const database = selectDatabase(thunkArgs.getState() as RootState)
		if (!isNil(database)) {
			return await doAsync({
				url: SAVE_DATABASE_CANVAS(
					database.applicationId,
					database.id,
					database.schemas[0].id
				),
				noBusySpinner: true,
				...thunkArgs,
			})
		}
	}
)

export const saveDatabaseCanvas = createAsyncThunk(
	'database/saveCanvas',
	async (_: undefined, thunkArgs) => {
		const database = selectDatabase(thunkArgs.getState() as RootState)
		const data: { [name: string]: DatabaseTablePositionProps } = {}
		if (!isNil(database)) {
			for (let i = 0, iLen = database.schemas[0].tables.length; i < iLen; i++) {
				const t = database.schemas[0].tables[i]
				data[t.name as string] = t.position
			}
			return await doAsync({
				url: SAVE_DATABASE_CANVAS(
					database.applicationId,
					database.id,
					database.schemas[0].id
				),
				httpMethod: 'put',
				httpConfig: {
					body: JSON.stringify({
						data: { tables: data },
					}),
				},
				noBusySpinner: true,
				...thunkArgs,
			})
		}
	}
)

export const saveDatabase = createAsyncThunk(
	'database/save',
	async (_: undefined, thunkArgs) => {
		const database = selectDatabase(thunkArgs.getState() as RootState)
		if (!isNil(database)) {
			thunkArgs.dispatch(saveDatabaseCanvas())
			return await doAsync({
				url: SAVE_DATABASE(
					database.applicationId,
					database.id,
					database.schemas[0].id
				),
				httpMethod: 'put',
				httpConfig: {
					body: JSON.stringify({
						name: database.schemas[0].name,
						displayName: database.schemas[0].displayName,
						description: database.schemas[0].description,
						tables: database.schemas[0].tables.map((t) => omit(t, 'position')),
					}),
				},
				successMessage: i18n.t(DATABASE_SAVE_MESSAGE_SUCCESS, {
					name: database.name,
				}),
				...thunkArgs,
			})
		}
	}
)
export const deleteDatabase = createAsyncThunk(
	'database/delete',
	async (
		data: { appId: string; databaseId: string; name: string },
		thunkArgs
	) =>
		await doAsync({
			url: DELETE_DATABASE(data.appId, data.databaseId),
			httpMethod: 'delete',
			successMessage: i18n.t(DATABASE_DELETE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const deleteDatabaseCanvas = createAsyncThunk(
	'database/deleteCanvas',
	async (
		data: { appId: string; databaseId: string; schemaId: string },
		thunkArgs
	) => {
		await doAsync({
			url: SAVE_DATABASE_CANVAS(data.appId, data.databaseId, data.schemaId),
			noBusySpinner: true,
			httpMethod: 'delete',
			...thunkArgs,
		})
	}
)

export const confirmDatabaseDelete = createAsyncThunk(
	'database/confirmDatabaseDelete',
	async (
		data: { appId: string; databaseId: string },
		{ dispatch, getState }
	) => {
		const dbs = selectAllDatabases(getState() as RootState)
		const db = find(dbs, (db) => db.id === data.databaseId)
		if (!isNil(db)) {
			const dbName = db.name
			const conformationCallBack = () => {
				dispatch(deleteDatabaseCanvas({ ...data, schemaId: db.schemas[0].id }))
				dispatch(deleteDatabase({ ...data, name: dbName })).then((response) => {
					if (response && response.payload) {
						dispatch(fetchAllDatabases())
					}
				})
			}
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					data: {
						message: i18n.t(DATABASE_DELETE_MESSAGE, { name: dbName }),
					},
				})
			)
		}
	}
)

export const showDatabaseHelpLandingPage = createAsyncThunk(
	'isyTour/showDatabaseLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateDatabasePreferences({
					landingPage: false,
				})
			)
			thunkArgs.dispatch(updatePreferences()).then(() =>
				thunkArgs.dispatch(
					actions.updateDatabasePreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			)
		}
		const handleCancel = () => {
			thunkArgs.dispatch(showDatabaseSkipHelpLandingPage())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				onCancel: handleCancel,
				data: {
					message: i18n.t(HELP_DATABASE_DESCRIPTION),
					okayButtonLabel: i18n.t(START_TOUR),
					cancelButtonLabel: i18n.t(SKIP_TOUR),
				},
				title: i18n.t(HELP_DATABASE_TITLE),
			})
		)
	}
)

export const showDatabaseSkipHelpLandingPage = createAsyncThunk(
	'isyTour/showDatabaseSkipHelpLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateDatabasePreferences({
					landingPage: false,
					status: HelpModuleStatus.COMPLETED,
				})
			)
			thunkArgs.dispatch(updatePreferences())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				data: {
					message: i18n.t(HELP_SKIP_DESCRIPTION_SECTION),
					okayButtonLabel: i18n.t(OK_BUTTON_LABEL),
				},
				title: i18n.t(HELP_DATABASE_TITLE),
			})
		)
	}
)
