import { isNil, isEqual, differenceWith, find, isEmpty } from 'lodash'
import { RootState } from '../../base.types'
import { selectSlice as baseSelector } from './base.selector'
import { DatabaseSubReducersNames } from './base.types'
import {
	AttributeTypeProps,
	AttributeTypesConstraintProps,
	DatabaseLockStatus,
	DatabaseProps,
	DatabaseState,
	DatabaseTableProps,
} from './database.types'

export const selectSlice = (state: RootState): DatabaseState =>
	baseSelector(state)[DatabaseSubReducersNames.DATABASE]

export const selectAppId = (state: RootState): string | null =>
	selectSlice(state).appId

export const selectAllDatabases = (state: RootState): DatabaseProps[] =>
	selectSlice(state).databases

export const selectDbAttributeTypes = (
	state: RootState
): AttributeTypeProps[] => selectSlice(state).attributeTypes

export const selectDbConstraintsByAttributeType = (
	state: RootState,
	type: string
): AttributeTypesConstraintProps[] => {
	const attrTypes = selectDbAttributeTypes(state)
	const attr = find(attrTypes, (a) => a.value === type)
	if (!isNil(attr)) {
		return attr.constraints
	}
	return []
}

export const selectSelectedDatabaseIndex = (
	state: RootState
): number | null => {
	return selectSlice(state).selectedDatabaseIndex
}

export const selectSelectedTableIndex = (state: RootState): number | null => {
	return selectSlice(state).selectedTableIndex
}

export const selectSelectedAttrIndex = (state: RootState): number | null => {
	return selectSlice(state).selectedAttributeIndex
}

export const selectIsDbTouched = (state: RootState): boolean => {
	return selectSlice(state).isDbTouched
}

export const selectSelectedTable = (
	state: RootState
): DatabaseTableProps | null => {
	const tables = selectDatabaseTables(state)
	const tableIndex = selectSelectedTableIndex(state)
	if (!isNil(tableIndex) && tables.length > tableIndex) {
		return tables[tableIndex]
	}
	return null
}

export const selectSelectedDatabase = (
	state: RootState
): DatabaseProps | null => {
	const allDbs = selectAllDatabases(state)
	const dbIndex = selectSelectedDatabaseIndex(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return allDbs[dbIndex]
	}
	return null
}

export const selectSelectedDatabaseId = (state: RootState): string | null => {
	const allDbs = selectAllDatabases(state)
	const dbIndex = selectSelectedDatabaseIndex(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return allDbs[dbIndex].id
	}
	return null
}

export const selectDatabaseTables = (
	state: RootState
): DatabaseTableProps[] => {
	const dbIndex = selectSelectedDatabaseIndex(state)
	const allDbs = selectAllDatabases(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return allDbs[dbIndex].schemas[0].tables
	}
	return []
}

export const selectIsDBModified = (state: RootState): boolean => {
	const dbIndex = selectSlice(state).selectedDatabaseIndex
	const allDbs = selectAllDatabases(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return !isEmpty(
			differenceWith(
				[selectSlice(state).databases[dbIndex]],
				[selectSlice(state).prevAllDatabase[dbIndex]],
				isEqual
			)
		)
	}
	return false
}

export const selectPreviousAllDatabase = (
	state: RootState
): DatabaseProps[] => {
	return selectSlice(state).prevAllDatabase
}

export const selectDatabase = (state: RootState): DatabaseProps | null => {
	const dbIndex = selectSelectedDatabaseIndex(state)
	const allDbs = selectAllDatabases(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return allDbs[dbIndex]
	}
	return null
}

export const getSearchTerm = (state: RootState): string => {
	return selectSlice(state).searchTerm
}

export const getLockStatus = (state: RootState): DatabaseLockStatus => {
	return selectSlice(state).lockStatus
}

export const getLockedBy = (state: RootState): string | null => {
	return selectSlice(state).lockedBy
}
