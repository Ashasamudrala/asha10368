import { isNil, find, pick, cloneDeep, isNull, isEmpty } from 'lodash'
import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import * as asyncActions from './database.asyncActions'
import newTable from '../../config/database/newTable.json'
import newAttribute from '../../config/database/newAttribute.json'
import { getUniqueName } from '../../utilities/utilities'
import {
	AttributeTypesConstraintProps,
	DatabaseLockStatus,
	DatabaseState,
	DatabaseTableAttributeProps,
	DatabaseTableProps,
	DatabaseTableReferenceProps,
	DatabaseUpdateTablePositionActionProps,
	DeleteTableActionProps,
	SetTableAttributeForeignKeyActionProps,
	UpdateTableActionProps,
	UpdateTableAttributeActionProps,
	UpdateTableAttributeConstraintsActionProps,
} from './database.types'
import { Action } from '../../common.types'
import { DatabaseSubReducersNames } from './base.types'
import i18n from 'i18next'
import { COLUMN_NAME, UNTITLED_TABLE } from '../../utilities/constants'

export const defaultConstraints = {
	min: 10,
	max: 20,
	pattern: '',
	length: 20,
	unique: false,
	required: true,
	primary: false,
}

const initialState: DatabaseState = {
	appId: null,
	databases: [],
	selectedDatabaseIndex: null,
	selectedTableIndex: null,
	selectedAttributeIndex: null,
	attributeTypes: [],
	prevAllDatabase: [],
	isDbTouched: false,
	searchTerm: '',
	lockStatus: DatabaseLockStatus.UN_KNOWN,
	lockedBy: null,
}

const slice = createSlice<
	DatabaseState,
	SliceCaseReducers<DatabaseState>,
	DatabaseSubReducersNames.DATABASE
>({
	name: DatabaseSubReducersNames.DATABASE,
	initialState,
	reducers: {
		// synchronous actions
		setAppId(state: DatabaseState, action: Action<string>) {
			state.appId = action.payload
		},
		updateSelectedDatabaseIndex(state: DatabaseState, action: Action<number>) {
			if (state.databases.length > action.payload) {
				state.selectedDatabaseIndex = action.payload
				state.selectedTableIndex = null
				state.selectedAttributeIndex = null
				state.isDbTouched = false
			}
		},
		updateSelectedTableIndexAndAttrIndex(
			state: DatabaseState,
			action: Action<{ tableIndex: number | null; attrIndex: number | null }>
		) {
			state.selectedTableIndex = action.payload.tableIndex
			state.selectedAttributeIndex = action.payload.attrIndex
		},
		updateTablePosition(
			state: DatabaseState,
			action: Action<DatabaseUpdateTablePositionActionProps>
		) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			tables[tableIndex] = Object.assign({}, tables[tableIndex], {
				position: { x: action.payload.x, y: action.payload.y },
			})

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		addTable(state: DatabaseState, action: Action<{ id: string }>) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			const names = tables.map((table) => table.name)
			const table: DatabaseTableProps = Object.assign({}, cloneDeep(newTable), {
				name: getUniqueName(names, i18n.t(UNTITLED_TABLE), '_'),
				id: action.payload.id,
				position: {
					x: (tables.length + 1) * 300,
					y: 20,
				},
			})
			const attribute: DatabaseTableAttributeProps = JSON.parse(
				JSON.stringify(newAttribute)
			)
			attribute.name = 'id'
			attribute.constraints.primary = true
			table.attributes.push(attribute)
			tables.push(table)

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		updateTable(state: DatabaseState, action: Action<UpdateTableActionProps>) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex
			const data = action.payload.table

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			if (!isNil(data.name)) {
				// on updating the table name updating the reference names
				const oldName = tables[tableIndex].name
				const newName = data.name
				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
						const dataToChange: Partial<DatabaseTableReferenceProps> = {}
						const attr = tables[i].attributes[j].reference
						if (!isNil(attr)) {
							if (attr.sourceModel === oldName) {
								dataToChange.sourceModel = newName
							}
							if (attr.targetModel === oldName) {
								dataToChange.targetModel = newName
							}
						}
						if (Object.keys(dataToChange).length > 0) {
							const attributes = [...tables[i].attributes]
							const reference = Object.assign(
								{},
								attributes[j].reference,
								dataToChange
							)
							attributes[j] = Object.assign({}, attributes[j], { reference })
							tables[i] = Object.assign({}, tables[i], {
								attributes,
							})
						}
					}
				}
			}

			tables[tableIndex] = Object.assign({}, tables[tableIndex], data)

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		deleteTable(state: DatabaseState, action: Action<{ tableIndex: number }>) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			// need to remove relative foreign keys
			const tableName = tables[tableIndex].name
			for (let i = 0, iLen = tables.length; i < iLen; i++) {
				for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
					const ref = tables[i].attributes[j].reference
					if (!isNil(ref) && ref.targetModel === tableName) {
						const attrs = [...tables[i].attributes]
						attrs[j] = Object.assign({}, attrs[j], { reference: null })
						tables[i] = Object.assign({}, tables[i], {
							attributes: attrs,
						})
					}
				}
			}

			tables.splice(tableIndex, 1)

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
			state.isDbTouched = true
			if (tableIndex === state.selectedTableIndex) {
				state.selectedTableIndex = null
				state.selectedAttributeIndex = null
			}
		},
		addAttribute(state: DatabaseState, action: Action<{ tableIndex: number }>) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			const attribute = JSON.parse(JSON.stringify(newAttribute))
			const names = attributes.map((attr) => attr.name)
			attribute.name = getUniqueName(names, i18n.t(COLUMN_NAME), '_')
			if (attributes.length === 0) {
				attribute.constraints.primary = true
			}
			attributes.push(attribute)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		updateTableAttribute(
			state: DatabaseState,
			action: Action<UpdateTableAttributeActionProps>
		) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex
			const data = action.payload.attr

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			if (tables[tableIndex].attributes[attrIndex].constraints.primary) {
				const tableName = tables[tableIndex].name
				const oldName = tables[tableIndex].attributes[attrIndex].name
				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
						const ref = tables[i].attributes[j].reference
						if (!isNil(ref)) {
							const dataToChange: Partial<DatabaseTableReferenceProps> = {}
							const attrDatToChange: Partial<DatabaseTableAttributeProps> = {}
							if (
								ref.sourceModel === tableName &&
								ref.sourceModelPrimaryKeyColumn === oldName &&
								!isNil(data.name)
							) {
								// on updating the attribute name updating the reference names
								dataToChange.sourceModelPrimaryKeyColumn = data.name
							}
							if (ref.targetModel === tableName && ref.column === oldName) {
								if (!isNil(data.name)) {
									// on updating the attribute name updating the reference names
									dataToChange.column = data.name
								}
								if (!isNil(data.type)) {
									attrDatToChange.type = data.type
								}
							}
							if (
								Object.keys(dataToChange).length > 0 ||
								Object.keys(attrDatToChange).length > 0
							) {
								const attrs = [...tables[i].attributes]
								const reference = Object.assign(
									{},
									attrs[j].reference,
									dataToChange
								)
								attrs[j] = Object.assign({}, attrs[j], {
									reference,
									...attrDatToChange,
								})
								tables[i] = Object.assign({}, tables[i], {
									attributes: attrs,
								})
							}
						}
					}
				}
			}

			const attributes = [...tables[tableIndex].attributes]
			if (!isNil(data.type)) {
				const attributeTypes = state.attributeTypes
				const attribute = find(attributeTypes, (a) => a.value === data.type)
				if (!isNil(attribute)) {
					const pathsAllowed = attribute.constraints.map(
						(c: AttributeTypesConstraintProps) => c.name
					)
					const filteredAdjConstraints = pick(
						attributes[attrIndex].constraints,
						pathsAllowed
					)
					const filteredDefaultConstraints = pick(
						defaultConstraints,
						pathsAllowed
					)
					data.constraints = Object.assign(
						{},
						filteredDefaultConstraints,
						filteredAdjConstraints
					)
				}
			}
			attributes[attrIndex] = Object.assign({}, attributes[attrIndex], data)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
			if (isNull(data.reference)) {
				state.selectedTableIndex = null
				state.selectedAttributeIndex = null
			}
		},
		deleteTableAttribute(
			state: DatabaseState,
			action: Action<DeleteTableActionProps>
		) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			attributes.splice(attrIndex, 1)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
			if (
				tableIndex === state.selectedTableIndex &&
				attrIndex === state.selectedAttributeIndex
			) {
				state.selectedTableIndex = null
				state.selectedAttributeIndex = null
			}
		},
		updateTableAttributeConstraints(
			state: DatabaseState,
			action: Action<UpdateTableAttributeConstraintsActionProps>
		) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex
			const data = action.payload.constraints

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			if (data.primary) {
				// if primary key is true need to remove other primary keys in table
				let primaryKeyName = null
				for (let i = 0, iLen = attributes.length; i < iLen; i++) {
					if (attributes[i].constraints.primary) {
						primaryKeyName = attributes[i].name
						attributes[i] = Object.assign({}, attributes[i], {
							constraints: Object.assign({}, attributes[i].constraints, {
								primary: false,
							}),
						})
					}
				}
				if (!isNil(primaryKeyName)) {
					// need to remove foreign relations for all the tables
					// need to update the primary
					const tableName = tables[tableIndex].name
					for (let i = 0, iLen = tables.length; i < iLen; i++) {
						for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
							const ref = tables[i].attributes[j].reference
							if (!isNil(ref)) {
								if (
									ref.sourceModel === tableName &&
									ref.sourceModelPrimaryKeyColumn === primaryKeyName
								) {
									// need to update the primary
									const attrs = [...tables[i].attributes]
									const reference = Object.assign({}, attrs[j].reference, {
										sourceModelPrimaryKeyColumn: attributes[attrIndex].name,
									})
									attrs[j] = Object.assign({}, attrs[j], { reference })
									tables[i] = Object.assign({}, tables[i], {
										attributes: attrs,
									})
								}
								if (
									ref.targetModel === tableName &&
									ref.column === primaryKeyName
								) {
									// need to remove foreign relations for all the tables
									const attrs = [...tables[i].attributes]
									attrs[j] = Object.assign({}, attrs[j], { reference: null })
									tables[i] = Object.assign({}, tables[i], {
										attributes: attrs,
									})
								}
							}
						}
					}
				}
			}

			// porting the changes
			const constraints = Object.assign(
				{},
				attributes[attrIndex].constraints,
				data
			)

			const attributeData: Partial<DatabaseTableAttributeProps> = {}
			// setting the reference to null if primary or unique key is trying to set.
			if (!isNil(data.unique) || !isNil(data.primary)) {
				attributeData.reference = null
			}

			attributes[attrIndex] = Object.assign(
				{},
				attributes[attrIndex],
				attributeData,
				{
					constraints,
				}
			)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		setTableAttributeForeignKey(
			state: DatabaseState,
			action: Action<SetTableAttributeForeignKeyActionProps>
		) {
			const index = state.selectedDatabaseIndex
			if (isNil(index)) {
				return
			}
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex

			const primaryKeyIndex = action.payload.primaryKeyIndex
			const targetTableIndex = action.payload.targetTableIndex
			const targetAttrIndex = action.payload.targetAttrIndex
			const relationShipType = action.payload.relationShipType
			const containment = action.payload.containment

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			const reference = {
				sourceModel: tables[tableIndex].name,
				sourceModelPrimaryKeyColumn:
					tables[tableIndex].attributes[primaryKeyIndex].name,
				targetModel: tables[targetTableIndex].name,
				column: tables[targetTableIndex].attributes[targetAttrIndex].name,
				relation: {
					association: relationShipType,
					containment: {
						type: containment,
					},
				},
			}

			const constraints = Object.assign({}, attributes[attrIndex].constraints, {
				unique: false,
				primary: false,
			})

			attributes[attrIndex] = Object.assign({}, attributes[attrIndex], {
				reference,
				constraints,
			})

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
			state.selectedTableIndex = tableIndex
			state.selectedAttributeIndex = attrIndex
		},
		updateDatabaseOnCancel(state: DatabaseState) {
			state.databases = state.prevAllDatabase
			state.selectedTableIndex = null
			state.selectedAttributeIndex = null
		},
		updateSearchTerm(state: DatabaseState, action: Action<string>) {
			state.searchTerm = action.payload
		},
		clearData: () => initialState,
	},

	extraReducers: (builder) => {
		// asynchronous actions
		builder.addCase(
			asyncActions.fetchAllDatabasesHelper.fulfilled,
			(state: DatabaseState, action) => {
				if (action && action.payload) {
					state.prevAllDatabase = action.payload.content
					state.databases = action.payload.content
					if (
						action.payload.content.length > 0 &&
						isNil(state.selectedDatabaseIndex)
					) {
						// on first load of all databases and setting database 0th index
						state.selectedDatabaseIndex = 0
						state.selectedTableIndex = null
						state.selectedAttributeIndex = null
					}
				}
			}
		)
		builder.addCase(
			asyncActions.fetchAttributeTypes.fulfilled,
			(state: DatabaseState, action) => {
				state.attributeTypes = action.payload.content
			}
		)
		builder.addCase(
			asyncActions.checkLockStatus.fulfilled,
			(state: DatabaseState, action) => {
				if (isEmpty(action.payload)) {
					state.lockStatus = DatabaseLockStatus.LOCK_PRESENT
				} else {
					const loggedInUserId = action.meta.arg
					if (loggedInUserId === action.payload.userId) {
						state.lockStatus = DatabaseLockStatus.LOCK_ACQUIRED
					} else {
						state.lockStatus = DatabaseLockStatus.LOCK_NOT_PRESENT
						state.lockedBy = action.payload.displayName
					}
				}
			}
		)
		builder.addCase(
			asyncActions.acquireLock.fulfilled,
			(state: DatabaseState, action) => {
				if (action.payload.response === 'ACQUIRED_LOCK') {
					state.lockStatus = DatabaseLockStatus.LOCK_ACQUIRED
					state.selectedTableIndex = null
					state.selectedAttributeIndex = null
				} else {
					state.lockStatus = DatabaseLockStatus.LOCK_NOT_PRESENT
					state.lockedBy = action.payload.displayName
				}
			}
		)
		builder.addCase(
			asyncActions.releaseLock.fulfilled,
			(state: DatabaseState, action) => {
				if (!isNil(action.payload)) {
					state.lockStatus = DatabaseLockStatus.LOCK_PRESENT
				}
			}
		)
		builder.addCase(
			asyncActions.saveDatabase.fulfilled,
			(state: DatabaseState, action) => {
				if (action && action.payload) {
					state.prevAllDatabase = state.databases
				}
			}
		)
		builder.addCase(
			asyncActions.deleteDatabase.fulfilled,
			(state: DatabaseState, action) => {
				if (action && isEmpty(action.payload)) {
					state.selectedTableIndex = null
					state.selectedAttributeIndex = null
				}
			}
		)
		builder.addCase(
			asyncActions.loadDatabaseCanvas.fulfilled,
			(state: DatabaseState, action) => {
				const data =
					action && action.payload ? action.payload.data.tables || {} : {}

				const index = state.selectedDatabaseIndex
				if (isNil(index)) {
					return
				}
				const databases = [...state.databases]
				const schemas = [...databases[index].schemas]
				const tables = [...schemas[0].tables]

				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					if (!isNil(data[tables[i].name])) {
						tables[i] = Object.assign({}, tables[i], {
							position: data[tables[i].name],
						})
					} else {
						tables[i] = Object.assign({}, tables[i], {
							position: {
								x: (i + 1) * 300,
								y: 20,
							},
						})
					}
				}

				schemas[0] = Object.assign({}, schemas[0], { tables })
				databases[index] = Object.assign({}, databases[index], { schemas })
				state.databases = databases
				state.prevAllDatabase = state.databases
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
