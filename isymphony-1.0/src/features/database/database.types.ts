import {
	DatabaseAttributeConstraintDataTypes,
	DatabaseContainmentType,
	DatabaseRelationShipType,
} from '../../utilities/apiEnumConstants'

export interface SetTableAttributeForeignKeyActionProps {
	tableIndex: number
	attrIndex: number
	primaryKeyIndex: number
	targetTableIndex: number
	targetAttrIndex: number
	relationShipType: DatabaseRelationShipType
	containment: DatabaseContainmentType
}

export interface UpdateTableAttributeConstraintsActionProps {
	tableIndex: number
	attrIndex: number
	constraints: Partial<DatabaseTableConstraintsProps>
}

export interface DeleteTableActionProps {
	tableIndex: number
	attrIndex: number
}

export interface UpdateTableAttributeActionProps {
	tableIndex: number
	attrIndex: number
	attr: Partial<DatabaseTableAttributeProps>
}

export interface UpdateTableActionProps {
	tableIndex: number
	table: Partial<DatabaseTableProps>
}

export interface DatabaseTablePositionProps {
	x: number
	y: number
}

export interface DatabaseUpdateTablePositionActionProps
	extends DatabaseTablePositionProps {
	tableIndex: number
}

export interface DatabaseTableConstraintsProps {
	filter?: string | null
	length?: number | null
	max?: number | null
	min?: number | null
	pattern?: string | null
	primary?: boolean
	required?: boolean
	unique?: boolean
}

export interface DatabaseTableReferenceRelationContainmentProps {
	type: DatabaseContainmentType
}

export interface DatabaseTableReferenceRelationProps {
	association: DatabaseRelationShipType
	containment: DatabaseTableReferenceRelationContainmentProps
}

export interface DatabaseTableReferenceProps {
	sourceModel: string
	sourceModelPrimaryKeyColumn: string
	targetModel: string
	column: string
	relation: DatabaseTableReferenceRelationProps
}

export interface DatabaseTableAttributeProps {
	name: string
	description: string
	type: string
	constraints: DatabaseTableConstraintsProps
	reference: DatabaseTableReferenceProps | null
}

export interface DatabaseTableProps {
	id: string
	name: string
	attributes: DatabaseTableAttributeProps[]
	position: DatabaseTablePositionProps
}

export interface DatabaseSchemasProps {
	id: string
	databaseId: string
	name: string
	displayName: string
	description: string
	tables: DatabaseTableProps[]
}

export interface DatabaseProps {
	id: string
	applicationId: string
	name: string
	schemas: DatabaseSchemasProps[]
}

export interface AttributeTypesConstraintProps {
	dataType: DatabaseAttributeConstraintDataTypes
	name: string
}

export interface AttributeTypeProps {
	id: string
	code: string
	value: string
	constraints: AttributeTypesConstraintProps[]
}

export enum DatabaseLockStatus {
	'LOCK_PRESENT',
	'LOCK_NOT_PRESENT',
	'LOCK_ACQUIRED',
	'UN_KNOWN',
}

export interface DatabaseState {
	appId: string | null
	databases: DatabaseProps[]
	selectedDatabaseIndex: number | null
	selectedTableIndex: number | null
	selectedAttributeIndex: number | null
	attributeTypes: AttributeTypeProps[]
	prevAllDatabase: DatabaseProps[]
	isDbTouched: boolean
	searchTerm: string
	lockStatus: DatabaseLockStatus
	lockedBy: string | null
}
