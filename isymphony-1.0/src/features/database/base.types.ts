import { DatabaseState } from './database.types'
import { DatabaseSettingsState } from './databaseSettings/databaseSettings.types'
import { TablesState } from './tables/tables.types'

export enum DatabaseSubReducersNames {
	DATABASE = 'database',
	TABLES = 'tables',
	DATABASE_SETTINGS = 'databaseSettings',
}

export interface DatabaseBaseState {
	[DatabaseSubReducersNames.DATABASE]: DatabaseState
	[DatabaseSubReducersNames.TABLES]: TablesState
	[DatabaseSubReducersNames.DATABASE_SETTINGS]: DatabaseSettingsState
}
