import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import { DatabaseIcon } from '../../icons/DatabaseIcon'
import { useParams } from 'react-router-dom'
import {
	DATABASE_TRANSLATIONS,
	DATABASE_TEXT,
	DATABASE_BUTTON,
} from '../../utilities/constants'
import './Database.scss'
import { useTranslation } from 'react-i18next'
import {
	confirmDatabaseDelete,
	releaseLock,
	showDatabaseHelpLandingPage,
} from './database.asyncActions'
import {
	confirmAccordionChange,
	confirmOnAddDatabase,
	createDatabaseAsync,
} from './tables/tables.asyncActions'
import {
	getLockStatus,
	selectAllDatabases,
	selectDatabaseTables,
	selectSelectedDatabaseIndex,
	selectSelectedTableIndex,
} from './database.selectors'
import { actions } from './database.slice'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import { actions as tableActions } from './tables/tables.slice'
import { DBAccordion } from './components/DBAccordion/DBAccordion'
import { Tables } from './tables/Tables'
import DatabaseSettings from './databaseSettings/DatabaseSettings'
import { IsySteps } from '../../widgets/IsySteps/IsySteps'
import { getAddDatabaseSteps } from '../../utilities/isyStepsConfig'
import { selectGetDatabasePreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { DatabaseLockStatus } from './database.types'
import { initDatabaseService } from './database.controller'
import { cloneDeep } from 'lodash'

export function Database() {
	const { appid } = useParams<{ appid: string }>()
	const databases = useSelector(selectAllDatabases)
	const selectedDatabaseIndex = useSelector(selectSelectedDatabaseIndex)
	const selectedTableIndex = useSelector(selectSelectedTableIndex)
	const databasePreferenceData = useSelector(selectGetDatabasePreferenceStatus)
	const tablesData = useSelector(selectDatabaseTables)
	const [isDatabaseLoaded, setIsDatabaseLoaded] = useState<boolean>(false)

	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const dispatch = useDispatch()
	const lockStatus = useSelector(getLockStatus)
	let steps = cloneDeep(getAddDatabaseSteps(lockStatus))

	useEffect(() => {
		dispatch(initDatabaseService(appid))
		return () => {
			dispatch(releaseLock())
			dispatch(actions.clearData(null))
			dispatch(tableActions.clearData(null))
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (databasePreferenceData && databasePreferenceData.landingPage === true) {
			dispatch(showDatabaseHelpLandingPage())
		}
	}, [databases, databasePreferenceData]) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		return () => {
			dispatch(actions.clearData(null))
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	const getSteps = () => {
		if (
			databasePreferenceData &&
			databasePreferenceData.status === HelpModuleStatus.INPROGRESS &&
			databases.length > 0 &&
			steps.length !== 15 &&
			!isDatabaseLoaded
		) {
			if (databases.length !== 0) {
				steps = steps.splice(2, steps.length - 1)
			}
			if (lockStatus === DatabaseLockStatus.LOCK_NOT_PRESENT) {
				steps = steps.splice(0, 2)
			}
		}
		return steps
	}

	const updateCurrentStep = (currentStep: number) => {
		dispatch(
			preferenceActions.updateDatabasePreferences({
				currentStep: currentStep,
			})
		)
	}

	const handleCreateDatabaseStep = (goToNext: any) => {
		if (
			goToNext &&
			goToNext.name &&
			databasePreferenceData.status === HelpModuleStatus.INPROGRESS
		) {
			setTimeout(() => {
				updateCurrentStep(2)
			}, 700)
			setIsDatabaseLoaded(true)
		}
	}

	const onAddDatabase = () => {
		if (databases.length !== 0) {
			dispatch(confirmOnAddDatabase({ appId: appid }))
		} else {
			if (databasePreferenceData.status === HelpModuleStatus.INPROGRESS) {
				setTimeout(() => {
					updateCurrentStep(1)
				})
			}
			dispatch(
				createDatabaseAsync({
					appId: appid,
					onCreateDatabaseStep: handleCreateDatabaseStep,
				}) as any
			)
		}
	}

	const handleBeforeChange = (currentStep: number) => {
		updateCurrentStep(currentStep)
		const tablesLength = tablesData.length - 1
		if (
			steps.length === 15 &&
			currentStep === 1 &&
			lockStatus === DatabaseLockStatus.LOCK_ACQUIRED
		) {
			updateCurrentStep(2)
			return false
		} else if (
			(steps.length > 15 && currentStep === 6) ||
			(steps.length <= 15 && currentStep === 4)
		) {
			dispatch(tableActions.setEditTableView(null))
		} else if (
			steps &&
			steps[currentStep].element === '.table-properties-panel'
		) {
			setSelectedTableIndexAndAttrIndex(tablesLength, null)
		} else if (
			steps &&
			steps[currentStep].element === '.key-icon .none_key_svg' &&
			tablesData[tablesLength].attributes.length <= 1
		) {
			dispatch(actions.addAttribute({ tableIndex: tablesLength }))
		}
		return true
	}

	const handleOnTableExit = (currentStep: number) => {
		let currentStepStatus
		if (steps && currentStep === steps.length - 1) {
			currentStepStatus = HelpModuleStatus.COMPLETED
		} else if (currentStep >= 0) {
			currentStepStatus = HelpModuleStatus.INCOMPLETE
		}
		if (currentStep !== -1) {
			dispatch(
				preferenceActions.updateDatabasePreferences({
					status: currentStepStatus,
					currentStep: currentStep,
				})
			)
			dispatch(updatePreferences())
		}
		setIsDatabaseLoaded(false)
	}

	const handleUpdateRelationShipDialogStep = (confirm: boolean) => {
		if (
			confirm &&
			databasePreferenceData.status === HelpModuleStatus.INPROGRESS
		) {
			dispatch(
				actions.updateSelectedTableIndexAndAttrIndex({
					tableIndex: tablesData.length - 1,
					attrIndex: 1,
				})
			)
			updateCurrentStep(steps.length - 3)
		} else {
			updateCurrentStep(databasePreferenceData.currentStep + 1)
		}
	}

	const handleUpdateCurrentStep = (
		currentStep: number,
		updateType?: string
	) => {
		if (steps.length > 15 && updateType === 'key') {
			updateCurrentStep(12)
		} else if (steps.length <= 15 && updateType === 'key') {
			updateCurrentStep(10)
		} else {
			updateCurrentStep(currentStep + 1)
		}
	}

	const setSelectedDatabaseIndex = (index: number) => {
		dispatch(confirmAccordionChange({ index: index, appid: appid }))
	}

	const setSelectedTableIndexAndAttrIndex = (
		tableIndex: number | null,
		attrIndex: number | null
	) => {
		dispatch(
			actions.updateSelectedTableIndexAndAttrIndex({ tableIndex, attrIndex })
		)
	}

	const setSelectedTableIntoView = (
		tableIndex: number,
		attrIndex: number | null
	) => {
		dispatch(
			actions.updateSelectedTableIndexAndAttrIndex({ tableIndex, attrIndex })
		)
		setTimeout(() => {
			dispatch(tableActions.setSelectedTableIndexForPosition(null))
		}, 10)
		dispatch(tableActions.setSelectedTableIndexForPosition(tableIndex))
	}

	const deleteDatabaseById = (databaseId: string) => {
		dispatch(
			confirmDatabaseDelete({
				appId: appid,
				databaseId: databaseId,
			})
		)
	}

	const renderNoDbScreen = () => {
		return (
			<div className='database-page'>
				<DatabaseIcon className='data-base-icon' />
				<div className='data-base-text'>{t(DATABASE_TEXT)}</div>
				<div>
					<IsyButton
						className='primary-btn add-new-database'
						onClick={onAddDatabase}
					>
						{t(DATABASE_BUTTON)}
					</IsyButton>
				</div>
			</div>
		)
	}

	const renderMainView = () => {
		return (
			<div className='db-container'>
				<div className='left'>
					<DBAccordion
						databases={databases}
						selectedDatabaseIndex={selectedDatabaseIndex}
						selectedTableIndex={selectedTableIndex}
						setSelectedDatabaseIndex={setSelectedDatabaseIndex}
						onSelectedTableIndexAndAttrIndex={setSelectedTableIntoView}
						onDeleteDatabase={deleteDatabaseById}
						onAddDatabase={onAddDatabase}
						isEditable={lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					/>
					<DatabaseSettings
						appId={appid}
						isEditable={lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					/>
				</div>
				<div className='right'>
					<Tables
						appId={appid}
						onSelectedTableIndexAndAttrIndex={setSelectedTableIndexAndAttrIndex}
						lockStatus={lockStatus}
						currentStep={databasePreferenceData.currentStep}
						isTableStepsEnabled={
							databasePreferenceData &&
							databasePreferenceData.status === HelpModuleStatus.INPROGRESS
						}
						onUpdateRelationShipDialogStep={handleUpdateRelationShipDialogStep}
						onUpdateCurrentStep={handleUpdateCurrentStep}
						isDatabaseLoaded={isDatabaseLoaded}
					/>
				</div>
			</div>
		)
	}

	const renderSteps = () => {
		const stepsData = getSteps()
		return (
			<IsySteps
				steps={stepsData}
				initialStep={databasePreferenceData.currentStep}
				isEnabled={
					databasePreferenceData &&
					databasePreferenceData.status === HelpModuleStatus.INPROGRESS
				}
				updateSteps={true}
				onBeforeChange={handleBeforeChange}
				onExit={handleOnTableExit}
			/>
		)
	}

	return (
		<>
			{databases.length === 0 ? renderNoDbScreen() : renderMainView()}
			{renderSteps()}
		</>
	)
}
