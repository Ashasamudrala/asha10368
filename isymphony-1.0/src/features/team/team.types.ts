export interface UserRoleProps {
	id: string
	name: string
	description: string
}

export enum TeamActiveTabs {
	ACTIVE,
	INVITE,
}

export interface PendingInviteMember {
	email: string
	roles: UserRoleProps[]
}

export interface PendingInviteProps {
	id: string
	member: PendingInviteMember
	checked: boolean
}

export interface ActiveMemberProps {
	id: string
	email: string
	firstName: string
	lastName: string
	roles: UserRoleProps[]
	profilePic: string | null
	checked: boolean
}

export interface TeamState {
	activeTab: TeamActiveTabs
	totalCount: number
	userRoles: UserRoleProps[]
	searchString: string
	list: Array<ActiveMemberProps | PendingInviteProps>
	loading: boolean
	recordsPerPage: number
	currentPage: number
}
