import slice from './team.slice'
import { selectSlice as baseSelector } from './base.selector'
import { RootState } from '../../base.types'
import {
	ActiveMemberProps,
	PendingInviteProps,
	TeamActiveTabs,
	TeamState,
	UserRoleProps,
} from './team.types'
import { filter } from 'lodash'

export const selectSlice = (state: RootState): TeamState =>
	baseSelector(state)[slice.name]

export const getTeamActiveTab = (state: RootState): TeamActiveTabs =>
	selectSlice(state).activeTab

export const getUserRoles = (state: RootState): UserRoleProps[] =>
	selectSlice(state).userRoles

export const getTeamSearchString = (state: RootState): string =>
	selectSlice(state).searchString

export const selectRecordsPerPage = (state: RootState): number => {
	return selectSlice(state).recordsPerPage
}

export const selectCurrentPage = (state: RootState): number => {
	return selectSlice(state).currentPage
}

export const selectTotalCount = (state: RootState): number => {
	return selectSlice(state).totalCount
}

export const getTeamLoading = (state: RootState): boolean =>
	selectSlice(state).loading

export const getTeamList = (
	state: RootState
): Array<ActiveMemberProps | PendingInviteProps> => selectSlice(state).list

export const getTeamSelectedIds = (state: RootState): string[] =>
	filter(getTeamList(state), (item) => item.checked).map((i) => i.id)

export const getTeamSelectedListLength = (state: RootState): number =>
	filter(getTeamList(state), (item) => item.checked).length
