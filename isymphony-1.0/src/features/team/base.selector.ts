import { RootState } from '../../base.types'
import { TeamsBaseState } from './base.types'

export const sliceName = 'teamBase'
export const selectSlice = (state: RootState): TeamsBaseState =>
	state[sliceName]
