import { combineReducers } from 'redux'
import * as selectors from './base.selector'
import * as team from './team.slice'
import { TeamsBaseState } from './base.types'
export const reducer = combineReducers<TeamsBaseState>({
	[team.name]: team.reducer,
})
export const name = selectors.sliceName
