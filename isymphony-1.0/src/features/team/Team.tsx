import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import { IsyTabItemProps, IsyTabs } from '../../widgets/IsyTabs/IsyTabs'
import './team.scss'
import {
	DELETE_ID_NUMBER,
	INVITE_MEMBER,
	TEAM_TRANSLATIONS,
	SEARCH_PENDING_PLACEHOLDER,
	SEARCH_ACTIVE_PLACEHOLDER,
	MY_TEAM,
	ACTIVE_MEMBERS,
	PENDING_INVITES,
	RECORDS_PER_PAGE,
} from '../../utilities/constants'
import {
	getTeamActiveTab,
	getTeamSearchString,
	getTeamSelectedListLength,
	selectCurrentPage,
	selectRecordsPerPage,
	selectTotalCount,
} from './team.selectors'
import { selectGetLogInUserInfo } from '../../authAndPermissions/loginUserDetails.selectors'
import ActiveMembers from './activeMembers/ActiveMembers'
import PendingInvites from './pendingInvites/PendingInvites'
import { actions } from './team.slice'
import {
	confirmOnDeleteRecords,
	fetchTeamsRecords,
	fetchUserRoles,
	inviteUsers,
} from './team.asyncActions'
import { TeamActiveTabs } from './team.types'
import { debounce } from 'lodash'
import { IsySearch } from '../../widgets/IsySearch/IsySearch'
import { IsyCan } from '../../widgets/IsyCan/IsyCan'
import { IsyPermissionTypes } from '../../authAndPermissions/loginUserDetails.types'
import { IsyPagination } from '../../widgets/IsyPagination/IsyPagination'

export default function Team() {
	const { t } = useTranslation(TEAM_TRANSLATIONS)
	const activeTab = useSelector(getTeamActiveTab)
	const searchString = useSelector(getTeamSearchString)
	const selectedIdsLength = useSelector(getTeamSelectedListLength)
	const loginDetails = useSelector(selectGetLogInUserInfo)
	const recordsPerPage = useSelector(selectRecordsPerPage)
	const currentPage = useSelector(selectCurrentPage)
	const totalCount = useSelector(selectTotalCount)
	const dispatch = useDispatch()

	const getTabsConfig = () => [
		{
			id: TeamActiveTabs.ACTIVE,
			header: t(ACTIVE_MEMBERS),
			content: <ActiveMembers />,
		},
		{
			id: TeamActiveTabs.INVITE,
			header: t(PENDING_INVITES),
			content: <PendingInvites />,
		},
	]

	useEffect(() => {
		loadRecords()
		if (
			loginDetails &&
			loginDetails.permissions &&
			loginDetails.permissions.includes(IsyPermissionTypes.UPDATE_ROLE)
		) {
			dispatch(fetchUserRoles())
		}
		return () => {
			dispatch(actions.clearData(null))
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	const loadRecords = () => {
		dispatch(actions.setLoading(true))
		dispatch(fetchTeamsRecords(false))
	}

	const handleInviteUser = () => {
		dispatch(inviteUsers())
	}

	const handleDeleteUsers = () => {
		dispatch(confirmOnDeleteRecords(null))
	}

	const handleSearchDebounce = debounce((value: string) => {
		dispatch(fetchTeamsRecords(false))
	}, 300)

	const handleSearch = (value: string) => {
		dispatch(actions.setSearchString(value))
		handleSearchDebounce(value)
	}

	const handleTabChange = (tab: IsyTabItemProps<TeamActiveTabs>) => {
		dispatch(actions.setActiveTab(tab.id))
		loadRecords()
	}

	const handleChangeRowsPerPage = (value: number) => {
		dispatch(actions.setRecordsPerPage(value))
		dispatch(fetchTeamsRecords(false))
	}

	const handleChangePage = (page: number) => {
		dispatch(actions.setCurrentPage(page))
		dispatch(fetchTeamsRecords(false))
	}

	const renderButton = () => {
		return (
			<IsyCan
				action={IsyPermissionTypes.CREATE_INVITATION}
				yes={() =>
					selectedIdsLength > 0 ? (
						<IsyButton className='primary-btn' onClick={handleDeleteUsers}>
							{t(DELETE_ID_NUMBER, { number: selectedIdsLength })}
						</IsyButton>
					) : (
						<IsyButton className='primary-btn' onClick={handleInviteUser}>
							{t(INVITE_MEMBER)}
						</IsyButton>
					)
				}
			/>
		)
	}

	const renderSearch = () => {
		return (
			<IsySearch
				value={searchString}
				onChange={handleSearch}
				onCancel={handleSearch}
				placeholder={
					activeTab === TeamActiveTabs.INVITE
						? t(SEARCH_PENDING_PLACEHOLDER)
						: t(SEARCH_ACTIVE_PLACEHOLDER)
				}
			/>
		)
	}

	const renderPagination = () => {
		return (
			<IsyPagination
				labelName={t(RECORDS_PER_PAGE)}
				totalCount={totalCount}
				currentPage={currentPage}
				recordsPerPage={recordsPerPage}
				rowsPerPageOptions={[12, 24, 36, 48, 60]}
				onChangeRowsPerPage={handleChangeRowsPerPage}
				onChangePage={handleChangePage}
				className='pagination-tool-bar'
			/>
		)
	}

	function renderTeams() {
		return (
			<div className='team-container'>
				<div className='title-holder'>
					<div className='team-title'>{t(MY_TEAM)}</div>
					<div className='button-component'>
						{renderButton()}
						{renderSearch()}
					</div>
				</div>
				<div className='header'>
					{renderPagination()}
					<div className='tabs-component'>
						<IsyTabs<TeamActiveTabs>
							tabs={getTabsConfig()}
							onTabChange={handleTabChange}
							activeTab={activeTab}
						/>
					</div>
				</div>
			</div>
		)
	}

	return <>{renderTeams()}</>
}
