import { TeamState } from './team.types'

export enum TeamsSubReducersNames {
	TEAM = 'team',
}

export interface TeamsBaseState {
	[TeamsSubReducersNames.TEAM]: TeamState
}
