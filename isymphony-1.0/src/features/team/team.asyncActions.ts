import { createAsyncThunk } from '@reduxjs/toolkit'
import { showDialog, DialogTypes } from '../dialogs'
import {
	getTeamActiveTab,
	getTeamSearchString,
	getTeamSelectedIds,
	getUserRoles,
	selectRecordsPerPage,
	selectCurrentPage,
	getTeamSelectedListLength,
} from './team.selectors'
import {
	DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE,
	CONFIRMATION_MESSAGE_DELETE,
	CONFIRMATION_MESSAGE_DELETE_USER,
	DELETE_SINGLE_USER_SUCCESS_MESSAGE,
	CONFIRMATION_MESSAGE_SELECT,
	ROLE_SUCCESS_MESSAGE,
	INVITE_SUCCESS_MESSAGE,
	BUTTON_CANCEL,
	BUTTON_REMOVE_MEMBERS,
} from '../../utilities/constants'
import i18n from 'i18next'
import {
	CREATE_INVITE_USERS,
	DELETE_INVITE_USERS,
	DELETE_USERS,
	GET_ACTIVE_MEMBERS,
	GET_ACTIVE_SEARCH_USERS,
	GET_PENDING_SEARCH_USERS,
	UPDATE_ACTIVE_USER,
	UPDATE_INVITE_USER,
	USER_ROLES,
} from '../../utilities/apiEndpoints'
import doAsync from '../../infrastructure/doAsync/doAsync'
import { RootState } from '../../base.types'
import { TeamActiveTabs } from './team.types'
import { isEmpty, isNil } from 'lodash'
import { InviteUsersReturnProps } from '../dialogs/inviteUsers/inviteUsers.types'

export const fetchUserRoles = createAsyncThunk(
	'team/loadUserRoles',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: USER_ROLES,
			noBusySpinner: true,
			errorMessage: 'Unable to load inviteUser. Please try again later.',
			...thunkArgs,
		})
)

export const fetchTeamsRecords = createAsyncThunk(
	'team/loadRecords',
	async (loadOnInvite: boolean, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const activeTab = getTeamActiveTab(state)
		const searchString = getTeamSearchString(state)
		const recordsPerPage = selectRecordsPerPage(state)
		const currentPage = selectCurrentPage(state)

		if (activeTab === TeamActiveTabs.ACTIVE && !loadOnInvite) {
			return await doAsync({
				url: GET_ACTIVE_SEARCH_USERS(searchString, currentPage, recordsPerPage),
				...thunkArgs,
			})
		} else if (activeTab === TeamActiveTabs.INVITE) {
			if (isEmpty(searchString)) {
				return await doAsync({
					url: GET_ACTIVE_MEMBERS,
					...thunkArgs,
				})
			} else {
				return await doAsync({
					url: GET_PENDING_SEARCH_USERS(
						searchString,
						currentPage,
						recordsPerPage
					),
					...thunkArgs,
				})
			}
		}
	}
)

export const deleteRecords = createAsyncThunk(
	'team/delete',
	async (data: { ids: string[]; msg: string }, thunkArgs) => {
		const activeTab = getTeamActiveTab(thunkArgs.getState() as RootState)
		if (activeTab === TeamActiveTabs.ACTIVE) {
			return await doAsync({
				url: DELETE_USERS(data.ids),
				httpMethod: 'delete',
				...thunkArgs,
				successMessage: data.msg,
				noBusySpinner: true,
			}).then(() => thunkArgs.dispatch(fetchTeamsRecords(false)))
		} else {
			return await doAsync({
				url: DELETE_INVITE_USERS(data.ids),
				httpMethod: 'delete',
				...thunkArgs,
				successMessage: data.msg,
				noBusySpinner: true,
			}).then(() => thunkArgs.dispatch(fetchTeamsRecords(false)))
		}
	}
)

export const confirmOnDeleteRecords = createAsyncThunk(
	'team/deleteConformation',
	async (data: { id: string; name: string } | null, thunkArgs) => {
		const selectedIdsLength = getTeamSelectedListLength(
			thunkArgs.getState() as RootState
		)
		const conformationCallBack = () => {
			let successMessage
			let selectedIds
			if (!isNil(data)) {
				selectedIds = [data.id]
				successMessage = i18n.t(DELETE_SINGLE_USER_SUCCESS_MESSAGE, {
					user: data.name,
				})
			} else {
				selectedIds = getTeamSelectedIds(thunkArgs.getState() as RootState)
				successMessage = i18n.t(DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE, {
					count: selectedIds.length,
				})
			}
			thunkArgs.dispatch(
				deleteRecords({ ids: selectedIds, msg: successMessage })
			)
		}

		let conformationMsg = ''
		if (!isNil(data)) {
			conformationMsg = i18n.t(CONFIRMATION_MESSAGE_DELETE_USER, {
				user: data.name,
			})
		} else {
			conformationMsg = i18n.t(CONFIRMATION_MESSAGE_DELETE, {
				number: selectedIdsLength,
			})
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: conformationCallBack,
				data: {
					message: conformationMsg,
					okayButtonLabel: i18n.t(BUTTON_REMOVE_MEMBERS),
					cancelButtonLabel: i18n.t(BUTTON_CANCEL),
				},
			})
		)
	}
)

export const changeRole = createAsyncThunk(
	'team/changeRole',
	async (data: { userId: string; roleId: string }, thunkArgs) => {
		const activeTab = getTeamActiveTab(thunkArgs.getState() as RootState)
		if (activeTab === TeamActiveTabs.ACTIVE) {
			return await doAsync({
				url: UPDATE_ACTIVE_USER(data.userId),
				httpMethod: 'put',
				httpConfig: {
					body: JSON.stringify([data.roleId]),
				},
				...thunkArgs,
				noBusySpinner: true,
				successMessage: i18n.t(ROLE_SUCCESS_MESSAGE),
			})
		} else {
			return await doAsync({
				url: UPDATE_INVITE_USER(data.userId),
				httpMethod: 'put',
				httpConfig: {
					body: JSON.stringify({
						invitationId: data.userId,
						roleId: data.roleId,
					}),
				},
				...thunkArgs,
			})
		}
	}
)

export const confirmRoleChange = createAsyncThunk(
	'team/roleChangeConformation',
	async (data: { userId: string; roleId: string }, thunkArgs) => {
		const conformationCallBack = () => {
			thunkArgs.dispatch(changeRole(data))
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: conformationCallBack,
				data: {
					message: i18n.t(CONFIRMATION_MESSAGE_SELECT),
				},
			})
		)
	}
)

const inviteUsersHelper = createAsyncThunk(
	'team/delete',
	async (data: InviteUsersReturnProps, thunkArgs) => {
		return await doAsync({
			url: CREATE_INVITE_USERS,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(data),
			},
			successMessage: i18n.t(INVITE_SUCCESS_MESSAGE, {
				length: data.members.length,
			}),
			noBusySpinner: true,
			...thunkArgs,
		}).then(() => thunkArgs.dispatch(fetchTeamsRecords(true)))
	}
)

export const inviteUsers = createAsyncThunk(
	'team/inviteUsers',
	async (_: undefined, thunkArgs) => {
		const conformationCallBack = (data: InviteUsersReturnProps) => {
			thunkArgs.dispatch(inviteUsersHelper(data))
		}
		const roles = getUserRoles(thunkArgs.getState() as RootState)
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.INVITE_USERS,
				onOkay: conformationCallBack,
				data: {
					roles,
				},
			})
		)
	}
)
