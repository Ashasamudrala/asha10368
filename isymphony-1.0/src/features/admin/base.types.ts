import { CoreBaseState } from './core/base.types'

export enum AdminSubReducersNames {
	CORE = 'core',
}

export interface AdminBaseState {
	[AdminSubReducersNames.CORE]: CoreBaseState
}
