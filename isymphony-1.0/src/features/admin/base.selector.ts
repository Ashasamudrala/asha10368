import { RootState } from '../../base.types'
import { AdminBaseState } from './base.types'

export const name = 'admin'
export const selectSlice = (state: RootState): AdminBaseState => {
	return state[name]
}
