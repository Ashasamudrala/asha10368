import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { actions } from './platformSettings/platformSettings.slice'
// import { CurrentStateType } from './platformSettings/platformSettings.types'
import {
	PLATFORM_TRANSLATIONS,
	// ADD_PLATFORM,
	NAME,
	DESCRIPTION,
	VERSIONS,
	NO_PLATFORMS_FOUND,
} from '../../../../utilities/constants'
// import { IsyButton } from '../../../../widgets/IsyButton/IsyButton'
import { PlatformSettings } from './platformSettings/PlatformSettings'
import { useTranslation } from 'react-i18next'
import {
	IsyGrid,
	IsyGridColumnTypes,
} from '../../../../widgets/IsyGrid/IsyGrid'

import './platform.scss'
import { selectorPlatforms } from '../core.selectors'
import { PlatformDataProps } from '../core.types'
import { IsyCan } from '../../../../widgets/IsyCan/IsyCan'
import { IsyPermissionTypes } from '../../../../authAndPermissions/loginUserDetails.types'

export default function Platform() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const platforms = useSelector(selectorPlatforms)
	const dispatch = useDispatch()

	const gridConfig = [
		{
			header: t(NAME),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'name',
		},
		{
			header: t(DESCRIPTION),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'description',
		},
		{
			header: t(VERSIONS),
			type: IsyGridColumnTypes.STRING_ARRAY,
			dataRef: 'versions',
			separator: ', ',
		},
	]

	const handleViewItem = (viewItem: PlatformDataProps) => {
		dispatch(actions.updatePlatformId(viewItem.id))
	}

	// const handleButtonOnClick = () => {
	// 	dispatch(actions.updateCurrentState(CurrentStateType.CREATE))
	// }

	const renderGrid = () => {
		return (
			<IsyGrid
				data={platforms}
				config={gridConfig}
				onRowClick={handleViewItem}
				emptyMessage={t(NO_PLATFORMS_FOUND)}
			/>
		)
	}

	return (
		<>
			<div className='platform-view'>
				<div className='add-platform-button-container'>
					<IsyCan
						action={IsyPermissionTypes.CREATE_PLATFORM}
						yes={
							() => null
							// <IsyButton
							// 	className={'primary-btn'}
							// 	onClick={handleButtonOnClick}
							// >
							// 	{t(ADD_PLATFORM)}
							// </IsyButton>
						}
					/>
				</div>
				{renderGrid()}
			</div>

			<IsyCan
				action={IsyPermissionTypes.VIEW_PLATFORM}
				yes={() => <PlatformSettings />}
			/>
		</>
	)
}
