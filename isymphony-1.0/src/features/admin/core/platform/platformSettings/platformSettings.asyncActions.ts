import { createAsyncThunk } from '@reduxjs/toolkit'
import {
	SAVE_PLATFORM,
	VIEW_PLATFORM_BY_ID,
} from '../../../../../utilities/apiEndpoints'
import doAsync from '../../../../../infrastructure/doAsync'
import {
	CurrentStateType,
	PlatformSettingsProps,
} from './platformSettings.types'
import { clone, keyBy, mapValues, omit } from 'lodash'
import { RootState } from '../../../../../base.types'
import {
	getCurrentState,
	getPlatformId,
	getPlatformSettingsData,
} from './platformSettings.selectors'
import {
	PLATFORM_SUCCESS_MESSAGE,
	EDIT_PLATFORM_SUCCESS_MESSAGE,
} from '../../../../../utilities/constants'
import i18n from 'i18next'
import { fetchAllPlatform } from '../../core.asyncActions'

// here it converts properties into ( key,value ) pairs and dispatch call to api
const convertMetdataToKeyValue = (platformData: PlatformSettingsProps) => {
	const clonedValues = clone(platformData)
	let processedMetadata = {}
	const { metadata } = clonedValues
	if (metadata && metadata.length >= 0) {
		processedMetadata = mapValues(keyBy(metadata, 'key'), 'value')
		processedMetadata = omit(processedMetadata, [''])
	}
	return processedMetadata
}

export const createPlatform = createAsyncThunk(
	'platformSettings/createPlatform',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		let platformData = getPlatformSettingsData(state)
		const name = platformData.name
		return await doAsync({
			url: SAVE_PLATFORM,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name: platformData.name,
					versions: platformData.versions,
					metadata: convertMetdataToKeyValue(platformData),
					description: platformData.description,
				}),
			},
			noBusySpinner: true,
			successMessage: i18n.t(EDIT_PLATFORM_SUCCESS_MESSAGE, { name }),
			...thunkArgs,
		})
	}
)

export const updatePlatform = createAsyncThunk(
	'platformSettings/updatePlatform',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		let platformData = getPlatformSettingsData(state)
		const platformId = getPlatformId(state)
		const name = platformData.name
		return await doAsync({
			url: `${VIEW_PLATFORM_BY_ID}/${platformId}`,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					name: name,
					versions: platformData.versions,
					metadata: convertMetdataToKeyValue(platformData),
					description: platformData.description,
				}),
			},
			successMessage: i18n.t(PLATFORM_SUCCESS_MESSAGE, { name }),
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const createAndUpdatePlatformSettings = createAsyncThunk(
	'platformSettings/savePlatform',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const currentState = getCurrentState(state)
		if (currentState === CurrentStateType.CREATE) {
			thunkArgs.dispatch(createPlatform()).then((response: any) => {
				if (response && response.payload) {
					thunkArgs.dispatch(fetchAllPlatform())
				}
			})
		} else {
			thunkArgs.dispatch(updatePlatform()).then((response: any) => {
				if (response && response.payload) {
					thunkArgs.dispatch(fetchAllPlatform())
				}
			})
		}
	}
)

export const fetchPlatformById = createAsyncThunk(
	'platformSettings/byId',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const platformId = getPlatformId(state)
		return await doAsync({
			url: `${VIEW_PLATFORM_BY_ID}/${platformId}`,
			...thunkArgs,
		})
	}
)
