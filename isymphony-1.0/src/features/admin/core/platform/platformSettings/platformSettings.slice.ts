import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../../../common.types'
import {
	CurrentStateType,
	PlatformSettingsMetadataType,
	PlatformSettingsErrorsList,
	PlatformSettingsProps,
	PlatformSettingsState,
} from './platformSettings.types'
import * as asyncActions from './platformSettings.asyncActions'
import { CoreSubReducersNames } from '../../base.types'

const defaultConfig = {
	name: '',
	description: '',
	versions: [],
	metadata: [],
}

const errorListConfig = {
	name: null,
	description: null,
	versions: null,
	metadata: null,
}

const initialState: PlatformSettingsState = {
	platformsData: defaultConfig,
	currentState: CurrentStateType.NONE,
	platformId: '',
	errorsList: errorListConfig,
}

const slice = createSlice<
	PlatformSettingsState,
	SliceCaseReducers<PlatformSettingsState>,
	CoreSubReducersNames.PLATFORM_SETTINGS
>({
	name: CoreSubReducersNames.PLATFORM_SETTINGS,
	initialState,
	reducers: {
		// synchronous actions
		updatePlatformSettingsData(
			state: PlatformSettingsState,
			action: Action<PlatformSettingsProps>
		) {
			state.platformsData = Object.assign(
				{},
				state.platformsData,
				action.payload
			)
		},
		updateCurrentState(
			state: PlatformSettingsState,
			action: Action<CurrentStateType>
		) {
			state.currentState = action.payload
		},
		updatePlatformId(state: PlatformSettingsState, action: Action<string>) {
			state.platformId = action.payload
			state.currentState = CurrentStateType.VIEW
		},
		updateErrorList(
			state: PlatformSettingsState,
			action: Action<PlatformSettingsErrorsList>
		) {
			state.errorsList = action.payload
		},
		clearData: (state: PlatformSettingsState) => initialState,
	},
	extraReducers: (builder) => {
		builder
			.addCase(asyncActions.createPlatform.fulfilled, (state, action) => {
				if (action && action.payload) {
					state.platformsData = defaultConfig
					state.currentState = CurrentStateType.NONE
					state.errorsList = errorListConfig
				}
			})
			.addCase(asyncActions.updatePlatform.fulfilled, (state, action) => {
				if (action && action.payload) {
					state.platformsData = defaultConfig
					state.platformId = ''
					state.currentState = CurrentStateType.NONE
					state.errorsList = errorListConfig
				}
			})
			.addCase(
				asyncActions.fetchPlatformById.fulfilled,
				(state: PlatformSettingsState, action) => {
					const data = action.payload as any
					// converts key value pair according to metadata component
					const metadata = data.metadata
					const convertedMetadata: PlatformSettingsMetadataType[] = []
					for (const [key, value] of Object.entries(metadata)) {
						let metadata = { key: key, value: value }
						convertedMetadata.push(metadata as PlatformSettingsMetadataType)
					}
					state.platformsData = {
						name: data.name,
						description: data.description,
						metadata: convertedMetadata,
						versions: data.versions,
					}
				}
			)
	},
})

export default slice

export const { name, actions, reducer } = slice
