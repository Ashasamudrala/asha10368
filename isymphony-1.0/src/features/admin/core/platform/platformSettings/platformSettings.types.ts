// import { AcceleratorsContentData } from './components/AcceleratorsLeftMenu/AcceleratorsLeftMenu'

export type PlatformSettingState = any

export interface PlatformSettingsMetadataType {
	key: string
	value: string
}

export enum CurrentStateType {
	NONE = 'NONE',
	CREATE = 'CREATE',
	EDIT = 'EDIT',
	VIEW = 'VIEW',
}

export interface PlatformSettingsProps {
	name: string
	description: string
	versions: string[]
	metadata: PlatformSettingsMetadataType[]
}

export interface PlatformSettingsErrorsList {
	name?: string | null
	description?: string | null
	versions?: string | null
	metadata?: string | null
}
export interface PlatformSettingsState {
	platformsData: PlatformSettingsProps
	currentState: CurrentStateType
	platformId: string
	errorsList: PlatformSettingsErrorsList
}
