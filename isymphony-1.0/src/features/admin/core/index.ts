import { combineReducers } from 'redux'
import * as baseSelector from './base.selector'
import { CoreBaseState } from './base.types'
import frameworkSettings from './framework/frameworkSettings/frameworkSettings.slice'
import platformSettings from './platform/platformSettings/platformSettings.slice'
import * as core from './core.slice'

export const reducer = combineReducers<CoreBaseState>({
	[core.name]: core.reducer,
	[frameworkSettings.name]: frameworkSettings.reducer,
	[platformSettings.name]: platformSettings.reducer,
})

export const { name } = baseSelector
export * from './base.types'
