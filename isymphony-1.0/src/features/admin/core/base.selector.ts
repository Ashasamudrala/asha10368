import { RootState } from '../../../base.types'
import { AdminSubReducersNames } from '../base.types'
import { CoreBaseState } from './base.types'
import { selectSlice as baseSelector } from '../base.selector'

export const name = AdminSubReducersNames.CORE
export const selectSlice = (state: RootState): CoreBaseState => {
	return baseSelector(state)[name]
}
