export interface FrameworkSettingsMetadataProps {
	key: string
	value: string
}

export interface FrameworkSettingsVersionModulesProps {
	name: string
	description: string
	version: string
	metadata: FrameworkSettingsMetadataProps[]
}

export interface FrameworkSettingsVersionProps {
	versionName: string
	supportedPlatformVersions: string[]
	modules: FrameworkSettingsVersionModulesProps[]
	metadata: FrameworkSettingsMetadataProps[]
}

export interface FrameworkChannelConfiguration {
	channelName: string
}

export interface FrameworkSettingsDetailsProps {
	metadata: FrameworkSettingsMetadataProps[]
	name: string
	description: string
	channelConfiguration: FrameworkChannelConfiguration
	versions: FrameworkSettingsVersionProps[]
}

export interface FrameworkSettingsDetailsErrors {
	name?: string
	description?: string
	channelConfiguration?: FrameworkChannelConfiguration
	versions?: Partial<FrameworkSettingsVersionProps[]>
}

export interface FrameworkSettingsState {
	frameworkDetails: FrameworkSettingsDetailsProps
	frameworkId: string
	platformId: string
	isEdit: boolean
	errorList: FrameworkSettingsDetailsErrors
	supportedVersions: string[]
}
