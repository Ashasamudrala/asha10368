import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../../../common.types'
import {
	FrameworkSettingsState,
	FrameworkSettingsDetailsProps,
	FrameworkSettingsDetailsErrors,
} from './frameworkSettings.types'
import { CoreSubReducersNames } from '../../base.types'
import { FrameworkProps } from '../../core.types'
import { getFrameworkSettingsFromFrameworkPayload } from './frameworkSettings.utilities'
import * as asyncActions from './frameworkSettings.asyncActions'

const defaultConfig: FrameworkSettingsDetailsProps = {
	metadata: [],
	name: '',
	description: '',
	channelConfiguration: {
		channelName: '',
	},
	versions: [
		{
			metadata: [],
			versionName: '',
			supportedPlatformVersions: [],
			modules: [],
		},
	],
}

const initialState: FrameworkSettingsState = {
	frameworkDetails: defaultConfig,
	frameworkId: '',
	platformId: '',
	errorList: {},
	supportedVersions: [],
	isEdit: false,
}

const slice = createSlice<
	FrameworkSettingsState,
	SliceCaseReducers<FrameworkSettingsState>,
	CoreSubReducersNames.FRAMEWORK_SETTINGS
>({
	name: CoreSubReducersNames.FRAMEWORK_SETTINGS,
	initialState,
	reducers: {
		// synchronous actions
		updateFrameworkData(
			state: FrameworkSettingsState,
			action: Action<FrameworkProps>
		) {
			state.frameworkId = action.payload.id
			state.platformId = action.payload.platformId
			state.frameworkDetails = getFrameworkSettingsFromFrameworkPayload(
				action.payload
			)
		},
		setIsEdit(state: FrameworkSettingsState, action: Action<boolean>) {
			state.isEdit = action.payload
		},
		updatePlatformId(state: FrameworkSettingsState, action: Action<string>) {
			state.platformId = action.payload
			state.isEdit = true
		},
		updateFrameworkId(state: FrameworkSettingsState, action: Action<string>) {
			state.frameworkId = action.payload
		},
		updateFrameworkApiData(
			state: FrameworkSettingsState,
			action: Action<FrameworkSettingsDetailsProps>
		) {
			state.frameworkDetails = action.payload
		},
		updateSupportedVersions(
			state: FrameworkSettingsState,
			action: Action<string[]>
		) {
			state.supportedVersions = action.payload
		},
		updateErrorList(
			state: FrameworkSettingsState,
			action: Action<FrameworkSettingsDetailsErrors>
		) {
			state.errorList = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(asyncActions.saveFramework.fulfilled, (state, action) => {
			if (action && action.payload) {
				state.frameworkDetails = defaultConfig
				state.frameworkId = ''
				state.platformId = ''
				state.errorList = {}
				state.supportedVersions = []
				state.isEdit = false
			}
		})
	},
})

export default slice

export const { name, actions, reducer } = slice
