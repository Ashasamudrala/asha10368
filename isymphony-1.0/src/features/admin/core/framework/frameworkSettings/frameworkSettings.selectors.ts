import { selectSlice as baseSelector } from '../../base.selector'
import { RootState } from '../../../../../base.types'
import {
	FrameworkSettingsState,
	FrameworkSettingsDetailsProps,
	FrameworkSettingsDetailsErrors,
} from './frameworkSettings.types'
import { CoreSubReducersNames } from '../../base.types'

export const selectSlice = (state: RootState): FrameworkSettingsState => {
	return baseSelector(state)[CoreSubReducersNames.FRAMEWORK_SETTINGS]
}

export const getFrameworkDetails = (
	state: RootState
): FrameworkSettingsDetailsProps => {
	return selectSlice(state).frameworkDetails
}

export const getPlatformId = (state: RootState): string => {
	return selectSlice(state).platformId
}

export const getErrorList = (
	state: RootState
): FrameworkSettingsDetailsErrors => {
	return selectSlice(state).errorList
}

export const getFrameworkId = (state: RootState): string => {
	return selectSlice(state).frameworkId
}

export const getSupportedVersions = (state: RootState): string[] => {
	return selectSlice(state).supportedVersions
}

export const getIsEdit = (state: RootState): boolean => {
	return selectSlice(state).isEdit
}
