import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../../../infrastructure/doAsync'
import { RootState } from '../../../../../base.types'
import {
	getFrameworkDetails,
	getFrameworkId,
	getPlatformId,
} from './frameworkSettings.selectors'
import {
	FrameworkSettingsDetailsProps,
	FrameworkSettingsVersionModulesProps,
	FrameworkSettingsVersionProps,
} from './frameworkSettings.types'
import {
	CREATE_FRAMEWORK_BY_ID,
	UPDATE_FRAMEWORK_BY_ID,
} from '../../../../../utilities/apiEndpoints'
import {
	EDIT_FRAMEWORK_SUCCESS_MESSAGE,
	ADD_FRAMEWORK_SUCCESS_MESSAGE,
} from '../../../../../utilities/constants'
import i18n from 'i18next'
import { keyBy, mapValues, omit, set } from 'lodash'

const convertMetadataToKeyValue = (
	frameworkData: FrameworkSettingsDetailsProps
) => {
	// convertMetadataToKeyValue
	const clonedFormValues = JSON.parse(JSON.stringify(frameworkData))
	const { metadata, versions } = clonedFormValues
	let formData = clonedFormValues
	let processedMetadata = {}

	if (metadata && metadata.length >= 0) {
		processedMetadata = mapValues(keyBy(metadata, 'key'), 'value')
		processedMetadata = omit(processedMetadata, [''])
	}
	if (versions && versions.length >= 0) {
		versions.forEach(
			(version: FrameworkSettingsVersionProps, index: number) => {
				// Retrofit the version metadata.
				const versionMetadata = version.metadata
				if (versionMetadata && versionMetadata.length >= 0) {
					set(
						formData,
						`versions.${index}.metadata`,
						omit(mapValues(keyBy(versionMetadata, 'key'), 'value'), [''])
					)
				}

				// Module metadata
				const { modules } = version
				if (modules && modules.length >= 0) {
					modules.forEach(
						(module: FrameworkSettingsVersionModulesProps, mid: number) => {
							const moduleMetadata = module.metadata
							if (moduleMetadata && moduleMetadata.length >= 0) {
								set(
									formData,
									`versions.${index}.modules.${mid}.metadata`,
									omit(mapValues(keyBy(moduleMetadata, 'key'), 'value'), [''])
								)
							}
						}
					)
				}
			}
		)
	}
	return { ...formData, metadata: { ...processedMetadata } }
}

export const saveFramework = createAsyncThunk(
	'frameworkSettings/saveFramework',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		let frameworkData = getFrameworkDetails(state)
		frameworkData = convertMetadataToKeyValue(frameworkData)
		const frameworkId = getFrameworkId(state)
		const platformId = getPlatformId(state)
		const JsonObject = JSON.stringify({
			metadata: frameworkData.metadata,
			name: frameworkData.name,
			description: frameworkData.description,
			channelConfiguration: frameworkData.channelConfiguration,
			versions: frameworkData.versions,
			platformId: platformId,
		})
		let httpMethod: string = 'post'
		let url: string = CREATE_FRAMEWORK_BY_ID(platformId)
		let successMessage: string = i18n.t(ADD_FRAMEWORK_SUCCESS_MESSAGE, {
			name: frameworkData.name,
		})

		if (frameworkId !== '') {
			httpMethod = 'put'
			url = UPDATE_FRAMEWORK_BY_ID(platformId, frameworkId)
			successMessage = i18n.t(EDIT_FRAMEWORK_SUCCESS_MESSAGE, {
				name: frameworkData.name,
			})
		}
		return await doAsync({
			url: url,
			httpMethod: httpMethod,
			httpConfig: {
				body: JsonObject,
			},
			successMessage: successMessage,
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)
