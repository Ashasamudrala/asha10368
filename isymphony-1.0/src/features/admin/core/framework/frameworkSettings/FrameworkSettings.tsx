import React from 'react'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { useDispatch, useSelector } from 'react-redux'
import {
	getFrameworkDetails,
	getErrorList,
	getFrameworkId,
	getPlatformId,
	getSupportedVersions,
	getIsEdit,
} from './frameworkSettings.selectors'
import { actions } from './frameworkSettings.slice'
import {
	FrameworkSettingsDetailsProps,
	FrameworkSettingsDetailsErrors,
	FrameworkSettingsVersionModulesProps,
	FrameworkSettingsVersionProps,
} from './frameworkSettings.types'
import {
	FRAMEWORK_NAME_PLACEHOLDER,
	FRAMEWORK_NAME,
	FRAMEWORK_DESCRIPTION,
	FRAMEWORK_DESCRIPTION_PLACEHOLDER,
	FRAMEWORK_VERSIONS,
	FRAMEWORK_DETAILS,
	FRAMEWORK_TRANSLATIONS,
	FRAMEWORK_ADD_VERSION,
	FRAMEWORK_VERSION_NAME,
	FRAMEWORK_VERSION_PLACEHOLDER,
	FRAMEWORK_SUPPORTED_VERSIONS,
	FRAMEWORK_SUPPORTED_PLACEHOLDER,
	FRAMEWORK_VERSION_METADATA,
	FRAMEWORK_VERSION_METADATA_LABEL,
	FRAMEWORK_ADD_MODULES,
	FRAMEWORK_MODULE_NAME,
	FRAMEWORK_MODULE_PLACEHOLDER,
	FRAMEWORK_MODULE_VERSION_NAME,
	FRAMEWORK_MODULE_VERSION_PLACEHOLDER,
	FRAMEWORK_MODULE_DESCRIPTION_PLACEHOLDER,
	FRAMEWORK_MODULE_DESCRIPTION,
	FRAMEWORK_METADATA,
	ADD_FRAMEWORK_METADATA_BUTTON_NAME,
	FRAMEWORK_CHANNEL_NAME,
	FRAMEWORK_CHANNEL_NAME_PLACEHOLDER,
	FRAMEWORK_CHANNEL_CONFIGURATION,
	FRAMEWORK_MODULE_LABEL,
	ADD_FRAMEWORK_METADATA_KEY_PLACEHOLDER,
	ADD_FRAMEWORK_METADATA_VALUE_PLACEHOLDER,
	BUTTON_CANCEL,
	BUTTON_SAVE,
	ADD_FRAMEWORK,
	VALIDATE_NAME,
	FRAMEWORK_VALIDATE_DESCRIPTION_PATTERN,
	VALIDATE_NAME_RANGE,
	VALIDATE_NAME_PATTERN,
	VALIDATE_CHANNEL_NAME,
	VALIDATE_CHANNEL_PATTERN,
	FRAMEWORK_VALIDATE_VERSION,
	VALIDATE_VERSION_RANGE,
	VALIDATE_MODULE_NAME_RANGE,
	VALIDATE_PLATFORM_VERSIONS,
	VALIDATE_VERSION_NAME,
	FRAMEWORK_VALIDATE_VERSION_PATTERN,
	// EDIT,
} from '../../../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import { IsyButton } from '../../../../../widgets/IsyButton/IsyButton'
import { Drawer } from '@material-ui/core'
import { saveFramework } from './frameworkSettings.asyncActions'
import './frameworkSettings.scss'
import { cloneDeep, get, isEmpty, set } from 'lodash'
import { makeStyles } from '@material-ui/core/styles'
import { IsyBusyIndicator } from '../../../../../widgets/IsyBusyIndicator/IsyBusyIndicator'
import { fetchAllPlatform } from '../../core.asyncActions'
// import { IsyCan } from '../../../../../widgets/IsyCan/IsyCan'
// import { IsyPermissionTypes } from '../../../../../authAndPermissions/loginUserDetails.types'

const useStyles = makeStyles({
	paper: {
		width: 850,
		boxSizing: 'border-box',
		backgroundColor: 'white',
		border: '1px solid #dedede',
	},
})

const nameValidRegex = /^[^\s]+(\s+[^\s]+)*$/

export function FrameworkSettings() {
	const frameworkData = useSelector(getFrameworkDetails)
	const errorList = useSelector(getErrorList)
	const frameworkId = useSelector(getFrameworkId)
	const platformId = useSelector(getPlatformId)
	const supportedVersions = useSelector(getSupportedVersions)
	const isEdit = useSelector(getIsEdit)
	const { t } = useTranslation(FRAMEWORK_TRANSLATIONS)
	const classes = useStyles()

	const dispatch = useDispatch()

	const getFormBuilderConfig = () => {
		const config: IsyFormBuilderFormItemProps[] = [
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				title: t(FRAMEWORK_DETAILS),
				forms: [
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(FRAMEWORK_NAME),
						dataRef: 'name',
						placeholder: t(FRAMEWORK_NAME_PLACEHOLDER),
						autoFocus: true,
						isRequired: true,
					},
					{
						type: IsyFormBuilderFormTypes.TEXT_AREA,
						title: t(FRAMEWORK_DESCRIPTION),
						dataRef: 'description',
						placeholder: t(FRAMEWORK_DESCRIPTION_PLACEHOLDER),
					},
					{
						type: IsyFormBuilderFormTypes.ARRAY_SECTION,
						title: t(FRAMEWORK_VERSIONS),
						dataRef: 'versions',
						addLabel: t(FRAMEWORK_ADD_VERSION),
						multiLabel: true,
						minimum: 1,
						onDelete: handleDelete,
						className: 'version-container',
						addDefault: {
							metadata: [],
							versionName: '',
							supportedPlatformVersions: [],
							modules: [],
						},
						forms: [
							{
								type: IsyFormBuilderFormTypes.STRING,
								title: t(FRAMEWORK_VERSION_NAME),
								dataRef: 'versionName',
								autoFocus: frameworkData.versions.length > 1 ? true : false,
								placeholder: t(FRAMEWORK_VERSION_PLACEHOLDER),
								isRequired: true,
								className: 'version-name',
							},
							{
								type: IsyFormBuilderFormTypes.CHIP_SELECT,
								title: t(FRAMEWORK_SUPPORTED_VERSIONS),
								dataRef: 'supportedPlatformVersions',
								placeholder: t(FRAMEWORK_SUPPORTED_PLACEHOLDER),
								options: supportedVersions,
								isRequired: true,
								className: 'supported-versions',
							},
							{
								type: IsyFormBuilderFormTypes.GRID_SECTION,
								title: t(FRAMEWORK_VERSION_METADATA_LABEL),
								addLabel: t(FRAMEWORK_VERSION_METADATA),
								dataRef: 'metadata',
								className: 'version-metadata-container',
								addDefault: { key: '', value: '' },
								forms: [
									{
										type: IsyFormBuilderFormTypes.STRING,
										title: 'Key',
										dataRef: 'key',
										autoFocus: true,
										placeholder: t(ADD_FRAMEWORK_METADATA_KEY_PLACEHOLDER),
									},
									{
										type: IsyFormBuilderFormTypes.STRING,
										title: 'Value',
										dataRef: 'value',
										placeholder: t(ADD_FRAMEWORK_METADATA_VALUE_PLACEHOLDER),
									},
								],
							},
							{
								type: IsyFormBuilderFormTypes.ARRAY_SECTION,
								title: t(FRAMEWORK_MODULE_LABEL),
								addLabel: t(FRAMEWORK_ADD_MODULES),
								addDefault: { name: '', version: '', description: '' },
								dataRef: 'modules',
								onDelete: handleDelete,
								className: 'modules-container',
								forms: [
									{
										type: IsyFormBuilderFormTypes.STRING,
										title: t(FRAMEWORK_MODULE_NAME),
										dataRef: 'name',
										autoFocus: true,
										className: 'module-name',
										placeholder: t(FRAMEWORK_MODULE_PLACEHOLDER),
										isRequired: true,
									},
									{
										type: IsyFormBuilderFormTypes.STRING,
										title: t(FRAMEWORK_MODULE_VERSION_NAME),
										dataRef: 'version',
										className: 'module-version-name',
										placeholder: t(FRAMEWORK_MODULE_VERSION_PLACEHOLDER),
										isRequired: true,
									},
									{
										type: IsyFormBuilderFormTypes.TEXT_AREA,
										title: t(FRAMEWORK_MODULE_DESCRIPTION),
										placeholder: t(FRAMEWORK_MODULE_DESCRIPTION_PLACEHOLDER),
										dataRef: 'description',
										className: 'module-description',
									},
									{
										type: IsyFormBuilderFormTypes.GRID_SECTION,
										addLabel: t(FRAMEWORK_VERSION_METADATA),
										dataRef: 'metadata',
										addDefault: { key: '', value: '' },
										className: 'module-metadata',
										forms: [
											{
												type: IsyFormBuilderFormTypes.STRING,
												title: 'Key',
												dataRef: 'key',
												autoFocus: true,
												placeholder: t(ADD_FRAMEWORK_METADATA_KEY_PLACEHOLDER),
											},
											{
												type: IsyFormBuilderFormTypes.STRING,
												title: 'Value',
												dataRef: 'value',
												placeholder: t(
													ADD_FRAMEWORK_METADATA_VALUE_PLACEHOLDER
												),
											},
										],
									},
								],
							},
						],
					},
				],
			},
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				title: t(FRAMEWORK_CHANNEL_CONFIGURATION),
				dataRef: 'channelConfiguration',
				forms: [
					{
						title: t(FRAMEWORK_CHANNEL_NAME),
						type: IsyFormBuilderFormTypes.STRING,
						dataRef: 'channelName',
						placeholder: t(FRAMEWORK_CHANNEL_NAME_PLACEHOLDER),
						isRequired: true,
					},
				],
			},
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				className: 'framework-metadata-container',
				title: t(FRAMEWORK_METADATA),
				forms: [
					{
						type: IsyFormBuilderFormTypes.GRID_SECTION,
						addLabel: t(ADD_FRAMEWORK_METADATA_BUTTON_NAME),
						addDefault: { key: '', value: '' },
						dataRef: 'metadata',
						forms: [
							{
								type: IsyFormBuilderFormTypes.STRING,
								dataRef: 'key',
								autoFocus: true,
								placeholder: t(ADD_FRAMEWORK_METADATA_KEY_PLACEHOLDER),
								title: 'Key',
							},
							{
								type: IsyFormBuilderFormTypes.STRING,
								dataRef: 'value',
								placeholder: t(ADD_FRAMEWORK_METADATA_VALUE_PLACEHOLDER),
								title: 'Value',
							},
						],
					},
				],
			},
		]

		return config
	}

	const isFieldEmpty = (value: string | string[]) => {
		return isEmpty(value)
	}

	const isDataValid = (value: string, regex: RegExp) => {
		return regex.test(value)
	}

	// version validations
	const versionsValidations = (errors: FrameworkSettingsDetailsErrors) => {
		frameworkData.versions.forEach(
			(versionFields: FrameworkSettingsVersionProps, idx: number) => {
				if (isFieldEmpty(versionFields.versionName)) {
					set(errors, `versions.${idx}.versionName`, t(VALIDATE_VERSION_NAME))
				} else if (versionFields.versionName.length >= 16) {
					set(errors, `versions.${idx}.versionName`, t(VALIDATE_VERSION_RANGE))
				} else if (!isDataValid(versionFields.versionName, nameValidRegex)) {
					set(
						errors,
						`versions.${idx}.versionName`,
						t(FRAMEWORK_VALIDATE_VERSION_PATTERN)
					)
				}
				if (isFieldEmpty(versionFields.supportedPlatformVersions)) {
					set(
						errors,
						`versions.${idx}.supportedPlatformVersions`,
						t(VALIDATE_PLATFORM_VERSIONS)
					)
				}
				moduleValidations(versionFields.modules, idx, errors)
			}
		)
		return errors
	}

	// version validations
	const moduleValidations = (
		modules: FrameworkSettingsVersionModulesProps[],
		idx: number,
		errors: FrameworkSettingsDetailsErrors
	) => {
		Object.values(modules).forEach(
			(
				moduleFields: FrameworkSettingsVersionModulesProps,
				moduleId: number
			) => {
				if (isFieldEmpty(moduleFields.name)) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.name`,
						t(VALIDATE_NAME)
					)
				} else if (
					moduleFields.name.length < 3 ||
					moduleFields.name.length >= 64
				) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.name`,
						t(VALIDATE_MODULE_NAME_RANGE)
					)
				} else if (!isDataValid(moduleFields.name, nameValidRegex)) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.name`,
						t(VALIDATE_NAME_PATTERN)
					)
				}
				if (moduleFields.description.length >= 1024) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.description`,
						t(FRAMEWORK_VALIDATE_DESCRIPTION_PATTERN)
					)
				}
				if (isFieldEmpty(moduleFields.version)) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.version`,
						t(FRAMEWORK_VALIDATE_VERSION)
					)
				} else if (moduleFields.version.length >= 16) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.version`,
						t(VALIDATE_VERSION_RANGE)
					)
				} else if (!isDataValid(moduleFields.version, nameValidRegex)) {
					set(
						errors,
						`versions.${idx}.modules.${moduleId}.version`,
						t(FRAMEWORK_VALIDATE_VERSION_PATTERN)
					)
				}
			}
		)
		return errors
	}

	const handleValidation = () => {
		const fields = { ...frameworkData }
		const errors: FrameworkSettingsDetailsErrors = {}

		if (isFieldEmpty(fields.name)) {
			errors.name = t(VALIDATE_NAME)
		} else if (fields.name.length >= 64) {
			errors.name = t(VALIDATE_NAME_RANGE)
		} else if (!isDataValid(fields.name, nameValidRegex)) {
			errors.name = t(VALIDATE_NAME_PATTERN)
		}
		if (fields.description.length > 1024) {
			errors.description = t(FRAMEWORK_VALIDATE_DESCRIPTION_PATTERN)
		}
		if (isFieldEmpty(fields.channelConfiguration.channelName)) {
			errors.channelConfiguration = { channelName: t(VALIDATE_CHANNEL_NAME) }
		} else if (fields.channelConfiguration.channelName.length >= 64) {
			errors.channelConfiguration = {
				channelName: t(VALIDATE_CHANNEL_PATTERN),
			}
		}
		return versionsValidations(errors)
	}

	const handleClose = () => {
		dispatch(actions.clearData(null))
	}

	const handleDelete = (ref: string, index: number) => {
		if (Object.keys(errorList).length > 0) {
			let errorListData: FrameworkSettingsDetailsErrors = cloneDeep(errorList)
			errorListData = set(
				errorListData,
				ref,
				get(errorListData, ref).filter(
					(_: FrameworkSettingsVersionProps, id: number) => index !== id
				)
			)
			dispatch(actions.updateErrorList(errorListData))
		}
	}

	const handleOnChange = (val: FrameworkSettingsDetailsProps) => {
		dispatch(actions.updateFrameworkApiData(val))
	}

	const handleSave = () => {
		const errors = handleValidation()
		if (Object.keys(errors).length > 0) {
			// has errors
			return dispatch(actions.updateErrorList(errors))
		} else {
			dispatch(saveFramework())
			dispatch(fetchAllPlatform())
		}
	}

	// const handleEditIcon = () => {
	// 	dispatch(actions.setIsEdit(true))
	// }

	const renderFormBuilder = () => {
		return (
			<IsyFormBuilder<FrameworkSettingsDetailsProps>
				forms={getFormBuilderConfig()}
				data={frameworkData}
				errors={{ ...errorList }}
				onChange={handleOnChange}
				isViewMode={!isEdit}
			/>
		)
	}

	const renderEditIcon = () => {
		// if (!isEdit) {
		// 	return (
		// 		<IsyCan
		// 			action={IsyPermissionTypes.UPDATE_FRAMEWORK}
		// 			yes={() => (
		// 				<span
		// 					onClick={handleEditIcon}
		// 					tabIndex={0}
		// 					className='edit_icon-section'
		// 				>
		// 					<span className='edit_icon'>
		// 						<img src={`/images/Edit.svg`} alt={t(EDIT)} />
		// 					</span>
		// 					<span className='edit-label'>{t(EDIT)}</span>
		// 				</span>
		// 			)}
		// 		/>
		// 	)
		// }
		return null
	}

	const renderHeader = () => {
		return (
			<div className='header-section'>
				<span className='header-title'>
					{frameworkId !== '' ? frameworkData.name : t(ADD_FRAMEWORK)}
				</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
					tabIndex={0}
				/>
				{renderEditIcon()}
			</div>
		)
	}

	const renderMiddle = () => {
		return (
			<IsyBusyIndicator doNotShowNoText={true}>
				<div className='middle-section'>{renderFormBuilder()}</div>
			</IsyBusyIndicator>
		)
	}

	const renderBottom = () => {
		return (
			<div className='bottom-section'>
				{isEdit && (
					<IsyButton className='primary-btn' onClick={handleSave}>
						{t(BUTTON_SAVE)}
					</IsyButton>
				)}
				<IsyButton className='standard-btn' onClick={handleClose}>
					{t(BUTTON_CANCEL)}
				</IsyButton>
			</div>
		)
	}

	return (
		<Drawer
			className={'framework-settings ' + (isEdit ? '' : 'view-mode')}
			open={platformId !== ''}
			anchor='right'
			elevation={0}
			classes={{ paper: classes.paper }}
		>
			<div className='framework-settings-parent'>
				{renderHeader()}
				<div className='framework-settings-container'>{renderMiddle()}</div>
				{renderBottom()}
			</div>
		</Drawer>
	)
}
