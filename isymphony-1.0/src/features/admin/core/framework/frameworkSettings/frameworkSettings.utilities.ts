import { map } from 'lodash'
import {
	FrameworkProps,
	FrameworkVersionModulesProps,
	FrameworkVersionProps,
	PlatformMetadataProps,
} from '../../core.types'
import {
	FrameworkSettingsDetailsProps,
	FrameworkSettingsMetadataProps,
	FrameworkSettingsVersionModulesProps,
	FrameworkSettingsVersionProps,
} from './frameworkSettings.types'

const getFrameworkSettingMetadataFromFrameworkPayloadMetadata = (
	data: PlatformMetadataProps
): FrameworkSettingsMetadataProps[] => {
	return map(Object.keys(data), (key: string) => {
		return { key: key, value: data[key as any] as any }
	})
}

const getFrameworkSettingVersionModulesFromFrameworkPayloadVersionModule = (
	data: FrameworkVersionModulesProps[]
): FrameworkSettingsVersionModulesProps[] => {
	return map(data, (module) => ({
		name: module.name,
		description: module.description,
		version: module.version,
		metadata: getFrameworkSettingMetadataFromFrameworkPayloadMetadata(
			module.metadata
		),
	}))
}

const getFrameworkSettingVersionsFromFrameworkPayloadVersions = (
	data: FrameworkVersionProps[]
): FrameworkSettingsVersionProps[] => {
	return map(data, (version) => ({
		versionName: version.versionName,
		metadata: getFrameworkSettingMetadataFromFrameworkPayloadMetadata(
			version.metadata
		),
		supportedPlatformVersions: version.supportedPlatformVersions,
		modules: getFrameworkSettingVersionModulesFromFrameworkPayloadVersionModule(
			version.modules
		),
	}))
}

export const getFrameworkSettingsFromFrameworkPayload = (
	data: FrameworkProps
): FrameworkSettingsDetailsProps => {
	return {
		name: data.name,
		description: data.description,
		channelConfiguration: {
			channelName: data.channelConfiguration.channelName,
		},
		versions: getFrameworkSettingVersionsFromFrameworkPayloadVersions(
			data.versions
		),
		metadata: getFrameworkSettingMetadataFromFrameworkPayloadMetadata(
			data.metadata
		),
	}
}
