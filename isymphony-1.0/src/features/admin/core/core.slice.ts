import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { CoreSubReducersNames } from './base.types'
import { CoreState } from './core.types'
import * as asyncActions from './core.asyncActions'

const initialState: CoreState = {
	activeTab: 0,
	platforms: [],
}

const slice = createSlice<
	CoreState,
	SliceCaseReducers<CoreState>,
	CoreSubReducersNames.CORE
>({
	name: CoreSubReducersNames.CORE,
	initialState,
	reducers: {
		setActiveTab(state: CoreState, action: Action<number>) {
			state.activeTab = action.payload
		},
	},
	extraReducers: (builder) => {
		// asynchronous actions
		builder.addCase(
			asyncActions.fetchAllPlatform.fulfilled,
			(state: CoreState, action) => {
				if (action.payload) {
					state.platforms = action.payload.content
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
