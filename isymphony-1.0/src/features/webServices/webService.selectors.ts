import { isNil } from 'lodash'
import { RootState } from '../../base.types'
import { selectSlice as baseSelector } from './base.selector'
import { name } from './webService.slice'
import {
	WebServiceAuthorizationLookUpProps,
	WebServiceRequestBodyLookUpProps,
	WebServiceRequestMethodsLookUpProps,
	WebServiceRestDataMap,
	WebServiceRestProps,
	WebServiceRestTabDataMap,
	WebServiceState,
} from './webService.types'

export const selectSlice = (state: RootState): WebServiceState =>
	baseSelector(state)[name]

export const selectAppId = (state: RootState): string | null =>
	selectSlice(state).appId

export const selectSearchString = (state: RootState): string =>
	selectSlice(state).searchString

export const selectServiceMap = (state: RootState): WebServiceRestDataMap =>
	selectSlice(state).data

export const selectOldServiceMap = (state: RootState): WebServiceRestDataMap =>
	selectSlice(state).oldData

export const selectServiceTabMap = (
	state: RootState
): WebServiceRestTabDataMap => selectSlice(state).tabData

export const selectOrderList = (state: RootState): string[] =>
	selectSlice(state).listOrder

export const selectOpenTabIds = (state: RootState): string[] =>
	selectSlice(state).openTabIds

export const selectActiveTabId = (state: RootState): string | null =>
	selectSlice(state).activeTabId

export const selectRequestMethodsLookUps = (
	state: RootState
): WebServiceRequestMethodsLookUpProps[] =>
	selectSlice(state).requestMethodsLookUps

export const selectAuthorizationLookUps = (
	state: RootState
): WebServiceAuthorizationLookUpProps[] =>
	selectSlice(state).authorizationLookUps

export const selectRequestBodyLookUps = (
	state: RootState
): WebServiceRequestBodyLookUpProps[] => selectSlice(state).requestBodyLookUps

export const getWebServiceItemById = (
	state: RootState,
	id: string
): WebServiceRestProps | null => {
	const dataMap = selectServiceMap(state)
	const item = dataMap[id]
	if (isNil(item)) {
		return null
	}
	return item
}

export const getOldWebServiceItemById = (
	state: RootState,
	id: string
): WebServiceRestProps | null => {
	const dataMap = selectOldServiceMap(state)
	const item = dataMap[id]
	if (isNil(item)) {
		return null
	}
	return item
}
