import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import { WebServiceLeftMenu } from './components/webServiceLeftMenu/WebServiceLeftMenu'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import CloseIcon from '@material-ui/icons/Close'
import {
	confirmWebServiceDelete,
	saveWebServiceRequest,
	showWebServicesHelpLandingPage,
} from './webService.asyncActions'
import {
	WEB_SERVICE_TRANSLATIONS,
	WEB_SERVICES,
	WEB_SERVICE_URL_CONSTANT,
	ADD_WEB_SERVICE,
	NO_WEB_SERVICE_TEXT,
	WEB_SERVICE_NO_TABS_OPEN,
} from '../../utilities/constants'
import {
	addThirdPartyServices,
	closeTabConformation,
	initWebService,
	sendWebServiceRequest,
} from './webService.controller'
import './webService.scss'
import { useParams } from 'react-router-dom'
import { actions } from './webService.slice'
import {
	selectOpenTabIds,
	selectActiveTabId,
	selectOrderList,
	selectServiceMap,
	selectRequestMethodsLookUps,
	selectServiceTabMap,
	selectAuthorizationLookUps,
	selectRequestBodyLookUps,
	selectSearchString,
} from './webService.selectors'
import { IsyTabs, IsyTabItemProps } from '../../widgets/IsyTabs/IsyTabs'
import {
	WebServiceRestPathVariableProps,
	WebServiceRestRequestParameterProps,
	WebServiceRestProps,
	WebServiceRestTabProps,
} from './webService.types'
import { find, isEmpty, isNil, uniq } from 'lodash'
import { RestServiceTabContent } from './components/restServiceTabContent/RestServiceTabContent'
import { IsySteps } from '../../widgets/IsySteps/IsySteps'
import { getWebServicesSteps } from '../../utilities/isyStepsConfig'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import { selectGetWebServicePreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'

export function WebServices() {
	const { t } = useTranslation(WEB_SERVICE_TRANSLATIONS)
	const { appid } = useParams<{ appid: string }>()
	const dispatch = useDispatch()
	const orderList = useSelector(selectOrderList)
	const openPageTabIds = useSelector(selectOpenTabIds)
	const dataMap = useSelector(selectServiceMap)
	const pagTabMap = useSelector(selectServiceTabMap)
	const activePageTabId = useSelector(selectActiveTabId)
	const requestMethodsLookUps = useSelector(selectRequestMethodsLookUps)
	const authorizationLookUps = useSelector(selectAuthorizationLookUps)
	const requestBodyLookUps = useSelector(selectRequestBodyLookUps)
	const searchString = useSelector(selectSearchString)
	const webServicePreferenceData = useSelector(
		selectGetWebServicePreferenceStatus
	)

	useEffect(() => {
		dispatch(initWebService(appid))
		return () => {
			dispatch(actions.clearData(null))
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (
			webServicePreferenceData &&
			webServicePreferenceData.landingPage === true
		) {
			dispatch(showWebServicesHelpLandingPage())
		}
	}, [webServicePreferenceData, dispatch])

	const getPageTabs = (): Array<IsyTabItemProps<string>> => {
		return openPageTabIds.map((id) => {
			const data = dataMap[id]
			return {
				id: data.id,
				header: renderPageTabHeader(data),
			}
		})
	}

	const updateStepInfo = (stepNumber: number) => {
		if (webServicePreferenceData.status === HelpModuleStatus.INPROGRESS) {
			if (stepNumber === 6) {
				setTimeout(() => {
					dispatch(
						preferenceActions.updateWebServicesPreferences({
							currentStep: 6,
						})
					)
				}, 700)
			}
			dispatch(
				preferenceActions.updateWebServicesPreferences({
					currentStep: stepNumber,
				})
			)
		}
	}

	const handleSelectService = (id: string) => {
		dispatch(actions.openTab(id))
	}

	const handleOpenService = (page: IsyTabItemProps<string>) => {
		dispatch(actions.openTab(page.id))
	}

	const handleUpdateTabData = (
		id: string,
		data: Partial<WebServiceRestTabProps>
	) => {
		dispatch(actions.updateTabData({ id, data }))
	}

	const handleUpdateData = (id: string, data: Partial<WebServiceRestProps>) => {
		dispatch(actions.updateData({ id, data }))
	}

	const handleSearchStringChange = (search: string) => {
		dispatch(actions.updateSearchString(search))
	}

	const handleCloseService = (
		e: React.MouseEvent<SVGSVGElement | HTMLButtonElement, MouseEvent>,
		id: string
	) => {
		e.stopPropagation()
		dispatch(closeTabConformation(id))
	}

	const handleSendRequest = (id: string) => {
		dispatch(sendWebServiceRequest({ id, updateStepInfo }))
	}

	const handleSaveRequest = (id: string) => {
		dispatch(saveWebServiceRequest({ id, updateStepInfo }))
	}

	const handleAddService = () => {
		dispatch(addThirdPartyServices({ updateStepInfo }))
		webServicePreferenceData.status === HelpModuleStatus.INPROGRESS &&
			setTimeout(() => {
				dispatch(
					preferenceActions.updateWebServicesPreferences({
						currentStep: 1,
					})
				)
			}, 300)
	}

	const handleDeleteService = (webServiceId: string) => {
		dispatch(confirmWebServiceDelete(webServiceId))
	}

	const handleOnExit = (currentStep: number) => {
		let currentStepStatus
		if (currentStep === 9) {
			currentStepStatus = HelpModuleStatus.COMPLETED
		} else if (currentStep >= 0) {
			currentStepStatus = HelpModuleStatus.INCOMPLETE
		}

		if (currentStep !== -1) {
			dispatch(
				preferenceActions.updateWebServicesPreferences({
					status: currentStepStatus,
					currentStep: currentStep,
				})
			)
			dispatch(updatePreferences())
		}
	}

	const handleBeforeChange = (currentStep: number) => {
		if (currentStep === 1) {
			dispatch(addThirdPartyServices({ updateStepInfo }))
			dispatch(
				preferenceActions.updateWebServicesPreferences({
					currentStep: 1,
				})
			)
		} else if (currentStep === 5) {
			let data = activePageTabId && dataMap[activePageTabId]
			const url = t(WEB_SERVICE_URL_CONSTANT)
			let requestUrl = ''
			if (data && isEmpty(data.requestUrl)) {
				requestUrl = url
				const urlPaths = url.split('?')
				// setting path variables
				const urlMainPath = urlPaths[0]
				const pathArrayRegExp = /{([^}]+)}/g
				const pathVariables: WebServiceRestPathVariableProps[] = []
				const pathVariableList = urlMainPath.match(pathArrayRegExp)
				if (!isNil(pathVariableList)) {
					const filteredList = uniq(pathVariableList)
					for (let i = 0, iLen = filteredList.length; i < iLen; i++) {
						const str = filteredList[i]
						const key = str.substring(1, str.length - 1)
						const oldItem = find(data.pathVariables, (p) => p.key === key)
						let value = ''
						if (!isNil(oldItem)) {
							value = oldItem.value
						} else if (data.pathVariables.length === iLen) {
							value = data.pathVariables[i].value
						}
						pathVariables.push({
							key: key,
							value: value,
						})
					}
				}

				const requestParameters: WebServiceRestRequestParameterProps[] = []
				if (urlPaths.length > 1) {
					const queryStrings = urlPaths[1].split('&')
					for (let i = 0, iLen = queryStrings.length; i < iLen; i++) {
						const queryValues = queryStrings[i].split('=')
						const key = queryValues[0]
						let value = ''
						if (queryValues.length > 1) {
							value = queryValues[1]
						}
						requestParameters.push({ key, value })
					}
				}
				dispatch(
					actions.updateData({
						id: activePageTabId,
						data: { requestUrl, pathVariables, requestParameters },
					})
				)
			}
		}
		return true
	}

	const renderLeftMenu = () => {
		return (
			<WebServiceLeftMenu
				search={searchString}
				onDelete={handleDeleteService}
				orderList={orderList}
				dataMap={dataMap}
				onSelect={handleSelectService}
				onAdd={handleAddService}
				onSearchChange={handleSearchStringChange}
			/>
		)
	}

	const renderEmptyPage = () => {
		return (
			<div className='web-services-page-empty'>
				<div className='web-service'>
					<img
						className='web-services-icon'
						alt={t(WEB_SERVICES)}
						src='/images/thirdPartyIcons/service.svg'
					/>
				</div>
				<span className='web-services-text'>{t(NO_WEB_SERVICE_TEXT)}</span>
				<IsyButton
					className='primary-btn add-web-service'
					onClick={handleAddService}
				>
					{t(ADD_WEB_SERVICE)}
				</IsyButton>
			</div>
		)
	}

	const renderNoTabsPage = () => {
		return (
			<div className='web-services-page-empty'>
				<div className='web-service'>
					<img
						className='web-services-icon'
						alt={t(WEB_SERVICES)}
						src='/images/thirdPartyIcons/service.svg'
					/>
				</div>
				<span className='web-services-text'>{t(WEB_SERVICE_NO_TABS_OPEN)}</span>
			</div>
		)
	}

	const renderPageTabHeader = (item: WebServiceRestProps) => {
		return (
			<div className='page-header'>
				<span className='title' title={item.name}>
					{item.name}
				</span>
				<CloseIcon
					className='close'
					onClick={(e) => handleCloseService(e, item.id)}
				/>
			</div>
		)
	}

	const renderOpenTabs = () => {
		return (
			<IsyTabs<string>
				className='page-tabs'
				tabs={getPageTabs()}
				activeTab={activePageTabId || ''}
				onTabChange={handleOpenService}
				maxTabsCountToShow={7}
			/>
		)
	}

	const renderActiveTabContent = () => {
		if (isNil(activePageTabId)) {
			return null
		}
		const data = dataMap[activePageTabId]
		const tabData = pagTabMap[activePageTabId]
		if (isNil(data) || isNil(tabData)) {
			return null
		}
		return (
			<RestServiceTabContent
				data={data}
				tabData={tabData}
				authorizationLookUps={authorizationLookUps}
				requestMethodsLookUps={requestMethodsLookUps}
				requestBodyLookUps={requestBodyLookUps}
				onTabDataChange={(d) => handleUpdateTabData(activePageTabId, d)}
				onDataChange={(d) => handleUpdateData(activePageTabId, d)}
				onClose={(e) => handleCloseService(e, activePageTabId)}
				onSave={() => handleSaveRequest(activePageTabId)}
				onSendRequest={() => handleSendRequest(activePageTabId)}
			/>
		)
	}

	const renderRight = () => {
		if (openPageTabIds.length === 0) {
			return renderNoTabsPage()
		}
		return (
			<>
				{renderOpenTabs()}
				{renderActiveTabContent()}
			</>
		)
	}

	const render = () => {
		if (orderList.length === 0) {
			return renderEmptyPage()
		}
		return (
			<div className='web-services-container'>
				<div className='left'>{renderLeftMenu()}</div>
				<div className='right'>{renderRight()}</div>
			</div>
		)
	}

	const renderIsySteps = () => {
		return (
			<IsySteps
				steps={getWebServicesSteps(orderList)}
				isEnabled={
					webServicePreferenceData &&
					webServicePreferenceData.status === HelpModuleStatus.INPROGRESS
				}
				initialStep={
					webServicePreferenceData && webServicePreferenceData.currentStep
				}
				updateSteps={true}
				onExit={handleOnExit}
				onBeforeChange={handleBeforeChange}
			/>
		)
	}

	return (
		<>
			{render()} {renderIsySteps()}
		</>
	)
}
