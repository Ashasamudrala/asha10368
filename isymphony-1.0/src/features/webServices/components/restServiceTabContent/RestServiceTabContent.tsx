import { cloneDeep, find, findIndex, isEmpty, isNil, set, uniq } from 'lodash'
import React from 'react'
import { useTranslation } from 'react-i18next'
import {
	WebServiceAuthorizationLookUpType,
	WebServiceRequestBodyLookUpType,
} from '../../../../utilities/apiEnumConstants'
import {
	BUTTON_CANCEL,
	BUTTON_SAVE,
	WEB_SERVICE_BODY_TYPE_NONE_DESCRIPTION,
	WEB_SERVICE_HTTP_AUTHORIZATION,
	WEB_SERVICE_HTTP_AUTHORIZATION_NONE_DESCRIPTION,
	WEB_SERVICE_SEND,
	WEB_SERVICE_TRANSLATIONS,
	WEB_SERVICE_TYPE,
} from '../../../../utilities/constants'
import { IsyButton } from '../../../../widgets/IsyButton/IsyButton'
import {
	IsyDropDown,
	IsyDropDownOptionProps,
} from '../../../../widgets/IsyDropDown/IsyDropDown'
import { IsyFormBuilder } from '../../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { IsyTabItemProps, IsyTabs } from '../../../../widgets/IsyTabs/IsyTabs'
import {
	WebServiceAuthorizationLookUpProps,
	WebServiceRequestBodyLookUpProps,
	WebServiceRequestMethodsLookUpProps,
	WebServiceRestPathVariableProps,
	WebServiceRestProps,
	WebServiceRestRequestParameterProps,
	WebServiceRestRequestPayload,
	WebServiceRestRequestPayloadFormDataTypes,
	WebServiceRestResponseStatus,
	WebServiceRestTabProps,
	WebServiceRestTabType,
	WebServiceRestTabValidationStatus,
} from '../../webService.types'
import './restServiceTabContent.scss'
import {
	getFormBodyURLEncodingSectionConfig,
	getFormConfigFromFields,
	getFormHeaderSectionConfig,
	getFormMainSectionConfig,
	getFormParamsSectionConfig,
	getFormBodyFormDataSectionConfig,
	getTabs,
	formatXml,
} from './restServiceTabContent.utilities'
import lz from 'lzutf8'
import { CircularProgress, Typography } from '@material-ui/core'
import { IsyEditor } from '../../../../widgets/IsyEditor/IsyEditor'
import { RestServiceTabContentResponse } from './restServiceTabContentResponse/RestServiceTabContentResponse'

export interface RestServiceAuthBasicProps {
	username: string
	password: string
	showPassword: boolean
}

export interface RestServiceAuthBearerProps {
	token: string
}

export type RestServiceAuthProps =
	| RestServiceAuthBasicProps
	| RestServiceAuthBearerProps

export interface RestServiceAuthBasicProps {
	username: string
	password: string
	showPassword: boolean
}

export interface RestServiceTabContentProps {
	data: WebServiceRestProps
	tabData: WebServiceRestTabProps
	requestMethodsLookUps: WebServiceRequestMethodsLookUpProps[]
	authorizationLookUps: WebServiceAuthorizationLookUpProps[]
	requestBodyLookUps: WebServiceRequestBodyLookUpProps[]
	onTabDataChange: (tabData: Partial<WebServiceRestTabProps>) => void
	onDataChange: (data: Partial<WebServiceRestProps>) => void
	onClose: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
	onSave: () => void
	onSendRequest: () => void
}

export function RestServiceTabContent(props: RestServiceTabContentProps) {
	let pageX: number
	const { t } = useTranslation(WEB_SERVICE_TRANSLATIONS)

	const getAuthorizationOptions = (): Array<
		IsyDropDownOptionProps<WebServiceAuthorizationLookUpType>
	> => {
		return props.authorizationLookUps.map((item) => ({
			id: item.name,
			name: item.displayName,
		}))
	}

	const getActiveAuthorizationInfo = () => {
		return find(
			props.authorizationLookUps,
			(d) => d.name === props.data.authorizationType
		)
	}

	const getRequestBodyType = (): WebServiceRequestBodyLookUpType => {
		const contentTypeHeader = find(
			props.data.httpHeaders,
			(i) => i.key === 'Content-Type'
		)
		if (isNil(contentTypeHeader)) {
			return WebServiceRequestBodyLookUpType.NONE
		}
		return contentTypeHeader.value as WebServiceRequestBodyLookUpType
	}

	const getParamsWidthValue = (hasResponse: boolean) => {
		if (hasResponse) {
			return 100 - props.tabData.responseViewSize
		}
		return 100
	}

	const getAuthorizationData = (): RestServiceAuthProps | null => {
		const authorizationHeader = find(
			props.data.httpHeaders,
			(i) => i.key === 'Authorization'
		)
		if (isNil(authorizationHeader)) {
			return null
		}
		switch (props.data.authorizationType) {
			case WebServiceAuthorizationLookUpType.BASIC: {
				const base64 = authorizationHeader.value.split('Basic ')[1]
				const authString = lz.decompress(lz.decodeBase64(base64))
				const authArray = authString.split(':')
				return {
					username: authArray[0],
					password: authArray[1],
					showPassword: props.tabData.showPassword,
				}
			}
			case WebServiceAuthorizationLookUpType.BEARER: {
				const token = authorizationHeader.value.split('Bearer ')[1]
				return {
					token: token,
				}
			}
			case WebServiceAuthorizationLookUpType.NONE:
			default:
				return null
		}
	}

	const handleFormDataTypeChange = (
		value: WebServiceRestRequestPayloadFormDataTypes,
		ref: string
	) => {
		const newRef = ref.split('.type')[0] + '.value'
		const updatedData = cloneDeep(props.data as any)
		set(updatedData, newRef, '')
		set(updatedData, ref, value)
		props.onDataChange(updatedData)
	}

	const handleDragStart = (ev: React.DragEvent<HTMLDivElement>) => {
		pageX = ev.pageX
		console.log(ev.pageX)
	}

	const handleDrag = (ev: React.DragEvent<HTMLDivElement>) => {
		const changeX = ev.pageX - pageX
		const parent = (ev.target as any).parentElement
		const parentWidth = parent.getBoundingClientRect().width
		const childWidth = parent.children[2].getBoundingClientRect().width + 2
		const finalPercentage = ((childWidth - changeX) / parentWidth) * 100
		if (finalPercentage < 85 && finalPercentage > 20) {
			parent.children[2].style.width = `calc(${finalPercentage}% - 2px)`
			parent.children[0].style.width = `calc(${100 - finalPercentage}% - 2px)`
			pageX = ev.pageX
		}
	}

	const handleDragEnd = (ev: React.DragEvent<HTMLDivElement>) => {
		const parent = (ev.target as any).parentElement
		const parentWidth = parent.getBoundingClientRect().width
		const childWidth = parent.children[2].getBoundingClientRect().width + 2
		const finalPercentage = (childWidth / parentWidth) * 100
		props.onTabDataChange({ responseViewSize: finalPercentage })
	}

	const handleOnChange = (data: WebServiceRestProps) => {
		if (props.data.requestUrl === data.requestUrl) {
			const queryStrings = data.requestParameters.map(
				(d) => d.key + '=' + d.value
			)
			const query = queryStrings.join('&')
			const urlParts = data.requestUrl.split('?')
			if (!isEmpty(query)) {
				urlParts[1] = query
			}
			data.requestUrl = urlParts.join('?')
		}
		props.onDataChange(data)
	}

	const handleOnChangeOfURL = (url: string) => {
		const urlPaths = url.split('?')
		// setting path variables
		const urlMainPath = urlPaths[0]
		const pathArrayRegExp = /{([^}]+)}/g
		const pathVariables: WebServiceRestPathVariableProps[] = []
		const pathVariableList = urlMainPath.match(pathArrayRegExp)
		if (!isNil(pathVariableList)) {
			const filteredList = uniq(pathVariableList)
			for (let i = 0, iLen = filteredList.length; i < iLen; i++) {
				const str = filteredList[i]
				const key = str.substring(1, str.length - 1)
				const oldItem = find(props.data.pathVariables, (p) => p.key === key)
				let value = ''
				if (!isNil(oldItem)) {
					value = oldItem.value
				} else if (props.data.pathVariables.length === iLen) {
					value = props.data.pathVariables[i].value
				}
				pathVariables.push({
					key: key,
					value: value,
				})
			}
		}

		const requestParameters: WebServiceRestRequestParameterProps[] = []
		if (urlPaths.length > 1) {
			const queryStrings = urlPaths[1].split('&')
			for (let i = 0, iLen = queryStrings.length; i < iLen; i++) {
				const queryValues = queryStrings[i].split('=')
				const key = queryValues[0]
				let value = ''
				if (queryValues.length > 1) {
					value = queryValues[1]
				}
				requestParameters.push({ key, value })
			}
		}

		props.onDataChange({ pathVariables, requestParameters })
	}

	const handleTabChange = (tab: IsyTabItemProps<WebServiceRestTabType>) => {
		props.onTabDataChange({ activeTab: tab.id })
	}

	const handleBodyTypeChange = (type: WebServiceRequestBodyLookUpType) => {
		const headers = cloneDeep(props.data.httpHeaders)
		const contentTypeHeader = findIndex(
			headers,
			(i) => i.key === 'Content-Type'
		)
		const payload: WebServiceRestRequestPayload = {}
		switch (type) {
			case WebServiceRequestBodyLookUpType.JSON: {
				if (contentTypeHeader === -1) {
					headers.unshift({
						key: 'Content-Type',
						value: type,
						type: 'string',
					})
				} else {
					headers[contentTypeHeader].value = type
				}
				payload[type] = {} as JSON
				break
			}
			case WebServiceRequestBodyLookUpType.XML: {
				if (contentTypeHeader === -1) {
					headers.unshift({
						key: 'Content-Type',
						value: type,
						type: 'string',
					})
				} else {
					headers[contentTypeHeader].value = type
				}
				payload[type] = '' as any
				break
			}
			case WebServiceRequestBodyLookUpType.HTML_PLAIN: {
				if (contentTypeHeader === -1) {
					headers.push({
						key: 'Content-Type',
						value: type,
						type: 'string',
					})
				} else {
					headers[contentTypeHeader].value = type
				}
				payload[type] = ''
				break
			}
			case WebServiceRequestBodyLookUpType.URL_ENCODED_DATA: {
				if (contentTypeHeader === -1) {
					headers.unshift({
						key: 'Content-Type',
						value: type,
						type: 'string',
					})
				} else {
					headers[contentTypeHeader].value = type
				}
				payload[type] = [{ key: '', value: '' }]
				break
			}
			case WebServiceRequestBodyLookUpType.FORM_DATA: {
				if (contentTypeHeader === -1) {
					headers.unshift({
						key: 'Content-Type',
						value: type,
						type: 'string',
					})
				} else {
					headers[contentTypeHeader].value = type
				}
				payload[type] = [
					{
						key: '',
						value: '',
						type: WebServiceRestRequestPayloadFormDataTypes.STRING,
					},
				]
				break
			}
			case WebServiceRequestBodyLookUpType.NONE:
			default: {
				if (contentTypeHeader !== -1) {
					headers.splice(contentTypeHeader, 1)
				}
				break
			}
		}
		props.onDataChange({ httpHeaders: headers, requestPayload: payload })
		const errors = cloneDeep(props.tabData.errors) || {}
		delete errors.requestPayload
		props.onTabDataChange({ errors: errors })
	}

	const handleJsonEditorChange = (data: string) => {
		try {
			const json = JSON.parse(data)
			props.onDataChange({
				requestPayload: { [WebServiceRequestBodyLookUpType.JSON]: json },
			})
			const errors = cloneDeep(props.tabData.errors) || {}
			delete errors.requestPayload
			props.onTabDataChange({ errors: errors })
		} catch (error) {
			const errors = cloneDeep(props.tabData.errors) || {}
			errors.requestPayload = {}
			errors.requestPayload[WebServiceRequestBodyLookUpType.JSON] =
				error.message
			props.onTabDataChange({ errors: errors })
		}
	}

	const handleHtmlEditorChange = (data: string) => {
		props.onDataChange({
			requestPayload: { [WebServiceRequestBodyLookUpType.HTML_PLAIN]: data },
		})
	}

	const handleXMLEditorChange = (data: string) => {
		props.onDataChange({
			requestPayload: {
				[WebServiceRequestBodyLookUpType.XML]: formatXml(data),
			},
		})
	}

	const handleAuthorizationTypeChange = (
		type: WebServiceAuthorizationLookUpType
	) => {
		const headers = cloneDeep(props.data.httpHeaders)
		const authorizationHeaderIndex = findIndex(
			headers,
			(i) => i.key === 'Authorization'
		)
		switch (type) {
			case WebServiceAuthorizationLookUpType.BASIC: {
				const base64 = lz.encodeBase64(lz.compress(':'))
				const finalString = 'Basic ' + base64
				if (authorizationHeaderIndex === -1) {
					headers.unshift({
						key: 'Authorization',
						value: finalString,
						type: 'String',
					})
				} else {
					headers[authorizationHeaderIndex].value = finalString
				}
				break
			}
			case WebServiceAuthorizationLookUpType.BEARER: {
				const finalString = 'Bearer '
				if (authorizationHeaderIndex === -1) {
					headers.unshift({
						key: 'Authorization',
						value: finalString,
						type: 'String',
					})
				} else {
					headers[authorizationHeaderIndex].value = finalString
				}
				break
			}
			case WebServiceAuthorizationLookUpType.NONE:
			default: {
				if (authorizationHeaderIndex !== -1) {
					headers.splice(authorizationHeaderIndex, 1)
				}
				break
			}
		}
		props.onDataChange({ httpHeaders: headers, authorizationType: type })
	}

	const handleHeaderDataChange = (data: RestServiceAuthProps) => {
		const headers = cloneDeep(props.data.httpHeaders)
		const authorizationHeaderIndex = findIndex(
			headers,
			(i) => i.key === 'Authorization'
		)
		switch (props.data.authorizationType) {
			case WebServiceAuthorizationLookUpType.BASIC: {
				const info = data as RestServiceAuthBasicProps
				const authString = info.username + ':' + info.password
				const base64 = lz.encodeBase64(lz.compress(authString))
				const finalString = 'Basic ' + base64
				if (authorizationHeaderIndex === -1) {
					headers.push({
						key: 'Authorization',
						value: finalString,
						type: 'string',
					})
				} else {
					headers[authorizationHeaderIndex].value = finalString
				}
				if (info.showPassword !== props.tabData.showPassword) {
					props.onTabDataChange({ showPassword: info.showPassword })
				}
				break
			}
			case WebServiceAuthorizationLookUpType.BEARER: {
				const info = data as RestServiceAuthBearerProps
				const finalString = 'Bearer ' + info.token
				if (authorizationHeaderIndex === -1) {
					headers.push({
						key: 'Authorization',
						value: finalString,
						type: 'string',
					})
				} else {
					headers[authorizationHeaderIndex].value = finalString
				}
				break
			}
			case WebServiceAuthorizationLookUpType.NONE:
			default: {
				if (authorizationHeaderIndex !== -1) {
					headers.splice(authorizationHeaderIndex, 1)
				}
				break
			}
		}
		props.onDataChange({ httpHeaders: headers })
	}

	const renderMainFormSection = () => {
		return (
			<div className='main-section'>
				<IsyFormBuilder<WebServiceRestProps>
					forms={getFormMainSectionConfig(
						props.requestMethodsLookUps,
						handleOnChangeOfURL
					)}
					data={props.data}
					onChange={handleOnChange}
				/>
				<div className='send-button-holder'>
					<IsyButton
						className='secondary-btn send-btn'
						onClick={props.onSendRequest}
					>
						{t(WEB_SERVICE_SEND)}
					</IsyButton>
				</div>
			</div>
		)
	}

	const renderTabs = () => {
		return (
			<IsyTabs<WebServiceRestTabType>
				tabs={getTabs()}
				activeTab={props.tabData.activeTab}
				onTabChange={handleTabChange}
				className='tabs'
			/>
		)
	}

	const renderParamsSection = () => {
		return (
			<IsyFormBuilder<WebServiceRestProps>
				forms={getFormParamsSectionConfig(props.data.pathVariables.length > 0)}
				data={props.data}
				onChange={handleOnChange}
			/>
		)
	}

	const renderAuthorizationForm = (
		config: WebServiceAuthorizationLookUpProps
	) => {
		const data = getAuthorizationData()
		if (isNil(data)) {
			return null
		}
		return (
			<IsyFormBuilder<RestServiceAuthProps>
				forms={getFormConfigFromFields(
					config.fields || [],
					props.tabData.showPassword
				)}
				data={data}
				onChange={handleHeaderDataChange}
			/>
		)
	}

	const renderAuthorizationSection = () => {
		const config = getActiveAuthorizationInfo()
		if (isNil(config)) {
			return null
		}
		return (
			<div className='authorization'>
				<Typography className='authorization-label'>
					{t(WEB_SERVICE_HTTP_AUTHORIZATION)}
				</Typography>
				<IsyDropDown<WebServiceAuthorizationLookUpType>
					onChange={handleAuthorizationTypeChange}
					options={getAuthorizationOptions()}
					value={props.data.authorizationType}
					className='authorization-dropdown'
				/>
				{props.data.authorizationType ===
					WebServiceAuthorizationLookUpType.NONE && (
					<div className='info'>
						{t(WEB_SERVICE_HTTP_AUTHORIZATION_NONE_DESCRIPTION)}
					</div>
				)}
				{renderAuthorizationForm(config)}
			</div>
		)
	}

	const renderHeaderSection = () => {
		return (
			<IsyFormBuilder<WebServiceRestProps>
				forms={getFormHeaderSectionConfig()}
				data={props.data}
				onChange={handleOnChange}
			/>
		)
	}

	const renderBodyContent = (type: WebServiceRequestBodyLookUpType) => {
		switch (type) {
			case WebServiceRequestBodyLookUpType.JSON:
				const hasError =
					!isNil(props.tabData.errors.requestPayload) &&
					!isNil(props.tabData.errors.requestPayload['application/json'])
				return (
					<>
						<IsyEditor
							value={JSON.stringify(
								props.data.requestPayload[type],
								undefined,
								2
							)}
							onChange={handleJsonEditorChange}
							options={{
								mode: { name: 'javascript', json: true },
							}}
						/>
						{hasError && (
							<div>
								{props.tabData.errors.requestPayload['application/json']}
							</div>
						)}
					</>
				)
			case WebServiceRequestBodyLookUpType.XML:
				return (
					<IsyEditor
						value={props.data.requestPayload[type] as string}
						onChange={handleXMLEditorChange}
						options={{
							mode: 'xml',
						}}
					/>
				)
			case WebServiceRequestBodyLookUpType.HTML_PLAIN:
				return (
					<IsyEditor
						value={props.data.requestPayload[type] as string}
						onChange={handleHtmlEditorChange}
						options={{
							mode: 'text/html',
						}}
					/>
				)
			case WebServiceRequestBodyLookUpType.URL_ENCODED_DATA:
				return (
					<IsyFormBuilder<WebServiceRestProps>
						forms={getFormBodyURLEncodingSectionConfig()}
						data={props.data}
						onChange={handleOnChange}
					/>
				)
			case WebServiceRequestBodyLookUpType.FORM_DATA:
				return (
					<IsyFormBuilder<WebServiceRestProps>
						forms={getFormBodyFormDataSectionConfig(handleFormDataTypeChange)}
						data={props.data}
						onChange={handleOnChange}
					/>
				)
			case WebServiceRequestBodyLookUpType.NONE:
			default:
				return (
					<div className='info'>
						{t(WEB_SERVICE_BODY_TYPE_NONE_DESCRIPTION)}
					</div>
				)
		}
	}

	const renderBodySection = () => {
		const type = getRequestBodyType()
		return (
			<div className='body-section'>
				<Typography className='body-section-label'>
					{t(WEB_SERVICE_TYPE)}
				</Typography>
				<IsyDropDown<WebServiceRequestBodyLookUpType>
					onChange={handleBodyTypeChange}
					options={props.requestBodyLookUps}
					value={type}
				/>
				{renderBodyContent(type)}
			</div>
		)
	}

	const renderActiveTabContent = () => {
		switch (props.tabData.activeTab) {
			case WebServiceRestTabType.PARAMS:
				return renderParamsSection()
			case WebServiceRestTabType.AUTHORIZATION:
				return renderAuthorizationSection()
			case WebServiceRestTabType.HEADER:
				return renderHeaderSection()
			case WebServiceRestTabType.BODY:
				return renderBodySection()
			default:
				return null
		}
	}

	const renderConfigDataSection = (hasResponse: boolean) => {
		return (
			<div
				className='config-data-section'
				style={{
					width: `calc(${getParamsWidthValue(hasResponse)}% - ${
						hasResponse ? 2 : 0
					}px)`,
				}}
			>
				{renderTabs()}
				<div className='config-tab-content'>{renderActiveTabContent()}</div>
			</div>
		)
	}

	const renderBottom = () => {
		return (
			<div className='bottom-section'>
				<IsyButton onClick={props.onClose} className='standard-btn'>
					{t(BUTTON_CANCEL)}
				</IsyButton>
				<IsyButton
					onClick={props.onSave}
					disabled={
						props.tabData.validationStatus !==
							WebServiceRestTabValidationStatus.DONE ||
						props.tabData.response?.status !== WebServiceRestResponseStatus.OKAY
					}
					className='primary-btn save-btn'
				>
					{t(BUTTON_SAVE)}
				</IsyButton>
			</div>
		)
	}

	const renderLoader = () => {
		if (
			props.tabData.validationStatus ===
			WebServiceRestTabValidationStatus.LOADING
		) {
			return (
				<div className='progress'>
					<CircularProgress size={40} className='loader' />
				</div>
			)
		}
		return null
	}

	const renderResponse = () => {
		return (
			<div
				className='right'
				style={{ width: `calc(${props.tabData.responseViewSize}% - 2px)` }}
			>
				<RestServiceTabContentResponse
					tabData={props.tabData}
					onTabDataChange={props.onTabDataChange}
				/>
			</div>
		)
	}

	const render = () => {
		const hasResponse =
			props.tabData.validationStatus ===
				WebServiceRestTabValidationStatus.DONE && !isNil(props.tabData.response)
		return (
			<div className='rest-service-page-tab-content'>
				{renderMainFormSection()}
				<div className='middle-section'>
					{renderConfigDataSection(hasResponse)}
					{hasResponse && (
						<div
							className='middle'
							draggable
							onDragStart={handleDragStart}
							onDrag={handleDrag}
							onDragEnd={handleDragEnd}
						/>
					)}
					{hasResponse && renderResponse()}
				</div>
				{renderBottom()}
				{renderLoader()}
			</div>
		)
	}
	return render()
}
