import { IsyTabItemProps } from '../../../../widgets/IsyTabs/IsyTabs'
import {
	WebServiceAuthorizationLookUpFieldProps,
	WebServiceRequestMethodsLookUpProps,
	WebServiceRestRequestPayloadFormDataTypes,
	WebServiceRestResponseViewByType,
	WebServiceRestTabType,
} from '../../webService.types'
import i18n from 'i18next'
import {
	ADD,
	COMMON_DESCRIPTION_PLACEHOLDER,
	DESCRIPTION,
	ENTER_KEY,
	ENTER_VALUE,
	KEY,
	VALUE,
	WEB_SERVICE_AUTHORIZATION,
	WEB_SERVICE_BODY,
	WEB_SERVICE_ENTER_URL_PLACEHOLDER,
	WEB_SERVICE_FILE,
	WEB_SERVICE_HEADER,
	WEB_SERVICE_PARAMS,
	WEB_SERVICE_PATH_PARAMS,
	WEB_SERVICE_PRETTY,
	WEB_SERVICE_QUERY_PARAMS,
	WEB_SERVICE_RAW,
	WEB_SERVICE_STRING,
	WEB_SERVICE_TYPE,
	WEB_SERVICE_WEB_SERVICE_NAME,
	WEB_SERVICE_WEB_SERVICE_NAME_PLACEHOLDER,
} from '../../../../utilities/constants'
import {
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { each } from 'lodash'

const getRestRequestPayloadFormDataOptions = () => [
	{
		id: WebServiceRestRequestPayloadFormDataTypes.STRING,
		name: i18n.t(WEB_SERVICE_STRING),
	},
	{
		id: WebServiceRestRequestPayloadFormDataTypes.FILE,
		name: i18n.t(WEB_SERVICE_FILE),
	},
]

export const getFormConfigFromFields = (
	fields: WebServiceAuthorizationLookUpFieldProps[],
	showPassword: boolean
): IsyFormBuilderFormItemProps[] => {
	const list: IsyFormBuilderFormItemProps[] = []
	for (let i = 0, iLen = fields.length; i < iLen; i++) {
		if (fields[i].type === 'string') {
			if (
				fields[i].uiConfiguration.metadata.renderAs === 'Password' &&
				!showPassword
			) {
				list.push({
					type: IsyFormBuilderFormTypes.PASSWORD,
					title: fields[i].displayName,
					dataRef: fields[i].name,
					placeholder: fields[i].uiConfiguration.metadata.placeholderValue,
					className: fields[i].name + '-input',
				})
			} else {
				list.push({
					type: IsyFormBuilderFormTypes.STRING,
					title: fields[i].displayName,
					dataRef: fields[i].name,
					placeholder: fields[i].uiConfiguration.metadata.placeholderValue,
					className: fields[i].name + '-input',
				})
			}
		} else {
			list.push({
				type: IsyFormBuilderFormTypes.CHECKBOX,
				label: fields[i].displayName,
				dataRef: fields[i].name,
			})
		}
	}
	return list
}

export const getTabs = (): IsyTabItemProps<WebServiceRestTabType>[] => {
	return [
		{
			id: WebServiceRestTabType.PARAMS,
			header: i18n.t(WEB_SERVICE_PARAMS),
		},
		{
			id: WebServiceRestTabType.AUTHORIZATION,
			header: i18n.t(WEB_SERVICE_AUTHORIZATION),
		},
		{
			id: WebServiceRestTabType.HEADER,
			header: i18n.t(WEB_SERVICE_HEADER),
		},
		{
			id: WebServiceRestTabType.BODY,
			header: i18n.t(WEB_SERVICE_BODY),
		},
	]
}

export const getFormMainSectionConfig = (
	requestMethodsLookUps: WebServiceRequestMethodsLookUpProps[],
	onUrlBlur: (val: string) => void
): IsyFormBuilderFormItemProps[] => {
	return [
		{
			type: IsyFormBuilderFormTypes.STRING,
			title: i18n.t(WEB_SERVICE_WEB_SERVICE_NAME),
			dataRef: 'name',
			className: 'name-input',
			placeholder: i18n.t(WEB_SERVICE_WEB_SERVICE_NAME_PLACEHOLDER),
		},
		{
			type: IsyFormBuilderFormTypes.STRING,
			title: i18n.t(DESCRIPTION),
			dataRef: 'description',
			className: 'description-input',
			placeholder: i18n.t(COMMON_DESCRIPTION_PLACEHOLDER),
		},
		{
			type: IsyFormBuilderFormTypes.SELECT,
			dataRef: 'requestMethod',
			options: requestMethodsLookUps,
			className: 'requestMethod-input',
		},
		{
			type: IsyFormBuilderFormTypes.STRING,
			dataRef: 'requestUrl',
			className: 'url-input',
			onBlur: onUrlBlur,
			placeholder: i18n.t(WEB_SERVICE_ENTER_URL_PLACEHOLDER),
		},
	]
}

export const getFormHeaderSectionConfig = (): IsyFormBuilderFormItemProps[] => {
	return [
		{
			type: IsyFormBuilderFormTypes.GRID_SECTION,
			dataRef: 'httpHeaders',
			className: 'query-params-section',
			addLabel: i18n.t(ADD),
			addDefault: { key: '', type: 'String', value: '' },
			addButtonAtEnd: true,
			isRowDisabled: (data) =>
				data.key === 'Authorization' || data.key === 'Content-Type',
			forms: [
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(KEY),
					dataRef: 'key',
					placeholder: i18n.t(ENTER_KEY),
				},
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(VALUE),
					dataRef: 'value',
					placeholder: i18n.t(ENTER_VALUE),
				},
			],
		},
	]
}

export const getFormBodyFormDataSectionConfig = (
	typeChangeCallBack: (val: any, ref: string) => void
): IsyFormBuilderFormItemProps[] => {
	return [
		{
			type: IsyFormBuilderFormTypes.GRID_SECTION,
			dataRef: 'requestPayload.multipart/form-data',
			className: 'form-data-section',
			addLabel: i18n.t(ADD),
			addDefault: {
				key: '',
				value: '',
				type: WebServiceRestRequestPayloadFormDataTypes.STRING,
			},
			addButtonAtEnd: true,
			forms: [
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(KEY),
					dataRef: 'key',
					placeholder: i18n.t(ENTER_KEY),
				},
				{
					type: IsyFormBuilderFormTypes.SELECT,
					title: i18n.t(WEB_SERVICE_TYPE),
					dataRef: 'type',
					options: getRestRequestPayloadFormDataOptions(),
					onChange: typeChangeCallBack,
				},
				{
					type: IsyFormBuilderFormTypes.ANY_OF_ONE,
					title: i18n.t(VALUE),
					dataRef: 'type',
					getChoice: (type) =>
						type === WebServiceRestRequestPayloadFormDataTypes.STRING ? 0 : 1,
					forms: [
						{
							type: IsyFormBuilderFormTypes.STRING,
							dataRef: 'value',
							placeholder: i18n.t(ENTER_VALUE),
						},
						{
							type: IsyFormBuilderFormTypes.FILE_SELECT,
							dataRef: 'value',
						},
					],
				},
			],
		},
	]
}

export const getFormBodyURLEncodingSectionConfig = (): IsyFormBuilderFormItemProps[] => {
	return [
		{
			type: IsyFormBuilderFormTypes.GRID_SECTION,
			dataRef: 'requestPayload.application/x-www-form-urlencoded',
			className: 'url-encoding-form',
			addLabel: i18n.t(ADD),
			addDefault: { key: '', value: '' },
			addButtonAtEnd: true,
			forms: [
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(KEY),
					dataRef: 'key',
					placeholder: i18n.t(ENTER_KEY),
				},
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(VALUE),
					dataRef: 'value',
					placeholder: i18n.t(ENTER_VALUE),
				},
			],
		},
	]
}

export const getFormParamsSectionConfig = (
	hasPathParams: boolean
): IsyFormBuilderFormItemProps[] => {
	return [
		{
			type: IsyFormBuilderFormTypes.GRID_SECTION,
			dataRef: 'requestParameters',
			title: i18n.t(WEB_SERVICE_QUERY_PARAMS),
			className: 'query-params-section',
			addLabel: i18n.t(ADD),
			addDefault: { key: '', type: 'String', value: '' },
			addButtonAtEnd: true,
			forms: [
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(KEY),
					dataRef: 'key',
					autoFocus: true,
					placeholder: i18n.t(ENTER_KEY),
				},
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(VALUE),
					dataRef: 'value',
					placeholder: i18n.t(ENTER_VALUE),
				},
			],
		},
		{
			type: IsyFormBuilderFormTypes.GRID_SECTION,
			dataRef: 'pathVariables',
			className: 'params-section',
			title: i18n.t(WEB_SERVICE_PATH_PARAMS),
			fixedLengthArray: true,
			isHidden: !hasPathParams,
			forms: [
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(KEY),
					dataRef: 'key',
					disabled: true,
					placeholder: i18n.t(ENTER_KEY),
				},
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(VALUE),
					dataRef: 'value',
					placeholder: i18n.t(ENTER_VALUE),
				},
			],
		},
	]
}

export const getResponseViewByOptions = () => [
	{
		name: i18n.t(WEB_SERVICE_PRETTY),
		id: WebServiceRestResponseViewByType.PRETTY,
	},
	{
		name: i18n.t(WEB_SERVICE_RAW),
		id: WebServiceRestResponseViewByType.RAW,
	},
]

export const formatXml = (xml: string): string => {
	const reg1 = /^<\w[^>]*[^\/]>.*$/ //  eslint-disable-line no-useless-escape
	let formatted = ''
	let reg = /(>)(<)(\/*)/g
	xml = xml.replace(reg, '$1\r\n$2$3')
	let pad = 0
	each(xml.split('\r\n'), function (node) {
		let indent = 0
		if (node.match(/.+<\/\w[^>]*>$/)) {
			indent = 0
		} else if (node.match(/^<\/\w/)) {
			if (pad !== 0) {
				pad -= 1
			}
		} else if (node.match(reg1)) {
			indent = 1
		} else {
			indent = 0
		}

		let padding = ''
		for (let i = 0; i < pad; i++) {
			padding += '  '
		}

		formatted += padding + node + '\r\n'
		pad += indent
	})

	return formatted
}

export const getStatusText = (code: number) => {
	switch (code) {
		case 100:
			return 'Continue'
		case 101:
			return 'Switching Protocol'
		case 102:
			return 'Processing'
		case 103:
			return 'Early Hints'
		case 200:
			return 'OK'
		case 201:
			return 'Created'
		case 202:
			return 'Accepted'
		case 203:
			return 'Non-Authoritative Information'
		case 204:
			return 'No Content'
		case 205:
			return 'Reset Content'
		case 206:
			return 'Partial Content'
		case 207:
			return 'Multi-Status'
		case 208:
			return 'Already Reported'
		case 226:
			return 'IM Used (HTTP Delta encoding)'
		case 300:
			return 'Multiple Choice'
		case 301:
			return 'Moved Permanently'
		case 302:
			return 'Found'
		case 303:
			return 'See Other'
		case 304:
			return 'Not Modified'
		case 305:
			return 'Use Proxy '
		case 306:
			return 'unused'
		case 307:
			return 'Temporary Redirect'
		case 308:
			return 'Permanent Redirect'
		case 400:
			return 'Bad Request'
		case 401:
			return 'Unauthorized'
		case 402:
			return 'Payment Required '
		case 403:
			return 'Forbidden'
		case 404:
			return 'Not Found'
		case 405:
			return 'Method Not Allowed'
		case 406:
			return 'Not Acceptable'
		case 407:
			return 'Proxy Authentication Required'
		case 408:
			return 'Request Timeout'
		case 409:
			return 'Conflict'
		case 410:
			return 'Gone'
		case 411:
			return 'Length Required'
		case 412:
			return 'Precondition Failed'
		case 413:
			return 'Payload Too Large'
		case 414:
			return 'URI Too Long'
		case 415:
			return 'Unsupported Media Type'
		case 416:
			return 'Range Not Satisfiable'
		case 417:
			return 'Expectation Failed'
		case 418:
			return `I'm a teapot`
		case 421:
			return 'Misdirected Request'
		case 422:
			return 'Unprocessable Entity'
		case 423:
			return 'Locked'
		case 424:
			return 'Failed Dependency'
		case 425:
			return 'Too Early '
		case 426:
			return 'Upgrade Required'
		case 428:
			return 'Precondition Required'
		case 429:
			return 'Too Many Requests'
		case 431:
			return 'Request Header Fields Too Large'
		case 451:
			return 'Unavailable For Legal Reasons'
		case 500:
			return 'Internal Server Error'
		case 501:
			return 'Not Implemented'
		case 502:
			return 'Bad Gateway'
		case 503:
			return 'Service Unavailable'
		case 504:
			return 'Gateway Timeout'
		case 505:
			return 'HTTP Version Not Supported'
		case 506:
			return 'Variant Also Negotiates'
		case 507:
			return 'Insufficient Storage'
		case 508:
			return 'Loop Detected'
		case 510:
			return 'Not Extended'
		case 511:
			return 'Network Authentication Required'
	}
}
