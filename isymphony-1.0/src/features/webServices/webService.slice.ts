import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import {
	WebServiceRequestBodyLookUpProps,
	WebServiceRequestMethodsLookUpProps,
	WebServiceRestDataMap,
	WebServiceRestProps,
	WebServiceRestTabProps,
	WebServiceRestTabType,
	WebServiceRestTabValidationStatus,
	WebServiceState,
	WebServiceRestResponseViewByType,
} from './webService.types'
import { webServiceSubReducersNames } from './base.types'
import { Action } from '../../common.types'
import * as asyncActions from './webService.asyncActions'
import { cloneDeep, isArray, isNil, omit } from 'lodash'
import { v4 } from 'uuid'
import {
	WebServiceAuthorizationLookUpType,
	WebServiceRequestMethodsLookUpType,
} from '../../utilities/apiEnumConstants'
import { getUniqueName } from '../../utilities/utilities'
import { setDataTOUiStandards } from './webService.utilities'

const tabDataInitial: WebServiceRestTabProps = {
	activeTab: WebServiceRestTabType.PARAMS,
	showPassword: false,
	errors: {},
	validationStatus: WebServiceRestTabValidationStatus.NONE,
	response: null,
	responseByView: WebServiceRestResponseViewByType.PRETTY,
	isResponseExpanded: false,
	responseViewSize: 50,
}

const initialWebServiceData: WebServiceRestProps = {
	id: '',
	isNew: true,
	name: '',
	description: '',
	requestMethod: WebServiceRequestMethodsLookUpType.GET,
	requestUrl: '',
	authorizationType: WebServiceAuthorizationLookUpType.NONE,
	pathVariables: [],
	requestParameters: [],
	requestPayload: {},
	httpHeaders: [],
}

const initialState: WebServiceState = {
	searchString: '',
	openTabIds: [],
	activeTabId: null,
	appId: null,
	data: {},
	tabData: {},
	listOrder: [],
	requestMethodsLookUps: [],
	authorizationLookUps: [],
	requestBodyLookUps: [],
	oldData: {},
}

const slice = createSlice<
	WebServiceState,
	SliceCaseReducers<WebServiceState>,
	webServiceSubReducersNames.WEB_SERVICE
>({
	name: webServiceSubReducersNames.WEB_SERVICE,
	initialState,
	reducers: {
		addNew(state) {
			const id = v4()
			const names = Object.values(state.data).map((d) => d.name)
			const newData = Object.assign({}, cloneDeep(initialWebServiceData), {
				name: getUniqueName(names, 'New Tab', ' '),
				id: id,
			})
			const data = cloneDeep(state.data)
			data[id] = newData
			state.data = data
			state.listOrder = [...state.listOrder, id]
			state.openTabIds = [...state.openTabIds, id]
			state.activeTabId = id
			state.tabData = {
				...state.tabData,
				[id]: cloneDeep(tabDataInitial),
			}
		},
		updateTabData(
			state,
			action: Action<{ id: string; data: Partial<WebServiceRestTabProps> }>
		) {
			state.tabData = {
				...state.tabData,
				[action.payload.id]: {
					...state.tabData[action.payload.id],
					...action.payload.data,
				},
			}
		},
		updateData(
			state,
			action: Action<{ id: string; data: Partial<WebServiceRestProps> }>
		) {
			state.data = {
				...state.data,
				[action.payload.id]: {
					...state.data[action.payload.id],
					...action.payload.data,
				},
			}
		},
		setAppId(state, action: Action<string>) {
			state.appId = action.payload
		},
		updateSearchString(state, action: Action<string>) {
			state.searchString = action.payload
		},
		openTab(state, action: Action<string>) {
			const list = [...state.openTabIds]
			const index = list.indexOf(action.payload)
			if (index === -1) {
				list.push(action.payload)
			}
			if (isNil(state.tabData[action.payload])) {
				state.tabData = {
					...state.tabData,
					[action.payload]: cloneDeep(tabDataInitial),
				}
			}
			state.openTabIds = list
			state.activeTabId = action.payload
		},
		closeTab(state, action: Action<string>) {
			const openTabIds = [...state.openTabIds]
			const index = openTabIds.indexOf(action.payload)
			if (index !== -1) {
				openTabIds.splice(index, 1)
			}
			if (state.activeTabId === action.payload) {
				if (openTabIds.length > index) {
					state.activeTabId = openTabIds[index]
				} else if (openTabIds.length > 0) {
					state.activeTabId = openTabIds[openTabIds.length - 1]
				} else {
					state.activeTabId = null
				}
			}
			state.tabData = omit(state.tabData, action.payload)
			state.openTabIds = openTabIds
		},
		discardChanges(state, action: Action<string>) {
			const id = action.payload
			const data = { ...state.data }
			data[id] = cloneDeep(state.oldData[id])
			state.data = data
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(
			asyncActions.updateNewService.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					const id = action.meta.arg.id
					const oldData = { ...state.oldData }
					oldData[id] = cloneDeep(state.data[id])
					state.oldData = oldData
				}
			}
		)
		builder.addCase(asyncActions.postNewService.fulfilled, (state, action) => {
			if (action && action.payload) {
				const oldId = action.meta.arg.id
				const newId = action.payload.id
				const data = { ...state.data }
				data[newId] = { ...state.data[oldId], id: newId, isNew: false }
				delete data[oldId]
				state.data = data
				const oldData = { ...state.oldData }
				oldData[newId] = cloneDeep(data[newId])
				state.oldData = oldData
				const openTabIds = [...state.openTabIds]
				const i = openTabIds.indexOf(oldId)
				if (i !== -1) {
					openTabIds[i] = newId
				}
				state.openTabIds = openTabIds
				if (state.activeTabId === oldId) {
					state.activeTabId = newId
				}
				const tabData = { ...state.tabData }
				tabData[newId] = { ...state.tabData[oldId] }
				delete tabData[oldId]
				state.tabData = tabData
				const listOrder = [...state.listOrder]
				const index = listOrder.indexOf(oldId)
				if (index !== -1) {
					listOrder[index] = newId
				}
				state.listOrder = listOrder
			}
		})
		builder.addCase(
			asyncActions.deleteWebService.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					const id = action.meta.arg.id
					const data = { ...state.data }
					delete data[id]
					state.data = data
					const oldData = { ...state.oldData }
					delete oldData[id]
					state.oldData = oldData
					const openTabIds = [...state.openTabIds]
					const i = openTabIds.indexOf(id)
					if (i !== -1) {
						openTabIds.splice(i, 1)
					}
					state.openTabIds = openTabIds
					if (state.activeTabId === id) {
						if (openTabIds.length > i) {
							state.activeTabId = openTabIds[i]
						} else if (openTabIds.length > 0) {
							state.activeTabId = openTabIds[openTabIds.length - 1]
						} else {
							state.activeTabId = null
						}
					}
					const tabData = { ...state.tabData }
					delete tabData[id]
					state.tabData = tabData
					const listOrder = [...state.listOrder]
					const index = listOrder.indexOf(id)
					if (index !== -1) {
						listOrder.splice(index, 1)
					}
					state.listOrder = listOrder
				}
			}
		)
		builder.addCase(
			asyncActions.loadWebServiceList.fulfilled,
			(state, action) => {
				if (action && action.payload && isArray(action.payload.content)) {
					const data: WebServiceRestDataMap = {}
					const list: string[] = []
					for (let i = 0, iLen = action.payload.content.length; i < iLen; i++) {
						const id = action.payload.content[i].id
						data[id] = {
							...setDataTOUiStandards(action.payload.content[i]),
							isNew: false,
						}
						list.push(id)
					}
					state.data = data
					state.listOrder = list
					state.oldData = cloneDeep(data)
				}
			}
		)
		builder.addCase(
			asyncActions.loadRequestMethodsLookUps.fulfilled,
			(state, action) => {
				if (
					action &&
					isArray(action.payload) &&
					action.payload.length > 0 &&
					isArray(action.payload[0].configuration)
				) {
					const data: WebServiceRequestMethodsLookUpProps[] = []
					for (
						let i = 0, iLen = action.payload[0].configuration.length;
						i < iLen;
						i++
					) {
						data.push({
							id: action.payload[0].configuration[i].name,
							name: action.payload[0].configuration[i].displayName,
						})
					}
					state.requestMethodsLookUps = data
				}
			}
		)
		builder.addCase(
			asyncActions.loadRequestBodyLookUps.fulfilled,
			(state, action) => {
				if (
					action &&
					isArray(action.payload) &&
					action.payload.length > 0 &&
					isArray(action.payload[0].configuration)
				) {
					const data: WebServiceRequestBodyLookUpProps[] = []
					for (
						let i = 0, iLen = action.payload[0].configuration.length;
						i < iLen;
						i++
					) {
						data.push({
							id: action.payload[0].configuration[i].name,
							name: action.payload[0].configuration[i].displayName,
						})
					}
					state.requestBodyLookUps = data
				}
			}
		)
		builder.addCase(
			asyncActions.loadAuthorizationLookUps.fulfilled,
			(state, action) => {
				if (
					action &&
					isArray(action.payload) &&
					action.payload.length > 0 &&
					isArray(action.payload[0].configuration)
				) {
					state.authorizationLookUps = action.payload[0].configuration
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
