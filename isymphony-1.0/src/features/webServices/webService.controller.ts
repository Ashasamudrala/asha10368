import { createAsyncThunk } from '@reduxjs/toolkit'
import { isEqual, isNil } from 'lodash'
import { RootState } from '../../base.types'
import { AddDialogProps, DialogTypes, showDialog } from '../dialogs'
import { SelectedThirdServiceReturnProps } from '../dialogs/selectThirdPartyService/selectThirdPartyService.types'
import {
	loadRequestMethodsLookUps,
	loadWebServiceList,
	loadAuthorizationLookUps,
	loadRequestBodyLookUps,
	deleteWebService,
} from './webService.asyncActions'
import {
	getOldWebServiceItemById,
	getWebServiceItemById,
} from './webService.selectors'
import { actions } from './webService.slice'
import { WebServiceRestTabValidationStatus } from './webService.types'
import { sendRequest } from './webService.utilities'
import { actions as notificationActions } from '../../infrastructure/notificationPopup/notificationPopup.slice'
import { ConfirmationDataProps } from '../dialogs/confirmation/confirmation.types'
import i18n from 'i18next'
import {
	COMMON_DISCARD_CHANGES,
	WEB_SERVICE_DISCARD_CHANGES_CONFORMATION,
	WEB_SERVICE_DISCARD_AND_DELETE_CHANGES_CONFORMATION,
} from '../../utilities/constants'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { selectGetWebServicePreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'

export const initWebService = createAsyncThunk(
	'webService/initWebService',
	async (id: string, { dispatch }) => {
		dispatch(actions.setAppId(id))
		dispatch(loadWebServiceList())
		dispatch(loadRequestMethodsLookUps())
		dispatch(loadAuthorizationLookUps())
		dispatch(loadRequestBodyLookUps())
	}
)

export const closeTabConformation = createAsyncThunk(
	'webServices/openAddDialog',
	async (id: string, thunkArgs) => {
		const service = getWebServiceItemById(thunkArgs.getState() as RootState, id)
		const oldService = getOldWebServiceItemById(
			thunkArgs.getState() as RootState,
			id
		)
		if (isNil(service)) {
			return
		}
		const handleOkay = () => {
			if (service.isNew) {
				thunkArgs.dispatch(deleteWebService(service))
			} else {
				thunkArgs.dispatch(actions.discardChanges(id))
				thunkArgs.dispatch(actions.closeTab(id))
			}
		}
		if (service.isNew) {
			return await thunkArgs.dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					title: i18n.t(COMMON_DISCARD_CHANGES),
					data: {
						message: i18n.t(
							WEB_SERVICE_DISCARD_AND_DELETE_CHANGES_CONFORMATION,
							{
								name: service.name,
							}
						),
					} as ConfirmationDataProps,
					onOkay: handleOkay,
				} as AddDialogProps)
			)
		} else if (!isEqual(service, oldService)) {
			return await thunkArgs.dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					title: i18n.t(COMMON_DISCARD_CHANGES),
					data: {
						message: i18n.t(WEB_SERVICE_DISCARD_CHANGES_CONFORMATION, {
							name: service.name,
						}),
					} as ConfirmationDataProps,
					onOkay: handleOkay,
				} as AddDialogProps)
			)
		} else {
			thunkArgs.dispatch(actions.closeTab(id))
		}
	}
)

export const addThirdPartyServices = createAsyncThunk(
	'webServices/openAddDialog',
	async (data: { updateStepInfo: (stepNumber: number) => void }, thunkArgs) => {
		const webServicePreferenceData = selectGetWebServicePreferenceStatus(
			thunkArgs.getState() as RootState
		)
		const handleOkay = (serviceType: SelectedThirdServiceReturnProps) => {
			thunkArgs.dispatch(actions.addNew(serviceType))
			data.updateStepInfo(2)
		}
		const handleCancel = () => {
			thunkArgs.dispatch(
				preferenceActions.updateWebServicesPreferences({
					status: HelpModuleStatus.INCOMPLETE,
				})
			)
			thunkArgs.dispatch(updatePreferences())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.SELECT_WEB_SERVICE,
				className:
					webServicePreferenceData.status === HelpModuleStatus.INPROGRESS &&
					'isy-steps-create-web-service-dialog',
				onOkay: handleOkay,
				onCancel: handleCancel,
			} as AddDialogProps)
		)
	}
)

export const sendWebServiceRequest = createAsyncThunk(
	'webService/sendWebServiceRequest',
	async (
		data: { id: string; updateStepInfo: (stepNumber: number) => void },
		{ dispatch, getState }
	) => {
		const id = data.id
		const updateStepInfo = data.updateStepInfo
		const service = getWebServiceItemById(getState() as RootState, id)
		if (isNil(service)) {
			return
		}
		dispatch(
			actions.updateTabData({
				id,
				data: { validationStatus: WebServiceRestTabValidationStatus.LOADING },
			})
		)
		sendRequest(service)
			.then((data) => {
				dispatch(
					actions.updateTabData({
						id,
						data: {
							validationStatus: WebServiceRestTabValidationStatus.DONE,
							response: data,
						},
					})
				)
				updateStepInfo(7)
			})
			.catch((error) => {
				dispatch(notificationActions.notifyError(error))
				dispatch(
					actions.updateTabData({
						id,
						data: { validationStatus: WebServiceRestTabValidationStatus.NONE },
					})
				)
				updateStepInfo(7)
			})
	}
)
