import React, { useState } from 'react'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { IsyAvatarImage } from '../../../../../widgets/IsyAvatarImage/IsyAvatarImage'
import {
	APPS_TRANSLATIONS,
	LAST_MODIFIED,
	SETTINGS,
	OWNER,
} from '../../../../../utilities/constants'
import { IsyPopover } from '../../../../../widgets/IsyPopover/IsyPopover'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import IconButton from '@material-ui/core/IconButton'
import './appCard.scss'
import { AppState } from '../../../apps.types'
import { isNil } from 'lodash'

export interface AppCardProps {
	data: AppState
	onEditApp: (appId: string) => void
	onItemSelect: (data: AppState) => void
}

export function AppCard(props: AppCardProps) {
	const { data, onEditApp } = props
	const { t } = useTranslation(APPS_TRANSLATIONS)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)
	const lastModifiedDate = moment(data.lastModifiedTimestamp).format('ll')

	const handleOnClickMoreVertIcon = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>
	) => {
		e.stopPropagation()
		setPopoverAnchorEl(e.currentTarget)
	}

	const handlePopoverClose = (e: any) => {
		e.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleMenuItems = (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		selectedItem: string
	) => {
		if (selectedItem === 'settings') {
			onEditApp(data.id)
		}
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const popoverActions = [
		{
			name: t(SETTINGS),
			key: 'settings',
		},
	]

	const handleOnClick = () => {
		props.onItemSelect(data)
	}

	const renderPopOver = () => {
		if (isNil(popoverAnchorEl)) {
			return
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={popoverActions}
				onSelect={handleMenuItems}
				className={'pop-over-menu-list'}
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
			/>
		)
	}

	const renderCardChip = () => {
		return <span className={'type-chip'}>{data.type}</span>
	}

	const renderCardDescription = () => {
		return (
			<CardContent className='card-content'>
				<span>{data.description}</span>
			</CardContent>
		)
	}

	const renderCardHeader = () => {
		return (
			<CardHeader
				className='card-header'
				title={data.name}
				subheader={`${t(LAST_MODIFIED)} : ${lastModifiedDate}`}
				classes={{ subheader: 'sub-header', title: 'title' }}
			/>
		)
	}

	const renderAvatarImage = () => {
		return (
			<IsyAvatarImage
				imageClassName={'profile-avatar'}
				defaultName={data.createdByUserDisplayName}
				imageData={data.createdByUserProfilePicture}
			/>
		)
	}

	const renderActions = () => {
		return (
			<>
				<IconButton className='more-vertical-icon'>
					<MoreVertIcon onClick={handleOnClickMoreVertIcon} />
				</IconButton>
				{renderPopOver()}
			</>
		)
	}

	const renderOwner = () => {
		return (
			<>
				<span className='owner-header'>{t(OWNER) + ' : '}</span>
				<span className='owner-name' title={data.createdByUserDisplayName}>
					{data.createdByUserDisplayName}
				</span>
			</>
		)
	}

	const renderCardFooter = () => {
		return (
			<CardHeader
				className='card-footer'
				avatar={renderAvatarImage()}
				action={renderActions()}
				title={renderOwner()}
				classes={{
					avatar: 'footer-avatar',
					action: 'card-actions',
					title: 'owner-holder',
				}}
			/>
		)
	}

	const renderCardContent = () => {
		return (
			<div className='app-card-header-content'>
				{renderCardChip()}
				{renderCardHeader()}
				{renderCardDescription()}
			</div>
		)
	}

	const renderCard = () => {
		return (
			<Card className='app-card' onClick={handleOnClick}>
				{renderCardContent()}
				{renderCardFooter()}
			</Card>
		)
	}

	return renderCard()
}
