import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
// import { IsyDropDown } from '../../../widgets/IsyDropDown/IsyDropDown'
import {
	APPS_TRANSLATIONS,
	RECORDS_PER_PAGE,
	// ALL_CHANNELS,
	// MOBILE,
	// WEB,
	DESCENDING,
	LAST_MODIFIED,
	APP_NAME,
	ASCENDING,
	DATE_CREATED,
} from '../../../utilities/constants'
import { CardViewIcon } from '../../../icons/CardView'
import { IsyPopover } from '../../../widgets/IsyPopover/IsyPopover'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import { actions } from '../apps.slice'
import CheckIcon from '@material-ui/icons/Check'
import DehazeIcon from '@material-ui/icons/Dehaze'
import {
	// AppsFilterBy,
	OrderBy,
	AppsSortBy,
} from '../../../utilities/apiEnumConstants'
import { fetchAllApps } from '../apps.asyncActions'
import {
	selectOrderBy,
	selectSortBy,
	// selectFilterByType,
	selectRecordsPerPage,
	selectCurrentPage,
	selectViewType,
	selectTotalCount,
} from '../apps.selectors'
import './appsBar.scss'
import {
	// AppsFilterByUi,
	AppsViewType,
} from '../apps.types'
import { isNil } from 'lodash'
import { IsyPagination } from '../../../widgets/IsyPagination/IsyPagination'

export function AppsBar() {
	const { t } = useTranslation(APPS_TRANSLATIONS)
	const orderBy = useSelector(selectOrderBy)
	const sortBy = useSelector(selectSortBy)
	// const filterByType = useSelector(selectFilterByType)
	const recordsPerPage = useSelector(selectRecordsPerPage)
	const currentPage = useSelector(selectCurrentPage)
	const viewType = useSelector(selectViewType)
	const totalCount = useSelector(selectTotalCount)

	const [anchorEl, setAnchorEl] = useState<Element | null>(null)

	const dispatch = useDispatch()
	// const DropdownOptions = [
	// 	{
	// 		id: AppsFilterByUi.ALL_CHANNELS,
	// 		name: t(ALL_CHANNELS),
	// 	},
	// 	{
	// 		id: AppsFilterBy.WEB,
	// 		name: t(WEB),
	// 	},
	// 	{
	// 		id: AppsFilterBy.MOBILE,
	// 		name: t(MOBILE),
	// 	},
	// ]

	const sortOptions = () => [
		{
			key: AppsSortBy.LAST_MODIFIED,
			hasIcon: sortBy === AppsSortBy.LAST_MODIFIED,
			icon: <CheckIcon />,
			name: t(LAST_MODIFIED),
		},
		{
			key: AppsSortBy.CREATED_ON,
			hasIcon: sortBy === AppsSortBy.CREATED_ON,
			icon: <CheckIcon />,
			name: t(DATE_CREATED),
		},
		{
			key: AppsSortBy.NAME,
			hasIcon: sortBy === AppsSortBy.NAME,
			icon: <CheckIcon />,
			name: t(APP_NAME),
			menuClass: 'border-bottom',
		},
		{
			key: OrderBy.ASC,
			hasIcon: orderBy === OrderBy.ASC,
			icon: <CheckIcon />,
			name: t(ASCENDING),
		},
		{
			key: OrderBy.DESC,
			hasIcon: orderBy === OrderBy.DESC,
			icon: <CheckIcon />,
			name: t(DESCENDING),
		},
	]

	const getSortByDisplayName = () => {
		switch (sortBy) {
			case AppsSortBy.LAST_MODIFIED:
				return t(LAST_MODIFIED)
			case AppsSortBy.CREATED_ON:
				return t(DATE_CREATED)
			case AppsSortBy.NAME:
				return t(APP_NAME)
			default:
				return null
		}
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setAnchorEl(null)
		dispatch(fetchAllApps())
	}

	const handlePopoverShow = (event: any) => {
		setAnchorEl(event.currentTarget)
	}

	const handleMenuItems = (_: any, key: any) => {
		key = key as OrderBy | AppsSortBy
		switch (key) {
			case OrderBy.ASC:
			case OrderBy.DESC:
				dispatch(actions.setOrderBy(key))
				break
			default:
				dispatch(actions.setSortBy(key))
		}
	}

	// const handleOnChangeFilterByType = (value: AppsFilterByUi | AppsFilterBy) => {
	// 	dispatch(actions.setFilterByType(value))
	// 	dispatch(fetchAllApps())
	// }

	const handleChangeRowsPerPage = (value: number) => {
		dispatch(actions.setRecordsPerPage(value))
		dispatch(fetchAllApps())
	}

	const handleChangePage = (page: number) => {
		dispatch(actions.setCurrentPage(page))
		dispatch(fetchAllApps())
	}

	const handleViewTypeChange = (type: AppsViewType) => {
		dispatch(actions.setViewType(type))
		dispatch(fetchAllApps())
	}

	// const renderFilterBy = () => {
	// 	return (
	// 		<IsyDropDown<AppsFilterByUi | AppsFilterBy>
	// 			options={DropdownOptions}
	// 			value={filterByType}
	// 			className='input-base'
	// 			onChange={handleOnChangeFilterByType}
	// 		/>
	// 	)
	// }

	const renderPagination = () => {
		return (
			<IsyPagination
				labelName={t(RECORDS_PER_PAGE)}
				totalCount={totalCount}
				currentPage={currentPage}
				rowsPerPageOptions={[12, 24, 36, 48, 60]}
				recordsPerPage={recordsPerPage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
				onChangePage={handleChangePage}
				className='pagination-tool-bar'
			/>
		)
	}

	const renderPopover = () => {
		if (isNil(anchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={anchorEl}
				actions={sortOptions()}
				className={'app-bar-popover'}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
			/>
		)
	}

	const renderSortOptions = () => {
		return (
			<>
				<button onClick={handlePopoverShow} className='pop-btn'>
					{getSortByDisplayName()}{' '}
					{orderBy === OrderBy.DESC ? (
						<ArrowDownwardIcon />
					) : (
						<ArrowUpwardIcon />
					)}
				</button>
				{renderPopover()}
			</>
		)
	}

	const renderViewByOptions = () => {
		return (
			<>
				<span className='border-right'></span>
				<span
					className={
						'list-view ' + (viewType === AppsViewType.LIST ? 'select-icon' : '')
					}
					onClick={() => handleViewTypeChange(AppsViewType.LIST)}
				>
					<DehazeIcon />
				</span>
				<span className='border-right'></span>
				<span
					className={
						'card-view ' + (viewType === AppsViewType.GRID ? 'select-icon' : '')
					}
					onClick={() => handleViewTypeChange(AppsViewType.GRID)}
				>
					<CardViewIcon className='icon' />
				</span>
			</>
		)
	}

	return (
		<div className='app-bar'>
			{/* {renderFilterBy()} */}
			<span className='flex-grow'></span>
			{renderPagination()}
			{viewType === AppsViewType.GRID && renderSortOptions()}
			{renderViewByOptions()}
		</div>
	)
}
