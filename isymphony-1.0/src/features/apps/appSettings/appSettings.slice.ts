import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import * as asyncActions from './appSettings.asyncActions'
import { isNil, isEmpty, set } from 'lodash'
import {
	AppSettingsState,
	SettingsCurrentState,
	AppSettingState,
	AppSettingErrorState,
	PlatformOptionsType,
	FrameworkOptionsType,
} from './appSettings.types'
import { Action } from '../../../common.types'
import { AppType } from '../../../utilities/apiEnumConstants'
import { AppsSubReducersNames } from '../base.types'
import { APP_NAME_INVALID } from '../../../utilities/constants'
import i18n from 'i18next'

const defaultAppDetails: AppSettingState = {
	name: '',
	description: '',
	type: AppType.WEB,
	platformAndFrameworkSelection: {
		platformId_platformVersion: '',
		frameworkId_frameworkVersion: '',
	},
	metadata: {
		groupId: '',
		artifactId: '',
		version: '',
		packageName: '',
	},
	copyright:
		'Copyright (c) 2020-2021 Innominds inc. All Rights Reserved. This software is the confidential and proprietary information of Innominds inc. You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the source code license agreement you entered into with Innominds inc.',
}
const initialState: AppSettingsState = {
	appDetails: defaultAppDetails,
	currentState: SettingsCurrentState.NONE,
	platformOptions: [],
	errorList: {},
	applicationId: null,
}

const slice = createSlice<
	AppSettingsState,
	SliceCaseReducers<AppSettingsState>,
	AppsSubReducersNames.APP_SETTINGS
>({
	name: AppsSubReducersNames.APP_SETTINGS,
	initialState,
	reducers: {
		setCurrentState(
			state: AppSettingsState,
			action: Action<SettingsCurrentState>
		) {
			state.currentState = action.payload
		},
		updateAppData(state: AppSettingsState, action: Action<AppSettingState>) {
			state.appDetails = Object.assign({}, state.appDetails, action.payload)
		},
		updateErrorList(
			state: AppSettingsState,
			action: Action<AppSettingErrorState>
		) {
			state.errorList = action.payload
		},
		updateAppId(state: AppSettingsState, action: Action<string>) {
			state.applicationId = action.payload
		},
		updateMetadata(state: AppSettingsState, action: Action<AppSettingState>) {
			let processedAppData = state.appDetails
			processedAppData = set(processedAppData, 'metadata', {
				...processedAppData.metadata,
				...action.payload,
			})
			state.appDetails = processedAppData
		},
		clearData: () => initialState,
	},
	// asynchronous actions
	extraReducers: (builder) => {
		builder
			.addCase(
				asyncActions.fetchAllPlatforms.fulfilled,
				(state: AppSettingsState, action) => {
					if (action.payload) {
						const processedPlatformData: PlatformOptionsType[] = []
						action.payload.content.forEach(
							(platformData: {
								versions: string[]
								frameworks: { [s: string]: unknown } | ArrayLike<unknown>
								id: string
								name: string
							}) => {
								let frameworkOptionsData: FrameworkOptionsType[] = []
								platformData.versions.forEach((version: string) => {
									frameworkOptionsData = []
									Object.values(platformData.frameworks).forEach(
										(frameworkData: any) => {
											frameworkData.versions.forEach(
												(frameworkVersion: {
													supportedPlatformVersions: string | string[]
													versionName: string
												}) => {
													if (
														frameworkVersion.supportedPlatformVersions.includes(
															version
														)
													) {
														frameworkOptionsData.push({
															id: `${frameworkData.id}_${frameworkVersion.versionName}`,
															name: `${frameworkData.name}(${frameworkVersion.versionName})`,
														})
													}
												}
											)
										}
									)
									processedPlatformData.push({
										id: `${platformData.id}_${version}`,
										name: `${platformData.name}(${version})`,
										frameworkOptions: frameworkOptionsData,
									})
								})
							}
						)
						state.platformOptions = processedPlatformData
						state.appDetails.platformAndFrameworkSelection = {
							platformId_platformVersion: processedPlatformData[0].id,
							frameworkId_frameworkVersion:
								processedPlatformData[0].frameworkOptions.length !== 0
									? processedPlatformData[0].frameworkOptions[0].id
									: '',
						}
					}
				}
			)
			.addCase(
				asyncActions.saveApplication.fulfilled,
				(state: AppSettingsState, action) => {
					if (!isNil(action.payload)) {
						if (state.applicationId === null) {
							state.currentState = SettingsCurrentState.SUCCESS_PAGE_CREATE
							state.applicationId = action.payload.id
						} else {
							state.currentState = SettingsCurrentState.SUCCESS_PAGE_UPDATE
						}
					}
				}
			)
			.addCase(
				asyncActions.getApplicationById.fulfilled,
				(state: AppSettingsState, action) => {
					if (!isNil(action.payload)) {
						const obj = {
							name: action.payload.name,
							description: action.payload.description || '',
							type: action.payload.type,
							platformAndFrameworkSelection: {
								platformId_platformVersion:
									action.payload.platform.id +
									'_' +
									action.payload.platform.version,
								frameworkId_frameworkVersion:
									action.payload.framework.id +
									'_' +
									action.payload.framework.version,
							},
							metadata: { ...action.payload.metadata },
							copyright: action.payload.customer.copyright.template,
						}
						state.appDetails = Object.assign({}, state.appDetails, obj)
					}
				}
			)
			.addCase(
				asyncActions.validateAppName.fulfilled,
				(state: AppSettingsState, action) => {
					if (isEmpty(action.payload)) {
						let errorList = state.errorList
						if (errorList.name) {
							delete state.errorList.name
						}
					} else {
						state.errorList = Object.assign({}, state.errorList, {
							name: i18n.t(APP_NAME_INVALID, { name: state.appDetails.name }),
						})
					}
				}
			)
	},
})

export default slice

export const { name, actions, reducer } = slice
