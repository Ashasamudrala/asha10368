import React, { useEffect } from 'react'
import Drawer from '@material-ui/core/Drawer'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import { Typography } from '@material-ui/core'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import PhoneAndroidOutlinedIcon from '@material-ui/icons/PhoneAndroidOutlined'
import LanguageOutlinedIcon from '@material-ui/icons/LanguageOutlined'
import { useDispatch, useSelector } from 'react-redux'
import {
	getCurrentState,
	getAppDetails,
	getAppId,
	selectedErrorList,
	getPlatformOptions,
	getFrameworkDetails,
} from './appSettings.selectors'
import { debounce, isEmpty, isNil } from 'lodash'
import { useTranslation } from 'react-i18next'
import {
	APP_CREATE_TEXT,
	APP_WEB,
	APP_MOBILE,
	BUTTON_SAVE,
	BUTTON_CANCEL,
	APP_CREATE,
	APP_SUCCESS_MESSAGE,
	APP_OPEN_BTN,
	APP_SUCCESS_UPDATE_MESSAGE,
	APP_SETTINGS_TITLE,
	APP_CREATE_NAME,
	APP_NAME_PLACEHOLDER,
	APP_DESCRIPTION,
	APP_DESCRIPTION_PLACEHOLDER,
	APP_PLATFORM,
	APP_COPYRIGHT,
	APP_PACKAGE_NAME_PLACEHOLDER,
	APP_PACKAGE_NAME,
	APP_VERSION,
	APP_VERSION_PLACEHOLDER,
	APP_ARTIFACT_ID_PLACEHOLDER,
	APP_GROUP_ID,
	APP_GROUP_ID_PLACEHOLDER,
	APP_FRAMEWORK,
	APP_NAME_EMPTY,
	APP_NAME_INVALID_RANGE,
	APP_FRAMEWORK_EMPTY,
	APP_GROUP_ID_EMPTY,
	APP_ARTIFACT_ID_EMPTY,
	APP_PACKAGE_NAME_EMPTY,
	APP_PACKAGE_NAME_REG_ERROR,
	APP_VERSION_EMPTY,
	APP_ARTIFACT_ID,
	MAVEN,
	APP_DETAILS,
	APPS_TRANSLATIONS,
	VALIDATE_VERSION_PATTERN,
	APP_GROUP_ID_REG_ERROR,
	APP_ARTIFACT_ID_REG_ERROR,
	APP_PACKAGE_NAME_GROUP_ID_ERROR,
} from '../../../utilities/constants'
import {
	getPlatformsOnload,
	saveApplication,
	validateAppName,
} from './appSettings.asyncActions'
import CircularProgress from '@material-ui/core/CircularProgress'
import DoneOutlinedIcon from '@material-ui/icons/DoneOutlined'
import { makeStyles } from '@material-ui/core/styles'
import { actions } from './appSettings.slice'
import {
	SettingsCurrentState,
	AppSettingErrorState,
	AppSettingState,
} from './appSettings.types'
import { AppType } from '../../../utilities/apiEnumConstants'
import './appSettings.scss'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { fetchAllApps } from '../apps.asyncActions'
import { CustomButtonType } from '../../../widgets/IsySteps/IsyTour/Steps'

const useStyles = makeStyles({
	paper: {
		width: 450,
		boxSizing: 'border-box',
		backgroundColor: 'white',
		border: '1px solid #dedede',
	},
})

export interface AppSettingProps {
	onGoToApplicationView: (appId: string) => void
	updateSteps: (stepPosition: number, type?: string) => void
	appsTourEnable: boolean
}

export default function AppSettings(props: AppSettingProps) {
	const configAppData = useSelector(getAppDetails)
	const appId = useSelector(getAppId)
	const errorList = useSelector(selectedErrorList)
	const currentState = useSelector(getCurrentState)
	const platformOptions = useSelector(getPlatformOptions)
	const frameworkOptionsData = useSelector(getFrameworkDetails)

	const { t } = useTranslation(APPS_TRANSLATIONS)
	const classes = useStyles()
	const dispatch = useDispatch()

	useEffect(() => {
		if (currentState === SettingsCurrentState.TABS_PAGE) {
			dispatch(getPlatformsOnload())
		}
	}, [currentState, dispatch])

	const getFormBuilderConfig = () => {
		const config: IsyFormBuilderFormItemProps[] = [
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				title: t(APP_DETAILS),
				forms: [
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(APP_CREATE_NAME),
						dataRef: 'name',
						placeholder: t(APP_NAME_PLACEHOLDER),
						autoFocus: true,
						isRequired: true,
						disabled: !isNil(appId),
						onChange: handleAppNameChange,
					},
					{
						type: IsyFormBuilderFormTypes.TEXT_AREA,
						title: t(APP_DESCRIPTION),
						dataRef: 'description',
						className: 'app-description',
						placeholder: t(APP_DESCRIPTION_PLACEHOLDER),
					},
					{
						type: IsyFormBuilderFormTypes.SELECT,
						title: t(APP_PLATFORM),
						isRequired: true,
						dataRef: 'platformAndFrameworkSelection.platformId_platformVersion',
						options: platformOptions,
						onChange: handlePlatformChange,
					},
					{
						type: IsyFormBuilderFormTypes.SELECT,
						title: t(APP_FRAMEWORK),
						isRequired: true,
						dataRef:
							'platformAndFrameworkSelection.frameworkId_frameworkVersion',
						options: frameworkOptionsData,
					},
					{
						type: IsyFormBuilderFormTypes.TEXT_AREA,
						title: t(APP_COPYRIGHT),
						dataRef: 'copyright',
					},
				],
			},
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				title: t(MAVEN),
				dataRef: 'metadata',
				forms: [
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(APP_GROUP_ID),
						dataRef: 'groupId',
						placeholder: t(APP_GROUP_ID_PLACEHOLDER),
						isRequired: true,
						onChange: handleAppGroupIdChange,
					},
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(APP_ARTIFACT_ID),
						dataRef: 'artifactId',
						placeholder: t(APP_ARTIFACT_ID_PLACEHOLDER),
						isRequired: true,
						onChange: handleAppArtifactIdChange,
					},
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(APP_VERSION),
						dataRef: 'version',
						placeholder: t(APP_VERSION_PLACEHOLDER),
						isRequired: true,
						onChange: handleAppVersionChange,
					},
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(APP_PACKAGE_NAME),
						dataRef: 'packageName',
						placeholder: t(APP_PACKAGE_NAME_PLACEHOLDER),
						isRequired: true,
						onChange: handlePackageNameChange,
					},
				],
			},
		]
		return config
	}

	const isFieldEmpty = (value: string) => {
		return isEmpty(value)
	}

	const isValidVersion = (version: string) => {
		const versionValidationRegex = /[a-zA-Z~`!#$%&*+=\-\]\\';,/{}|\\":<>()]/
		return versionValidationRegex.test(version)
	}

	const handleValidation = () => {
		const fields = { ...configAppData }
		const errors: AppSettingErrorState = {}

		if (isFieldEmpty(fields.name)) {
			errors.name = t(APP_NAME_EMPTY)
		} else if (!(fields.name.length >= 3 && fields.name.length <= 64)) {
			errors.name = t(APP_NAME_INVALID_RANGE)
		} else if (!isFieldEmpty(fields.name)) {
			// whenever duplicate is present setting to name to duplicate
			if (errorList.name) {
				errors.name = errorList.name
			}
		}

		if (
			isFieldEmpty(
				fields.platformAndFrameworkSelection.frameworkId_frameworkVersion
			)
		) {
			errors.platformAndFrameworkSelection = {
				frameworkId_frameworkVersion: t(APP_FRAMEWORK_EMPTY),
			}
		}

		const groupIdRegex = new RegExp('^[a-z]+(\\.[a-z]+)+[a-z]$')
		if (isFieldEmpty(fields.metadata.groupId)) {
			errors.metadata = { ...errors.metadata, groupId: t(APP_GROUP_ID_EMPTY) }
		} else if (!groupIdRegex.test(fields.metadata.groupId)) {
			errors.metadata = {
				...errors.metadata,
				groupId: t(APP_GROUP_ID_REG_ERROR),
			}
		}

		const artifactIdRegex = new RegExp('^[a-z]+(\\-*[a-z])*[a-z]$')
		if (isFieldEmpty(fields.metadata.artifactId)) {
			errors.metadata = {
				...errors.metadata,
				artifactId: t(APP_ARTIFACT_ID_EMPTY),
			}
		} else if (!artifactIdRegex.test(fields.metadata.artifactId)) {
			errors.metadata = {
				...errors.metadata,
				artifactId: t(APP_ARTIFACT_ID_REG_ERROR),
			}
		}

		if (isFieldEmpty(fields.metadata.version)) {
			errors.metadata = { ...errors.metadata, version: t(APP_VERSION_EMPTY) }
		}

		if (isValidVersion(fields.metadata.version)) {
			errors.metadata = {
				...errors.metadata,
				version: t(VALIDATE_VERSION_PATTERN),
			}
		}

		const packageNameRegex = new RegExp('^[a-z]+(\\.[a-z]+)+[a-z]$')
		if (isFieldEmpty(fields.metadata.packageName)) {
			errors.metadata = {
				...errors.metadata,
				packageName: t(APP_PACKAGE_NAME_EMPTY),
			}
		} else if (!packageNameRegex.test(fields.metadata.packageName)) {
			errors.metadata = {
				...errors.metadata,
				packageName: t(APP_PACKAGE_NAME_REG_ERROR),
			}
		} else if (
			fields.metadata.packageName.indexOf(fields.metadata.groupId) !== 0
		) {
			errors.metadata = {
				...errors.metadata,
				packageName: t(APP_PACKAGE_NAME_GROUP_ID_ERROR),
			}
		}
		return errors
	}

	const handleClose = () => {
		if (
			props.appsTourEnable &&
			SettingsCurrentState.TABS_PAGE === currentState
		) {
			props.updateSteps(8)
		} else if (
			props.appsTourEnable &&
			SettingsCurrentState.LANDING === currentState
		) {
			props.updateSteps(5, CustomButtonType.EXIT)
		} else if (
			props.appsTourEnable &&
			SettingsCurrentState.SUCCESS_PAGE_CREATE === currentState
		) {
			props.updateSteps(7, CustomButtonType.EXIT)
		} else {
			dispatch(actions.clearData(undefined))
		}
	}

	const handleOpenApp = () => {
		dispatch(actions.clearData(undefined))
		if (appId !== null) {
			props.onGoToApplicationView(appId)
		}
	}

	const handleAppNameDebounce = debounce(() => {
		dispatch(validateAppName())
	}, 300)

	const handleAppNameChange = (value: string) => {
		dispatch(
			actions.updateAppData({
				name: value.trimStart().replace(/\s{2,}/g, ' '),
			})
		)
		handleAppNameDebounce()
	}

	const handleSave = () => {
		const errors = handleValidation()
		if (Object.keys(errors).length > 0) {
			// has errors
			return dispatch(actions.updateErrorList(errors))
		} else {
			dispatch(actions.setCurrentState(SettingsCurrentState.PROGRESS))
			;(dispatch(saveApplication()) as any).then(() => {
				props.updateSteps(7)
				setTimeout(() => {
					dispatch(fetchAllApps())
				})
			})
		}
	}

	const handlePlatformChange = (value: string) => {
		let frameworkData = ''

		const selectedPlatformData = platformOptions.filter((platformData) => {
			return platformData.id === value
		})

		if (selectedPlatformData[0].frameworkOptions.length > 0) {
			frameworkData = selectedPlatformData[0].frameworkOptions[0].id
		}
		dispatch(
			actions.updateAppData({
				platformAndFrameworkSelection: {
					platformId_platformVersion: value,
					frameworkId_frameworkVersion: frameworkData,
				},
			})
		)
	}

	/**
	 *
	 * @param {*} value
	 * For setting type of application and setting tabs page on click of web button
	 */
	const handleAppCreation = (value: AppType) => {
		dispatch(actions.updateAppData({ type: value }))
		dispatch(actions.setCurrentState(SettingsCurrentState.TABS_PAGE))
		props.updateSteps(6)
	}

	const handleOnChange = (value: AppSettingState) => {
		dispatch(actions.updateAppData(value))
	}

	const handleAppArtifactIdChange = (value: string) => {
		dispatch(
			actions.updateMetadata({
				artifactId: value.trimStart().replace(/\s{2,}/g, ' '),
			})
		)
	}

	const handleAppGroupIdChange = (value: string) => {
		value = value.trimStart().replace(/\s{2,}/g, ' ')
		dispatch(
			actions.updateMetadata({
				groupId: value,
			})
		)
		if ((configAppData.metadata.packageName || '').indexOf(value) !== 0) {
			dispatch(
				actions.updateErrorList({
					...errorList,
					metadata: {
						...errorList.metadata,
						packageName: t(APP_PACKAGE_NAME_GROUP_ID_ERROR),
					},
				})
			)
		} else {
			dispatch(
				actions.updateErrorList({
					...errorList,
					metadata: {
						...errorList.metadata,
						packageName: null,
					},
				})
			)
		}
	}

	const handleAppVersionChange = (value: string) => {
		dispatch(
			actions.updateMetadata({
				version: value.trimStart().replace(/\s{2,}/g, ' '),
			})
		)
	}

	const handlePackageNameChange = (value: string) => {
		value = value.trimStart().replace(/\s{2,}/g, ' ')
		dispatch(
			actions.updateMetadata({
				packageName: value,
			})
		)
		if (value.indexOf(configAppData.metadata.groupId) !== 0) {
			dispatch(
				actions.updateErrorList({
					...errorList,
					metadata: {
						...errorList.metadata,
						packageName: t(APP_PACKAGE_NAME_GROUP_ID_ERROR),
					},
				})
			)
		} else {
			dispatch(
				actions.updateErrorList({
					...errorList,
					metadata: {
						...errorList.metadata,
						packageName: null,
					},
				})
			)
		}
	}

	const renderMiddle = () => {
		return (
			<div className='middle-section'>
				<Typography className='app-create-text'>
					{t(APP_CREATE_TEXT)}
				</Typography>
				<IsyButton
					className='secondary-btn hide-class'
					onClick={() => handleAppCreation(AppType.MOBILE)}
				>
					<PhoneAndroidOutlinedIcon />
					{t(APP_MOBILE)}
				</IsyButton>
				<IsyButton
					className='secondary-btn web-btn'
					onClick={() => handleAppCreation(AppType.WEB)}
				>
					<LanguageOutlinedIcon />
					{t(APP_WEB)}
				</IsyButton>
			</div>
		)
	}

	const renderCreateAppMiddle = () => {
		return (
			<div className='create-app-middle-section'>
				<IsyFormBuilder<AppSettingState>
					forms={getFormBuilderConfig()}
					data={configAppData}
					errors={errorList}
					onChange={handleOnChange}
				/>
			</div>
		)
	}

	const renderHeader = () => {
		return (
			<div className='header-section'>
				<span className='header-title'>{t(APP_CREATE)}</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
				/>
			</div>
		)
	}

	const renderCreateHeader = () => {
		return (
			<div className='header-section'>
				<span className='header-title'>
					{!isNil(appId) ? t(APP_SETTINGS_TITLE) : t(APP_CREATE)}
				</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
				/>
			</div>
		)
	}

	const renderAppBottom = () => {
		return (
			<div className='bottom-section'>
				<IsyButton onClick={handleClose} className='standard-btn'>
					{t(BUTTON_CANCEL)}
				</IsyButton>
				<IsyButton onClick={handleSave} className='primary-btn'>
					{t(BUTTON_SAVE)}
				</IsyButton>
			</div>
		)
	}

	const renderLandingPageDetails = () => {
		return (
			<>
				{renderHeader()}
				{renderMiddle()}
			</>
		)
	}

	const renderDetailsPage = () => {
		return (
			<>
				{renderCreateHeader()}
				{renderCreateAppMiddle()}
				{renderAppBottom()}
			</>
		)
	}

	const renderLoaderPage = () => {
		return (
			<>
				<div className='header-create-section'>
					<CloseOutlinedIcon
						className='header-close-icon'
						onClick={handleClose}
					/>
				</div>
				<div className='create-app-progress'>
					<CircularProgress size={40} />
				</div>
			</>
		)
	}

	const renderAppSuccessPage = (isUpdate: boolean) => {
		return (
			<>
				<div className='header-create-section'>
					<CloseOutlinedIcon
						className='header-close-icon'
						onClick={handleClose}
					/>
				</div>
				<div className='app-success-page'>
					<span className='app' role='button'>
						<DoneOutlinedIcon color='secondary' />
					</span>
					<div className='app-success-text'>
						{isUpdate ? t(APP_SUCCESS_UPDATE_MESSAGE) : t(APP_SUCCESS_MESSAGE)}
					</div>
					<div>
						<IsyButton className='primary-btn open-app' onClick={handleOpenApp}>
							{t(APP_OPEN_BTN)}
						</IsyButton>
					</div>
				</div>
			</>
		)
	}

	const renderPage = () => {
		switch (currentState) {
			case SettingsCurrentState.LANDING:
				return renderLandingPageDetails()
			case SettingsCurrentState.TABS_PAGE:
				return renderDetailsPage()
			case SettingsCurrentState.PROGRESS:
				return renderLoaderPage()
			case SettingsCurrentState.SUCCESS_PAGE_CREATE:
				return renderAppSuccessPage(false)
			case SettingsCurrentState.SUCCESS_PAGE_UPDATE:
				return renderAppSuccessPage(true)
			default:
				return ''
		}
	}

	const renderDrawerContent = () => {
		return (
			<Drawer
				className='default-create-app'
				open={currentState !== SettingsCurrentState.NONE}
				anchor='right'
				elevation={0}
				classes={{ paper: classes.paper + ' isy-create-app' }}
				disablePortal
			>
				{renderPage()}
			</Drawer>
		)
	}
	return renderDrawerContent()
}
