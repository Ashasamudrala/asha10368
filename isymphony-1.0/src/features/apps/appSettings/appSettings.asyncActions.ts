import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import { RootState } from '../../../base.types'
import {
	CREATE_APPLICATION,
	GET_APP_BY_ID,
	UPDATE_APP_BY_ID,
	FIND_BY_NAME,
	GET_ALL_PLATFORMS,
} from '../../../utilities/apiEndpoints'
import { getAppDetails, getAppId } from './appSettings.selectors'
import { isNil } from 'lodash'

export const fetchAllPlatforms = createAsyncThunk(
	'appSettings/getPlatformData',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: GET_ALL_PLATFORMS,
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const saveApplication = createAsyncThunk(
	'appSettings/saveApplication',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const appData = getAppDetails(state)
		const appId = getAppId(state)

		const platformData = appData.platformAndFrameworkSelection.platformId_platformVersion.split(
			'_'
		)
		const frameworkData = appData.platformAndFrameworkSelection.frameworkId_frameworkVersion.split(
			'_'
		)

		let httpMethod: string = 'post'
		let url: string = CREATE_APPLICATION

		if (!isNil(appId)) {
			httpMethod = 'put'
			url = UPDATE_APP_BY_ID(appId)
		}

		return await doAsync({
			url: url,
			httpMethod: httpMethod,
			httpConfig: {
				body: JSON.stringify({
					metadata: appData.metadata,
					name: appData.name,
					description: appData.description,
					type: appData.type,
					platformAndFrameworkSelection: {
						platformId: platformData[0],
						platformVersion: platformData[1],
						frameworkId: frameworkData[0],
						frameworkVersion: frameworkData[1],
					},
					copyright: appData.copyright,
				}),
			},
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const getApplicationById = createAsyncThunk(
	'appSettings/getApplicationById',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const appId = getAppId(state)
		if (isNil(appId)) {
			return
		}
		return await doAsync({
			url: GET_APP_BY_ID(appId),
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const validateAppName = createAsyncThunk(
	'appSettings/validateAppName',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const appData = getAppDetails(state)
		if (appData.name !== '') {
			return await doAsync({
				url: FIND_BY_NAME(appData.name, appData.type),
				noBusySpinner: true,
				...thunkArgs,
			})
		}
	}
)

export const getPlatformsOnload = createAsyncThunk(
	'appSettings/getPlatformsOnload',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const appId = getAppId(state)
		if (isNil(appId)) {
			thunkArgs.dispatch(fetchAllPlatforms())
		} else {
			thunkArgs.dispatch(fetchAllPlatforms()).then(() => {
				thunkArgs.dispatch(getApplicationById())
			})
		}
	}
)
