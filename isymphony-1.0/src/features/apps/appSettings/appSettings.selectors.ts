import { selectSlice as baseSelector } from '../base.selector'
import { RootState } from '../../../base.types'
import {
	AppSettingState,
	SettingsCurrentState,
	AppSettingErrorState,
	PlatformOptionsType,
	FrameworkOptionsType,
	AppSettingsState,
} from './appSettings.types'
import slice from './appSettings.slice'

export const selectSlice = (state: RootState): AppSettingsState => {
	return baseSelector(state)[slice.name]
}

export const getCurrentState = (state: RootState): SettingsCurrentState => {
	return selectSlice(state).currentState
}

export const getPlatformOptions = (state: RootState): PlatformOptionsType[] => {
	return selectSlice(state).platformOptions
}

export const getAppDetails = (state: RootState): AppSettingState => {
	return selectSlice(state).appDetails
}

export const selectedErrorList = (state: RootState): AppSettingErrorState => {
	return selectSlice(state).errorList
}

export const getAppId = (state: RootState): string | null => {
	return selectSlice(state).applicationId
}

export const getFrameworkDetails = (
	state: RootState
): FrameworkOptionsType[] => {
	const platformDetails = selectSlice(state).platformOptions
	const appDetails = selectSlice(state).appDetails
	let selectedPlatformData: PlatformOptionsType[] = []
	platformDetails.forEach((platformData: PlatformOptionsType) => {
		if (
			platformData.id ===
			appDetails.platformAndFrameworkSelection.platformId_platformVersion
		) {
			selectedPlatformData.push(platformData)
		}
	})
	if (selectedPlatformData.length === 0) {
		return []
	}
	return selectedPlatformData[0].frameworkOptions
}
