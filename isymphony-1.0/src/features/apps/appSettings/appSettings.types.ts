import { AppType } from '../../../utilities/apiEnumConstants'

export enum SettingsCurrentState {
	LANDING,
	TABS_PAGE,
	PROGRESS,
	SUCCESS_PAGE_CREATE,
	SUCCESS_PAGE_UPDATE,
	NONE,
}

export type FrameworkOptionsType = {
	id: string
	name: string
}

export type PlatformOptionsType = {
	id: string
	name: string
	frameworkOptions: FrameworkOptionsType[]
}

export type AppSettingState = {
	name: string
	description: string
	type: AppType
	platformAndFrameworkSelection: PlatformAndFrameworkoptions
	metadata: AppsSettingsMetadata
	copyright: string
}

export type PlatformAndFrameworkoptions = {
	platformId_platformVersion: string
	frameworkId_frameworkVersion: string
}

export type AppsSettingsMetadata = {
	groupId: string
	artifactId: string
	version: string
	packageName: string
}

export type AppSettingErrorState = {
	name?: string
	description?: string
	type?: AppType
	platformAndFrameworkSelection?: Partial<PlatformAndFrameworkoptions>
	metadata?: Partial<AppsSettingsMetadata>
	copyright?: string
}

export interface AppSettingsState {
	appDetails: AppSettingState
	currentState: SettingsCurrentState
	platformOptions: PlatformOptionsType[]
	errorList: AppSettingErrorState
	applicationId: string | null
}
