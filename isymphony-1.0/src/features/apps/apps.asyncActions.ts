import { createAsyncThunk } from '@reduxjs/toolkit'
import { RootState } from '../../base.types'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_APPS } from '../../utilities/apiEndpoints'
import {
	selectSearchString,
	selectFilterByType,
	selectOrderBy,
	selectRecordsPerPage,
	selectCurrentPage,
	selectSortBy,
} from './apps.selectors'
import { DialogTypes, showDialog } from '../dialogs'
import { actions } from '../../authAndPermissions/loginUserDetails.slice'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import {
	START_TOUR,
	HELP_DESCRIPTION_SECTION,
	HELP_HEADING,
	SKIP_TOUR,
	HELP_SKIP_DESCRIPTION_SECTION,
	OK_BUTTON_LABEL,
} from '../../utilities/constants'
import i18n from 'i18next'

export const fetchAllApps = createAsyncThunk(
	'apps/getAll',
	async (_: undefined, thunkArgs) => {
		const state = thunkArgs.getState() as RootState
		const searchString = selectSearchString(state)
		const filterByType = selectFilterByType(state)
		const orderBy = selectOrderBy(state)
		const sortBy = selectSortBy(state)
		const recordsPerPage = selectRecordsPerPage(state)
		const currentPage = selectCurrentPage(state)
		return await doAsync({
			url: GET_ALL_APPS(
				searchString,
				filterByType,
				currentPage,
				recordsPerPage,
				sortBy,
				orderBy
			) as string,
			...thunkArgs,
		})
	}
)

export const showHelpLandingPage = createAsyncThunk(
	'isyTour/showLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateAppsPreferences({
					landingPage: false,
					status: HelpModuleStatus.INPROGRESS,
					currentStep: 0,
				})
			)
			thunkArgs.dispatch(updatePreferences())
		}
		const handleCancel = () => {
			thunkArgs.dispatch(showSkipHelpLandingPage())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				onCancel: handleCancel,
				data: {
					message: i18n.t(HELP_DESCRIPTION_SECTION),
					okayButtonLabel: i18n.t(START_TOUR),
					cancelButtonLabel: i18n.t(SKIP_TOUR),
				},
				title: i18n.t(HELP_HEADING),
			})
		)
	}
)

export const showSkipHelpLandingPage = createAsyncThunk(
	'isyTour/showSkipHelpLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateAppsPreferences({
					landingPage: false,
					status: HelpModuleStatus.COMPLETED,
				})
			)
			thunkArgs.dispatch(updatePreferences())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				data: {
					message: i18n.t(HELP_SKIP_DESCRIPTION_SECTION),
					okayButtonLabel: i18n.t(OK_BUTTON_LABEL),
				},
				title: i18n.t(HELP_HEADING),
			})
		)
	}
)
