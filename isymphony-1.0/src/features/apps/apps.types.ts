import {
	AppsSortBy,
	OrderBy,
	AppsFilterBy,
} from '../../utilities/apiEnumConstants'

export enum AppsViewType {
	GRID = 'GRID',
	LIST = 'LIST',
}

export enum AppsFilterByUi {
	ALL_CHANNELS = 'ALL_CHANNELS',
}

export type AppState = {
	createdBy: string
	id: string
	lastModifiedTimestamp: number
	createdByUserDisplayName: string
	type: string
	description: string
	name: string
	createdByUserProfilePicture: string
}

export interface AppsState {
	allApps: AppState[]
	totalCount: number
	searchString: string
	sortBy: AppsSortBy
	orderBy: OrderBy
	filterByType: AppsFilterBy | AppsFilterByUi
	recordsPerPage: number
	currentPage: number
	viewType: AppsViewType
	isLoading: boolean
}
