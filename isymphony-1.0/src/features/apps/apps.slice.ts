import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import * as asyncActions from './apps.asyncActions'
import { AppsFilterByUi, AppsState, AppsViewType } from './apps.types'
import { Action } from '../../common.types'
import {
	AppsSortBy,
	OrderBy,
	AppsFilterBy,
} from '../../utilities/apiEnumConstants'
import { AppsSubReducersNames } from './base.types'

const initialState: AppsState = {
	allApps: [],
	totalCount: 0,
	searchString: '',
	sortBy: AppsSortBy.LAST_MODIFIED,
	orderBy: OrderBy.ASC,
	filterByType: AppsFilterByUi.ALL_CHANNELS,
	recordsPerPage: 12,
	currentPage: 0,
	viewType: AppsViewType.GRID,
	isLoading: false,
}

const slice = createSlice<
	AppsState,
	SliceCaseReducers<AppsState>,
	AppsSubReducersNames.APPS
>({
	name: AppsSubReducersNames.APPS,
	initialState,
	reducers: {
		// synchronous actions
		setSortBy(state, action: Action<AppsSortBy>) {
			state.sortBy = action.payload
			state.currentPage = 0
			state.isLoading = true
		},
		setOrderBy(state, action: Action<OrderBy>) {
			state.orderBy = action.payload
			state.currentPage = 0
			state.isLoading = true
		},
		setFilterByType(state, action: Action<AppsFilterByUi | AppsFilterBy>) {
			state.filterByType = action.payload
			state.currentPage = 0
			state.isLoading = true
		},
		setRecordsPerPage(state, action: Action<number>) {
			state.recordsPerPage = action.payload
			state.currentPage = 0
			state.isLoading = true
		},
		setCurrentPage(state, action: Action<number>) {
			state.currentPage = action.payload
			state.isLoading = true
		},
		setSearchString(state, action: Action<string>) {
			state.searchString = action.payload
			state.currentPage = 0
			state.isLoading = true
		},
		setViewType(state, action: Action<AppsViewType>) {
			state.viewType = action.payload
		},
	},
	extraReducers: (builder) => {
		builder.addCase(asyncActions.fetchAllApps.fulfilled, (state, action) => {
			state.allApps = action.payload.content
			state.totalCount = action.payload.totalElements
			state.isLoading = false
		})
	},
})

export default slice

export const { name, actions, reducer } = slice
