import { AppsState } from './apps.types'
import { AppSettingsState } from './appSettings/appSettings.types'

export enum AppsSubReducersNames {
	APPS = 'apps',
	APP_SETTINGS = 'appSettings',
}

export interface AppsBaseState {
	[AppsSubReducersNames.APPS]: AppsState
	[AppsSubReducersNames.APP_SETTINGS]: AppSettingsState
}
