import { combineReducers } from 'redux'
import apps from './apps.slice'
import appSettings from './appSettings/appSettings.slice'

import * as baseSelector from './base.selector'
import { AppsBaseState } from './base.types'

export const reducer = combineReducers<AppsBaseState>({
	[apps.name]: apps.reducer,
	[appSettings.name]: appSettings.reducer,
})

export const { name } = baseSelector
export * from './base.types'
