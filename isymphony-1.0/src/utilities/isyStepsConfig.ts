import { DatabaseLockStatus } from '../features/database/database.types'
import { ToolTipPosition } from '../widgets/IsySteps/IsyTour/Steps'
import i18n from 'i18next'
import {
	HELP_ACCELERATORS_CONFIGURATOR_DESCRIPTION,
	HELP_ACCELERATORS_TITLE,
	HELP_ADMIN_DESCRIPTION,
	HELP_ADMIN_TITLE,
	HELP_APPS_CREATED_APPS_STEP,
	HELP_APPS_CREATE_APP_STEP,
	HELP_APPS_CREATE_DRAWER_STEP,
	HELP_APPS_DESCRIPTION,
	HELP_APPS_FORM_EXIT_STEP,
	HELP_APPS_TITLE,
	HELP_APPS_WEB_TYPE_STEP,
	HELP,
	HELP_TEAMS_DESCRIPTION,
	HELP_TEAMS_TITLE,
	HELP_ACCELERATORS_MARKETPLACE_DESCRIPTION,
	HELP_ACCELERATORS_MY_ACCELERATORS_DESCRIPTION,
	HELP_ACCELERATORS_CATEGORIES_DESCRIPTION,
	HELP_ACCELERATORS_CATEGORIES_DATA_DESCRIPTION,
	HELP_ACCELERATORS_FORM_FIELDS_DESCRIPTION,
	HELP_ACCELERATORS_EXIT_DESCRIPTION,
	HELP_ACCELERATORS_TAKE_ME_THERE_DESCRIPTION,
	HELP_ACCELERATORS_CARD_CONTAINER_DESCRIPTION,
	HELP_ACCELERATORS_EDIT_LOCK_TO_ACQUIRE_DESCRIPTION,
	HELP_ACCELERATORS_EDIT_LOCK_ACQUIRE_DESCRIPTION,
	HELP_EDIT_LOCK,
	HELP_DATABASE_MANAGER_TITLE,
	HELP_DATABASE_ADD_DESCRIPTION,
	HELP_DATABASE_CREATE_DIALOG_DESCRIPTION,
	HELP_DATABASE_LIST_OF_DATABASES_DESCRIPTION,
	HELP_DATABASE_LOCK_NOT_PRESENT_DESCRIPTION,
	HELP_DATABASE_LOCK_ACQUIRE_DESCRIPTION,
	HELP_DATABASE_LOCK_PRESENT_DESCRIPTION,
	HELP_DATABASE_ADD_NEW_TABLE_DESCRIPTION,
	HELP_DATABASE_NEW_TABLE_CREATED_DESCRIPTION,
	HELP_DATABASE_HOVER_TABLE_DESCRIPTION,
	HELP_DATABASE_TABLES_PROPERTY_PANEL_DESCRIPTION,
	HELP_DATABASE_NEW_TABLE_DESCRIPTION,
	HELP_DATABASE_SECOND_TABLE_DESCRIPTION,
	HELP_DATABASE_KEY_DESCRIPTION,
	HELP_DATABASE_KEY_TYPES_DESCRIPTION,
	HELP_DATABASE_RELATIONSHIP_DIALOG_DESCRIPTION,
	HELP_DATABASE_CLICK_ON_CONNECT_DESCRIPTION,
	HELP_DATABASE_RELATIONSHIP_PROPERTIES_PANEL_DESCRIPTION,
	HELP_INTEGRATIONS_TITLE,
	HELP_INTEGRATIONS_ADD_SERVICE_DESCRIPTION,
	HELP_INTEGRATIONS_SELECT_REST_DESCRIPTION,
	HELP_INTEGRATIONS_ENTER_SERVICE_NAME_DESCRIPTION,
	HELP_INTEGRATIONS_SELECT_GET_HTTP_DESCRIPTION,
	HELP_INTEGRATIONS_TO_HOST_URL_WEB_SERVICE_DESCRIPTION,
	HELP_INTEGRATIONS_VIEW_PARAMETERS_AUTOMATICALLY_DESCRIPTION,
	HELP_INTEGRATIONS_SEND_TO_EXECUTE_REST_API_DESCRIPTION,
	HELP_INTEGRATIONS_JSON_FORMAT_DESCRIPTION,
	HELP_INTEGRATIONS_SAVE_TO_CONTINUE_DESCRIPTION,
	HELP_INTEGRATIONS_ADDED_REST_SERVICE_DESCRIPTION,
	HELP_SIDE_NAV_TITLE,
	HELP_SIDE_NAV_MENU_ICON_DESCRIPTION,
	HELP_SIDE_NAV_CLICK_ON_DATABASE_DESCRIPTION,
	HELP_INTEGRATIONS_ADD_NEW_SERVICE_DESCRIPTION,
	MARKETPLACE,
	HELP_ACCELERATORS_EDIT_LOCK_STEP,
	HELP_DATABASE_SAVE_DESCRIPTION,
} from './constants'

export const getHelpTourStartSteps = (helpDescription: string) => {
	const helpTourStartSteps = [
		{
			title: i18n.t(HELP),
			description: helpDescription,
			element: '.help-icon',
			position: ToolTipPosition.BOTTOM,
			addSkipButton: true,
			addStartButton: true,
			hideNextButton: true,
		},
	]
	return helpTourStartSteps
}

export const getAppsSteps = (appName: string) => {
	const appsSteps = [
		{
			title: i18n.t(HELP_APPS_TITLE),
			description: i18n.t(HELP_APPS_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.apps-tab',
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_TEAMS_TITLE),
			description: i18n.t(HELP_TEAMS_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.team-tab',
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_ADMIN_TITLE),
			description: i18n.t(HELP_ADMIN_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.admin-tab',
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_CONFIGURATOR_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.accelerators-tab',
			showStepNumbers: true,
		},

		{
			title: i18n.t(HELP_APPS_TITLE),
			description: i18n.t(HELP_APPS_CREATE_APP_STEP),
			position: ToolTipPosition.BOTTOM,
			element: '.create-btn',
			hideNextButton: true,
			showStepNumbers: true,
			hidePrevButton: true,
		},
		{
			title: i18n.t(HELP_APPS_TITLE),
			description: i18n.t(HELP_APPS_WEB_TYPE_STEP),
			position: ToolTipPosition.LEFT,
			element: '.web-btn',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_APPS_TITLE),
			description: i18n.t(HELP_APPS_CREATE_DRAWER_STEP),
			position: ToolTipPosition.LEFT,
			element: '.isy-create-app',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_APPS_TITLE),
			description: i18n.t(HELP_APPS_CREATED_APPS_STEP, { appName: appName }),
			position: ToolTipPosition.LEFT,
			element: '.open-app',
			zIndexNotSupport: true,
			hidePrevButton: true,
			hideNextButton: true,
			showStepNumbers: true,
		},
	]
	return appsSteps
}

export const getAppExitStep = () => {
	return {
		title: i18n.t(HELP_APPS_TITLE),
		description: i18n.t(HELP_APPS_FORM_EXIT_STEP),
		position: ToolTipPosition.LEFT,
		element: '.app-description',
		zIndexNotSupport: false,
		hideNextButton: true,
		addNewButton: true,
		hidePrevButton: true,
		addCompleteDetailsButton: true,
		showStepNumbers: false,
	}
}

export const getAcceleratorsSteps = (
	categoryName: string,
	categoryData: string
) => {
	const acceleratorsSteps = [
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_MARKETPLACE_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.my-accelerators',
			hidePrevButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(MARKETPLACE),
			description: i18n.t(HELP_ACCELERATORS_MY_ACCELERATORS_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.marketplace',
			hidePrevButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(MARKETPLACE),
			description: i18n.t(HELP_ACCELERATORS_CATEGORIES_DESCRIPTION, {
				categoryName: categoryName,
			}),
			position: ToolTipPosition.RIGHT,
			element: '.isy-tabs-content',
			hideNextButton: true,
			hidePrevButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_CATEGORIES_DATA_DESCRIPTION, {
				categoryName: categoryName,
				categoryData: categoryData,
			}),
			position: ToolTipPosition.RIGHT,
			element: '.isy-accelerators-grid-card-container',
			hideNextButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_FORM_FIELDS_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.isy-steps-create-instance',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_EXIT_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.isy-steps-create-instance',
			zIndexNotSupport: false,
			hideNextButton: true,
			addNewButton: true,
			hidePrevButton: true,
			addCompleteDetailsButton: true,
		},
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_TAKE_ME_THERE_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.isy-steps-take-me-there-btn',
			zIndexNotSupport: true,
			hidePrevButton: true,
			hideNextButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_ACCELERATORS_TITLE),
			description: i18n.t(HELP_ACCELERATORS_CARD_CONTAINER_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.new-instance',
			zIndexNotSupport: true,
			hidePrevButton: true,
			hideNextButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_EDIT_LOCK),
			description: i18n.t(HELP_ACCELERATORS_EDIT_LOCK_TO_ACQUIRE_DESCRIPTION),
			position: ToolTipPosition.TOP,
			element: '.request-btn',
			hidePrevButton: true,
			hideNextButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_EDIT_LOCK),
			description: i18n.t(HELP_ACCELERATORS_EDIT_LOCK_ACQUIRE_DESCRIPTION),
			position: ToolTipPosition.TOP,
			element: '.release-btn',
			hidePrevButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
	]
	return acceleratorsSteps
}

export const getAcceleratorEditLockStep = () => {
	return {
		title: i18n.t(HELP_EDIT_LOCK),
		description: i18n.t(HELP_ACCELERATORS_EDIT_LOCK_STEP),
		position: ToolTipPosition.TOP,
		element: '.locked-by',
		hidePrevButton: true,
		zIndexNotSupport: true,
		showStepNumbers: true,
	}
}

export const getAddDatabaseSteps = (lockStatus: DatabaseLockStatus) => {
	let lockStatusDescription
	let hideButton
	if (lockStatus === DatabaseLockStatus.LOCK_NOT_PRESENT) {
		lockStatusDescription = i18n.t(HELP_DATABASE_LOCK_NOT_PRESENT_DESCRIPTION)
		hideButton = false
	} else if (lockStatus === DatabaseLockStatus.LOCK_ACQUIRED) {
		lockStatusDescription = i18n.t(HELP_DATABASE_LOCK_ACQUIRE_DESCRIPTION)
		hideButton = true
	} else if (lockStatus === DatabaseLockStatus.LOCK_PRESENT) {
		lockStatusDescription = i18n.t(HELP_DATABASE_LOCK_PRESENT_DESCRIPTION)
		hideButton = true
	}
	const addDatabaseSteps = [
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_ADD_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.add-new-database',
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_CREATE_DIALOG_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.isy-steps-create-database',
			hideNextButton: true,
			hidePrevButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_LIST_OF_DATABASES_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.database-accordion-overflow',
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_EDIT_LOCK),
			description: lockStatusDescription,
			position: ToolTipPosition.LEFT,
			element: '.isy-toolbar-component',
			hideNextButton: hideButton,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_ADD_NEW_TABLE_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.isy-btn-new-table',
			hidePrevButton: true,
			hideNextButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_NEW_TABLE_CREATED_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.database-last-table',
			zIndexNotSupport: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_HOVER_TABLE_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.database-last-table',
			hidePrevButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_TABLES_PROPERTY_PANEL_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.table-properties-panel',
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_NEW_TABLE_DESCRIPTION),
			position: ToolTipPosition.LEFT,
			element: '.isy-btn-new-table',
			hidePrevButton: true,
			hideNextButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_SECOND_TABLE_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.database-last-table',
			zIndexNotSupport: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_KEY_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.key-icon .none_key_svg',
			zIndexNotSupport: true,
			hideNextButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_KEY_TYPES_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.key-popover',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_RELATIONSHIP_DIALOG_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.isy-step-connect-foreign-key',
			zIndexNotSupport: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(HELP_DATABASE_CLICK_ON_CONNECT_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.auto-search-input',
			hidePrevButton: true,
			hideNextButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_DATABASE_MANAGER_TITLE),
			description: i18n.t(
				HELP_DATABASE_RELATIONSHIP_PROPERTIES_PANEL_DESCRIPTION
			),
			position: ToolTipPosition.LEFT,
			element: '.relation-properties-panel',
			zIndexNotSupport: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_EDIT_LOCK),
			description: i18n.t(HELP_DATABASE_SAVE_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.isy-steps-save-icon',
			hidePrevButton: true,
			hideNextButton: true,
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_EDIT_LOCK),
			description: lockStatusDescription,
			position: ToolTipPosition.LEFT,
			element: '.isy-toolbar-component',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
	]
	return addDatabaseSteps
}

export const getWebServicesSteps = (orderList: string[]) => {
	let position = ToolTipPosition.BOTTOM
	let element = '.add-web-service'
	let description = i18n.t(HELP_INTEGRATIONS_ADD_SERVICE_DESCRIPTION)

	if (orderList.length !== 0) {
		position = ToolTipPosition.RIGHT
		element = '.webService-title'
		description = i18n.t(HELP_INTEGRATIONS_ADD_NEW_SERVICE_DESCRIPTION)
	}

	const webServiceConfig = [
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: description,
			position: position,
			element: element,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(HELP_INTEGRATIONS_SELECT_REST_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.isy-steps-create-web-service-dialog',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(HELP_INTEGRATIONS_ENTER_SERVICE_NAME_DESCRIPTION),
			position: ToolTipPosition.BOTTOM,
			element: '.isy_input',
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(HELP_INTEGRATIONS_SELECT_GET_HTTP_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.isy-dropdown',
			zIndexNotSupport: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(
				HELP_INTEGRATIONS_TO_HOST_URL_WEB_SERVICE_DESCRIPTION
			),
			position: ToolTipPosition.BOTTOM,
			element: '.url-input',
			showStepNumbers: true,
			hidePrevButton: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(
				HELP_INTEGRATIONS_VIEW_PARAMETERS_AUTOMATICALLY_DESCRIPTION
			),
			position: ToolTipPosition.TOP,
			element: '.query-params-section',
			zIndexNotSupport: true,
			showStepNumbers: true,
			hidePrevButton: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(
				HELP_INTEGRATIONS_SEND_TO_EXECUTE_REST_API_DESCRIPTION
			),
			position: ToolTipPosition.TOP,
			element: '.send-btn',
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(HELP_INTEGRATIONS_JSON_FORMAT_DESCRIPTION),
			position: ToolTipPosition.TOP,
			element: '.view-type',
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(HELP_INTEGRATIONS_SAVE_TO_CONTINUE_DESCRIPTION),
			position: ToolTipPosition.TOP,
			element: '.save-btn',
			hideNextButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_INTEGRATIONS_TITLE),
			description: i18n.t(HELP_INTEGRATIONS_ADDED_REST_SERVICE_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.isy-accordion',
			hidePrevButton: true,
			showStepNumbers: true,
		},
	]
	return webServiceConfig
}

export const getSideNavSteps = () => {
	const sideNavConfig = [
		{
			title: i18n.t(HELP_SIDE_NAV_TITLE),
			description: i18n.t(HELP_SIDE_NAV_MENU_ICON_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.toggle-button',
			zIndexNotSupport: true,
			hideNextButton: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
		{
			title: i18n.t(HELP_SIDE_NAV_TITLE),
			description: i18n.t(HELP_SIDE_NAV_CLICK_ON_DATABASE_DESCRIPTION),
			position: ToolTipPosition.RIGHT,
			element: '.database',
			zIndexNotSupport: true,
			hidePrevButton: true,
			showStepNumbers: true,
		},
	]
	return sideNavConfig
}
