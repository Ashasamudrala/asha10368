import {
	AppsFilterBy,
	OrderBy,
	AppsSortBy,
	AcceleratorSortBy,
} from './apiEnumConstants'

export const GET_ALL_PLATFORMS = 'administration/platforms'
export const SAVE_PLATFORM = 'administration/platforms'
export const GET_ALL_FRAMEWORKS =
	'administration/platforms/5d57fc4c8a37cf5e626ad9ff/frameworks'
export const GET_PLATFORM_BY_ID = 'administration/platforms/platformId'

export const SAVE_FRAMEWORK_DETAILS = 'administration/platforms'
export const CREATE_FRAMEWORK_BY_ID = (id: string) =>
	`administration/platforms/${id}/frameworks`

export const VIEW_PLATFORM_BY_ID = 'administration/platforms'
export const LOGIN_AUTH = 'auth/login'
export const LOGOUT_AUTH = 'auth/logout'
export const LOGIN_REFRESH = 'users/me'
export const USER_GET_PROFILE = (id: string) => 'users/' + id
export const USER_UPDATE_PROFILE_PIC = (id: string) =>
	'users/' + id + '/picture'
export const GET_ALL_REPOSITORIES =
	'administration/devops/repositories/configurations'
export const SAVE_REPOSITORY =
	'administration/devops/repositories/configurations'
export const GET_ALL_CONTINUOUS_INTEGRATION =
	'administration/devops/continuousIntegration/configurations'
export const UPDATE_FRAMEWORK = 'administration/platforms'
export const GET_ALL_USERS = 'users'
export const DELETE_USERS = (ids: string[]) => 'users/?userIds=' + ids
export const GET_ACTIVE_MEMBERS = 'invitations?status=invited'
export const GET_PENDING_INVITES = 'invitations?status=draft'
export const UPDATE_REPOSITORIES_BY_ID =
	'administration/devops/repositories/configurations/'
export const UPDATE_CONTINUOUS_DELIVERY_BY_ID =
	'administration/devops/continuousDelivery/configurations/'
export const USER_ROLES = 'roles'
export const GET_ACTIVE_SEARCH_USERS = (
	search: string,
	page: number,
	size: number
) => `users/search?queryString=${search}&pageNumber=${page}&pageSize=${size}`
export const GET_PENDING_SEARCH_USERS = (
	search: string,
	page: number,
	size: number
) =>
	`invitations/search?queryString=${search}&pageNumber=${page}&pageSize=${size}`
export const CREATE_INVITE_USERS = 'invitations'
export const DELETE_INVITE_USERS = (ids: string[]) =>
	'invitations/?invitationIds=' + ids
export const SAVE_CONTINUOUS_DELIVERY =
	'administration/devops/continuousDelivery/configurations'
export const GET_ALL_CONTINUOUS_DELIVERY =
	'administration/devops/continuousDelivery/configurations'
export const PASSWORD_RESET_REQUESTS = 'passwords/resetRequests'
export const PASSWORD_RESET_PASSWORD = 'passwords/resetPassword'
export const GET_TOKEN_FOR_RESET_PASSWORD = (token: string) =>
	'passwords/resetRequests?token=' + token
export const UPDATE_INVITE_USER = (id: string) => 'invitations/' + id
export const UPDATE_ACTIVE_USER = (id: string) => 'users/' + id + '/roles'
export const UPDATE_DB_CONFIG = (appid: string, databaseId: string) =>
	`applications/${appid}/databases/${databaseId}`
export const SIGN_UP_USERS = 'users/signup'
export const GET_INVITATIONS_FOR_SIGN_UP = (token: string) =>
	'invitations/verify?token=' + token
export const GET_ALL_DATABASES = (id: string) => `applications/${id}/databases`
export const CREATE_DATABASE = (id: string) => `applications/${id}/databases`
export const SAVE_DATABASE = (
	appId: string,
	databaseId: string,
	schemaId: string
) => `applications/${appId}/databases/${databaseId}/schemas/${schemaId}`

export const DATABASE_CHECK_IN_USER = (
	appId: string,
	databaseId: string,
	schemaId: string
) =>
	`applications/${appId}/databases/${databaseId}/schemas/${schemaId}/checkinUser`

export const DATABASE_CHECK_IN = (
	appId: string,
	databaseId: string,
	schemaId: string
) => `applications/${appId}/databases/${databaseId}/schemas/${schemaId}/checkin`

export const DATABASE_CHECK_OUT = (
	appId: string,
	databaseId: string,
	schemaId: string
) =>
	`applications/${appId}/databases/${databaseId}/schemas/${schemaId}/checkout`

export const DELETE_DATABASE = (applicationId: string, databaseId: string) =>
	`applications/${applicationId}/databases/${databaseId}`
export const GET_ATTRIBUTE_TYPES = 'lookups/attributeTypes'
export const GET_CONSTRAINT_KEY_TYPES = 'lookups/keyTypes'
export const CREATE_APPLICATION = 'applications'
export const SAVE_DATABASE_CANVAS = (
	appId: string,
	databaseId: string,
	schemaId: string
) => `applications/${appId}/databases/${databaseId}/schemas/${schemaId}/canvas`
export const GET_APP_BY_ID = (id: string) => `applications/${id}`
export const UPDATE_APP_BY_ID = (id: string) => `applications/${id}`
export const FIND_BY_NAME = (name: string, type: string) =>
	`applications/findByName?name=${name}&type=${type.toUpperCase()}`
export const GET_ALL_APPS = (
	search: string,
	type: AppsFilterBy | 'ALL_CHANNELS',
	page: number,
	size: number,
	sortBy: AppsSortBy,
	orderBy: OrderBy
) => {
	let string = `applications/search?queryString=${search}`

	if (type !== 'ALL_CHANNELS') {
		string += '&type=' + type
	}

	string += `&pageNumber=${page}&pageSize=${size}&sortBy=${sortBy}&direction=${orderBy}`

	return string
}
export const UPDATE_FRAMEWORK_BY_ID = (
	platformId: string,
	frameworkId: string
) => `administration/platforms/${platformId}/frameworks/${frameworkId}`
export const GET_PLATFORM_DETAILS_BY_ID = (platformId: string) =>
	`administration/platforms/${platformId}`

export const ACCELERATORS_GET_CATEGORIES_WITH_MARKET_PLACE =
	'marketplace/categories?pageSize=100'

export const ACCELERATORS_GET_CATEGORIES_WITH_MARKET_PLACE_WITH_SEARCH = (
	searchString: string
) => 'marketplace/search?pageSize=100&queryString=' + searchString

export const ACCELERATORS_GET_CATEGORIES = 'marketplace/categories/names'

export const ACCELERATORS_GET_APPS_ACCELERATORS = (
	acceleratorIds: string[],
	sortBy: AcceleratorSortBy,
	orderBy: OrderBy
) => {
	return `applications/acceleratorInstances?pageSize=100&applicationIds=${acceleratorIds.join(
		','
	)}&sortBy=${sortBy}&direction=${orderBy}`
}

export const ACCELERATORS_GET_APPS_ACCELERATORS_SEARCH = (
	acceleratorIds: string[],
	sortBy: AcceleratorSortBy,
	orderBy: OrderBy,
	search: string
) => {
	return `applications/acceleratorInstances/search?pageSize=100&applicationIds=${acceleratorIds.join(
		','
	)}&sortBy=${sortBy}&direction=${orderBy}&queryString=${search}`
}

export const ACCELERATORS_GET_ACCELERATOR_DATA = (
	categoryId: string,
	acceleratorId: string
) => {
	return (
		'marketplace/categories/' + categoryId + '/accelerators/' + acceleratorId
	)
}

export const ACCELERATORS_GET_ALL_APPS_EXCLUDE = (acceleratorIds: string[]) => {
	return (
		'applications/excludedApplications?pageSize=100&acceleratorIds=' +
		acceleratorIds.join(',')
	)
}

export const ACCELERATOR_CHECK_IN_USER = (
	appId: string,
	acceleratorInstanceId: string
) =>
	`applications/${appId}/acceleratorInstances/${acceleratorInstanceId}/checkinUser`

export const ACCELERATOR_CHECK_IN = (
	appId: string,
	acceleratorInstanceId: string
) =>
	`applications/${appId}/acceleratorInstances/${acceleratorInstanceId}/checkin`

export const ACCELERATOR_CHECK_OUT = (
	appId: string,
	acceleratorInstanceId: string
) =>
	`applications/${appId}/acceleratorInstances/${acceleratorInstanceId}/checkout`

export const ACCELERATORS_GET_ALL_APPS = () => {
	return 'applications?pageSize=100'
}

export const ACCELERATORS_GET_ACCELERATOR_INSTANCES = (
	applicationId: string
) => {
	return 'applications/' + applicationId + '/acceleratorInstances'
}

export const ACCELERATORS_GET_ACCELERATOR_INSTANCE = (
	applicationId: string,
	instanceId: string
) => {
	return 'applications/' + applicationId + '/acceleratorInstances/' + instanceId
}

export const ACCELERATORS_GET_APP = (applicationId: string) => {
	return 'applications/' + applicationId
}

export const ACCELERATORS_VALIDATE_NAME_FOR_DUPLICATE = (
	name: string,
	categoryId: string
) => {
	return (
		'applications/acceleratorInstances/findByName?categoryId=' +
		categoryId +
		'&name=' +
		name
	)
}

export const ACCELERATORS_GET_ATTRIBUTE_TYPES =
	'lookups/attributeTypes?primary=true'

export const WEB_SERVICE_GET_LIST = (applicationId: string) => {
	return 'applications/' + applicationId + '/services/rest'
}

export const WEB_SERVICE_ITEM = (applicationId: string, serviceId: string) => {
	return 'applications/' + applicationId + '/services/rest/' + serviceId
}

export const WEB_SERVICE_GET_REQUEST_METHODS_LOOKUPS =
	'services/rest/lookups?componentType=REQUEST_METHODS'

export const WEB_SERVICE_GET_AUTHORIZATION_LOOKUPS =
	'services/rest/lookups?componentType=AUTHORIZATION'

export const WEB_SERVICE_GET_REQUEST_BODY_LOOKUPS =
	'services/rest/lookups?componentType=REQUEST_BODY'

export const APP_DEV_OPS_LATEST = (appId: string) =>
	`applications/${appId}/jobs/latest`

export const APP_DEV_OPS_JOBS = (appId: string) => `applications/${appId}/jobs`

export const APP_DEV_OPS_JOB = (appId: string, jobId: string) =>
	`applications/${appId}/jobs/${jobId}`
export const APP_DEV_OPS_JOB_DOWNLOAD_CODE = (appId: string, jobId: string) =>
	`applications/${appId}/jobs/${jobId}/downloadcode`
export const GET_ALL_PREFERENCES = 'users/me/preferences'

export const UPDATE_ALL_PREFERENCES = 'users/me/preferences'
