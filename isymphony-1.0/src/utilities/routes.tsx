import React from 'react'
import { Login } from '../features/loginAndSignUp/login/Login'
import { ForgotPassword } from '../features/loginAndSignUp/forgotPassword/ForgotPassword'
import { ResetPassword } from '../features/loginAndSignUp/resetPassword/ResetPassword'
import { ActiveInviteSignUp } from '../features/loginAndSignUp/activeInviteSignUp/ActiveInviteSignUp'
import { Apps } from '../features/apps/Apps'
import { Database } from '../features/database/Database'
import { Accelerators } from '../features/accelerators/Accelerators'
import { WebServices } from '../features/webServices/WebServices'
import Team from '../features/team/Team'
import Core from '../features/admin/core/Core'

import DashboardIcon from '@material-ui/icons/Dashboard'
import DnsIcon from '@material-ui/icons/Dns'
import { MediationIcon } from '../icons/mediation'
import ExtensionIcon from '@material-ui/icons/Extension'
import WebServiceIcon from '@material-ui/icons/Language'

export const getSidebarIconBasedOnRouteId = (id: string) => {
	switch (id) {
		case 'dashboard':
			return <DashboardIcon />
		case 'database':
			return <DnsIcon />
		case 'decisionEngine':
			return <MediationIcon />
		case 'accelerators':
			return <ExtensionIcon />
		case 'webServices':
			return <WebServiceIcon />
		default:
			return null
	}
}

export const getComponentsBasedOnRouteId = (id: string) => {
	switch (id) {
		case 'login':
			return <Login />
		case 'forgotPassword':
			return <ForgotPassword />
		case 'resetPassword':
			return <ResetPassword />
		case 'activeInviteSignUp':
			return <ActiveInviteSignUp />
		case 'apps':
			return <Apps />
		case 'dashboard':
			return 'Dashboard'
		case 'database':
			return <Database />
		case 'decisionEngine':
			return 'Decision Engine'
		case 'accelerators':
			return <Accelerators />
		case 'webServices':
			return <WebServices />
		case 'team':
			return <Team />
		case 'core':
			return <Core />
		default:
			return null
	}
}
