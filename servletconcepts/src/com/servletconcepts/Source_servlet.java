package com.servletconcepts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Source_servlet
 */
@WebServlet("/Source")
public class Source_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Source_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String pwd1 = request.getParameter("pwd1");
		String pwd2 = request.getParameter("pwd2");
	    PrintWriter pw = response.getWriter();
	    Cookie ck=new Cookie("asha",pwd1); 
	    response.addCookie(ck);
	    pw.println("<h1>  this is example of include method </h2>");
		 RequestDispatcher rd = request.getRequestDispatcher("/Display");
		if(pwd1.equals(pwd2)) {
			rd.include(request, response);
		}
		else {
			response.sendRedirect("error.html");
		}
		
	}

}
