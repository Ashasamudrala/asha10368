import React, { Suspense, useState } from 'react'
import { isNil } from 'lodash'
import { useSelector } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import { Container } from '@material-ui/core'
import Routes from './Routes'
import Theme from './widgets/theme/theme'
import './app.scss'
import AuthProvider from './widgets/auth/Auth'
import { selectGetLogInUserInfo } from './authAndPermissions/loginUserDetails.selectors'

function App() {
	const themeNames = { dark: `dark-theme`, light: `light-theme` }
	const [themeName] = useState(themeNames.light)
	const userData = useSelector(selectGetLogInUserInfo)

	return (
		<Theme themeName={themeName}>
			<AuthProvider
				userPermissions={!isNil(userData) ? userData.permissions : null}
			></AuthProvider>
			<div className='main-container'>
				<Suspense fallback='loading'>
					<Router>
						<Container className='page'>
							<Routes />
						</Container>
					</Router>
				</Suspense>
			</div>
		</Theme>
	)
}

export default App
