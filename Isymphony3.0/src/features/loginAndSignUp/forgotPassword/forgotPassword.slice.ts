import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { LoginAndSignUpSubReducersNames } from '../loginAndSignUp.types'
import {
	ForgotPasswordDetailsErrorState,
	ForgotPasswordDetailsState,
	ForgotPasswordState,
} from './forgotPassword.types'

const forgotPasswordDetailsDefault: ForgotPasswordDetailsState = {
	email: '',
}

const initialState: ForgotPasswordState = {
	details: forgotPasswordDetailsDefault,
	errors: {},
}

const slice = createSlice<
	ForgotPasswordState,
	SliceCaseReducers<ForgotPasswordState>,
	LoginAndSignUpSubReducersNames.FORGOT_PASSWORD
>({
	name: LoginAndSignUpSubReducersNames.FORGOT_PASSWORD,
	initialState,
	reducers: {
		// synchronous actions
		updateDetails(state, action: Action<ForgotPasswordDetailsState>) {
			state.details = action.payload
		},
		setErrorList(state, action: Action<ForgotPasswordDetailsErrorState>) {
			state.errors = action.payload
		},
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
