export interface ForgotPasswordDetailsState {
	email: string
}
export interface ForgotPasswordDetailsErrorState {
	email?: string
}
export interface ForgotPasswordState {
	details: ForgotPasswordDetailsState
	errors: ForgotPasswordDetailsErrorState
}
