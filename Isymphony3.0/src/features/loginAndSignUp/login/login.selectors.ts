import slice from './login.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUp.selector'
import { RootState } from '../../../base.types'
import {
	LoginDetailsErrorState,
	LoginDetailsState,
	LoginState,
} from './login.types'

export const selectSlice = (state: RootState): LoginState =>
	loginAndRecoverySlice(state)[slice.name]

export const selectLoginDetails = (state: RootState): LoginDetailsState =>
	selectSlice(state).details
export const selectLoginErrors = (state: RootState): LoginDetailsErrorState =>
	selectSlice(state).errors
