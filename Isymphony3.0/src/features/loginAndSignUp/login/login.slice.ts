import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { LoginAndSignUpSubReducersNames } from '../loginAndSignUp.types'
import {
	LoginDetailsErrorState,
	LoginDetailsState,
	LoginState,
} from './login.types'

const loginDetails: LoginDetailsState = {
	email: '',
	password: '',
}
const initialState: LoginState = {
	details: loginDetails,
	errors: {},
}
const slice = createSlice<
	LoginState,
	SliceCaseReducers<LoginState>,
	LoginAndSignUpSubReducersNames.LOGIN
>({
	name: LoginAndSignUpSubReducersNames.LOGIN,
	initialState,
	reducers: {
		updateDetails(state, action: Action<LoginDetailsState>) {
			state.details = action.payload
		},
		setErrorList(state, action: Action<LoginDetailsErrorState>) {
			state.errors = action.payload
		},
		setEmail(state, action: Action<string>) {
			state.details.email = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
