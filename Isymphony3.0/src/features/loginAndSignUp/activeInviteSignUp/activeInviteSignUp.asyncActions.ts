import { createAsyncThunk } from '@reduxjs/toolkit'
import { omit } from 'lodash'
import { RootState } from '../../../base.types'
import { DialogTypes, showDialog } from '../../dialogs'
import i18n from 'i18next'
import doAsync from '../../../infrastructure/doAsync'
import {
	SIGN_UP_USERS,
	GET_INVITATIONS_FOR_SIGN_UP,
} from '../../../utilities/apiEndpoints'
import {
	SIGN_UP_ACCOUNT_SUCCESS_MESSAGE,
	TERM_NOT_ACCEPTED,
} from '../../../utilities/constants'
import { selectSignUpDetails } from './activeInviteSignUp.selectors'

export const validateInvitationId = createAsyncThunk(
	'signUp/getAll',
	async (InvitationIdFromCurrentPath: string, thunkArgs) =>
		await doAsync({
			url: GET_INVITATIONS_FOR_SIGN_UP(InvitationIdFromCurrentPath),
			...thunkArgs,
		})
)

export const signUpUserDetails = createAsyncThunk(
	'activeInviteSignUp',
	async (_: undefined, thunkArgs) => {
		const details = selectSignUpDetails(thunkArgs.getState() as RootState)
		if (!details.isTermsAgreed) {
			return thunkArgs.dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(TERM_NOT_ACCEPTED),
					},
				})
			)
		}
		return await doAsync({
			url: SIGN_UP_USERS,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(
					omit(details, 'conformationPassword', 'isTermsAgreed')
				),
			},
			successMessage: SIGN_UP_ACCOUNT_SUCCESS_MESSAGE,
			...thunkArgs,
		})
	}
)
export const showTermsAndConditionsDialog = createAsyncThunk(
	'termsAndConditions/openDialog',
	async (_: undefined, thunkArgs) => {
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.TERMS_AND_CONDITIONS,
			})
		)
	}
)
