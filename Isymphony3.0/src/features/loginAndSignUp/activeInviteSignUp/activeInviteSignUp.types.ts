export interface ActiveInviteUserDetailsProps {
	firstName: string
	lastName: string
	title: string
	email: string
	password: string
	conformationPassword: string
	invitationId: string
	isTermsAgreed: boolean
}

export enum ActiveInviteUserPageStatus {
	INITIAL,
	SUCCESS,
}

export interface ActiveInviteUserDetailsErrorProps {
	firstName?: string
	lastName?: string
	title?: string
	password?: string
	conformationPassword?: string
}

export interface ActiveInviteSignUpState {
	details: ActiveInviteUserDetailsProps
	status: ActiveInviteUserPageStatus
	errors: ActiveInviteUserDetailsErrorProps
}
