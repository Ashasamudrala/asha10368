import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
	signUpUserDetails,
	validateInvitationId,
	showTermsAndConditionsDialog,
} from './activeInviteSignUp.asyncActions'
import { IsyLogoHeader } from '../../../widgets/IsyLogoHeader/IsyLogoHeader'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { useTranslation } from 'react-i18next'
import {
	CREATE_ACCOUNT,
	SIGN_UP_TRANSLATIONS,
	AGREE_ALL,
	TERMS_AND_CONDITIONS,
	FIRST_NAME,
	LAST_NAME,
	EMAIL,
	JOB_TITLE,
	PASSWORD,
	CONFIRM_PASSWORD,
	FIRST_NAME_EMPTY,
	JOB_TITLE_EMPTY,
	VALIDATE_PASSWORD,
	VALIDATE_PASSWORD_PATTERN,
	VALIDATE_CONFIRM_PASSWORD,
	PASSWORD_NOT_MATCH,
	SIGN_UP,
} from '../../../utilities/constants'
import './activeInviteSignUp.scss'
import { isEmpty, isNil } from 'lodash'
import {
	selectSignUpDetails,
	selectSignUpErrors,
	selectSignUpStatus,
} from './activeInviteSignUp.selectors'
import { useHistory } from 'react-router-dom'
import {
	ActiveInviteUserDetailsErrorProps,
	ActiveInviteUserDetailsProps,
	ActiveInviteUserPageStatus,
} from './activeInviteSignUp.types'
import { actions } from './activeInviteSignUp.slice'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'

export function ActiveInviteSignUp() {
	const details = useSelector(selectSignUpDetails)
	const status = useSelector(selectSignUpStatus)
	const errors = useSelector(selectSignUpErrors)
	const history = useHistory()

	const { t } = useTranslation(SIGN_UP_TRANSLATIONS)
	const dispatch = useDispatch()

	useEffect(() => {
		const currentPath = window.location.href
		const url = new URL(currentPath)
		const invitationIdFromCurrentPath = url.searchParams.get('invitationId')
		if (!isNil(invitationIdFromCurrentPath)) {
			dispatch(actions.setInvitationId(invitationIdFromCurrentPath))
			dispatch(validateInvitationId(invitationIdFromCurrentPath))
		} else {
			history.push('/login')
		}
	}, [history, dispatch])

	useEffect(() => {
		if (status === ActiveInviteUserPageStatus.SUCCESS) {
			history.push('/login')
		}
	}, [status, history])

	const getFormBuilderConfig = (): IsyFormBuilderFormItemProps[] => [
		{
			type: IsyFormBuilderFormTypes.SECTION,
			title: t(SIGN_UP),
			forms: [
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'firstName',
					title: t(FIRST_NAME),
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'lastName',
					title: t(LAST_NAME),
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'title',
					title: t(JOB_TITLE),
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'email',
					title: t(EMAIL),
					disabled: true,
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_PASSWORD,
					dataRef: 'password',
					title: t(PASSWORD),
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_PASSWORD,
					dataRef: 'conformationPassword',
					title: t(CONFIRM_PASSWORD),
				},
			],
		},
	]

	const passwordIsValid = (password: string) => {
		return password.match(/^(?=.*[\da-zA-Z]).{8,}$/)
	}

	const handleChangeCheckbox = () => {
		dispatch(actions.toggleIsTermsAgreed(null))
	}

	const handleUpdateData = (data: ActiveInviteUserDetailsProps) => {
		dispatch(actions.updateDetails(data))
	}

	const handleValidation = () => {
		const errors: ActiveInviteUserDetailsErrorProps = {}
		if (isEmpty(details.firstName)) {
			errors.firstName = t(FIRST_NAME_EMPTY)
		}
		if (isEmpty(details.lastName)) {
			errors.lastName = t(FIRST_NAME_EMPTY)
		}
		if (isEmpty(details.title)) {
			errors.title = t(JOB_TITLE_EMPTY)
		}
		if (isEmpty(details.password)) {
			errors.password = t(VALIDATE_PASSWORD)
		} else if (!passwordIsValid(details.password)) {
			errors.password = t(VALIDATE_PASSWORD_PATTERN)
		}
		if (isEmpty(details.conformationPassword)) {
			errors.conformationPassword = t(VALIDATE_CONFIRM_PASSWORD)
		} else if (details.password !== details.conformationPassword) {
			errors.conformationPassword = t(PASSWORD_NOT_MATCH)
		}
		dispatch(actions.setErrorList(errors))
		return isEmpty(errors)
	}
	const handleSave = () => {
		if (handleValidation()) {
			dispatch(signUpUserDetails())
		}
	}
	const handleTermsAndConditionsClick = () => {
		dispatch(showTermsAndConditionsDialog())
	}

	return (
		<div
			className='signUp-container'
			style={{ background: 'url(/images/Login.png)' }}
		>
			<IsyLogoHeader />
			<div className='form'>
				<div className='sign-up-form'>
					<IsyFormBuilder<ActiveInviteUserDetailsProps>
						data={details}
						forms={getFormBuilderConfig()}
						onChange={handleUpdateData}
						errors={errors}
					/>
					<div className='agree-all-terms'>
						<div className='check-agree'>
							<input
								type='checkbox'
								id='checkbox'
								checked={details.isTermsAgreed}
								onChange={handleChangeCheckbox}
								className='checkbox'
							/>
							<label htmlFor='checkbox'>
								<span className='check-text'>{t(AGREE_ALL)}</span>
							</label>
							<span
								className='terms-conditions'
								onClick={handleTermsAndConditionsClick}
							>
								{' '}
								{t(TERMS_AND_CONDITIONS)}
							</span>
						</div>
						<IsyButton onClick={handleSave} className='create-btn'>
							{t(CREATE_ACCOUNT)}
						</IsyButton>
					</div>
				</div>
			</div>
		</div>
	)
}
