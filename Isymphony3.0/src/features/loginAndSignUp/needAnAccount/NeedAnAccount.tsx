import React from 'react'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	INNOMINDS_COPYRIGHT,
} from '../../../utilities/constants'
import './needAnAccount.scss'

export function NeedAnAccount() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	return (
		<div className='login-footer'>
			<div className='footer-text'>
				<p className='left-text'>&copy;{t(INNOMINDS_COPYRIGHT)}</p>
			</div>
		</div>
	)
}
