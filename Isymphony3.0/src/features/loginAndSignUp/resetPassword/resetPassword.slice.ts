import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { LoginAndSignUpSubReducersNames } from '../loginAndSignUp.types'
import * as asyncActions from './resetPassword.asyncActions'
import {
	ResetPasswordDetailsErrorState,
	ResetPasswordDetailsState,
	ResetPasswordState,
} from './resetPassword.types'

const resetPasswordDetails: ResetPasswordDetailsState = {
	email: '',
	newPassword: '',
	conformationPassword: '',
	token: '',
}

const initialState: ResetPasswordState = {
	details: resetPasswordDetails,
	errors: {},
}

const slice = createSlice<
	ResetPasswordState,
	SliceCaseReducers<ResetPasswordState>,
	LoginAndSignUpSubReducersNames.RESET_PASSWORD
>({
	name: LoginAndSignUpSubReducersNames.RESET_PASSWORD,
	initialState,
	reducers: {
		setToken(state, action: Action<string>) {
			state.details.token = action.payload
		},
		updateDetails(state, action: Action<ResetPasswordDetailsState>) {
			state.details = action.payload
		},
		setErrorList(state, action: Action<ResetPasswordDetailsErrorState>) {
			state.errors = action.payload
		},
	},
	extraReducers: (builder) => {
		// asynchronous actions
		builder.addCase(
			asyncActions.validateResetToken.fulfilled,
			(state, action) => {
				if (action.payload) {
					state.details.email = action.payload.email
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
