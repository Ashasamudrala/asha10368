import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	selectResetPasswordDetails,
	selectResetPasswordErrors,
} from './resetPassword.selectors'
import { actions } from './resetPassword.slice'
import {
	validateResetToken,
	postEmailAndToken,
} from './resetPassword.asyncActions'

import { isEmpty, isNil } from 'lodash'
import { useTranslation } from 'react-i18next'
import { NeedAnAccount } from '../needAnAccount/NeedAnAccount'
import {
	PLATFORM_TRANSLATIONS,
	REMEMBER_YOUR_PASSWORD,
	LOGIN,
	CHANGE_YOUR_PASSWORD,
	RESET_PASSWORD,
	EMAIL,
	PASSWORD,
	CONFIRM_PASSWORD,
	VALIDATE_PASSWORD,
	VALIDATE_PASSWORD_PATTERN,
	PASSWORD_NOT_MATCH,
	VALIDATE_CONFIRM_PASSWORD,
} from '../../../utilities/constants'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import './ResetPassword.scss'
import { Link, useHistory } from 'react-router-dom'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import {
	ResetPasswordDetailsErrorState,
	ResetPasswordDetailsState,
} from './resetPassword.types'
import { IsyLogoHeader } from '../../../widgets/IsyLogoHeader/IsyLogoHeader'

export function ResetPassword() {
	const details = useSelector(selectResetPasswordDetails)
	const errors = useSelector(selectResetPasswordErrors)
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const history = useHistory()
	const dispatch = useDispatch()

	useEffect(() => {
		const currentPath = window.location.href
		const url = new URL(currentPath)
		const tokenFromCurrentPath = url.searchParams.get('token')
		if (!isNil(tokenFromCurrentPath)) {
			dispatch(actions.setToken(tokenFromCurrentPath))
			dispatch(validateResetToken(tokenFromCurrentPath))
		} else {
			history.push('/login')
		}
	}, [dispatch, history])

	const getFormBuilderConfig = (): IsyFormBuilderFormItemProps[] => [
		{
			type: IsyFormBuilderFormTypes.SECTION,
			title: t(RESET_PASSWORD),
			forms: [
				{
					type: IsyFormBuilderFormTypes.ANIMATE_STRING,
					dataRef: 'email',
					title: t(EMAIL),
					disabled: true,
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_PASSWORD,
					dataRef: 'newPassword',
					title: t(PASSWORD),
				},
				{
					type: IsyFormBuilderFormTypes.ANIMATE_PASSWORD,
					dataRef: 'conformationPassword',
					title: t(CONFIRM_PASSWORD),
				},
			],
		},
	]

	const passwordIsValid = (password: string) => {
		return password.match(/^(?=.*[\da-zA-Z]).{8,}$/)
	}

	const handleUpdateData = (data: ResetPasswordDetailsState) => {
		dispatch(actions.updateDetails(data))
	}

	const handleValidation = () => {
		const errors: ResetPasswordDetailsErrorState = {}
		if (isEmpty(details.newPassword)) {
			errors.newPassword = t(VALIDATE_PASSWORD)
		} else if (!passwordIsValid(details.newPassword)) {
			errors.newPassword = t(VALIDATE_PASSWORD_PATTERN)
		}
		if (isEmpty(details.conformationPassword)) {
			errors.conformationPassword = t(VALIDATE_CONFIRM_PASSWORD)
		} else if (details.newPassword !== details.conformationPassword) {
			errors.conformationPassword = t(PASSWORD_NOT_MATCH)
		}
		dispatch(actions.setErrorList(errors))
		return isEmpty(errors)
	}

	const handleSave = () => {
		if (handleValidation()) {
			dispatch(postEmailAndToken())
		}
	}

	return (
		<div
			className='rest-password-container'
			style={{ background: 'url(/images/Login.png)' }}
		>
			<IsyLogoHeader />
			<div className='form'>
				<div className='rest-password-form'>
					<IsyFormBuilder<ResetPasswordDetailsState>
						data={details}
						forms={getFormBuilderConfig()}
						onChange={handleUpdateData}
						errors={errors}
					/>
					<IsyButton onClick={handleSave} className='primary-btn'>
						{t(CHANGE_YOUR_PASSWORD)}
					</IsyButton>
					<div className='remember-password'>
						<div className='message-class'>
							<div className='remember-tag'>
								<span>{t(REMEMBER_YOUR_PASSWORD)}</span>
							</div>
						</div>
						<Link className='forgot-text' to='/login'>
							{t(LOGIN)}
						</Link>
					</div>
				</div>
				<div className='footer-class'>
					<NeedAnAccount />
				</div>
			</div>
		</div>
	)
}
