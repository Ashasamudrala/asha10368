import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import { RootState } from '../../base.types'
import { DialogTypes, showDialog } from '../dialogs'
import { getAcceleratorInstanceBasedOnId } from './accelerators.selectors'
import i18n from 'i18next'
import { ACCELERATOR_CONFIRM_MESSAGE_DELETE } from '../../utilities/constants'
import {
	deleteAcceleratorInstance,
	fetchCategories,
	fetchCategoriesWithMarketPlace,
	loadAcceleratorApps,
} from './accelerators.asyncActions'
import { actions } from './accelerators.slice'

export const initAccelerators = createAsyncThunk(
	'accelerators/initAccelerators',
	async (appid: string | null, thunkArgs) => {
		thunkArgs.dispatch(fetchCategories())
		if (isNil(appid)) {
			thunkArgs.dispatch(fetchCategoriesWithMarketPlace())
			thunkArgs.dispatch(loadAcceleratorApps())
		} else {
			thunkArgs.dispatch(actions.setSelectedApps([appid]))
		}
	}
)

export const conformDeleteAcceleratorInstance = createAsyncThunk(
	'accelerators/conformDeleteAcceleratorInstance',
	async (
		data: { acceleratorInstanceId: string; categoryId: string },
		{ dispatch, getState }
	) => {
		const instanceData = getAcceleratorInstanceBasedOnId(
			getState() as RootState,
			data.acceleratorInstanceId,
			data.categoryId
		)
		if (isNil(instanceData)) {
			return null
		}
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => {
					dispatch(
						deleteAcceleratorInstance({
							appId: instanceData.applicationId,
							acceleratorInstanceId: data.acceleratorInstanceId,
						})
					).then(() => {
						dispatch(actions.deleteInstanceData(data))
					})
				},
				data: {
					message: i18n.t(ACCELERATOR_CONFIRM_MESSAGE_DELETE, {
						name: instanceData.name,
					}),
				},
			})
		)
	}
)
