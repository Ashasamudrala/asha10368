import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import {
	AcceleratorInstanceProps,
	AcceleratorsAppItemProps,
	AcceleratorsState,
	CategoriesToAcceleratorInstancesMap,
	CategoriesToMarketPlaceMap,
	AcceleratorsViewType,
} from './accelerators.types'
import { AcceleratorSortBy, OrderBy } from '../../utilities/apiEnumConstants'
import { AcceleratorsSubReducersNames } from './base.types'
import { Action } from '../../common.types'
import * as asyncActions from './accelerators.asyncActions'
import { cloneDeep, find, findIndex, isArray, isEmpty, isNil } from 'lodash'

const initialState: AcceleratorsState = {
	categoriesData: [],
	categoriesToMarketPlaceMap: {},
	categoriesToAcceleratorInstancesMap: {},
	selectedApps: [],
	appsList: [],
	activeTab: 0,
	activeCategoryId: '',
	searchString: '',
	sortBy: AcceleratorSortBy.LAST_MODIFIED,
	orderBy: OrderBy.ASC,
	viewType: AcceleratorsViewType.GRID,
	highlightApp: null,
}

const slice = createSlice<
	AcceleratorsState,
	SliceCaseReducers<AcceleratorsState>,
	AcceleratorsSubReducersNames.ACCELERATORS
>({
	name: AcceleratorsSubReducersNames.ACCELERATORS,
	initialState,
	reducers: {
		clearAcceleratorInstancesMap(state) {
			state.categoriesToAcceleratorInstancesMap = {}
		},
		setSearchString(state, action: Action<string>) {
			state.searchString = action.payload
			state.categoriesToMarketPlaceMap = {}
			state.categoriesToAcceleratorInstancesMap = {}
			state.highlightApp = null
		},
		setSortBy(state, action: Action<AcceleratorSortBy>) {
			state.sortBy = action.payload
			state.highlightApp = null
		},
		setOrderBy(state, action: Action<OrderBy>) {
			state.orderBy = action.payload
			state.highlightApp = null
		},
		setViewType(state, action: Action<AcceleratorsViewType>) {
			state.viewType = action.payload
			state.highlightApp = null
		},
		setSelectedApps(state, action: Action<string[]>) {
			state.selectedApps = action.payload
			state.categoriesToAcceleratorInstancesMap = {}
			state.highlightApp = null
		},
		setActiveTab(state, action: Action<number>) {
			state.activeTab = action.payload
			state.highlightApp = null
			// if (state.categoriesData.length > 0) {
			// 	state.activeCategoryId = state.categoriesData[0].id
			// }
		},
		setActiveCategoryId(state, action: Action<string>) {
			state.activeCategoryId = action.payload
			state.highlightApp = null
		},
		deleteInstanceData(
			state,
			action: Action<{
				acceleratorInstanceId: string
				categoryId: string
			}>
		) {
			const accelerators: CategoriesToAcceleratorInstancesMap = cloneDeep(
				state.categoriesToAcceleratorInstancesMap
			)
			const id = action.payload.categoryId
			if (!isArray(accelerators[id])) {
				accelerators[id] = []
			}
			const index = findIndex(
				accelerators[id],
				(data) => data.id === action.payload.acceleratorInstanceId
			)
			if (index !== -1) {
				accelerators[id].splice(index, 1)
			}
			if (accelerators[id].length < 1) {
				delete accelerators[id]
			}
			state.categoriesToAcceleratorInstancesMap = accelerators
			const filteredList = state.categoriesData.filter(
				(category) => !isNil(accelerators[category.id])
			)
			const currentActiveTabId = find(
				filteredList,
				(i) => i.id === state.activeCategoryId
			)
			if (isNil(currentActiveTabId)) {
				if (filteredList.length > 0) {
					state.activeCategoryId = filteredList[0].id
				}
			}
		},
		updateInstanceData(
			state,
			action: Action<{
				prevId: string | null
				data: AcceleratorInstanceProps
				categoryId: string | null
			}>
		) {
			let accelerators: CategoriesToAcceleratorInstancesMap = cloneDeep(
				state.categoriesToAcceleratorInstancesMap
			)
			const id = action.payload.data.categoryId
			if (!isArray(accelerators[id])) {
				accelerators[id] = []
			}
			const index = findIndex(
				accelerators[id],
				(data) => data.id === action.payload.prevId
			)
			if (action.payload.prevId === action.payload.data.id) {
				accelerators[id][index] = action.payload.data
			} else {
				if (index !== -1) {
					accelerators[id].splice(index, 1)
				}
				if (
					state.selectedApps.indexOf(action.payload.data.applicationId) !== -1
				) {
					accelerators[id].push(action.payload.data)
				}
				if (accelerators[id].length < 1) {
					delete accelerators[id]
				}
				if (!isNil(action.payload.categoryId)) {
					if (
						state.selectedApps.indexOf(action.payload.data.applicationId) === -1
					) {
						state.selectedApps = [
							...state.selectedApps,
							action.payload.data.applicationId,
						]
						accelerators = {}
					}
					state.activeTab = 0
				}
			}
			state.categoriesToAcceleratorInstancesMap = accelerators
			if (isNil(action.payload.categoryId)) {
				const filteredList = state.categoriesData.filter(
					(category) => !isNil(accelerators[category.id])
				)
				const currentActiveTabId = find(
					filteredList,
					(i) => i.id === state.activeCategoryId
				)
				if (isNil(currentActiveTabId)) {
					if (filteredList.length > 0) {
						state.activeCategoryId = filteredList[0].id
					}
				}
			} else {
				state.activeCategoryId = action.payload.categoryId
			}
			if (isNil(action.payload.prevId) || isEmpty(action.payload.prevId)) {
				state.highlightApp = action.payload.data.id
			}
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(asyncActions.fetchCategories.fulfilled, (state, action) => {
			if (action && action.payload) {
				state.categoriesData = action.payload
				if (action.payload.length > 0) {
					state.activeCategoryId = action.payload[0].id
				}
			}
		})
		builder.addCase(
			asyncActions.fetchCategoriesWithMarketPlace.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					const marketplace: CategoriesToMarketPlaceMap = {}
					for (let i = 0, iLen = action.payload.content.length; i < iLen; i++) {
						const id = action.payload.content[i].id
						if (
							isArray(action.payload.content[i].accelerators) &&
							action.payload.content[i].accelerators.length > 0
						) {
							marketplace[id] = action.payload.content[i].accelerators
						}
					}
					state.categoriesToMarketPlaceMap = marketplace
				}
			}
		)
		builder.addCase(
			asyncActions.loadAcceleratorsForSelectedApps.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					const accelerators: CategoriesToAcceleratorInstancesMap = {}
					for (let i = 0, iLen = action.payload.content.length; i < iLen; i++) {
						const id = action.payload.content[i].categoryId
						if (isNil(accelerators[id])) {
							accelerators[id] = []
						}
						accelerators[id].push(action.payload.content[i])
					}
					state.categoriesToAcceleratorInstancesMap = accelerators
					const filteredList = state.categoriesData.filter(
						(category) => !isNil(accelerators[category.id])
					)
					const currentActiveTabId = find(
						filteredList,
						(i) => i.id === state.activeCategoryId
					)
					if (isNil(currentActiveTabId)) {
						if (filteredList.length > 0) {
							state.activeCategoryId = filteredList[0].id
						}
					}
				}
			}
		)
		builder.addCase(
			asyncActions.loadAcceleratorApps.fulfilled,
			(state, action) => {
				if (action && action.payload) {
					const list: AcceleratorsAppItemProps[] = []
					const selectedList: string[] = []
					for (let i = 0, iLen = action.payload.content.length; i < iLen; i++) {
						list.push({
							id: action.payload.content[i].id,
							name: action.payload.content[i].name,
							type: action.payload.content[i].type,
						})
						selectedList.push(action.payload.content[i].id)
					}
					state.appsList = list
					state.selectedApps = selectedList
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
