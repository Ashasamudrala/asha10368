import React, { useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import {
	selectActiveTab,
	selectActiveCategoryId,
	selectCategoriesData,
	getCategoriesToMarketPlaceMap,
	getSelectedApps,
	getCategoriesToAcceleratorInstancesMap,
	getAppsList,
	getSearchString,
	selectOrderBy,
	selectSortBy,
	selectViewType,
	selectHighlightAppId,
} from './accelerators.selectors'
import { actions } from './accelerators.slice'
import { actions as aiActions } from './acceleratorInstance/acceleratorInstance.slice'
import { AcceleratorsLeftMenu } from './components/AcceleratorsLeftMenu/AcceleratorsLeftMenu'
import './accelerators.scss'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import { MarketPlaceGrid } from './components/MarketPlaceGrid/MarketPlaceGrid'
import { useTranslation } from 'react-i18next'
import {
	ACCELERATORS_TEXT_EMPTY,
	ACCELERATOR_TRANSLATIONS,
	GO_TO_MP_BUTTON,
	MY_ACCELERATORS,
	MY_ACCELERATORS_CAPTION,
	MARKETPLACE,
	MARKETPLACE_CATION,
	SEARCH_ACCELERATORS,
	SEARCH_MARKETPLACE,
	NO_CARDS_TO_SHOW,
	MY_ACCELERATORS_CAPTION_IN_APP,
	ACCELERATORS,
} from '../../utilities/constants'
import { AcceleratorsCardGrid } from './components/AcceleratorsCardGrid/AcceleratorsCardGrid'
import { IsySearch } from '../../widgets/IsySearch/IsySearch'
import { AcceleratorBar } from './components/acceleratorBar/AcceleratorBar'
import {
	fetchCategoriesWithMarketPlace,
	loadAcceleratorsForSelectedApps,
	showAcceleratorHelpLandingPage,
} from './accelerators.asyncActions'
import {
	AcceleratorInstanceProps,
	AcceleratorsViewType,
	MarketplaceProps,
} from './accelerators.types'
import { AcceleratorInstanceSetIdsActionState } from './acceleratorInstance/acceleratorInstance.types'
import { AcceleratorInstance } from './acceleratorInstance/AcceleratorInstance'
import { debounce, find, isNil, isNumber } from 'lodash'
import {
	conformDeleteAcceleratorInstance,
	initAccelerators,
} from './accelerators.controller'
import { AcceleratorSortBy, OrderBy } from '../../utilities/apiEnumConstants'
import { AcceleratorsTableView } from './components/AcceleratorsTableView/AcceleratorsTableView'
import { IsySteps } from '../../widgets/IsySteps/IsySteps'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { selectGetAcceleratorsPreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import {
	getAcceleratorEditLockStep,
	getAcceleratorsSteps,
} from '../../utilities/isyStepsConfig'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import {
	CustomButtonType,
	IsyStepItemProps,
} from '../../widgets/IsySteps/IsyTour/Steps'

export function Accelerators() {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const { appid } = useParams<{ appid: string }>()
	const categories = useSelector(selectCategoriesData)
	const activeCategoryId = useSelector(selectActiveCategoryId)
	const categoriesToMarketPlaceMap = useSelector(getCategoriesToMarketPlaceMap)
	const selectedApps = useSelector(getSelectedApps)
	const instancesMap = useSelector(getCategoriesToAcceleratorInstancesMap)
	const activeTab = useSelector(selectActiveTab)
	const appsList = useSelector(getAppsList)
	const searchString = useSelector(getSearchString)
	const orderBy = useSelector(selectOrderBy)
	const sortBy = useSelector(selectSortBy)
	const viewType = useSelector(selectViewType)
	const acceleratorPreferenceData = useSelector(
		selectGetAcceleratorsPreferenceStatus
	)
	const highlightAppId = useSelector(selectHighlightAppId)
	let stepsConfigData: IsyStepItemProps[]

	const history = useHistory()
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(initAccelerators(appid))
		return () => {
			dispatch(actions.clearData(null))
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (selectedApps.length > 0) {
			dispatch(loadAcceleratorsForSelectedApps())
		}
	}, [selectedApps, dispatch])

	useEffect(() => {
		if (
			isNil(appid) &&
			acceleratorPreferenceData &&
			acceleratorPreferenceData.landingPage === true
		) {
			dispatch(showAcceleratorHelpLandingPage())
		}
	}, [acceleratorPreferenceData, dispatch]) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (!isNil(highlightAppId)) {
			setTimeout(() => handleSteps(6), 1200)
		}
	}, [highlightAppId, dispatch]) // eslint-disable-line react-hooks/exhaustive-deps

	const getFilteredCategories = (tabId: number) => {
		if (tabId === 0) {
			const filteredList = categories.filter(
				(category) => !isNil(instancesMap[category.id])
			)
			return filteredList
		} else {
			const filteredList = categories.filter(
				(category) => !isNil(categoriesToMarketPlaceMap[category.id])
			)
			return filteredList
		}
	}

	const handleTabChange = (id: number) => {
		dispatch(actions.setActiveTab(id))
		const filteredList = getFilteredCategories(id)
		if (filteredList.length > 0 && filteredList[0].id !== activeCategoryId) {
			dispatch(actions.setActiveCategoryId(filteredList[0].id))
			history.push(`#${filteredList[0].name.replace(/ /g, '_')}`)
		}
	}

	const handleFilterByAppsUpdate = (ids: string[]) => {
		dispatch(actions.setSelectedApps(ids))
	}

	const handleSelectedCategory = (id: string) => {
		dispatch(actions.setActiveCategoryId(id))
	}

	const handleGoToMarketPlace = () => {
		if (isNil(appid)) {
			dispatch(actions.setActiveTab(1))
		} else {
			history.push('/marketplace')
			setTimeout(() => {
				dispatch(actions.setActiveTab(1))
			})
		}
	}

	const handleOnSearch = (value: string) => {
		dispatch(actions.setSearchString(value))
		handleSearchDebounce()
	}

	const handleSearchDebounce = debounce(() => {
		if (isNil(appid)) {
			dispatch(fetchCategoriesWithMarketPlace())
		}
		dispatch(loadAcceleratorsForSelectedApps())
	}, 300)

	const handleOnDeleteAcceleratorInstance = (
		instance: AcceleratorInstanceProps
	) => {
		dispatch(
			conformDeleteAcceleratorInstance({
				acceleratorInstanceId: instance.id,
				categoryId: instance.categoryId,
			})
		)
	}

	const handleOnDuplicateAcceleratorInstance = (
		data: AcceleratorInstanceProps
	) => {
		dispatch(
			aiActions.showAcceleratorInstance({
				categoryId: data.categoryId,
				acceleratorId: data.acceleratorId,
				acceleratorInstanceId: data.id,
				acceleratorInstanceAppId: data.applicationId,
				isDuplicate: true,
			} as AcceleratorInstanceSetIdsActionState)
		)
	}

	const handleOnMarketPlaceSelect = (data: MarketplaceProps) => {
		dispatch(
			aiActions.showAcceleratorInstance({
				categoryId: data.categoryId,
				acceleratorId: data.id,
				acceleratorInstanceId: null,
				acceleratorInstanceAppId: null,
				isDuplicate: false,
			} as AcceleratorInstanceSetIdsActionState)
		)
	}

	const handleAcceleratorInstanceChange = (
		prevAcceleratorId: string | null,
		data: AcceleratorInstanceProps,
		categoryId: string | null
	) => {
		dispatch(
			actions.updateInstanceData({
				prevId: prevAcceleratorId,
				data,
				categoryId,
			})
		)
		if (!isNil(categoryId)) {
			setTimeout(() => {
				const category = find(categories, (c) => c.id === categoryId)
				const element = document.getElementById('new-icon')
				if (!isNil(category) && !isNil(element)) {
					var aTag = document.createElement('a')
					aTag.setAttribute('href', '#new-icon')
					aTag.click()
					history.push(`#${category.name.replace(/ /g, '_')}`)
				}
			}, 500)
		}
	}

	const handleOnAcceleratorInstanceSelect = (
		data: AcceleratorInstanceProps
	) => {
		dispatch(
			aiActions.showAcceleratorInstance({
				categoryId: data.categoryId,
				acceleratorId: data.acceleratorId,
				acceleratorInstanceId: data.id,
				acceleratorInstanceAppId: data.applicationId,
				isDuplicate: false,
			} as AcceleratorInstanceSetIdsActionState)
		)
	}

	const handleViewTypeChange = (type: AcceleratorsViewType) => {
		dispatch(actions.setViewType(type))
	}

	const handleOrderByChange = (key: OrderBy) => {
		dispatch(actions.setOrderBy(key))
	}

	const handleSortByChange = (key: AcceleratorSortBy) => {
		dispatch(actions.setSortBy(key))
	}

	const handleOrderAndSortByChange = (
		key: OrderBy,
		sortBy: AcceleratorSortBy
	) => {
		dispatch(actions.setOrderBy(key))
		dispatch(actions.setSortBy(sortBy))
		dispatch(actions.clearAcceleratorInstancesMap(null))
		dispatch(loadAcceleratorsForSelectedApps())
	}

	const handleLoadApplications = () => {
		dispatch(actions.clearAcceleratorInstancesMap(null))
		dispatch(loadAcceleratorsForSelectedApps())
	}

	const handleOnComplete = () => {
		dispatch(
			preferenceActions.updateAcceleratorsPreferences({
				status: HelpModuleStatus.INCOMPLETE,
			})
		)
	}

	const handleOnExit = (currentStep: number) => {
		let currentStepStatus
		if (currentStep === stepsConfigData.length - 1) {
			currentStepStatus = HelpModuleStatus.COMPLETED
		} else if (currentStep >= 0) {
			currentStepStatus = HelpModuleStatus.INCOMPLETE
		}

		if (currentStep !== -1) {
			dispatch(
				preferenceActions.updateAcceleratorsPreferences({
					status: currentStepStatus,
					currentStep: currentStep,
				})
			)
			dispatch(updatePreferences())
		}
	}

	const handleBeforeChange = (currentStep: number) => {
		if (currentStep === 0) {
			setTimeout(() => {
				dispatch(actions.setActiveTab(0))
			}, 200)
		} else if (currentStep === 1) {
			setTimeout(() => {
				dispatch(actions.setActiveTab(1))
			}, 200)
		} else if (currentStep === 2) {
			dispatch(
				preferenceActions.updateAcceleratorsPreferences({
					currentStep: currentStep,
				})
			)
		}
		return true
	}

	const handleDrawerExit = (currentStep: number, type: string) => {
		if (currentStep === 5 && type === CustomButtonType.EXIT) {
			dispatch(
				preferenceActions.updateAcceleratorsPreferences({
					status: HelpModuleStatus.INCOMPLETE,
				})
			)
			dispatch(aiActions.clearData(null))
		} else if (currentStep === 5 && type === CustomButtonType.COMPLETE) {
			dispatch(
				preferenceActions.updateAcceleratorsPreferences({
					currentStep: 4,
				})
			)
		}
	}

	const handleSteps = (currentStep: number) => {
		if (
			acceleratorPreferenceData &&
			acceleratorPreferenceData.status === HelpModuleStatus.INPROGRESS &&
			isNumber(currentStep)
		) {
			setTimeout(() => {
				dispatch(
					preferenceActions.updateAcceleratorsPreferences({
						currentStep: currentStep + 1,
					})
				)
			}, 500)
		}
	}

	const renderLeftMenu = () => {
		const activeCategory = getFilteredCategories(activeTab)[1]
		const categoryName = activeCategory && activeCategory.name
		const categoryId = activeCategory && activeCategory.id
		const categoryData =
			categoryId &&
			categoriesToMarketPlaceMap[categoryId] &&
			categoriesToMarketPlaceMap[categoryId][0] &&
			categoriesToMarketPlaceMap[categoryId][0].name
		stepsConfigData = getAcceleratorsSteps(categoryName, categoryData)

		if (
			acceleratorPreferenceData &&
			acceleratorPreferenceData.currentStep === 10
		) {
			stepsConfigData = stepsConfigData.concat(getAcceleratorEditLockStep())
		}
		return (
			<div className='left'>
				<AcceleratorsLeftMenu
					categories={getFilteredCategories(activeTab)}
					activeCategoryId={activeCategoryId}
					activeTab={activeTab}
					appsList={appsList}
					selectedApps={selectedApps}
					isViewForApp={!isNil(appid)}
					onFilterByAppsUpdate={handleFilterByAppsUpdate}
					onTabChange={handleTabChange}
					onSelectCategory={handleSelectedCategory}
					updateSteps={handleSteps}
				/>
				{isNil(appid) && (
					<IsySteps
						steps={stepsConfigData}
						isEnabled={
							acceleratorPreferenceData &&
							acceleratorPreferenceData.status === HelpModuleStatus.INPROGRESS
						}
						initialStep={
							acceleratorPreferenceData && acceleratorPreferenceData.currentStep
						}
						updateSteps={true}
						getCustomButtonData={handleDrawerExit}
						onBeforeChange={handleBeforeChange}
						onComplete={handleOnComplete}
						onExit={handleOnExit}
					/>
				)}
			</div>
		)
	}

	const renderNoMarketplaceScreen = () => {
		return (
			<div className='accelerators-no-cards'>
				<img
					className='accelerators-icon'
					alt='Accelerator'
					src='/images/Accelerator.svg'
				/>
				<span className='accelerator-text'>{t(NO_CARDS_TO_SHOW)}</span>
			</div>
		)
	}

	const renderMarketPlaceContentData = () => {
		const children = []
		for (let i = 0, iLen = categories.length; i < iLen; i++) {
			const category = categories[i]
			const list = categoriesToMarketPlaceMap[category.id] || []
			if (list.length > 0) {
				children.push(
					<div
						id={category.name.replace(/ /g, '_')}
						className='category-container'
						key={i}
					>
						<p className='category-label'>{`${category.name} (${list.length})`}</p>
						<div className='media-container'>
							<MarketPlaceGrid
								className={`${
									i !== 0 ? 'isy-accelerators-grid-card-container' : ''
								}`}
								updateSteps={handleSteps}
								items={list}
								onSelect={handleOnMarketPlaceSelect}
							/>
						</div>
					</div>
				)
			}
		}
		return children
	}

	const renderNoAcceleratorsScreen = () => {
		return (
			<div className='accelerators'>
				<img
					className='accelerators-icon'
					alt='Accelerator'
					src='/images/Accelerator.svg'
				/>
				<span className='accelerator-text'>{t(ACCELERATORS_TEXT_EMPTY)}</span>
				<IsyButton
					className='primary-btn isy-steps-go-to-mp-btn'
					onClick={handleGoToMarketPlace}
				>
					{t(GO_TO_MP_BUTTON)}
				</IsyButton>
			</div>
		)
	}

	const renderAcceleratorsContentGrid = () => {
		const children = []
		for (let i = 0, iLen = categories.length; i < iLen; i++) {
			const category = categories[i]
			const list = instancesMap[category.id] || []
			if (list.length > 0) {
				children.push(
					<div
						id={category.name.replace(/ /g, '_')}
						className='category-container'
						key={i}
					>
						<p className='category-label'>{`${category.name} (${list.length})`}</p>
						<div className='media-container'>
							<AcceleratorsCardGrid
								list={list}
								highlightAppId={highlightAppId}
								includeDuplicate={isNil(appid)}
								onSelect={handleOnAcceleratorInstanceSelect}
								onDelete={handleOnDeleteAcceleratorInstance}
								onDuplicate={handleOnDuplicateAcceleratorInstance}
							/>
						</div>
					</div>
				)
			}
		}
		return children
	}

	const renderSearch = () => {
		return (
			<IsySearch
				value={searchString}
				onChange={handleOnSearch}
				onCancel={handleOnSearch}
				placeholder={
					activeTab === 0 ? t(SEARCH_ACCELERATORS) : t(SEARCH_MARKETPLACE)
				}
			/>
		)
	}

	const renderAcceleratorsContentTable = () => {
		return (
			<AcceleratorsTableView
				categories={categories}
				instancesMap={instancesMap}
				orderBy={orderBy}
				sortBy={sortBy}
				highlightAppId={highlightAppId}
				includeDuplicate={isNil(appid)}
				onSelect={handleOnAcceleratorInstanceSelect}
				onDelete={handleOnDeleteAcceleratorInstance}
				onDuplicate={handleOnDuplicateAcceleratorInstance}
				onOrderAndSortByChange={handleOrderAndSortByChange}
			/>
		)
	}

	const renderMyAcceleratorsContent = () => {
		if (Object.keys(instancesMap).length === 0) {
			return renderNoAcceleratorsScreen()
		}
		if (viewType === AcceleratorsViewType.GRID) {
			return renderAcceleratorsContentGrid()
		} else {
			return renderAcceleratorsContentTable()
		}
	}

	const renderMarketplaceContent = () => {
		if (Object.keys(categoriesToMarketPlaceMap).length === 0) {
			return renderNoMarketplaceScreen()
		}
		return renderMarketPlaceContentData()
	}

	const renderContentData = () => {
		if (activeTab === 0) {
			return (
				<>
					<AcceleratorBar
						viewType={viewType}
						sortBy={sortBy}
						orderBy={orderBy}
						onViewTypeChange={handleViewTypeChange}
						onOrderByChange={handleOrderByChange}
						onSortByChange={handleSortByChange}
						onCloseSortBy={handleLoadApplications}
					/>
					{renderMyAcceleratorsContent()}
				</>
			)
		}
		return renderMarketplaceContent()
	}

	const renderRightPartOfHeader = () => {
		return (
			<div className='header-right-part'>
				{renderGoToMarketplace()}
				{renderSearch()}
			</div>
		)
	}
	const renderGoToMarketplace = () => {
		if (isNil(appid)) {
			return null
		}
		return (
			<IsyButton className='primary-btn' onClick={handleGoToMarketPlace}>
				{t(GO_TO_MP_BUTTON)}
			</IsyButton>
		)
	}

	const renderAcceleratorsLandingPage = () => {
		return (
			<div className='accelerators-page'>
				<div className='accelerator-header'>
					<div className='header-text'>
						<div className='header-text-content'>
							<span>
								{isNil(appid)
									? activeTab === 0
										? t(MY_ACCELERATORS)
										: t(MARKETPLACE)
									: t(ACCELERATORS)}
							</span>
							<p className='header-caption'>
								{isNil(appid)
									? activeTab === 0
										? t(MY_ACCELERATORS_CAPTION)
										: t(MARKETPLACE_CATION)
									: t(MY_ACCELERATORS_CAPTION_IN_APP)}
							</p>
						</div>
						{renderRightPartOfHeader()}
					</div>
				</div>
				<div className='content-container'>{renderContentData()}</div>
			</div>
		)
	}

	const renderContent = () => {
		return <div className='right'>{renderAcceleratorsLandingPage()}</div>
	}

	return (
		<div className='accelerators-container'>
			{renderLeftMenu()}
			{renderContent()}
			<AcceleratorInstance
				acceleratorsTourEnable={
					acceleratorPreferenceData &&
					acceleratorPreferenceData.status === HelpModuleStatus.INPROGRESS
				}
				updateSteps={handleSteps}
				onUpdate={handleAcceleratorInstanceChange}
			/>
		</div>
	)
}

export default Accelerators
