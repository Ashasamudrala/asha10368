import slice from './accelerators.slice'
import { selectSlice as baseSelector } from './base.selector'
import { RootState } from '../../base.types'
import {
	AcceleratorInstanceProps,
	AcceleratorsAppItemProps,
	AcceleratorsState,
	CategoriesProps,
	CategoriesToAcceleratorInstancesMap,
	CategoriesToMarketPlaceMap,
	AcceleratorsViewType,
} from './accelerators.types'
import { AcceleratorSortBy, OrderBy } from '../../utilities/apiEnumConstants'
import { find, isArray } from 'lodash'

export const selectSlice = (state: RootState): AcceleratorsState => {
	return baseSelector(state)[slice.name]
}

export const selectOrderBy = (state: RootState): OrderBy => {
	return selectSlice(state).orderBy
}

export const selectSortBy = (state: RootState): AcceleratorSortBy => {
	return selectSlice(state).sortBy
}

export const selectViewType = (state: RootState): AcceleratorsViewType => {
	return selectSlice(state).viewType
}

export const selectActiveTab = (state: RootState): number =>
	selectSlice(state).activeTab

export const selectActiveCategoryId = (state: RootState): string =>
	selectSlice(state).activeCategoryId

export const selectCategoriesData = (state: RootState): CategoriesProps[] =>
	selectSlice(state).categoriesData

export const getCategoriesToMarketPlaceMap = (
	state: RootState
): CategoriesToMarketPlaceMap => selectSlice(state).categoriesToMarketPlaceMap

export const getCategoriesToAcceleratorInstancesMap = (
	state: RootState
): CategoriesToAcceleratorInstancesMap =>
	selectSlice(state).categoriesToAcceleratorInstancesMap

export const getAcceleratorInstanceBasedOnId = (
	state: RootState,
	acceleratorInstanceId: string,
	categoryId: string
): AcceleratorInstanceProps | null => {
	const map = getCategoriesToAcceleratorInstancesMap(state)
	if (isArray(map[categoryId])) {
		return (
			find(map[categoryId], (item) => item.id === acceleratorInstanceId) || null
		)
	}
	return null
}

export const getSelectedApps = (state: RootState): string[] =>
	selectSlice(state).selectedApps

export const getAppsList = (state: RootState): AcceleratorsAppItemProps[] =>
	selectSlice(state).appsList

export const getSearchString = (state: RootState): string =>
	selectSlice(state).searchString

export const selectHighlightAppId = (state: RootState): string | null =>
	selectSlice(state).highlightApp
