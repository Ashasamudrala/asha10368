import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import './marketPlaceCard.scss'
import { MarketplaceProps } from '../../../accelerators.types'
import { getAcceleratorsIconUrl } from '../../../accelerators.utilities'
import { isFunction } from 'lodash'
export interface MarketPlaceCardDataProps {
	data: MarketplaceProps
	className?: string
	onSelect: (data: MarketplaceProps) => void
	updateSteps?: (stepNumber: number) => void
}
export function MarketPlaceCard(props: MarketPlaceCardDataProps) {
	const { data, className } = props

	const handleOnClick = () => {
		props.onSelect(data)
		if (isFunction(props.updateSteps)) {
			props.updateSteps(3)
		}
	}

	const renderCardImage = () => {
		return (
			<img
				src={getAcceleratorsIconUrl(data.metadata.imageRef)}
				alt={data.metadata.imageRef}
			/>
		)
	}
	const renderCardContent = () => {
		return (
			<CardContent className='card-content'>
				<span title={data.name}>{data.name}</span>
			</CardContent>
		)
	}
	const renderCardActionArea = () => {
		return (
			<CardActionArea className='card-actions'>
				{renderCardImage()}
				{renderCardContent()}
			</CardActionArea>
		)
	}
	const renderCard = () => {
		return (
			<Card className={`${className} card-component`} onClick={handleOnClick}>
				{renderCardActionArea()}
			</Card>
		)
	}

	return renderCard()
}
