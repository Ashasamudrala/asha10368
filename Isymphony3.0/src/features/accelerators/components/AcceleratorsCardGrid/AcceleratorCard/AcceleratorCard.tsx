import React, { useState } from 'react'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import {
	ACCELERATOR_TRANSLATIONS,
	LAST_MODIFIED,
	DELETE,
	DUPLICATE,
	OWNER,
} from '../../../../../utilities/constants'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import IconButton from '@material-ui/core/IconButton'
import './acceleratorCard.scss'
import { IsyAvatarImage } from '../../../../../widgets/IsyAvatarImage/IsyAvatarImage'
import { IsyPopover } from '../../../../../widgets/IsyPopover/IsyPopover'
import { isNil } from 'lodash'
import {
	AcceleratorInstanceProps,
	AcceleratorsCardMoreOver,
} from '../../../accelerators.types'
import { getAcceleratorsIconUrl } from '../../../accelerators.utilities'

export interface AcceleratorDataProps {
	includeDuplicate: boolean
	data: AcceleratorInstanceProps
	highlightAppId: string | null
	onSelect: (data: AcceleratorInstanceProps) => void
	onDelete: (data: AcceleratorInstanceProps) => void
	onDuplicate: (data: AcceleratorInstanceProps) => void
}
export function AcceleratorCard(props: AcceleratorDataProps) {
	const { data } = props
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)
	const lastModifiedDate = moment(data.lastModifiedTimestamp).format('ll')

	const getPopoverActions = () => {
		const list = []
		if (props.includeDuplicate) {
			list.push({
				name: t(DUPLICATE),
				key: AcceleratorsCardMoreOver.DUPLICATE,
			})
		}
		list.push({
			name: t(DELETE),
			key: AcceleratorsCardMoreOver.DELETE,
		})
		return list
	}

	const handleOnClickMoreVertIcon = (e: React.MouseEvent<SVGSVGElement>) => {
		e.stopPropagation()
		setPopoverAnchorEl(e.currentTarget)
	}

	const handlePopoverClose = (e: any) => {
		e.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleOnClick = () => {
		props.onSelect(data)
	}

	const handleMenuItems = (
		e: React.MouseEvent<HTMLLIElement>,
		selectedItem: string
	) => {
		e.stopPropagation()
		setPopoverAnchorEl(null)
		switch (selectedItem) {
			case AcceleratorsCardMoreOver.DUPLICATE: {
				props.onDuplicate(props.data)
				break
			}
			case AcceleratorsCardMoreOver.DELETE: {
				props.onDelete(props.data)
				break
			}
		}
	}

	const renderPopOver = () => {
		if (isNil(popoverAnchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={getPopoverActions()}
				onSelect={handleMenuItems}
				className='pop-over-menu-list'
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
			></IsyPopover>
		)
	}

	const renderAvatarImage = (userName: string, imageData: string) => {
		return (
			<IsyAvatarImage
				imageClassName='profile-avatar'
				defaultName={userName}
				imageData={imageData}
			/>
		)
	}

	const renderAcceleratorImage = (imageData: string) => {
		return <img alt={imageData} src={getAcceleratorsIconUrl(imageData)} />
	}

	const renderTitleContent = () => {
		return (
			<>
				<span className='owner-text'>{t(OWNER)} : </span>
				<span className='user-name'>{data.createdByUserDisplayName}</span>
			</>
		)
	}

	const renderActions = () => {
		return (
			<>
				<IconButton className='more-vertical-icon'>
					<MoreVertIcon onClick={handleOnClickMoreVertIcon} />
				</IconButton>
				{popoverAnchorEl && renderPopOver()}
			</>
		)
	}

	const renderHeaderName = () => {
		return (
			<>
				<span className='card-header-title'>{data.acceleratorName}</span>
				{data.id === props.highlightAppId && (
					<img
						alt='new'
						id='new-icon'
						className='new-icon'
						src='/images/acceleratorsIcons/newBadge.svg'
					/>
				)}
			</>
		)
	}

	const renderCardHeader = () => {
		return (
			<CardHeader
				className='card-header'
				avatar={renderAcceleratorImage(data.imageRef)}
				title={renderHeaderName()}
				action={renderActions()}
				classes={{
					action: 'card-actions',
					avatar: 'card-avatar',
					title: 'avatar-title',
				}}
			/>
		)
	}

	const renderContentTitle = () => {
		return (
			<CardHeader
				title={data.name}
				subheader={`${t(LAST_MODIFIED)}: ${lastModifiedDate}`}
				classes={{ subheader: 'sub-header', title: 'title' }}
			/>
		)
	}

	const renderContentDescription = () => {
		return (
			<CardContent className='card-description'>
				<span>{data.description}</span>
			</CardContent>
		)
	}

	const renderCardContent = () => {
		return (
			<div className='accelerators-card-header-content'>
				{renderContentTitle()}
				{renderContentDescription()}
			</div>
		)
	}

	const renderCardFooter = () => {
		return (
			<CardHeader
				className='card-header'
				title={renderTitleContent()}
				avatar={renderAvatarImage(
					data.createdByUserDisplayName,
					data.createdUserProfilePic
				)}
				classes={{ action: 'card-actions', avatar: 'card-avatar' }}
			/>
		)
	}

	const renderCard = () => {
		return (
			<Card
				className={`accelerators-card  ${
					data.id === props.highlightAppId ? 'new-instance' : ''
				}`}
				onClick={handleOnClick}
			>
				{renderCardHeader()}
				{renderCardContent()}
				{renderCardFooter()}
			</Card>
		)
	}

	return <div className='accelerators-card-container'>{renderCard()}</div>
}
