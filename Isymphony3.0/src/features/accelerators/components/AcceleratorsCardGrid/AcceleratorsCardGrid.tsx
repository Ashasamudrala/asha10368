import React from 'react'
import { CardViewSecondIcon } from '../../../../icons/CardViewSecond'
import { useTranslation } from 'react-i18next'
import {
	ACCELERATOR_TRANSLATIONS,
	NO_ACCELERATORS,
} from '../../../../utilities/constants'
import './acceleratorsCardGrid.scss'
import { AcceleratorCard } from './AcceleratorCard/AcceleratorCard'
import { AcceleratorInstanceProps } from '../../accelerators.types'

export interface AcceleratorsCardGridProps {
	includeDuplicate: boolean
	list: AcceleratorInstanceProps[]
	highlightAppId: string | null
	onSelect: (data: AcceleratorInstanceProps) => void
	onDelete: (data: AcceleratorInstanceProps) => void
	onDuplicate: (data: AcceleratorInstanceProps) => void
}

export function AcceleratorsCardGrid(props: AcceleratorsCardGridProps) {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)

	const renderNoCardsView = () => {
		return (
			<div className='empty-card-view'>
				<CardViewSecondIcon className='icon' />
				<div className='text'>{t(NO_ACCELERATORS)}</div>
			</div>
		)
	}

	const renderList = () => {
		return props.list.map((data: AcceleratorInstanceProps, i: number) => (
			<AcceleratorCard
				data={data}
				key={i}
				highlightAppId={props.highlightAppId}
				includeDuplicate={props.includeDuplicate}
				onSelect={props.onSelect}
				onDelete={props.onDelete}
				onDuplicate={props.onDuplicate}
			/>
		))
	}

	return (
		<div className='accelerators-grid-card-container'>
			{props.list.length === 0 ? renderNoCardsView() : renderList()}
		</div>
	)
}
