import React, { useState } from 'react'
import Divider from '@material-ui/core/Divider'
import { useTranslation } from 'react-i18next'
import {
	ACCELERATOR_MULTI_APP_SELECTED,
	ACCELERATOR_ONE_APP_SELECTED,
	ACCELERATOR_TRANSLATIONS,
	FILTER_BY_APPS,
	SEE_ALL,
} from '../../../../../utilities/constants'
import { AcceleratorFilterByAppPanel } from './AcceleratorFilterByAppPanel/AcceleratorFilterByAppPanel'
import { IsyCheckbox } from '../../../../../widgets/IsyCheckbox/IsyCheckbox'
import { AcceleratorsAppItemProps } from '../../../accelerators.types'

export interface AcceleratorFilterByAppProps {
	appsList: AcceleratorsAppItemProps[]
	selectedApps: string[]
	onUpdate: (ids: string[]) => void
}

export function AcceleratorFilterByApp(props: AcceleratorFilterByAppProps) {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const [isPanelOpen, setPanelOpen] = useState<boolean>(false)

	const getSortedApps = () => {
		const selected = []
		const notSelected = []
		for (let i = 0, iLen = props.appsList.length; i < iLen; i++) {
			if (props.selectedApps.indexOf(props.appsList[i].id) === -1) {
				notSelected.push(props.appsList[i])
			} else {
				selected.push(props.appsList[i])
			}
		}
		return [...selected, ...notSelected]
	}

	const handlePanelOpen = (isPanelOpen: boolean) => {
		setPanelOpen(isPanelOpen)
	}

	const handleCheckItem = (item: AcceleratorsAppItemProps) => {
		const index = props.selectedApps.indexOf(item.id)
		const updatedList = [...props.selectedApps]
		if (index === -1) {
			updatedList.push(item.id)
		} else {
			updatedList.splice(index, 1)
		}
		props.onUpdate(updatedList)
	}

	const handlePanelOnUpdate = (ids: string[]) => {
		props.onUpdate(ids)
		setPanelOpen(false)
	}

	const renderCheckBox = (item: AcceleratorsAppItemProps) => {
		return (
			<IsyCheckbox
				className='checkbox-group'
				label={item.name}
				value={props.selectedApps.indexOf(item.id) !== -1}
				onChange={() => handleCheckItem(item)}
			/>
		)
	}

	return (
		<>
			<Divider className='divider' />
			<span className='category-filter'>{t(FILTER_BY_APPS)}</span>
			<span className='counter-apps'>
				{props.selectedApps.length === 1
					? t(ACCELERATOR_ONE_APP_SELECTED)
					: t(ACCELERATOR_MULTI_APP_SELECTED, {
							count: props.selectedApps.length,
					  })}
			</span>
			<div className='list-container'>
				{getSortedApps()
					.slice(0, 4)
					.map((item, i) => {
						return (
							<div className='checkbox-group-container' key={i}>
								{renderCheckBox(item)}
							</div>
						)
					})}
				<div className='access-filters'>
					<span
						onClick={() => handlePanelOpen(true)}
						className='select-clear-text'
					>
						{t(SEE_ALL)}
					</span>
					{isPanelOpen && (
						<AcceleratorFilterByAppPanel
							appsList={getSortedApps()}
							selectedApps={props.selectedApps}
							onUpdate={handlePanelOnUpdate}
							onClose={() => handlePanelOpen(false)}
						/>
					)}
				</div>
			</div>
		</>
	)
}
