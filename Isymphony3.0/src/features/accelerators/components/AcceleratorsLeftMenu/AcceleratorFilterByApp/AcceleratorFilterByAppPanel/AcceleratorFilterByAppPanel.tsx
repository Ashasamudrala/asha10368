import React, { useEffect, useState } from 'react'
import {
	ACCELERATOR_APPLY,
	COMMON_CLEAR_ALL,
	COMMON_SELECT_ALL,
	ACCELERATOR_TRANSLATIONS,
	ALL_CHANNELS,
	FILTER_BY_APPS,
	// MOBILE,
	// WEB,
} from '../../../../../../utilities/constants'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import { Drawer } from '@material-ui/core'
import { useTranslation } from 'react-i18next/'
import { AcceleratorsAppItemProps } from '../../../../accelerators.types'
import './acceleratorFilterByAppPanel.scss'
import { IsyCheckbox } from '../../../../../../widgets/IsyCheckbox/IsyCheckbox'
// import { AppType } from '../../../../../../utilities/apiEnumConstants'
import { IsyButton } from '../../../../../../widgets/IsyButton/IsyButton'

export interface FilterByTypeData {
	id: string
	name: string
}

export interface AcceleratorFilterByType {
	filterByTypeData: FilterByTypeData[]
}

export interface AcceleratorsFilterData {
	filterByType: AcceleratorFilterByType
}

export interface AcceleratorFilterByAppPanelProps {
	appsList: AcceleratorsAppItemProps[]
	selectedApps: string[]
	onUpdate: (ids: string[]) => void
	onClose: () => void
}

export function AcceleratorFilterByAppPanel(
	props: AcceleratorFilterByAppPanelProps
) {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const [selectedAppType, setSelectedAppType] = useState<string>('')
	const [selectedApps, setSelectedApps] = useState<string[]>([])

	useEffect(() => {
		setSelectedApps(props.selectedApps)
	}, [props.selectedApps])

	const getFilterBy = () => [
		{
			id: '',
			name: t(ALL_CHANNELS),
		},
		// {
		// 	id: AppType.WEB,
		// 	name: t(WEB),
		// },
		// {
		// 	id: AppType.MOBILE,
		// 	name: t(MOBILE),
		// },
	]

	const getFilteredAppList = () => {
		if (selectedAppType === '') {
			return props.appsList
		}
		return props.appsList.filter((item) => item.type === selectedAppType)
	}

	const handleSelectedFilterBy = (id: string) => {
		setSelectedAppType(id)
	}

	const handleClose = () => {
		props.onClose()
	}

	const handleApply = () => {
		props.onUpdate(selectedApps)
	}

	const handleSelectAll = () => {
		setSelectedApps(props.appsList.map((d) => d.id))
	}

	const handleClearAll = () => {
		setSelectedApps([])
	}

	const handleCheckItem = (item: AcceleratorsAppItemProps) => {
		const index = selectedApps.indexOf(item.id)
		const updatedList = [...selectedApps]
		if (index === -1) {
			updatedList.push(item.id)
		} else {
			updatedList.splice(index, 1)
		}
		setSelectedApps(updatedList)
	}

	const renderCheckBox = (item: AcceleratorsAppItemProps) => {
		return (
			<IsyCheckbox
				className='checkbox-group'
				label={item.name}
				value={selectedApps.indexOf(item.id) !== -1}
				onChange={() => handleCheckItem(item)}
			/>
		)
	}

	const renderHeader = () => {
		return (
			<div className='header-section'>
				<span className='header-title'>{t(FILTER_BY_APPS)}</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
				/>
			</div>
		)
	}

	const renderFilterBy = (
		item: { id: string; name: string },
		index: number
	) => {
		return (
			<div
				onClick={() => handleSelectedFilterBy(item.id)}
				className={`category ${
					selectedAppType === item.id ? 'list-selected' : ''
				}`}
				key={index}
			>
				{item.name}
			</div>
		)
	}

	const renderLeftPart = () => {
		return <div className='left'>{getFilterBy().map(renderFilterBy)}</div>
	}

	const renderRightPart = () => {
		return (
			<div className='right'>
				<div className='top-actions'>
					<span className='select-all' onClick={handleSelectAll}>
						{t(COMMON_SELECT_ALL)}
					</span>
					<span className='clear-all' onClick={handleClearAll}>
						{t(COMMON_CLEAR_ALL)}
					</span>
				</div>
				<div className='scroll-container'>
					{getFilteredAppList().map((item, i) => {
						return (
							<div className='checkbox-group-container' key={i}>
								{renderCheckBox(item)}
							</div>
						)
					})}
				</div>
			</div>
		)
	}

	const renderBottom = () => {
		return (
			<div className='bottom-section'>
				<IsyButton onClick={handleApply} className='primary-btn'>
					{t(ACCELERATOR_APPLY)}
				</IsyButton>
			</div>
		)
	}

	return (
		<Drawer
			className={'accelerator-filter '}
			open={true}
			anchor='right'
			classes={{
				paper: 'drawer-paper',
			}}
		>
			{renderHeader()}
			<div className='accelerator-filter-parent'>
				{renderLeftPart()}
				{renderRightPart()}
			</div>
			{renderBottom()}
		</Drawer>
	)
}
