import React from 'react'
import { AcceleratorsCategories } from './acceleratorsCategories'
import { AcceleratorFilterByApp } from './AcceleratorFilterByApp/AcceleratorsFilterByApp'
import './acceleratorsLeftMenu.scss'
import {
	AcceleratorsAppItemProps,
	CategoriesProps,
} from '../../accelerators.types'
import { useTranslation } from 'react-i18next'
import {
	ACCELERATOR_TRANSLATIONS,
	MARKETPLACE,
	MY_ACCELERATORS,
} from '../../../../utilities/constants'
import { Divider } from '@material-ui/core'
import { isFunction } from 'lodash'

export interface AcceleratorsLeftMenuProps {
	categories: CategoriesProps[]
	activeCategoryId: string
	activeTab: number
	appsList: AcceleratorsAppItemProps[]
	selectedApps: string[]
	isViewForApp: boolean
	onFilterByAppsUpdate: (ids: string[]) => void
	onTabChange: (id: number) => void
	onSelectCategory: (id: string) => void
	updateSteps?: (stepNumber: number) => void
}

export function AcceleratorsLeftMenu(props: AcceleratorsLeftMenuProps) {
	const { activeTab, appsList, activeCategoryId, categories } = props

	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)

	const getTabsConfig = () => [
		{
			id: 0,
			name: t(MY_ACCELERATORS),
			className: 'my-accelerators',
		},
		{
			id: 1,
			name: t(MARKETPLACE),
			className: 'marketplace',
		},
	]

	const handleSteps = (stepNumber: number) => {
		if (isFunction(props.updateSteps)) {
			props.updateSteps(stepNumber)
		}
	}

	const renderCategories = () => {
		return (
			<AcceleratorsCategories
				activeCategoryId={activeCategoryId}
				onSelectedCategory={props.onSelectCategory}
				categories={categories}
				updateSteps={handleSteps}
			/>
		)
	}

	const renderContent = () => {
		if (activeTab === 1 || props.isViewForApp) {
			return null
		}
		return (
			<AcceleratorFilterByApp
				selectedApps={props.selectedApps}
				appsList={appsList}
				onUpdate={props.onFilterByAppsUpdate}
			/>
		)
	}

	const renderOtherSection = () => {
		return (
			<div className='isy-tabs-content'>
				{!props.isViewForApp && <Divider className='divider' />}
				{renderCategories()}
				{renderContent()}
			</div>
		)
	}

	const handleTab = (id: number) => {
		props.onTabChange(id)
		if (isFunction(props.updateSteps)) {
			props.updateSteps(id === 0 ? 0 : 1)
		}
	}

	const renderTab = (tab: { id: number; name: string; className: string }) => {
		return (
			<div
				key={tab.id}
				className={
					'isy-tab-header ' +
					tab.className +
					(tab.id === activeTab ? ` active` : '')
				}
				onClick={() => handleTab(tab.id)}
			>
				<div>{tab.name}</div>
			</div>
		)
	}

	const renderTabs = () => {
		return <div className='isy-tabs-root'>{getTabsConfig().map(renderTab)}</div>
	}

	return (
		<div className='acc-left-menu'>
			{!props.isViewForApp && renderTabs()}
			{renderOtherSection()}
		</div>
	)
}
