import React, { useState } from 'react'
import { cloneDeep, find, isNil, pick } from 'lodash'
import { useTranslation } from 'react-i18next'
import { DBTable } from '../../../databases/components/DBTable/DBTable'
import {
	AttributeTypeProps,
	AttributeTypesConstraintProps,
} from '../../../databases/databases.types'
import {
	ACCELERATOR_PASSWORD_COLUMN,
	ACCELERATOR_TRANSLATIONS,
	ACCELERATOR_USERNAME_COLUMN,
	ACCELERATOR_USER_ID_COLUMN,
} from '../../../../utilities/constants'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import './TableVIew.scss'
import { DBColumnPopOver } from '../../../databases/components/DBTablePanel/DBColumnPopOver/DbColumnPopOver'
import {
	DatabaseTableAttributeProps,
	DatabaseTableConstraintsProps,
	DatabaseTableProps,
	TableAttributeKeyTypes,
} from '../../../databases/schema/schema.types'
import { defaultConstraints } from '../../../databases/schema/schema.slice'

export interface TableViewProps {
	data: DatabaseTableProps
	attributeTypes: AttributeTypeProps[]
	isViewMode: boolean
	onUpdate: (data: DatabaseTableProps) => void
}

export function TableView(props: TableViewProps) {
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)
	const [anchorEl, setAnchorEl] = useState<{
		element: Element
		index: number
	} | null>(null)

	const getAttributeTypeConstraints = (index: number) => {
		const attributeType = find(
			props.attributeTypes,
			(a) => a.value === props.data.attributes[index].type
		)
		if (!isNil(attributeType)) {
			return attributeType.constraints
		}
		return []
	}

	const handleAttrChange = (
		attr: Partial<DatabaseTableAttributeProps>,
		_: number,
		attrIndex: number
	) => {
		const updatedAttrs = cloneDeep(props.data.attributes)
		if (!isNil(attr.type)) {
			const attribute = find(props.attributeTypes, (a) => a.value === attr.type)
			if (!isNil(attribute)) {
				const pathsAllowed = attribute.constraints.map(
					(c: AttributeTypesConstraintProps) => c.name
				)
				const filteredAdjConstraints = pick(
					updatedAttrs[attrIndex].constraints,
					pathsAllowed
				)
				const filteredDefaultConstraints = pick(
					defaultConstraints,
					pathsAllowed
				)
				attr.constraints = Object.assign(
					{},
					filteredDefaultConstraints,
					filteredAdjConstraints
				)
			}
		}
		updatedAttrs[attrIndex] = { ...updatedAttrs[attrIndex], ...attr }
		props.onUpdate({ ...props.data, attributes: updatedAttrs })
	}

	const handleChangeOfConstraint = (
		attrIndex: number,
		data: Partial<DatabaseTableConstraintsProps>
	) => {
		const updatedAttrs = cloneDeep(props.data.attributes)
		updatedAttrs[attrIndex] = {
			...updatedAttrs[attrIndex],
			constraints: {
				...updatedAttrs[attrIndex].constraints,
				...data,
			},
		}
		props.onUpdate({ ...props.data, attributes: updatedAttrs })
	}

	const handleClosePopOver = () => {
		setAnchorEl(null)
	}

	const handleClickOptions = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>,
		index: number
	) => {
		event.stopPropagation()
		setAnchorEl({ element: event.currentTarget, index })
	}

	const handleKeyChange = (
		_: number,
		attrIndex: number,
		key: TableAttributeKeyTypes
	) => {
		const updatedAttrs = cloneDeep(props.data.attributes)
		switch (key) {
			case TableAttributeKeyTypes.NONE: {
				updatedAttrs[attrIndex] = {
					...updatedAttrs[attrIndex],
					constraints: {
						...updatedAttrs[attrIndex].constraints,
						unique: false,
					},
				}
				break
			}
			case TableAttributeKeyTypes.UNIQUE_KEY: {
				updatedAttrs[attrIndex] = {
					...updatedAttrs[attrIndex],
					constraints: {
						...updatedAttrs[attrIndex].constraints,
						unique: true,
					},
				}
				break
			}
		}
		props.onUpdate({ ...props.data, attributes: updatedAttrs })
	}

	return (
		<div className='accelerators-table-view'>
			<div className='accelerators-table-summary'>
				<div className='top' />
				<div className='label'>{t(ACCELERATOR_USER_ID_COLUMN)}</div>
				<div className='label'>{t(ACCELERATOR_USERNAME_COLUMN)}</div>
				<div className='label'>{t(ACCELERATOR_PASSWORD_COLUMN)}</div>
			</div>
			<div className='accelerators-table'>
				<DBTable
					table={props.data}
					attributeTypes={props.attributeTypes}
					isEditMode={!props.isViewMode}
					isEditable={true}
					tableIndex={0}
					isSelected={false}
					onAttrChange={handleAttrChange}
					isLimitedView={true}
					onSetKey={handleKeyChange}
				/>
			</div>
			<div className='accelerators-table-actions'>
				<div className='top' />
				{props.data.attributes.map((attr, index) => {
					return (
						<MoreHorizIcon
							onClick={(e) => handleClickOptions(e, index)}
							className='more'
						/>
					)
				})}
			</div>
			{!isNil(anchorEl) && (
				<DBColumnPopOver
					anchorEl={anchorEl.element}
					isLimitedView={true}
					isViewMode={props.isViewMode}
					constraints={props.data.attributes[anchorEl.index].constraints}
					reference={props.data.attributes[anchorEl.index].reference}
					constraintsConfig={getAttributeTypeConstraints(anchorEl.index)}
					onClose={handleClosePopOver}
					onChangeOfConstraint={(data) =>
						handleChangeOfConstraint(anchorEl.index, data)
					}
					onKeyChange={(data) => handleKeyChange(0, anchorEl.index, data)}
				/>
			)}
		</div>
	)
}
