import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil, omit, isEmpty } from 'lodash'
import { selectGetLoginUserId } from '../../../authAndPermissions/loginUserDetails.selectors'
import { RootState } from '../../../base.types'
import doAsync from '../../../infrastructure/doAsync'
import {
	ACCELERATORS_GET_ACCELERATOR_DATA,
	ACCELERATORS_GET_ACCELERATOR_INSTANCES,
	ACCELERATORS_GET_ALL_APPS_EXCLUDE,
	ACCELERATORS_GET_ACCELERATOR_INSTANCE,
	ACCELERATORS_GET_APP,
	ACCELERATORS_VALIDATE_NAME_FOR_DUPLICATE,
	ACCELERATORS_GET_ATTRIBUTE_TYPES,
	ACCELERATOR_CHECK_IN_USER,
	ACCELERATOR_CHECK_IN,
	ACCELERATOR_CHECK_OUT,
	GET_ALL_DATABASES,
} from '../../../utilities/apiEndpoints'
import { DialogTypes, showDialog } from '../../dialogs'
import i18n from 'i18next'
import {
	selectCategoryId,
	selectAcceleratorId,
	selectAcceleratorInstanceId,
	selectConfigurationValues,
	selectAcceleratorInstanceAppId,
	selectPrevName,
	selectIsDuplicate,
	getLockStatus,
	selectAcceleratorData,
} from './acceleratorInstance.selectors'
import { AcceleratorLockStatus } from './acceleratorInstance.types'
import {
	ACCELERATOR_ACQUIRE_LOCK_MESSAGE,
	ACCELERATOR_SAVE_MESSAGE_SUCCESS,
	BUTTON_OK,
	COMMON_LOCK_NOT_PRESENT_MESSAGE,
	COMMON_REQUEST_LOCK_GRANTED,
} from '../../../utilities/constants'
import { filterDeep } from './acceleratorInstance.utilities'

export const fetchAttributeTypes = createAsyncThunk(
	'acceleratorInstance/dataType',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: ACCELERATORS_GET_ATTRIBUTE_TYPES,
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const loadAcceleratorInitialize = createAsyncThunk(
	'acceleratorInstance/loadAcceleratorInitialize',
	async (_: undefined, thunkArgs) => {
		const categoryId = selectCategoryId(thunkArgs.getState() as RootState)
		const acceleratorId = selectAcceleratorId(thunkArgs.getState() as RootState)
		const isDuplicate = selectIsDuplicate(thunkArgs.getState() as RootState)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		const acceleratorInstanceAppId = selectAcceleratorInstanceAppId(
			thunkArgs.getState() as RootState
		)
		if (isNil(categoryId) || isNil(acceleratorId)) {
			return null
		}
		thunkArgs.dispatch(fetchAttributeTypes())
		thunkArgs.dispatch(loadAcceleratorSupportApps(acceleratorId))
		if (!isNil(acceleratorInstanceId) && !isNil(acceleratorInstanceAppId)) {
			thunkArgs.dispatch(
				loadAcceleratorInstance({
					acceleratorInstanceId,
					acceleratorInstanceAppId,
				})
			)
			if (!isDuplicate) {
				thunkArgs.dispatch(
					checkLockStatus(
						selectGetLoginUserId(thunkArgs.getState() as RootState)
					)
				)
				thunkArgs.dispatch(loadCurrentInstanceAppData())
			}
		} else {
			thunkArgs.dispatch(loadAcceleratorMetadata({ categoryId, acceleratorId }))
		}
	}
)

export const loadCurrentInstanceAppData = createAsyncThunk(
	'acceleratorInstance/loadCurrentInstanceAppData',
	async (_: undefined, thunkArgs) => {
		const acceleratorInstanceAppId = selectAcceleratorInstanceAppId(
			thunkArgs.getState() as RootState
		)
		if (isNil(acceleratorInstanceAppId)) {
			return
		}
		return await doAsync({
			url: ACCELERATORS_GET_APP(acceleratorInstanceAppId),
			...thunkArgs,
		})
	}
)

export const refreshLockStatus = createAsyncThunk(
	'acceleratorInstance/refreshLockStatus',
	async (_: undefined, thunkArgs) => {
		thunkArgs.dispatch(
			checkLockStatus(selectGetLoginUserId(thunkArgs.getState() as RootState))
		)
	}
)

export const checkLockStatus = createAsyncThunk(
	'acceleratorInstance/checkLockStatus',
	async (loggedInUserId: string | null, thunkArgs) => {
		const acceleratorInstanceAppId = selectAcceleratorInstanceAppId(
			thunkArgs.getState() as RootState
		)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		if (
			isNil(acceleratorInstanceAppId) ||
			isNil(acceleratorInstanceId) ||
			isNil(loggedInUserId)
		) {
			return
		}
		return await doAsync({
			url: ACCELERATOR_CHECK_IN_USER(
				acceleratorInstanceAppId,
				acceleratorInstanceId
			),
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const acquireLock = createAsyncThunk(
	'acceleratorInstance/acquireLock',
	async (lockData: { isEnabled?: boolean }, thunkArgs) => {
		const acceleratorInstanceAppId = selectAcceleratorInstanceAppId(
			thunkArgs.getState() as RootState
		)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		const acceleratorData = selectAcceleratorData(
			thunkArgs.getState() as RootState
		)
		if (
			isNil(acceleratorInstanceAppId) ||
			isNil(acceleratorInstanceId) ||
			isNil(acceleratorData)
		) {
			return
		}
		return await doAsync({
			url: ACCELERATOR_CHECK_IN(
				acceleratorInstanceAppId,
				acceleratorInstanceId
			),
			noBusySpinner: true,
			httpMethod: 'put',
			...thunkArgs,
		}).then((data: any) => {
			if (data.response === 'ACQUIRED_LOCK') {
				const conformationCallBack = () => {
					thunkArgs.dispatch(
						loadAcceleratorInstance({
							acceleratorInstanceId,
							acceleratorInstanceAppId,
						})
					)
				}
				if (lockData && !lockData.isEnabled) {
					thunkArgs.dispatch(
						showDialog({
							type: DialogTypes.ALERT,
							onOkay: conformationCallBack,
							onCancel: conformationCallBack,
							title: i18n.t(COMMON_REQUEST_LOCK_GRANTED),
							data: {
								message: i18n.t(ACCELERATOR_ACQUIRE_LOCK_MESSAGE, {
									name: acceleratorData.name,
								}),
								okayButtonLabel: i18n.t(BUTTON_OK),
							},
						})
					)
				}
			} else {
				if (lockData && !lockData.isEnabled) {
					thunkArgs.dispatch(
						showDialog({
							type: DialogTypes.ALERT,
							title: i18n.t(COMMON_REQUEST_LOCK_GRANTED),
							data: {
								message: i18n.t(COMMON_LOCK_NOT_PRESENT_MESSAGE, {
									name: data.displayName,
								}),
								okayButtonLabel: i18n.t(BUTTON_OK),
							},
						})
					)
				}
			}
			return data
		})
	}
)

export const releaseLock = createAsyncThunk(
	'acceleratorInstance/releaseLock',
	async (_: undefined, thunkArgs) => {
		const acceleratorInstanceAppId = selectAcceleratorInstanceAppId(
			thunkArgs.getState() as RootState
		)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		const lockStatus = getLockStatus(thunkArgs.getState() as RootState)
		if (
			isNil(acceleratorInstanceAppId) ||
			isNil(acceleratorInstanceId) ||
			lockStatus !== AcceleratorLockStatus.LOCK_ACQUIRED
		) {
			return Promise.resolve()
		}
		return await doAsync({
			url: ACCELERATOR_CHECK_OUT(
				acceleratorInstanceAppId,
				acceleratorInstanceId
			),
			httpMethod: 'put',
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const loadAcceleratorSupportApps = createAsyncThunk(
	'acceleratorInstance/loadAcceleratorSupportApps',
	async (data: string, thunkArgs) => {
		return await doAsync({
			url: ACCELERATORS_GET_ALL_APPS_EXCLUDE([data]),
			...thunkArgs,
		})
	}
)

export const loadAcceleratorMetadata = createAsyncThunk(
	'acceleratorInstance/loadAcceleratorMetadata',
	async (data: { categoryId: string; acceleratorId: string }, thunkArgs) => {
		return await doAsync({
			url: ACCELERATORS_GET_ACCELERATOR_DATA(
				data.categoryId,
				data.acceleratorId
			),
			...thunkArgs,
		})
	}
)

export const loadDropDownOption = createAsyncThunk(
	'acceleratorInstance/loadDropDownOption',
	async (key: string, thunkArgs) => {
		switch (key) {
			case 'databaseList': {
				const configValues = selectConfigurationValues(
					thunkArgs.getState() as RootState
				)
				const appId = configValues.assignToApp
				if (isEmpty(appId)) {
					return []
				}
				return await doAsync({
					url: GET_ALL_DATABASES(appId),
					...thunkArgs,
				})
			}
			default: {
				return []
			}
		}
	}
)

export const loadDropDownOptions = createAsyncThunk(
	'acceleratorInstance/loadDropDownOptions',
	async (_: undefined, thunkArgs) => {
		setTimeout(() => {
			const configData = selectAcceleratorData(
				thunkArgs.getState() as RootState
			)
			const optionsToLoad = filterDeep(configData, 'optionsRef')
			for (let i = 0, iLen = optionsToLoad.length; i < iLen; i++) {
				thunkArgs.dispatch(loadDropDownOption(optionsToLoad[i]))
			}
		})
	}
)

export const loadAcceleratorInstance = createAsyncThunk(
	'acceleratorInstance/loadAcceleratorInstance',
	async (
		data: { acceleratorInstanceId: string; acceleratorInstanceAppId: string },
		thunkArgs
	) => {
		return await doAsync({
			url: ACCELERATORS_GET_ACCELERATOR_INSTANCE(
				data.acceleratorInstanceAppId,
				data.acceleratorInstanceId
			),
			...thunkArgs,
		})
	}
)

export const createNewAcceleratorInstance = createAsyncThunk(
	'acceleratorInstance/createNewAcceleratorInstance',
	async (_: undefined, thunkArgs) => {
		const data = selectConfigurationValues(thunkArgs.getState() as RootState)
		const acceleratorId = selectAcceleratorId(thunkArgs.getState() as RootState)
		const dataToSend = {
			name: data.name,
			description: data.description,
			applicationId: data.assignToApp,
			acceleratorId: acceleratorId,
			configurationValues: omit(data, ['name', 'description', 'assignToApp']),
		}
		return await doAsync({
			url: ACCELERATORS_GET_ACCELERATOR_INSTANCES(data.assignToApp),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(dataToSend),
			},
			...thunkArgs,
		})
	}
)

export const addDeleteAcceleratorInstance = createAsyncThunk(
	'acceleratorInstance/addDeleteAcceleratorInstance',
	async (getLock: boolean, thunkArgs) => {
		const data = selectConfigurationValues(thunkArgs.getState() as RootState)
		const acceleratorId = selectAcceleratorId(thunkArgs.getState() as RootState)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		if (isNil(acceleratorInstanceId)) {
			return null
		}
		const dataToSend = {
			name: data.name,
			description: data.description,
			applicationId: data.assignToApp,
			acceleratorId: acceleratorId,
			configurationValues: omit(data, ['name', 'description', 'assignToApp']),
		}
		// delete and create new
		return await doAsync({
			url: ACCELERATORS_GET_ACCELERATOR_INSTANCES(data.assignToApp),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(dataToSend),
			},
			...thunkArgs,
		}).then((result: any) => {
			doAsync({
				url: ACCELERATORS_GET_ACCELERATOR_INSTANCE(
					data.assignToApp,
					acceleratorInstanceId
				),
				httpMethod: 'delete',
				successMessage: i18n.t(ACCELERATOR_SAVE_MESSAGE_SUCCESS, {
					name: data.name,
				}),
				...thunkArgs,
			})
			if (getLock) {
				setTimeout(() => {
					thunkArgs.dispatch(acquireLock({ isEnabled: true }))
				})
			}
			return result
		})
	}
)

export const updateAcceleratorInstance = createAsyncThunk(
	'acceleratorInstance/updateAcceleratorInstance',
	async (_: undefined, thunkArgs) => {
		const data = selectConfigurationValues(thunkArgs.getState() as RootState)
		const acceleratorId = selectAcceleratorId(thunkArgs.getState() as RootState)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		if (isNil(acceleratorInstanceId)) {
			return null
		}
		const dataToSend = {
			name: data.name,
			description: data.description,
			applicationId: data.assignToApp,
			acceleratorId: acceleratorId,
			configurationValues: omit(data, ['name', 'description', 'assignToApp']),
		}
		// update
		return await doAsync({
			url: ACCELERATORS_GET_ACCELERATOR_INSTANCE(
				data.assignToApp,
				acceleratorInstanceId
			),
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({ ...dataToSend, id: acceleratorInstanceId }),
			},
			successMessage: i18n.t(ACCELERATOR_SAVE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			...thunkArgs,
		})
	}
)

export const validateAcceleratorInstance = createAsyncThunk(
	'acceleratorInstance/validateAcceleratorInstance',
	async (_: undefined, thunkArgs) => {
		const categoryId = selectCategoryId(thunkArgs.getState() as RootState)
		const data = selectConfigurationValues(thunkArgs.getState() as RootState)
		const prevName = selectPrevName(thunkArgs.getState() as RootState)
		if (data.name !== '' && !isNil(categoryId) && prevName !== data.name) {
			return await doAsync({
				url: ACCELERATORS_VALIDATE_NAME_FOR_DUPLICATE(data.name, categoryId),
				noBusySpinner: true,
				...thunkArgs,
			})
		}
	}
)
