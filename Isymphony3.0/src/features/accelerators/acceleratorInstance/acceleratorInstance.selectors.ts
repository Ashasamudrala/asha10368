import slice from './acceleratorInstance.slice'
import { selectSlice as baseSelector } from '../base.selector'
import { RootState } from '../../../base.types'
import {
	AcceleratorConfigurationValuesProps,
	AcceleratorInstanceAppOption,
	AcceleratorInstanceState,
	AcceleratorInstanceUserDetailsProps,
	AcceleratorInstanceViewState,
	AcceleratorLockStatus,
} from './acceleratorInstance.types'
import {
	AcceleratorInstanceProps,
	MarketplaceProps,
} from '../accelerators.types'
import { isEqual } from 'lodash'

export const selectSlice = (state: RootState): AcceleratorInstanceState => {
	return baseSelector(state)[slice.name]
}

export const selectCategoryId = (state: RootState): string | null => {
	return selectSlice(state).categoryId
}

export const selectAcceleratorId = (state: RootState): string | null =>
	selectSlice(state).acceleratorId

export const selectAcceleratorInstanceId = (state: RootState): string | null =>
	selectSlice(state).acceleratorInstanceId

export const selectAcceleratorInstanceAppId = (
	state: RootState
): string | null => selectSlice(state).acceleratorInstanceAppId

export const selectCurrentViewState = (
	state: RootState
): AcceleratorInstanceViewState => selectSlice(state).state

export const selectIsDuplicate = (state: RootState): boolean =>
	selectSlice(state).isDuplicate

export const selectAcceleratorData = (
	state: RootState
): MarketplaceProps | null => selectSlice(state).acceleratorData

export const selectConfigurationValues = (
	state: RootState
): AcceleratorConfigurationValuesProps => selectSlice(state).configurationValues

export const selectAppOptions = (
	state: RootState
): AcceleratorInstanceAppOption[] => selectSlice(state).appOptions

export const selectUserDetails = (
	state: RootState
): AcceleratorInstanceUserDetailsProps | null => selectSlice(state).userDetails

export const selectPrevAssignTo = (state: RootState): string | null =>
	selectSlice(state).prevAssignTo

export const selectPrevName = (state: RootState): string | null =>
	selectSlice(state).prevName

export const selectSaveInstanceData = (
	state: RootState
): AcceleratorInstanceProps | null => selectSlice(state).savedInstanceData

export const selectErrorList = (state: RootState) =>
	selectSlice(state).errorList
export const selectAttributeTypes = (state: RootState) =>
	selectSlice(state).attributeTypes

export const getLockStatus = (state: RootState): AcceleratorLockStatus => {
	return selectSlice(state).lockStatus
}

export const getLockedBy = (state: RootState): string | null => {
	return selectSlice(state).lockedBy
}

export const getPrevAcceleratorInstanceId = (
	state: RootState
): string | null => {
	return selectSlice(state).prevAcceleratorInstanceId
}

export const isDataUpdated = (state: RootState): boolean => {
	return !isEqual(
		selectSlice(state).configurationValues,
		selectSlice(state).prevConfigurationValues
	)
}

export const getOptionsObject = (state: RootState) => {
	return selectSlice(state).optionsObject
}
