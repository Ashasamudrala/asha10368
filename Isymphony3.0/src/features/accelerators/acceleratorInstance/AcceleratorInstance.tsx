import { Drawer, makeStyles } from '@material-ui/core'
import { isNil, isFunction, debounce, isEmpty, isArray } from 'lodash'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { useSelector, useDispatch } from 'react-redux'
import {
	ACCELERATOR_TRANSLATIONS,
	ADD_ACCELERATOR_INSTANCE,
	BUTTON_CANCEL,
	BUTTON_SAVE,
	EDIT_ACCELERATOR_INSTANCE,
	ACCELERATOR_SUCCESS_MESSAGE,
	ACCELERATOR_SUCCESS_UPDATE_MESSAGE,
	TAKE_ME_THERE,
	ACCELERATOR_CREATED_BY,
	CREATING,
	UPDATING,
	VIEW_ACCELERATOR_INSTANCE,
	DUPLICATE_ACCELERATOR_INSTANCE,
	COMMON_REQUEST_LOCK,
	COMMON_RELEASE_LOCK,
	COMMON_LOCKED_BY,
	ACCELERATOR_RELEASE_LOCK_INFO,
	REQUEST_LOCK_INFO,
	ACCELERATOR_NO_DB_ERROR,
	ACCELERATOR_CREATE_DATABASE,
} from '../../../utilities/constants'
import {
	acquireLock,
	loadAcceleratorInitialize,
	loadDropDownOptions,
	refreshLockStatus,
	releaseLock,
	validateAcceleratorInstance,
} from './acceleratorInstance.asyncActions'
import { actions } from './acceleratorInstance.slice'
import {
	getLockStatus,
	selectAcceleratorData,
	selectAcceleratorId,
	selectAcceleratorInstanceId,
	selectAppOptions,
	selectAttributeTypes,
	selectCategoryId,
	selectConfigurationValues,
	selectCurrentViewState,
	selectErrorList,
	selectIsDuplicate,
	selectSaveInstanceData,
	selectUserDetails,
	getLockedBy,
	getOptionsObject,
	getPrevAcceleratorInstanceId,
} from './acceleratorInstance.selectors'
import {
	AcceleratorConfigurationValuesProps,
	AcceleratorInstanceViewState,
	AcceleratorLockStatus,
} from './acceleratorInstance.types'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import './acceleratorInstance.scss'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import DoneOutlinedIcon from '@material-ui/icons/DoneOutlined'
import {
	IsyFormBuilder,
	IsyFormBuilderFormItemProps,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import {
	buildFormBuilderConfigFromBackendConfig,
	validateData,
} from './acceleratorInstance.utilities'
import {
	closeConformation,
	releaseLockConformation,
	saveAcceleratorInstance,
} from './acceleratorInstance.controller'
import { AcceleratorInstanceProps } from '../accelerators.types'
import { getAcceleratorsIconUrl } from '../accelerators.utilities'
import { TableView } from '../components/TableView/TableView'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined'
import CachedOutlinedIcon from '@material-ui/icons/CachedOutlined'
import HighlightOffIcon from '@material-ui/icons/HighlightOff'
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles({
	paper: {
		width: 888,
		boxSizing: 'border-box',
		backgroundColor: 'white',
		border: '1px solid #dedede',
	},
})

export interface AcceleratorInstancePanelProps {
	acceleratorsTourEnable: boolean
	updateSteps?: (stepPosition: number) => void
	onUpdate: (
		prevAcceleratorId: string | null,
		data: AcceleratorInstanceProps,
		categoryId: string | null
	) => void
}

export function AcceleratorInstance(props: AcceleratorInstancePanelProps) {
	const { acceleratorsTourEnable } = props
	const { t } = useTranslation(ACCELERATOR_TRANSLATIONS)

	const categoryId = useSelector(selectCategoryId)
	const acceleratorId = useSelector(selectAcceleratorId)
	const prevAcceleratorInstanceId = useSelector(getPrevAcceleratorInstanceId)
	const acceleratorInstanceId = useSelector(selectAcceleratorInstanceId)
	const acceleratorData = useSelector(selectAcceleratorData)
	const currentViewState = useSelector(selectCurrentViewState)
	const configurationValues = useSelector(selectConfigurationValues)
	const appOptions = useSelector(selectAppOptions)
	const userDetails = useSelector(selectUserDetails)
	const savedData = useSelector(selectSaveInstanceData)
	const errorList = useSelector(selectErrorList)
	const isDuplicate = useSelector(selectIsDuplicate)
	const attributeTypes = useSelector(selectAttributeTypes)
	const lockStatus = useSelector(getLockStatus)
	const lockedBy = useSelector(getLockedBy)
	const optionsObject = useSelector(getOptionsObject)

	const dispatch = useDispatch()
	const classes = useStyles()
	const history = useHistory()

	useEffect(() => {
		if (!isNil(categoryId) && !isNil(acceleratorId)) {
			dispatch(loadAcceleratorInitialize())
		}
	}, [categoryId, acceleratorId, dispatch])

	useEffect(() => {
		if (!isEmpty(configurationValues.assignToApp)) {
			dispatch(loadDropDownOptions())
		}
	}, [configurationValues.assignToApp, dispatch])

	useEffect(() => {
		if (AcceleratorInstanceViewState.SUCCESS === currentViewState) {
			if (acceleratorsTourEnable && isFunction(props.updateSteps)) {
				props.updateSteps(5)
			}
		}
	}, [currentViewState]) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (lockStatus === AcceleratorLockStatus.LOCK_ACQUIRED) {
			if (acceleratorsTourEnable && isFunction(props.updateSteps)) {
				props.updateSteps(8)
			}
		} else if (lockStatus === AcceleratorLockStatus.LOCK_PRESENT) {
			if (acceleratorsTourEnable && isFunction(props.updateSteps)) {
				props.updateSteps(7)
			}
		} else if (lockStatus === AcceleratorLockStatus.LOCK_NOT_PRESENT) {
			if (acceleratorsTourEnable && isFunction(props.updateSteps)) {
				props.updateSteps(9)
			}
		}
	}, [lockStatus, acceleratorsTourEnable]) // eslint-disable-line react-hooks/exhaustive-deps

	const getFormBuilderConfig = (): IsyFormBuilderFormItemProps[] => {
		if (isNil(acceleratorData)) {
			return []
		}
		return buildFormBuilderConfigFromBackendConfig(
			acceleratorData.configuration,
			appOptions,
			handleNameChange
		)
	}

	const getSubInstanceLabel = (): string => {
		if (isNil(acceleratorInstanceId)) {
			if (isDuplicate) {
				return t(DUPLICATE_ACCELERATOR_INSTANCE)
			}
			return t(ADD_ACCELERATOR_INSTANCE)
		}
		if (currentViewState === AcceleratorInstanceViewState.VIEW) {
			return t(VIEW_ACCELERATOR_INSTANCE)
		}
		return t(EDIT_ACCELERATOR_INSTANCE)
	}

	const handleClose = () => {
		const finalCallBack = () => {
			dispatch(actions.clearData(null))
			if (!isNil(savedData)) {
				props.onUpdate(prevAcceleratorInstanceId, savedData, null)
			}
		}
		if (acceleratorsTourEnable && isFunction(props.updateSteps)) {
			props.updateSteps(4)
		} else {
			dispatch(closeConformation(finalCallBack))
		}
	}

	const handleSave = () => {
		if (isNil(acceleratorData)) {
			return
		}
		const errors = validateData(
			acceleratorData.configuration,
			configurationValues,
			errorList
		)
		if (Object.keys(errors).length !== 0) {
			dispatch(actions.setErrorList(errors))
			return
		}
		dispatch(actions.setErrorList({}))
		dispatch(saveAcceleratorInstance(true))
	}

	const handleOnChange = (data: AcceleratorConfigurationValuesProps) => {
		dispatch(actions.setConfigurationValues(data))
		if (data.assignToApp !== configurationValues.assignToApp) {
			dispatch(actions.clearOptionsObject(null))
		}
	}

	const handleOpenAccelerator = () => {
		if (!isNil(savedData)) {
			props.onUpdate(prevAcceleratorInstanceId, savedData, categoryId)
		}
		dispatch(actions.clearData(null))
	}

	const handleNameDebounce = debounce(() => {
		dispatch(validateAcceleratorInstance())
	}, 300)

	const handleNameChange = (value: string) => {
		dispatch(
			actions.setConfigurationValues({
				name: value.trimStart().replace(/\s{2,}/g, ' '),
			})
		)
		handleNameDebounce()
	}

	const handleLockAcquire = () => {
		dispatch(
			acquireLock({
				isEnabled: acceleratorsTourEnable,
			})
		)
	}

	const handleReleaseLock = () => {
		dispatch(releaseLockConformation())
	}

	const handleRefreshLock = () => {
		dispatch(refreshLockStatus())
	}

	const handleClickOnNewDb = () => {
		const appId = configurationValues.assignToApp
		if (!isNil(appId)) {
			history.push(`/app/${appId}/database`)
			dispatch(releaseLock())
			dispatch(actions.clearData(null))
		}
	}

	const renderHeader = () => {
		return (
			<div className='header'>
				<div className='title-section'>
					<img
						src={getAcceleratorsIconUrl(
							isNil(acceleratorData) ? '' : acceleratorData.metadata.imageRef
						)}
						alt={
							isNil(acceleratorData) ? '' : acceleratorData.metadata.imageRef
						}
						className='image'
					/>
					<div className='title'>
						<div className='header-t'>
							{isNil(acceleratorData) ? '' : acceleratorData.name}
						</div>
						{!isNil(userDetails) && (
							<div className='header-sub-title'>
								{t(ACCELERATOR_CREATED_BY, {
									user: userDetails.createdByUserDisplayName,
									createdDate: moment(userDetails.lastModifiedTimestamp).format(
										'll'
									),
								})}
							</div>
						)}
					</div>
					<div className='vertical-line' />
					<div className='sub-info'>{getSubInstanceLabel()}</div>
					<CloseOutlinedIcon className='close-icon' onClick={handleClose} />
				</div>
			</div>
		)
	}

	const renderAcceleratorSavingPage = () => {
		return (
			<>
				<div className='create-accelerator-progress '>
					<CircularProgress size={70} />
					<div className='progress-label'>
						{isNil(acceleratorInstanceId) ? t(CREATING) : t(UPDATING)}
					</div>
				</div>
			</>
		)
	}

	const renderAcceleratorSuccessPage = () => {
		return (
			<>
				<div className='accelerator-success-page'>
					<span className='accelerator' role='button'>
						<DoneOutlinedIcon color='secondary' />
					</span>
					<div className='accelerator-success-text'>
						{isNil(acceleratorInstanceId)
							? t(ACCELERATOR_SUCCESS_MESSAGE)
							: t(ACCELERATOR_SUCCESS_UPDATE_MESSAGE)}
					</div>
					<div>
						<IsyButton
							className='primary-btn isy-steps-take-me-there-btn'
							onClick={handleOpenAccelerator}
						>
							{t(TAKE_ME_THERE)}
						</IsyButton>
					</div>
				</div>
			</>
		)
	}

	const renderComponent = (
		componentType: string,
		data: any,
		isViewMode: boolean,
		updateCallback: (data: any) => void
	): React.ReactNode => {
		switch (componentType) {
			case 'tables':
				return (
					<TableView
						data={data}
						attributeTypes={attributeTypes}
						isViewMode={isViewMode}
						onUpdate={updateCallback}
					/>
				)
			default:
				return null
		}
	}

	const renderFormBuilder = () => {
		return (
			<IsyFormBuilder<AcceleratorConfigurationValuesProps>
				forms={getFormBuilderConfig()}
				data={configurationValues}
				onChange={handleOnChange}
				isViewMode={currentViewState === AcceleratorInstanceViewState.VIEW}
				errors={errorList}
				componentRender={renderComponent}
				optionsObject={optionsObject}
			/>
		)
	}

	const renderPage = () => {
		switch (currentViewState) {
			case AcceleratorInstanceViewState.VIEW:
			case AcceleratorInstanceViewState.EDIT: {
				return renderFormBuilder()
			}
			case AcceleratorInstanceViewState.SUCCESS: {
				return renderAcceleratorSuccessPage()
			}
			case AcceleratorInstanceViewState.SAVING: {
				return renderAcceleratorSavingPage()
			}
			default: {
				return null
			}
		}
	}

	const renderContent = () => {
		return <div className='middle-section'>{renderPage()}</div>
	}

	const renderLockButtons = () => {
		switch (lockStatus) {
			case AcceleratorLockStatus.LOCK_PRESENT: {
				return (
					<span>
						<IsyButton onClick={handleLockAcquire} className='request-btn'>
							<LockOutlinedIcon className='lock-icon' />
							{t(COMMON_REQUEST_LOCK)}
						</IsyButton>
						<span className='lock-tooltip'>{t(REQUEST_LOCK_INFO)}</span>
					</span>
				)
			}
			case AcceleratorLockStatus.LOCK_NOT_PRESENT: {
				return (
					<span className='locked-by'>
						<span className='info'>
							{t(COMMON_LOCKED_BY, { name: lockedBy })}
						</span>
						<CachedOutlinedIcon className='icon' onClick={handleRefreshLock} />
					</span>
				)
			}
			case AcceleratorLockStatus.LOCK_ACQUIRED: {
				return (
					<span>
						<IsyButton onClick={handleReleaseLock} className='release-btn'>
							<LockOpenOutlinedIcon className='lock-icon' />
							{t(COMMON_RELEASE_LOCK)}
						</IsyButton>
						<span className='lock-tooltip'>
							{t(ACCELERATOR_RELEASE_LOCK_INFO)}
						</span>
					</span>
				)
			}
			default:
				return null
		}
	}

	const renderBottom = () => {
		if (
			currentViewState !== AcceleratorInstanceViewState.EDIT &&
			currentViewState !== AcceleratorInstanceViewState.VIEW
		) {
			return null
		}
		return (
			<div className='bottom-section'>
				<IsyButton onClick={handleClose} className='standard-btn'>
					{t(BUTTON_CANCEL)}
				</IsyButton>
				{currentViewState === AcceleratorInstanceViewState.EDIT && (
					<IsyButton onClick={handleSave} className='primary-btn'>
						{t(BUTTON_SAVE)}
					</IsyButton>
				)}
				{renderLockButtons()}
			</div>
		)
	}

	const renderAlert = () => {
		if (
			isArray(optionsObject.databaseList) &&
			optionsObject.databaseList.length === 0
		) {
			return (
				<div className='alert'>
					<HighlightOffIcon className='icon' />
					<span className='text'>{t(ACCELERATOR_NO_DB_ERROR)}</span>
					<IsyButton className='button' onClick={handleClickOnNewDb}>
						{t(ACCELERATOR_CREATE_DATABASE)}
					</IsyButton>
				</div>
			)
		}

		return null
	}

	const renderDrawerContent = () => {
		return (
			<>
				<Drawer
					className='accelerator-instance'
					open={currentViewState !== AcceleratorInstanceViewState.NONE}
					anchor='right'
					elevation={0}
					classes={{ paper: classes.paper + ' isy-steps-create-instance' }}
					disablePortal
				>
					{renderHeader()}
					{renderContent()}
					{renderBottom()}
					{renderAlert()}
				</Drawer>
			</>
		)
	}
	return renderDrawerContent()
}
