import {
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import {
	MarketplaceConfigurationFieldConstraintsProps,
	MarketplaceConfigurationFieldProps,
	MarketplaceConfigurationProps,
	MarketplaceConfigurationSectionProps,
	MarketplaceConfigurationUiConfigurationMetadataProps,
} from '../accelerators.types'
import i18n from 'i18next'
import {
	ACCELERATOR_ASSIGNED_TO_APP,
	ACCELERATOR_DESCRIPTION_PLACEHOLDER,
	ACCELERATOR_ASSIGNED_TO_APP_PLACEHOLDER,
	ACCELERATOR_DUPLICATE_NAME,
	ACCELERATOR_NAME_PLACEHOLDER,
	APP_DETAILS,
	COMMON_FIELD_EMPTY,
	COMMON_MAXIMUM_LENGTH_ERROR,
	COMMON_MINIMUM_LENGTH_ERROR,
	DESCRIPTION,
	NAME,
	ACCELERATOR_SELECT,
} from '../../../utilities/constants'
import { IsyDropDownOptionProps } from '../../../widgets/IsyDropDown/IsyDropDown'
import { AcceleratorConfigurationValuesProps } from './acceleratorInstance.types'
import {
	cloneDeep,
	get,
	isArray,
	isEmpty,
	isNil,
	isNumber,
	isString,
	set,
} from 'lodash'
import databaseArrayData from '../../../config/accelerators/databaseArray.json'

const getIsFieldRequired = (
	constraints: MarketplaceConfigurationFieldConstraintsProps
) => {
	return (
		constraints &&
		(constraints.required === 'true' || constraints.required === true)
	)
}

const getFieldMaxLength = (
	constraints: MarketplaceConfigurationFieldConstraintsProps
) => {
	return constraints && constraints.length
}

const getFieldRegex = (
	constraints: MarketplaceConfigurationFieldConstraintsProps
) => {
	return constraints && constraints.regex
}

const getFieldErrorMessage = (
	constraints: MarketplaceConfigurationFieldConstraintsProps
) => {
	return constraints && constraints.errorMessage
}

const getFieldPlaceholder = (
	metadata: MarketplaceConfigurationUiConfigurationMetadataProps
) => {
	return metadata.placeholderValue || i18n.t(ACCELERATOR_SELECT)
}

const buildFormBuilderFieldConfigFromBackendConfig = (
	config: MarketplaceConfigurationFieldProps
): IsyFormBuilderFormItemProps => {
	if (config.type === 'String' || config.type === 'string') {
		if (config.uiConfiguration.metadata.renderAs === 'Select') {
			return {
				type: IsyFormBuilderFormTypes.SELECT,
				title: config.displayName,
				dataRef: config.name,
				options: config.uiConfiguration.metadata.optionsRef || [],
				placeholder: getFieldPlaceholder(config.uiConfiguration.metadata),
				isRequired: getIsFieldRequired(config.constraints),
			}
		}
		return {
			type:
				config.uiConfiguration.metadata.renderAs === 'Password'
					? IsyFormBuilderFormTypes.PASSWORD
					: IsyFormBuilderFormTypes.STRING,
			title: config.displayName,
			placeholder: config.uiConfiguration.metadata.placeholderValue,
			dataRef: config.name,
			isRequired: getIsFieldRequired(config.constraints),
		}
	}
	if (config.type === 'List') {
		return {
			type: IsyFormBuilderFormTypes.CHIP_INPUT,
			title: config.displayName,
			placeholder: config.uiConfiguration.metadata.placeholderValue,
			dataRef: config.name,
			chipLength: getFieldMaxLength(config.constraints),
			isRequired: getIsFieldRequired(config.constraints),
		}
	}
	if (config.type === 'Object') {
		if (config.uiConfiguration.metadata.renderAs.indexOf('Array') !== -1) {
			const children: IsyFormBuilderFormItemProps[] = []
			let label = ''
			if (!isArray(config.fields) || config.fields.length === 0) {
				children.push({
					type: IsyFormBuilderFormTypes.COMPONENT,
					dataRef: '',
					componentType: config.name,
				})
			} else {
				for (let i = 0, iLen = config.fields.length; i < iLen; i++) {
					children.push(
						buildFormBuilderFieldConfigFromBackendConfig(config.fields[i])
					)
				}
			}
			return {
				type: IsyFormBuilderFormTypes.ARRAY_SECTION,
				title: label,
				addLabel: '',
				addDefault: {},
				dataRef: config.name,
				forms: children,
				className: config.name,
				minimum: config.constraints.min,
				maximum: config.constraints.max,
			}
		}
	}
	return {
		type: IsyFormBuilderFormTypes.STRING,
		title: config.displayName,
		placeholder: config.uiConfiguration.metadata.placeholderValue,
		dataRef: config.name,
		isRequired: getIsFieldRequired(config.constraints),
	}
}

const buildFormBuilderSectionConfigFromBackendConfig = (
	config: MarketplaceConfigurationSectionProps,
	index: number
): IsyFormBuilderFormItemProps => {
	const children = []
	const formRef = config.uiConfiguration.metadata.outOfForm
		? config.name
		: 'form'
	for (let i = 0, iLen = config.fields.length; i < iLen; i++) {
		children.push(
			buildFormBuilderFieldConfigFromBackendConfig(config.fields[i])
		)
	}
	return {
		type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
		isExpandByDefault: true,
		title: index + 1 + '. ' + config.displayName,
		forms: children,
		dataRef: formRef,
	}
}

export const buildFormBuilderConfigFromBackendConfig = (
	config: MarketplaceConfigurationProps,
	appOptions: IsyDropDownOptionProps<string>[],
	nameChangeCallback: (name: string) => void
): IsyFormBuilderFormItemProps[] => {
	const formConfig: IsyFormBuilderFormItemProps[] = [
		{
			type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
			isExpandByDefault: true,
			title: i18n.t(APP_DETAILS),
			forms: [
				{
					type: IsyFormBuilderFormTypes.STRING,
					title: i18n.t(NAME),
					dataRef: 'name',
					placeholder: i18n.t(ACCELERATOR_NAME_PLACEHOLDER),
					autoFocus: true,
					isRequired: true,
					onChange: nameChangeCallback,
				},
				{
					type: IsyFormBuilderFormTypes.TEXT_AREA,
					title: i18n.t(DESCRIPTION),
					dataRef: 'description',
					placeholder: i18n.t(ACCELERATOR_DESCRIPTION_PLACEHOLDER),
				},
				{
					type: IsyFormBuilderFormTypes.SELECT,
					title: i18n.t(ACCELERATOR_ASSIGNED_TO_APP),
					dataRef: 'assignToApp',
					placeholder: i18n.t(ACCELERATOR_ASSIGNED_TO_APP_PLACEHOLDER),
					options: appOptions,
					isRequired: true,
				},
			],
		},
	]

	let sectionCount = 1

	for (let i = 0, iLen = config.sections.length; i < iLen; i++) {
		formConfig.push(
			buildFormBuilderSectionConfigFromBackendConfig(
				config.sections[i],
				sectionCount
			)
		)
		sectionCount++
	}
	return formConfig
}

const buildFieldDataFromBackendConfig = (
	config: MarketplaceConfigurationFieldProps,
	formRef: string,
	data: AcceleratorConfigurationValuesProps
) => {
	if (config.type === 'string') {
		data[formRef][config.name] = ''
	} else if (config.type === 'List') {
		data[formRef][config.name] = []
	} else if (config.type === 'Object') {
		if (config.uiConfiguration.metadata.renderAs.indexOf('Array') !== -1) {
			if (config.name === 'databases') {
				data[formRef][config.name] = cloneDeep(databaseArrayData)
			} else {
				data[formRef][config.name] = []
			}
		} else {
			data[formRef][config.name] = {}
		}
	} else {
		data[formRef][config.name] = ''
	}
}

const buildSectionDataFromBackendConfig = (
	config: MarketplaceConfigurationSectionProps,
	data: AcceleratorConfigurationValuesProps
) => {
	let formRef = 'form'
	if (config.uiConfiguration.metadata.outOfForm) {
		data[config.name] = {}
		formRef = config.name
	}
	for (let i = 0, iLen = config.fields.length; i < iLen; i++) {
		buildFieldDataFromBackendConfig(config.fields[i], formRef, data)
	}
}

export const buildDataObjectFromBackendConfig = (
	config: MarketplaceConfigurationProps
): AcceleratorConfigurationValuesProps => {
	const data: AcceleratorConfigurationValuesProps = {
		name: '',
		description: '',
		assignToApp: '',
		form: {},
	}
	for (let i = 0, iLen = config.sections.length; i < iLen; i++) {
		buildSectionDataFromBackendConfig(config.sections[i], data)
	}
	return data
}

const validateSelectField = (
	name: string,
	value: string,
	isRequired: boolean
) => {
	if (isRequired && isEmpty(value)) {
		return i18n.t(COMMON_FIELD_EMPTY, { name })
	}
}

const validateStringField = (
	name: string,
	value: string,
	isRequired: boolean,
	minLength: number | null,
	maxLength: number | null,
	regexString?: string | null,
	errorMessage?: string | null
) => {
	if (isRequired && isEmpty(value)) {
		return i18n.t(COMMON_FIELD_EMPTY, { name })
	}
	if (isNumber(minLength) && !isEmpty(value) && value.length < minLength) {
		return i18n.t(COMMON_MINIMUM_LENGTH_ERROR, { length: minLength })
	}
	if (isNumber(maxLength) && !isEmpty(value) && value.length > maxLength) {
		return i18n.t(COMMON_MAXIMUM_LENGTH_ERROR, { length: maxLength })
	}
	if (isString(regexString)) {
		const regex = new RegExp(regexString)
		if (!regex.test(value)) {
			return errorMessage || regexString
		}
	}
	return null
}

const validateField = (
	config: MarketplaceConfigurationFieldProps,
	data: AcceleratorConfigurationValuesProps,
	formRef: string,
	errors: { [key: string]: string }
) => {
	if (config.type === 'String' || config.type === 'string') {
		if (config.uiConfiguration.metadata.renderAs === 'Select') {
			const ref = formRef + '.' + config.name
			const value = get(data, ref)
			const error = validateSelectField(
				config.displayName,
				value,
				getIsFieldRequired(config.constraints)
			)
			if (!isNil(error)) {
				set(errors, ref, error)
			}
		} else {
			const ref = formRef + '.' + config.name
			const value = get(data, ref)
			const error = validateStringField(
				config.displayName,
				value,
				getIsFieldRequired(config.constraints),
				null,
				getFieldMaxLength(config.constraints),
				getFieldRegex(config.constraints),
				getFieldErrorMessage(config.constraints)
			)
			if (!isNil(error)) {
				set(errors, ref, error)
			}
		}
	} else if (config.type === 'List' && getIsFieldRequired(config.constraints)) {
		const ref = formRef + '.' + config.name
		const list = get(data, ref)
		if (!isArray(list) || list.length < 1) {
			set(errors, ref, i18n.t(COMMON_FIELD_EMPTY, { name: config.displayName }))
		}
	} else if (config.type === 'Object') {
		if (config.uiConfiguration.metadata.renderAs.indexOf('Array') !== -1) {
			const ref = isEmpty(config.name) ? formRef : formRef + '.' + config.name
			const dataArray = get(data, ref) || []
			for (let i = 0, iLen = dataArray.length; i < iLen; i++) {
				const subRef = ref + '[' + i + ']'
				for (let j = 0, jLen = (config.fields || []).length; j < jLen; j++) {
					validateField(config.fields[j], data, subRef, errors)
				}
			}
		}
	}
}

export const validateData = (
	config: MarketplaceConfigurationProps,
	data: AcceleratorConfigurationValuesProps,
	currentErrors: { [key: string]: string }
) => {
	let errors: { [key: string]: any } = {}
	const nameError = validateStringField(i18n.t(NAME), data.name, true, null, 64)
	const duplicateError = i18n.t(ACCELERATOR_DUPLICATE_NAME).split('  ')[0]
	if (nameError) {
		errors.name = nameError
	} else if (
		!isNil(currentErrors.name) &&
		currentErrors.name.indexOf(duplicateError) !== -1
	) {
		errors.name = currentErrors.name
	}
	const descriptionError = validateStringField(
		i18n.t(DESCRIPTION),
		data.description,
		false,
		null,
		1024
	)
	if (descriptionError) {
		errors.description = descriptionError
	}
	const assignToAppError = validateSelectField(
		i18n.t(ACCELERATOR_ASSIGNED_TO_APP),
		data.assignToApp,
		true
	)
	if (assignToAppError) {
		errors.assignToApp = assignToAppError
	}
	for (let i = 0, iLen = config.sections.length; i < iLen; i++) {
		const formRef = config.sections[i].uiConfiguration.metadata.outOfForm
			? config.sections[i].name
			: 'form'
		for (let j = 0, jLen = config.sections[i].fields.length; j < jLen; j++) {
			validateField(config.sections[i].fields[j], data, formRef, errors)
		}
	}
	return errors
}

const isObject = (item: any) => {
	return item && typeof item === 'object' && !Array.isArray(item)
}

export const mergeDeep = (target: any, ...sources: any): any => {
	if (!sources.length) return target
	const source = sources.shift()

	if (isObject(target) && isObject(source)) {
		for (const key in source) {
			if (isObject(source[key])) {
				if (!target[key]) Object.assign(target, { [key]: {} })
				mergeDeep(target[key], source[key])
			} else {
				Object.assign(target, { [key]: source[key] })
			}
		}
	}

	return mergeDeep(target, ...sources)
}

const clearDropDownOptionsLoop = (
	config: MarketplaceConfigurationFieldProps,
	data: AcceleratorConfigurationValuesProps,
	formRef: string
) => {
	if (config.type === 'String' || config.type === 'string') {
		if (
			config.uiConfiguration.metadata.renderAs === 'Select' &&
			!isNil(config.uiConfiguration.metadata.optionsRef)
		) {
			const ref = formRef + '.' + config.name
			set(data, ref, '')
		}
	}
	if (config.type === 'Object') {
		if (config.uiConfiguration.metadata.renderAs.indexOf('Array') !== -1) {
			const ref = isEmpty(config.name) ? formRef : formRef + '.' + config.name
			const dataArray = get(data, ref) || []
			for (let i = 0, iLen = dataArray.length; i < iLen; i++) {
				const subRef = ref + '[' + i + ']'
				for (let j = 0, jLen = (config.fields || []).length; j < jLen; j++) {
					clearDropDownOptionsLoop(config.fields[j], data, subRef)
				}
			}
		}
	}
}

export const clearDropDownOptionsData = (
	config: MarketplaceConfigurationProps,
	data: AcceleratorConfigurationValuesProps
): AcceleratorConfigurationValuesProps => {
	for (let i = 0, iLen = config.sections.length; i < iLen; i++) {
		const formRef = config.sections[i].uiConfiguration.metadata.outOfForm
			? config.sections[i].name
			: 'form'
		for (let j = 0, jLen = config.sections[i].fields.length; j < jLen; j++) {
			clearDropDownOptionsLoop(config.sections[i].fields[j], data, formRef)
		}
	}
	return data
}

export const filterDeep = (data: any, findProp: string): any[] => {
	var result = []
	if (data instanceof Array) {
		for (var i = 0; i < data.length; i++) {
			const subResult = filterDeep(data[i], findProp)
			if (subResult.length > 0) {
				result.push(...subResult)
			}
		}
	} else {
		for (var prop in data) {
			if (prop === findProp) {
				if (!isNil(data[prop])) {
					result.push(data[prop])
				}
			}
			if (data[prop] instanceof Object || data[prop] instanceof Array) {
				const subResult = filterDeep(data[prop], findProp)
				if (subResult.length > 0) {
					result.push(...subResult)
				}
			}
		}
	}
	return result
}
