import { AppType } from '../../../utilities/apiEnumConstants'
import { IsyDropDownOptionProps } from '../../../widgets/IsyDropDown/IsyDropDown'
import { AttributeTypeProps } from '../../databases/databases.types'
import {
	AcceleratorInstanceProps,
	MarketplaceProps,
} from '../accelerators.types'

export enum AcceleratorInstanceViewState {
	NONE,
	VIEW,
	EDIT,
	LOADING,
	SAVING,
	SUCCESS,
}

export enum AcceleratorLockStatus {
	'LOCK_PRESENT',
	'LOCK_NOT_PRESENT',
	'LOCK_ACQUIRED',
	'UN_KNOWN',
}

export interface AcceleratorInstanceSetIdsActionState {
	categoryId: string
	acceleratorId: string
	acceleratorInstanceId: string | null
	acceleratorInstanceAppId: string | null
	isDuplicate: boolean
}

export interface AcceleratorConfigurationValuesProps {
	[key: string]: any
}

export interface AcceleratorInstanceAppOption
	extends IsyDropDownOptionProps<string> {
	type: AppType
}

export interface AcceleratorInstanceUserDetailsProps {
	createdByUserDisplayName: string
	createdUserProfilePic: string
	creationTimestamp: string
	lastModifiedTimestamp: string
}

export interface AcceleratorInstanceState {
	categoryId: string | null
	acceleratorId: string | null
	acceleratorInstanceId: string | null
	acceleratorInstanceAppId: string | null
	state: AcceleratorInstanceViewState
	acceleratorData: MarketplaceProps | null
	optionsObject: { [key: string]: Array<IsyDropDownOptionProps<any>> }
	configurationValues: AcceleratorConfigurationValuesProps
	appOptions: AcceleratorInstanceAppOption[]
	userDetails: AcceleratorInstanceUserDetailsProps | null
	prevName: string | null
	prevAssignTo: string | null
	savedInstanceData: AcceleratorInstanceProps | null
	errorList: { [key: string]: string }
	isDuplicate: boolean
	attributeTypes: AttributeTypeProps[]
	lockStatus: AcceleratorLockStatus
	lockedBy: string | null
	prevConfigurationValues: AcceleratorConfigurationValuesProps
	prevAcceleratorInstanceId: string | null
}
