import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import i18n from 'i18next'
import { RootState } from '../../../base.types'
import {
	addDeleteAcceleratorInstance,
	createNewAcceleratorInstance,
	releaseLock,
	updateAcceleratorInstance,
} from './acceleratorInstance.asyncActions'
import {
	getLockStatus,
	isDataUpdated,
	selectAcceleratorData,
	selectAcceleratorInstanceId,
	selectConfigurationValues,
	selectErrorList,
	selectPrevAssignTo,
} from './acceleratorInstance.selectors'
import { actions } from './acceleratorInstance.slice'
import { actions as notificationActions } from '../../../infrastructure/notificationPopup/notificationPopup.slice'
import {
	AcceleratorInstanceViewState,
	AcceleratorLockStatus,
} from './acceleratorInstance.types'
import {
	ACCELERATOR_DISCARD_MESSAGE,
	COMMON_EXIT_LOCK,
	ACCELERATOR_EXIT_MESSAGE_LOCK_WITH_CHANGES,
	ACCELERATOR_EXIT_MESSAGE_LOCK_WITH_OUT_CHANGES,
	FORM_DIALOG_EXIT_MESSAGE_WITH_CHANGES,
	BUTTON_CONTINUE,
	BUTTON_DISCARD,
	BUTTON_SAVE,
	COMMON_BUTTON_CLOSE,
	COMMON_CONFORMATION_UPDATE_MESSAGE_LOCK,
} from '../../../utilities/constants'
import { DialogTypes, showDialog } from '../../dialogs'
import { validateData } from './acceleratorInstance.utilities'

export const saveAcceleratorInstance = createAsyncThunk(
	'acceleratorInstance/saveAcceleratorInitialize',
	async (getLock: boolean, thunkArgs) => {
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			thunkArgs.getState() as RootState
		)
		const data = selectConfigurationValues(thunkArgs.getState() as RootState)
		const previouslyAssignedApp = selectPrevAssignTo(
			thunkArgs.getState() as RootState
		)
		thunkArgs.dispatch(
			actions.setViewCurrentState(AcceleratorInstanceViewState.SAVING)
		)
		return new Promise<boolean>((resolve) => {
			if (isNil(acceleratorInstanceId)) {
				thunkArgs.dispatch(createNewAcceleratorInstance()).then(() => {
					resolve(true)
				})
			} else if (previouslyAssignedApp === data.assignToApp) {
				// update
				thunkArgs.dispatch(updateAcceleratorInstance()).then(() => {
					resolve(true)
				})
			} else {
				thunkArgs.dispatch(addDeleteAcceleratorInstance(getLock)).then(() => {
					resolve(false)
				})
			}
		})
	}
)

export const closeConformation = createAsyncThunk(
	'acceleratorInstance/closeConformation',
	async (callback: () => void, { dispatch, getState }) => {
		const acceleratorData = selectAcceleratorData(getState() as RootState)
		if (isNil(acceleratorData)) {
			return
		}
		const errorList = selectErrorList(getState() as RootState)
		const configValues = selectConfigurationValues(getState() as RootState)
		const isDataModified = isDataUpdated(getState() as RootState)
		const lockStatus = getLockStatus(getState() as RootState)
		const acceleratorInstanceId = selectAcceleratorInstanceId(
			getState() as RootState
		)

		const finalCloseAndClear = () => {
			dispatch(actions.clearData(null))
			callback()
		}
		if (!isDataModified) {
			if (lockStatus === AcceleratorLockStatus.LOCK_ACQUIRED) {
				const conformationCallBack = () => {
					dispatch(releaseLock()).then(() => {
						finalCloseAndClear()
					})
				}
				dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						title: i18n.t(COMMON_EXIT_LOCK),
						onOkay: conformationCallBack,
						data: {
							message: i18n.t(ACCELERATOR_EXIT_MESSAGE_LOCK_WITH_OUT_CHANGES),
							okayButtonLabel: i18n.t(COMMON_BUTTON_CLOSE),
							cancelButtonLabel: i18n.t(BUTTON_CONTINUE),
						},
					})
				)
			} else {
				finalCloseAndClear()
			}
		} else {
			if (!isNil(acceleratorInstanceId)) {
				const conformationCallBack = () => {
					const errors = validateData(
						acceleratorData.configuration,
						configValues,
						errorList
					)
					if (Object.keys(errors).length !== 0) {
						dispatch(actions.setErrorList(errors))
						return
					}
					dispatch(saveAcceleratorInstance(false)).then((data) => {
						if (data.payload) {
							dispatch(releaseLock()).then(() => {
								finalCloseAndClear()
							})
						} else {
							finalCloseAndClear()
						}
					})
				}
				const handleOnClose = () => {
					dispatch(
						notificationActions.notifySuccess(
							i18n.t(ACCELERATOR_DISCARD_MESSAGE, { name: configValues.name })
						)
					)
					dispatch(releaseLock()).then(() => {
						finalCloseAndClear()
					})
				}
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: conformationCallBack,
						onCancel: handleOnClose,
						data: {
							message: i18n.t(ACCELERATOR_EXIT_MESSAGE_LOCK_WITH_CHANGES),
							okayButtonLabel: i18n.t(BUTTON_SAVE),
							cancelButtonLabel: i18n.t(BUTTON_DISCARD),
						},
					})
				)
			} else {
				const conformationCallBack = () => {
					const errors = validateData(
						acceleratorData.configuration,
						configValues,
						errorList
					)
					if (Object.keys(errors).length !== 0) {
						dispatch(actions.setErrorList(errors))
						return
					}
					dispatch(saveAcceleratorInstance(false)).then(() => {
						finalCloseAndClear()
					})
				}
				const handleOnClose = () => {
					finalCloseAndClear()
				}
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: conformationCallBack,
						onCancel: handleOnClose,
						data: {
							message: i18n.t(FORM_DIALOG_EXIT_MESSAGE_WITH_CHANGES),
							okayButtonLabel: i18n.t(BUTTON_SAVE),
							cancelButtonLabel: i18n.t(BUTTON_DISCARD),
						},
					})
				)
			}
		}
	}
)

export const releaseLockConformation = createAsyncThunk(
	'acceleratorInstance/releaseLockConformation',
	async (_: undefined, { dispatch, getState }) => {
		const acceleratorData = selectAcceleratorData(getState() as RootState)
		if (isNil(acceleratorData)) {
			return
		}
		const errorList = selectErrorList(getState() as RootState)
		const configValues = selectConfigurationValues(getState() as RootState)
		const conformationCallBack = () => {
			const errors = validateData(
				acceleratorData.configuration,
				configValues,
				errorList
			)
			if (Object.keys(errors).length !== 0) {
				dispatch(actions.setErrorList(errors))
				return
			}
			dispatch(saveAcceleratorInstance(false)).then((data) => {
				if (data.payload) {
					dispatch(releaseLock()) as any
				}
			})
		}
		const handleOnClose = () => {
			dispatch(
				notificationActions.notifySuccess(
					i18n.t(ACCELERATOR_DISCARD_MESSAGE, { name: configValues.name })
				)
			)
			dispatch(actions.discardChanges(null))
			dispatch(releaseLock())
		}
		if (!isDataUpdated(getState() as RootState)) {
			dispatch(releaseLock())
		} else {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(COMMON_CONFORMATION_UPDATE_MESSAGE_LOCK, {
							name: configValues.name,
						}),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		}
	}
)
