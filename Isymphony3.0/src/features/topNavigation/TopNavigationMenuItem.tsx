import React, { useState } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { isArray, isNil } from 'lodash'
import { useLocation } from 'react-router-dom'
import { Button, Link, MenuItem, Menu } from '@material-ui/core'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import './topNavigation.scss'

export interface TopNavigationMenuItemProps {
	item: any
}

export function TopNavigationMenuItem(props: TopNavigationMenuItemProps) {
	const { item } = props
	const [anchorEl, setAnchorEl] = useState<Element | null>(null)
	const location = useLocation()
	const isPathActive = (item: any) => {
		const path = item.path
		if (path === '/apps') {
			return (
				location.pathname.indexOf('/apps') !== -1 ||
				location.pathname.indexOf('/app') !== -1
			)
		}
		return location.pathname.indexOf(path) !== -1
	}

	const handleMenuButtonClick = (
		event: React.MouseEvent<HTMLButtonElement, MouseEvent>
	) => {
		setAnchorEl(event.currentTarget)
	}

	const handleClose = () => {
		setAnchorEl(null)
	}

	const renderNavItemWithoutChildren = () => {
		return (
			<LinkContainer to={item.path}>
				<Link
					color='inherit'
					className={`app-nav ${isPathActive(item) ? 'activeSelf' : ''} ${
						item.stepClass
					}`}
				>
					{item.title}
				</Link>
			</LinkContainer>
		)
	}

	const renderNavItemWithChildren = () => {
		return (
			<React.Fragment>
				<Button
					aria-controls={item.id}
					aria-haspopup='true'
					color='inherit'
					onClick={handleMenuButtonClick}
					className={`app-nav ${isPathActive(item) ? 'active' : ''} ${
						item.stepClass
					}`}
				>
					{item.title}
					{<ArrowDropDownIcon />}
				</Button>
				<Menu
					className='button-menu'
					id={item.id}
					anchorEl={anchorEl}
					open={!isNil(anchorEl)}
					onClose={handleClose}
				>
					{item.children.map((child: any, idx: number) => (
						<LinkContainer to={item.path + child.path} key={idx}>
							<MenuItem onClick={handleClose}>{child.title}</MenuItem>
						</LinkContainer>
					))}
				</Menu>
			</React.Fragment>
		)
	}

	const render = () => {
		if (!item.includeInTopBar) {
			return null
		}
		if (!isArray(item.children) || item.children.length === 0) {
			return renderNavItemWithoutChildren()
		} else {
			return renderNavItemWithChildren()
		}
	}

	return render()
}
