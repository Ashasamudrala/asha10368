import { createAsyncThunk } from '@reduxjs/toolkit'
import { DialogTypes, showDialog } from '../dialogs'

export const showAboutPage = createAsyncThunk(
	'isyTour/showLandingPage',
	async (_: undefined, thunkArgs) => {
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.ABOUT,
			})
		)
	}
)
