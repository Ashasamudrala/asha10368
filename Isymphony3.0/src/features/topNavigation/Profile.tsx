import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import { makeStyles } from '@material-ui/core/styles'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import { IsyAvatarImage } from '../../widgets/IsyAvatarImage/IsyAvatarImage'
import {
	VIEW_PROFILE,
	VIEW_PROFILE_TRANSLATIONS,
	SIGN_OUT,
} from '../../utilities/constants'
import { useTranslation } from 'react-i18next'
import { LoginUserInfoState } from '../../authAndPermissions/loginUserDetails.types'

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: 'transparent',
		display: 'flex',
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
	},
}))

export interface ProfileProps {
	userDetails: LoginUserInfoState
	onCancel: () => void
	onAction: (action: string) => void
}

export function Profile(props: ProfileProps) {
	const { userDetails } = props
	const classes = useStyles()
	const { t } = useTranslation(VIEW_PROFILE_TRANSLATIONS)

	const getImageLetters = () => {
		const fl = userDetails.firstName || ''
		const ll = userDetails.lastName || ''
		return fl + ' ' + ll
	}

	const handleViewProfile = () => {
		props.onAction('ViewProfile')
	}

	const handleSignOut = () => {
		props.onAction('SignOut')
	}

	const renderProfileInfo = () => {
		return (
			<List>
				<ListItem alignItems='flex-start'>
					<CloseOutlinedIcon className='close-icon' onClick={props.onCancel} />
					<ListItemAvatar>
						<IsyAvatarImage
							imageClassName={classes.large}
							imageData={userDetails.profilePic}
							defaultName={getImageLetters()}
						/>
					</ListItemAvatar>
					<ListItemText
						primary={userDetails.displayName}
						secondary={
							<>
								<Typography>{userDetails.title}</Typography>
							</>
						}
					/>
				</ListItem>
			</List>
		)
	}

	const renderActions = () => {
		return (
			<ButtonGroup variant='text'>
				<IsyButton className='profile-btn' onClick={handleViewProfile}>
					{t(VIEW_PROFILE)}
				</IsyButton>
				<span>
					<Divider orientation='vertical' className='form-divider' flexItem />
				</span>
				<IsyButton className='sign-out-btn' onClick={handleSignOut}>
					{t(SIGN_OUT)}
				</IsyButton>
			</ButtonGroup>
		)
	}

	const render = () => {
		return (
			<Dialog
				onClose={props.onCancel}
				open={true}
				BackdropProps={{
					classes: {
						root: classes.root,
					},
				}}
				className='profile-box'
			>
				{renderProfileInfo()}
				<Divider className='form-divider' />
				{renderActions()}
			</Dialog>
		)
	}

	return render()
}
