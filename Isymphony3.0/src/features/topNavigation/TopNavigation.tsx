import React, { useEffect, useState } from 'react'
import { Profile } from './Profile'
import { EditProfile } from '../editProfile/editProfile'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import {
	getUpdatedPreferences,
	selectGetLogInUserInfo,
	selectGetSelectedAppId,
	selectGetSelectedAppName,
} from '../../authAndPermissions/loginUserDetails.selectors'
import { IsyAvatarImage } from '../../widgets/IsyAvatarImage/IsyAvatarImage'
import {
	fetchAllPreferences,
	logOutUser,
} from '../../authAndPermissions/loginUserDetails.asyncActions'
import { useHistory, useLocation } from 'react-router-dom'

import { AppBar, Toolbar, IconButton } from '@material-ui/core'
import HelpOutlineIcon from '@material-ui/icons/HelpOutline'
import {
	LOGIN_HEADER_TRANSLATION,
	ISYMPHONY_LOGO_ALT,
	HELP_MESSAGE_TO_CONTINUE,
	HELP_MESSAGE_TO_START,
	HELP,
	ABOUT,
	COMMON_USER_DOCUMENTATION,
} from '../../utilities/constants'
import './topNavigation.scss'
import { TopNavigationMenuItem } from './TopNavigationMenuItem'
import { isNil } from 'lodash'
import { IsyCan } from '../../widgets/IsyCan/IsyCan'
import { AppDevOps } from '../appDevOps/AppDevOps'
import {
	HelpModuleStatus,
	IsyPermissionTypes,
} from '../../authAndPermissions/loginUserDetails.types'
import { IsySteps } from '../../widgets/IsySteps/IsySteps'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import { showAboutPage } from './topNavigation.controller'
import { IsyPopover } from '../../widgets/IsyPopover/IsyPopover'
import { getHelpTourStartSteps } from '../../utilities/isyStepsConfig'
import { CustomButtonType } from '../../widgets/IsySteps/IsyTour/Steps'

export enum TopNavigationDialogTypes {
	NONE,
	DETAILS,
	EDIT_PROFILE,
}

export enum TopHelpMenuItem {
	HELP = 'help',
	ABOUT = 'about',
	USER_DOCUMENTATION = 'userDocumentation',
}

export enum CurrentModules {
	APPS = 'Apps',
	ACCELERATORS = 'Accelerators',
	WEB_SERVICES = 'WebServices',
	DATABASE = 'Database',
}

export interface TopNavigationProps {
	routes: any[]
}

export function TopNavigation(props: TopNavigationProps) {
	const { routes } = props
	const userDetails = useSelector(selectGetLogInUserInfo)
	const selectedAppName = useSelector(selectGetSelectedAppName)
	const selectedAppId = useSelector(selectGetSelectedAppId)
	const preferences = useSelector(getUpdatedPreferences)
	const [currentView, setCurrentView] = useState<TopNavigationDialogTypes>(
		TopNavigationDialogTypes.NONE
	)
	const [isEnabledHelpText, setIsEnabledHelpText] = useState<boolean>(false)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)

	const dispatch = useDispatch()
	const history = useHistory()
	const location = useLocation()

	const { t } = useTranslation(LOGIN_HEADER_TRANSLATION)
	useEffect(() => {
		dispatch(fetchAllPreferences())
	}, [dispatch])

	useEffect(() => {
		return () => {
			dispatch(preferenceActions.clearData(null))
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	const sortOptions = () => [
		{
			key: TopHelpMenuItem.HELP,
			name: t(HELP),
		},
		{
			key: TopHelpMenuItem.USER_DOCUMENTATION,
			name: t(COMMON_USER_DOCUMENTATION),
		},
		{
			key: TopHelpMenuItem.ABOUT,
			name: t(ABOUT),
		},
	]

	const getIsInAppView = () => {
		return location.pathname.indexOf('/app/') !== -1
	}

	const getCurrentModule = () => {
		if (location.pathname.indexOf('/apps') !== -1) return CurrentModules.APPS
		if (location.pathname.indexOf('/marketplace') !== -1)
			return CurrentModules.ACCELERATORS
		if (location.pathname.indexOf('/webServices') !== -1)
			return CurrentModules.WEB_SERVICES
		if (location.pathname.indexOf(`/database`) !== -1)
			return CurrentModules.DATABASE
		return null
	}

	const getImageLetters = () => {
		const fl = (userDetails && userDetails.firstName) || ''
		const ll = (userDetails && userDetails.lastName) || ''
		return fl + ' ' + ll
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handlePopoverShow = (event: any) => {
		if (!isEnabledHelpText) {
			setPopoverAnchorEl(event.currentTarget)
		}
	}

	const handleOnClickOfLogo = () => {
		history.push('/apps')
	}

	const handleClickOpen = () => {
		setCurrentView(TopNavigationDialogTypes.DETAILS)
	}

	const handleCancel = () => {
		setCurrentView(TopNavigationDialogTypes.NONE)
	}

	const handleAction = (type: string) => {
		switch (type) {
			case 'ViewProfile':
				setCurrentView(TopNavigationDialogTypes.EDIT_PROFILE)
				break
			case 'SignOut':
				setCurrentView(TopNavigationDialogTypes.NONE)
				;(dispatch(logOutUser()) as any).then(() => {
					setTimeout(() => {
						history.push('/login')
					})
				})

				break
			default:
		}
	}

	const handleHelpTourExit = () => {
		setIsEnabledHelpText(false)
	}

	const handleIsyTourHelpSteps = (_: number, type: string) => {
		const currentModule = getCurrentModule()
		if (type === CustomButtonType.START) {
			setIsEnabledHelpText(false)
			if (currentModule === CurrentModules.APPS) {
				dispatch(
					preferenceActions.updateAppsPreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			} else if (currentModule === CurrentModules.ACCELERATORS) {
				dispatch(
					preferenceActions.updateAcceleratorsPreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			} else if (currentModule === CurrentModules.WEB_SERVICES) {
				dispatch(
					preferenceActions.updateWebServicesPreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			} else if (currentModule === CurrentModules.DATABASE) {
				dispatch(
					preferenceActions.updateDatabasePreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			}
		} else {
			setIsEnabledHelpText(false)
		}
	}

	const handleMenuItems = (_: React.MouseEvent<HTMLLIElement>, key: string) => {
		setPopoverAnchorEl(null)
		switch (key) {
			case TopHelpMenuItem.HELP: {
				setIsEnabledHelpText(true)
				break
			}
			case TopHelpMenuItem.ABOUT: {
				dispatch(showAboutPage())
				break
			}
			case TopHelpMenuItem.USER_DOCUMENTATION: {
				const a = document.createElement('a')
				a.href = '/iSymphony-User-Documentation.pdf'
				a.target = '_blank'
				a.click()
				break
			}
			default:
		}
	}

	const renderIsyHelpTourSteps = () => {
		const firstName = userDetails && userDetails.firstName
		const currentModule = getCurrentModule()
		let helpDescription = t(HELP_MESSAGE_TO_START, {
			firstName: firstName,
			currentModule: currentModule,
		})

		if (
			preferences &&
			!isNil(currentModule) &&
			((currentModule === CurrentModules.APPS &&
				preferences.apps &&
				preferences.apps.status === HelpModuleStatus.INCOMPLETE) ||
				(currentModule === CurrentModules.WEB_SERVICES &&
					preferences.webServices &&
					preferences.webServices.status === HelpModuleStatus.INCOMPLETE) ||
				(currentModule === CurrentModules.ACCELERATORS &&
					preferences.accelerators &&
					preferences.accelerators.status === HelpModuleStatus.INCOMPLETE) ||
				(currentModule === CurrentModules.DATABASE &&
					preferences.database &&
					preferences.database.status === HelpModuleStatus.INCOMPLETE))
		) {
			helpDescription = t(HELP_MESSAGE_TO_CONTINUE, {
				firstName: firstName,
				currentModule: currentModule,
			})
		}

		const steps = getHelpTourStartSteps(helpDescription)

		return (
			<IsySteps
				steps={steps}
				initialStep={0}
				isEnabled={isEnabledHelpText}
				getCustomButtonData={handleIsyTourHelpSteps}
				onExit={handleHelpTourExit}
			/>
		)
	}

	/**
	 * handles the Top Navigation Header Name
	 */
	const renderTopNavigationHeader = () => {
		return (
			<img
				src={`/images/iSymphonyLogo.svg`}
				alt={t(ISYMPHONY_LOGO_ALT)}
				className='header-logo'
				onClick={handleOnClickOfLogo}
			/>
		)
	}

	const renderDialogs = () => {
		if (isNil(userDetails)) {
			return null
		}
		switch (currentView) {
			case TopNavigationDialogTypes.DETAILS: {
				return (
					<Profile
						userDetails={userDetails}
						onCancel={handleCancel}
						onAction={handleAction}
					/>
				)
			}
			case TopNavigationDialogTypes.EDIT_PROFILE: {
				return <EditProfile onClose={handleCancel} userId={userDetails.id} />
			}
			default:
				return null
		}
	}

	const renderTopNavigationDownloadIcon = () => {
		return <AppDevOps appId={selectedAppId} appName={selectedAppName} />
	}

	const renderTopNavigationHelpIcon = () => {
		const currentModule = getCurrentModule()
		if (isNil(currentModule)) {
			return null
		}
		return (
			<span className='help-icon' role='button'>
				<HelpOutlineIcon
					className='isy-about-logo'
					onClick={handlePopoverShow}
				/>
				{renderPopover()}
				{renderIsyHelpTourSteps()}
			</span>
		)
	}

	const renderPopover = () => {
		if (isNil(popoverAnchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={sortOptions()}
				className={'instructions-popover'}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
			/>
		)
	}

	/**
	 * handles the Top Navigation User Icon
	 */
	const renderTopNavigationUser = () => {
		return (
			<IsyCan
				action={IsyPermissionTypes.VIEW_USER_PROFILE}
				yes={() => (
					<div className='user-icon-div'>
						<IconButton onClick={handleClickOpen}>
							<IsyAvatarImage
								imageData={userDetails && userDetails.profilePic}
								defaultName={getImageLetters()}
							/>
						</IconButton>
						{renderDialogs()}
					</div>
				)}
			/>
		)
	}

	/**
	 * handles the top Navigation Items(Apps, Team, and Admin)
	 */
	const renderTopNavigationItems = () => {
		return (
			<React.Fragment>
				{routes.map((item) => {
					return <TopNavigationMenuItem item={item} key={item.id} />
				})}
			</React.Fragment>
		)
	}

	const renderAppName = () => {
		return <div className='app-name'>{selectedAppName}</div>
	}

	return (
		<div>
			<AppBar className='AppBar' position='static'>
				<Toolbar variant='dense' className='tool-bar'>
					{renderTopNavigationHeader()}
					{getIsInAppView() ? renderAppName() : renderTopNavigationItems()}
					<div className='right-side-icons'>
						{renderTopNavigationDownloadIcon()}
						{renderTopNavigationHelpIcon()}
						{renderTopNavigationUser()}
					</div>
				</Toolbar>
			</AppBar>
		</div>
	)
}
