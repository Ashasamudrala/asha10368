import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { filter, isNil, isNumber, unionBy } from 'lodash'
import { useTranslation } from 'react-i18next'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import {
	APP_DEV_OPS_FOOTER_DONE_MESSAGE,
	APP_DEV_OPS_FOOTER_MESSAGE,
	APP_DEV_OPS_RELAUNCH,
	APP_DEV_OPS_START,
	APP_DEV_OPS_START_BTN_MESSAGE,
	APP_DEV_OPS_TRANSLATIONS,
} from '../../utilities/constants'
import './appDevOps.scss'
import {
	selectEndTime,
	selectIsDialogOpen,
	selectJobStatus,
	selectJobTasks,
} from './appDevOps.selectors'
import { actions } from './appDevOps.slice'
import { LinearProgress } from '@material-ui/core'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import {
	downloadCode,
	loadAppLatestJob,
	startDevOpsJob,
} from './appDevOps.asyncActions'
import {
	AppDevOpsJobStatus,
	AppDevOpsJobTaskPros,
	AppDevOpsJobTaskStatus,
	AppDevOpsJobTaskType,
} from './appDevOps.types'
import { AppDevOpsTaskItem } from './AppDevOpsTaskItem'
import moment from 'moment'

export interface AppDevOpsProps {
	appId: string | null
	appName: string | null
}

export function AppDevOps(props: AppDevOpsProps) {
	const isDialogOpen = useSelector(selectIsDialogOpen)
	const jobStatus = useSelector(selectJobStatus)
	const jobTasks = useSelector(selectJobTasks)
	const endTime = useSelector(selectEndTime)

	const dispatch = useDispatch()
	const { t } = useTranslation(APP_DEV_OPS_TRANSLATIONS)

	useEffect(() => {
		if (isNil(props.appId)) {
			dispatch(actions.clearData(null))
		} else {
			dispatch(loadAppLatestJob(props.appId))
		}
	}, [props.appId, dispatch])

	const getJobTasks = () => {
		const subTaskStatus =
			jobStatus === AppDevOpsJobStatus.NONE
				? AppDevOpsJobTaskStatus.NEW
				: AppDevOpsJobTaskStatus.NOT_CONFIG
		const tasksDefaultList: AppDevOpsJobTaskPros[] = [
			{
				status: subTaskStatus,
				type: AppDevOpsJobTaskType.CODE_GENERATION,
				metadata: {},
			},
			{
				status: subTaskStatus,
				type: AppDevOpsJobTaskType.CODE_COMMIT,
				metadata: {},
			},
			{
				status: subTaskStatus,
				type: AppDevOpsJobTaskType.CI,
				metadata: {},
			},
		]
		return unionBy(jobTasks, tasksDefaultList, 'type')
	}

	const getProgressBarStatus = () => {
		const completedTasksLength = filter(
			jobTasks,
			(i) => i.status === AppDevOpsJobTaskStatus.DONE
		).length
		return (completedTasksLength / jobTasks.length) * 100
	}

	const handleCloseDialog = () => {
		dispatch(actions.setDialogState(false))
	}

	const handleShowDialog = () => {
		dispatch(actions.setDialogState(true))
	}

	const handleDownloadCode = (fileName: string) => {
		if (!isNil(props.appId)) {
			dispatch(downloadCode({ appId: props.appId, fileName }))
		}
	}

	const handleStartDevOpsJob = () => {
		if (!isNil(props.appId)) {
			dispatch(startDevOpsJob(props.appId))
		}
	}

	const renderStartSection = () => {
		return (
			<div className='start-section'>
				<IsyButton className='secondary-btn' onClick={handleStartDevOpsJob}>
					{t(APP_DEV_OPS_START)}
				</IsyButton>
				<div className='section-text'>{t(APP_DEV_OPS_START_BTN_MESSAGE)}</div>
			</div>
		)
	}

	const renderBodySection = () => {
		return (
			<div className='middle-section'>
				{getJobTasks().map((item: AppDevOpsJobTaskPros, index: number) => {
					return (
						<AppDevOpsTaskItem
							onDownloadCode={handleDownloadCode}
							item={item}
							index={index + 1}
							key={index}
						/>
					)
				})}
			</div>
		)
	}

	const renderFooterSection = () => {
		switch (jobStatus) {
			case AppDevOpsJobStatus.NONE: {
				return null
			}
			case AppDevOpsJobStatus.NEW: {
				return <div className='footer'>{t(APP_DEV_OPS_FOOTER_MESSAGE)}</div>
			}
			case AppDevOpsJobStatus.IN_PROGRESS: {
				return <div className='footer'>{t(APP_DEV_OPS_FOOTER_MESSAGE)}</div>
			}
			case AppDevOpsJobStatus.FAILED: {
				return (
					<div className='relaunch-btn'>
						<IsyButton className='primary-btn' onClick={handleStartDevOpsJob}>
							{t(APP_DEV_OPS_RELAUNCH)}
						</IsyButton>
					</div>
				)
			}
			case AppDevOpsJobStatus.DONE: {
				return (
					<span>
						<div className='done-msg-footer'>
							{t(APP_DEV_OPS_FOOTER_DONE_MESSAGE, {
								endTime: isNumber(endTime)
									? moment(endTime).format('MM/D/YYYY') +
									  ' at ' +
									  moment(endTime).format('H:mm:ss')
									: '',
							})}
						</div>
						<div className='relaunch-btn'>
							<IsyButton className='primary-btn' onClick={handleStartDevOpsJob}>
								{t(APP_DEV_OPS_RELAUNCH)}
							</IsyButton>
						</div>
					</span>
				)
			}
			default:
				return null
		}
	}

	const renderDialogTitle = () => {
		return (
			<DialogTitle>
				<CloseOutlinedIcon className='close-icon' onClick={handleCloseDialog} />
				<span className='dialog-heading'>{props.appName}</span>
			</DialogTitle>
		)
	}

	const renderDialogContent = () => {
		return (
			<DialogContent>
				<div className='select-download-code'>
					{jobStatus === AppDevOpsJobStatus.NONE && renderStartSection()}
					{renderBodySection()}
					{renderFooterSection()}
				</div>
			</DialogContent>
		)
	}

	const renderDialog = () => {
		return (
			<Dialog
				open={isDialogOpen}
				onClose={handleCloseDialog}
				className={'dialog-box-common download-code-dialog'}
			>
				{renderDialogTitle()}
				{renderDialogContent()}
			</Dialog>
		)
	}

	const renderProgressBar = () => {
		if (
			jobStatus === AppDevOpsJobStatus.IN_PROGRESS ||
			jobStatus === AppDevOpsJobStatus.DONE
		) {
			return (
				<LinearProgress
					value={getProgressBarStatus()}
					variant='determinate'
					className='linear-progress-bar'
				/>
			)
		}
		return null
	}

	const renderIcon = () => {
		return (
			<div className='download-icon' role='button' onClick={handleShowDialog}>
				<img
					className='isy-download-logo'
					src='/images/Download.svg'
					alt='dev ops'
				/>
				{renderProgressBar()}
			</div>
		)
	}

	const render = () => {
		if (isNil(props.appId)) {
			return null
		}
		return (
			<React.Fragment>
				{renderDialog()}
				{renderIcon()}
			</React.Fragment>
		)
	}

	return render()
}
