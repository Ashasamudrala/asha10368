import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import {
	DELETE_DATABASE,
	GET_ALL_DATABASES_WITH_SCHEMAS,
	GET_ATTRIBUTE_TYPES,
	SAVE_SCHEMA,
} from '../../utilities/apiEndpoints'
import i18n from 'i18next'
import {
	DATABASE_DELETE_MESSAGE_SUCCESS,
	DATABASE_SCHEMA_ADDED_MESSAGE_SUCCESS,
} from '../../utilities/constants'

export const fetchDatabasesListWithSchemas = createAsyncThunk(
	'databases/fetchDatabasesListWithSchemas',
	async (appId: string, thunkArgs) =>
		await doAsync({
			url: GET_ALL_DATABASES_WITH_SCHEMAS(appId),
			...thunkArgs,
		})
)

export const fetchAttributeTypes = createAsyncThunk(
	'databases/dataType',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: GET_ATTRIBUTE_TYPES,
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const addNewSchema = createAsyncThunk(
	'databases/addNewSchema',
	async (
		data: { appId: string; databaseId: string; name: string },
		thunkArgs
	) =>
		await doAsync({
			url: SAVE_SCHEMA(data.appId, data.databaseId),
			httpMethod: 'post',
			noBusySpinner: true,
			httpConfig: {
				body: JSON.stringify({
					name: data.name,
					displayName: data.name,
				}),
			},
			successMessage: i18n.t(DATABASE_SCHEMA_ADDED_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			...thunkArgs,
		})
)

export const deleteDatabase = createAsyncThunk(
	'databases/deleteDatabase',
	async (
		data: { appId: string; databaseId: string; name: string },
		thunkArgs
	) =>
		await doAsync({
			url: DELETE_DATABASE(data.appId, data.databaseId),
			httpMethod: 'delete',
			successMessage: i18n.t(DATABASE_DELETE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			...thunkArgs,
		})
)
