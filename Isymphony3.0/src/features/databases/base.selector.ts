import { RootState } from '../../base.types'
import { DatabasesBaseState } from './base.types'

export const name = 'databases'
export const selectSlice = (state: RootState): DatabasesBaseState => {
	return state[name]
}
