import { createAsyncThunk } from '@reduxjs/toolkit'
import {
	addNewSchema,
	deleteDatabase,
	fetchAttributeTypes,
	fetchDatabasesListWithSchemas,
} from './databases.asyncActions'
import { actions as dbActions } from './databases.slice'
import { RootState } from '../../base.types'
import { switchConformation } from './canvas/canvas.controller'
import { actions as settingActions } from './settings/settings.slice'
import { isNil } from 'lodash'
import { DialogTypes, showDialog } from '../dialogs'
import { AddDatabaseSchemaReturnProps } from '../dialogs/addDatabaseSchema/addDatabaseSchema.types'
import { selectAppId, selectDatabasesById } from './databases.selectors'
import i18n from 'i18next'
import { DATABASE_DELETE_MESSAGE } from '../../utilities/constants'

export const initDatabaseService = createAsyncThunk(
	'databases/initWebService',
	async (id: string, { dispatch }) => {
		dispatch(dbActions.setAppId(id))
		dispatch(fetchDatabasesListWithSchemas(id))
		dispatch(fetchAttributeTypes())
	}
)

export const confirmOnSelectDatabase = createAsyncThunk(
	'databases/confirmOnSelectDatabase',
	async (id: string, { dispatch }) => {
		dispatch(
			switchConformation(() => {
				dispatch(dbActions.setDatabaseId(id))
			})
		)
	}
)

export const confirmOnSelectSchema = createAsyncThunk(
	'databases/confirmOnSelectSchema',
	async (id: string, { dispatch }) => {
		dispatch(
			switchConformation(() => {
				dispatch(dbActions.setSchemaId(id))
			})
		)
	}
)

export const confirmOnOpenDbSettings = createAsyncThunk(
	'databases/confirmOnOpenDbSettings',
	async (databaseId: string | null, { dispatch }) => {
		dispatch(
			switchConformation(() => {
				if (isNil(databaseId)) {
					dispatch(settingActions.openPanelInNewMode(null))
				} else {
					dispatch(settingActions.editPanelInNewMode(databaseId))
				}
			})
		)
	}
)

export const confirmOnAddSchema = createAsyncThunk(
	'databases/confirmOnAddSchema',
	async (databaseId: string, { dispatch, getState }) => {
		const appId = selectAppId(getState() as RootState)
		if (isNil(appId)) {
			return
		}
		dispatch(
			switchConformation(() => {
				const conformationCallBack = (data: AddDatabaseSchemaReturnProps) => {
					dispatch(addNewSchema({ appId, databaseId, name: data.name }))
				}
				dispatch(
					showDialog({
						type: DialogTypes.ADD_DATABASE_SCHEMA,
						onOkay: conformationCallBack,
					})
				)
			})
		)
	}
)

export const confirmDatabaseDelete = createAsyncThunk(
	'databases/confirmDatabaseDelete',
	async (databaseId: string, { dispatch, getState }) => {
		const appId = selectAppId(getState() as RootState)
		const database = selectDatabasesById(getState() as RootState, databaseId)
		if (isNil(appId) || isNil(database)) {
			return
		}
		const conformationCallBack = () => {
			dispatch(deleteDatabase({ databaseId, appId, name: database.name }))
		}
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: conformationCallBack,
				data: {
					message: i18n.t(DATABASE_DELETE_MESSAGE, { name: database.name }),
				},
			})
		)
	}
)
