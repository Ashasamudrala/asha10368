import {
	CONNECT_DATABASE,
	DATABASE_FIND_BY_NAME,
	UPDATE_DB_CONFIG,
} from '../../../utilities/apiEndpoints'
import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import { ConnectToYourDatabaseProps } from './settings.types'
import { isNil, omit } from 'lodash'
import { validateDbName } from './settings.utilities'

export const loadDBDetails = createAsyncThunk(
	'databases/schema/loadDBDetails',
	async (data: { databaseId: string; appId: string }, thunkArgs) => {
		return await doAsync({
			url: UPDATE_DB_CONFIG(data.appId, data.databaseId),
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const connectDatabase = createAsyncThunk(
	'databases/settings/connectDatabase',
	async (
		data: {
			appId: string
			configData: ConnectToYourDatabaseProps
		},
		thunkArgs
	) => {
		return await doAsync({
			url: CONNECT_DATABASE(data.appId),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					configurations: {
						[data.configData.environmentType]: omit(data.configData, [
							'isConnectionDetailsOpen',
							'schemas',
						]),
					},
				}),
			},
			noBusySpinner: true,
			propagateAPIError: true,
			...thunkArgs,
		})
	}
)

export const validateDbNameDuplicate = createAsyncThunk(
	'databases/settings/validateDbName',
	async (data: { appId: string; name: string }, thunkArgs) => {
		if (isNil(validateDbName(data.name, {}))) {
			return await doAsync({
				url: DATABASE_FIND_BY_NAME(data.appId, data.name),
				noBusySpinner: true,
				...thunkArgs,
			})
		}
	}
)
