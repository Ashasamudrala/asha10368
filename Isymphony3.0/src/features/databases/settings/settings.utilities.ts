import {
	CONNECT,
	CONNECTION_DETAILS,
	CONNECT_SCHEMA_TEXT,
	CONNECT_SELECT_VENDOR,
	DATABASE_DUPLICATE_NAME,
	DATABASE_NAME,
	DATABASE_NAME_PLACEHOLDER,
	DATABASE_PORT_PLACEHOLDER,
	DATABASE_VENDOR,
	DATABASE_VENDOR_EMPTY,
	DB2,
	ENTER_HOST_PLACEHOLDER,
	ENTER_PASSWORD_PLACEHOLDER,
	ENTER_USERNAME_PLACEHOLDER,
	HOST,
	HOST_EMPTY,
	INVALID_DATABASE_NAME,
	INVALID_DATABASE_NAME_START,
	MYSQL,
	ORACLE,
	PASSWORD,
	PASSWORD_EMPTY,
	PASSWORD_INVALID,
	PORT,
	PORT_EMPTY,
	PORT_INVALID,
	POSTGRE_SQL,
	SCHEMA_DETAILS,
	SQL_SERVER,
	USERNAME,
	USERNAME_EMPTY,
	VALIDATE_DATABASE_NAME_LENGTH,
	VALIDATE_NAME,
} from '../../../utilities/constants'
import {
	IsyFormBuilderButtonProps,
	IsyFormBuilderComponentItemProps,
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
	IsyFormBuilderNumberProps,
	IsyFormBuilderPasswordProps,
	IsyFormBuilderSelectProps,
	IsyFormBuilderStringProps,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import i18n from 'i18next'
import {
	ConnectToYourDatabaseConnectionStatus,
	ConnectToYourDatabaseProps,
	CreateDatabaseState,
	EditDatabaseSettingState,
	SettingConfigurationValues,
} from './settings.types'
import { isEmpty, isNil } from 'lodash'
import { DatabaseVendorOptionsTypes } from '../../../utilities/apiEnumConstants'

const regex = /^[a-zA-Z]+[a-zA-Z0-9_#@$ ]*$/i
const portValidRegex = /^\d{4}$/
const dbPasswordRegex = /^[a-zA-Z0-9]{8,}$/

const getVendorTypeOptions = () => {
	return [
		{
			id: DatabaseVendorOptionsTypes.MYSQL,
			name: i18n.t(MYSQL),
		},
		{
			id: DatabaseVendorOptionsTypes.ORACLE,
			name: i18n.t(ORACLE),
		},
		{
			id: DatabaseVendorOptionsTypes.POSTGRESQL,
			name: i18n.t(POSTGRE_SQL),
		},
		{
			id: DatabaseVendorOptionsTypes.SQLSERVER,
			name: i18n.t(SQL_SERVER),
		},
		{
			id: DatabaseVendorOptionsTypes.DB2,
			name: i18n.t(DB2),
		},
	]
}

const getNameConfig = (
	isEditable: boolean,
	handleNameChange: (name: string) => void
): IsyFormBuilderStringProps => {
	return {
		type: IsyFormBuilderFormTypes.STRING,
		title: i18n.t(DATABASE_NAME),
		dataRef: 'name',
		placeholder: i18n.t(DATABASE_NAME_PLACEHOLDER),
		autoFocus: true,
		isRequired: true,
		disabled: !isEditable,
		onChange: handleNameChange,
	}
}

const getVendorTypeConfig = (
	isDisabled: boolean
): IsyFormBuilderSelectProps => {
	return {
		type: IsyFormBuilderFormTypes.SELECT,
		title: i18n.t(DATABASE_VENDOR),
		dataRef: 'platform',
		placeholder: i18n.t(CONNECT_SELECT_VENDOR),
		isRequired: true,
		options: getVendorTypeOptions(),
		disabled: isDisabled,
	}
}

const getHostConfig = (isDisabled: boolean): IsyFormBuilderStringProps => {
	return {
		type: IsyFormBuilderFormTypes.STRING,
		title: i18n.t(HOST),
		dataRef: 'host',
		placeholder: i18n.t(ENTER_HOST_PLACEHOLDER),
		isRequired: true,
		disabled: isDisabled,
	}
}

const getPortConfig = (isDisabled: boolean): IsyFormBuilderNumberProps => {
	return {
		type: IsyFormBuilderFormTypes.NUMBER,
		title: i18n.t(PORT),
		dataRef: 'port',
		placeholder: i18n.t(DATABASE_PORT_PLACEHOLDER),
		isRequired: true,
		disabled: isDisabled,
	}
}

const getUsernameConfig = (isDisabled: boolean): IsyFormBuilderStringProps => {
	return {
		type: IsyFormBuilderFormTypes.STRING,
		title: i18n.t(USERNAME),
		dataRef: 'userName',
		placeholder: i18n.t(ENTER_USERNAME_PLACEHOLDER),
		isRequired: true,
		disabled: isDisabled,
	}
}

const getPasswordConfig = (
	isDisabled: boolean
): IsyFormBuilderPasswordProps => {
	return {
		type: IsyFormBuilderFormTypes.PASSWORD,
		title: i18n.t(PASSWORD),
		dataRef: 'password',
		placeholder: i18n.t(ENTER_PASSWORD_PLACEHOLDER),
		isRequired: true,
		disabled: isDisabled,
	}
}

const getConnectButton = (
	handleConnect: () => void
): IsyFormBuilderButtonProps => {
	return {
		type: IsyFormBuilderFormTypes.BUTTON,
		label: i18n.t(CONNECT),
		onClick: handleConnect,
		className: 'secondary-btn',
	}
}

const getStatusComponent = (
	isHidden: boolean,
	className: string
): IsyFormBuilderComponentItemProps => {
	return {
		type: IsyFormBuilderFormTypes.COMPONENT,
		dataRef: '',
		componentType: 'STATUS',
		isHidden,
		className,
	}
}

export const getCreateDatabaseFormBuilderConfig = (
	databaseId: string | null,
	isConnectDatabase: boolean | null,
	isConnectionDetailsOpen: boolean,
	connectionStatus: ConnectToYourDatabaseConnectionStatus,
	handleConnect: () => void,
	handleNameChange: (name: string) => void
): IsyFormBuilderFormItemProps[] => {
	if (isNil(databaseId)) {
		// new db
		if (isConnectDatabase) {
			// connect
			const isDisabled =
				connectionStatus === ConnectToYourDatabaseConnectionStatus.IN_PROGRESS
			return [
				{
					type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
					title: i18n.t(CONNECTION_DETAILS),
					openDataRef: 'isConnectionDetailsOpen',
					className: !isConnectionDetailsOpen ? 'no-border' : '',
					forms: [
						getStatusComponent(false, ''),
						getNameConfig(!isDisabled, handleNameChange),
						getVendorTypeConfig(isDisabled),
						getHostConfig(isDisabled),
						getPortConfig(isDisabled),
						getUsernameConfig(isDisabled),
						getPasswordConfig(isDisabled),
						getConnectButton(handleConnect),
					],
				},
				getStatusComponent(isConnectionDetailsOpen, 'out-of-accordion'),
				{
					type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
					isExpandByDefault: true,
					title: i18n.t(SCHEMA_DETAILS),
					forms: [
						{
							type: IsyFormBuilderFormTypes.INFO,
							text: i18n.t(CONNECT_SCHEMA_TEXT),
						},
						{
							type: IsyFormBuilderFormTypes.CHECKBOX_LIST,
							dataRef: 'schemas',
							items: 'schemasList',
						},
					],
					isHidden:
						connectionStatus !==
						ConnectToYourDatabaseConnectionStatus.CONNECTED,
				},
			]
		} else {
			// create
			return [
				{
					type: IsyFormBuilderFormTypes.SECTION,
					forms: [getNameConfig(true, handleNameChange)],
				},
			]
		}
	} else {
		if (isConnectDatabase) {
			// connect
		} else {
			// create
			return [
				{
					type: IsyFormBuilderFormTypes.SECTION,
					forms: [
						getNameConfig(false, handleNameChange),
						getVendorTypeConfig(false),
						getHostConfig(false),
						getPortConfig(false),
						getUsernameConfig(false),
						getPasswordConfig(false),
					],
				},
			]
		}
	}
	return []
}

export const validateDbName = (
	name: string,
	prevErrors: { [key: string]: string }
) => {
	if (isEmpty(name)) {
		return i18n.t(VALIDATE_NAME)
	} else if (!regex.test(name)) {
		return i18n.t(INVALID_DATABASE_NAME)
	} else if (name.indexOf('ii') === 0) {
		return i18n.t(INVALID_DATABASE_NAME_START)
	} else if (name.length < 3 || name.length >= 32) {
		return i18n.t(VALIDATE_DATABASE_NAME_LENGTH)
	} else if (prevErrors.name === i18n.t(DATABASE_DUPLICATE_NAME)) {
		// whenever duplicate is present setting to name to duplicate
		return prevErrors.name
	}
	return null
}

const validatePlatform = (platform: string) => {
	if (isEmpty(platform)) {
		return i18n.t(DATABASE_VENDOR_EMPTY)
	}
	return null
}

const validateHost = (host: string) => {
	if (isEmpty(host)) {
		return i18n.t(HOST_EMPTY)
	}
	return null
}

const validatePort = (port: string) => {
	if (isEmpty(port)) {
		return i18n.t(PORT_EMPTY)
	} else if (!portValidRegex.test(port)) {
		return i18n.t(PORT_INVALID)
	}
	return null
}

const validateUserName = (userName: string) => {
	if (isEmpty(userName)) {
		return i18n.t(USERNAME_EMPTY)
	}
	return null
}

const validatePassword = (password: string) => {
	if (isEmpty(password)) {
		return i18n.t(PASSWORD_EMPTY)
	} else if (!dbPasswordRegex.test(password)) {
		return i18n.t(PASSWORD_INVALID)
	}
	return null
}

const validateConnectionDetails = (
	info: EditDatabaseSettingState | ConnectToYourDatabaseProps,
	prevErrors: { [key: string]: string }
) => {
	let errors: { [key: string]: string } = {}
	const nameError = validateDbName(info.name, prevErrors)
	if (!isNil(nameError)) {
		errors.name = nameError
	}
	const platformError = validatePlatform(info.platform)
	if (!isNil(platformError)) {
		errors.platform = platformError
	}
	const hostError = validateHost(info.host)
	if (!isNil(hostError)) {
		errors.host = hostError
	}
	const portError = validatePort(
		isNil(info) ? '' : ((info.port + '') as string)
	)
	if (!isNil(portError)) {
		errors.port = portError
	}
	const userNameError = validateUserName(info.userName)
	if (!isNil(userNameError)) {
		errors.userName = userNameError
	}
	const passwordError = validatePassword(info.password)
	if (!isNil(passwordError)) {
		errors.password = passwordError
	}
	return errors
}

export const validateConfigurationData = (
	data: SettingConfigurationValues,
	isConnectDatabase: boolean | null,
	databaseId: string | null,
	prevErrors: { [key: string]: string }
) => {
	let errors: { [key: string]: string } = {}
	if (isNil(databaseId)) {
		// new db
		if (isConnectDatabase) {
			// connect
			errors = validateConnectionDetails(
				data as ConnectToYourDatabaseProps,
				prevErrors
			)
		} else {
			//create
			const info = data as CreateDatabaseState
			const nameError = validateDbName(info.name, prevErrors)
			if (!isNil(nameError)) {
				errors.name = nameError
			}
		}
	} else {
		if (isConnectDatabase) {
			// connect
		} else {
			// create
			errors = validateConnectionDetails(
				data as EditDatabaseSettingState,
				prevErrors
			)
		}
	}
	return errors
}
