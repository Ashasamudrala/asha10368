import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import i18n from 'i18next'
import { cloneDeep, isArray, isEmpty, isNil, pick } from 'lodash'
import { Action } from '../../../common.types'
import { DatabasesSubReducersNames } from '../base.types'
import {
	ConnectionSchemaProps,
	ConnectToYourDatabaseConnectionStatus,
	ConnectToYourDatabaseProps,
	CreateDatabaseState,
	EditDatabaseSettingState,
	SettingConfigurationValues,
	SettingsState,
	SettingsViewState,
} from './settings.types'
import * as asyncActions from './settings.asyncActions'
import { DatabaseVendorOptionsTypes } from '../../../utilities/apiEnumConstants'
import { DATABASE_DUPLICATE_NAME } from '../../../utilities/constants'

const createDatabaseConfigValuesDefault: CreateDatabaseState = {
	name: '',
}

const initialState: SettingsState = {
	state: SettingsViewState.NONE,
	databaseId: null,
	isConnectDatabase: null,
	configurationValues: {},
	prevConfigurationValues: {},
	errors: {},
	connectionStatus: ConnectToYourDatabaseConnectionStatus.INITIAL,
	connectionStatusError: null,
	connectionSchemas: [],
	configurationValuesForConnectValidation: null,
	stateError: null,
}

const defaultEditConfig: EditDatabaseSettingState = {
	password: '',
	userName: '',
	port: '',
	type: 'RDBMS',
	host: '',
	platform: DatabaseVendorOptionsTypes.POSTGRESQL,
	name: '',
	environmentType: 'Development',
	provider: 'Container',
}

const connectToYourDatabaseDefaults: ConnectToYourDatabaseProps = {
	...defaultEditConfig,
	isConnectionDetailsOpen: true,
	schemas: [],
}

const slice = createSlice<
	SettingsState,
	SliceCaseReducers<SettingsState>,
	DatabasesSubReducersNames.SETTINGS
>({
	name: DatabasesSubReducersNames.SETTINGS,
	initialState,
	reducers: {
		// synchronous actions
		setViewCurrentState(state, action: Action<SettingsViewState>) {
			state.state = action.payload
			state.stateError = null
		},
		setStateError(state, action: Action<string>) {
			state.stateError = action.payload
			state.state = SettingsViewState.FAILURE
		},
		setIsConnectDatabase(state, action: Action<boolean>) {
			state.isConnectDatabase = action.payload
			state.state = SettingsViewState.EDIT
			if (state.isConnectDatabase) {
				state.configurationValues = cloneDeep(connectToYourDatabaseDefaults)
			} else {
				state.configurationValues = cloneDeep(createDatabaseConfigValuesDefault)
			}
			state.prevConfigurationValues = cloneDeep(state.configurationValues)
		},
		openPanelInNewMode(state) {
			state.state = SettingsViewState.LANDING
		},
		editPanelInNewMode(state, action: Action<string>) {
			state.databaseId = action.payload
			state.state = SettingsViewState.EDIT
		},
		revertConfigValuesAtImport(state) {
			state.configurationValues = {
				...cloneDeep(state.configurationValuesForConnectValidation),
				isConnectionDetailsOpen: (state.configurationValues as ConnectToYourDatabaseProps)
					.isConnectionDetailsOpen,
				schemas: (state.configurationValues as ConnectToYourDatabaseProps)
					.schemas,
			}
			state.state = SettingsViewState.SAVING
		},
		cancelAtImport(state) {
			state.configurationValues = {
				...state.configurationValues,
				isConnectionDetailsOpen: true,
			}
		},
		setConfigurationValues(
			state,
			action: Action<Partial<SettingConfigurationValues>>
		) {
			state.configurationValues = {
				...state.configurationValues,
				...action.payload,
			}
		},
		setErrors(state, action: Action<{ [key: string]: string }>) {
			state.errors = action.payload
		},
		setConnectionStatus(
			state,
			action: Action<ConnectToYourDatabaseConnectionStatus>
		) {
			state.connectionStatus = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(asyncActions.connectDatabase.fulfilled, (state, action) => {
			if (action.payload && isArray(action.payload.schemas)) {
				state.connectionSchemas = action.payload.schemas.map(
					(s: any) =>
						({
							label: s.name,
							value: s.name,
							disabled: false,
						} as ConnectionSchemaProps)
				)
				state.connectionStatus = ConnectToYourDatabaseConnectionStatus.CONNECTED
				state.configurationValues = {
					...state.configurationValues,
					isConnectionDetailsOpen: false,
					schemas: [],
				}
				state.configurationValuesForConnectValidation = cloneDeep(
					state.configurationValues as ConnectToYourDatabaseProps
				)
			} else {
				state.connectionStatusError = action.payload.errorMessage
				state.connectionStatus = ConnectToYourDatabaseConnectionStatus.ERROR
			}
		})
		builder.addCase(asyncActions.loadDBDetails.fulfilled, (state, action) => {
			if (action.payload) {
				const list = [
					'password',
					'userName',
					'port',
					'host',
					'platform',
					'name',
					'environmentType',
					'type',
					'provider',
				]
				const configurations = action.payload.configurations
				if (!isNil(configurations.Development)) {
					state.configurationValues = pick(configurations.Development, list)
				} else {
					state.configurationValues = Object.assign({}, defaultEditConfig, {
						name: action.payload.name,
					})
				}
			}
			state.isConnectDatabase = false
			state.state = SettingsViewState.EDIT
			state.prevConfigurationValues = cloneDeep(state.configurationValues)
		})

		builder.addCase(
			asyncActions.validateDbNameDuplicate.fulfilled,
			(state, action) => {
				if (isEmpty(action.payload)) {
					let errors = { ...state.errors }
					if (errors.name) {
						delete errors.name
					}
					state.errors = errors
				} else {
					state.errors = Object.assign({}, state.errors, {
						name: i18n.t(DATABASE_DUPLICATE_NAME),
					})
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
