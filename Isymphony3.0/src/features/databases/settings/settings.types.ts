import { DatabaseVendorOptionsTypes } from '../../../utilities/apiEnumConstants'

export enum SettingsViewState {
	NONE,
	LANDING,
	EDIT,
	SAVING,
	FAILURE,
	LOADING,
}

export enum ConnectToYourDatabaseConnectionStatus {
	IN_PROGRESS,
	ERROR,
	CONNECTED,
	INITIAL,
}

export interface CreateDatabaseState {
	name: string
}

export interface EditDatabaseSettingState {
	password: string
	userName: string
	port: number | string
	type: string
	host: string
	platform: DatabaseVendorOptionsTypes
	name: string
	environmentType: string
	provider: string
}

export interface ConnectToYourDatabaseProps extends EditDatabaseSettingState {
	schemas: string[]
	isConnectionDetailsOpen: boolean
}

export interface ConnectionSchemaProps {
	label: string
	value: string
	disabled: boolean
}

export type SettingConfigurationValues =
	| {}
	| CreateDatabaseState
	| EditDatabaseSettingState
	| ConnectToYourDatabaseProps

export interface SettingsState {
	state: SettingsViewState
	databaseId: string | null
	isConnectDatabase: boolean | null
	stateError: string | null
	configurationValues: SettingConfigurationValues
	configurationValuesForConnectValidation: null | ConnectToYourDatabaseProps
	prevConfigurationValues: SettingConfigurationValues
	errors: { [key: string]: string }
	connectionStatus: ConnectToYourDatabaseConnectionStatus
	connectionStatusError: string | null
	connectionSchemas: ConnectionSchemaProps[]
}
