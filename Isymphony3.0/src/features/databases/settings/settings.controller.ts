import { createAsyncThunk } from '@reduxjs/toolkit'
import { RootState } from '../../../base.types'
import {
	isConnectValidAtImport,
	selectConfigurationValues,
	selectErrors,
	selectIsConnectDatabase,
	selectSettingsDatabaseId,
	selectSettingsIsModified,
} from './settings.selectors'
import { validateConfigurationData } from './settings.utilities'
import { actions } from './settings.slice'
import {
	ConnectToYourDatabaseConnectionStatus,
	ConnectToYourDatabaseProps,
	CreateDatabaseState,
	EditDatabaseSettingState,
	SettingsViewState,
} from './settings.types'
import {
	CREATE_DATABASE,
	IMPORT_DATABASE,
	UPDATE_DB_CONFIG,
} from '../../../utilities/apiEndpoints'
import doAsync from '../../../infrastructure/doAsync'
import i18n from 'i18next'
import {
	BUTTON_DISCARD,
	BUTTON_NO,
	BUTTON_SAVE,
	BUTTON_YES,
	DATABASE_CONFIG_SUCCESS,
	DATABASE_CONNECT_DATA_MISMATCH_AT_IMPORT,
	DATABASE_CONNECT_SCHEMA_IMPORTED_SUCCESS,
	DATABASE_CREATE_MESSAGE_SUCCESS,
	DATABASE_SETTINGS_CLOSE_MESSAGE,
} from '../../../utilities/constants'
import { DatabasesDatabaseProps } from '../databases.types'
import { Action } from '../../../common.types'
import { DialogTypes, showDialog } from '../../dialogs'
import { isNil, omit } from 'lodash'
import { connectDatabase, loadDBDetails } from './settings.asyncActions'

export const initSettings = createAsyncThunk(
	'databases/settings/initSettings',
	async (appId: string, thunkArgs) => {
		const databaseId = selectSettingsDatabaseId(
			thunkArgs.getState() as RootState
		)
		if (isNil(databaseId)) {
			return
		}
		thunkArgs.dispatch(actions.setViewCurrentState(SettingsViewState.LOADING))
		thunkArgs.dispatch(loadDBDetails({ appId, databaseId }))
	}
)

export const connectToDB = createAsyncThunk(
	'databases/settings/connectToDB',
	async (appId: string, { dispatch, getState }) => {
		const configData = selectConfigurationValues(getState() as RootState)
		const databaseId = selectSettingsDatabaseId(getState() as RootState)
		const prevErrors = selectErrors(getState() as RootState)
		const errors = validateConfigurationData(
			configData,
			true,
			databaseId,
			prevErrors
		)
		if (Object.keys(errors).length > 0) {
			// has errors
			dispatch(actions.setErrors(errors))
			return null
		} else {
			dispatch(
				actions.setConnectionStatus(
					ConnectToYourDatabaseConnectionStatus.IN_PROGRESS
				)
			)
			dispatch(actions.setErrors({}))
			return dispatch(
				connectDatabase({
					appId,
					configData: configData as ConnectToYourDatabaseProps,
				})
			)
		}
	}
)

export const saveSettings = createAsyncThunk(
	'databases/settings/saveSettings',
	async (appId: string, { dispatch, getState }) => {
		const configData = selectConfigurationValues(getState() as RootState)
		const isConnectDatabase = selectIsConnectDatabase(getState() as RootState)
		const databaseId = selectSettingsDatabaseId(getState() as RootState)
		const prevErrors = selectErrors(getState() as RootState)
		const errors = validateConfigurationData(
			configData,
			isConnectDatabase,
			databaseId,
			prevErrors
		)
		if (Object.keys(errors).length > 0) {
			// has errors
			dispatch(actions.setErrors(errors))
			return null
		} else {
			dispatch(actions.setErrors({}))
			if (isNil(databaseId)) {
				// new
				if (isConnectDatabase) {
					// connect
					const canContinue = isConnectValidAtImport(getState() as RootState)
					if (canContinue) {
						// can import
						dispatch(actions.setViewCurrentState(SettingsViewState.SAVING))
						return dispatch(
							importDatabase({
								appId,
								configData: configData as ConnectToYourDatabaseProps,
							}) as any
						).then((data: Action<DatabasesDatabaseProps>) => data.payload)
					} else {
						// error
						return new Promise((resolve) => {
							const okayFunction = () => {
								dispatch(actions.revertConfigValuesAtImport(null))
								dispatch(
									importDatabase({
										appId,
										configData: selectConfigurationValues(
											getState() as RootState
										) as ConnectToYourDatabaseProps,
									}) as any
								).then((data: Action<DatabasesDatabaseProps>) =>
									resolve(data.payload as any)
								)
							}
							const cancelFunction = () => {
								dispatch(actions.cancelAtImport(null))
								resolve(undefined as any)
							}
							dispatch(
								showDialog({
									type: DialogTypes.CONFIRMATION_DIALOG,
									onOkay: okayFunction,
									onCancel: cancelFunction,
									data: {
										message: i18n.t(DATABASE_CONNECT_DATA_MISMATCH_AT_IMPORT),
										okayButtonLabel: i18n.t(BUTTON_YES),
										cancelButtonLabel: i18n.t(BUTTON_NO),
									},
								})
							)
						})
					}
				} else {
					// create
					dispatch(actions.setViewCurrentState(SettingsViewState.SAVING))
					return dispatch(
						createDatabase({
							appId,
							name: (configData as CreateDatabaseState).name,
						}) as any
					).then((data: Action<DatabasesDatabaseProps>) => data.payload)
				}
			} else {
				// edit
				if (isConnectDatabase) {
					// connect
				} else {
					// create
					return dispatch(
						updateDatabase({
							appId,
							databaseId,
							configData: configData as EditDatabaseSettingState,
						}) as any
					).then((data: Action<DatabasesDatabaseProps>) => data.payload)
				}
			}
		}
	}
)

export const importDatabase = createAsyncThunk(
	'databases/settings/importDatabase',
	async (
		data: {
			appId: string
			configData: ConnectToYourDatabaseProps
		},
		thunkArgs
	) => {
		return await doAsync({
			url: IMPORT_DATABASE(data.appId),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					configurations: {
						[data.configData.environmentType]: omit(
							data.configData,
							'isConnectionDetailsOpen',
							'schemas'
						),
					},
					schemas: data.configData.schemas,
				}),
			},
			successMessage: i18n.t(DATABASE_CONNECT_SCHEMA_IMPORTED_SUCCESS),
			noBusySpinner: true,
			propagateAPIError: true,
			...thunkArgs,
		}).then((data: any) => {
			if (!isNil(data.id)) {
				const returnData: DatabasesDatabaseProps = {
					id: data.id,
					name: data.name,
					schemas: data.schemaInfo.map((s: any) => ({
						id: s.id,
						name: s.name,
					})),
				}
				return returnData
			} else {
				thunkArgs.dispatch(actions.setStateError(data.errorMessage))
				return
			}
		})
	}
)

export const updateDatabase = createAsyncThunk(
	'databases/settings/updateDatabase',
	async (
		data: {
			appId: string
			databaseId: string
			configData: EditDatabaseSettingState
		},
		thunkArgs
	) => {
		return await doAsync({
			url: UPDATE_DB_CONFIG(data.appId, data.databaseId),
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					configurations: {
						[data.configData.environmentType]: data.configData,
					},
				}),
			},
			successMessage: i18n.t(DATABASE_CONFIG_SUCCESS),
			noBusySpinner: true,
			...thunkArgs,
		}).then((data: any) => {
			const returnData: DatabasesDatabaseProps = {
				id: data.id,
				name: data.name,
				schemas: data.schemas.map((s: any) => ({ id: s.id, name: s.name })),
			}
			return returnData
		})
	}
)

export const createDatabase = createAsyncThunk(
	'databases/settings/createDatabase',
	async (
		data: {
			appId: string
			name: string
		},
		thunkArgs
	) => {
		return await doAsync({
			url: CREATE_DATABASE(data.appId),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name: data.name,
				}),
			},
			successMessage: i18n.t(DATABASE_CREATE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			noBusySpinner: true,
			...thunkArgs,
		}).then((data: any) => {
			const returnData: DatabasesDatabaseProps = {
				id: data.id,
				name: data.name,
				schemas: data.schemas.map((s: any) => ({ id: s.id, name: s.name })),
			}
			return returnData
		})
	}
)

export const confirmClose = createAsyncThunk(
	'databases/settings/createDatabase',
	async (
		data: { appId: string; callback: (data: DatabasesDatabaseProps) => void },
		{ dispatch, getState }
	) => {
		const isModified = selectSettingsIsModified(getState() as RootState)
		const cancelFunction = () => {
			dispatch(actions.clearData(null))
		}
		const okayFunction = () => {
			dispatch(saveSettings(data.appId) as any).then(
				(result: Action<DatabasesDatabaseProps>) => {
					data.callback(result.payload)
					dispatch(actions.clearData(null))
				}
			)
		}
		if (isModified) {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: okayFunction,
					onCancel: cancelFunction,
					data: {
						message: i18n.t(DATABASE_SETTINGS_CLOSE_MESSAGE),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		} else {
			cancelFunction()
		}
	}
)
