import { selectSlice as baseSelector } from '../base.selector'
import { RootState } from '../../../base.types'
import {
	ConnectionSchemaProps,
	ConnectToYourDatabaseConnectionStatus,
	ConnectToYourDatabaseProps,
	SettingConfigurationValues,
	SettingsState,
	SettingsViewState,
} from './settings.types'
import slice from './settings.slice'
import { isEqual, omit } from 'lodash'

export const selectSlice = (state: RootState): SettingsState => {
	return baseSelector(state)[slice.name]
}

export const selectCurrentState = (state: RootState): SettingsViewState => {
	return selectSlice(state).state
}

export const selectSettingsDatabaseId = (state: RootState): string | null => {
	return selectSlice(state).databaseId
}

export const selectIsConnectDatabase = (state: RootState): boolean | null => {
	return selectSlice(state).isConnectDatabase
}

export const selectConfigurationValues = (
	state: RootState
): SettingConfigurationValues => {
	return selectSlice(state).configurationValues
}

export const selectErrors = (state: RootState) => {
	return selectSlice(state).errors
}

export const selectPrevConfigurationValues = (
	state: RootState
): SettingConfigurationValues => {
	return selectSlice(state).prevConfigurationValues
}

export const selectSettingsIsModified = (state: RootState): boolean => {
	return !isEqual(
		selectPrevConfigurationValues(state),
		selectConfigurationValues(state)
	)
}

export const selectConnectionStatus = (
	state: RootState
): ConnectToYourDatabaseConnectionStatus => {
	return selectSlice(state).connectionStatus
}

export const selectConnectionStatusError = (
	state: RootState
): string | null => {
	return selectSlice(state).connectionStatusError
}

export const selectConnectionSchemas = (
	state: RootState
): ConnectionSchemaProps[] => {
	return selectSlice(state).connectionSchemas
}

export const selectConfigurationValuesForConnectValidation = (
	state: RootState
): ConnectToYourDatabaseProps | null => {
	return selectSlice(state).configurationValuesForConnectValidation
}

export const isConnectValidAtImport = (state: RootState): boolean => {
	return isEqual(
		omit(
			selectConfigurationValuesForConnectValidation(state),
			'isConnectionDetailsOpen',
			'schemas'
		),
		omit(selectConfigurationValues(state), 'isConnectionDetailsOpen', 'schemas')
	)
}
