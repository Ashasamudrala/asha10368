import React, { useEffect } from 'react'
import './settings.scss'
import Drawer from '@material-ui/core/Drawer'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import CancelIcon from '@material-ui/icons/Cancel'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline'
import { useDispatch, useSelector } from 'react-redux'
import {
	selectConfigurationValues,
	selectConnectionSchemas,
	selectConnectionStatus,
	selectConnectionStatusError,
	selectCurrentState,
	selectErrors,
	selectIsConnectDatabase,
	selectSettingsDatabaseId,
} from './settings.selectors'
import {
	ConnectToYourDatabaseConnectionStatus,
	ConnectToYourDatabaseProps,
	SettingConfigurationValues,
	SettingsViewState,
} from './settings.types'
import { CircularProgress, makeStyles, Typography } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import {
	BUTTON_CANCEL,
	BUTTON_CREATE,
	BUTTON_NO,
	BUTTON_SAVE,
	BUTTON_YES,
	CONNECT_EXISTING_DATABASE,
	CONNECT_HEADER_TITLE,
	CONNECT_PROGRESS_LABEL_TEXT,
	CONNECT_TO_DB,
	CREATE_DATABASE,
	CREATE_NEW_DATABASE,
	DATABASE,
	DATABASE_TRANSLATIONS,
	SETTINGS,
	DATABASE_CREATING_NEW_DB_TEXT,
	COMMON_BUTTON_IMPORT,
	CONNECT_LOADING_TEXT,
	CONNECT_SUCCESS_TEXT,
	CONNECT_FAILURE_TEXT,
	CONNECT_PROGRESS_ERROR_TEXT,
} from '../../../utilities/constants'
import { debounce, isNil } from 'lodash'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import LinkIcon from '@material-ui/icons/Link'
import { actions } from './settings.slice'
import { getCreateDatabaseFormBuilderConfig } from './settings.utilities'
import { IsyFormBuilder } from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import {
	confirmClose,
	connectToDB,
	initSettings,
	saveSettings,
} from './settings.controller'
import { DatabasesDatabaseProps } from '../databases.types'
import { Action } from '../../../common.types'
import { validateDbNameDuplicate } from './settings.asyncActions'

const useStyles = makeStyles({
	paper: {
		width: 470,
		height: '100%',
		boxSizing: 'border-box',
		backgroundColor: 'white',
		border: '1px solid #dedede',
	},
})

export interface SettingsProps {
	appId: string
	onUpdate: (data: DatabasesDatabaseProps) => void
}

export default function Settings(props: SettingsProps) {
	const currentState = useSelector(selectCurrentState)
	const isConnectDatabase = useSelector(selectIsConnectDatabase)
	const databaseId = useSelector(selectSettingsDatabaseId)
	const configData = useSelector(selectConfigurationValues)
	const connectToYourDatabaseConnectionStatus = useSelector(
		selectConnectionStatus
	)
	const connectToYourDatabaseConnectionStatusError = useSelector(
		selectConnectionStatusError
	)
	const connectionSchemas = useSelector(selectConnectionSchemas)
	const errors = useSelector(selectErrors)

	const classes = useStyles()
	const dispatch = useDispatch()
	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	useEffect(() => {
		dispatch(initSettings(props.appId))
	}, [databaseId]) // eslint-disable-line react-hooks/exhaustive-deps

	const getTitle = () => {
		if (!isNil(databaseId)) {
			return t(SETTINGS)
		}
		if (currentState === SettingsViewState.LANDING) {
			return t(DATABASE)
		}
		if (isConnectDatabase) {
			return t(CONNECT_TO_DB)
		}
		return t(CREATE_DATABASE)
	}

	const getCancelButtonLabel = () => {
		if (currentState === SettingsViewState.FAILURE) {
			return t(BUTTON_NO)
		}
		return t(BUTTON_CANCEL)
	}

	const getOkayButtonLabel = () => {
		if (currentState === SettingsViewState.FAILURE) {
			return t(BUTTON_YES)
		}
		if (!isNil(databaseId)) {
			return t(BUTTON_SAVE)
		}
		if (isConnectDatabase) {
			return t(COMMON_BUTTON_IMPORT)
		}
		return t(BUTTON_CREATE)
	}

	const getFormBuilderConfig = () => {
		return getCreateDatabaseFormBuilderConfig(
			databaseId,
			isConnectDatabase,
			(configData as ConnectToYourDatabaseProps).isConnectionDetailsOpen,
			connectToYourDatabaseConnectionStatus,
			handleConnect,
			handleNameChange
		)
	}

	const handleClose = () => {
		dispatch(confirmClose({ appId: props.appId, callback: props.onUpdate }))
	}

	const handleConnect = () => {
		// to handle connect
		dispatch(connectToDB(props.appId))
	}

	const handleAppNameDebounce = debounce((name: string) => {
		dispatch(validateDbNameDuplicate({ appId: props.appId, name }))
	}, 300)

	const handleNameChange = (name: string) => {
		dispatch(
			actions.setConfigurationValues({
				name,
			})
		)
		handleAppNameDebounce(name)
	}

	const handleCancel = () => {
		if (currentState === SettingsViewState.FAILURE) {
			dispatch(actions.setViewCurrentState(SettingsViewState.EDIT))
		} else {
			dispatch(confirmClose({ appId: props.appId, callback: props.onUpdate }))
		}
	}

	const handleSave = () => {
		dispatch(saveSettings(props.appId) as any).then(
			(data: Action<DatabasesDatabaseProps | null>) => {
				if (!isNil(data.payload)) {
					props.onUpdate(data.payload)
					dispatch(actions.clearData(null))
				}
			}
		)
	}

	const handleOnConfigChange = (data: SettingConfigurationValues) => {
		dispatch(actions.setConfigurationValues(data))
	}

	const handleDatabaseTypeSelection = (isConnectDatabase: boolean) => {
		dispatch(actions.setIsConnectDatabase(isConnectDatabase))
	}

	const renderComponent = (componentType: string): React.ReactNode => {
		if (componentType === 'STATUS') {
			switch (connectToYourDatabaseConnectionStatus) {
				case ConnectToYourDatabaseConnectionStatus.IN_PROGRESS:
					return (
						<div className='connect-status loading'>
							<CircularProgress size={20} />
							<div className='loading-text'>{t(CONNECT_LOADING_TEXT)}</div>
						</div>
					)
				case ConnectToYourDatabaseConnectionStatus.ERROR:
					return (
						<div className='connect-status failure'>
							<CancelIcon />
							{isNil(connectToYourDatabaseConnectionStatusError)
								? t(CONNECT_FAILURE_TEXT)
								: connectToYourDatabaseConnectionStatusError}
						</div>
					)
				case ConnectToYourDatabaseConnectionStatus.CONNECTED:
					return (
						<div className='connect-status success'>
							<CheckCircleIcon />
							{t(CONNECT_SUCCESS_TEXT)}
						</div>
					)
				default:
					return null
			}
		}
		return null
	}

	const renderLandingPageDetails = () => {
		return (
			<div className='flex-class'>
				<Typography className='database-heading'>
					{t(CONNECT_HEADER_TITLE)}
				</Typography>
				<IsyButton
					className='secondary-btn'
					onClick={() => handleDatabaseTypeSelection(false)}
				>
					<img
						className='create-icon'
						alt={t(DATABASE)}
						src='/images/databaseIcons/Database.svg'
					/>
					{t(CREATE_NEW_DATABASE)}
				</IsyButton>
				<IsyButton
					className='secondary-btn'
					onClick={() => handleDatabaseTypeSelection(true)}
				>
					<LinkIcon />
					{t(CONNECT_EXISTING_DATABASE)}
				</IsyButton>
			</div>
		)
	}

	const renderFormData = () => {
		return (
			<IsyFormBuilder<SettingConfigurationValues>
				forms={getFormBuilderConfig()}
				data={configData}
				errors={errors}
				onChange={handleOnConfigChange}
				componentRender={renderComponent}
				optionsObject={{ schemasList: connectionSchemas }}
			/>
		)
	}

	const renderConnectDatabaseSavingPage = () => {
		return (
			<div className='connect-database-progress'>
				<CircularProgress size={70} />
				<div className='progress-label'>
					{isConnectDatabase
						? t(CONNECT_PROGRESS_LABEL_TEXT)
						: t(DATABASE_CREATING_NEW_DB_TEXT)}
				</div>
			</div>
		)
	}

	const renderConnectDatabaseLoadingPage = () => {
		return (
			<div className='connect-database-progress'>
				<CircularProgress size={70} />
			</div>
		)
	}

	const renderConnectDatabaseErrorPage = () => {
		return (
			<div className='error-page'>
				<span className='error' role='button'>
					<ErrorOutlineIcon color='secondary' />
				</span>
				<div className='error-text'>{t(CONNECT_PROGRESS_ERROR_TEXT)}</div>
			</div>
		)
	}

	const renderPage = () => {
		switch (currentState) {
			case SettingsViewState.LOADING:
				return renderConnectDatabaseLoadingPage()
			case SettingsViewState.LANDING:
				return renderLandingPageDetails()
			case SettingsViewState.EDIT:
				return renderFormData()
			case SettingsViewState.SAVING:
				return renderConnectDatabaseSavingPage()
			case SettingsViewState.FAILURE:
				return renderConnectDatabaseErrorPage()
			default:
				return ''
		}
	}

	const renderHeader = () => {
		return (
			<div className='header'>
				<span className='header-title'>{getTitle()}</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
				/>
			</div>
		)
	}

	const renderFooter = () => {
		return (
			<div className='footer'>
				<IsyButton onClick={handleCancel} className='standard-btn'>
					{getCancelButtonLabel()}
				</IsyButton>
				<IsyButton
					className='primary-btn'
					onClick={handleSave}
					disabled={false}
				>
					{getOkayButtonLabel()}
				</IsyButton>
			</div>
		)
	}

	const render = () => {
		const hasFooter =
			(currentState === SettingsViewState.EDIT &&
				(!isConnectDatabase ||
					connectToYourDatabaseConnectionStatus ===
						ConnectToYourDatabaseConnectionStatus.CONNECTED)) ||
			currentState === SettingsViewState.FAILURE
		return (
			<Drawer
				className='default-db-form'
				open={currentState !== SettingsViewState.NONE}
				anchor='right'
				elevation={0}
				classes={{ paper: classes.paper }}
				disablePortal
			>
				{renderHeader()}
				<div className={(hasFooter ? '' : 'with-out-footer ') + 'middle'}>
					{renderPage()}
				</div>
				{hasFooter && renderFooter()}
			</Drawer>
		)
	}
	return render()
}
