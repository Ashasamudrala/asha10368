import { StateWithHistory } from 'redux-undo'
import { CanvasState } from './canvas/canvas.types'
import { DatabasesState } from './databases.types'
import { SchemaState } from './schema/schema.types'
import { SettingsState } from './settings/settings.types'

export enum DatabasesSubReducersNames {
	DATABASES = 'databases',
	CANVAS = 'canvas',
	SCHEMA = 'Schema',
	SETTINGS = 'Settings',
}

export interface DatabasesBaseState {
	[DatabasesSubReducersNames.DATABASES]: DatabasesState
	[DatabasesSubReducersNames.SCHEMA]: StateWithHistory<SchemaState>
	[DatabasesSubReducersNames.CANVAS]: CanvasState
	[DatabasesSubReducersNames.SETTINGS]: SettingsState
}
