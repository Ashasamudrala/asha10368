import React from 'react'
import {
	IsyToolbar,
	IsyToolbarItemProps,
	IsyToolbarItemTypes,
} from '../../../../widgets/IsyToolbar/IsyToolbar'
import './dbToolbar.scss'
import {
	ADD_NEW_TABLE,
	BUTTON_SAVE,
	DATABASE_TRANSLATIONS,
	SEARCH_TABLE,
	COMMON_REQUEST_LOCK,
	COMMON_RELEASE_LOCK,
	COMMON_LOCKED_BY,
	DATABASE_RELEASE_LOCK_INFO,
	DATABASE_REQUEST_LOCK_INFO,
	COMMON_UNDO,
	COMMON_REDO,
} from '../../../../utilities/constants'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined'
import RedoIcon from '@material-ui/icons/Redo'
import UndoIcon from '@material-ui/icons/Undo'
import { useTranslation } from 'react-i18next'
import { IsySearchSuggestionProps } from '../../../../widgets/IsySearch/IsySearch'
import { IsyButton } from '../../../../widgets/IsyButton/IsyButton'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined'
import CachedOutlinedIcon from '@material-ui/icons/CachedOutlined'
import { DatabaseTableProps } from '../../schema/schema.types'
import { DatabaseLockStatus } from '../../canvas/canvas.types'

export interface DBToolbarProps {
	tables: DatabaseTableProps[]
	searchTerm: string
	isSchemaModified: boolean
	lockStatus: DatabaseLockStatus
	lockedBy: string | null
	canUndo: boolean
	canRedo: boolean
	onSave: () => void
	onSearchTermChange: (val: string) => void
	onTableSelect: (tableIndex: number) => void
	onAddNewTable: () => void
	onAcquireLock: () => void
	onReleaseLock: () => void
	onRefreshLock: () => void
	onUndo: () => void
	onRedo: () => void
}

export function DBToolbar(props: DBToolbarProps) {
	const searchSuggestions = props.tables.map((table, index) => ({
		value: index,
		name: table.name,
	}))

	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	const getToolbarConfig = (): IsyToolbarItemProps[] => [
		{
			type: IsyToolbarItemTypes.SEARCH,
			value: props.searchTerm,
			onCancel: handleSearchChange,
			onChange: handleSearchChange,
			suggestions: getFilterSuggestions(),
			onSuggestionSelect: handleTableSelect,
			placeholder: t(SEARCH_TABLE),
		},
		{
			type: IsyToolbarItemTypes.FLEX_GROW,
		},
		{
			type: IsyToolbarItemTypes.BUTTON,
			name: t(ADD_NEW_TABLE),
			onClick: props.onAddNewTable,
			disabled: props.lockStatus !== DatabaseLockStatus.LOCK_ACQUIRED,
			icon: <AddOutlinedIcon />,
			className: 'isy-btn-new-table',
		},
		{
			type: IsyToolbarItemTypes.SEPARATOR,
		},
		{
			type: IsyToolbarItemTypes.ICON,
			icon: <UndoIcon />,
			tooltip: t(COMMON_UNDO),
			onClick: props.onUndo,
			disabled: !props.canUndo,
		},
		{
			type: IsyToolbarItemTypes.ICON,
			icon: <RedoIcon />,
			tooltip: t(COMMON_REDO),
			onClick: props.onRedo,
			disabled: !props.canRedo,
		},
		{
			type: IsyToolbarItemTypes.ICON,
			icon: <SaveOutlinedIcon />,
			tooltip: t(BUTTON_SAVE),
			onClick: props.onSave,
			disabled:
				!props.isSchemaModified ||
				props.lockStatus !== DatabaseLockStatus.LOCK_ACQUIRED,
			className: 'isy-steps-save-icon',
		},
		{
			type: IsyToolbarItemTypes.SEPARATOR,
		},
		{
			type: IsyToolbarItemTypes.COMPONENT,
			render: renderEditLock,
		},
	]

	const getFilterSuggestions = () => {
		const searchString = props.searchTerm.toLowerCase()
		return searchSuggestions.filter(
			(option) => option.name.toLowerCase().indexOf(searchString) > -1
		)
	}

	const handleSearchChange = (val: string) => {
		props.onSearchTermChange(val)
	}

	const handleTableSelect = (item: IsySearchSuggestionProps) => {
		props.onSearchTermChange(item.name)
		props.onTableSelect(item.value as number)
	}

	const handleReloadLockStatus = () => {
		props.onRefreshLock()
	}

	const renderEditLock = () => {
		switch (props.lockStatus) {
			case DatabaseLockStatus.LOCK_PRESENT: {
				return (
					<span className='edit-lock'>
						<IsyButton className='request-button' onClick={props.onAcquireLock}>
							<LockOutlinedIcon className='icon' />
							{t(COMMON_REQUEST_LOCK)}
						</IsyButton>
						<span className='lock-tooltip'>
							{t(DATABASE_REQUEST_LOCK_INFO)}
						</span>
					</span>
				)
			}
			case DatabaseLockStatus.LOCK_ACQUIRED: {
				return (
					<span className='edit-lock'>
						<IsyButton className='release-button' onClick={props.onReleaseLock}>
							<LockOpenOutlinedIcon className='icon' />
							{t(COMMON_RELEASE_LOCK)}
						</IsyButton>
						<span className='lock-tooltip'>
							{t(DATABASE_RELEASE_LOCK_INFO)}
						</span>
					</span>
				)
			}
			case DatabaseLockStatus.LOCK_NOT_PRESENT: {
				return (
					<div className='locked-by'>
						<span className='info'>
							{t(COMMON_LOCKED_BY, { name: props.lockedBy })}
						</span>
						<CachedOutlinedIcon
							className='icon'
							onClick={handleReloadLockStatus}
						/>
					</div>
				)
			}
			default:
				return null
		}
	}

	return (
		<div className='db-tool-bar'>
			<IsyToolbar items={getToolbarConfig()} />
		</div>
	)
}
