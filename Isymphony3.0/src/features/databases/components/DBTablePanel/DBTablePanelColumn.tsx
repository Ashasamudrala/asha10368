import React, { useState, useEffect } from 'react'
import './DBTablePanel.scss'
import { isNil, find } from 'lodash'
import { IsyInput } from '../../../../widgets/IsyInput/IsyInput'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import { DBColumnPopOver } from './DBColumnPopOver/DbColumnPopOver'
import {
	DatabaseTableAttributeProps,
	DatabaseTableConstraintsProps,
} from '../../schema/schema.types'
import { AttributeTypeProps } from '../../databases.types'
import { TableAttributeKeyTypes } from '../../schema/schema.types'

export interface DBTablePanelColumnProps {
	data: DatabaseTableAttributeProps
	attributeTypes: AttributeTypeProps[]
	onUpdateTableAttribute: (data: Partial<DatabaseTableAttributeProps>) => void
	onChangeOfTableAttributeConstraint: (
		data: Partial<DatabaseTableConstraintsProps>
	) => void
	onDeleteTableAttribute: () => void
	onAttributeKeyChange: (key: TableAttributeKeyTypes) => void
	isEditable: boolean
}

export function DBTablePanelColumn(props: DBTablePanelColumnProps) {
	const [anchorEl, setAnchorEl] = useState<Element | null>(null)

	useEffect(() => {
		setAnchorEl(null)
	}, [props.data.name])

	const getFilteredOptions = () => {
		if (props.data.constraints.primary) {
			return props.attributeTypes.filter((attr) => {
				return !isNil(find(attr.constraints, (c) => c.name === 'primary'))
			})
		}
		return props.attributeTypes
	}

	const handleClosePopOver = () => {
		setAnchorEl(null)
	}

	const handleClickOptions = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>
	) => {
		event.stopPropagation()
		setAnchorEl(event.currentTarget)
	}

	const handleNameChange = (value: string) => {
		props.onUpdateTableAttribute({ name: value })
	}

	const handleTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
		props.onUpdateTableAttribute({ type: e.target.value })
	}

	const getAttributeTypeConstraints = () => {
		const attributeType = find(
			props.attributeTypes,
			(a) => a.value === props.data.type
		)
		if (!isNil(attributeType)) {
			return attributeType.constraints
		}
		return []
	}

	const renderOptions = () => {
		return (
			<div className='moreContainer'>
				<MoreHorizIcon onClick={handleClickOptions} className='more' />
				{!isNil(anchorEl) && (
					<DBColumnPopOver
						isLimitedView={false}
						anchorEl={anchorEl}
						constraints={props.data.constraints}
						reference={props.data.reference}
						constraintsConfig={getAttributeTypeConstraints()}
						onClose={handleClosePopOver}
						onChangeOfConstraint={props.onChangeOfTableAttributeConstraint}
						onDelete={props.onDeleteTableAttribute}
						onKeyChange={props.onAttributeKeyChange}
						isViewMode={!props.isEditable}
					/>
				)}
			</div>
		)
	}

	const renderType = () => {
		return (
			<div className='dataTypeContainer'>
				<select
					value={props.data.type}
					className='dataType'
					onChange={handleTypeChange}
					disabled={!isNil(props.data.reference) || !props.isEditable}
				>
					<option disabled selected hidden value=''>
						Type
					</option>
					{getFilteredOptions().map((item) => (
						<option key={item.value} value={item.value}>
							{item.value}
						</option>
					))}
				</select>
			</div>
		)
	}

	const renderName = () => {
		return (
			<div className='dataNameContainer'>
				<IsyInput<string>
					type='text'
					onBlur={handleNameChange}
					value={props.data.name}
					disabled={!props.isEditable}
				/>
			</div>
		)
	}

	return (
		<div className='columnData' key={props.data as any}>
			{renderName()}
			{renderType()}
			{renderOptions()}
		</div>
	)
}
