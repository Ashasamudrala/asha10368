import React from 'react'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import { withStyles } from '@material-ui/core/styles'
import Popover from '@material-ui/core/Popover'
import { isFunction } from 'lodash'
import {
	TableRow,
	TableBody,
	TableCell,
	Table,
	Switch,
} from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import {
	KEY_TYPE,
	DATABASE_TRANSLATIONS,
	NONE,
	PRIMARY_KEY,
	FOREIGN_KEY,
	UNIQUE_KEY,
} from '../../../../../utilities/constants'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import { IsyButton } from '../../../../../widgets/IsyButton/IsyButton'
import './dbColumnPopOver.scss'
import { IsyInput } from '../../../../../widgets/IsyInput/IsyInput'
import { isNil } from 'lodash'
import {
	IsyDropDown,
	IsyDropDownOptionProps,
} from '../../../../../widgets/IsyDropDown/IsyDropDown'
import { DatabaseAttributeConstraintDataTypes } from '../../../../../utilities/apiEnumConstants'
import {
	DatabaseTableConstraintsProps,
	DatabaseTableReferenceProps,
	TableAttributeKeyTypes,
} from '../../../schema/schema.types'
import { AttributeTypesConstraintProps } from '../../../databases.types'

const RequiredSwitch = withStyles({
	switchBase: {
		color: '#e34d28',
		float: 'right',
		'&$checked': {
			color: '#e34d28',
		},
		'&$checked + $track': {
			backgroundColor: '#e34d28',
		},
	},
	checked: {},
	track: {},
})(Switch)

export interface DBColumnPopOverProps {
	constraints: DatabaseTableConstraintsProps
	isLimitedView: boolean
	isViewMode?: boolean
	constraintsConfig: AttributeTypesConstraintProps[]
	anchorEl: Element
	reference: DatabaseTableReferenceProps | null
	onClose: () => void
	onChangeOfConstraint: (data: Partial<DatabaseTableConstraintsProps>) => void
	onDelete?: () => void
	onKeyChange: (key: TableAttributeKeyTypes) => void
}

export function DBColumnPopOver(props: DBColumnPopOverProps) {
	const { constraints, constraintsConfig, anchorEl, reference } = props
	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	const getSelectedKeyType = () => {
		if (!isNil(reference)) {
			return TableAttributeKeyTypes.FOREIGN_KEY
		}
		if (constraints.primary) {
			return TableAttributeKeyTypes.PRIMARY_KEY
		}
		if (constraints.unique) {
			return TableAttributeKeyTypes.UNIQUE_KEY
		}
		return TableAttributeKeyTypes.NONE
	}

	const getConstraintValue = (constraint: AttributeTypesConstraintProps) => {
		return (constraints as any)[constraint.name]
	}

	const getPopoverActions = (hasPrimary: boolean, hasUnique: boolean) => {
		const options: IsyDropDownOptionProps<TableAttributeKeyTypes>[] = []
		if (hasPrimary && (!props.isLimitedView || constraints.primary)) {
			options.push({
				id: TableAttributeKeyTypes.PRIMARY_KEY,
				name: t(PRIMARY_KEY),
				icon: (
					<VpnKeyIcon className='key-icon-popover-attribute primary_key_svg' />
				),
			})
		}
		if (!constraints.primary) {
			options.unshift({
				id: TableAttributeKeyTypes.NONE,
				icon: (
					<VpnKeyIcon className='key-icon-popover-attribute none_key_svg' />
				),
				name: t(NONE),
			})
			if (hasPrimary && !props.isLimitedView) {
				options.push({
					id: TableAttributeKeyTypes.FOREIGN_KEY,
					icon: (
						<VpnKeyIcon className='key-icon-popover-attribute foreign_key_svg' />
					),
					name: t(FOREIGN_KEY),
				})
			}
			if (hasUnique) {
				options.push({
					id: TableAttributeKeyTypes.UNIQUE_KEY,
					icon: (
						<VpnKeyIcon className='key-icon-popover-attribute unique_key_svg' />
					),
					name: t(UNIQUE_KEY),
				})
			}
		}
		return options
	}

	const handleClosePopOver = () => {
		props.onClose()
	}

	const handleConstraintChange = (name: string, value: any) => {
		props.onChangeOfConstraint({ [name]: value })
	}

	const handleDeleteAttribute = () => {
		if (isFunction(props.onDelete)) {
			props.onDelete()
		}
	}

	const handleRelationshipTypeChange = (value: TableAttributeKeyTypes) => {
		props.onKeyChange(value)
	}

	const renderStringType = (constraint: AttributeTypesConstraintProps) => {
		return (
			<TableRow key={constraint.name}>
				<TableCell className='table-cell-pop-over'>{constraint.name}</TableCell>
				<TableCell>
					<IsyInput<string>
						type='text'
						onBlur={(value) => handleConstraintChange(constraint.name, value)}
						value={getConstraintValue(constraint)}
						disabled={props.isViewMode}
					/>
				</TableCell>
			</TableRow>
		)
	}

	const renderIntegerType = (constraint: AttributeTypesConstraintProps) => {
		return (
			<TableRow key={constraint.name}>
				<TableCell className='table-cell-pop-over'>{constraint.name}</TableCell>
				<TableCell>
					<IsyInput<number>
						type='number'
						onBlur={(value) => handleConstraintChange(constraint.name, value)}
						value={getConstraintValue(constraint)}
						disabled={props.isViewMode}
					/>
				</TableCell>
			</TableRow>
		)
	}

	const renderBooleanType = (
		constraint: AttributeTypesConstraintProps,
		isDisabled?: boolean
	) => {
		return (
			<TableRow key={constraint.name}>
				<TableCell className='table-cell-pop-over'>
					{constraint.name}?
				</TableCell>
				<TableCell className='require-switch'>
					<RequiredSwitch
						checked={getConstraintValue(constraint)}
						disabled={isDisabled || props.isViewMode}
						onChange={(e) =>
							handleConstraintChange(constraint.name, e.target.checked)
						}
						name={constraint.name}
						className={props.isViewMode ? 'require-switch-disabled' : ''}
					/>
				</TableCell>
			</TableRow>
		)
	}

	const renderKeyDropdown = (hasPrimary: boolean, hasUnique: boolean) => {
		return (
			<TableRow key='key_type'>
				<TableCell className='table-cell-key-type'>{t(KEY_TYPE)}</TableCell>
				<TableCell>
					<div className='keyTypeDropdown'>
						<IsyDropDown<TableAttributeKeyTypes>
							options={getPopoverActions(hasPrimary, hasUnique)}
							onChange={handleRelationshipTypeChange}
							value={getSelectedKeyType()}
							className='input-base'
							disabled={props.isViewMode}
						/>
					</div>
				</TableCell>
			</TableRow>
		)
	}

	const renderConfig = () => {
		const children = []
		let requiredChild = null
		let hasPrimary = false
		let hasUnique = false
		for (let i = 0, iLen = constraintsConfig.length; i < iLen; i++) {
			const name = constraintsConfig[i].name
			if (name === 'required') {
				requiredChild = renderBooleanType(
					constraintsConfig[i],
					constraints.primary
				)
			} else if (name === 'primary') {
				hasPrimary = true
			} else if (name === 'unique') {
				hasUnique = true
			} else {
				if (
					constraintsConfig[i].dataType ===
					DatabaseAttributeConstraintDataTypes.BOOLEAN
				) {
					children.push(renderBooleanType(constraintsConfig[i]))
				} else if (
					constraintsConfig[i].dataType ===
					DatabaseAttributeConstraintDataTypes.INTEGER
				) {
					children.push(renderIntegerType(constraintsConfig[i]))
				} else if (
					constraintsConfig[i].dataType ===
					DatabaseAttributeConstraintDataTypes.STRING
				) {
					children.push(renderStringType(constraintsConfig[i]))
				}
			}
		}
		children.push(requiredChild)
		if (hasPrimary || hasUnique) {
			children.push(renderKeyDropdown(hasPrimary, hasUnique))
		}
		return children
	}
	const renderPopOverContent = () => {
		return (
			<Table className='table-pop-over-container'>
				<TableBody>
					{renderConfig()}
					{!props.isLimitedView && !props.isViewMode && (
						<TableRow>
							<TableCell colSpan={2} className={'delete-column-container'}>
								<IsyButton
									className='delete-column'
									onClick={handleDeleteAttribute}
								>
									<DeleteOutlineIcon className='delete-icon' />
									{'Delete Column'}
								</IsyButton>
							</TableCell>
						</TableRow>
					)}
				</TableBody>
			</Table>
		)
	}

	return (
		<Popover
			open={true}
			onClose={handleClosePopOver}
			anchorEl={anchorEl}
			anchorOrigin={{
				vertical: 'bottom',
				horizontal: 'right',
			}}
			transformOrigin={{
				vertical: 'top',
				horizontal: 'right',
			}}
			className='pop-over-container'
		>
			{renderPopOverContent()}
		</Popover>
	)
}
