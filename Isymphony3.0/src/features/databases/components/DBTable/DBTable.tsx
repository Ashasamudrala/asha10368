import React from 'react'
import AddIcon from '@material-ui/icons/Add'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined'
import { DBAttribute } from './DBAttribute/DBAttribute'
import {
	ADD_COLUMN,
	DATABASE_TRANSLATIONS,
} from '../../../../utilities/constants'
import './DBTable.scss'
import { IsyInput } from '../../../../widgets/IsyInput/IsyInput'
import { TFunction, withTranslation } from 'react-i18next'
import { isFunction } from 'lodash'
import {
	DatabaseTableAttributeProps,
	DatabaseTableProps,
	TableAttributeKeyTypes,
} from '../../schema/schema.types'
import { AttributeTypeProps } from '../../databases.types'

export interface DBTableProps {
	table: DatabaseTableProps
	attributeTypes: AttributeTypeProps[]
	isEditMode: boolean
	isEditable: boolean
	tableIndex: number
	isSelected: boolean
	isLastIndex?: boolean
	isLimitedView: boolean
	onChange?: (table: Partial<DatabaseTableProps>, tableIndex: number) => void
	onAttrChange: (
		attr: Partial<DatabaseTableAttributeProps>,
		tableIndex: number,
		attrIndex: number
	) => void
	setEditMode?: (tableId: string) => void
	onAddAttr?: (tableIndex: number) => void
	onDelete?: (tableIndex: number) => void
	onDeleteAttr?: (tableIndex: number, attrIndex: number) => void
	onSetKey: (
		tableIndex: number,
		attrIndex: number,
		key: TableAttributeKeyTypes
	) => void

	t: TFunction<string[]> // will add by translate component
}

class DBTableComponent extends React.PureComponent<DBTableProps> {
	handleOnNameChange = (name: string) => {
		if (isFunction(this.props.onChange) && this.props.table.name !== name) {
			this.props.onChange({ name: name }, this.props.tableIndex)
		}
	}

	handleOnAttrChange = (
		attr: Partial<DatabaseTableAttributeProps>,
		index: number
	) => {
		this.props.onAttrChange(attr, this.props.tableIndex, index)
	}

	handleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		if (this.props.isEditMode) {
			e.stopPropagation()
		}
	}

	handleEnableEdit = (e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
		e.stopPropagation()
		if (isFunction(this.props.setEditMode)) {
			this.props.setEditMode(this.props.table.id)
		}
	}

	handleOnAddColumn = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		e.stopPropagation()
		if (isFunction(this.props.onAddAttr)) {
			this.props.onAddAttr(this.props.tableIndex)
		}
	}

	handleOnDelete = (e: React.MouseEvent<SVGSVGElement, MouseEvent>) => {
		e.stopPropagation()
		if (isFunction(this.props.onDelete)) {
			this.props.onDelete(this.props.tableIndex)
		}
	}

	handleOnAttrDelete = (attrIndex: number) => {
		if (isFunction(this.props.onDeleteAttr)) {
			this.props.onDeleteAttr(this.props.tableIndex, attrIndex)
		}
	}

	handleOnSetKey = (attrIndex: number, keyType: TableAttributeKeyTypes) => {
		this.props.onSetKey(this.props.tableIndex, attrIndex, keyType)
	}

	renderAttributes = () => {
		return this.props.table.attributes.map(
			(item: DatabaseTableAttributeProps, index: number) => (
				<DBAttribute
					key={(index as any) + item}
					index={index}
					data={item}
					attributeTypes={this.props.attributeTypes}
					isEditMode={this.props.isEditMode}
					onChange={this.handleOnAttrChange}
					onDelete={this.handleOnAttrDelete}
					onSetKey={this.handleOnSetKey}
					isLimitedView={this.props.isLimitedView}
				/>
			)
		)
	}

	renderEditMode = () => {
		return (
			<div className='table-name' onClick={this.handleClick}>
				<IsyInput
					type='text'
					onBlur={this.handleOnNameChange}
					value={this.props.table.name}
					autoFocus={true}
				/>
				<span className='header-icon-edit'>
					<DeleteOutlineIcon onClick={this.handleOnDelete} />
				</span>
			</div>
		)
	}

	renderTitleView = () => {
		return (
			<div className='table-name' onClick={this.handleClick}>
				<span title={this.props.table.name} className='title-name'>
					{this.props.table.name}
				</span>
				{!this.props.isLimitedView && this.props.isEditable && (
					<span className='header-icon'>
						<EditOutlinedIcon
							className='database-container-edit'
							onClick={this.handleEnableEdit}
						/>
					</span>
				)}
			</div>
		)
	}

	render() {
		const { t } = this.props
		return (
			<div className='database-container'>
				<div
					className={
						'database-table ' +
						(this.props.isSelected ? 'database-table-selected' : '') +
						(this.props.isLastIndex ? 'database-last-table' : '')
					}
				>
					{this.props.isEditMode && !this.props.isLimitedView
						? this.renderEditMode()
						: this.renderTitleView()}
					<div className='table-styles'>
						<table>
							<tbody>{this.renderAttributes()}</tbody>
						</table>
					</div>
					{this.props.isEditMode && !this.props.isLimitedView && (
						<button className='add-column-btn' onClick={this.handleOnAddColumn}>
							<AddIcon className='add-icon' />
							<span className='add-column'>{t(ADD_COLUMN)}</span>
						</button>
					)}
				</div>
			</div>
		)
	}
}

export const DBTable = withTranslation(DATABASE_TRANSLATIONS)(DBTableComponent)
