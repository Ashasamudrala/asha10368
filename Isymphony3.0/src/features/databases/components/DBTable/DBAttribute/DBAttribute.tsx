import React, { Fragment, useState, useEffect } from 'react'
import { IsyPopover } from '../../../../../widgets/IsyPopover/IsyPopover'
import { useTranslation } from 'react-i18next'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import Tooltip from '@material-ui/core/Tooltip'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import { IsyInput } from '../../../../../widgets/IsyInput/IsyInput'
import { isNil, find } from 'lodash'
import {
	DATABASE_TRANSLATIONS,
	NONE,
	PRIMARY_KEY,
	FOREIGN_KEY,
	UNIQUE_KEY,
} from '../../../../../utilities/constants'
import './DBAttribute.scss'
import {
	DatabaseTableAttributeProps,
	TableAttributeKeyTypes,
} from '../../../schema/schema.types'
import { AttributeTypeProps } from '../../../databases.types'

export interface DBAttributeProps {
	data: DatabaseTableAttributeProps
	index: number
	attributeTypes: AttributeTypeProps[]
	isEditMode: boolean
	isLimitedView: boolean
	onChange: (attr: Partial<DatabaseTableAttributeProps>, index: number) => void
	onSetKey: (index: number, key: TableAttributeKeyTypes) => void
	onDelete: (index: number) => void
}

export function DBAttribute(props: DBAttributeProps) {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const [anchorEl, setAnchorEl] = useState<Element | null>(null)

	useEffect(() => {
		setAnchorEl(null)
	}, [props.isEditMode])

	const getFilteredOptions = () => {
		if (props.data.constraints.primary) {
			return props.attributeTypes.filter((attr) => {
				return !isNil(find(attr.constraints, (c) => c.name === 'primary'))
			})
		}
		return props.attributeTypes
	}

	const getSvgTypeClassName = (canNull: boolean) => {
		if (!isNil(props.data.reference)) {
			return 'foreign_key_svg'
		}
		if (!isNil(props.data.constraints)) {
			if (props.data.constraints.primary) {
				return 'primary_key_svg'
			}
			if (props.data.constraints.unique) {
				return 'unique_key_svg'
			}
		}
		if (!canNull) {
			return 'none_key_svg'
		}
		return 'no_key_svg'
	}
	const getTooltipName = () => {
		if (!isNil(props.data.reference)) {
			return t(FOREIGN_KEY)
		}
		if (!isNil(props.data.constraints)) {
			if (props.data.constraints.primary) {
				return t(PRIMARY_KEY)
			}
			if (props.data.constraints.unique) {
				return t(UNIQUE_KEY)
			}
		}
		return ''
	}

	const getAttributeTypeConstraints = () => {
		const attributeType = find(
			props.attributeTypes,
			(a) => a.value === props.data.type
		)
		if (!isNil(attributeType)) {
			return attributeType.constraints.map((c) => c.name)
		}
		return []
	}

	const getPopoverActions = () => {
		const options = []
		const constraints = getAttributeTypeConstraints()
		if (
			constraints.indexOf('primary') !== -1 &&
			(!props.isLimitedView || props.data.constraints.primary)
		) {
			options.push({
				key: TableAttributeKeyTypes.PRIMARY_KEY,
				hasIcon: true,
				icon: <VpnKeyIcon className='key-icon primary_key_svg' />,
				name: t(PRIMARY_KEY),
				labelClass: 'key_label',
			})
		}
		if (!props.data.constraints.primary) {
			options.unshift({
				key: TableAttributeKeyTypes.NONE,
				hasIcon: true,
				icon: <VpnKeyIcon className='key-icon none_key_svg' />,
				name: t(NONE),
				labelClass: 'key_label',
			})
			if (constraints.indexOf('primary') !== -1 && !props.isLimitedView) {
				options.push({
					key: TableAttributeKeyTypes.FOREIGN_KEY,
					hasIcon: true,
					icon: <VpnKeyIcon className='key-icon foreign_key_svg' />,
					name: t(FOREIGN_KEY),
					labelClass: 'key_label',
					menuClass: 'isy-foreign-key',
				})
			}
			if (constraints.indexOf('unique') !== -1) {
				options.push({
					key: TableAttributeKeyTypes.UNIQUE_KEY,
					hasIcon: true,
					icon: <VpnKeyIcon className='key-icon unique_key_svg' />,
					name: t(UNIQUE_KEY),
					labelClass: 'key_label',
				})
			}
		}
		return options
	}

	const handleOnClick = (
		e: React.MouseEvent<HTMLTableRowElement, MouseEvent>
	) => {
		e.stopPropagation()
	}

	const handleOnNameChange = (name: string) => {
		if (props.data.name !== name) {
			props.onChange({ name: name }, props.index)
		}
	}

	const handleOnTypeChange = (evt: React.ChangeEvent<HTMLSelectElement>) => {
		props.onChange({ type: evt.target.value }, props.index)
	}

	const handleClickOnKeyIcon = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>
	) => {
		event.stopPropagation()
		setAnchorEl(event.currentTarget)
	}
	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setAnchorEl(null)
	}
	const handleMenuItems = (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		key: string
	) => {
		event.stopPropagation()
		props.onSetKey(props.index, key as TableAttributeKeyTypes)
		setAnchorEl(null)
	}
	const renderPopover = () => {
		if (isNil(anchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={anchorEl}
				actions={getPopoverActions()}
				className={'key-popover'}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			/>
		)
	}

	return (
		<Fragment>
			{props.isEditMode ? (
				<tr onClick={handleOnClick}>
					<td>
						{!props.isLimitedView && (
							<span className='icon'>
								<DeleteOutlineIcon
									className='delete-icon'
									onClick={() => props.onDelete(props.index)}
								/>
							</span>
						)}
						<IsyInput<string>
							type='text'
							onBlur={handleOnNameChange}
							value={props.data.name}
							disabled={props.isLimitedView}
						/>
					</td>

					<td>
						<select
							name='type'
							value={props.data.type}
							onChange={handleOnTypeChange}
							disabled={!isNil(props.data.reference)}
						>
							<option disabled selected hidden value=''>
								Type
							</option>
							{getFilteredOptions().map((type) => (
								<option key={type.value} value={type.value}>
									{type.value}
								</option>
							))}
						</select>
						<span className='icon key-icon'>
							<VpnKeyIcon
								className={getSvgTypeClassName(false)}
								onClick={handleClickOnKeyIcon}
							/>
						</span>
						{renderPopover()}
					</td>
				</tr>
			) : (
				<tr key={props.index}>
					<td>
						<span title={props.data.name} className='attribute-content'>
							{props.data.name}
						</span>
					</td>
					<td>
						<span>{props.data.type}</span>
						<Tooltip title={getTooltipName()} className='tooltip-icon'>
							<span className='icon key-icon'>
								<VpnKeyIcon className={getSvgTypeClassName(true)} />
							</span>
						</Tooltip>
					</td>
				</tr>
			)}
		</Fragment>
	)
}
