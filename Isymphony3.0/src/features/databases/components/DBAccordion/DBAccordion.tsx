import React, { useState } from 'react'
import './dBAccordion.scss'
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Accordion from '@material-ui/core/Accordion'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import IconButton from '@material-ui/core/IconButton'
import {
	DATABASE_TRANSLATIONS,
	DATABASES,
	CREATE_DATABASE,
	// SETTINGS,
	DELETE,
	DATABASE_ADD_A_NEW_SCHEMA,
	DATABASE_RENAME,
} from '../../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined'
import {
	DatabasesDatabaseProps,
	DatabasesDatabaseSchemaProps,
} from '../../databases.types'
import { DatabaseTableProps } from '../../schema/schema.types'
import { isNil } from 'lodash'
import {
	IsyPopover,
	IsyPopoverActionProps,
} from '../../../../widgets/IsyPopover/IsyPopover'

export interface DBAccordionProps {
	databases: DatabasesDatabaseProps[]
	tables: DatabaseTableProps[]
	isEditable: boolean

	selectedDatabase: string | null
	selectedSchema: string | null
	selectedTable: number | null

	setSelectedDatabase: (id: string) => void
	setSelectedSchema: (id: string) => void
	setSelectedTable: (index: number) => void

	onAddDatabase: () => void
	onDeleteDatabase: (databaseId: string) => void
	onDatabaseSettings: (databaseId: string) => void
	onAddSchema: (databaseId: string) => void
	onDeleteSchema: (databaseId: string, schemaId: string) => void
}

export interface DBAccordionPopoverProps {
	for: 'DATABASE' | 'SCHEMA'
	databaseId: string
	schemaId: string | null
	anchor: Element
}

export function DBAccordion(props: DBAccordionProps) {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const [
		popoverDetails,
		setPopoverDbDetails,
	] = useState<DBAccordionPopoverProps | null>(null)

	const getPopoverOptions = (): IsyPopoverActionProps[] => {
		if (!isNil(popoverDetails)) {
			if (popoverDetails.for === 'DATABASE') {
				return [
					// {
					// 	name: t(SETTINGS),
					// 	key: 'DB_SETTINGS',
					// },
					{
						name: t(DELETE),
						key: 'DB_DELETE',
					},
					{
						name: t(DATABASE_ADD_A_NEW_SCHEMA),
						key: 'DB_ADD_NEW_SCHEMA',
					},
				]
			} else if (popoverDetails.for === 'SCHEMA') {
				return [
					{
						name: t(DATABASE_RENAME),
						key: 'SCHEMA_RENAME',
					},
					{
						name: t(DELETE),
						key: 'SCHEMA_DELETE',
					},
				]
			}
		}
		return []
	}

	const handleMenuItems = (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		selectedItem: string
	) => {
		event.stopPropagation()
		if (!isNil(popoverDetails)) {
			switch (selectedItem) {
				case 'DB_SETTINGS': {
					props.onDatabaseSettings(popoverDetails.databaseId)
					break
				}
				case 'DB_DELETE': {
					props.onDeleteDatabase(popoverDetails.databaseId)
					break
				}
				case 'DB_ADD_NEW_SCHEMA': {
					props.onAddSchema(popoverDetails.databaseId)
					break
				}
				case 'SCHEMA_RENAME': {
					break
				}
				case 'SCHEMA_DELETE': {
					if (!isNil(popoverDetails.schemaId)) {
						props.onDeleteSchema(
							popoverDetails.databaseId,
							popoverDetails.schemaId
						)
					}
					break
				}
			}
			setPopoverDbDetails(null)
		}
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setPopoverDbDetails(null)
	}

	const handleDatabaseSelect = (
		ev: React.ChangeEvent<{}>,
		databaseId: string,
		isExpanded: boolean
	) => {
		ev.stopPropagation()
		if (isExpanded) {
			props.setSelectedDatabase(databaseId)
		}
	}

	const handleSchemaSelect = (
		ev: React.ChangeEvent<{}>,
		schemaId: string,
		isExpanded: boolean
	) => {
		ev.stopPropagation()
		if (isExpanded) {
			props.setSelectedSchema(schemaId)
		}
	}

	const handleListItem = (index: number) => {
		props.setSelectedTable(index)
	}

	const handleShowDatabaseActions = (
		event: React.MouseEvent<SVGSVGElement, MouseEvent>,
		databaseId: string
	) => {
		event.stopPropagation()
		setPopoverDbDetails({
			for: 'DATABASE',
			databaseId,
			schemaId: null,
			anchor: event.currentTarget,
		})
	}

	// const handleShowSchemaActions = (
	// 	event: React.MouseEvent<SVGSVGElement, MouseEvent>,
	// 	databaseId: string,
	// 	schemaId: string
	// ) => {
	// 	event.stopPropagation()
	// 	setPopoverDbDetails({
	// 		for: 'SCHEMA',
	// 		databaseId,
	// 		schemaId,
	// 		anchor: event.currentTarget,
	// 	})
	// }

	const renderSubListDetails = () => {
		return (
			<List>
				{props.tables.map((tableData: DatabaseTableProps, index: number) => {
					return (
						<ListItem
							button
							key={index}
							onClick={() => handleListItem(index)}
							className={`${
								props.selectedTable === index ? 'list-selected' : ''
							} list-item`}
						>
							<ListItemIcon>
								<TableChartOutlinedIcon />
							</ListItemIcon>
							<ListItemText
								className='list-item-text'
								primary={tableData.name}
								title={tableData.name}
							/>
						</ListItem>
					)
				})}
			</List>
		)
	}

	const renderPopover = () => {
		if (isNil(popoverDetails)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverDetails.anchor}
				actions={getPopoverOptions()}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
				className={'pop-over-menu-list'}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			/>
		)
	}

	const renderSchemaAccordion = (
		databaseId: string,
		schema: DatabasesDatabaseSchemaProps,
		index: number
	) => {
		return (
			<Accordion
				expanded={schema.id === props.selectedSchema}
				onChange={(event, isExpanded) =>
					handleSchemaSelect(event, schema.id, isExpanded)
				}
				className='schemaAccordion'
				key={index}
			>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<div className='account-tree-icon'>
						<AccountTreeOutlinedIcon />
					</div>
					<Typography title={schema.name} className='name'>
						{schema.name}
					</Typography>
					<div className='hide-settings-icon'>
						{/* <MoreHorizIcon
							onClick={(event) =>
								handleShowSchemaActions(event, databaseId, schema.id)
							}
						/> */}
					</div>
				</AccordionSummary>
				<AccordionDetails className='tables-list-container'>
					{schema.id === props.selectedSchema && renderSubListDetails()}
				</AccordionDetails>
			</Accordion>
		)
	}

	const renderDatabaseAccordion = (
		database: DatabasesDatabaseProps,
		index: number
	) => {
		return (
			<Accordion
				expanded={database.id === props.selectedDatabase}
				onChange={(event, isExpanded) =>
					handleDatabaseSelect(event, database.id, isExpanded)
				}
				className='databaseAccordion'
				key={index}
			>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography title={database.name} className='name'>
						{database.name}
					</Typography>
					<div className='hide-settings-icon'>
						<MoreHorizIcon
							onClick={(event) => handleShowDatabaseActions(event, database.id)}
						/>
					</div>
				</AccordionSummary>
				<AccordionDetails>
					{database.schemas.map((s, i) =>
						renderSchemaAccordion(database.id, s, i)
					)}
				</AccordionDetails>
			</Accordion>
		)
	}

	const renderAccordions = () => {
		return (
			<div className='database-accordion-overflow'>
				{props.databases.map(renderDatabaseAccordion)}
			</div>
		)
	}

	const renderHeader = () => {
		return (
			<div className='header'>
				<div className='database-parent-accordion'>
					<div className='database'>{t(DATABASES)}</div>
					<Tooltip title={t(CREATE_DATABASE) as string}>
						<IconButton
							aria-label='Add Database'
							onClick={props.onAddDatabase}
							component='span'
						>
							<AddOutlinedIcon />
						</IconButton>
					</Tooltip>
				</div>
			</div>
		)
	}

	return (
		<div className='database-parent'>
			{renderHeader()}
			{renderAccordions()}
			{renderPopover()}
		</div>
	)
}
