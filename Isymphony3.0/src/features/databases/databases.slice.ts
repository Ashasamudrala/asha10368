import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import {
	DatabasesDatabaseProps,
	DatabasesDatabaseSchemaProps,
	DatabasesState,
} from './databases.types'
import { Action } from '../../common.types'
import { DatabasesSubReducersNames } from './base.types'
import * as asyncActions from './databases.asyncActions'
import { findIndex, isArray, isEmpty, isNil } from 'lodash'

const initialState: DatabasesState = {
	appId: null,
	databases: [],
	databaseId: null,
	schemaId: null,
	attributeTypes: [],
}

const slice = createSlice<
	DatabasesState,
	SliceCaseReducers<DatabasesState>,
	DatabasesSubReducersNames.DATABASES
>({
	name: DatabasesSubReducersNames.DATABASES,
	initialState,
	reducers: {
		// synchronous actions
		setAppId(state: DatabasesState, action: Action<string>) {
			state.appId = action.payload
		},
		setDatabaseId(state: DatabasesState, action: Action<string>) {
			state.databaseId = action.payload
			state.schemaId = null
		},
		setSchemaId(state: DatabasesState, action: Action<string>) {
			state.schemaId = action.payload
		},
		updateDatabaseSettingsFallback(
			state: DatabasesState,
			action: Action<DatabasesDatabaseProps>
		) {
			const databases = [...state.databases]
			const index = findIndex(databases, (d) => d.id === action.payload.id)
			if (index === -1) {
				databases.push(action.payload)
			} else {
				databases[index] = action.payload
			}
			state.databases = databases
			state.databaseId = action.payload.id
			state.schemaId = null
		},
		clearData: () => initialState,
	},

	extraReducers: (builder) => {
		builder.addCase(
			asyncActions.fetchDatabasesListWithSchemas.fulfilled,
			(state: DatabasesState, action) => {
				if (action.payload && isArray(action.payload.content)) {
					state.databases = action.payload.content.map((d: any) => {
						return {
							id: d.id,
							name: d.name,
							schemas: d.schemaInfo.map(
								(s: any) =>
									({
										id: s.id,
										name: s.name,
									} as DatabasesDatabaseSchemaProps)
							),
						} as DatabasesDatabaseProps
					})
					if (state.databases.length > 0 && isNil(state.databaseId)) {
						state.databaseId = state.databases[0].id
					}
				}
			}
		)
		builder.addCase(
			asyncActions.fetchAttributeTypes.fulfilled,
			(state: DatabasesState, action) => {
				state.attributeTypes = action.payload.content
			}
		)
		builder.addCase(
			asyncActions.addNewSchema.fulfilled,
			(state: DatabasesState, action) => {
				if (action.payload) {
					const databaseId = action.meta.arg.databaseId
					const databases = [...state.databases]
					const index = findIndex(databases, (d) => d.id === databaseId)
					if (index !== -1) {
						const schemas = [...databases[index].schemas]
						schemas.push({ id: action.payload.id, name: action.payload.name })
						databases[index] = { ...databases[index], schemas }
						state.databases = databases
						state.databaseId = databaseId
						state.schemaId = action.payload.id
					}
				}
			}
		)
		builder.addCase(
			asyncActions.deleteDatabase.fulfilled,
			(state: DatabasesState, action) => {
				if (action && isEmpty(action.payload)) {
					const databaseId = action.meta.arg.databaseId
					const databases = [...state.databases]
					const index = findIndex(databases, (d) => d.id === databaseId)
					if (index !== -1) {
						databases.splice(index, 1)
						state.databases = databases
						state.databaseId = databases.length > 0 ? databases[0].id : null
						state.schemaId = null
					}
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
