import { combineReducers } from 'redux'
import databases from './databases.slice'
import schema from './schema/schema.slice'
import canvas from './canvas/canvas.slice'
import settings from './settings/settings.slice'
import * as baseSelector from './base.selector'
import { DatabasesBaseState } from './base.types'
import undoable from 'redux-undo'

export const reducer = combineReducers<DatabasesBaseState>({
	[databases.name]: databases.reducer,
	[schema.name]: undoable(schema.reducer),
	[canvas.name]: canvas.reducer,
	[settings.name]: settings.reducer,
})

export const { name } = baseSelector

export * from './base.types'
