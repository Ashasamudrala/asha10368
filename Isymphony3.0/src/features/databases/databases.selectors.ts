import { find, findIndex, isNil } from 'lodash'
import { RootState } from '../../base.types'
import { selectSlice as baseSelector } from './base.selector'
import { DatabasesSubReducersNames } from './base.types'
import {
	AttributeTypeProps,
	AttributeTypesConstraintProps,
	DatabasesDatabaseProps,
	DatabasesState,
} from './databases.types'

export const selectSlice = (state: RootState): DatabasesState =>
	baseSelector(state)[DatabasesSubReducersNames.DATABASES]

export const selectAppId = (state: RootState): string | null => {
	return selectSlice(state).appId
}

export const selectDatabases = (state: RootState): DatabasesDatabaseProps[] => {
	return selectSlice(state).databases
}

export const selectDatabaseId = (state: RootState): string | null => {
	return selectSlice(state).databaseId
}

export const selectSchemaId = (state: RootState): string | null => {
	return selectSlice(state).schemaId
}

export const selectAttributeTypes = (
	state: RootState
): AttributeTypeProps[] => {
	return selectSlice(state).attributeTypes
}

export const selectDbConstraintsByAttributeType = (
	state: RootState,
	type: string
): AttributeTypesConstraintProps[] => {
	const attrTypes = selectAttributeTypes(state)
	const attr = find(attrTypes, (a) => a.value === type)
	if (!isNil(attr)) {
		return attr.constraints
	}
	return []
}

export const selectDatabasesById = (
	state: RootState,
	databaseId: string
): DatabasesDatabaseProps | null => {
	const databases = selectDatabases(state)
	const index = findIndex(databases, (d) => d.id === databaseId)
	if (index === -1) {
		return null
	}
	return databases[index]
}
