import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import { DatabaseIcon } from '../../icons/DatabaseIcon'
import { useParams } from 'react-router-dom'
import {
	DATABASE_TRANSLATIONS,
	DATABASE_TEXT,
	DATABASE_BUTTON,
	DATABASE_NO_SCHEMA_SELECTED,
} from '../../utilities/constants'
import './databases.scss'
import { useTranslation } from 'react-i18next'
import { actions } from './databases.slice'
import { actions as schemaActions } from './schema/schema.slice'
import { actions as canvasActions } from './canvas/canvas.slice'
import {
	confirmDatabaseDelete,
	confirmOnAddSchema,
	confirmOnOpenDbSettings,
	confirmOnSelectDatabase,
	confirmOnSelectSchema,
	initDatabaseService,
} from './databases.controller'
import {
	selectAttributeTypes,
	selectDatabaseId,
	selectDatabases,
	selectSchemaId,
} from './databases.selectors'
import { DBAccordion } from './components/DBAccordion/DBAccordion'
import {
	selectAttributeIndex,
	selectTableIndex,
	selectTables,
} from './schema/schema.selectors'
import { isNil } from 'lodash'
import { Canvas } from './canvas/Canvas'
import { checkLockStatus, releaseLock } from './canvas/canvas.asyncActions'
import Settings from './settings/Settings'
import { DatabasesDatabaseProps } from './databases.types'
import { loadSelectedSchema } from './schema/schema.controller'
import { selectGetLoginUserId } from '../../authAndPermissions/loginUserDetails.selectors'
import { ActionCreators } from 'redux-undo'

export function Databases() {
	const { appid } = useParams<{ appid: string }>()
	const databases = useSelector(selectDatabases)
	const databaseId = useSelector(selectDatabaseId)
	const schemaId = useSelector(selectSchemaId)
	const tables = useSelector(selectTables)
	const tableIndex = useSelector(selectTableIndex)
	const attributeIndex = useSelector(selectAttributeIndex)
	const attributeTypes = useSelector(selectAttributeTypes)
	const loginUserId = useSelector(selectGetLoginUserId)

	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(initDatabaseService(appid))
		return () => {
			dispatch(releaseLock())
			dispatch(actions.clearData(null))
			dispatch(canvasActions.clearData(null))
			dispatch(schemaActions.clearData(null))
			dispatch(ActionCreators.clearHistory())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (isNil(schemaId)) {
			dispatch(schemaActions.clearData(null))
			dispatch(ActionCreators.clearHistory())
			dispatch(canvasActions.clearData(null))
		} else {
			dispatch(canvasActions.clearData(null))
			dispatch(loadSelectedSchema(schemaId))
			dispatch(checkLockStatus(loginUserId))
		}
	}, [schemaId, dispatch]) // eslint-disable-line react-hooks/exhaustive-deps

	const handleDatabaseSelect = (id: string) => {
		dispatch(confirmOnSelectDatabase(id))
	}

	const handleSchemaSelect = (id: string) => {
		dispatch(confirmOnSelectSchema(id))
	}

	const handleTableSelect = (tableIndex: number) => {
		dispatch(
			schemaActions.setTableAndAttributeIndex({
				tableIndex,
				attributeIndex: null,
			})
		)
	}

	const handleSelectedTableIndexAndAttrIndex = (
		tableIndex: number | null,
		attributeIndex: number | null
	) => {
		dispatch(
			schemaActions.setTableAndAttributeIndex({
				tableIndex,
				attributeIndex,
			})
		)
	}

	const handleDatabaseDelete = (databaseId: string) => {
		dispatch(confirmDatabaseDelete(databaseId))
	}

	const handleOpenDatabaseSettings = (databaseId: string) => {
		dispatch(confirmOnOpenDbSettings(databaseId))
	}

	const handleAddDatabaseSchema = (databaseId: string) => {
		dispatch(confirmOnAddSchema(databaseId))
	}

	const handleDeleteDatabaseSchema = (databaseId: string, schemaId: string) => {
		// todo
	}

	const handleSettingsUpdate = (data: DatabasesDatabaseProps) => {
		dispatch(actions.updateDatabaseSettingsFallback(data))
	}

	const handleAddDatabase = () => {
		dispatch(confirmOnOpenDbSettings(null))
	}

	const renderNoDbScreen = () => {
		return (
			<div className='database-page'>
				<DatabaseIcon className='data-base-icon' />
				<div className='data-base-text'>{t(DATABASE_TEXT)}</div>
				<div>
					<IsyButton
						className='primary-btn add-new-database'
						onClick={handleAddDatabase}
					>
						{t(DATABASE_BUTTON)}
					</IsyButton>
				</div>
			</div>
		)
	}

	const renderNoSchemaSelectedView = () => {
		return (
			<div className='database-page'>
				<DatabaseIcon className='data-base-icon' />
				<div className='data-base-text'>{t(DATABASE_NO_SCHEMA_SELECTED)}</div>
			</div>
		)
	}

	const renderTables = () => {
		return (
			<Canvas
				tables={tables}
				tableIndex={tableIndex}
				attributeIndex={attributeIndex}
				attributeTypes={attributeTypes}
				onSelectedTableIndexAndAttrIndex={handleSelectedTableIndexAndAttrIndex}
			/>
		)
	}

	const renderMainView = () => {
		return (
			<div className='db-container'>
				<div className='left'>
					<DBAccordion
						databases={databases}
						tables={tables}
						selectedDatabase={databaseId}
						selectedSchema={schemaId}
						selectedTable={tableIndex}
						isEditable={true}
						setSelectedDatabase={handleDatabaseSelect}
						setSelectedSchema={handleSchemaSelect}
						setSelectedTable={handleTableSelect}
						onAddDatabase={handleAddDatabase}
						onDeleteDatabase={handleDatabaseDelete}
						onDatabaseSettings={handleOpenDatabaseSettings}
						onAddSchema={handleAddDatabaseSchema}
						onDeleteSchema={handleDeleteDatabaseSchema}
					/>
				</div>
				<div className='right'>
					{isNil(schemaId) ? renderNoSchemaSelectedView() : renderTables()}
				</div>
			</div>
		)
	}

	return (
		<>
			{databases.length === 0 ? renderNoDbScreen() : renderMainView()}
			<Settings appId={appid} onUpdate={handleSettingsUpdate} />
		</>
	)
}
