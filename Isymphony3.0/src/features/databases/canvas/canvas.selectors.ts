import { RootState } from '../../../base.types'
import { selectSlice as baseSelector } from '../base.selector'
import { DatabasesSubReducersNames } from '../base.types'
import { CanvasState, DatabaseLockStatus } from './canvas.types'

export const selectSlice = (state: RootState): CanvasState =>
	baseSelector(state)[DatabasesSubReducersNames.CANVAS]

export const selectEditItem = (state: RootState): string | null => {
	return selectSlice(state).editItem
}

export const selectLockStatus = (state: RootState): DatabaseLockStatus => {
	return selectSlice(state).lockStatus
}

export const selectLockedBy = (state: RootState): string | null => {
	return selectSlice(state).lockedBy
}

export const selectSearchTerm = (state: RootState): string => {
	return selectSlice(state).searchTerm
}

export const selectShoePropertiesPanel = (state: RootState): boolean => {
	return selectSlice(state).showPropertiesPanel
}

export const selectTableIndexForPosition = (
	state: RootState
): number | null => {
	return selectSlice(state).tableIndexForPosition
}
