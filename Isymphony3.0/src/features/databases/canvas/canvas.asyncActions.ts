import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import { RootState } from '../../../base.types'
import doAsync from '../../../infrastructure/doAsync'
import {
	DATABASE_CHECK_IN,
	DATABASE_CHECK_IN_USER,
	DATABASE_CHECK_OUT,
} from '../../../utilities/apiEndpoints'
import {
	BUTTON_OK,
	COMMON_LOCK_NOT_PRESENT_MESSAGE,
	COMMON_REQUEST_LOCK_GRANTED,
	COMMON_REQUEST_LOCK_REJECTED,
	DATABASE_ACQUIRE_LOCK_MESSAGE,
} from '../../../utilities/constants'
import { DialogTypes, showDialog } from '../../dialogs'
import {
	selectAppId,
	selectDatabaseId,
	selectSchemaId,
} from '../databases.selectors'
import i18n from 'i18next'
import { selectOldData } from '../schema/schema.selectors'
import { loadSelectedSchema } from '../schema/schema.controller'
import { selectLockStatus } from './canvas.selectors'
import { DatabaseLockStatus } from './canvas.types'

export const checkLockStatus = createAsyncThunk(
	'databases/canvas/checkLockStatus',
	async (loggedInUserId: string | null, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const databaseId = selectDatabaseId(thunkArgs.getState() as RootState)
		const schemaId = selectSchemaId(thunkArgs.getState() as RootState)
		if (
			isNil(appId) ||
			isNil(databaseId) ||
			isNil(schemaId) ||
			isNil(loggedInUserId)
		) {
			return
		}
		return await doAsync({
			url: DATABASE_CHECK_IN_USER(appId, databaseId, schemaId),
			...thunkArgs,
		})
	}
)

export const releaseLock = createAsyncThunk(
	'databases/canvas/releaseLock',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const databaseId = selectDatabaseId(thunkArgs.getState() as RootState)
		const schemaId = selectSchemaId(thunkArgs.getState() as RootState)
		const lockStatus = selectLockStatus(thunkArgs.getState() as RootState)
		if (
			isNil(appId) ||
			isNil(databaseId) ||
			isNil(schemaId) ||
			lockStatus !== DatabaseLockStatus.LOCK_ACQUIRED
		) {
			return
		}
		return await doAsync({
			url: DATABASE_CHECK_OUT(appId, databaseId, schemaId),
			httpMethod: 'put',
			...thunkArgs,
		})
	}
)

export const acquireLock = createAsyncThunk(
	'databases/canvas/acquireLock',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const databaseId = selectDatabaseId(thunkArgs.getState() as RootState)
		const schemaId = selectSchemaId(thunkArgs.getState() as RootState)
		if (isNil(appId) || isNil(databaseId) || isNil(schemaId)) {
			return
		}
		return await doAsync({
			url: DATABASE_CHECK_IN(appId, databaseId, schemaId),
			httpMethod: 'put',
			...thunkArgs,
		}).then((data: any) => {
			if (data.response === 'ACQUIRED_LOCK') {
				const conformationCallBack = () => {
					thunkArgs.dispatch(loadSelectedSchema(schemaId))
				}
				const name = (selectOldData(thunkArgs.getState() as RootState) || {})
					.name
				thunkArgs.dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						onOkay: conformationCallBack,
						onCancel: conformationCallBack,
						title: i18n.t(COMMON_REQUEST_LOCK_GRANTED),
						data: {
							message: i18n.t(DATABASE_ACQUIRE_LOCK_MESSAGE, {
								name: name,
							}),
							okayButtonLabel: i18n.t(BUTTON_OK),
						},
					})
				)
			} else {
				thunkArgs.dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(COMMON_REQUEST_LOCK_REJECTED),
						data: {
							message: i18n.t(COMMON_LOCK_NOT_PRESENT_MESSAGE, {
								name: data.displayName,
							}),
							okayButtonLabel: i18n.t(BUTTON_OK),
						},
					})
				)
			}
			return data
		})
	}
)
