import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { CanvasState, DatabaseLockStatus } from './canvas.types'
import { Action } from '../../../common.types'
import { DatabasesSubReducersNames } from '../base.types'
import * as asyncActions from './canvas.asyncActions'
import { isEmpty, isNil } from 'lodash'

const initialState: CanvasState = {
	editItem: null,
	tableIndexForPosition: null,
	showPropertiesPanel: true,
	searchTerm: '',
	lockStatus: DatabaseLockStatus.UN_KNOWN,
	lockedBy: null,
}

const slice = createSlice<
	CanvasState,
	SliceCaseReducers<CanvasState>,
	DatabasesSubReducersNames.CANVAS
>({
	name: DatabasesSubReducersNames.CANVAS,
	initialState,
	reducers: {
		// synchronous actions
		setEditItem(state: CanvasState, action: Action<string | null>) {
			state.editItem = action.payload
		},
		setTableIndexForPosition(
			state: CanvasState,
			action: Action<number | null>
		) {
			state.tableIndexForPosition = action.payload
		},
		toggleShowPropertiesPanel(state: CanvasState) {
			state.showPropertiesPanel = !state.showPropertiesPanel
		},
		setSearchTerm(state: CanvasState, action: Action<string>) {
			state.searchTerm = action.payload
		},
		clearData: () => initialState,
	},

	extraReducers: (builder) => {
		builder.addCase(
			asyncActions.acquireLock.fulfilled,
			(state: CanvasState, action) => {
				if (action.payload.response === 'ACQUIRED_LOCK') {
					state.lockStatus = DatabaseLockStatus.LOCK_ACQUIRED
				} else {
					state.lockStatus = DatabaseLockStatus.LOCK_NOT_PRESENT
					state.lockedBy = action.payload.displayName
				}
			}
		)
		builder.addCase(
			asyncActions.releaseLock.fulfilled,
			(state: CanvasState, action) => {
				if (!isNil(action.payload)) {
					state.lockStatus = DatabaseLockStatus.LOCK_PRESENT
				}
			}
		)
		builder.addCase(
			asyncActions.checkLockStatus.fulfilled,
			(state: CanvasState, action) => {
				if (isEmpty(action.payload)) {
					state.lockStatus = DatabaseLockStatus.LOCK_PRESENT
				} else {
					const loggedInUserId = action.meta.arg
					if (loggedInUserId === action.payload.userId) {
						state.lockStatus = DatabaseLockStatus.LOCK_ACQUIRED
					} else {
						state.lockStatus = DatabaseLockStatus.LOCK_NOT_PRESENT
						state.lockedBy = action.payload.displayName
					}
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
