export enum DatabaseLockStatus {
	'LOCK_PRESENT',
	'LOCK_NOT_PRESENT',
	'LOCK_ACQUIRED',
	'UN_KNOWN',
}

export interface CanvasState {
	editItem: string | null
	tableIndexForPosition: number | null
	showPropertiesPanel: boolean
	searchTerm: string
	lockStatus: DatabaseLockStatus
	lockedBy: string | null
}
