import React, { useEffect } from 'react'
import { v4 } from 'uuid'
import { isNil, findIndex, find } from 'lodash'
import { DBToolbar } from '../components/DBToolbar/DBToolbar'
import { useTranslation } from 'react-i18next'
import {
	DATABASE_TRANSLATIONS,
	PROPERTIES_EMPTY_TEXT,
	EXPAND,
	COLLAPSE,
} from '../../../utilities/constants'
import { isParentUntil } from '../../../utilities/utilities'
import './canvas.scss'
import { OneToOneIcon } from '../../../icons/OneToOne'
import { OneToManyIcon } from '../../../icons/OneToMany'
import { ManyToOneIcon } from '../../../icons/ManyToOne'
import { ManyToManyIcon } from '../../../icons/ManyToMany'
import LastPageIcon from '@material-ui/icons/LastPage'
import FirstPageIcon from '@material-ui/icons/FirstPage'
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined'
import {
	DatabaseTableAttributeProps,
	DatabaseTableConstraintsProps,
	DatabaseTableProps,
	TableAttributeKeyTypes,
} from '../schema/schema.types'
import { AttributeTypeProps } from '../databases.types'
import { useDispatch, useSelector } from 'react-redux'
import {
	selectEditItem,
	selectLockedBy,
	selectLockStatus,
	selectSearchTerm,
	selectShoePropertiesPanel,
	selectTableIndexForPosition,
} from './canvas.selectors'
import { actions } from './canvas.slice'
import { actions as schemaActions } from '../schema/schema.slice'
import {
	DbCanvas,
	DbCanvasItemLinkProps,
	DbCanvasItemProps,
} from '../components/DBCanvas/DBCanvas'
import { DatabaseLockStatus } from './canvas.types'
import { DBRelationPanel } from '../components/DBRelationPanel/DBRelationPanel'
import { DBTablePanel } from '../components/DBTablePanel/DBTablePanel'
import { DBTable } from '../components/DBTable/DBTable'
import { saveSchema } from '../schema/schema.asyncActions'
import {
	conformTableAttributeDelete,
	conformTableDelete,
	setAttributeKey,
	updateTableAsync,
	updateTableAttributeAsync,
} from '../schema/schema.controller'
import { refreshLockStatus, releaseLockConformation } from './canvas.controller'
import { acquireLock } from './canvas.asyncActions'
import {
	selectCanSchemaRedo,
	selectCanSchemaUndo,
	selectIsSchemaModified,
} from '../schema/schema.selectors'
import { ActionCreators } from 'redux-undo'

export interface CanvasProps {
	tables: DatabaseTableProps[]
	tableIndex: number | null
	attributeIndex: number | null
	attributeTypes: AttributeTypeProps[]
	onSelectedTableIndexAndAttrIndex: (
		tableIndex: number | null,
		attributeIndex: number | null
	) => void
}

export function Canvas(props: CanvasProps) {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const editItem = useSelector(selectEditItem)
	const lockStatus = useSelector(selectLockStatus)
	const lockedBy = useSelector(selectLockedBy)
	const searchTerm = useSelector(selectSearchTerm)
	const showPropertiesPanel = useSelector(selectShoePropertiesPanel)
	const tableIndexForPosition = useSelector(selectTableIndexForPosition)
	const isSchemaModified = useSelector(selectIsSchemaModified)
	const canUndo = useSelector(selectCanSchemaUndo)
	const canRedo = useSelector(selectCanSchemaRedo)

	const dispatch = useDispatch()
	useEffect(() => {
		const listener = (event: MouseEvent) => {
			let eventListener = [
				'MuiPopover-root',
				'database-container',
				'database-container-edit',
				'MuiDialog-root',
				'input-base',
				'select-class',
				'property-panel-toggle-icon',
				'isy-toolbar-button',
				'add-new-table',
				'steps-tooltipReferenceLayer',
				'steps-helperLayer',
				'steps-parts-overlay',
				'standard-btn',
			]
			if (
				!isNil(event.srcElement) &&
				!isParentUntil(event.srcElement as Element, eventListener)
			) {
				dispatch(actions.setEditItem(null))
			}
		}
		document.addEventListener('click', listener)
		return () => {
			document.removeEventListener('click', listener)
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	// empty array is important to say its should happen only on mount.

	const getSelectedTableId = () => {
		if (!isNil(props.tableIndex) && props.tableIndex < props.tables.length) {
			return props.tables[props.tableIndex].id
		}
		return null
	}

	const getTableHeight = (table: DatabaseTableProps) => {
		const attrLength = table.attributes.length > 8 ? 8 : table.attributes.length
		const rowsHeight = attrLength * 31
		const headerHeight = 33
		const buttonHeight = 32 + (attrLength - 1)
		if (editItem === table.id) {
			return rowsHeight + headerHeight + buttonHeight
		}
		return rowsHeight + headerHeight
	}

	const getLinksOfTable = (
		table: DatabaseTableProps
	): DbCanvasItemLinkProps[] => {
		const links: DbCanvasItemLinkProps[] = []
		for (let i = 0, iLen = table.attributes.length; i < iLen; i++) {
			const ref = table.attributes[i].reference
			if (!isNil(ref)) {
				const targetTable = find(
					props.tables,
					(t) => t.name === ref.targetModel
				)
				if (!isNil(targetTable)) {
					const targetAttrIndex = findIndex(
						targetTable.attributes,
						(a) => a.name === ref.column
					)
					const targetTableId = targetTable.id
					links.push({
						targetTableId,
						targetAttrIndex,
						sourceTableId: table.id,
						sourceAttrIndex: i,
						targetTableName: targetTable.name,
						targetAttrName: targetTable.attributes[targetAttrIndex].name,
						sourceTableName: table.name,
						sourceAttrName: table.attributes[i].name,
						association: ref.relation.association,
					})
				}
			}
		}
		return links
	}

	const getAssociationIcon = (
		data: DbCanvasItemLinkProps,
		x: number,
		y: number
	): React.ReactNode => {
		const classes =
			'DbCanvasLine ' + (getIsLinkSelected(data) ? 'DbCanvasLineSelected' : '')
		switch (data.association) {
			case 'OneToOne':
				return (
					<OneToOneIcon
						x={x - 11 + 'px'}
						y={y - 15.5 + 'px'}
						className={classes}
					/>
				)
			case 'OneToMany':
				return (
					<OneToManyIcon
						x={x - 9 + 'px'}
						y={y - 16.3846154 + 'px'}
						className={classes}
					/>
				)
			case 'ManyToOne':
				return (
					<ManyToOneIcon
						x={x - 9 + 'px'}
						y={y - 16.3846154 + 'px'}
						className={classes}
					/>
				)
			case 'ManyToMany':
				return (
					<ManyToManyIcon
						x={x - 9 + 'px'}
						y={y - 16.3846154 + 'px'}
						className={classes}
					/>
				)
			default:
				return null
		}
	}

	const getTooltipForRelation = (data: DbCanvasItemLinkProps) => {
		return (
			data.sourceTableName +
			':' +
			data.sourceAttrName +
			' -> ' +
			data.targetTableName +
			':' +
			data.targetAttrName
		)
	}

	const getIsLinkSelected = (data: DbCanvasItemLinkProps) => {
		return (
			data.sourceTableId === getSelectedTableId() &&
			data.sourceAttrIndex === props.attributeIndex
		)
	}

	const getCanvasTables = () => {
		return props.tables.map((item, tableIndex) => {
			return {
				id: item.id,
				width: 230,
				height: getTableHeight(item),
				x: isNil(item.position) ? (tableIndex + 1) * 300 : item.position.x,
				y: isNil(item.position) ? 20 : item.position.y,
				links: getLinksOfTable(item),
				data: item,
				index: tableIndex,
			}
		})
	}

	/**
	 * Delete attributes from database table when we click delete icon.
	 */
	const handleDeleteAttribute = (tableIndex: number, attrIndex: number) => {
		dispatch(conformTableAttributeDelete({ tableIndex, attrIndex }))
	}

	/**
	 * Add attributes to the database table based on selecting.
	 */
	const handleAddAttribute = (tableIndex: number) => {
		dispatch(schemaActions.addAttribute({ tableIndex }))
	}

	const handleAcquireLock = () => {
		dispatch(acquireLock())
	}

	const handleReleaseLock = () => {
		dispatch(releaseLockConformation())
	}

	const handleRefreshLock = () => {
		dispatch(refreshLockStatus())
	}

	const handleSearchTermChange = (val: string) => {
		dispatch(actions.setSearchTerm(val))
	}

	const handleTableChange = (
		table: Partial<DatabaseTableProps>,
		tableIndex: number
	) => {
		// dispatch update table
		dispatch(updateTableAsync({ table, tableIndex }))
	}

	const onTableAttributeChange = (
		attr: Partial<DatabaseTableAttributeProps>,
		tableIndex: number,
		attributeIndex: number
	) => {
		// dispatch update table attribute
		dispatch(
			updateTableAttributeAsync({
				attr,
				tableIndex,
				attributeIndex,
				attributeTypes: props.attributeTypes,
			})
		)
	}

	const handleSelectTable = (tb: DbCanvasItemProps) => {
		props.onSelectedTableIndexAndAttrIndex(tb.index, null)
	}

	const handleSelectTableIndex = (tableIndex: number) => {
		props.onSelectedTableIndexAndAttrIndex(tableIndex, null)
		setTimeout(() => {
			dispatch(actions.setTableIndexForPosition(null))
		}, 10)
		dispatch(actions.setTableIndexForPosition(tableIndex))
	}

	const handleSelectRelation = (data: DbCanvasItemLinkProps) => {
		const index = findIndex(
			props.tables,
			(table) => table.id === data.sourceTableId
		)
		props.onSelectedTableIndexAndAttrIndex(index, data.sourceAttrIndex)
	}

	const handleCanvasClick = () => {
		dispatch(actions.setEditItem(null))
		props.onSelectedTableIndexAndAttrIndex(null, null)
	}

	const handleSetEditTable = (id: string | null) => {
		dispatch(actions.setEditItem(id))
	}

	const handleAddNewTable = () => {
		const id = v4()
		dispatch(schemaActions.addTable({ id: id }))
		dispatch(actions.setEditItem(id))
		setTimeout(() => {
			dispatch(actions.setTableIndexForPosition(null))
		}, 10)
		dispatch(actions.setTableIndexForPosition(props.tables.length))
	}

	const handleDeleteTable = (tableIndex: number) => {
		dispatch(conformTableDelete({ tableIndex }))
	}

	const handleSetKey = (
		tableIndex: number,
		attributeIndex: number,
		keyType: TableAttributeKeyTypes
	) => {
		dispatch(setAttributeKey({ tableIndex, attributeIndex, keyType }))
	}

	const handleTableReposition = (
		data: DbCanvasItemProps,
		x: number,
		y: number
	) => {
		dispatch(
			schemaActions.updateTablePosition({ tableIndex: data.index, x, y })
		)
	}

	const handleSave = () => {
		dispatch(saveSchema())
	}

	const handleUpdateTableAttributeConstraints = (
		constraints: Partial<DatabaseTableConstraintsProps>,
		tableIndex: number,
		attributeIndex: number
	) => {
		dispatch(
			schemaActions.updateTableAttributeConstraints({
				constraints,
				tableIndex,
				attributeIndex,
			})
		)
	}

	const handleToggleProperties = () => {
		dispatch(actions.toggleShowPropertiesPanel(null))
	}

	const handleDeleteKeyDownInCanvas = () => {
		if (
			!isNil(props.tableIndex) &&
			lockStatus === DatabaseLockStatus.LOCK_ACQUIRED
		) {
			if (!isNil(props.attributeIndex)) {
				onTableAttributeChange(
					{ reference: null },
					props.tableIndex,
					props.attributeIndex
				)
			} else {
				handleDeleteTable(props.tableIndex)
			}
		}
	}

	const handleUndo = () => {
		dispatch(ActionCreators.undo())
	}

	const handleRedo = () => {
		dispatch(ActionCreators.redo())
	}

	const renderToolbarComponent = () => {
		return (
			<DBToolbar
				lockStatus={lockStatus}
				lockedBy={lockedBy}
				onAddNewTable={handleAddNewTable}
				tables={props.tables}
				isSchemaModified={isSchemaModified}
				searchTerm={searchTerm}
				canUndo={canUndo}
				canRedo={canRedo}
				onSave={handleSave}
				onTableSelect={handleSelectTableIndex}
				onAcquireLock={handleAcquireLock}
				onReleaseLock={handleReleaseLock}
				onRefreshLock={handleRefreshLock}
				onSearchTermChange={handleSearchTermChange}
				onUndo={handleUndo}
				onRedo={handleRedo}
			/>
		)
	}

	const renderTableComponent = (table: DbCanvasItemProps, index: number) => {
		return (
			<foreignObject
				width={table.width + 'px'}
				height={table.height + 'px'}
				x={'0px'}
				y={'0px'}
			>
				<DBTable
					table={table.data}
					tableIndex={index}
					isSelected={index === props.tableIndex && isNil(props.attributeIndex)}
					isLastIndex={props.tables.length - 1 === index}
					attributeTypes={props.attributeTypes}
					isEditMode={editItem === table.id}
					isEditable={lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					isLimitedView={false}
					setEditMode={handleSetEditTable}
					onChange={handleTableChange}
					onAttrChange={onTableAttributeChange}
					onAddAttr={handleAddAttribute}
					onDeleteAttr={handleDeleteAttribute}
					onDelete={handleDeleteTable}
					onSetKey={handleSetKey}
				/>
			</foreignObject>
		)
	}

	const renderCanvas = () => {
		return (
			<div className='DbCanvasParentContainer'>
				<DbCanvas
					list={getCanvasTables()}
					selected={getSelectedTableId()}
					canReposition={lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					positionCenterToRef={tableIndexForPosition}
					onSelectNode={handleSelectTable}
					onSelectLink={handleSelectRelation}
					onCanvasClick={handleCanvasClick}
					onReposition={handleTableReposition}
					onRenderItem={renderTableComponent}
					getLinkTooltip={getTooltipForRelation}
					getIsLinkSelected={getIsLinkSelected}
					getLineIcon={getAssociationIcon}
					onDeleteKeyDown={handleDeleteKeyDownInCanvas}
				/>
			</div>
		)
	}

	const renderRelationProperties = () => {
		const tableIndex = props.tableIndex
		const attributeIndex = props.attributeIndex
		if (!isNil(tableIndex) && !isNil(attributeIndex)) {
			return (
				<DBRelationPanel
					tables={props.tables}
					isEditable={lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					tableIndex={tableIndex}
					attributeIndex={attributeIndex}
					onChange={(data: Partial<DatabaseTableAttributeProps>) =>
						onTableAttributeChange(data, tableIndex, attributeIndex)
					}
				/>
			)
		}
		return null
	}

	const renderTableProperties = () => {
		const tableIndex = props.tableIndex
		if (!isNil(tableIndex)) {
			return (
				<DBTablePanel
					table={props.tables[tableIndex]}
					attributeTypes={props.attributeTypes}
					isEditable={lockStatus === DatabaseLockStatus.LOCK_ACQUIRED}
					onTableDelete={() => handleDeleteTable(tableIndex)}
					onTableChange={(data: Partial<DatabaseTableProps>) => {
						handleTableChange(data, tableIndex)
					}}
					onAttributeChange={(
						data: Partial<DatabaseTableAttributeProps>,
						attrIndex: number
					) => onTableAttributeChange(data, tableIndex, attrIndex)}
					onAddAttribute={() => handleAddAttribute(tableIndex)}
					onAttributeConstraintChange={(
						d: Partial<DatabaseTableConstraintsProps>,
						attrIndex: number
					) => handleUpdateTableAttributeConstraints(d, tableIndex, attrIndex)}
					onDeleteTableAttribute={(attrIndex: number) =>
						handleDeleteAttribute(tableIndex, attrIndex)
					}
					onAttributeKeyChange={(
						key: TableAttributeKeyTypes,
						attrIndex: number
					) => handleSetKey(tableIndex, attrIndex, key)}
				/>
			)
		}
		return null
	}

	const renderEmptyProperties = () => {
		return (
			<div className='empty-properties'>
				<div className='icon-holder'>
					<SettingsOutlinedIcon className='icon' />
				</div>
				<div className='text'>{t(PROPERTIES_EMPTY_TEXT)}</div>
			</div>
		)
	}

	const renderProperties = () => {
		if (isNil(props.tableIndex)) {
			return renderEmptyProperties()
		}
		if (!isNil(props.attributeIndex)) {
			return renderRelationProperties()
		}
		return renderTableProperties()
	}

	const renderMainArea = () => {
		return (
			<div
				className={
					showPropertiesPanel ? 'tables-main-view-props' : 'tables-main-view'
				}
			>
				{renderCanvas()}
			</div>
		)
	}

	const renderPropertiesToggleIcon = () => {
		return (
			<div
				className='property-panel-toggle-icon'
				onClick={handleToggleProperties}
				title={showPropertiesPanel ? t(COLLAPSE) : t(EXPAND)}
			>
				{showPropertiesPanel ? (
					<LastPageIcon className='icon' />
				) : (
					<FirstPageIcon className='icon' />
				)}
			</div>
		)
	}

	const renderTablesView = () => {
		return (
			<div className='table-view'>
				{renderToolbarComponent()}
				<div className='canvas-properties-container'>
					{renderMainArea()}
					{showPropertiesPanel && renderProperties()}
					{renderPropertiesToggleIcon()}
				</div>
			</div>
		)
	}

	const renderView = () => {
		return renderTablesView()
	}

	return renderView()
}
