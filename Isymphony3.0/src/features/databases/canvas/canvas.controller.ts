import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import { selectGetLoginUserId } from '../../../authAndPermissions/loginUserDetails.selectors'
import { RootState } from '../../../base.types'
import {
	selectAppId,
	selectDatabaseId,
	selectSchemaId,
} from '../databases.selectors'
import {
	selectIsSchemaModified,
	selectOldData,
} from '../schema/schema.selectors'
import { checkLockStatus, releaseLock } from './canvas.asyncActions'
import i18n from 'i18next'
import { actions as schemaActions } from '../schema/schema.slice'
import { actions as notificationActions } from '../../../infrastructure/notificationPopup/notificationPopup.slice'
import {
	BUTTON_CONTINUE,
	BUTTON_DISCARD,
	BUTTON_SAVE,
	COMMON_BUTTON_CLOSE,
	COMMON_CONFORMATION_UPDATE_MESSAGE_LOCK,
	COMMON_EXIT_LOCK,
	DATABASE_DISCARD_MESSAGE,
	DATABASE_EXIT_MESSAGE_LOCK_WITH_OUT_CHANGES,
	DATABASE_UPDATE_MESSAGE,
} from '../../../utilities/constants'
import { DialogTypes, showDialog } from '../../dialogs'
import { saveSchema } from '../schema/schema.asyncActions'
import { selectLockStatus } from './canvas.selectors'
import { DatabaseLockStatus } from './canvas.types'
import { ActionCreators } from 'redux-undo'

export const refreshLockStatus = createAsyncThunk(
	'databases/canvas/refreshLockStatus',
	async (_: undefined, thunkArgs) => {
		thunkArgs.dispatch(
			checkLockStatus(selectGetLoginUserId(thunkArgs.getState() as RootState))
		)
	}
)

export const releaseLockConformation = createAsyncThunk(
	'databases/canvas/releaseLockConformation',
	async (_: undefined, { dispatch, getState }) => {
		const appId = selectAppId(getState() as RootState)
		const databaseId = selectDatabaseId(getState() as RootState)
		const schemaId = selectSchemaId(getState() as RootState)
		if (isNil(appId) || isNil(databaseId) || isNil(schemaId)) {
			return
		}
		const name = (selectOldData(getState() as RootState) || {}).name
		const isDBModified = selectIsSchemaModified(getState() as RootState)
		if (isDBModified) {
			const conformationCallBack = () => {
				dispatch(saveSchema()).then(dispatch(releaseLock()) as any)
			}
			const handleOnClose = () => {
				dispatch(
					notificationActions.notifySuccess(
						i18n.t(DATABASE_DISCARD_MESSAGE, { name: name })
					)
				)
				dispatch(schemaActions.resetTablesToOld(null))
				dispatch(ActionCreators.clearHistory())
				dispatch(releaseLock())
			}
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(COMMON_CONFORMATION_UPDATE_MESSAGE_LOCK, {
							name: name,
						}),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		} else {
			dispatch(releaseLock())
		}
	}
)

export const switchConformation = createAsyncThunk(
	'databases/canvas/switchConformation',
	async (okayCallback: () => void, { dispatch, getState }) => {
		const isDBModified = selectIsSchemaModified(getState() as RootState)
		if (!isDBModified) {
			const lockStatus = selectLockStatus(getState() as RootState)
			if (lockStatus === DatabaseLockStatus.LOCK_ACQUIRED) {
				const conformationCallBack = () => {
					dispatch(releaseLock()).then(() => {
						okayCallback()
					})
				}
				dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						title: i18n.t(COMMON_EXIT_LOCK),
						onOkay: conformationCallBack,
						data: {
							message: i18n.t(DATABASE_EXIT_MESSAGE_LOCK_WITH_OUT_CHANGES),
							okayButtonLabel: i18n.t(COMMON_BUTTON_CLOSE),
							cancelButtonLabel: i18n.t(BUTTON_CONTINUE),
						},
					})
				)
			} else {
				okayCallback()
			}
		} else {
			const conformationCallBack = () => {
				dispatch(saveSchema()).then(() => {
					dispatch(releaseLock()).then(() => {
						okayCallback()
					})
				})
			}
			const handleOnClose = () => {
				dispatch(schemaActions.resetTablesToOld(null))
				dispatch(releaseLock()).then(() => {
					okayCallback()
				})
			}
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(DATABASE_UPDATE_MESSAGE),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		}
	}
)
