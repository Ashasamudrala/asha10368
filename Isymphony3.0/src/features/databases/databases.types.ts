import { DatabaseAttributeConstraintDataTypes } from '../../utilities/apiEnumConstants'

export interface DatabasesDatabaseSchemaProps {
	id: string
	name: string
}

export interface DatabasesDatabaseProps {
	id: string
	name: string
	schemas: DatabasesDatabaseSchemaProps[]
}

export interface AttributeTypesConstraintProps {
	dataType: DatabaseAttributeConstraintDataTypes
	name: string
}

export interface AttributeTypeProps {
	id: string
	code: string
	value: string
	constraints: AttributeTypesConstraintProps[]
}

export interface DatabasesState {
	appId: string | null
	databases: DatabasesDatabaseProps[]
	attributeTypes: AttributeTypeProps[]
	databaseId: string | null
	schemaId: string | null
}
