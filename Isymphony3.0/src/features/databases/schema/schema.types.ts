import {
	DatabaseContainmentType,
	DatabaseRelationShipType,
} from '../../../utilities/apiEnumConstants'
import { AttributeTypeProps } from '../databases.types'

export enum TableAttributeKeyTypes {
	PRIMARY_KEY = 'PrimaryKey',
	NONE = 'None',
	FOREIGN_KEY = 'ForeignKey',
	UNIQUE_KEY = 'UniqueKey',
}

export interface UpdateTableActionProps {
	tableIndex: number
	table: Partial<DatabaseTableProps>
}

export interface DeleteTableActionProps {
	tableIndex: number
	attributeIndex: number
}

export interface SetTableAttributeForeignKeyActionProps {
	tableIndex: number
	attributeIndex: number
	primaryKeyIndex: number
	targetTableIndex: number
	targetAttrIndex: number
	relationShipType: DatabaseRelationShipType
	containment: DatabaseContainmentType
}

export interface UpdateTableAttributeActionProps {
	tableIndex: number
	attributeIndex: number
	attr: Partial<DatabaseTableAttributeProps>
	attributeTypes: AttributeTypeProps[]
}

export interface DatabaseUpdateTablePositionActionProps
	extends DatabaseTablePositionProps {
	tableIndex: number
}
export interface UpdateTableAttributeConstraintsActionProps {
	tableIndex: number
	attributeIndex: number
	constraints: Partial<DatabaseTableConstraintsProps>
}

export interface DatabaseTablePositionProps {
	x: number
	y: number
}

export interface DatabaseTableReferenceRelationContainmentProps {
	type: DatabaseContainmentType
}

export interface DatabaseTableReferenceRelationProps {
	association: DatabaseRelationShipType
	containment: DatabaseTableReferenceRelationContainmentProps
}

export interface DatabaseTableReferenceProps {
	sourceModel: string
	sourceModelPrimaryKeyColumn: string
	targetModel: string
	column: string
	relation: DatabaseTableReferenceRelationProps
}

export interface DatabaseTableConstraintsProps {
	filter?: string | null
	length?: number | null
	max?: number | null
	min?: number | null
	pattern?: string | null
	primary?: boolean
	required?: boolean
	unique?: boolean
}

export interface DatabaseTableAttributeProps {
	name: string
	description: string
	type: string
	constraints: DatabaseTableConstraintsProps
	reference: DatabaseTableReferenceProps | null
}

export interface DatabaseTableProps {
	id: string
	name: string
	attributes: DatabaseTableAttributeProps[]
	position: DatabaseTablePositionProps
}

export interface DatabaseSchemasProps {
	id: string
	databaseId: string
	name: string
	displayName: string
	description: string
	tables: DatabaseTableProps[]
}

export interface SchemaState {
	tableIndex: number | null
	attributeIndex: number | null
	tables: DatabaseTableProps[]
	oldData: DatabaseSchemasProps | null
}
