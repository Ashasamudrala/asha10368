import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { DatabasesSubReducersNames } from '../base.types'
import * as asyncActions from './schema.asyncActions'
import { cloneDeep, find, isNil, pick } from 'lodash'
import {
	DatabaseSchemasProps,
	DatabaseTableAttributeProps,
	DatabaseTableProps,
	DatabaseTableReferenceProps,
	DatabaseUpdateTablePositionActionProps,
	DeleteTableActionProps,
	SchemaState,
	SetTableAttributeForeignKeyActionProps,
	UpdateTableActionProps,
	UpdateTableAttributeActionProps,
	UpdateTableAttributeConstraintsActionProps,
} from './schema.types'
import newTable from '../../../config/database/newTable.json'
import newAttribute from '../../../config/database/newAttribute.json'
import { getUniqueName } from '../../../utilities/utilities'
import i18n from 'i18next'
import { COLUMN_NAME, UNTITLED_TABLE } from '../../../utilities/constants'
import { AttributeTypesConstraintProps } from '../databases.types'

export const defaultConstraints = {
	min: 10,
	max: 20,
	pattern: '',
	length: 20,
	unique: false,
	required: true,
	primary: false,
}

const initialState: SchemaState = {
	tableIndex: null,
	attributeIndex: null,
	tables: [],
	oldData: null,
}

const slice = createSlice<
	SchemaState,
	SliceCaseReducers<SchemaState>,
	DatabasesSubReducersNames.SCHEMA
>({
	name: DatabasesSubReducersNames.SCHEMA,
	initialState,
	reducers: {
		// synchronous actions
		setTableAndAttributeIndex(
			state: SchemaState,
			action: Action<{
				tableIndex: number | null
				attributeIndex: number | null
			}>
		) {
			state.tableIndex = action.payload.tableIndex
			state.attributeIndex = action.payload.attributeIndex
		},
		resetTablesToOld(state: SchemaState) {
			state.tables = state.oldData ? state.oldData.tables : []
			state.tableIndex = null
			state.attributeIndex = null
		},
		updateTableAttributeConstraints(
			state: SchemaState,
			action: Action<UpdateTableAttributeConstraintsActionProps>
		) {
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attributeIndex
			const data = action.payload.constraints

			const tables = [...state.tables]
			const attributes = [...tables[tableIndex].attributes]

			if (data.primary) {
				// if primary key is true need to remove other primary keys in table
				let primaryKeyName = null
				for (let i = 0, iLen = attributes.length; i < iLen; i++) {
					if (attributes[i].constraints.primary) {
						primaryKeyName = attributes[i].name
						attributes[i] = Object.assign({}, attributes[i], {
							constraints: Object.assign({}, attributes[i].constraints, {
								primary: false,
							}),
						})
					}
				}
				if (!isNil(primaryKeyName)) {
					// need to remove foreign relations for all the tables
					// need to update the primary
					const tableName = tables[tableIndex].name
					for (let i = 0, iLen = tables.length; i < iLen; i++) {
						for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
							const ref = tables[i].attributes[j].reference
							if (!isNil(ref)) {
								if (
									ref.sourceModel === tableName &&
									ref.sourceModelPrimaryKeyColumn === primaryKeyName
								) {
									// need to update the primary
									const attrs = [...tables[i].attributes]
									const reference = Object.assign({}, attrs[j].reference, {
										sourceModelPrimaryKeyColumn: attributes[attrIndex].name,
									})
									attrs[j] = Object.assign({}, attrs[j], { reference })
									tables[i] = Object.assign({}, tables[i], {
										attributes: attrs,
									})
								}
								if (
									ref.targetModel === tableName &&
									ref.column === primaryKeyName
								) {
									// need to remove foreign relations for all the tables
									const attrs = [...tables[i].attributes]
									attrs[j] = Object.assign({}, attrs[j], { reference: null })
									tables[i] = Object.assign({}, tables[i], {
										attributes: attrs,
									})
								}
							}
						}
					}
				}
			}

			// porting the changes
			const constraints = Object.assign(
				{},
				attributes[attrIndex].constraints,
				data
			)

			const attributeData: Partial<DatabaseTableAttributeProps> = {}
			// setting the reference to null if primary or unique key is trying to set.
			if (!isNil(data.unique) || !isNil(data.primary)) {
				attributeData.reference = null
			}

			attributes[attrIndex] = Object.assign(
				{},
				attributes[attrIndex],
				attributeData,
				{
					constraints,
				}
			)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			state.tables = tables
		},
		updateTablePosition(
			state: SchemaState,
			action: Action<DatabaseUpdateTablePositionActionProps>
		) {
			const tableIndex = action.payload.tableIndex
			const tables = [...state.tables]
			tables[tableIndex] = Object.assign({}, tables[tableIndex], {
				position: { x: action.payload.x, y: action.payload.y },
			})
			state.tables = tables
		},
		setTableAttributeForeignKey(
			state: SchemaState,
			action: Action<SetTableAttributeForeignKeyActionProps>
		) {
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attributeIndex

			const primaryKeyIndex = action.payload.primaryKeyIndex
			const targetTableIndex = action.payload.targetTableIndex
			const targetAttrIndex = action.payload.targetAttrIndex
			const relationShipType = action.payload.relationShipType
			const containment = action.payload.containment

			const tables = [...state.tables]
			const attributes = [...tables[tableIndex].attributes]

			const reference = {
				sourceModel: tables[tableIndex].name,
				sourceModelPrimaryKeyColumn:
					tables[tableIndex].attributes[primaryKeyIndex].name,
				targetModel: tables[targetTableIndex].name,
				column: tables[targetTableIndex].attributes[targetAttrIndex].name,
				relation: {
					association: relationShipType,
					containment: {
						type: containment,
					},
				},
			}

			const constraints = Object.assign({}, attributes[attrIndex].constraints, {
				unique: false,
				primary: false,
			})

			attributes[attrIndex] = Object.assign({}, attributes[attrIndex], {
				reference,
				constraints,
			})

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			state.tables = tables
			state.tableIndex = tableIndex
			state.attributeIndex = attrIndex
		},
		deleteTable(state: SchemaState, action: Action<{ tableIndex: number }>) {
			const tableIndex = action.payload.tableIndex

			const tables = [...state.tables]

			// need to remove relative foreign keys
			const tableName = tables[tableIndex].name
			for (let i = 0, iLen = tables.length; i < iLen; i++) {
				for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
					const ref = tables[i].attributes[j].reference
					if (!isNil(ref) && ref.targetModel === tableName) {
						const attrs = [...tables[i].attributes]
						attrs[j] = Object.assign({}, attrs[j], { reference: null })
						tables[i] = Object.assign({}, tables[i], {
							attributes: attrs,
						})
					}
				}
			}

			tables.splice(tableIndex, 1)
			state.tables = tables
			if (tableIndex === state.tableIndex) {
				state.tableIndex = null
				state.attributeIndex = null
			}
		},
		addTable(state: SchemaState, action: Action<{ id: string }>) {
			const tables = [...state.tables]

			const names = tables.map((table) => table.name)
			const table: DatabaseTableProps = Object.assign({}, cloneDeep(newTable), {
				name: getUniqueName(names, i18n.t(UNTITLED_TABLE), '_'),
				id: action.payload.id,
				position: {
					x: (tables.length + 1) * 300,
					y: 20,
				},
			})
			const attribute: DatabaseTableAttributeProps = JSON.parse(
				JSON.stringify(newAttribute)
			)
			attribute.name = 'id'
			attribute.constraints.primary = true
			table.attributes.push(attribute)
			tables.push(table)

			state.tables = tables
		},
		updateTableAttribute(
			state: SchemaState,
			action: Action<UpdateTableAttributeActionProps>
		) {
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attributeIndex
			const data = action.payload.attr

			const tables = [...state.tables]

			if (tables[tableIndex].attributes[attrIndex].constraints.primary) {
				const tableName = tables[tableIndex].name
				const oldName = tables[tableIndex].attributes[attrIndex].name
				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
						const ref = tables[i].attributes[j].reference
						if (!isNil(ref)) {
							const dataToChange: Partial<DatabaseTableReferenceProps> = {}
							const attrDatToChange: Partial<DatabaseTableAttributeProps> = {}
							if (
								ref.sourceModel === tableName &&
								ref.sourceModelPrimaryKeyColumn === oldName &&
								!isNil(data.name)
							) {
								// on updating the attribute name updating the reference names
								dataToChange.sourceModelPrimaryKeyColumn = data.name
							}
							if (ref.targetModel === tableName && ref.column === oldName) {
								if (!isNil(data.name)) {
									// on updating the attribute name updating the reference names
									dataToChange.column = data.name
								}
								if (!isNil(data.type)) {
									attrDatToChange.type = data.type
								}
							}
							if (
								Object.keys(dataToChange).length > 0 ||
								Object.keys(attrDatToChange).length > 0
							) {
								const attrs = [...tables[i].attributes]
								const reference = Object.assign(
									{},
									attrs[j].reference,
									dataToChange
								)
								attrs[j] = Object.assign({}, attrs[j], {
									reference,
									...attrDatToChange,
								})
								tables[i] = Object.assign({}, tables[i], {
									attributes: attrs,
								})
							}
						}
					}
				}
			}

			const attributes = [...tables[tableIndex].attributes]
			if (!isNil(data.type)) {
				const attributeTypes = action.payload.attributeTypes
				const attribute = find(attributeTypes, (a) => a.value === data.type)
				if (!isNil(attribute)) {
					const pathsAllowed = attribute.constraints.map(
						(c: AttributeTypesConstraintProps) => c.name
					)
					const filteredAdjConstraints = pick(
						attributes[attrIndex].constraints,
						pathsAllowed
					)
					const filteredDefaultConstraints = pick(
						defaultConstraints,
						pathsAllowed
					)
					data.constraints = Object.assign(
						{},
						filteredDefaultConstraints,
						filteredAdjConstraints
					)
				}
			}
			attributes[attrIndex] = Object.assign({}, attributes[attrIndex], data)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			state.tables = tables
			if (isNil(data.reference)) {
				state.tableIndex = null
				state.attributeIndex = null
			}
		},
		updateTable(state: SchemaState, action: Action<UpdateTableActionProps>) {
			const tableIndex = action.payload.tableIndex
			const data = action.payload.table

			const tables = [...state.tables]

			if (!isNil(data.name)) {
				// on updating the table name updating the reference names
				const oldName = tables[tableIndex].name
				const newName = data.name
				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
						const dataToChange: Partial<DatabaseTableReferenceProps> = {}
						const attr = tables[i].attributes[j].reference
						if (!isNil(attr)) {
							if (attr.sourceModel === oldName) {
								dataToChange.sourceModel = newName
							}
							if (attr.targetModel === oldName) {
								dataToChange.targetModel = newName
							}
						}
						if (Object.keys(dataToChange).length > 0) {
							const attributes = [...tables[i].attributes]
							const reference = Object.assign(
								{},
								attributes[j].reference,
								dataToChange
							)
							attributes[j] = Object.assign({}, attributes[j], { reference })
							tables[i] = Object.assign({}, tables[i], {
								attributes,
							})
						}
					}
				}
			}
			tables[tableIndex] = Object.assign({}, tables[tableIndex], data)

			state.tables = tables
		},
		addAttribute(state: SchemaState, action: Action<{ tableIndex: number }>) {
			const tableIndex = action.payload.tableIndex

			const tables = [...state.tables]
			const attributes = [...tables[tableIndex].attributes]

			const attribute = JSON.parse(JSON.stringify(newAttribute))
			const names = attributes.map((attr) => attr.name)
			attribute.name = getUniqueName(names, i18n.t(COLUMN_NAME), '_')
			if (attributes.length === 0) {
				attribute.constraints.primary = true
			}
			attributes.push(attribute)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			state.tables = tables
		},
		deleteTableAttribute(
			state: SchemaState,
			action: Action<DeleteTableActionProps>
		) {
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attributeIndex

			const tables = [...state.tables]
			const attributes = [...tables[tableIndex].attributes]

			attributes.splice(attrIndex, 1)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			state.tables = tables
			if (
				tableIndex === state.tableIndex &&
				attrIndex === state.attributeIndex
			) {
				state.tableIndex = null
				state.attributeIndex = null
			}
		},
		clearData: () => initialState,
	},

	extraReducers: (builder) => {
		builder.addCase(
			asyncActions.loadSelectedSchemaTables.fulfilled,
			(state: SchemaState, action) => {
				if (action.payload) {
					const tables = action.payload[0].tables || []
					const data = action.payload[1].data.tables || {}
					for (let i = 0, iLen = tables.length; i < iLen; i++) {
						if (!isNil(data[tables[i].name])) {
							tables[i] = Object.assign({}, tables[i], {
								position: data[tables[i].name],
							})
						} else {
							tables[i] = Object.assign({}, tables[i], {
								position: {
									x: (i + 1) * 300,
									y: 20,
								},
							})
						}
					}
					state.tables = tables
					state.oldData = { ...action.payload[0], tables }
				}
			}
		)
		builder.addCase(asyncActions.saveSchema.fulfilled, (state, action) => {
			state.oldData = {
				...(state.oldData as DatabaseSchemasProps),
				tables: cloneDeep(state.tables),
			}
		})
	},
})

export default slice

export const { name, actions, reducer } = slice
