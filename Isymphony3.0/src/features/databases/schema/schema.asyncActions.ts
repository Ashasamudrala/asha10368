import { createAsyncThunk } from '@reduxjs/toolkit'
import { isNil, omit, pick } from 'lodash'
import { RootState } from '../../../base.types'
import doAsync from '../../../infrastructure/doAsync'
import {
	GET_SCHEMA_DETAILS,
	SAVE_DATABASE_CANVAS,
} from '../../../utilities/apiEndpoints'
import { DATABASE_SAVE_MESSAGE_SUCCESS } from '../../../utilities/constants'
import {
	selectAppId,
	selectDatabaseId,
	selectSchemaId,
} from '../databases.selectors'
import { selectOldData, selectTables } from './schema.selectors'
import i18n from 'i18next'
import { DatabaseTablePositionProps } from './schema.types'
import { ActionCreators } from 'redux-undo'

export const loadSelectedSchemaTables = createAsyncThunk(
	'databases/schema/loadSelectedSchemaTables',
	async (schemaId: string, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const databaseId = selectDatabaseId(thunkArgs.getState() as RootState)
		if (isNil(appId) || isNil(databaseId)) {
			return
		}
		return await Promise.all([
			doAsync({
				url: GET_SCHEMA_DETAILS(appId, databaseId, schemaId),
				...thunkArgs,
			}),
			doAsync({
				url: SAVE_DATABASE_CANVAS(appId, databaseId, schemaId),
				...thunkArgs,
			}),
		]).then((data) => {
			setTimeout(() => {
				thunkArgs.dispatch(ActionCreators.clearHistory())
			}, 10)
			return data
		})
	}
)

export const saveSchema = createAsyncThunk(
	'databases/schema/saveSchema',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		const databaseId = selectDatabaseId(thunkArgs.getState() as RootState)
		const schemaId = selectSchemaId(thunkArgs.getState() as RootState)
		const oldData = selectOldData(thunkArgs.getState() as RootState)
		const tables = selectTables(thunkArgs.getState() as RootState)
		if (
			isNil(appId) ||
			isNil(databaseId) ||
			isNil(schemaId) ||
			isNil(oldData)
		) {
			return
		}
		const positionData: { [name: string]: DatabaseTablePositionProps } = {}
		for (let i = 0, iLen = tables.length; i < iLen; i++) {
			const t = tables[i]
			positionData[t.name as string] = t.position
		}
		return await Promise.all([
			doAsync({
				url: GET_SCHEMA_DETAILS(appId, databaseId, schemaId),
				httpMethod: 'put',
				httpConfig: {
					body: JSON.stringify({
						...pick(oldData, 'name', 'displayName', 'description'),
						tables: tables.map((t) => omit(t, 'position')),
					}),
				},
				successMessage: i18n.t(DATABASE_SAVE_MESSAGE_SUCCESS, {
					name: oldData.name,
				}),
				...thunkArgs,
			}),
			doAsync({
				url: SAVE_DATABASE_CANVAS(appId, databaseId, schemaId),
				httpMethod: 'put',
				httpConfig: {
					body: JSON.stringify({
						data: { tables: positionData },
					}),
				},
				noBusySpinner: true,
				...thunkArgs,
			}),
		]).then((data) => {
			setTimeout(() => {
				thunkArgs.dispatch(ActionCreators.clearHistory())
			}, 10)
			return data
		})
	}
)
