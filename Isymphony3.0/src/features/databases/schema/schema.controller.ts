import { createAsyncThunk } from '@reduxjs/toolkit'
import { findIndex, isEmpty, isNil } from 'lodash'
import { RootState } from '../../../base.types'
import { DialogTypes, showDialog } from '../../dialogs'
import { selectTables } from './schema.selectors'
import {
	TableAttributeKeyTypes,
	UpdateTableActionProps,
	UpdateTableAttributeActionProps,
} from './schema.types'
import i18n from 'i18next'
import {
	BUTTON_CANCEL,
	CONFIRM_MESSAGE_CHANGING_TARGET_CONTAINMENT,
	CONFIRM_MESSAGE_CHANGING_TARGET_MODEL,
	CONFIRM_MESSAGE_CHANGING_TARGET_RELATIONSHIP_TYPE,
	CONFIRM_MESSAGE_DELETE_ATTRIBUTE,
	CONFIRM_MESSAGE_DELETE_TABLE,
	DELETE,
	DUPLICATE_COLUMN_NAME,
	DUPLICATE_COLUMN_NAME_MESSAGE,
	DUPLICATE_TABLE_NAME,
	DUPLICATE_TABLE_NAME_MESSAGE,
	EMPTY_COLUMN_NAME,
	EMPTY_COLUMN_NAME_MESSAGE,
	EMPTY_TABLE_NAME,
	EMPTY_TABLE_NAME_MESSAGE,
	FOREIGN_KEY_CHANGED_TO_NONE_CONFORMATION,
	FOREIGN_KEY_NO_PRIMARY_KEY,
	FOREIGN_KEY_UPDATE_TYPE_CONFIRMATION,
	INVALID_COLUMN_NAME,
	INVALID_COLUMN_NAME_MESSAGE,
	INVALID_TABLE_NAME,
	INVALID_TABLE_NAME_MESSAGE,
	PRIMARY_KET_DELETE_ERROR,
	PRIMARY_KEY_CHANGED_TO_OTHER,
	PRIMARY_KEY_UPDATE_ERROR,
	PRIMARY_KEY_UPDATE_TYPE,
	PRIMARY_KEY_UPDATE_TYPE_ERROR,
	UNIQUE_KEY_CHANGED_TO_NONE_CONFORMATION,
	UNIQUE_KEY_UPDATE_TYPE_CONFIRMATION,
} from '../../../utilities/constants'
import { actions } from './schema.slice'
import {
	ConnectForeignKeyDataProps,
	ConnectForeignKeyReturnProps,
} from '../../dialogs/connectForeignKey/connectForeignKey.types'
import { selectDbConstraintsByAttributeType } from '../databases.selectors'
import { loadSelectedSchemaTables } from './schema.asyncActions'
import { ActionCreators } from 'redux-undo'
const nameRegex = /^[a-zA-Z_]+[a-zA-Z0-9_#@$ ]*$/i

export const loadSelectedSchema = createAsyncThunk(
	'databases/schema/loadSelectedSchema',
	async (schemaId: string, thunkArgs) => {
		thunkArgs.dispatch(actions.clearData(null))
		thunkArgs.dispatch(ActionCreators.clearHistory())
		thunkArgs.dispatch(loadSelectedSchemaTables(schemaId))
	}
)

const setAttributeForeignKey = createAsyncThunk(
	'databases/schema/setAttributeForeignKey',
	async (
		data: {
			tableIndex: number
			attributeIndex: number
		},
		{ dispatch, getState }
	) => {
		const tables = selectTables(getState() as RootState)
		const primaryKeyIndex: number = findIndex(
			tables[data.tableIndex].attributes,
			(attr) => attr.constraints.primary || false
		)
		if (primaryKeyIndex === -1) {
			return await dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(FOREIGN_KEY_NO_PRIMARY_KEY),
					},
				})
			)
		} else {
			const okayCallBack = (result: ConnectForeignKeyReturnProps) => {
				dispatch(
					actions.setTableAttributeForeignKey({
						...result,
						...data,
						primaryKeyIndex,
					})
				)
			}
			return await dispatch(
				showDialog({
					type: DialogTypes.CONNECT_FOREIGN_KEY,
					className: 'isy-step-connect-foreign-key',
					onOkay: okayCallBack,
					data: {
						tablesData: tables,
						tableIndex: data.tableIndex,
						attrIndex: data.attributeIndex,
					} as ConnectForeignKeyDataProps,
				})
			)
		}
	}
)

export const setAttributeKey = createAsyncThunk(
	'databases/schema/setAttributeKey',
	async (
		data: {
			keyType: TableAttributeKeyTypes
			tableIndex: number
			attributeIndex: number
		},
		{ dispatch, getState }
	) => {
		const key = data.keyType
		const tables = selectTables(getState() as RootState)
		const attributes = tables[data.tableIndex].attributes
		const oldPrimaryKey = findIndex(
			attributes,
			(a) => a.constraints.primary || false
		)
		if (
			oldPrimaryKey === data.attributeIndex &&
			key !== TableAttributeKeyTypes.PRIMARY_KEY
		) {
			return await dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(PRIMARY_KEY_UPDATE_ERROR),
					},
				})
			)
		}

		switch (key) {
			case TableAttributeKeyTypes.NONE: {
				const okayFunction = () => {
					dispatch(
						actions.updateTableAttributeConstraints({
							tableIndex: data.tableIndex,
							attributeIndex: data.attributeIndex,
							constraints: {
								unique: false,
								primary: false,
							},
						})
					)
				}
				const attribute = attributes[data.attributeIndex]
				if (attribute.constraints.unique) {
					// has unique key
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(UNIQUE_KEY_CHANGED_TO_NONE_CONFORMATION),
							},
						})
					)
				}
				if (!isNil(attribute.reference)) {
					// has foreign key
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(FOREIGN_KEY_CHANGED_TO_NONE_CONFORMATION, {
									columnName: attribute.name,
								}),
								okayButtonLabel: i18n.t(DELETE),
								cancelButtonLabel: i18n.t(BUTTON_CANCEL),
							},
						})
					)
				}
				return okayFunction()
			}
			case TableAttributeKeyTypes.PRIMARY_KEY:
				if (oldPrimaryKey !== data.attributeIndex) {
					const okayFunction = () =>
						dispatch(
							actions.updateTableAttributeConstraints({
								tableIndex: data.tableIndex,
								attrIndex: data.attributeIndex,
								constraints: {
									unique: false,
									primary: true,
									required: true,
								},
							})
						)
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(PRIMARY_KEY_CHANGED_TO_OTHER),
							},
						})
					)
				}
				break
			case TableAttributeKeyTypes.UNIQUE_KEY:
				return dispatch(
					actions.updateTableAttributeConstraints({
						tableIndex: data.tableIndex,
						attrIndex: data.attributeIndex,
						constraints: {
							unique: true,
							primary: false,
						},
					})
				)
			case TableAttributeKeyTypes.FOREIGN_KEY:
				return dispatch(
					setAttributeForeignKey({
						tableIndex: data.tableIndex,
						attributeIndex: data.attributeIndex,
					})
				)
			default:
		}
	}
)

export const conformTableDelete = createAsyncThunk(
	'databases/schema/confirmTableDelete',
	async (data: { tableIndex: number }, { dispatch, getState }) => {
		const tables = selectTables(getState() as RootState)
		const tableName = tables[data.tableIndex].name
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => {
					dispatch(actions.deleteTable(data))
				},
				data: {
					message: i18n.t(CONFIRM_MESSAGE_DELETE_TABLE, { name: tableName }),
				},
			})
		)
	}
)

export const updateTableAttributeAsync = createAsyncThunk(
	'databases/schema/updateTableAttributeAsync',
	async (data: UpdateTableAttributeActionProps, { dispatch, getState }) => {
		const tables = selectTables(getState() as RootState)
		const attribute = tables[data.tableIndex].attributes[data.attributeIndex]
		const okayFunction = () => dispatch(actions.updateTableAttribute(data))

		if (!isNil(data.attr.name)) {
			if (isEmpty(data.attr.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(EMPTY_COLUMN_NAME),
						data: {
							message: i18n.t(EMPTY_COLUMN_NAME_MESSAGE),
						},
					})
				)
			}
			if (!nameRegex.test(data.attr.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(INVALID_COLUMN_NAME),
						data: {
							message: i18n.t(INVALID_COLUMN_NAME_MESSAGE),
						},
					})
				)
			}
			const sameNameIndex = findIndex(
				tables[data.tableIndex].attributes,
				(a) => a.name === data.attr.name
			)
			if (sameNameIndex !== -1 && sameNameIndex !== data.attributeIndex) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(DUPLICATE_COLUMN_NAME),
						data: {
							message: i18n.t(DUPLICATE_COLUMN_NAME_MESSAGE),
						},
					})
				)
			}
		}

		if (!isNil(data.attr.type)) {
			const constraintsConfig = selectDbConstraintsByAttributeType(
				getState() as RootState,
				data.attr.type
			)
			let hasPrimary = false
			let hasUnique = false
			for (let i = 0, iLen = constraintsConfig.length; i < iLen; i++) {
				if (constraintsConfig[i].name === 'primary') {
					hasPrimary = true
				} else if (constraintsConfig[i].name === 'unique') {
					hasUnique = true
				}
			}

			if (attribute.constraints.primary) {
				if (!hasPrimary) {
					return await dispatch(
						showDialog({
							type: DialogTypes.ALERT,
							data: {
								message: i18n.t(PRIMARY_KEY_UPDATE_TYPE_ERROR),
							},
						})
					)
				}
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(PRIMARY_KEY_UPDATE_TYPE),
						},
					})
				)
			}
			if (attribute.constraints.unique) {
				if (!hasUnique) {
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: () => {
								dispatch(
									actions.updateTableAttributeConstraints({
										tableIndex: data.tableIndex,
										attrIndex: data.attributeIndex,
										constraints: {
											unique: false,
										},
									})
								)
								okayFunction()
							},
							data: {
								message: i18n.t(UNIQUE_KEY_UPDATE_TYPE_CONFIRMATION),
							},
						})
					)
				}
			}
			if (!isNil(attribute.reference)) {
				if (!hasPrimary) {
					return await dispatch(
						showDialog({
							type: DialogTypes.CONFIRMATION_DIALOG,
							onOkay: () => {
								data.attr.reference = null
								okayFunction()
							},
							data: {
								message: i18n.t(FOREIGN_KEY_UPDATE_TYPE_CONFIRMATION),
							},
						})
					)
				}
			}
		}

		if (isNil(data.attr.reference) && !isNil(attribute.reference)) {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: okayFunction,
					data: {
						message: i18n.t(FOREIGN_KEY_CHANGED_TO_NONE_CONFORMATION, {
							columnName: attribute.name,
						}),
						okayButtonLabel: i18n.t(DELETE),
						cancelButtonLabel: i18n.t(BUTTON_CANCEL),
					},
				})
			)
		}

		if (!isNil(attribute.reference) && !isNil(data.attr.reference)) {
			if (data.attr.reference.targetModel !== attribute.reference.targetModel) {
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(CONFIRM_MESSAGE_CHANGING_TARGET_MODEL, {
								targetTableName: data.attr.reference.targetModel,
							}),
						},
					})
				)
			}
			if (
				data.attr.reference.relation.association !==
				attribute.reference.relation.association
			) {
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(
								CONFIRM_MESSAGE_CHANGING_TARGET_RELATIONSHIP_TYPE,
								{
									targetRelationShipType:
										data.attr.reference.relation.association,
								}
							),
						},
					})
				)
			}
			if (
				data.attr.reference.relation.containment.type !==
				attribute.reference.relation.containment.type
			) {
				return await dispatch(
					showDialog({
						type: DialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(CONFIRM_MESSAGE_CHANGING_TARGET_CONTAINMENT, {
								targetContainment:
									data.attr.reference.relation.containment.type,
							}),
						},
					})
				)
			}
		}
		okayFunction()
	}
)

export const updateTableAsync = createAsyncThunk(
	'databases/schema/updateTableAsync',
	async (data: UpdateTableActionProps, { dispatch, getState }) => {
		const tables = selectTables(getState() as RootState)
		if (!isNil(data.table.name)) {
			if (isEmpty(data.table.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(EMPTY_TABLE_NAME),
						data: {
							message: i18n.t(EMPTY_TABLE_NAME_MESSAGE),
						},
					})
				)
			}
			if (!nameRegex.test(data.table.name)) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(INVALID_TABLE_NAME),
						data: {
							message: i18n.t(INVALID_TABLE_NAME_MESSAGE),
						},
					})
				)
			}
			const sameNameIndex = findIndex(tables, (t) => t.name === data.table.name)
			if (sameNameIndex !== -1 && sameNameIndex !== data.tableIndex) {
				return await dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						title: i18n.t(DUPLICATE_TABLE_NAME),
						data: {
							message: i18n.t(DUPLICATE_TABLE_NAME_MESSAGE),
						},
					})
				)
			}
		}
		dispatch(actions.updateTable(data))
	}
)

export const conformTableAttributeDelete = createAsyncThunk(
	'databases/schema/confirmTableAttributeDelete',
	async (
		data: { tableIndex: number; attrIndex: number },
		{ dispatch, getState }
	) => {
		const tables = selectTables(getState() as RootState)
		const attr = tables[data.tableIndex].attributes[data.attrIndex]
		if (attr.constraints.primary) {
			return await dispatch(
				showDialog({
					type: DialogTypes.ALERT,
					data: {
						message: i18n.t(PRIMARY_KET_DELETE_ERROR),
					},
				})
			)
		}
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => dispatch(actions.deleteTableAttribute(data)),
				data: {
					message: i18n.t(CONFIRM_MESSAGE_DELETE_ATTRIBUTE, {
						name: attr.name,
					}),
				},
			})
		)
	}
)
