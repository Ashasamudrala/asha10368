import { differenceWith, isEmpty, isEqual, isNil } from 'lodash'
import { RootState } from '../../../base.types'
import { selectSlice as baseSelector } from '../base.selector'
import { DatabasesSubReducersNames } from '../base.types'
import {
	DatabaseSchemasProps,
	DatabaseTableProps,
	SchemaState,
} from './schema.types'

export const selectSlice = (state: RootState): SchemaState =>
	baseSelector(state)[DatabasesSubReducersNames.SCHEMA].present

export const selectTables = (state: RootState): DatabaseTableProps[] => {
	return selectSlice(state).tables
}

export const selectTableIndex = (state: RootState): number | null => {
	return selectSlice(state).tableIndex
}

export const selectAttributeIndex = (state: RootState): number | null => {
	return selectSlice(state).attributeIndex
}

export const selectOldData = (
	state: RootState
): DatabaseSchemasProps | null => {
	return selectSlice(state).oldData
}

export const selectIsSchemaModified = (state: RootState): boolean => {
	const tables = selectTables(state)
	const oldData = selectOldData(state)
	if (!isNil(oldData)) {
		return !isEmpty(differenceWith(tables, oldData.tables, isEqual))
	}
	return false
}

export const selectCanSchemaUndo = (state: RootState): boolean =>
	baseSelector(state)[DatabasesSubReducersNames.SCHEMA].past.length > 0

export const selectCanSchemaRedo = (state: RootState): boolean =>
	baseSelector(state)[DatabasesSubReducersNames.SCHEMA].future.length > 0
