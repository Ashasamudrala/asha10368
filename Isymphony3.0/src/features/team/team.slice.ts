import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { cloneDeep, findIndex, isBoolean } from 'lodash'
import { Action } from '../../common.types'
import { TeamsSubReducersNames } from './base.types'
import * as asyncActions from './team.asyncActions'
import { TeamActiveTabs, TeamState } from './team.types'

const initialState: TeamState = {
	activeTab: TeamActiveTabs.ACTIVE,
	userRoles: [],
	searchString: '',
	list: [],
	loading: false,
	totalCount: 0,
	recordsPerPage: 12,
	currentPage: 0,
}

const slice = createSlice<
	TeamState,
	SliceCaseReducers<TeamState>,
	TeamsSubReducersNames.TEAM
>({
	name: TeamsSubReducersNames.TEAM,
	initialState,
	reducers: {
		setActiveTab(state, action: Action<TeamActiveTabs>) {
			state.activeTab = action.payload
			state.currentPage = 0
			state.searchString = ''
			state.list = []
		},
		setSearchString(state, action: Action<string>) {
			state.searchString = action.payload
			state.currentPage = 0
			state.loading = true
		},
		setRecordsPerPage(state, action: Action<number>) {
			state.recordsPerPage = action.payload
			state.currentPage = 0
			state.loading = true
		},
		setCurrentPage(state, action: Action<number>) {
			state.currentPage = action.payload
			state.loading = true
		},
		setLoading(state, action: Action<boolean>) {
			state.loading = action.payload
		},
		toggleSelectedId(state, action: Action<string>) {
			const id = action.payload
			const list = state.list
			list.forEach((item) => {
				if (item.id === id) {
					item.checked = isBoolean(item.checked) ? !item.checked : true
				}
			})
			state.list = [...list]
		},
		setSelectAll(state, action: Action<boolean>) {
			const list = state.list
			list.forEach((item) => {
				if (action.payload) {
					item.checked = true
				} else {
					item.checked = false
				}
			})
			state.list = cloneDeep(list)
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		builder.addCase(asyncActions.fetchUserRoles.fulfilled, (state, action) => {
			if (action.payload) {
				state.userRoles = action.payload.content
			}
		})
		builder.addCase(asyncActions.changeRole.fulfilled, (state, action) => {
			if (action.payload) {
				const id = action.payload.id
				const index = findIndex(state.list, (i) => i.id === id)
				if (index !== -1) {
					const list = state.list
					list[index] = action.payload
					state.list = [...list]
				}
			}
		})

		builder.addCase(
			asyncActions.fetchTeamsRecords.fulfilled,
			(state, action) => {
				if (action.payload) {
					state.list = action.payload.content
					state.totalCount = action.payload.totalElements
				}
				state.loading = false
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
