import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
	TEAM_TRANSLATIONS,
	NAME,
	EMAIL,
	ROLE,
	NO_USERS_FOUND,
} from '../../../utilities/constants'
import { IsyGrid, IsyGridColumnTypes } from '../../../widgets/IsyGrid/IsyGrid'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import { IsyAvatarImage } from '../../../widgets/IsyAvatarImage/IsyAvatarImage'
import { useTranslation } from 'react-i18next'
import { get, isNil } from 'lodash'
import './activemembers.scss'
import { EditProfile } from '../../editProfile/editProfile'
import { actions } from '../team.slice'
import {
	getTeamList,
	getTeamSelectedListLength,
	getUserRoles,
} from '../team.selectors'
import { ActiveMemberProps } from '../team.types'
import { confirmOnDeleteRecords, confirmRoleChange } from '../team.asyncActions'
import { IsyDropDown } from '../../../widgets/IsyDropDown/IsyDropDown'
import {
	IsyCheckbox,
	IsyCheckboxStatus,
} from '../../../widgets/IsyCheckbox/IsyCheckbox'
import { IsyCan } from '../../../widgets/IsyCan/IsyCan'
import { IsyPermissionTypes } from '../../../authAndPermissions/loginUserDetails.types'

export default function ActiveMembers() {
	const list = useSelector(getTeamList) as ActiveMemberProps[]
	const selectedListLength = useSelector(getTeamSelectedListLength)
	const roles = useSelector(getUserRoles)
	const [userId, setUserId] = useState<string | null>(null)

	const dispatch = useDispatch()
	const { t } = useTranslation(TEAM_TRANSLATIONS)

	const gridConfig = () => [
		{
			header: renderHeaderCheckbox,
			dataRef: renderRowCheckbox,
		},
		{
			header: t(NAME),
			dataRef: renderName,
		},
		{
			header: t(EMAIL),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'email',
		},
		{
			header: t(ROLE),
			dataRef: renderRolesCan,
		},
		{
			header: '',
			dataRef: renderDeleteIconCan,
		},
	]

	const getSelectAllStatus = () => {
		if (selectedListLength === 0) {
			return IsyCheckboxStatus.UNCHECKED
		}
		if (selectedListLength === list.length) {
			return IsyCheckboxStatus.CHECKED
		}
		return IsyCheckboxStatus.INTERMEDIATE
	}

	const getRowSelectedClassName = (row: ActiveMemberProps) => {
		return row.checked ? 'selected' : ''
	}

	const handleSelectAllClick = (value: boolean) => {
		dispatch(actions.setSelectAll(value))
	}

	const handleCheckItem = (item: ActiveMemberProps) => {
		dispatch(actions.toggleSelectedId(item.id))
	}

	const handleCancel = () => {
		setUserId(null)
	}

	const handleOnRoleChange = (value: string, item: ActiveMemberProps) => {
		dispatch(confirmRoleChange({ userId: item.id, roleId: value }))
	}

	const handleDeleteAction = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>,
		item: ActiveMemberProps
	) => {
		e.stopPropagation()
		dispatch(
			confirmOnDeleteRecords({
				id: item.id,
				name: item.firstName + ' ' + item.lastName,
			})
		)
	}

	const handleViewItem = (item: ActiveMemberProps) => {
		setUserId(item.id)
	}

	const renderRoleDropdown = (item: ActiveMemberProps) => {
		const role = get(item, 'roles[0]') || {}
		return (
			<IsyDropDown<string>
				onChange={(value: string) => handleOnRoleChange(value, item)}
				options={roles}
				value={role.id}
			/>
		)
	}

	const renderRolesCan = (item: ActiveMemberProps) => {
		const role = get(item, 'roles[0]') || {}
		return (
			<IsyCan
				action={IsyPermissionTypes.UPDATE_ROLE}
				yes={() => renderRoleDropdown(item)}
				no={() => role.name}
			/>
		)
	}

	const renderDeleteIcon = (item: ActiveMemberProps) => {
		return (
			<DeleteOutlinedIcon
				className='delete-icon'
				onClick={(e: React.MouseEvent<SVGSVGElement, MouseEvent>) =>
					handleDeleteAction(e, item)
				}
			/>
		)
	}

	const renderDeleteIconCan = (item: ActiveMemberProps) => {
		return (
			<IsyCan
				action={IsyPermissionTypes.CREATE_PLATFORM}
				yes={() => renderDeleteIcon(item)}
			/>
		)
	}

	const renderHeaderCheckbox = () => {
		return (
			<IsyCheckbox
				className={
					'checkbox-class ' + (selectedListLength > 0 ? 'selected' : '')
				}
				value={getSelectAllStatus()}
				onChange={handleSelectAllClick}
			/>
		)
	}

	const renderRowCheckbox = (item: ActiveMemberProps) => {
		return (
			<IsyCheckbox
				className='checkbox-class'
				value={item.checked}
				onChange={() => handleCheckItem(item)}
			/>
		)
	}

	const renderName = (item: ActiveMemberProps) => {
		const firstName = item.firstName || ''
		const lastName = item.lastName || ''
		const defaultName = firstName + ' ' + lastName
		return (
			<div className='parent-icon'>
				<IsyAvatarImage
					imageClassName={'small'}
					imageData={item.profilePic}
					defaultName={defaultName}
				/>
				<div className='icon-name'>
					{firstName} {lastName}
				</div>
			</div>
		)
	}

	const renderGrid = () => {
		return (
			<IsyGrid
				data={list}
				config={gridConfig()}
				emptyMessage={t(NO_USERS_FOUND)}
				rowCustomClass={getRowSelectedClassName}
				onRowClick={(data) => handleViewItem(data)}
			/>
		)
	}

	return (
		<>
			<div className='active-content'> {renderGrid()} </div>
			{!isNil(userId) && <EditProfile onClose={handleCancel} userId={userId} />}
		</>
	)
}
