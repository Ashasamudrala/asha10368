import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import './pendingInvites.scss'
import { useTranslation } from 'react-i18next'
import _ from 'lodash'
import {
	TEAM_TRANSLATIONS,
	NO_USERS_FOUND,
	EMAIL,
	ROLE,
} from '../../../utilities/constants'
import { actions } from '../team.slice'
import { IsyGrid, IsyGridColumnTypes } from '../../../widgets/IsyGrid/IsyGrid'
import {
	getTeamList,
	getTeamSelectedListLength,
	getUserRoles,
} from '../team.selectors'
import { PendingInviteProps } from '../team.types'
import { confirmOnDeleteRecords, confirmRoleChange } from '../team.asyncActions'
import { IsyDropDown } from '../../../widgets/IsyDropDown/IsyDropDown'
import {
	IsyCheckbox,
	IsyCheckboxStatus,
} from '../../../widgets/IsyCheckbox/IsyCheckbox'
import { IsyCan } from '../../../widgets/IsyCan/IsyCan'
import { IsyPermissionTypes } from '../../../authAndPermissions/loginUserDetails.types'

export default function PendingInvites() {
	const list = useSelector(getTeamList) as PendingInviteProps[]
	const selectedListLength = useSelector(getTeamSelectedListLength)
	const roles = useSelector(getUserRoles)
	const { t } = useTranslation(TEAM_TRANSLATIONS)
	const dispatch = useDispatch()

	const gridConfig = () => [
		{
			header: renderHeaderCheckbox,
			dataRef: renderRowCheckbox,
		},
		{
			header: t(EMAIL),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'member.email',
		},
		{
			header: t(ROLE),
			dataRef: renderRolesCan,
		},
		{
			header: '',
			dataRef: renderDeleteIconCan,
		},
	]

	const getRowSelectedClassName = (row: PendingInviteProps) => {
		return row.checked ? 'selected' : ''
	}

	const getSelectAllStatus = () => {
		if (selectedListLength === 0) {
			return IsyCheckboxStatus.UNCHECKED
		}
		if (selectedListLength === list.length) {
			return IsyCheckboxStatus.CHECKED
		}
		return IsyCheckboxStatus.INTERMEDIATE
	}

	const handleOnRoleChange = (value: string, item: PendingInviteProps) => {
		dispatch(confirmRoleChange({ userId: item.id, roleId: value }))
	}

	const handleDeleteAction = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>,
		item: PendingInviteProps
	) => {
		e.stopPropagation()
		dispatch(
			confirmOnDeleteRecords({
				id: item.id,
				name: item.member.email,
			})
		)
	}

	const handleSelectAllClick = (value: boolean) => {
		dispatch(actions.setSelectAll(value))
	}

	const handleCheckItem = (item: PendingInviteProps) => {
		dispatch(actions.toggleSelectedId(item.id))
	}

	const renderHeaderCheckbox = () => {
		return (
			<IsyCheckbox
				className={
					'checkbox-class ' + (selectedListLength > 0 ? 'selected' : '')
				}
				value={getSelectAllStatus()}
				onChange={handleSelectAllClick}
			/>
		)
	}

	const renderRowCheckbox = (item: PendingInviteProps) => {
		return (
			<IsyCheckbox
				className='checkbox-class'
				value={item.checked}
				onChange={() => handleCheckItem(item)}
			/>
		)
	}

	const renderRoleDropdown = (item: PendingInviteProps) => {
		const role = _.get(item, 'member.role') || {}
		return (
			<IsyDropDown<string>
				onChange={(value: string) => handleOnRoleChange(value, item)}
				options={roles}
				value={role.id}
			/>
		)
	}

	const renderDeleteIcon = (item: PendingInviteProps) => {
		return (
			<DeleteOutlinedIcon
				className='delete-icon'
				onClick={(e) => handleDeleteAction(e, item)}
			/>
		)
	}

	const renderDeleteIconCan = (item: PendingInviteProps) => {
		return (
			<IsyCan
				action={IsyPermissionTypes.CREATE_PLATFORM}
				yes={() => renderDeleteIcon(item)}
			/>
		)
	}

	const renderRolesCan = (item: PendingInviteProps) => {
		const role = _.get(item, 'member.role') || {}
		return (
			<IsyCan
				action={IsyPermissionTypes.UPDATE_ROLE}
				yes={() => renderRoleDropdown(item)}
				no={() => role.name}
			/>
		)
	}

	const renderGrid = () => {
		return (
			<IsyGrid
				data={list}
				config={gridConfig()}
				emptyMessage={t(NO_USERS_FOUND)}
				rowCustomClass={getRowSelectedClassName}
			/>
		)
	}

	return (
		<div className='pendingInvites'>
			<div className='pending-data'>{renderGrid()}</div>
		</div>
	)
}
