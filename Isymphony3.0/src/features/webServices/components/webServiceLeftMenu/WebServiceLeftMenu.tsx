import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { isEmpty } from 'lodash'
import { Tooltip, List, ListItem, ListItemText } from '@material-ui/core'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import SearchIcon from '@material-ui/icons/Search'
import { IsySearch } from '../../../../widgets/IsySearch/IsySearch'
import DeleteOutlined from '@material-ui/icons/DeleteOutlined'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import IsyAccordion from '../../../../widgets/IsyAccordion/IsyAccordion'
import {
	WEB_SERVICE_TRANSLATIONS,
	WEB_SERVICES,
	ADD_WEB_SERVICE,
	NO_SUB_SERVICES_TEXT,
	WEB_SERVICE_SEARCH_WEB_SERVICES,
	WEB_SERVICE_REST_SERVICES,
	WEB_SERVICE_NO_SUB_SERVICES_TEXT_SEARCH,
} from '../../../../utilities/constants'
import './webServiceLeftMenu.scss'
import { WebServiceRestDataMap } from '../../webService.types'

export interface WebServiceLeftMenuProps {
	orderList: string[]
	dataMap: WebServiceRestDataMap
	search: string
	onDelete: (id: string) => void
	onSelect: (id: string) => void
	onSearchChange: (search: string) => void
	onAdd: () => void
}

export function WebServiceLeftMenu(props: WebServiceLeftMenuProps) {
	const { t } = useTranslation(WEB_SERVICE_TRANSLATIONS)
	const [searchEnabled, setSearchEnabled] = useState(false)

	const accordions = [
		{
			id: 'rest-service',
			name: t(WEB_SERVICE_REST_SERVICES),
		},
	]

	const handleSearchClose = () => {
		if (isEmpty(props.search)) {
			setSearchEnabled(false)
		} else {
			props.onSearchChange('')
		}
	}

	const handleDelete = (
		e: React.MouseEvent<SVGSVGElement, MouseEvent>,
		id: string
	) => {
		e.stopPropagation()
		props.onDelete(id)
	}

	const handleSelect = (id: string) => {
		props.onSelect(id)
	}

	const handleAdd = () => {
		props.onAdd()
	}

	const handleSearchChange = (value: string) => {
		props.onSearchChange(value)
	}

	const renderWebServicesSearch = () => {
		setSearchEnabled(true)
	}

	const renderWebServiceSearch = () => {
		return (
			<div className='search-web-service'>
				<IsySearch
					value={props.search}
					onChange={handleSearchChange}
					onCancel={handleSearchClose}
					hideSearchIcon={true}
					placeholder={t(WEB_SERVICE_SEARCH_WEB_SERVICES)}
					autoFocus={true}
				/>
			</div>
		)
	}

	const renderListItem = (id: string) => {
		const item = props.dataMap[id]
		return (
			<ListItem
				button
				onClick={() => handleSelect(item.id)}
				key={item.id}
				className='sub-header'
			>
				<ListItemText
					className='list-item-text'
					primary={item.name}
					title={item.name}
				/>
				<DeleteOutlined
					className='item-delete'
					onClick={(e) => handleDelete(e, item.id)}
				/>
			</ListItem>
		)
	}

	const renderSubWebServices = (_: string) => {
		const list = isEmpty(props.search)
			? props.orderList
			: props.orderList.filter(
					(id) =>
						props.dataMap[id].name
							.toLowerCase()
							.indexOf(props.search.toLowerCase()) !== -1
			  )
		return (
			<List className='sub-headers'>
				{list.length > 0 ? (
					list.map(renderListItem)
				) : (
					<div className='no-sub-service'>
						{t(
							isEmpty(props.search)
								? NO_SUB_SERVICES_TEXT
								: WEB_SERVICE_NO_SUB_SERVICES_TEXT_SEARCH
						)}
					</div>
				)}
			</List>
		)
	}

	const renderList = () => {
		return accordions.map((item) => {
			return (
				<div>
					<IsyAccordion
						key={item.id}
						header={item.name}
						id={item.id}
						isExpandByDefault={true}
					>
						<AccordionDetails className='sub-header-details'>
							{renderSubWebServices(item.id)}
						</AccordionDetails>
					</IsyAccordion>
				</div>
			)
		})
	}

	const renderHeader = () => {
		return (
			<div className='webService-left-menu'>
				<div className='webService-title'>
					<label>{t(WEB_SERVICES)}</label>
					<div className='icons'>
						<SearchIcon
							className='searchIcon'
							onClick={renderWebServicesSearch}
						/>
						<Tooltip title={t(ADD_WEB_SERVICE) as string} onClick={handleAdd}>
							<AddOutlinedIcon className='add-web-service'></AddOutlinedIcon>
						</Tooltip>
					</div>
				</div>
				{searchEnabled && renderWebServiceSearch()}
				{renderList()}
			</div>
		)
	}

	return <>{renderHeader()}</>
}
