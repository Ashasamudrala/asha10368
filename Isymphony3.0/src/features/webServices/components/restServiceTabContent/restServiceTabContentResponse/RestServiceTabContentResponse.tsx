import React from 'react'
import { isEmpty, isNil } from 'lodash'
import { useTranslation } from 'react-i18next'
import {
	WEB_SERVICE_ERROR_SEND,
	WEB_SERVICE_RESPONSE,
	WEB_SERVICE_TRANSLATIONS,
	WEB_SERVICES,
} from '../../../../../utilities/constants'
import {
	WebServiceRestResponseProps,
	WebServiceRestResponseStatus,
	WebServiceRestResponseViewByType,
	WebServiceRestTabProps,
} from '../../../webService.types'
import './restServiceTabContentResponse.scss'
import {
	formatXml,
	getResponseViewByOptions,
	getStatusText,
} from '../restServiceTabContent.utilities'
import { IsyEditor } from '../../../../../widgets/IsyEditor/IsyEditor'
import { IsyTextarea } from '../../../../../widgets/IsyTextarea/IsyTextarea'

export interface RestServiceTabContentResponseProps {
	tabData: WebServiceRestTabProps
	onTabDataChange: (tabData: Partial<WebServiceRestTabProps>) => void
}

export function RestServiceTabContentResponse(
	props: RestServiceTabContentResponseProps
) {
	const { t } = useTranslation(WEB_SERVICE_TRANSLATIONS)

	const getPrettyString = (data: string) => {
		try {
			return JSON.stringify(JSON.parse(data), undefined, 2)
		} catch (_) {
			return formatXml(data)
		}
	}

	const handleViewTypeChange = (type: WebServiceRestResponseViewByType) => {
		props.onTabDataChange({ responseByView: type })
	}

	const handleToggleExpand = () => {
		props.onTabDataChange({
			isResponseExpanded: !props.tabData.isResponseExpanded,
		})
	}

	const renderResponseViewItem = (item: {
		name: string
		id: WebServiceRestResponseViewByType
	}) => {
		return (
			<div
				onClick={() => handleViewTypeChange(item.id)}
				className={
					'view-item ' +
					(props.tabData.responseByView === item.id ? 'active' : '')
				}
			>
				{t(item.name)}
			</div>
		)
	}

	const renderResponseOkayContent = (data: string) => {
		switch (props.tabData.responseByView) {
			case WebServiceRestResponseViewByType.PRETTY:
				return (
					<IsyEditor
						value={getPrettyString(data)}
						onChange={() => {}}
						options={{
							readOnly: true,
							mode: { name: 'javascript', json: true },
						}}
					/>
				)
			case WebServiceRestResponseViewByType.RAW:
				return (
					<IsyTextarea
						className='raw-view'
						value={data}
						onChange={() => {}}
						disabled={true}
						heightBasedOnContent={true}
					/>
				)
		}
	}

	const renderResponseOkaySection = (data: WebServiceRestResponseProps) => {
		return (
			<div className='okay-section'>
				<div className='view-type'>
					{getResponseViewByOptions().map(renderResponseViewItem)}
				</div>
				<div className='scroll-content'>
					{renderResponseOkayContent(data.data)}
				</div>
				<div className='status-info'>
					{data.statusCode +
						' ' +
						(isEmpty(data.statusText)
							? getStatusText(data.statusCode)
							: data.statusText)}
				</div>
			</div>
		)
	}

	const renderResponseError = (data: WebServiceRestResponseProps) => {
		return (
			<div className='error-section'>
				<img
					className='web-services-icon'
					alt={t(WEB_SERVICES)}
					src='/images/thirdPartyIcons/service.svg'
				/>
				<div className='general-error'>{t(WEB_SERVICE_ERROR_SEND)}</div>
				<div className='errorMessage'>{(data.data as any).message}</div>
			</div>
		)
	}

	const renderResponseBody = () => {
		if (isNil(props.tabData.response)) {
			return null
		}
		switch (props.tabData.response.status) {
			case WebServiceRestResponseStatus.ERROR:
				return renderResponseError(props.tabData.response)
			case WebServiceRestResponseStatus.OKAY:
			default:
				return renderResponseOkaySection(props.tabData.response)
		}
	}

	const render = () => {
		return (
			<div
				className={
					'response-section ' +
					(props.tabData.isResponseExpanded ? 'expanded' : '')
				}
			>
				<div className='header'>
					<span className='title'>{t(WEB_SERVICE_RESPONSE)}</span>
					<img
						src={
							props.tabData.isResponseExpanded
								? '/images/acceleratorsIcons/min.svg'
								: '/images/acceleratorsIcons/max.svg'
						}
						alt='min-max-icon'
						className='icon'
						onClick={handleToggleExpand}
					/>
				</div>
				{renderResponseBody()}
			</div>
		)
	}

	return render()
}
