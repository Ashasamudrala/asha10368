import { cloneDeep, find, isEmpty, isNil } from 'lodash'
import { WEB_SERVICE_NO_URL_ERROR } from '../../utilities/constants'
import {
	WebServiceRestProps,
	WebServiceRestRequestPayloadFormDataProps,
	WebServiceRestRequestPayloadFormDataTypes,
	WebServiceRestRequestPayloadURLEncodedProps,
	WebServiceRestResponseProps,
	WebServiceRestResponseStatus,
} from './webService.types'
import i18n from 'i18next'
import {
	WebServiceRequestBodyLookUpType,
	WebServiceRequestMethodsLookUpType,
} from '../../utilities/apiEnumConstants'
import { ImageBuffer } from '../../infrastructure/imageBuffer'

export const sendRequest = (
	data: WebServiceRestProps
): Promise<WebServiceRestResponseProps> => {
	let urlString = data.requestUrl
	if (isEmpty(urlString)) {
		return new Promise((_, reject) => {
			reject(new Error(i18n.t(WEB_SERVICE_NO_URL_ERROR)))
		})
	}

	// replacing Path Variables
	for (let i = 0, iLen = data.pathVariables.length; i < iLen; i++) {
		const key = data.pathVariables[i].key
		const value = data.pathVariables[i].value
		const replaceRegex = new RegExp('{' + key + '}', 'g')
		urlString = urlString.replace(replaceRegex, value)
	}

	const config: RequestInit = {}
	config.method = data.requestMethod

	// setting header
	config.headers = {}
	for (let i = 0, iLen = data.httpHeaders.length; i < iLen; i++) {
		const key = data.httpHeaders[i].key
		const value = data.httpHeaders[i].value
		if (
			key !== 'Content-Type' ||
			value !== WebServiceRequestBodyLookUpType.FORM_DATA
		) {
			config.headers[key] = value
		}
	}
	if (
		data.requestMethod === WebServiceRequestMethodsLookUpType.POST ||
		data.requestMethod === WebServiceRequestMethodsLookUpType.PUT
	) {
		//setting body content
		const contentTypeHeader = find(
			data.httpHeaders,
			(i) => i.key === 'Content-Type'
		)
		if (!isNil(contentTypeHeader)) {
			switch (contentTypeHeader.value) {
				case WebServiceRequestBodyLookUpType.JSON:
					config.body = JSON.stringify(
						data.requestPayload[WebServiceRequestBodyLookUpType.JSON]
					)
					break
				case WebServiceRequestBodyLookUpType.HTML_PLAIN:
				case WebServiceRequestBodyLookUpType.XML:
					config.body = data.requestPayload[WebServiceRequestBodyLookUpType.XML]
					break
				case WebServiceRequestBodyLookUpType.URL_ENCODED_DATA: {
					const dataObj: any = {}
					const info = data.requestPayload[
						WebServiceRequestBodyLookUpType.URL_ENCODED_DATA
					] as WebServiceRestRequestPayloadURLEncodedProps[]
					for (let i = 0, iLen = info.length; i < iLen; i++) {
						dataObj[info[i].key] = info[i].value
					}
					config.body = JSON.stringify(dataObj)
					break
				}
				case WebServiceRequestBodyLookUpType.FORM_DATA: {
					const formData = new FormData()
					const info = data.requestPayload[
						WebServiceRequestBodyLookUpType.FORM_DATA
					] as WebServiceRestRequestPayloadFormDataProps[]
					for (let i = 0, iLen = info.length; i < iLen; i++) {
						if (
							info[i].type === WebServiceRestRequestPayloadFormDataTypes.FILE
						) {
							const file = ImageBuffer.Instance.retrieveImage(info[i].value)
							if (!isNil(file)) {
								formData.append(info[i].key, file)
							} else {
								formData.append(info[i].key, '')
							}
						} else {
							formData.append(info[i].key, info[i].value)
						}
					}
					config.body = formData
				}
			}
		}
	}

	// making call
	return fetch(urlString, config)
		.then((response) => {
			return response.text().then((text) => {
				return {
					status: WebServiceRestResponseStatus.OKAY,
					data: text || '',
					statusText: response.statusText,
					statusCode: response.status,
				}
			})
		})
		.catch((data) => {
			return {
				status: WebServiceRestResponseStatus.ERROR,
				data: data,
				statusText: data.statusText,
				statusCode: data.status,
			}
		})
}

export const setDataTOUiStandards = (data: WebServiceRestProps) => {
	const finalData: any = cloneDeep(data)
	const contentTypeHeader = find(
		finalData.httpHeaders,
		(i) => i.key === 'Content-Type'
	)
	if (!isNil(contentTypeHeader)) {
		switch (contentTypeHeader.value) {
			case WebServiceRequestBodyLookUpType.URL_ENCODED_DATA: {
				const dataArray: WebServiceRestRequestPayloadURLEncodedProps[] = []
				const info =
					finalData.requestPayload[
						WebServiceRequestBodyLookUpType.URL_ENCODED_DATA
					]
				for (let key in info) {
					dataArray.push({
						key: key,
						value: info[key],
					})
				}
				finalData.requestPayload[
					WebServiceRequestBodyLookUpType.URL_ENCODED_DATA
				] = dataArray
				break
			}
			case WebServiceRequestBodyLookUpType.FORM_DATA: {
				const info =
					finalData.requestPayload[WebServiceRequestBodyLookUpType.FORM_DATA]
				const d: WebServiceRestRequestPayloadFormDataProps[] = info.map(
					(item: any) => {
						const keys = Object.keys(item.data)
						return {
							type: item.type,
							key: keys[0],
							value: item.data[keys[0]],
						}
					}
				)
				finalData.requestPayload[WebServiceRequestBodyLookUpType.FORM_DATA] = d
			}
		}
	}
	return finalData
}

export const prepareDataForSave = (data: WebServiceRestProps) => {
	const finalData: any = cloneDeep(data)
	const contentTypeHeader = find(
		finalData.httpHeaders,
		(i) => i.key === 'Content-Type'
	)
	if (!isNil(contentTypeHeader)) {
		switch (contentTypeHeader.value) {
			case WebServiceRequestBodyLookUpType.URL_ENCODED_DATA: {
				const dataObj: any = {}
				const info = finalData.requestPayload[
					WebServiceRequestBodyLookUpType.URL_ENCODED_DATA
				] as WebServiceRestRequestPayloadURLEncodedProps[]
				for (let i = 0, iLen = info.length; i < iLen; i++) {
					dataObj[info[i].key] = info[i].value
				}
				finalData.requestPayload[
					WebServiceRequestBodyLookUpType.URL_ENCODED_DATA
				] = dataObj
				break
			}
			case WebServiceRequestBodyLookUpType.FORM_DATA: {
				const info = finalData.requestPayload[
					WebServiceRequestBodyLookUpType.FORM_DATA
				] as WebServiceRestRequestPayloadFormDataProps[]
				const d = info.map((item) => {
					let value = item.value as any
					if (item.type === WebServiceRestRequestPayloadFormDataTypes.FILE) {
						const file = ImageBuffer.Instance.retrieveImage(item.value)
						if (!isNil(file)) {
							value = file
						} else {
							value = ''
						}
					}
					return {
						type: item.type,
						data: {
							[item.key]: value,
						},
					}
				})
				finalData.requestPayload[
					WebServiceRequestBodyLookUpType.FORM_DATA
				] = d as any
			}
		}
	}
	if (finalData.isNew) {
		delete finalData.id
	}
	delete finalData.isNew
	return finalData
}
