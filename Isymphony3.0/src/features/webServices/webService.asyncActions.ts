import { createAsyncThunk } from '@reduxjs/toolkit'
import {
	WEB_SERVICE_DELETE_CONFIRMATION_TEXT,
	WEB_SERVICE_SAVE_MESSAGE_SUCCESS,
	WEB_SERVICE_UPDATE_MESSAGE_SUCCESS,
	DELETE_TAB_MESSAGE,
	COMMON_ARE_YOU_SURE,
	OK_BUTTON_LABEL,
	HELP_SKIP_DESCRIPTION_SECTION,
	HELP_WEB_SERVICES_TITLE,
	SKIP_TOUR,
	START_TOUR,
	HELP_WEB_SERVICES_DESCRIPTION,
} from '../../utilities/constants'
import { showDialog, DialogTypes, AddDialogProps } from '../dialogs'
import i18n from 'i18next'
import doAsync from '../../infrastructure/doAsync'
import { RootState } from '../../base.types'
import { getWebServiceItemById, selectAppId } from './webService.selectors'
import { isNil } from 'lodash'
import {
	WEB_SERVICE_GET_AUTHORIZATION_LOOKUPS,
	WEB_SERVICE_GET_LIST,
	WEB_SERVICE_GET_REQUEST_BODY_LOOKUPS,
	WEB_SERVICE_GET_REQUEST_METHODS_LOOKUPS,
	WEB_SERVICE_ITEM,
} from '../../utilities/apiEndpoints'
import { prepareDataForSave } from './webService.utilities'
import { WebServiceRestProps } from './webService.types'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { actions } from '../../authAndPermissions/loginUserDetails.slice'

export const loadWebServiceList = createAsyncThunk(
	'webService/loadWebServiceList',
	async (_: undefined, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState)
		if (isNil(appId)) {
			return
		}
		return await doAsync({
			url: WEB_SERVICE_GET_LIST(appId),
			...thunkArgs,
		})
	}
)

export const loadRequestMethodsLookUps = createAsyncThunk(
	'webService/loadRequestMethodsLookUps',
	async (_: undefined, thunkArgs) => {
		return await doAsync({
			url: WEB_SERVICE_GET_REQUEST_METHODS_LOOKUPS,
			...thunkArgs,
		})
	}
)

export const loadAuthorizationLookUps = createAsyncThunk(
	'webService/loadAuthorizationLookUps',
	async (_: undefined, thunkArgs) => {
		return await doAsync({
			url: WEB_SERVICE_GET_AUTHORIZATION_LOOKUPS,
			...thunkArgs,
		})
	}
)

export const loadRequestBodyLookUps = createAsyncThunk(
	'webService/loadRequestBodyLookUps',
	async (_: undefined, thunkArgs) => {
		return await doAsync({
			url: WEB_SERVICE_GET_REQUEST_BODY_LOOKUPS,
			...thunkArgs,
		})
	}
)

export const deleteWebService = createAsyncThunk(
	'webService/deleteWebService',
	async (data: WebServiceRestProps, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState) as string
		if (data.isNew) {
			return Promise.resolve({ payload: {} })
		}
		return await doAsync({
			url: WEB_SERVICE_ITEM(appId, data.id),
			httpMethod: 'delete',
			successMessage: i18n.t(DELETE_TAB_MESSAGE, {
				name: data.name,
			}),
			...thunkArgs,
		})
	}
)

export const confirmWebServiceDelete = createAsyncThunk(
	'webService/confirmWebServiceDelete',
	async (id: string, { dispatch, getState }) => {
		const service = getWebServiceItemById(getState() as RootState, id)
		if (isNil(service)) {
			return
		}
		return await dispatch(
			showDialog({
				type: DialogTypes.CONFIRMATION_DIALOG,
				title: i18n.t(COMMON_ARE_YOU_SURE),
				onOkay: () => dispatch(deleteWebService(service)),
				data: {
					message: i18n.t(WEB_SERVICE_DELETE_CONFIRMATION_TEXT, {
						name: service.name,
					}),
				},
			} as AddDialogProps)
		)
	}
)

export const postNewService = createAsyncThunk(
	'webService/postNewService',
	async (data: WebServiceRestProps, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState) as string
		const dataToSave = prepareDataForSave(data)
		return await doAsync({
			url: WEB_SERVICE_GET_LIST(appId),
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(dataToSave),
			},
			successMessage: i18n.t(WEB_SERVICE_SAVE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			...thunkArgs,
		})
	}
)

export const updateNewService = createAsyncThunk(
	'webService/updateNewService',
	async (data: WebServiceRestProps, thunkArgs) => {
		const appId = selectAppId(thunkArgs.getState() as RootState) as string
		const dataToSave = prepareDataForSave(data)
		return await doAsync({
			url: WEB_SERVICE_ITEM(appId, data.id),
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify(dataToSave),
			},
			successMessage: i18n.t(WEB_SERVICE_UPDATE_MESSAGE_SUCCESS, {
				name: data.name,
			}),
			...thunkArgs,
		})
	}
)

export const saveWebServiceRequest = createAsyncThunk(
	'webService/saveWebServiceRequest',
	async (
		data: { id: string; updateStepInfo: (stepNumber: number) => void },
		{ dispatch, getState }
	) => {
		const service = getWebServiceItemById(getState() as RootState, data.id)
		if (isNil(service)) {
			return
		}
		if (service.isNew) {
			dispatch(postNewService(service)).then(() => {
				data.updateStepInfo(9)
			})
		} else {
			dispatch(updateNewService(service)).then(() => {
				data.updateStepInfo(9)
			})
		}
	}
)

export const showWebServicesHelpLandingPage = createAsyncThunk(
	'isyTour/showWebServiceLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateWebServicesPreferences({
					landingPage: false,
				})
			)
			thunkArgs.dispatch(updatePreferences()).then(() =>
				thunkArgs.dispatch(
					actions.updateWebServicesPreferences({
						status: HelpModuleStatus.INPROGRESS,
						currentStep: 0,
					})
				)
			)
		}
		const handleCancel = () => {
			thunkArgs.dispatch(showWebServicesSkipHelpLandingPage())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				onCancel: handleCancel,
				data: {
					message: i18n.t(HELP_WEB_SERVICES_DESCRIPTION),
					okayButtonLabel: i18n.t(START_TOUR),
					cancelButtonLabel: i18n.t(SKIP_TOUR),
				},
				title: i18n.t(HELP_WEB_SERVICES_TITLE),
			})
		)
	}
)

export const showWebServicesSkipHelpLandingPage = createAsyncThunk(
	'isyTour/showWebServiceSkipHelpLandingPage',
	async (_: undefined, thunkArgs) => {
		const confirmationCallBack = () => {
			thunkArgs.dispatch(
				actions.updateWebServicesPreferences({
					landingPage: false,
					status: HelpModuleStatus.COMPLETED,
				})
			)
			thunkArgs.dispatch(updatePreferences())
		}
		return await thunkArgs.dispatch(
			showDialog({
				type: DialogTypes.HELP_LANDING_PAGE,
				onOkay: confirmationCallBack,
				data: {
					message: i18n.t(HELP_SKIP_DESCRIPTION_SECTION),
					okayButtonLabel: i18n.t(OK_BUTTON_LABEL),
				},
				title: i18n.t(HELP_WEB_SERVICES_TITLE),
			})
		)
	}
)
