import {
	WebServiceAuthorizationLookUpType,
	WebServiceRequestBodyLookUpType,
	WebServiceRequestMethodsLookUpType,
} from '../../utilities/apiEnumConstants'

export interface WebServiceRestPathVariableProps {
	key: string
	value: string
}

export interface WebServiceRestRequestParameterProps {
	key: string
	value: string
}

export interface WebServiceRestHttpHeaderProps {
	key: string
	value: string
	type: string
}

export enum WebServiceRestRequestPayloadFormDataTypes {
	STRING = 'String',
	FILE = 'File',
}

export interface WebServiceRestRequestPayloadFormDataProps {
	type: WebServiceRestRequestPayloadFormDataTypes
	key: string
	value: string
}

export interface WebServiceRestRequestPayloadURLEncodedProps {
	key: string
	value: string
}

export interface WebServiceRestRequestPayload {
	'application/json'?: JSON
	'application/xml'?: string
	'multipart/form-data'?: WebServiceRestRequestPayloadFormDataProps[]
	'application/x-www-form-urlencoded'?: WebServiceRestRequestPayloadURLEncodedProps[]
	'text/html'?: string
}

export interface WebServiceRestProps {
	id: string
	isNew: boolean
	name: string
	description: string
	requestMethod: WebServiceRequestMethodsLookUpType
	requestUrl: string
	authorizationType: WebServiceAuthorizationLookUpType
	pathVariables: WebServiceRestPathVariableProps[]
	httpHeaders: WebServiceRestHttpHeaderProps[]
	requestParameters: WebServiceRestRequestParameterProps[]
	requestPayload: WebServiceRestRequestPayload
}

export enum WebServiceRestTabType {
	PARAMS = 'PARAMS',
	AUTHORIZATION = 'AUTHORIZATION',
	HEADER = 'HEADER',
	BODY = 'BODY',
}

export enum WebServiceRestTabValidationStatus {
	NONE,
	LOADING,
	DONE,
}

export enum WebServiceRestResponseStatus {
	ERROR,
	OKAY,
}

export enum WebServiceRestResponseViewByType {
	PRETTY,
	RAW,
}

export interface WebServiceRestResponseProps {
	status: WebServiceRestResponseStatus
	data: string
	statusText: string
	statusCode: number
}

export interface WebServiceRestTabProps {
	activeTab: WebServiceRestTabType
	showPassword: boolean
	errors: any
	validationStatus: WebServiceRestTabValidationStatus
	response: WebServiceRestResponseProps | null
	responseByView: WebServiceRestResponseViewByType
	isResponseExpanded: boolean
	responseViewSize: number
}

export interface WebServiceRestDataMap {
	[key: string]: WebServiceRestProps
}

export interface WebServiceRestTabDataMap {
	[key: string]: WebServiceRestTabProps
}

export interface WebServiceRequestMethodsLookUpProps {
	id: WebServiceRequestMethodsLookUpType
	name: string
}

export interface WebServiceRequestBodyLookUpProps {
	id: WebServiceRequestBodyLookUpType
	name: string
}

export interface WebServiceAuthorizationLookUpFieldUiConfigurationMetaDataProps {
	placeholderValue: string
	renderAs: string
}

export interface WebServiceAuthorizationLookUpFieldConstraintsProps {
	length: number
	required: 'true' | 'false'
}

export interface WebServiceAuthorizationLookUpFieldUiConfigurationProps {
	metadata: WebServiceAuthorizationLookUpFieldUiConfigurationMetaDataProps
}

export interface WebServiceAuthorizationLookUpFieldProps {
	name: string
	type: string
	displayName: string
	constraints: WebServiceAuthorizationLookUpFieldConstraintsProps
	uiConfiguration: WebServiceAuthorizationLookUpFieldUiConfigurationProps
}
export interface WebServiceAuthorizationLookUpProps {
	name: WebServiceAuthorizationLookUpType
	displayName: string
	description: string
	fields: WebServiceAuthorizationLookUpFieldProps[]
}

export interface WebServiceState {
	searchString: string
	openTabIds: string[]
	activeTabId: string | null
	appId: string | null
	data: WebServiceRestDataMap
	tabData: WebServiceRestTabDataMap
	listOrder: string[]
	requestMethodsLookUps: WebServiceRequestMethodsLookUpProps[]
	authorizationLookUps: WebServiceAuthorizationLookUpProps[]
	requestBodyLookUps: WebServiceRequestBodyLookUpProps[]
	oldData: WebServiceRestDataMap
}
