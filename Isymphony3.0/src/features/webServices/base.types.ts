export enum webServiceSubReducersNames {
	WEB_SERVICE = 'webService',
}

export interface WebServiceBaseState {
	[webServiceSubReducersNames.WEB_SERVICE]: any
}
