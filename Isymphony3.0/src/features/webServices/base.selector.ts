import { RootState } from '../../base.types'
import { WebServiceBaseState } from './base.types'

export const name = 'webServiceBase'
export const selectSlice = (state: RootState): WebServiceBaseState => {
	return state[name]
}
