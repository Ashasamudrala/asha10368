import { combineReducers } from 'redux'

import * as baseSelector from './base.selector'
import { WebServiceBaseState } from './base.types'
import * as webService from './webService.slice'

export const reducer = combineReducers<WebServiceBaseState>({
	[webService.name]: webService.reducer,
})

export const { name } = baseSelector
export * from './base.types'
