import { combineReducers } from 'redux'
import * as core from './core'

import * as baseSelector from './base.selector'
import { AdminBaseState } from './base.types'

export const reducer = combineReducers<AdminBaseState>({
	[core.name]: core.reducer,
})

export const { name } = baseSelector
export * from './base.types'
