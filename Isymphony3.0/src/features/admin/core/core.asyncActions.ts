import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import { GET_ALL_PLATFORMS } from '../../../utilities/apiEndpoints'

export const fetchAllPlatform = createAsyncThunk(
	'platform/getAll',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: GET_ALL_PLATFORMS,
			...thunkArgs,
		})
)
