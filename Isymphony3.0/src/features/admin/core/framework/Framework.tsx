import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import Avatar from '@material-ui/core/Avatar'
import { FrameworkSettings } from './frameworkSettings/FrameworkSettings'
import {
	IsyGrid,
	IsyGridColumnTypes,
} from '../../../../widgets/IsyGrid/IsyGrid'
import { map, join, filter } from 'lodash'
import {
	PLATFORM_TRANSLATIONS,
	// ADD_FRAMEWORK,
	FRAMEWORK_ALL,
	NAME,
	DESCRIPTION,
	VERSION,
	NO_FRAMEWORKS,
} from '../../../../utilities/constants'
import { IsyCan } from '../../../../widgets/IsyCan/IsyCan'
import './framework.scss'
import { selectorPlatforms } from '../core.selectors'
import { FrameworkProps, PlatformDataProps } from '../core.types'
import { actions } from './frameworkSettings/frameworkSettings.slice'
// import { IsyButton } from '../../../../widgets/IsyButton/IsyButton'
import { IsyPermissionTypes } from '../../../../authAndPermissions/loginUserDetails.types'

export default function Framework() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const allPlatforms = useSelector(selectorPlatforms)
	const [filteredPlatformId, setFilteredPlatformId] = useState<string>('ALL')

	const dispatch = useDispatch()

	const getPlatforms = () => {
		if (filteredPlatformId === 'ALL') {
			return allPlatforms
		}
		return filter(
			allPlatforms,
			(p: PlatformDataProps) => p.id === filteredPlatformId
		)
	}

	const getVersions = (data: FrameworkProps): string => {
		return join(
			map(data.versions, (info) => info.versionName),
			', '
		)
	}

	const gridConfig = [
		{
			header: t(NAME),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'name',
		},
		{
			header: t(DESCRIPTION),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'description',
		},
		{
			header: t(VERSION),
			type: IsyGridColumnTypes.STRING,
			dataRef: getVersions,
		},
	]

	const getFilterPlatform = (event: React.ChangeEvent<HTMLSelectElement>) => {
		setFilteredPlatformId(event.target.value)
	}

	const handleViewFramework = (
		frameworkDetails: FrameworkProps,
		platformData: PlatformDataProps
	) => {
		dispatch(actions.updateSupportedVersions(platformData.versions))
		dispatch(actions.updateFrameworkData(frameworkDetails))
	}

	// const handleAddFramework = (
	// 	_: React.MouseEvent<HTMLButtonElement, MouseEvent>,
	// 	platformDetails: PlatformDataProps
	// ) => {
	// 	dispatch(actions.updatePlatformId(platformDetails.id))
	// 	dispatch(actions.updateSupportedVersions(platformDetails.versions))
	// }

	const renderGrid = (item: PlatformDataProps) => {
		return (
			<IsyGrid
				data={Object.values(item.frameworks)}
				config={gridConfig}
				onRowClick={(data) => handleViewFramework(data, item)}
				emptyMessage={t(NO_FRAMEWORKS)}
			/>
		)
	}

	const renderHeader = () => {
		return (
			<div className='dropdown-filter'>
				<select onChange={getFilterPlatform}>
					<option value={'ALL'}>{t(FRAMEWORK_ALL)}</option>
					{allPlatforms.map((item) => (
						<option key={item.id} value={item.id}>
							{item.name}
						</option>
					))}
				</select>
			</div>
		)
	}

	const renderPlatformIcon = (item: PlatformDataProps) => {
		return (
			<div className='platform-icon'>
				<Avatar>
					{item.metadata.icon &&
					(item.metadata.icon === 'java' || item.metadata.icon === 'nodejs') ? (
						<img
							src={`/images/platformIcons/${item.metadata.icon}.svg`}
							alt={item.name.charAt(0)}
						/>
					) : (
						<img
							src={`/images/platformIcons/${'defaultIcon'}.svg`}
							alt={item.name.charAt(0)}
						/>
					)}
				</Avatar>
			</div>
		)
	}

	const renderAddFrameworkButton = (item: PlatformDataProps) => {
		return null
		// return (
		// 	<IsyCan
		// 		action={IsyPermissionTypes.CREATE_FRAMEWORK}
		// 		yes={() => (
		// 			<div>
		// 				<div className='add-framework-btn'>
		// 					<IsyButton
		// 						onClick={(e: any) => handleAddFramework(e, item)}
		// 						className='primary-btn'
		// 					>
		// 						{t(ADD_FRAMEWORK)}
		// 					</IsyButton>
		// 				</div>
		// 			</div>
		// 		)}
		// 	/>
		// )
	}

	const renderPlatformDetails = (item: PlatformDataProps) => {
		return (
			<div className='description'>
				<div className='platform-name'>{item.name}</div>
				<div title={item.description} className='platform-desc'>
					{item.description}
				</div>
			</div>
		)
	}

	const renderPlatform = (item: PlatformDataProps, index: number) => {
		return (
			<div key={index} className='framework-container'>
				<div className='framework-grid'>
					<div className='content'>
						{renderPlatformIcon(item)}
						{renderPlatformDetails(item)}
						{renderAddFrameworkButton(item)}
					</div>
				</div>
				{renderGrid(item)}
			</div>
		)
	}

	return (
		<div className='list-platform-container'>
			{renderHeader()}
			{getPlatforms().map(renderPlatform)}
			<IsyCan
				action={IsyPermissionTypes.VIEW_FRAMEWORK}
				yes={() => <FrameworkSettings />}
			/>
		</div>
	)
}
