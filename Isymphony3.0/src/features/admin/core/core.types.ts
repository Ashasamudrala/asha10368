export interface PlatformMetadataProps {
	[key: string]: any
}

export interface FrameworkVersionModulesProps {
	name: string
	description: string
	version: string
	metadata: PlatformMetadataProps
}

export interface FrameworkVersionProps {
	versionName: string
	supportedPlatformVersions: string[]
	modules: FrameworkVersionModulesProps[]
	metadata: PlatformMetadataProps
}

export interface FrameworkChannelConfiguration {
	channelName: string
	metadata: PlatformMetadataProps
}

export interface FrameworkProps {
	id: string
	description: string
	name: string
	platformId: string
	platformName: string
	versions: FrameworkVersionProps[]
	channelConfiguration: FrameworkChannelConfiguration
	metadata: PlatformMetadataProps
}

export interface PlatformFrameworksDataProps {
	[key: string]: FrameworkProps
}

export interface PlatformDataProps {
	id: string
	description: string
	name: string
	versions: string[]
	frameworks: PlatformFrameworksDataProps
	metadata: PlatformMetadataProps
}

export interface CoreState {
	activeTab: number
	platforms: PlatformDataProps[]
}
