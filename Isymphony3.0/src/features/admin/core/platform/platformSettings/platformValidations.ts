import { isEmpty } from 'lodash'

const validateNameWithoutSpaces = /^[^\s]+(\s+[^\s]+)*$/
const versionValidationRegex = /[a-zA-Z~`!#$%&*+=\-\]\\';,/{}|\\":<>()]/

function isRequiredFieldEmpty(name: string | string[]): boolean {
	return isEmpty(name)
}

function isValidNameWithoutSpaces(name: string): boolean {
	return !validateNameWithoutSpaces.test(name)
}

function isValidNameLength(name: string): boolean {
	return name.length >= 64
}

function isValidDescriptionLength(name: string): boolean {
	return name.length > 1024
}

function isValidVersionsArray(versions: string[]) {
	return (
		versions.length > 0 &&
		versions.find((version) => {
			return versionValidationRegex.test(version)
		})
	)
}

export {
	isRequiredFieldEmpty,
	isValidNameWithoutSpaces,
	isValidVersionsArray,
	isValidNameLength,
	isValidDescriptionLength,
}
