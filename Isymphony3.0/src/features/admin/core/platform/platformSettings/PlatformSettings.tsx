import React, { useEffect } from 'react'
import {
	IsyFormBuilderFormItemProps,
	IsyFormBuilderFormTypes,
	IsyFormBuilder,
} from '../../../../../widgets/IsyFormBuilder/IsyFormBuilder'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import {
	PLATFORM_TRANSLATIONS,
	PLATFORM_DETAILS,
	NAME,
	PLATFORM_NAME_PLACEHOLDER,
	DESCRIPTION,
	PLATFORM_DESCRIPTION_PLACEHOLDER,
	VERSION,
	PLATFORM_VERSIONS_PLACEHOLDER,
	PLATFORM_METADATA,
	ADD_METADATA,
	ENTER_KEY,
	ENTER_VALUE,
	VALIDATE_NAME,
	INVALID_NAME_LENGTH,
	VALIDATE_VERSION,
	VALIDATE_VERSION_PATTERN,
	VALIDATE_DESCRIPTION_PATTERN,
	KEY,
	VALUE,
	ADD_PLATFORM,
	// EDIT,
	BUTTON_SAVE,
	BUTTON_CANCEL,
} from '../../../../../utilities/constants'
import {
	isRequiredFieldEmpty,
	isValidDescriptionLength,
	isValidNameLength,
	isValidVersionsArray,
} from './platformValidations'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { actions } from './platformSettings.slice'
import {
	getPlatformSettingsData,
	getCurrentState,
	getErrorsList,
	getPlatformId,
} from './platformSettings.selectors'
import { useDispatch } from 'react-redux'
import {
	createAndUpdatePlatformSettings,
	fetchPlatformById,
} from './platformSettings.asyncActions'
import {
	CurrentStateType,
	PlatformSettingsErrorsList,
	PlatformSettingsProps,
} from './platformSettings.types'
import { Drawer } from '@material-ui/core'
import './platformSettings.scss'
import { IsyBusyIndicator } from '../../../../../widgets/IsyBusyIndicator/IsyBusyIndicator'
import { isEmpty } from 'lodash'
import { IsyButton } from '../../../../../widgets/IsyButton/IsyButton'
// import { IsyPermissionTypes } from '../../../../../authAndPermissions/loginUserDetails.types'
// import { IsyCan } from '../../../../../widgets/IsyCan/IsyCan'

export function PlatformSettings() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const platformId = useSelector(getPlatformId)
	const platformSettingsData = useSelector(getPlatformSettingsData)
	const currentState = useSelector(getCurrentState)
	const errorList = useSelector(getErrorsList)
	const dispatch = useDispatch()

	useEffect(() => {
		if (!isEmpty(platformId)) {
			dispatch(fetchPlatformById())
		}
	}, [platformId, dispatch])

	const getFormBuilderConfig = () => {
		const config: IsyFormBuilderFormItemProps[] = [
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				title: t(PLATFORM_DETAILS),
				forms: [
					{
						type: IsyFormBuilderFormTypes.STRING,
						title: t(NAME),
						dataRef: 'name',
						placeholder: t(PLATFORM_NAME_PLACEHOLDER),
						autoFocus: true,
						isRequired: true,
						onChange: handlePlatformNameChange,
						isHidden: currentState !== CurrentStateType.CREATE,
					},
					{
						type: IsyFormBuilderFormTypes.TEXT_AREA,
						title: t(DESCRIPTION),
						dataRef: 'description',
						placeholder: t(PLATFORM_DESCRIPTION_PLACEHOLDER),
					},
					{
						type: IsyFormBuilderFormTypes.CHIP_INPUT,
						title: t(VERSION),
						isRequired: true,
						dataRef: 'versions',
						placeholder: t(PLATFORM_VERSIONS_PLACEHOLDER),
					},
				],
			},
			{
				type: IsyFormBuilderFormTypes.ACCORDION_SECTION,
				isExpandByDefault: true,
				title: t(PLATFORM_METADATA),
				forms: [
					{
						type: IsyFormBuilderFormTypes.GRID_SECTION,
						addLabel: t(ADD_METADATA),
						addDefault: { key: '', value: '' },
						className: 'platform-metadata-container',
						dataRef: 'metadata',
						forms: [
							{
								type: IsyFormBuilderFormTypes.STRING,
								dataRef: 'key',
								autoFocus: true,
								placeholder: t(ENTER_KEY),
								title: t(KEY),
							},
							{
								type: IsyFormBuilderFormTypes.STRING,
								dataRef: 'value',
								placeholder: t(ENTER_VALUE),
								title: t(VALUE),
							},
						],
					},
				],
			},
		]
		return config
	}

	const handleClose = () => {
		dispatch(actions.clearData(undefined))
	}

	const handleValidations = () => {
		const fields = { ...platformSettingsData }
		let errors: PlatformSettingsErrorsList = {}
		if (isRequiredFieldEmpty(fields.name)) {
			errors.name = t(VALIDATE_NAME)
		} else if (isValidNameLength(fields.name)) {
			errors.name = t(INVALID_NAME_LENGTH)
		}
		if (isRequiredFieldEmpty(fields.versions)) {
			errors.versions = t(VALIDATE_VERSION)
		} else if (isValidVersionsArray(fields.versions)) {
			errors.versions = t(VALIDATE_VERSION_PATTERN)
		}
		if (isValidDescriptionLength(fields.description)) {
			errors.description = t(VALIDATE_DESCRIPTION_PATTERN)
		}
		return errors
	}

	const handleSave = () => {
		const errors = handleValidations()
		if (Object.keys(errors).length > 0) {
			return dispatch(actions.updateErrorList(errors))
		} else {
			dispatch(createAndUpdatePlatformSettings())
		}
	}

	const handlePlatformNameChange = (value: string) => {
		dispatch(
			actions.updatePlatformSettingsData({
				name: value.trimStart().replace(/\s{2,}/g, ' '),
			})
		)
	}

	const handleOnChange = (val: PlatformSettingsProps) => {
		dispatch(actions.updatePlatformSettingsData(val))
	}

	// const handleEditIcon = () => {
	// 	dispatch(actions.updateCurrentState(CurrentStateType.EDIT))
	// }

	const renderFormBuilder = () => {
		return (
			<IsyFormBuilder<PlatformSettingsProps>
				forms={getFormBuilderConfig()}
				data={platformSettingsData}
				errors={{ ...errorList }}
				onChange={handleOnChange}
				isViewMode={currentState === CurrentStateType.VIEW}
			/>
		)
	}

	const renderEditIcon = () => {
		// if (currentState === CurrentStateType.VIEW) {
		// 	return (
		// 		<IsyCan
		// 			action={IsyPermissionTypes.UPDATE_PLATFORM}
		// 			yes={() => (
		// 				<span
		// 					onClick={handleEditIcon}
		// 					tabIndex={0}
		// 					className='edit_icon-section'
		// 				>
		// 					<span className='edit_icon'>
		// 						<img src={`/images/Edit.svg`} alt={t(EDIT)} />
		// 					</span>
		// 					<span className='edit-label'>{t(EDIT)}</span>
		// 				</span>
		// 			)}
		// 		/>
		// 	)
		// }
		return null
	}

	const renderHeader = () => {
		return (
			<div className='header-section'>
				<span className='header-title'>
					{currentState === CurrentStateType.CREATE
						? t(ADD_PLATFORM)
						: platformSettingsData.name}
				</span>
				<CloseOutlinedIcon
					className='header-close-icon'
					onClick={handleClose}
					tabIndex={0}
				/>
				{renderEditIcon()}
			</div>
		)
	}

	const renderMiddle = () => {
		return (
			<IsyBusyIndicator doNotShowNoText={true}>
				<div className='middle-section'>{renderFormBuilder()}</div>
			</IsyBusyIndicator>
		)
	}

	const renderBottom = () => {
		return (
			<div className='bottom-section'>
				{CurrentStateType.VIEW !== currentState &&
					CurrentStateType.NONE !== currentState && (
						<IsyButton className='primary-btn' onClick={handleSave}>
							{t(BUTTON_SAVE)}
						</IsyButton>
					)}
				<IsyButton className='standard-btn' onClick={handleClose}>
					{t(BUTTON_CANCEL)}
				</IsyButton>
			</div>
		)
	}

	return (
		<Drawer
			className={
				'platform-settings ' +
				(CurrentStateType.VIEW === currentState ? 'view-mode' : '')
			}
			open={CurrentStateType.NONE !== currentState}
			anchor='right'
			classes={{
				paper: 'drawer-paper',
			}}
		>
			<div className='platform-settings-parent'>
				{renderHeader()}
				<div className='platform-settings-container'>{renderMiddle()}</div>
				{renderBottom()}
			</div>
		</Drawer>
	)
}
