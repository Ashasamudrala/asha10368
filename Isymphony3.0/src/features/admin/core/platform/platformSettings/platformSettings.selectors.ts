import { selectSlice as baseSelector } from '../../base.selector'
import { RootState } from '../../../../../base.types'
import {
	CurrentStateType,
	PlatformSettingsErrorsList,
	PlatformSettingsProps,
	PlatformSettingState,
} from './platformSettings.types'
import { CoreSubReducersNames } from '../../base.types'

export const selectSlice = (state: RootState): PlatformSettingState => {
	return baseSelector(state)[CoreSubReducersNames.PLATFORM_SETTINGS]
}

export const getPlatformSettingsData = (
	state: RootState
): PlatformSettingsProps => {
	return selectSlice(state).platformsData
}

export const getCurrentState = (state: RootState): CurrentStateType => {
	return selectSlice(state).currentState
}

export const getPlatformId = (state: RootState): string => {
	return selectSlice(state).platformId
}

export const getErrorsList = (state: RootState): PlatformSettingsErrorsList => {
	return selectSlice(state).errorsList
}
