import slice from './core.slice'
import { selectSlice as baseSelector } from './base.selector'
import { RootState } from '../../../base.types'
import { CoreState, PlatformDataProps } from './core.types'

export const selectSlice = (state: RootState): CoreState =>
	baseSelector(state)[slice.name]

export const getActiveTab = (state: RootState): number =>
	selectSlice(state).activeTab

export const selectorPlatforms = (state: RootState): PlatformDataProps[] =>
	selectSlice(state).platforms
