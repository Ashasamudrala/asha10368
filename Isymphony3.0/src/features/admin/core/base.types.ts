import { CoreState } from './core.types'
import { FrameworkSettingsState } from './framework/frameworkSettings/frameworkSettings.types'
import { PlatformSettingState } from './platform/platformSettings/platformSettings.types'

export enum CoreSubReducersNames {
	CORE = 'core',
	FRAMEWORK_SETTINGS = 'frameworkSettings',
	PLATFORM_SETTINGS = 'platformSettings',
}
export interface CoreBaseState {
	[CoreSubReducersNames.CORE]: CoreState
	[CoreSubReducersNames.FRAMEWORK_SETTINGS]: FrameworkSettingsState
	[CoreSubReducersNames.PLATFORM_SETTINGS]: PlatformSettingState
}
