import React, { useEffect } from 'react'
import {
	PLATFORM_TRANSLATIONS,
	PLATFORM,
	FRAMEWORK,
	PLATFORM_ADMIN,
} from '../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import { IsyTabItemProps, IsyTabs } from '../../../widgets/IsyTabs/IsyTabs'
import { actions } from './core.slice'
import { getActiveTab } from './core.selectors'
import Platform from './platform/Platform'
import Framework from './framework/Framework'
import { fetchAllPlatform } from './core.asyncActions'
import './core.scss'

export default function Core() {
	const activeTab = useSelector(getActiveTab)
	const dispatch = useDispatch()
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)

	useEffect(() => {
		dispatch(fetchAllPlatform())
	}, [dispatch])

	const getTabsConfig = () => [
		{
			id: 0,
			header: t(PLATFORM),
			content: <Platform />,
		},
		{
			id: 1,
			header: t(FRAMEWORK),
			content: <Framework />,
		},
	]

	const handleTabChange = (tab: IsyTabItemProps<number>) => {
		dispatch(actions.setActiveTab(tab.id))
	}

	return (
		<div className='core-platform'>
			<div className='title-holder'>
				<div className='title'>{t(PLATFORM_ADMIN)}</div>
			</div>
			<IsyTabs
				tabs={getTabsConfig()}
				onTabChange={handleTabChange}
				activeTab={activeTab}
			/>
		</div>
	)
}
