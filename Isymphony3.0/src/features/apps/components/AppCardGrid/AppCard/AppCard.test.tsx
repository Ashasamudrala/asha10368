import React from 'react'
import { mount } from 'enzyme'
import { AppCard, AppCardProps } from './AppCard'

describe('AppCard Component', () => {
	let wrapper: any

	const props: AppCardProps = {
		data: {
			createdBy: '608bb6f2e76be1b12696948d',
			lastModifiedTimestamp: 1619769074828,
			id: '608bb6f2c8c87ef623a9c486',
			type: 'Web',
			name: 'Cello Health Platform',
			description: 'Cello Health Platform.',
			createdByUserDisplayName: 'Admin',
			createdByUserProfilePicture: '',
		},
		onEditApp: jest.fn(),
		onItemSelect: jest.fn(),
	}

	describe('AppCard component', () => {
		beforeEach(() => {
			wrapper = mount(<AppCard {...props} />)
		})

		it('should render web as app type', () => {
			expect(wrapper.find('span.type-chip').text()).toEqual(props.data.type)
		})

		it('should render description as app type Cello Health platform', () => {
			expect(wrapper.find('div.card-content').text()).toEqual(
				props.data.description
			)
		})

		it('should render name as Cello Health Platform', () => {
			expect(wrapper.find('span.title').text()).toEqual(props.data.name)
		})

		it('should render createdByUserDisplayName as Admin', () => {
			expect(wrapper.find('span.owner-name').text()).toEqual(
				props.data.createdByUserDisplayName
			)
		})

		it('should onItemSelect function to be called on click of card 1 time', () => {
			wrapper.find('div.app-card').simulate('click')
			expect(props.onItemSelect).toHaveBeenCalledTimes(1)
		})
	})
})
