import React from 'react'
import { useTranslation } from 'react-i18next'
import { AppCard } from './AppCard/AppCard'
import Grid from '@material-ui/core/Grid'
import {
	NO_CARDS_TO_SHOW,
	APPS_TRANSLATIONS,
} from '../../../../utilities/constants'
import { CardViewSecondIcon } from '../../../../icons/CardViewSecond'
import './appCardGrid.scss'
import { AppState } from '../../apps.types'

export interface AppCardGridProps {
	listOfApplications: AppState[]
	onEditApp: (appId: string) => void
	onItemSelect: (data: AppState) => void
}

export function AppCardGrid(props: AppCardGridProps) {
	const { listOfApplications } = props
	const { t } = useTranslation(APPS_TRANSLATIONS)

	const renderNoCardsView = () => {
		return (
			<div className='empty-card-view'>
				<CardViewSecondIcon className='icon' />
				<div className='text'>{t(NO_CARDS_TO_SHOW)}</div>
			</div>
		)
	}

	const renderList = () => {
		return (
			<Grid container spacing={2}>
				{listOfApplications.map((data, i) => (
					<Grid item xs={4} xl={3}>
						<AppCard
							key={data.id}
							data={data}
							onItemSelect={props.onItemSelect}
							onEditApp={props.onEditApp}
						/>
					</Grid>
				))}
			</Grid>
		)
	}

	return (
		<div className='grid-card-container'>
			{listOfApplications.length === 0 ? renderNoCardsView() : renderList()}
		</div>
	)
}
