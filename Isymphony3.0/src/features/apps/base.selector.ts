import { RootState } from '../../base.types'
import { AppsBaseState } from './base.types'

export const name = 'apps'
export const selectSlice = (state: RootState): AppsBaseState => {
	return state[name]
}
