import React from 'react'
import { mount } from 'enzyme'
import { Apps } from './Apps'
import * as reactRedux from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import {
	AppsFilterBy,
	AppsSortBy,
	OrderBy,
} from '../../utilities/apiEnumConstants'
import { AppsFilterByUi, AppsViewType } from './apps.types'
import { defaultAppDetails } from './appSettings/appSettings.slice'
import { SettingsCurrentState } from './appSettings/appSettings.types'
import { defaultConfig } from '../../authAndPermissions/loginUserDetails.slice'

describe('Apps Component test when apps list is of 0', () => {
	let wrapper: any
	let store: any
	let useEffect: any

	const mockUseEffect = () => {
		useEffect.mockImplementationOnce((f: () => any) => f())
	}

	beforeEach(() => {
		useEffect = jest.spyOn(React, 'useEffect')
		store = configureStore([thunk])({
			apps: {
				apps: {
					allApps: [],
					totalCount: 0,
					searchString: '',
					sortBy: AppsSortBy.LAST_MODIFIED,
					orderBy: OrderBy.ASC,
					filterByType: AppsFilterByUi.ALL_CHANNELS,
					recordsPerPage: 12,
					currentPage: 0,
					viewType: AppsViewType.GRID,
					isLoading: false,
				},
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.NONE,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
			loginUserDetails: {
				preferences: defaultConfig,
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		mockUseEffect()
		mockUseEffect()

		wrapper = mount(
			<Provider store={store}>
				<Apps />
			</Provider>
		)
	})

	afterEach(() => {
		jest.clearAllMocks()
	})

	it('should render with no-apps page ', () => {
		expect(wrapper.find('div.app-page').length).toBe(1)
	})

	it(`checking when no-apps button is present or not`, () => {
		expect(wrapper.find('button').length).toBe(1)
	})
})

describe('Apps Component test when apps list is of > 1', () => {
	let wrapper: any
	let store: any
	let useEffect: any

	const mockUseEffect = () => {
		useEffect.mockImplementationOnce((f: () => any) => f())
	}

	beforeEach(() => {
		useEffect = jest.spyOn(React, 'useEffect')

		store = configureStore([thunk])({
			apps: {
				apps: {
					allApps: [
						{
							createdBy: '608bb6f2e76be1b12696948d',
							lastModifiedTimestamp: 1619769074828,
							id: '608bb6f2c8c87ef623a9c486',
							type: 'Web',
							name: 'Cello Health Platform',
							description: 'Cello Health Platform.',
							createdByUserDisplayName: 'Admin ',
							createdByUserProfilePicture: '',
						},
					],
					totalCount: 1,
					searchString: '',
					sortBy: AppsSortBy.LAST_MODIFIED,
					orderBy: OrderBy.ASC,
					filterByType: AppsFilterBy.WEB,
					recordsPerPage: 12,
					currentPage: 0,
					viewType: AppsViewType.GRID,
					isLoading: false,
				},
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.NONE,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
			loginUserDetails: {
				preferences: defaultConfig,
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		mockUseEffect()
		mockUseEffect()

		wrapper = mount(
			<Provider store={store}>
				<Apps />
			</Provider>
		)
	})

	afterEach(() => {
		jest.clearAllMocks()
	})

	it('should render apps card details ', () => {
		expect(wrapper.find('div.app-card-header-content')).toHaveLength(1)
	})
})

describe('Apps Component test when we search apps with search string of "app" and channel web', () => {
	let wrapper: any
	let store: any
	let useEffect: any

	const mockUseEffect = () => {
		useEffect.mockImplementationOnce((f: () => any) => f())
	}

	beforeEach(() => {
		useEffect = jest.spyOn(React, 'useEffect')

		store = configureStore([thunk])({
			apps: {
				apps: {
					allApps: [],
					totalCount: 1,
					searchString: 'web',
					sortBy: AppsSortBy.LAST_MODIFIED,
					orderBy: OrderBy.ASC,
					filterByType: AppsFilterBy.WEB,
					recordsPerPage: 12,
					currentPage: 0,
					viewType: AppsViewType.GRID,
					isLoading: false,
				},
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.NONE,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
			loginUserDetails: {
				preferences: defaultConfig,
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		mockUseEffect()
		mockUseEffect()

		wrapper = mount(
			<Provider store={store}>
				<Apps />
			</Provider>
		)
	})

	afterEach(() => {
		jest.clearAllMocks()
	})

	it('on mocking it returns [] of apps and finding whether it contains empty-card-view', () => {
		expect(wrapper.find('div.empty-card-view')).toHaveLength(1)
	})
})

describe('Apps Component test when we search apps with search string of "cell" and channel web and by mocking api it returns 2 list of apps', () => {
	let wrapper: any
	let store: any
	let useEffect: any

	const mockUseEffect = () => {
		useEffect.mockImplementationOnce((f: () => any) => f())
	}

	beforeEach(() => {
		useEffect = jest.spyOn(React, 'useEffect')

		store = configureStore([thunk])({
			apps: {
				apps: {
					allApps: [
						{
							createdBy: '608bb6f2e76be1b12696945d',
							lastModifiedTimestamp: 1619769074828,
							id: '608bb6f2c8c87ef623a9c485',
							type: 'Web',
							name: 'Cello Health Platform',
							description: 'Cello Health Platform.',
							createdByUserDisplayName: 'Admin ',
							createdByUserProfilePicture: '',
						},
						{
							createdBy: '608bb6f2e76be1b12696948d',
							lastModifiedTimestamp: 1619769074828,
							id: '608bb6f2c8c87ef623a9c486',
							type: 'Web',
							name: 'Cello Health Platforms',
							description: 'Cello Health Platforms.',
							createdByUserDisplayName: 'Admins ',
							createdByUserProfilePicture: '',
						},
					],
					totalCount: 2,
					searchString: 'web',
					sortBy: AppsSortBy.LAST_MODIFIED,
					orderBy: OrderBy.ASC,
					filterByType: AppsFilterBy.WEB,
					recordsPerPage: 12,
					currentPage: 0,
					viewType: AppsViewType.GRID,
					isLoading: false,
				},
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.NONE,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
			loginUserDetails: {
				preferences: defaultConfig,
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		mockUseEffect()
		mockUseEffect()

		wrapper = mount(
			<Provider store={store}>
				<Apps />
			</Provider>
		)
	})

	afterEach(() => {
		jest.clearAllMocks()
	})

	it('on mocking it returns apps of length 2  and whether it is rendering 2 app cards or not', () => {
		expect(wrapper.find('div.app-card-header-content')).toHaveLength(2)
	})
})
