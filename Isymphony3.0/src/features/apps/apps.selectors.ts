import { selectSlice as baseSelector } from './base.selector'
import { AppsState, AppsFilterByUi, AppsViewType, AppState } from './apps.types'
import { RootState } from '../../base.types'
import {
	AppsFilterBy,
	OrderBy,
	AppsSortBy,
} from '../../utilities/apiEnumConstants'
import { AppsSubReducersNames } from './base.types'

export const selectSlice = (state: RootState): AppsState => {
	return baseSelector(state)[AppsSubReducersNames.APPS]
}

export const selectAllApps = (state: RootState): AppState[] => {
	return selectSlice(state).allApps
}

export const selectFilterByType = (
	state: RootState
): AppsFilterBy | AppsFilterByUi => {
	return selectSlice(state).filterByType
}

export const selectFilterTouched = (state: RootState): boolean => {
	return (
		selectFilterByType(state) !== AppsFilterByUi.ALL_CHANNELS ||
		selectSearchString(state) !== ''
	)
}

export const selectOrderBy = (state: RootState): OrderBy => {
	return selectSlice(state).orderBy
}

export const selectSortBy = (state: RootState): AppsSortBy => {
	return selectSlice(state).sortBy
}

export const selectRecordsPerPage = (state: RootState): number => {
	return selectSlice(state).recordsPerPage
}

export const selectCurrentPage = (state: RootState): number => {
	return selectSlice(state).currentPage
}

export const selectViewType = (state: RootState): AppsViewType => {
	return selectSlice(state).viewType
}

export const selectSearchString = (state: RootState): string => {
	return selectSlice(state).searchString
}

export const selectTotalCount = (state: RootState): number => {
	return selectSlice(state).totalCount
}

export const selectIsLoading = (state: RootState): boolean => {
	return selectSlice(state).isLoading
}
