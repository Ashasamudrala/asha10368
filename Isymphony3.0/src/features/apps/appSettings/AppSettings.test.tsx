import { mount } from 'enzyme'
import { AppSettingProps } from './AppSettings'
import AppSettings from './AppSettings'
import * as reactRedux from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { defaultAppDetails } from './appSettings.slice'
import { SettingsCurrentState } from './appSettings.types'
import CircularProgress from '@material-ui/core/CircularProgress'

describe('AppSettings Test render landing page', () => {
	let wrapper: any
	let store: any
	const props: AppSettingProps = {
		onGoToApplicationView: jest.fn(),
		updateSteps: jest.fn(),
		appsTourEnable: false,
	}

	beforeEach(() => {
		store = configureStore([thunk])({
			apps: {
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.LANDING,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<AppSettings {...props} />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('should render  IsyButton with text web ', () => {
		expect(wrapper.find('button.web-btn')).toHaveLength(1)
	})
})

describe('AppSettings display Loader page', () => {
	let wrapper: any
	let store: any
	const props: AppSettingProps = {
		onGoToApplicationView: jest.fn(),
		updateSteps: jest.fn(),
		appsTourEnable: false,
	}

	beforeEach(() => {
		store = configureStore([thunk])({
			apps: {
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.PROGRESS,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<AppSettings {...props} />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('checking loader is displayed or not', () => {
		expect(wrapper.contains(<CircularProgress size={40} />)).toBe(true)
	})
})

describe('AppSettings display create success page', () => {
	let wrapper: any
	let store: any
	const props: AppSettingProps = {
		onGoToApplicationView: jest.fn(),
		updateSteps: jest.fn(),
		appsTourEnable: false,
	}

	beforeEach(() => {
		store = configureStore([thunk])({
			apps: {
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.SUCCESS_PAGE_CREATE,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<AppSettings {...props} />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('checking app-success-text is containing ', () => {
		expect(wrapper.find('div.app-success-text').text()).toEqual(
			'apps:APP_SUCCESS_MESSAGE'
		)
	})

	it('checking whether it contains button ', () => {
		expect(wrapper.find('button.open-app')).toHaveLength(1)
	})
})

describe('AppSettings display update success page', () => {
	let wrapper: any
	let store: any
	const props: AppSettingProps = {
		onGoToApplicationView: jest.fn(),
		updateSteps: jest.fn(),
		appsTourEnable: false,
	}

	beforeEach(() => {
		store = configureStore([thunk])({
			apps: {
				appSettings: {
					appDetails: defaultAppDetails,
					currentState: SettingsCurrentState.SUCCESS_PAGE_UPDATE,
					platformOptions: [],
					errorList: {},
					applicationId: null,
				},
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<AppSettings {...props} />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('checking app-success-text is containing ', () => {
		expect(wrapper.find('div.app-success-text').text()).toEqual(
			'apps:APP_SUCCESS_UPDATE_MESSAGE'
		)
	})

	it('checking whether it contains button ', () => {
		expect(wrapper.find('button.open-app')).toHaveLength(1)
	})
})
