import AppSettings from './AppSettings'
import slice from './appSettings.slice'

export const { name, reducer } = slice

// we export the component most likely to be desired by default
export default AppSettings
