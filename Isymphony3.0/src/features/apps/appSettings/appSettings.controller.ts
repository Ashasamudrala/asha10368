import { createAsyncThunk } from '@reduxjs/toolkit'
import { isEqual } from 'lodash'
import { RootState } from '../../../base.types'
import { DialogTypes, showDialog } from '../../dialogs'
import { getAppDetails } from './appSettings.selectors'
import { defaultAppDetails, actions } from './appSettings.slice'
import i18n from 'i18next'
import {
	FORM_DIALOG_EXIT_MESSAGE_WITH_CHANGES,
	BUTTON_DISCARD,
	BUTTON_SAVE,
} from '../../../utilities/constants'

export const closeConformationAppSettings = createAsyncThunk(
	'appSettings/closeConformationAppSettings',
	async (
		data: { cancelCallBack: () => void; saveCallBack: () => void },
		{ dispatch, getState }
	) => {
		const configurationData = getAppDetails(getState() as RootState)
		const conformationCallBack = () => {
			data.saveCallBack()
		}
		const handleOnClose = () => {
			data.cancelCallBack()
		}
		if (isEqual(configurationData, defaultAppDetails)) {
			dispatch(actions.clearData(undefined))
		} else {
			return await dispatch(
				showDialog({
					type: DialogTypes.CONFIRMATION_DIALOG,
					onOkay: conformationCallBack,
					onCancel: handleOnClose,
					data: {
						message: i18n.t(FORM_DIALOG_EXIT_MESSAGE_WITH_CHANGES),
						okayButtonLabel: i18n.t(BUTTON_SAVE),
						cancelButtonLabel: i18n.t(BUTTON_DISCARD),
					},
				})
			)
		}
	}
)
