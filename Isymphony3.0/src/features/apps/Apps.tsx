import React, { useState } from 'react'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import { useSelector, useDispatch } from 'react-redux'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import {
	APPS_TRANSLATIONS,
	APP_TEXT,
	APP,
	SEARCH_APPS,
	CREATE_APP,
	APP_SETTINGS,
	// DELETE,
	APP_NAME,
	OWNER,
	LAST_MODIFIED,
	CHANNEL,
	NO_APPS_FOUND,
	DATE_CREATED,
} from '../../utilities/constants'
import {
	selectAllApps,
	selectViewType,
	selectFilterTouched,
	selectIsLoading,
	selectSearchString,
	selectOrderBy,
	selectSortBy,
} from './apps.selectors'
import { fetchAllApps, showHelpLandingPage } from './apps.asyncActions'
import { IsyPopover } from '../../widgets/IsyPopover/IsyPopover'
import { AppsBar } from './appsBar/AppsBar'
import { AppCardGrid } from './components/AppCardGrid/AppCardGrid'
import { useTranslation } from 'react-i18next'
import AppSettings from './appSettings/AppSettings'
import { useHistory } from 'react-router-dom'
import { actions as asActions } from './appSettings/appSettings.slice'
import { actions as appActions } from './apps.slice'
import { debounce, isNil } from 'lodash'
import { IsyGrid, IsyGridColumnTypes } from '../../widgets/IsyGrid/IsyGrid'
import './apps.scss'
import { AppState, AppsViewType } from './apps.types'
import { SettingsCurrentState } from './appSettings/appSettings.types'
import { IsySearch } from '../../widgets/IsySearch/IsySearch'
import { OrderBy, AppsSortBy } from '../../utilities/apiEnumConstants'
import { IsySteps } from '../../widgets/IsySteps/IsySteps'
import { getAppDetails } from './appSettings/appSettings.selectors'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import { updatePreferences } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { selectGetAppPreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'
import { getAppExitStep, getAppsSteps } from '../../utilities/isyStepsConfig'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { CustomButtonType } from '../../widgets/IsySteps/IsyTour/Steps'

export function Apps() {
	const { t } = useTranslation(APPS_TRANSLATIONS)
	const apps = useSelector(selectAllApps)
	const filterTouched = useSelector(selectFilterTouched)
	const viewType = useSelector(selectViewType)
	const isLoading = useSelector(selectIsLoading)
	const searchString = useSelector(selectSearchString)
	const orderBy = useSelector(selectOrderBy)
	const sortBy = useSelector(selectSortBy)
	const configAppData = useSelector(getAppDetails)
	const appsPreferenceData = useSelector(selectGetAppPreferenceStatus)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState<Element | null>(null)
	const [settingsData, setSettingsData] = useState<AppState | null>(null)
	const appName = configAppData && configAppData.name
	let stepsConfig = getAppsSteps(appName)
	const history = useHistory()
	const dispatch = useDispatch()

	React.useEffect(() => {
		dispatch(fetchAllApps())
	}, [dispatch])

	React.useEffect(() => {
		if (appsPreferenceData && appsPreferenceData.landingPage === true) {
			dispatch(showHelpLandingPage())
		}
	}, [appsPreferenceData, dispatch])

	const gridConfig = () => [
		{
			header: t(APP_NAME),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'name',
			sortRef: AppsSortBy.NAME,
		},
		{
			header: '',
			dataRef: renderMoreIcon,
		},
		{
			header: t(OWNER),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'createdByUserDisplayName',
		},
		{
			header: t(LAST_MODIFIED),
			type: IsyGridColumnTypes.DATE,
			dataRef: 'lastModifiedTimestamp',
			sortRef: AppsSortBy.LAST_MODIFIED,
		},
		{
			header: t(DATE_CREATED),
			type: IsyGridColumnTypes.DATE,
			dataRef: 'creationTimestamp',
			sortRef: AppsSortBy.CREATED_ON,
		},
		{
			header: t(CHANNEL),
			type: IsyGridColumnTypes.STRING,
			dataRef: 'type',
		},
	]

	const popoverActions = [
		{
			name: t(APP_SETTINGS),
			key: 'settings',
		},
	]

	const goToAppView = (appId: string) => {
		history.push(`/app/${appId}/database`)
	}

	const handleMenuItems = (
		event: React.MouseEvent<HTMLLIElement>,
		selectedItem: string
	) => {
		if (selectedItem === 'settings' && !isNil(settingsData)) {
			handleSettings(settingsData.id)
		}
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleShowMenu = (
		e: React.MouseEvent<SVGSVGElement>,
		item: AppState
	) => {
		setSettingsData(item)
		e.stopPropagation()
		setPopoverAnchorEl(e.currentTarget)
	}

	const handleViewItem = (app: AppState) => {
		goToAppView(app.id)
	}

	const handlePopoverClose = (event: any) => {
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleCreateApp = () => {
		dispatch(asActions.setCurrentState(SettingsCurrentState.LANDING))
		appsPreferenceData.status === HelpModuleStatus.INPROGRESS &&
			setTimeout(() => {
				dispatch(
					preferenceActions.updateAppsPreferences({
						currentStep: 5,
					})
				)
			}, 300)
	}

	const handleSettings = (appId: string) => {
		dispatch(asActions.setCurrentState(SettingsCurrentState.TABS_PAGE))
		dispatch(asActions.updateAppId(appId))
	}

	const handleSearchDebounce = debounce(() => {
		dispatch(fetchAllApps())
	}, 300)

	const handleOnSearch = (value: string) => {
		dispatch(appActions.setSearchString(value))
		handleSearchDebounce()
	}

	const handleAppsSortChange = (sortBy: string, isDesc: boolean) => {
		dispatch(appActions.setOrderBy(isDesc ? OrderBy.DESC : OrderBy.ASC))
		dispatch(appActions.setSortBy(sortBy))
		dispatch(fetchAllApps())
	}

	const handleBeforeChange = (currentStep: number) => {
		if (currentStep === 5) {
			dispatch(asActions.clearData(undefined))
			dispatch(asActions.setCurrentState(SettingsCurrentState.LANDING))
			dispatch(
				preferenceActions.updateAppsPreferences({
					currentStep: 5,
				})
			)
		} else if (currentStep === 4) {
			dispatch(asActions.clearData(undefined))
			dispatch(
				preferenceActions.updateAppsPreferences({
					currentStep: 4,
				})
			)
		}
		return true
	}

	const handleAppSettingsExit = (currentStep: number, type: string) => {
		if (currentStep === 8 && type === CustomButtonType.EXIT) {
			dispatch(
				preferenceActions.updateAppsPreferences({
					status: HelpModuleStatus.INCOMPLETE,
				})
			)
			dispatch(asActions.clearData(undefined))
		} else if (currentStep === 8 && type === CustomButtonType.COMPLETE) {
			dispatch(
				preferenceActions.updateAppsPreferences({
					currentStep: 6,
				})
			)
		}
	}

	const handleSteps = (currentStep: number, type?: string) => {
		if (appsPreferenceData.status === HelpModuleStatus.INPROGRESS) {
			if (type) {
				dispatch(
					preferenceActions.updateAppsPreferences({
						status: HelpModuleStatus.INCOMPLETE,
					})
				)
				dispatch(asActions.clearData(undefined))
				return
			}
			dispatch(
				preferenceActions.updateAppsPreferences({
					currentStep: currentStep,
				})
			)
		}
	}

	const handleTourExit = (currentStep: number) => {
		let currentStepStatus
		if (currentStep === stepsConfig.length - 1 && stepsConfig.length !== 9) {
			currentStepStatus = HelpModuleStatus.COMPLETED
		} else if (currentStep >= 0) {
			currentStepStatus = HelpModuleStatus.INCOMPLETE
		}
		if (currentStep !== -1) {
			dispatch(
				preferenceActions.updateAppsPreferences({
					status: currentStepStatus,
					currentStep: currentStep,
				})
			)
			dispatch(updatePreferences())
		}
	}

	const renderPopover = () => {
		if (isNil(popoverAnchorEl)) {
			return null
		}
		return (
			<IsyPopover
				anchorEl={popoverAnchorEl}
				actions={popoverActions}
				className={'pop-over-menu-list'}
				onClose={handlePopoverClose}
				onSelect={handleMenuItems}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			/>
		)
	}

	const renderSearch = () => {
		return (
			<IsySearch
				value={searchString}
				onChange={handleOnSearch}
				onCancel={handleOnSearch}
				placeholder={t(SEARCH_APPS)}
			/>
		)
	}

	const renderHeaderSection = () => {
		return (
			<div className='app-information'>
				<div className='title-holder'>
					<div className='app-title'>{t(APP)}</div>
					<div className='button-component'>
						<IsyButton
							className='primary-btn create-btn'
							onClick={handleCreateApp}
						>
							{t(CREATE_APP)}
						</IsyButton>
						{renderSearch()}
					</div>
				</div>
				<AppsBar />
			</div>
		)
	}

	const renderTableView = () => {
		return (
			<div className='grid-container'>
				<IsyGrid
					data={apps}
					config={gridConfig()}
					emptyMessage={t(NO_APPS_FOUND)}
					onRowClick={handleViewItem}
					sortBy={sortBy}
					isSortDesc={orderBy === OrderBy.DESC}
					onSortChange={handleAppsSortChange}
				/>
				{renderPopover()}
			</div>
		)
	}

	const renderMoreIcon = (item: AppState) => {
		return (
			<MoreVertIcon
				className='more-icon'
				onClick={(e) => handleShowMenu(e, item)}
			/>
		)
	}

	const renderGridVIew = () => {
		return (
			<AppCardGrid
				listOfApplications={apps}
				onItemSelect={handleViewItem}
				onEditApp={handleSettings}
			/>
		)
	}

	const renderApps = () => {
		if (viewType === AppsViewType.LIST) {
			return renderTableView()
		}
		return renderGridVIew()
	}

	const renderIsySteps = () => {
		return (
			<IsySteps
				steps={stepsConfig}
				isEnabled={
					appsPreferenceData &&
					appsPreferenceData.status === HelpModuleStatus.INPROGRESS
				}
				initialStep={appsPreferenceData && appsPreferenceData.currentStep}
				updateSteps={true}
				getCustomButtonData={handleAppSettingsExit}
				onBeforeChange={handleBeforeChange}
				onExit={handleTourExit}
			/>
		)
	}
	const renderAppsScreen = () => {
		return (
			<div className='app-container'>
				{renderHeaderSection()}
				{renderApps()}
			</div>
		)
	}

	const renderNoAppScreen = () => {
		return (
			<div className='app-page'>
				<div className='apps'>
					<img
						className='apps-icon'
						alt={t(CREATE_APP)}
						src='/images/Database.svg'
					/>
				</div>
				<div className='apps-text'>{t(APP_TEXT)}</div>
				<div>
					<IsyButton
						className='primary-btn create-btn'
						onClick={handleCreateApp}
					>
						{t(CREATE_APP)}
					</IsyButton>
				</div>
			</div>
		)
	}

	const renderAppSettings = () => {
		return (
			<AppSettings
				onGoToApplicationView={goToAppView}
				updateSteps={handleSteps}
				appsTourEnable={
					appsPreferenceData &&
					appsPreferenceData.status === HelpModuleStatus.INPROGRESS
				}
			/>
		)
	}

	const render = () => {
		if (appsPreferenceData && appsPreferenceData.currentStep === 8) {
			stepsConfig = stepsConfig.concat(getAppExitStep())
		}
		return (
			<>
				{renderAppSettings()}
				{apps.length === 0 && !filterTouched && !isLoading
					? renderNoAppScreen()
					: renderAppsScreen()}
				{renderIsySteps()}
			</>
		)
	}

	return render()
}
