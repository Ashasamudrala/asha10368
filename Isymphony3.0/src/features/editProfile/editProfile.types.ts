export interface EditProfileState {
	email: string
	firstName: string
	lastName: string
	title: string
	phoneNumber: number | null
	address: string | null
	roleIds: string[]
	profilePic: string
}
