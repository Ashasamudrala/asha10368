import { createSlice } from '@reduxjs/toolkit'
import { isNil } from 'lodash'
import { Action } from '../../common.types'
import { EditProfileState } from './editProfile.types'
import * as asyncActions from './editProfile.asyncActions'
import { sliceName } from './editProfile.selectors'

const initialState: EditProfileState = {
	email: '',
	firstName: '',
	lastName: '',
	title: '',
	phoneNumber: null,
	address: null,
	roleIds: [],
	profilePic: '',
}

const slice = createSlice({
	name: sliceName,
	initialState,
	reducers: {
		updateState: (_, action: Action<EditProfileState>) => action.payload,
		clearState: () => initialState,
	},
	extraReducers: (builder) => {
		// asynchronous actions
		builder.addCase(
			asyncActions.fetchUserDetails.fulfilled,
			(state: EditProfileState, action) => {
				if (!isNil(action.payload)) {
					state.email = action.payload.email
					state.firstName = action.payload.firstName
					state.lastName = action.payload.lastName
					state.title = action.payload.title
					state.phoneNumber = action.payload.phoneNumber
					state.address = action.payload.address
					state.roleIds = action.payload.roles.map((role: any) => role.id)
					state.profilePic = action.payload.profilePic
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
