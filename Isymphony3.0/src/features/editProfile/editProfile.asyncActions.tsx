import { createAsyncThunk } from '@reduxjs/toolkit'
import { pick } from 'lodash'
import { RootState } from '../../base.types'
import doAsync from '../../infrastructure/doAsync'
import { doAsyncUpload } from '../../infrastructure/doAsync/doAsync'
import {
	USER_GET_PROFILE,
	USER_UPDATE_PROFILE_PIC,
} from '../../utilities/apiEndpoints'
import { EDIT_PROFILE_SUCCESS_MESSAGE } from '../../utilities/constants'
import { selectEditProfileState } from './editProfile.selectors'

export const fetchUserDetails = createAsyncThunk(
	'users/get',
	async (userId: string, thunkArgs) =>
		await doAsync({
			url: USER_GET_PROFILE(userId),
			errorMessage: 'Unable to load profile. Please try again later.',
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const updateProfileById = createAsyncThunk(
	'users/update',
	async (userId: string, thunkArgs) => {
		const stateInfo = selectEditProfileState(thunkArgs.getState() as RootState)
		await doAsync({
			url: USER_GET_PROFILE(userId),
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify(
					pick(stateInfo, [
						'firstName',
						'lastName',
						'phoneNumber',
						'address',
						'roleIds',
					])
				),
			},
			noBusySpinner: true,
			successMessage: `${EDIT_PROFILE_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
	}
)

export const updateProfilePicById = createAsyncThunk(
	'users/updatePic',
	async ({ fileData, userId }: { fileData: File; userId: string }) => {
		await doAsyncUpload(USER_UPDATE_PROFILE_PIC(userId), fileData)
	}
)
