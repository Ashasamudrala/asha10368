import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import {
	updateProfileById,
	updateProfilePicById,
	fetchUserDetails,
} from './editProfile.asyncActions'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { IsyButton } from '../../widgets/IsyButton/IsyButton'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import { useTranslation } from 'react-i18next'
import { IsyAvatarImage } from '../../widgets/IsyAvatarImage/IsyAvatarImage'
import {
	BUTTON_SAVE,
	BUTTON_CANCEL,
	PROFILE_ERROR,
	EMAIL,
	PHONE,
	ADDRESS,
	TEAM_TRANSLATIONS,
	BUTTON_EDIT_PROFILE,
} from '../../utilities/constants'
import './editProfile.scss'
import { selectEditProfileState } from './editProfile.selectors'
import { actions } from './editProfile.slice'
import {
	IsyFormBuilder,
	IsyFormBuilderFormTypes,
	IsyFormBuilderInputItemProps,
} from '../../widgets/IsyFormBuilder/IsyFormBuilder'
import { EditProfileState } from './editProfile.types'
import { get, isNil } from 'lodash'
import { Divider, Typography } from '@material-ui/core'
import { IsyPermissionTypes } from '../../authAndPermissions/loginUserDetails.types'
import { IsyCan } from '../../widgets/IsyCan/IsyCan'

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: 'transparent',
		display: 'flex',
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
		margin: 'auto',
	},
	listItemText: {
		textAlign: 'center',
		fontSize: '24px',
	},
}))

export interface EditProfileProps {
	userId: string
	onClose: () => void
}

export function EditProfile(props: EditProfileProps) {
	const { userId } = props
	const userDetails = useSelector(selectEditProfileState)

	const { t } = useTranslation(TEAM_TRANSLATIONS)
	const classes = useStyles()
	const [selectedFile, setSelectedFile] = useState<File | null>(null)
	const [isEdit, setIsEdit] = useState<boolean>(false)
	const hiddenFileInput = React.useRef<HTMLInputElement>(null)
	const [uploadError, setUploadError] = useState<string>('')
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(fetchUserDetails(userId))
		return () => {
			dispatch(actions.clearState())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	// empty array is important to say its should happen on mount.

	const getImageLetters = () => {
		const fl = userDetails.firstName || ''
		const ll = userDetails.lastName || ''
		return fl + ' ' + ll
	}

	const getFileUrl = () => {
		if (isNil(selectedFile)) {
			return null
		}
		return URL.createObjectURL(selectedFile)
	}

	const getFormBuilderConfig = (): IsyFormBuilderInputItemProps[] => {
		return [
			{
				type: IsyFormBuilderFormTypes.STRING,
				title: t(EMAIL),
				dataRef: 'email',
				disabled: true,
				isRequired: true,
			},
			{
				type: IsyFormBuilderFormTypes.NUMBER,
				title: t(PHONE),
				placeholder: t(PHONE),
				dataRef: 'phoneNumber',
			},
			{
				type: IsyFormBuilderFormTypes.STRING,
				title: t(ADDRESS),
				placeholder: t(ADDRESS),
				dataRef: 'address',
			},
		]
	}

	const handleSave = () => {
		dispatch(updateProfileById(userId))
		if (!isNil(selectedFile)) {
			dispatch(
				updateProfilePicById({
					fileData: selectedFile,
					userId: userId,
				})
			)
		}
		props.onClose()
	}

	const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const files = event.target.files
		if (!isNil(files) && files.length > 0) {
			if (files[0].size < 1000000) {
				setSelectedFile(files[0])
				setUploadError('')
			} else {
				setUploadError(t(PROFILE_ERROR))
			}
		}
	}

	const handleClick = () => {
		if (!isNil(hiddenFileInput) && !isNil(hiddenFileInput.current)) {
			hiddenFileInput.current.click()
		}
	}

	const handleDataChange = (data: EditProfileState) => {
		dispatch(actions.updateState(data))
	}

	const handleEditProfile = () => {
		setIsEdit(true)
	}

	const renderBottom = () => {
		if (isEdit) {
			return (
				<DialogActions className='actions-btn'>
					<IsyButton onClick={handleSave} className='primary-btn'>
						{t(BUTTON_SAVE)}
					</IsyButton>
					<IsyButton onClick={props.onClose} className='standard-btn'>
						{t(BUTTON_CANCEL)}
					</IsyButton>
				</DialogActions>
			)
		}
		return null
	}

	const renderEditContent = () => {
		return (
			<DialogContent dividers>
				<div className='dynamic-form-input'>
					<IsyFormBuilder<EditProfileState>
						forms={getFormBuilderConfig()}
						data={userDetails}
						onChange={handleDataChange}
					/>
				</div>
			</DialogContent>
		)
	}

	const renderEditButton = () => {
		return (
			<IsyCan
				action={IsyPermissionTypes.UPDATE_USER_PROFILE}
				yes={() => (
					<>
						<Divider className='form-divider' variant='middle' />
						<IsyButton className='edit-btn' onClick={handleEditProfile}>
							{t(BUTTON_EDIT_PROFILE)}
						</IsyButton>
						<Divider className='form-divider' variant='middle' />
					</>
				)}
			/>
		)
	}

	const renderViewData = () => {
		return getFormBuilderConfig().map(
			(config: IsyFormBuilderInputItemProps, index: number) => (
				<div key={index}>
					<Typography className='view-header'>{config.title}</Typography>
					<Typography className='view-content'>
						{get(userDetails, config.dataRef)}
					</Typography>
					<Divider className='form-divider' />
				</div>
			)
		)
	}

	const renderViewContent = () => {
		return <DialogContent>{renderViewData()}</DialogContent>
	}

	const renderUserAvatar = () => {
		return (
			<div className='profile' onClick={handleClick}>
				{isEdit && (
					<input
						id='file'
						type='file'
						onChange={handleFileChange}
						accept='image/*'
						ref={hiddenFileInput}
						className='file-input'
					/>
				)}
				<IsyAvatarImage
					imageClassName={classes.large}
					imageData={userDetails.profilePic}
					defaultName={getImageLetters()}
					fileUrl={getFileUrl()}
				/>
				{isEdit && uploadError && <p className='file-error'>{uploadError}</p>}
			</div>
		)
	}

	const renderHeader = () => {
		return (
			<DialogTitle>
				<CloseOutlinedIcon className='close-icon' onClick={props.onClose} />
				{renderUserAvatar()}
				<List>
					<ListItem alignItems='flex-start'>
						<ListItemText
							classes={{ primary: classes.listItemText }}
							primary={userDetails.firstName + ' ' + userDetails.lastName}
							secondary={userDetails.title}
						/>
					</ListItem>
				</List>
			</DialogTitle>
		)
	}

	const render = () => {
		return (
			<Dialog
				onClose={props.onClose}
				open={true}
				className='editProfile'
				BackdropProps={{
					classes: {
						root: classes.root,
					},
				}}
			>
				{renderHeader()}
				{!isEdit && renderEditButton()}
				{isEdit ? renderEditContent() : renderViewContent()}
				{renderBottom()}
			</Dialog>
		)
	}

	return render()
}
