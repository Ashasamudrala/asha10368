import React from 'react'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { isNil } from 'lodash'
import {
	PLATFORM_TRANSLATIONS,
	BUTTON_CANCEL,
	BUTTON_NEXT,
} from '../../../utilities/constants'
import { getSelectedService } from './selectThirdPartyService.selectors'
import { SelectedThirdServiceReturnProps } from './selectThirdPartyService.types'

export interface SelectThirdPartyServiceActionsProps {
	onOkay: (data: SelectedThirdServiceReturnProps) => void
	onCancel: () => void
}

export function SelectThirdPartyServiceActions(
	props: SelectThirdPartyServiceActionsProps
) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const selectedService = useSelector(getSelectedService)
	const handleOkay = () => {
		if (!isNil(selectedService)) {
			props.onOkay({ service: selectedService })
		}
	}
	const handleCancel = () => {
		props.onCancel()
	}

	const render = () => {
		return (
			<>
				<IsyButton onClick={handleCancel} className='standard-btn'>
					{t(BUTTON_CANCEL)}
				</IsyButton>
				<IsyButton
					className='primary-btn'
					disabled={isNil(selectedService)}
					onClick={handleOkay}
				>
					{t(BUTTON_NEXT)}
				</IsyButton>
			</>
		)
	}

	return render()
}
