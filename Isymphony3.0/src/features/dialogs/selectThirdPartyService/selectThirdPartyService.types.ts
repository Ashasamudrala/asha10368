export interface SelectedThirdServiceReturnProps {
	service: string
}

export interface SelectedServiceState {
	selectedService: string | null
}
