import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectOrderedDialogs, selectDialogsMap } from './dialogs.selectors'
import { actions } from './dialogs.slice'
import { isNil, isString } from 'lodash'
import { useTranslation } from 'react-i18next'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import {
	CONFIRMATION,
	PLATFORM_TRANSLATIONS,
	FOREIGN_KEY_DIALOG_HEADER,
	ERROR,
	ADD_DATABASE_SCHEMA,
	INVITE_MEMBER,
	SELECT_WEB_SERVICE,
	HELP_HEADING,
	TERMS_AND_CONDITIONS_HEADER,
} from '../../utilities/constants'
import './dialogs.scss'
import { ConfirmationContent } from './confirmation/ConfirmationContent'
import { ConfirmationActions } from './confirmation/ConfirmationActions'
import { ConnectForeignKeyContent } from './connectForeignKey/ConnectForeignKeyContent'
import { ConnectForeignKeyActions } from './connectForeignKey/ConnectForeignKeyActions'
import { AlertContent } from './alert/AlertContent'
import { AlertActions } from './alert/AlertActions'
import { DialogProps, DialogTypes } from './dialogs.types'
import { AlertDataProps } from './alert/alert.types'
import { ConfirmationDataProps } from './confirmation/confirmation.types'
import { ConnectForeignKeyDataProps } from './connectForeignKey/connectForeignKey.types'
import { InviteUsersContent } from './inviteUsers/InviteUsersContent'
import { InviteUsersDataProps } from './inviteUsers/inviteUsers.types'
import { InviteUsersActions } from './inviteUsers/InviteUsersActions'
import { clearSubReducersData } from './dialogs.async'
import { SelectThirdPartyServiceActions } from './selectThirdPartyService/SelectThirdPartyServiceActions'
import { SelectThirdPartyServiceContent } from './selectThirdPartyService/SelectThirdPartyServiceContent'
import { HelpLandingPageContent } from './helpLandingPage/HelpLandingPageContent'
import { HelpLandingPageActions } from './helpLandingPage/HelpLandingPageActions'
import { AboutPage } from './aboutPage/aboutPage'
import { HelpLandingDataProps } from './helpLandingPage/helpLanding.types'
import {
	selectGetDatabasePreferenceStatus,
	selectGetWebServicePreferenceStatus,
} from '../../authAndPermissions/loginUserDetails.selectors'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'
import { TermsAndConditions } from './termsAndConditions/TermsAndConditions'
import { AddDatabaseSchemaContent } from './addDatabaseSchema/AddDatabaseSchemaContent'
import { AddDatabaseSchemaActions } from './addDatabaseSchema/AddDatabaseSchemaActions'

export function Dialogs() {
	const dialogIds = useSelector(selectOrderedDialogs)
	const dialogsMap = useSelector(selectDialogsMap)
	const databasePreferenceData = useSelector(selectGetDatabasePreferenceStatus)
	const webServicePreferenceData = useSelector(
		selectGetWebServicePreferenceStatus
	)

	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()

	const handleCloseHelpTour = () => {
		if (databasePreferenceData.status === HelpModuleStatus.INPROGRESS) {
			dispatch(
				preferenceActions.updateDatabasePreferences({
					currentStep: databasePreferenceData.currentStep,
					status: HelpModuleStatus.INCOMPLETE,
				})
			)
		} else if (
			webServicePreferenceData.status === HelpModuleStatus.INPROGRESS
		) {
			dispatch(
				preferenceActions.updateWebServicesPreferences({
					currentStep: webServicePreferenceData.currentStep,
					status: HelpModuleStatus.INCOMPLETE,
				})
			)
		}
	}

	const handleClose = (id: string) => {
		dispatch(clearSubReducersData(id))
		dispatch(actions.removeDialog({ id, eventType: 'CLOSE' }))
		handleCloseHelpTour()
	}
	const handleCancel = (id: string) => {
		dispatch(clearSubReducersData(id))
		dispatch(actions.removeDialog({ id, eventType: 'CANCEL' }))
		handleCloseHelpTour()
	}
	const handleOkay = (id: string, data: any) => {
		dispatch(clearSubReducersData(id))
		dispatch(actions.removeDialog({ id, eventType: 'OKAY', data }))
	}

	const getDialogClasses = (dialog: DialogProps) => {
		switch (dialog.type) {
			case DialogTypes.INVITE_USERS:
				return 'dialog-box-invite-users'
			case DialogTypes.SELECT_WEB_SERVICE:
				return 'web-service-dialog'
			case DialogTypes.HELP_LANDING_PAGE:
				return 'help-landing-page-dialog'
			case DialogTypes.ABOUT:
				return 'about-page-dialog'
			case DialogTypes.TERMS_AND_CONDITIONS:
				return 'terms-and-conditions-page-dialog'
			default:
				return ''
		}
	}

	const getTitle = (dialog: DialogProps) => {
		if (isString(dialog.title)) {
			return dialog.title
		}
		switch (dialog.type) {
			case DialogTypes.CONFIRMATION_DIALOG:
				return t(CONFIRMATION)
			case DialogTypes.CONNECT_FOREIGN_KEY:
				return t(FOREIGN_KEY_DIALOG_HEADER)
			case DialogTypes.ALERT:
				return t(ERROR)
			case DialogTypes.ADD_DATABASE_SCHEMA:
				return t(ADD_DATABASE_SCHEMA)
			case DialogTypes.INVITE_USERS:
				return t(INVITE_MEMBER)
			case DialogTypes.SELECT_WEB_SERVICE:
				return t(SELECT_WEB_SERVICE)
			case DialogTypes.HELP_LANDING_PAGE:
				return t(HELP_HEADING)
			case DialogTypes.TERMS_AND_CONDITIONS:
				return t(TERMS_AND_CONDITIONS_HEADER)
			case DialogTypes.ABOUT:
				return ''
			default:
				return 'iSymphony Studio'
		}
	}

	const renderTitle = (id: string, dialog: DialogProps) => {
		return (
			<DialogTitle>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => handleClose(id)}
				/>
				<span className='dialog-heading'>{getTitle(dialog)}</span>
			</DialogTitle>
		)
	}

	const renderContent = (_: string, dialog: DialogProps) => {
		switch (dialog.type) {
			case DialogTypes.ALERT:
				return <AlertContent dialogData={dialog.data as AlertDataProps} />
			case DialogTypes.CONFIRMATION_DIALOG:
				return (
					<ConfirmationContent
						dialogData={dialog.data as ConfirmationDataProps}
					/>
				)
			case DialogTypes.CONNECT_FOREIGN_KEY:
				return (
					<ConnectForeignKeyContent
						dialogData={dialog.data as ConnectForeignKeyDataProps}
					/>
				)
			case DialogTypes.INVITE_USERS:
				return (
					<InviteUsersContent
						dialogData={dialog.data as InviteUsersDataProps}
					/>
				)
			case DialogTypes.ADD_DATABASE_SCHEMA:
				return <AddDatabaseSchemaContent />
			case DialogTypes.SELECT_WEB_SERVICE:
				return <SelectThirdPartyServiceContent />
			case DialogTypes.HELP_LANDING_PAGE:
				return (
					<HelpLandingPageContent
						dialogData={dialog.data as HelpLandingDataProps}
					/>
				)
			case DialogTypes.ABOUT:
				return <AboutPage />
			case DialogTypes.TERMS_AND_CONDITIONS:
				return <TermsAndConditions />
			default:
				return null
		}
	}

	const renderActions = (id: string, dialog: DialogProps) => {
		switch (dialog.type) {
			case DialogTypes.ALERT:
				return (
					<AlertActions
						dialogData={dialog.data as AlertDataProps}
						onOkay={(data: any) => handleOkay(id, data)}
					/>
				)
			case DialogTypes.CONNECT_FOREIGN_KEY:
				return (
					<ConnectForeignKeyActions
						onOkay={(data: any) => handleOkay(id, data)}
						onCancel={() => handleCancel(id)}
					/>
				)
			case DialogTypes.CONFIRMATION_DIALOG:
				return (
					<ConfirmationActions
						dialogData={dialog.data as ConfirmationDataProps}
						onOkay={(data: any) => handleOkay(id, data)}
						onCancel={() => handleCancel(id)}
					/>
				)
			case DialogTypes.ADD_DATABASE_SCHEMA:
				return (
					<AddDatabaseSchemaActions
						onOkay={(data: any) => handleOkay(id, data)}
						onCancel={() => handleCancel(id)}
					/>
				)
			case DialogTypes.INVITE_USERS:
				return (
					<InviteUsersActions onOkay={(data: any) => handleOkay(id, data)} />
				)
			case DialogTypes.SELECT_WEB_SERVICE:
				return (
					<SelectThirdPartyServiceActions
						onOkay={(data: any) => handleOkay(id, data)}
						onCancel={() => handleCancel(id)}
					/>
				)
			case DialogTypes.HELP_LANDING_PAGE:
				return (
					<HelpLandingPageActions
						dialogData={dialog.data as HelpLandingDataProps}
						onOkay={(data: any) => handleOkay(id, data)}
						onCancel={() => handleCancel(id)}
					/>
				)
			default:
				return null
		}
	}

	const renderDialog = (id: string) => {
		const dialog = dialogsMap[id]
		if (isNil(dialog)) {
			return null
		}
		return (
			<Dialog
				open={true}
				className={'dialog-box-common ' + getDialogClasses(dialog)}
				// onClose={() => handleClose(id)}
				classes={{ paper: dialog.className }}
				key={id}
			>
				{renderTitle(id, dialog)}
				<DialogContent>{renderContent(id, dialog)}</DialogContent>
				<DialogActions className='actions-btn'>
					{renderActions(id, dialog)}
				</DialogActions>
			</Dialog>
		)
	}

	return (
		<>
			{dialogIds.map((id) => {
				return renderDialog(id)
			})}
		</>
	)
}
