import React from 'react'
import { useTranslation } from 'react-i18next'
import {
	TERMS_AND_CONDITIONS_TRANSLATIONS,
	TERMS_AND_CONDITIONS_DESCRIPTION,
} from '../../../utilities/constants'
import './termsAndConditions.scss'

export function TermsAndConditions() {
	const { t } = useTranslation(TERMS_AND_CONDITIONS_TRANSLATIONS)

	const render = () => {
		return (
			<div className='terms-and-conditions-section'>
				{t(TERMS_AND_CONDITIONS_DESCRIPTION)}
			</div>
		)
	}
	return render()
}
