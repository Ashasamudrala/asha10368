import { DialogsState } from './dialogs.types'
import { ConnectForeignKeyState } from './connectForeignKey/connectForeignKey.types'
import { InviteUsersState } from './inviteUsers/inviteUsers.types'
import { SelectedServiceState } from './selectThirdPartyService/selectThirdPartyService.types'
import { AddDatabaseSchemaState } from './addDatabaseSchema/addDatabaseSchema.types'

export enum DialogsSubReducersNames {
	DIALOGS = 'dialogs',
	CREATE_DATABASE = 'createDatabase',
	ADD_DATABASE_SCHEMA = 'addDatabaseSchema',
	CONNECT_FOREIGN_KEY = 'connectForeignKey',
	INVITE_USERS = 'inviteUsers',
	THIRD_PARTY_SERVICE = 'thirdPartyService',
}

export interface DialogsBaseState {
	[DialogsSubReducersNames.DIALOGS]: DialogsState
	[DialogsSubReducersNames.ADD_DATABASE_SCHEMA]: AddDatabaseSchemaState
	[DialogsSubReducersNames.CONNECT_FOREIGN_KEY]: ConnectForeignKeyState
	[DialogsSubReducersNames.INVITE_USERS]: InviteUsersState
	[DialogsSubReducersNames.THIRD_PARTY_SERVICE]: SelectedServiceState
}
