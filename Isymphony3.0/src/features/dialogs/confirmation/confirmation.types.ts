export interface ConfirmationDataProps {
	message: string
	okayButtonLabel?: string
	cancelButtonLabel?: string
}
