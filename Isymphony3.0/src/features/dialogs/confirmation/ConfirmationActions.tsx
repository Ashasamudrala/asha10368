import React from 'react'
import { isString } from 'lodash'
import { useTranslation } from 'react-i18next'
import {
	BUTTON_YES,
	BUTTON_NO,
	PLATFORM_TRANSLATIONS,
} from '../../../utilities/constants'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { ConfirmationDataProps } from './confirmation.types'

export interface ConfirmationActionsProps {
	dialogData: ConfirmationDataProps
	onOkay: (data?: any) => void
	onCancel: () => void
}

export function ConfirmationActions(props: ConfirmationActionsProps) {
	const { dialogData } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const getYesButtonLabel = () => {
		if (isString(dialogData.okayButtonLabel)) {
			return dialogData.okayButtonLabel
		}
		return t(BUTTON_YES)
	}
	const getCancelButtonLabel = () => {
		if (isString(dialogData.cancelButtonLabel)) {
			return dialogData.cancelButtonLabel
		}
		return t(BUTTON_NO)
	}

	const handleCancel = () => {
		props.onCancel()
	}

	const handleOkay = () => {
		props.onOkay()
	}

	return (
		<>
			<IsyButton onClick={handleCancel} className='standard-btn'>
				{getCancelButtonLabel()}
			</IsyButton>
			<IsyButton onClick={handleOkay} className='primary-btn'>
				{getYesButtonLabel()}
			</IsyButton>
		</>
	)
}
