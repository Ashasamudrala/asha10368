import { combineReducers } from 'redux'
import { Dialogs } from './Dialogs'
import * as selectors from './base.selector'
import * as dialogs from './dialogs.slice'
import * as addDatabaseSchema from './addDatabaseSchema/addDatabaseSchema.slice'
import * as connectForeignKey from './connectForeignKey/connectForeignKey.slice'
import * as inviteUsers from './inviteUsers/inviteUsers.slice'
import * as thirdPartyService from './selectThirdPartyService/selectThirdPartyService.slice'
import { DialogsBaseState } from './base.types'

export const reducer = combineReducers<DialogsBaseState>({
	[dialogs.name]: dialogs.reducer,
	[connectForeignKey.name]: connectForeignKey.reducer,
	[addDatabaseSchema.name]: addDatabaseSchema.reducer,
	[inviteUsers.name]: inviteUsers.reducer,
	[thirdPartyService.name]: thirdPartyService.reducer,
})

export * from './dialogs.types'
export const { showDialog } = dialogs.actions
export const name = selectors.sliceName

// we export the component most likely to be desired by default
export default Dialogs
