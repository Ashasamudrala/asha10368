import React from 'react'
import { DialogContentText } from '@material-ui/core'
import { AlertDataProps } from './alert.types'

export interface AlertContentProps {
	dialogData: AlertDataProps
}

export function AlertContent(props: AlertContentProps) {
	const { dialogData } = props
	return (
		<DialogContentText>
			<span className='content'>{dialogData.message}</span>
		</DialogContentText>
	)
}
