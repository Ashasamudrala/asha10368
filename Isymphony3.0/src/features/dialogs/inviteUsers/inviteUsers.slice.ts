import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { DialogsSubReducersNames } from '../base.types'
import { InviteUserProps, InviteUsersState } from './inviteUsers.types'

const initialState: InviteUsersState = {
	members: [],
}

const slice = createSlice<
	InviteUsersState,
	SliceCaseReducers<InviteUsersState>,
	DialogsSubReducersNames.INVITE_USERS
>({
	name: DialogsSubReducersNames.INVITE_USERS,
	initialState,
	reducers: {
		// synchronous actions
		addMember(state: InviteUsersState, action: Action<InviteUserProps>) {
			state.members = [...state.members, action.payload]
		},
		removeMember(state: InviteUsersState, action: Action<number>) {
			const list = state.members
			list.splice(action.payload, 1)
			state.members = [...list]
		},
		updateMemberRole(
			state: InviteUsersState,
			action: Action<{ index: number; roleId: string }>
		) {
			const list = state.members
			list[action.payload.index] = {
				...list[action.payload.index],
				roleId: action.payload.roleId,
			}
			state.members = [...list]
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
