import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import {
	ADD_LINK,
	DUPLICATE_EMAIL,
	EMAIL,
	EMPTY_ROLE,
	INVALID_EMAIL,
	INVITE_USER_TRANSFORMATIONS,
	ROLE,
	VALIDATE_EMAIL,
} from '../../../utilities/constants'
import { actions } from './inviteUsers.slice'
import './inviteUsers.scss'
import { InviteUserProps, InviteUsersDataProps } from './inviteUsers.types'
import { getInviteUsersMembers } from './inviteUsers.selectors'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import {
	IsyFormBuilder,
	IsyFormBuilderFormTypes,
	IsyFormBuilderInputItemProps,
} from '../../../widgets/IsyFormBuilder/IsyFormBuilder'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { findIndex, isEmpty } from 'lodash'
import { IsyDropDown } from '../../../widgets/IsyDropDown/IsyDropDown'

export interface InviteUsersContentProps {
	dialogData: InviteUsersDataProps
}

export function InviteUsersContent(props: InviteUsersContentProps) {
	const { dialogData } = props
	const { t } = useTranslation(INVITE_USER_TRANSFORMATIONS)
	const members = useSelector(getInviteUsersMembers)
	const [inviteUserMember, setInviteUserMember] = useState<InviteUserProps>({
		email: '',
		roleId: dialogData.roles.length > 0 ? dialogData.roles[0].id : '',
	})
	const [errors, setErrors] = useState<Partial<InviteUserProps>>({})
	const dispatch = useDispatch()

	const getFormBuilderConfig = (): IsyFormBuilderInputItemProps[] => {
		return [
			{
				type: IsyFormBuilderFormTypes.STRING,
				title: t(EMAIL),
				placeholder: t(EMAIL),
				dataRef: 'email',
				isRequired: true,
				className: 'email-input',
			},
			{
				type: IsyFormBuilderFormTypes.SELECT,
				title: t(ROLE),
				dataRef: 'roleId',
				options: dialogData.roles,
				className: 'role-input',
			},
		]
	}

	const emailIsValid = (email: string) => {
		return email.match(/^[\w-.]+@([\w-]+\.)+[\w-]{2,3}$/)
	}

	const handleValidations = () => {
		const errors: Partial<InviteUserProps> = {}
		if (isEmpty(inviteUserMember.email)) {
			errors.email = t(VALIDATE_EMAIL)
		} else if (!emailIsValid(inviteUserMember.email)) {
			errors.email = t(INVALID_EMAIL)
		} else if (
			findIndex(members, (e) => e.email === inviteUserMember.email) !== -1
		) {
			errors.email = t(DUPLICATE_EMAIL)
		}
		if (isEmpty(inviteUserMember.roleId)) {
			errors.roleId = t(EMPTY_ROLE)
		}
		setErrors({ ...errors })
		return isEmpty(errors)
	}

	const handleFormDataChange = (data: InviteUserProps) => {
		setInviteUserMember(data)
	}

	const handleAddMember = () => {
		if (handleValidations()) {
			dispatch(actions.addMember(inviteUserMember))
			setInviteUserMember({
				...inviteUserMember,
				email: '',
			})
		}
	}

	const handleRemoveMember = (index: number) => {
		dispatch(actions.removeMember(index))
	}

	const handleUpdateMemberRole = (roleId: string, index: number) => {
		dispatch(actions.updateMemberRole({ roleId, index }))
	}

	const renderListOfMember = (invitedUser: InviteUserProps, index: number) => {
		return (
			invitedUser && (
				<div className='invited-user-list' key={invitedUser.email}>
					<span className='email-list-class' title={invitedUser.email}>
						{invitedUser.email}
					</span>
					<span className='dropdown-class'>
						<IsyDropDown<string>
							options={dialogData.roles}
							onChange={(value: string) => handleUpdateMemberRole(value, index)}
							className='role'
							value={invitedUser.roleId}
						/>
					</span>
					<CloseOutlinedIcon
						className='close-icon-invite-user'
						onClick={() => handleRemoveMember(index)}
					/>
				</div>
			)
		)
	}

	const renderMembersGrid = () => {
		return (
			<div className='invited-user-list-container'>
				{members.map(renderListOfMember)}
			</div>
		)
	}

	const renderForm = () => {
		return (
			<IsyFormBuilder<InviteUserProps>
				forms={getFormBuilderConfig()}
				data={inviteUserMember}
				errors={errors}
				onChange={handleFormDataChange}
			/>
		)
	}

	const renderAddButton = () => {
		return (
			<div className={'add-link-container'}>
				<IsyButton className='add-link' onClick={handleAddMember}>
					<AddOutlinedIcon />
					{t(ADD_LINK)}
				</IsyButton>
			</div>
		)
	}

	return (
		<div className='invite-users'>
			{renderForm()}
			{renderAddButton()}
			{renderMembersGrid()}
		</div>
	)
}
