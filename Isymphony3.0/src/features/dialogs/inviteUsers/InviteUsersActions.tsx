import React from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	INVITE_USER_TRANSFORMATIONS,
	SEND_LINK,
} from '../../../utilities/constants'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { InviteUsersReturnProps } from './inviteUsers.types'
import { getInviteUsersMembers } from './inviteUsers.selectors'

export interface InviteUsersActionsProps {
	onOkay: (data: InviteUsersReturnProps) => void
}

export function InviteUsersActions(props: InviteUsersActionsProps) {
	const { t } = useTranslation(INVITE_USER_TRANSFORMATIONS)
	const members = useSelector(getInviteUsersMembers)

	const handleOkay = () => {
		props.onOkay({ members })
	}

	const render = () => {
		if (members.length === 0) {
			return null
		}
		return (
			<IsyButton className='primary-btn' onClick={handleOkay}>
				{t(SEND_LINK)}
			</IsyButton>
		)
	}

	return render()
}
