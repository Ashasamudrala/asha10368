export interface AddDatabaseSchemaReturnProps {
	name: string
}

export enum AddDatabaseSchemaErrorType {
	EMPTY,
	INVALID,
	INVALID_STARTS,
	LENGTH_EXCEEDS,
	NONE,
}

export interface AddDatabaseSchemaState {
	name: string
	error: AddDatabaseSchemaErrorType
}
