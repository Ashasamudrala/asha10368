import slice from './addDatabaseSchema.slice'
import { selectSlice as dialogSelect } from '../base.selector'
import { RootState } from '../../../base.types'
import {
	AddDatabaseSchemaErrorType,
	AddDatabaseSchemaState,
} from './addDatabaseSchema.types'

export const selectSlice = (state: RootState): AddDatabaseSchemaState =>
	dialogSelect(state)[slice.name]

export const getSchemaName = (state: RootState): string =>
	selectSlice(state).name

export const getSchemaNameError = (
	state: RootState
): AddDatabaseSchemaErrorType => selectSlice(state).error
