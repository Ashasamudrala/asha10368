import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	BUTTON_CANCEL,
	DATABASE_SCHEMA_ADD,
} from '../../../utilities/constants'
import { isEmpty } from 'lodash'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import { getSchemaName } from './addDatabaseSchema.selectors'
import {
	AddDatabaseSchemaErrorType,
	AddDatabaseSchemaReturnProps,
} from './addDatabaseSchema.types'
import { actions } from './addDatabaseSchema.slice'

export interface AddDatabaseSchemaActionProps {
	onOkay: (data: AddDatabaseSchemaReturnProps) => void
	onCancel: () => void
}

export function AddDatabaseSchemaActions(props: AddDatabaseSchemaActionProps) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const name = useSelector(getSchemaName)
	const dispatch = useDispatch()

	const handleCancel = () => {
		props.onCancel()
	}

	const handleOkay = () => {
		if (isEmpty(name)) {
			return dispatch(actions.setError(AddDatabaseSchemaErrorType.EMPTY))
		}
		dispatch(actions.setError(AddDatabaseSchemaErrorType.NONE))
		props.onOkay({
			name,
		})
	}

	return (
		<>
			<IsyButton onClick={handleCancel} className='standard-btn'>
				{t(BUTTON_CANCEL)}
			</IsyButton>
			<IsyButton
				disabled={isEmpty(name) ? true : false}
				className='primary-btn'
				onClick={handleOkay}
			>
				{t(DATABASE_SCHEMA_ADD)}
			</IsyButton>
		</>
	)
}
