import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import { DialogsSubReducersNames } from '../base.types'
import {
	AddDatabaseSchemaErrorType,
	AddDatabaseSchemaState,
} from './addDatabaseSchema.types'

const initialState = {
	name: '',
	error: AddDatabaseSchemaErrorType.NONE,
}

const slice = createSlice<
	AddDatabaseSchemaState,
	SliceCaseReducers<AddDatabaseSchemaState>,
	DialogsSubReducersNames.ADD_DATABASE_SCHEMA
>({
	name: DialogsSubReducersNames.ADD_DATABASE_SCHEMA,
	initialState,
	reducers: {
		// synchronous actions
		setName(state: AddDatabaseSchemaState, action: Action<string>) {
			state.name = action.payload
		},
		setError(
			state: AddDatabaseSchemaState,
			action: Action<AddDatabaseSchemaErrorType>
		) {
			state.error = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
