import React from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import { Typography } from '@material-ui/core'
import {
	SCHEMA_NAME,
	DATABASE_TRANSLATIONS,
	SCHEMA_PLACEHOLDER_NAME,
	VALIDATE_NAME,
} from '../../../utilities/constants'
import { IsyInput } from '../../../widgets/IsyInput/IsyInput'
import { actions } from './addDatabaseSchema.slice'
import './addDatabaseSchema.scss'
import {
	getSchemaName,
	getSchemaNameError,
} from './addDatabaseSchema.selectors'
import { AddDatabaseSchemaErrorType } from './addDatabaseSchema.types'

export function AddDatabaseSchemaContent() {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const name = useSelector(getSchemaName)
	const error = useSelector(getSchemaNameError)
	const dispatch = useDispatch()

	const getErrorLabel = () => {
		switch (error) {
			case AddDatabaseSchemaErrorType.EMPTY:
				return t(VALIDATE_NAME)
			default:
				return ''
		}
	}

	const handleNameChange = (value: string) => {
		dispatch(actions.setName(value))
	}

	return (
		<div className='addDatabaseSchema'>
			<Typography>{t(SCHEMA_NAME)}</Typography>
			<IsyInput
				type='text'
				className={
					error === AddDatabaseSchemaErrorType.NONE ? '' : 'error-input'
				}
				placeholder={t(SCHEMA_PLACEHOLDER_NAME)}
				onChange={handleNameChange}
				value={name}
			/>
			<span className='error-label'>{getErrorLabel()}</span>
		</div>
	)
}
