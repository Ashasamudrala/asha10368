import React from 'react'
import { useTranslation } from 'react-i18next'
import { Configurator } from '../../../infrastructure/configFileLoader'
import {
	ISYMPHONY_LOGO_ALT,
	ISY_TOUR_TRANSLATIONS,
	ABOUT_VERSION,
	// ABOUT_WEBSITE,
	ABOUT_COPYRIGHT,
} from '../../../utilities/constants'
import './aboutPage.scss'

export function AboutPage() {
	const { t } = useTranslation(ISY_TOUR_TRANSLATIONS)

	const render = () => {
		return (
			<div className='about-section'>
				<div className='help-section-icon'>
					<img
						className='isy-about-logo'
						src={`/images/iSymphonyLogo.svg`}
						alt={t(ISYMPHONY_LOGO_ALT)}
					/>
				</div>
				<div className='about-section-description'>
					<p className='about-version'>
						{t(ABOUT_VERSION, { version: Configurator.Instance.getVersion() })}
					</p>
					{/* <p>
						<a className='about-website' href='#'>
							{t(ABOUT_WEBSITE)}
						</a>
					</p> */}
					<p className='about-copyright'>{t(ABOUT_COPYRIGHT)}</p>
				</div>
			</div>
		)
	}
	return render()
}
