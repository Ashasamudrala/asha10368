import React from 'react'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import './helpLanding.scss'
import { HelpLandingDataProps } from './helpLanding.types'
import { useTranslation } from 'react-i18next'
import {
	ISY_TOUR_TRANSLATIONS,
	SKIP_TOUR,
	START_TOUR,
} from '../../../utilities/constants'
import { isString } from 'lodash'

export interface HelpLandingPageActionsProps {
	dialogData: HelpLandingDataProps
	onOkay: (data?: any) => void
	onCancel: () => void
}

export function HelpLandingPageActions(props: HelpLandingPageActionsProps) {
	const { t } = useTranslation(ISY_TOUR_TRANSLATIONS)
	const { dialogData } = props

	const handleStartTour = () => {
		props.onOkay({
			tourStarted: true,
		})
	}

	const handleSkipTour = () => {
		props.onCancel()
	}

	const getYesButtonLabel = () => {
		if (isString(dialogData.okayButtonLabel)) {
			return dialogData.okayButtonLabel
		}
		return t(START_TOUR)
	}

	const getCancelButtonLabel = () => {
		if (isString(dialogData.cancelButtonLabel)) {
			return dialogData.cancelButtonLabel
		}
		return t(SKIP_TOUR)
	}

	const render = () => {
		return (
			<>
				{dialogData && dialogData.okayButtonLabel && (
					<IsyButton className='isy-start-btn' onClick={handleStartTour}>
						{getYesButtonLabel()}
					</IsyButton>
				)}
				{dialogData && dialogData.cancelButtonLabel && (
					<IsyButton className='isy-skip-btn' onClick={handleSkipTour}>
						{getCancelButtonLabel()}
					</IsyButton>
				)}
			</>
		)
	}

	return render()
}
