import React from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	BUTTON_CANCEL,
	BUTTON_CONNECT,
} from '../../../utilities/constants'
import { IsyButton } from '../../../widgets/IsyButton/IsyButton'
import {
	getSelectedTableIndex,
	getSelectedAttributeIndex,
	getContainment,
	getRelationshipType,
} from './connectForeignKey.selectors'
import { isNil } from 'lodash'
import { ConnectForeignKeyReturnProps } from './connectForeignKey.types'

export interface ConnectForeignKeyActionProps {
	onOkay: (data: ConnectForeignKeyReturnProps) => void
	onCancel: () => void
}

export function ConnectForeignKeyActions(props: ConnectForeignKeyActionProps) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const selectedTableIndex = useSelector(getSelectedTableIndex)
	const selectedAttributeIndex = useSelector(getSelectedAttributeIndex)
	const containment = useSelector(getContainment)
	const relationShipType = useSelector(getRelationshipType)

	const handleCancel = () => {
		props.onCancel()
	}

	const handleOkay = () => {
		if (isNil(selectedTableIndex) || isNil(selectedAttributeIndex)) {
			return
		}
		props.onOkay({
			targetTableIndex: selectedTableIndex,
			targetAttrIndex: selectedAttributeIndex,
			containment,
			relationShipType,
		})
	}

	return (
		<>
			<IsyButton onClick={handleCancel} className='standard-btn'>
				{t(BUTTON_CANCEL)}
			</IsyButton>
			<IsyButton
				className='primary-btn'
				disabled={isNil(selectedTableIndex) || isNil(selectedAttributeIndex)}
				onClick={handleOkay}
			>
				{t(BUTTON_CONNECT)}
			</IsyButton>
		</>
	)
}
