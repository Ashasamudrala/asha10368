import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { Action } from '../../../common.types'
import {
	DatabaseContainmentType,
	DatabaseRelationShipType,
} from '../../../utilities/apiEnumConstants'
import { DialogsSubReducersNames } from '../base.types'
import { ConnectForeignKeyState } from './connectForeignKey.types'

const initialState: ConnectForeignKeyState = {
	selectedTableIndex: null,
	selectedAttributeIndex: null,
	containment: DatabaseContainmentType.COMPOSITION,
	relationshipType: DatabaseRelationShipType.MANY_TO_ONE,
}

const slice = createSlice<
	ConnectForeignKeyState,
	SliceCaseReducers<ConnectForeignKeyState>,
	DialogsSubReducersNames.CONNECT_FOREIGN_KEY
>({
	name: DialogsSubReducersNames.CONNECT_FOREIGN_KEY,
	initialState,
	reducers: {
		// synchronous actions
		setSelectedData(
			state: ConnectForeignKeyState,
			action: Action<{ tableIndex: number; attrIndex: number }>
		) {
			state.selectedTableIndex = action.payload.tableIndex
			state.selectedAttributeIndex = action.payload.attrIndex
		},
		setContainment(
			state: ConnectForeignKeyState,
			action: Action<DatabaseContainmentType>
		) {
			state.containment = action.payload
		},
		setRelationshipType(
			state: ConnectForeignKeyState,
			action: Action<DatabaseRelationShipType>
		) {
			state.relationshipType = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
