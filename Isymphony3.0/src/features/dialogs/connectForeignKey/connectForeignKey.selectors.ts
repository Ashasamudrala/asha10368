import slice from './connectForeignKey.slice'
import { selectSlice as dialogsSelect } from '../base.selector'
import { RootState } from '../../../base.types'
import { ConnectForeignKeyState } from './connectForeignKey.types'
import {
	DatabaseContainmentType,
	DatabaseRelationShipType,
} from '../../../utilities/apiEnumConstants'

export const selectSlice = (state: RootState): ConnectForeignKeyState =>
	dialogsSelect(state)[slice.name]

export const getSelectedTableIndex = (state: RootState): number | null =>
	selectSlice(state).selectedTableIndex

export const getSelectedAttributeIndex = (state: RootState): number | null =>
	selectSlice(state).selectedAttributeIndex

export const getContainment = (state: RootState): DatabaseContainmentType =>
	selectSlice(state).containment
export const getRelationshipType = (
	state: RootState
): DatabaseRelationShipType => selectSlice(state).relationshipType
