import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import Autocomplete from '@material-ui/lab/Autocomplete'
import {
	TableRow,
	TableBody,
	TableCell,
	Table,
	TextField,
} from '@material-ui/core'
import { useSelector, useDispatch } from 'react-redux'
import {
	FOREIGN_KEY_CONTENT_TITLE,
	DATABASE_TRANSLATIONS,
	FOREIGN_KEY_PLACEHOLDER,
	FOREIGN_KEY_CONTENT_TO_POINT,
	FOREIGN_KEY_PARENT_TABLE,
	FOREIGN_KEY_DIALOG_CONTAINMENT,
	FOREIGN_KEY_DIALOG_CONTAINMENT_AGGREGATION,
	FOREIGN_KEY_DIALOG_CONTAINMENT_COMPOSITION,
	ONE_TO_ONE,
	ONE_TO_MANY,
	MANY_TO_ONE,
	MANY_TO_MANY,
	RELATIONSHIP_TYPE,
} from '../../../utilities/constants'
import {
	getSelectedTableIndex,
	getSelectedAttributeIndex,
	getContainment,
	getRelationshipType,
} from './connectForeignKey.selectors'
import { actions } from './connectForeignKey.slice'
import {
	DatabaseRelationShipType,
	DatabaseContainmentType,
} from '../../../utilities/apiEnumConstants'
import './connectForeignKey.scss'
import { findIndex, isNil } from 'lodash'
import { IsyRadioGroup } from '../../../widgets/IsyRadioGroup/IsyRadioGroup'
import { OneToOneIcon } from '../../../icons/OneToOne'
import { OneToManyIcon } from '../../../icons/OneToMany'
import { ManyToOneIcon } from '../../../icons/ManyToOne'
import { ManyToManyIcon } from '../../../icons/ManyToMany'
import { ConnectForeignKeyDataProps } from './connectForeignKey.types'
import { IsyDropDown } from '../../../widgets/IsyDropDown/IsyDropDown'
import { IsyInput } from '../../../widgets/IsyInput/IsyInput'
export interface ConnectForeignKeyContentProps {
	dialogData: ConnectForeignKeyDataProps
}

export interface ConnectForeignKeyContentToProps {
	tableIndex: number
	tableName: string
	attributeName: string
	attributeIndex: number
	dataType: string
}

export function ConnectForeignKeyContent(props: ConnectForeignKeyContentProps) {
	const { dialogData } = props
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const selectedTableIndex = useSelector(getSelectedTableIndex)
	const selectedAttributeIndex = useSelector(getSelectedAttributeIndex)
	const containment = useSelector(getContainment)
	const relationShipType = useSelector(getRelationshipType)
	const dispatch = useDispatch()

	useEffect(() => {
		const reference =
			dialogData.tablesData[dialogData.tableIndex].attributes[
				dialogData.attrIndex
			].reference
		if (!isNil(reference)) {
			const tableIndex = findIndex(
				dialogData.tablesData,
				(table) => table.name === reference.targetModel
			)
			const attrIndex = findIndex(
				dialogData.tablesData[tableIndex].attributes,
				(attr) => attr.name === reference.column
			)
			dispatch(
				actions.setSelectedData({
					tableIndex,
					attrIndex,
				})
			)
			dispatch(actions.setContainment(reference.relation.containment.type))
			dispatch(actions.setRelationshipType(reference.relation.association))
		}
	}, [dialogData, dispatch])

	const containmentOptions = [
		{
			value: DatabaseContainmentType.COMPOSITION,
			label: t(FOREIGN_KEY_DIALOG_CONTAINMENT_COMPOSITION),
		},
		{
			value: DatabaseContainmentType.AGGREGATION,
			label: t(FOREIGN_KEY_DIALOG_CONTAINMENT_AGGREGATION),
		},
	]

	const relationshipOptions = [
		{
			id: DatabaseRelationShipType.ONE_TO_ONE,
			name: t(ONE_TO_ONE),
			hasIcon: true,
			icon: <OneToOneIcon className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
		{
			id: DatabaseRelationShipType.ONE_TO_MANY,
			name: t(ONE_TO_MANY),
			hasIcon: true,
			icon: <OneToManyIcon className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
		{
			id: DatabaseRelationShipType.MANY_TO_ONE,
			name: t(MANY_TO_ONE),
			hasIcon: true,
			icon: <ManyToOneIcon className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
		{
			id: DatabaseRelationShipType.MANY_TO_MANY,
			name: t(MANY_TO_MANY),
			hasIcon: true,
			icon: <ManyToManyIcon className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
	]

	const getOptionsAndSelectedElement = () => {
		const tableIndex = dialogData.tableIndex
		const attributeIndex = dialogData.attrIndex
		const tablesData = dialogData.tablesData
		const sourceAttrType =
			tablesData[tableIndex].attributes[attributeIndex].type
		const options: ConnectForeignKeyContentToProps[] = []
		let selectedElement = null
		for (let i = 0, iLen = tablesData.length; i < iLen; i++) {
			if (i !== tableIndex) {
				const table = tablesData[i]
				const primaryKeyIndex = findIndex(table.attributes, (attr) => {
					return (
						(attr.type === sourceAttrType && attr.constraints.primary) || false
					)
				})
				if (primaryKeyIndex !== -1) {
					options.push({
						tableIndex: i,
						tableName: table.name,
						attributeName: table.attributes[primaryKeyIndex].name,
						attributeIndex: primaryKeyIndex,
						dataType: sourceAttrType,
					})
					if (selectedTableIndex === i) {
						selectedElement = options[options.length - 1]
					}
				}
			}
		}
		return { options, selectedElement }
	}

	const getLabelValue = (option: ConnectForeignKeyContentToProps) => {
		const labelData = `${option.tableName}: ${option.attributeName} [${option.dataType}]`
		return labelData
	}

	const handleSelectedValue = (_: any, option: any, reason: any) => {
		if (reason === 'clear') {
			dispatch(
				actions.setSelectedData({
					tableIndex: null,
					attrIndex: null,
				})
			)
		} else if (reason === 'select-option') {
			dispatch(
				actions.setSelectedData({
					tableIndex: option.tableIndex,
					attrIndex: option.attributeIndex,
				})
			)
		}
	}

	const handleContainmentChange = (value: DatabaseContainmentType) => {
		dispatch(actions.setContainment(value))
	}

	const handleRelationshipTypeChange = (value: DatabaseRelationShipType) => {
		dispatch(actions.setRelationshipType(value))
	}

	const renderListOfOptions = (option: ConnectForeignKeyContentToProps) => {
		return (
			<Table className='auto-complete-table'>
				<TableBody>
					<TableRow>
						<TableCell className='table-cell'>{option.tableName}</TableCell>
						<TableCell className='table-cell'>
							{option.attributeName} [{option.dataType}]
						</TableCell>
					</TableRow>
				</TableBody>
			</Table>
		)
	}

	const renderTextField = (params: any) => {
		return (
			<TextField
				{...params}
				placeholder={t(FOREIGN_KEY_PLACEHOLDER)}
				variant='outlined'
			/>
		)
	}

	const renderAutoCompleteComponent = () => {
		const optionsAndSelectedElement = getOptionsAndSelectedElement()
		return (
			<Autocomplete
				value={optionsAndSelectedElement.selectedElement}
				className='auto-search-input'
				options={optionsAndSelectedElement.options}
				getOptionLabel={getLabelValue}
				closeIcon={null}
				onChange={handleSelectedValue}
				renderOption={renderListOfOptions}
				renderInput={renderTextField}
			/>
		)
	}

	const renderContainment = () => {
		return (
			<IsyRadioGroup
				title={t(FOREIGN_KEY_DIALOG_CONTAINMENT)}
				value={containment}
				options={containmentOptions}
				onChange={handleContainmentChange}
			/>
		)
	}

	const renderRelations = () => {
		return (
			<div className='relationShipType'>
				<IsyDropDown<DatabaseRelationShipType>
					options={relationshipOptions}
					onChange={handleRelationshipTypeChange}
					value={relationShipType}
					title={t(RELATIONSHIP_TYPE)}
					className='input-base'
				/>
			</div>
		)
	}

	const renderParentTableName = () => {
		const tablesData = dialogData.tablesData[dialogData.tableIndex]
		const { name, type } = tablesData.attributes[dialogData.attrIndex]
		return (
			<>
				<p className='search-content'>{t(FOREIGN_KEY_PARENT_TABLE)}</p>
				<IsyInput<string>
					type='text'
					value={`${tablesData.name}: ${name} [${type}]`}
					disabled={true}
					className='parent-table-name'
				/>
			</>
		)
	}

	return (
		<div className='relation-ship'>
			{renderParentTableName()}
			<p className='search-content'>
				{!isNil(selectedTableIndex) && !isNil(selectedAttributeIndex)
					? t(FOREIGN_KEY_CONTENT_TO_POINT)
					: t(FOREIGN_KEY_CONTENT_TITLE)}
			</p>
			{renderAutoCompleteComponent()}
			{renderRelations()}
			{renderContainment()}
		</div>
	)
}
