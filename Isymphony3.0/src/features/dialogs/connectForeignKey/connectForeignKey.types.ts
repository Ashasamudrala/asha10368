import { DatabaseTableProps } from '../../databases/schema/schema.types'
import {
	DatabaseContainmentType,
	DatabaseRelationShipType,
} from '../../../utilities/apiEnumConstants'

export interface ConnectForeignKeyReturnProps {
	targetTableIndex: number
	targetAttrIndex: number
	containment: DatabaseContainmentType
	relationShipType: DatabaseRelationShipType
}

export interface ConnectForeignKeyDataProps {
	tablesData: DatabaseTableProps[]
	tableIndex: number
	attrIndex: number
}

export interface ConnectForeignKeyState {
	selectedTableIndex: number | null
	selectedAttributeIndex: number | null
	containment: DatabaseContainmentType
	relationshipType: DatabaseRelationShipType
}
