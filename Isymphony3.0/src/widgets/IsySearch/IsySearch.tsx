import React, { useState } from 'react'
import Input from '@material-ui/core/Input'
import './isySearch.scss'
import { isFunction, isNil, isArray } from 'lodash'
import { Cancel, Search } from '@material-ui/icons'
import { InputAdornment } from '@material-ui/core'
import { IsyClickAwayListener } from '../IsylickAwayListener/IsyClickAwayListener'

export interface IsySearchSuggestionProps {
	value: string | number
	name: string
}

export interface IsySearchProps {
	value: string
	onChange: (value: string) => void
	onCancel: (value: string) => void
	autoFocus?: boolean
	hideSearchIcon?: boolean
	onKeyUp?: (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => void
	placeholder?: string
	suggestions?: IsySearchSuggestionProps[]
	onSuggestionSelect?: (suggestion: IsySearchSuggestionProps) => void
}

export function IsySearch(props: IsySearchProps) {
	const [showSuggestions, setShowSuggestions] = useState<boolean>(false)
	const inputRef = React.useRef<HTMLInputElement>(null)

	const handleOnChange = (
		event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		if (isFunction(props.onChange)) {
			props.onChange(event.target.value)
		}
		setShowSuggestions(true)
	}

	const handleSearch = () => {
		if (
			!isNil(inputRef) &&
			!isNil(inputRef.current) &&
			!isNil(inputRef.current.lastChild) &&
			isFunction((inputRef.current.lastChild as HTMLInputElement).focus)
		) {
			;(inputRef.current.lastChild as HTMLInputElement).focus()
		}
	}

	const handleCancel = () => {
		if (isFunction(props.onCancel)) {
			props.onCancel('')
		}
	}

	const handleOnKeyUp = (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		if (isFunction(props.onKeyUp)) {
			props.onKeyUp(event)
		}
	}

	const handleSuggestions = (item: IsySearchSuggestionProps) => {
		if (isFunction(props.onSuggestionSelect)) {
			props.onSuggestionSelect(item)
		}
		setShowSuggestions(false)
	}

	const handleClickAway = () => {
		setShowSuggestions(false)
	}

	const renderStartAdornment = () => {
		if (props.value.length === 0 && !props.hideSearchIcon) {
			return (
				<InputAdornment
					component='div'
					position='start'
					className='isy-search-search-icon'
				>
					<Search onClick={handleSearch} className='icon' />
				</InputAdornment>
			)
		}
		return null
	}

	const renderEndAdornment = () => {
		if (props.value.length !== 0 || props.hideSearchIcon) {
			return (
				<InputAdornment
					component='div'
					position='end'
					className='isy-search-clear-icon'
				>
					<Cancel onClick={handleCancel} className='icon' />
				</InputAdornment>
			)
		}
		return null
	}

	const renderSuggestions = () => {
		if (!showSuggestions || !isArray(props.suggestions)) {
			return null
		}
		return (
			<div>
				<ul className='isy-search-suggestions'>
					{props.suggestions.map((suggestion: IsySearchSuggestionProps) => (
						<li
							key={suggestion.value}
							onClick={() => handleSuggestions(suggestion)}
						>
							{suggestion.name}
						</li>
					))}
				</ul>
			</div>
		)
	}

	const renderInput = () => {
		return (
			<Input
				ref={inputRef}
				className='isy-search-input'
				type='text'
				value={props.value}
				onChange={handleOnChange}
				autoFocus={props.autoFocus}
				startAdornment={renderStartAdornment()}
				endAdornment={renderEndAdornment()}
				onKeyUp={handleOnKeyUp}
				placeholder={props.placeholder}
			/>
		)
	}

	const render = () => {
		return (
			<IsyClickAwayListener
				className='isy-search'
				onClickAway={handleClickAway}
			>
				{renderInput()}
				{renderSuggestions()}
			</IsyClickAwayListener>
		)
	}

	return render()
}
