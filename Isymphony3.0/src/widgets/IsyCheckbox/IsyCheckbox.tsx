import React from 'react'
import { Checkbox } from '@material-ui/core'
import { isBoolean, isFunction } from 'lodash'
import './IsyCheckbox.scss'

export enum IsyCheckboxStatus {
	CHECKED,
	INTERMEDIATE,
	UNCHECKED,
}

export interface IsyCheckboxProps {
	label?: string
	value: boolean | IsyCheckboxStatus
	className?: string
	disabled?: boolean
	onChange: (value: boolean) => void
}

export function IsyCheckbox(props: IsyCheckboxProps) {
	const getIntermediateState = () => {
		return (
			!isBoolean(props.value) && props.value === IsyCheckboxStatus.INTERMEDIATE
		)
	}

	const getCheckedState = () => {
		if (isBoolean(props.value)) {
			return props.value
		}
		return props.value === IsyCheckboxStatus.CHECKED
	}

	const handleClick = (event: any) => {
		if (isFunction(props.onChange)) {
			event.stopPropagation()
			props.onChange(event.target.checked)
		}
	}

	const handleLabelClick = (
		event: React.MouseEvent<HTMLSpanElement, MouseEvent>
	) => {
		if (isFunction(props.onChange)) {
			event.stopPropagation()
			if (getIntermediateState() || !getCheckedState()) {
				props.onChange(true)
			} else {
				props.onChange(false)
			}
		}
	}

	const renderCheckbox = () => {
		return (
			<div className='isy-checkbox'>
				<Checkbox
					className={`isy-checkbox-control ${props.className || ''}`}
					indeterminate={getIntermediateState()}
					checked={getCheckedState()}
					disabled={props.disabled}
					onClick={handleClick}
				/>
				<span
					className={`${props.disabled ? 'disabled' : ''} isy-checkbox-label `}
					title={props.label}
					onClick={handleLabelClick}
				>
					{props.label}
				</span>
			</div>
		)
	}
	return renderCheckbox()
}
