import React from 'react'
import { Popover as MUIPopover, PopoverOrigin } from '@material-ui/core'
import { isArray } from 'lodash'
import MenuItem from '@material-ui/core/MenuItem'

export interface IsyPopoverActionProps {
	key: string
	menuClass?: string
	hasIcon?: boolean
	labelClass?: string
	icon?: React.ReactNode | null
	name: string
}

export interface IsyPopoverProps {
	actions: IsyPopoverActionProps[]
	anchorEl: Element
	anchorOrigin?: PopoverOrigin
	transformOrigin?: PopoverOrigin
	onSelect: (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		key: string
	) => void
	onClose: (event: {}) => void
	className?: string
}

export function IsyPopover(props: IsyPopoverProps) {
	const { actions, anchorEl, anchorOrigin, transformOrigin } = props

	const getActions = () => {
		if (isArray(actions)) {
			return actions
		}
		return []
	}

	const handleSelect = (
		event: React.MouseEvent<HTMLLIElement, MouseEvent>,
		key: string
	) => {
		return props.onSelect(event, key)
	}

	const renderAction = (action: IsyPopoverActionProps) => {
		return (
			<MenuItem
				key={action.key}
				onClick={(event) => handleSelect(event, action.key)}
				className={action.menuClass}
			>
				{action.hasIcon ? (
					<div>
						{action.icon}
						<span className={action.labelClass}>{action.name}</span>
					</div>
				) : (
					action.name
				)}
			</MenuItem>
		)
	}

	return (
		<MUIPopover
			open={true}
			anchorEl={anchorEl}
			onClose={(e) => props.onClose(e)}
			anchorOrigin={anchorOrigin}
			transformOrigin={transformOrigin}
		>
			<div className={props.className || ''}>
				{getActions().map(renderAction)}
			</div>
		</MUIPopover>
	)
}
