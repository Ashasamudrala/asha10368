import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import { isNil } from 'lodash'

export interface IsyAvatarImageProps {
	defaultName: string
	imageData?: string | null
	fileUrl?: string | null
	imageClassName?: string
}

export function IsyAvatarImage(props: IsyAvatarImageProps) {
	const { imageClassName, imageData, fileUrl, defaultName } = props

	const getDefaultNameChar = () => {
		const defaultNameChar = defaultName.split(' ').map((item: string) => {
			return item.charAt(0)
		})
		return defaultNameChar[0] + defaultNameChar[defaultNameChar.length - 1]
	}

	let imageUrl
	if (!isNil(fileUrl)) {
		imageUrl = fileUrl
	} else {
		imageUrl = `data:image/png;base64,${imageData}`
	}
	return (
		<Avatar className={imageClassName} src={imageUrl}>
			{getDefaultNameChar()}
		</Avatar>
	)
}
