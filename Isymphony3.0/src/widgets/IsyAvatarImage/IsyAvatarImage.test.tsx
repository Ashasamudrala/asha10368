import React from 'react'
import { mount } from 'enzyme'
import { IsyAvatarImage, IsyAvatarImageProps } from './IsyAvatarImage'

describe('<IsyAvatarImage />', () => {
	let wrapper: any

	const props: IsyAvatarImageProps = {
		defaultName: 'Admin',
		imageClassName: 'test-profile-avatar',
		imageData: null,
	}

	beforeEach(() => {
		wrapper = mount(<IsyAvatarImage {...props} />)
	})

	it('Avatar props with defaultname is equal to Admin', () => {
		expect(wrapper.props().defaultName).toEqual('Admin')
	})

	it('Avatar props with className is equal to profile-avatar', () => {
		expect(wrapper.props().imageClassName).toEqual('test-profile-avatar')
	})
})
