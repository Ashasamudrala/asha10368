import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import { IsyButton, IsyButtonProps } from '../IsyButton/IsyButton'
import Toolbar from '@material-ui/core/Toolbar'
import { IsySearch, IsySearchProps } from '../IsySearch/IsySearch'
import { omit } from 'lodash'
import './isyToolbar.scss'

export enum IsyToolbarItemTypes {
	SEARCH,
	FLEX_GROW,
	BUTTON,
	ICON,
	SEPARATOR,
	COMPONENT,
}

export interface IsyToolbarSearchItemProps extends IsySearchProps {
	type: IsyToolbarItemTypes.SEARCH
}

export interface IsyToolbarFlexGrowItemProps {
	type: IsyToolbarItemTypes.FLEX_GROW
	className?: string
}

export interface IsyToolbarButtonItemProps
	extends Omit<IsyButtonProps, 'children'> {
	type: IsyToolbarItemTypes.BUTTON
	name: string
	icon?: React.ReactNode
}

export interface IsyToolbarIconItemProps {
	type: IsyToolbarItemTypes.ICON
	icon: React.ReactNode
	onClick?: () => void
	tooltip: string
	className?: string
	disabled?: boolean
}

export interface IsyToolbarSeparatorItemProps {
	type: IsyToolbarItemTypes.SEPARATOR
	className?: string
}

export interface IsyToolbarComponentItemProps {
	type: IsyToolbarItemTypes.COMPONENT
	render: () => React.ReactNode
	className?: string
}

export type IsyToolbarItemProps =
	| IsyToolbarSearchItemProps
	| IsyToolbarFlexGrowItemProps
	| IsyToolbarButtonItemProps
	| IsyToolbarIconItemProps
	| IsyToolbarSeparatorItemProps
	| IsyToolbarComponentItemProps

export interface IsyToolbarProps {
	items: IsyToolbarItemProps[]
	className?: string
}

export function IsyToolbar(props: IsyToolbarProps) {
	const renderSearchWidget = (
		item: IsyToolbarSearchItemProps,
		index: number
	) => {
		return <IsySearch {...omit(item, 'type')} key={index} />
	}

	const renderIconWidget = (item: IsyToolbarIconItemProps, index: number) => {
		return (
			<Tooltip title={item.tooltip} key={index}>
				<IconButton
					className={'isy-toolbar-icon ' + item.className}
					aria-label={item.tooltip}
					disabled={item.disabled}
					onClick={item.onClick}
				>
					{item.icon}
				</IconButton>
			</Tooltip>
		)
	}
	const renderButtonWidget = (
		item: IsyToolbarButtonItemProps,
		index: number
	) => {
		return (
			<IsyButton
				key={index}
				{...omit(item, 'type', 'icon', 'name', 'className')}
				className={'standard-btn ' + item.className}
			>
				{item.icon}
				{item.name}
			</IsyButton>
		)
	}

	const renderFlexGrow = (item: IsyToolbarFlexGrowItemProps, index: number) => {
		return (
			<div className={'isy-toolbar-flex-grow ' + item.className} key={index} />
		)
	}

	const renderSeparatorWidget = (
		item: IsyToolbarSeparatorItemProps,
		index: number
	) => {
		return (
			<div className={'isy-toolbar-separator ' + item.className} key={index} />
		)
	}

	const renderComponentWidget = (
		item: IsyToolbarComponentItemProps,
		index: number
	) => {
		return (
			<div className={'isy-toolbar-component ' + item.className} key={index}>
				{item.render()}
			</div>
		)
	}

	const renderItem = (item: IsyToolbarItemProps, index: number) => {
		switch (item.type) {
			case IsyToolbarItemTypes.SEARCH:
				return renderSearchWidget(item, index)
			case IsyToolbarItemTypes.FLEX_GROW:
				return renderFlexGrow(item, index)
			case IsyToolbarItemTypes.BUTTON:
				return renderButtonWidget(item, index)
			case IsyToolbarItemTypes.ICON:
				return renderIconWidget(item, index)
			case IsyToolbarItemTypes.SEPARATOR:
				return renderSeparatorWidget(item, index)
			case IsyToolbarItemTypes.COMPONENT:
				return renderComponentWidget(item, index)
			default:
				return null
		}
	}

	const render = () => {
		return (
			<Toolbar className={'isy-toolbar ' + props.className}>
				<div className='isy-toolbar-row'>{props.items.map(renderItem)}</div>
			</Toolbar>
		)
	}

	return render()
}
