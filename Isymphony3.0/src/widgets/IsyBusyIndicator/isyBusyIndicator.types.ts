export interface IsyBusyIndicatorState {
	global: number
	[key: string]: number
}
