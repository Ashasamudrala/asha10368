import React from 'react'
import { useSelector } from 'react-redux'
import { getGlobalBusyIndicator } from './IsyBusyIndicator.selectors'
import CircularProgress from '@material-ui/core/CircularProgress'
import './IsyBusyIndicator.scss'
import { isArray, isNil } from 'lodash'

export interface IsyBusyIndicatorProps {
	children: React.ReactNode | React.ReactNode[]
	doNotShowNoText: boolean
}

export function IsyBusyIndicator(props: IsyBusyIndicatorProps) {
	const showLoader = useSelector(getGlobalBusyIndicator)

	const render = () => {
		if (showLoader) {
			return (
				<div className='busy-indicator'>
					<CircularProgress className='loader' />
				</div>
			)
		}
		if (
			(isArray(props.children) && props.children.length > 0) ||
			!isNil(props.children)
		) {
			return props.children
		}
		if (props.doNotShowNoText) {
			return ''
		}

		return <p>No content found</p>
	}

	return <>{render()}</>
}
