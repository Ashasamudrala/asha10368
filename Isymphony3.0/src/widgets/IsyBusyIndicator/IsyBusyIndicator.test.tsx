import React from 'react'
import { mount } from 'enzyme'
import { IsyBusyIndicator, IsyBusyIndicatorProps } from './IsyBusyIndicator'
import { FrameworkSettings } from '../../features/admin/core/framework/frameworkSettings/FrameworkSettings'
import * as reactRedux from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { IsyLogoHeader } from '../IsyLogoHeader/IsyLogoHeader'

describe('IsyBusyIndicator Test', () => {
	let wrapper: any
	let store: any
	const props: IsyBusyIndicatorProps = {
		children: <IsyLogoHeader />,
		doNotShowNoText: false,
	}

	beforeEach(() => {
		store = configureStore([thunk])({ busyIndicator: { global: 0 } })
		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<IsyBusyIndicator {...props} />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('should render IsyLogoHeader ', () => {
		expect(wrapper.contains(<IsyLogoHeader />)).toBe(true)
	})
	it('should not render FrameworkSettings ', () => {
		expect(wrapper.contains(<FrameworkSettings />)).toBe(false)
	})
})
