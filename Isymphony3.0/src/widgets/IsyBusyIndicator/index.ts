import * as busyIndicatorSelectors from './IsyBusyIndicator.selectors'
import slice from './isyBusyIndicator.slice'

export const {
	name,
	actions: { incrementBusyIndicator, decrementBusyIndicator },
	reducer,
} = slice

export const {
	getGlobalBusyIndicator,
	getNamedBusyIndicator,
} = busyIndicatorSelectors
