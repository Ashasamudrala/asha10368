import React from 'react'
import { mount } from 'enzyme'
import { IsyGlobalLoader } from './IsyGlobalLoader'
import * as reactRedux from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'

describe('IsyGlobalLoader Test when indicator store value is true', () => {
	let wrapper: any
	let store: any

	beforeEach(() => {
		store = configureStore([thunk])({ busyIndicator: { global: 1 } })
		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<IsyGlobalLoader />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('should render IsyLogoHeader ', () => {
		expect(wrapper.find('div.global-loader-holder').length).toBe(1)
	})
})

describe('IsyGlobalLoader Test when indicator store value is false', () => {
	let wrapper: any
	let store: any

	beforeEach(() => {
		store = configureStore([thunk])({ busyIndicator: { global: 0 } })
		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<IsyGlobalLoader />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('should render IsyLogoHeader ', () => {
		expect(wrapper.find('div.global-loader-holder').length).toBe(0)
	})
})
