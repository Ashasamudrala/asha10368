import React, { useState } from 'react'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { isBoolean, isFunction } from 'lodash'

export interface IsyAccordionProps {
	id?: string
	header: string | React.ReactNode
	children?: React.ReactNode | React.ReactNode[]
	isExpandByDefault?: boolean
	isDisable?: boolean
	isOpen?: boolean
	onOpenToggle?: (isExpanded: boolean) => void
}
export default function IsyAccordion(props: IsyAccordionProps) {
	const [expanded, setExpandedValue] = useState<boolean>(
		props.isExpandByDefault || false
	)

	const getExpanded = () => {
		if (isBoolean(props.isOpen) && isFunction(props.onOpenToggle)) {
			return props.isOpen
		}
		return expanded
	}

	const handleOpenToggle = (isExpanded: boolean) => {
		if (isBoolean(props.isOpen) && isFunction(props.onOpenToggle)) {
			props.onOpenToggle(isExpanded)
		} else {
			setExpandedValue(isExpanded)
		}
	}

	const renderExpandIcon = () => {
		return <ExpandMoreIcon className='isy-accordion-summary-icon' />
	}

	return (
		<Accordion
			id={props.id}
			className='isy-accordion'
			expanded={getExpanded()}
			disabled={props.isDisable}
			onChange={(_, isExpanded) => handleOpenToggle(isExpanded)}
		>
			<AccordionSummary
				className='isy-accordion-summary'
				expandIcon={renderExpandIcon()}
			>
				{props.header}
			</AccordionSummary>
			{props.children}
		</Accordion>
	)
}
