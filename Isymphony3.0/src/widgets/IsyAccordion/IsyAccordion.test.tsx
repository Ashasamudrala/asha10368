import React from 'react'
import { mount } from 'enzyme'
import { IsyAccordion, IsyAccordionProps } from './IsyAccordion'

describe('<IsyAccordion />', () => {
	let wrapper: any

	const props: IsyAccordionProps = {
		id: 'id',
		header: 'React Header',
		children: <div>React Header</div>,
		isExpandByDefault: true,
		isDisable: false,
	}

	beforeEach(() => {
		wrapper = mount(<IsyAccordion {...props} />)
	})

	it('accordion IS disabled default or not', () => {
		expect(wrapper.childAt(0).props().disabled).toEqual(false)
	})

	it('accordion expanded default or not', () => {
		expect(wrapper.childAt(0).props().defaultExpanded).toEqual(true)
	})

	it('accordion header is displayed with details in header prop', () => {
		expect(
			wrapper.find('.MuiAccordionSummary-content').find('div').text()
		).toEqual('React Header')
	})
})
