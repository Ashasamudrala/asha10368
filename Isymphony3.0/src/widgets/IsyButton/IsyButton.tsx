import React from 'react'
import Button from '@material-ui/core/Button'
import './isyButton.scss'

export interface IsyButtonProps {
	onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
	children: React.ReactNode[] | React.ReactNode
	className?: string
	disabled?: boolean
}

export function IsyButton(props: IsyButtonProps) {
	const handleButtonOnClick = (
		event: React.MouseEvent<HTMLButtonElement, MouseEvent>
	) => {
		props.onClick(event)
	}
	return (
		<Button
			onClick={handleButtonOnClick}
			disabled={props.disabled || false}
			className={`${props.className} isy-button`}
		>
			{props.children}
		</Button>
	)
}
