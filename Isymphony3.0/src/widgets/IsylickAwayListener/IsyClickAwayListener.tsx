import * as React from 'react'
import { isFunction, isNil } from 'lodash'
import { isParentUntil } from '../../utilities/utilities'

export interface IsyClickAwayListenerProps
	extends React.DetailedHTMLProps<
		React.HTMLAttributes<HTMLDivElement>,
		HTMLDivElement
	> {
	onClickAway: () => void
	skipClasses?: string[]
}

export class IsyClickAwayListener extends React.Component<IsyClickAwayListenerProps> {
	elementRef: HTMLDivElement | null
	constructor(props: IsyClickAwayListenerProps) {
		super(props)
		this.elementRef = null
		this.handleRef = this.handleRef.bind(this)
		this.handleBodyClick = this.handleBodyClick.bind(this)
	}

	componentDidMount() {
		document.body.addEventListener('mousedown', this.handleBodyClick)
	}

	componentWillUnmount() {
		document.body.removeEventListener('mousedown', this.handleBodyClick)
	}

	isDescendant(target: Element | null, element: Element | null): boolean {
		if (!isNil(target) && !isNil(target.parentNode) && !isNil(element)) {
			return (
				element === target ||
				this.isDescendant(target.parentNode as Element, element)
			)
		}
		return false
	}

	handleRef(ref: HTMLDivElement | null) {
		this.elementRef = ref
	}

	handleBodyClick(event: MouseEvent) {
		if (
			!this.isDescendant(event.target as Element, this.elementRef) &&
			isFunction(this.props.onClickAway) &&
			!isParentUntil(event.srcElement as Element, this.props.skipClasses || [])
		) {
			this.props.onClickAway()
		}
	}

	render() {
		const { children, ...rest } = this.props
		return (
			<div {...rest} ref={this.handleRef}>
				{children}
			</div>
		)
	}
}
