import React, { useEffect, useState } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import MenuIcon from '@material-ui/icons/Menu'
import { Container } from '@material-ui/core'
import {
	ProSidebar,
	Menu,
	MenuItem,
	SidebarContent,
	SidebarHeader,
} from 'react-pro-sidebar'
import 'react-pro-sidebar/dist/css/styles.css'
import './isySideBar.scss'
import { Route, useParams, useRouteMatch } from 'react-router-dom'
import {
	getSidebarIconBasedOnRouteId,
	getComponentsBasedOnRouteId,
} from '../../utilities/routes'
import { RouteProps } from '../../common.types'
import { isNil } from 'lodash'
import {
	setApplicationNameById,
	updatePreferences,
} from '../../authAndPermissions/loginUserDetails.asyncActions'
import { actions as authActions } from '../../authAndPermissions/loginUserDetails.slice'
import { useDispatch, useSelector } from 'react-redux'
import { getSideNavSteps } from '../../utilities/isyStepsConfig'
import { IsySteps } from '../IsySteps/IsySteps'
import { HelpModuleStatus } from '../../authAndPermissions/loginUserDetails.types'
import { selectGetSideNavbarPreferenceStatus } from '../../authAndPermissions/loginUserDetails.selectors'
import { actions as preferenceActions } from '../../authAndPermissions/loginUserDetails.slice'

/**
 * This component is to create the sideBar.
 */

function PageRoute({
	children,
	...rest
}: {
	children: any
	className?: string
	path: string
}) {
	return (
		<Route {...rest}>
			<Container fixed={false}>{children}</Container>
		</Route>
	)
}

export interface IsySideBarProps {
	routes: RouteProps[]
}

export function IsySideBar(props: IsySideBarProps) {
	const sideNavbarPreferenceData = useSelector(
		selectGetSideNavbarPreferenceStatus
	)
	const { appid } = useParams<{ appid: string }>()
	const { routes } = props
	const [collapsed, setCollapsed] = useState<boolean>(true)
	const { url, path } = useRouteMatch()
	const steps = getSideNavSteps()
	const dispatch = useDispatch()

	useEffect(() => {
		if (!isNil(appid)) {
			dispatch(setApplicationNameById(appid))
		}
		return () => {
			dispatch(authActions.clearApp(null))
		}
	}, [appid, dispatch])

	const handleCollapsed = () => {
		setCollapsed(!collapsed)
		if (
			collapsed &&
			sideNavbarPreferenceData &&
			sideNavbarPreferenceData.status === HelpModuleStatus.INPROGRESS
		) {
			dispatch(
				preferenceActions.updateSideNavbarPreferences({
					currentStep: 1,
				})
			)
		} else if (
			sideNavbarPreferenceData &&
			sideNavbarPreferenceData.status === HelpModuleStatus.INPROGRESS
		) {
			dispatch(
				preferenceActions.updateSideNavbarPreferences({
					currentStep: 0,
				})
			)
		}
	}

	const handleOnExit = (currentStep: number) => {
		let currentStepStatus
		if (currentStep === 1) {
			currentStepStatus = HelpModuleStatus.COMPLETED
		} else if (currentStep <= 0) {
			currentStepStatus = HelpModuleStatus.INCOMPLETE
		}
		if (currentStep !== -1) {
			dispatch(
				preferenceActions.updateSideNavbarPreferences({
					status: currentStepStatus,
					currentStep: currentStep,
				})
			)
			dispatch(
				preferenceActions.updateDatabasePreferences({
					landingPage: true,
				})
			)
			dispatch(updatePreferences())
		}
		setCollapsed(true)
	}

	const handleRoutes = () => {
		if (
			sideNavbarPreferenceData &&
			sideNavbarPreferenceData.status === HelpModuleStatus.INPROGRESS
		) {
			setCollapsed(!collapsed)
			let currentStep = 1
			let currentStepStatus
			if (collapsed) {
				currentStep = 0
			}
			if (currentStep === 1) {
				currentStepStatus = HelpModuleStatus.COMPLETED
			} else if (currentStep <= 0) {
				currentStepStatus = HelpModuleStatus.INCOMPLETE
			}
			if (currentStep !== -1) {
				dispatch(
					preferenceActions.updateSideNavbarPreferences({
						status: currentStepStatus,
						currentStep: currentStep,
					})
				)
				dispatch(
					preferenceActions.updateDatabasePreferences({
						landingPage: true,
					})
				)
				dispatch(updatePreferences())
			}
		}
	}

	const sidebarContent = () => {
		return (
			<SidebarContent>
				<Menu>
					{routes.map((route: RouteProps) => {
						return (
							<LinkContainer
								to={url + route.path}
								key={route.id}
								className={route.id}
								icon={getSidebarIconBasedOnRouteId(route.id)}
								onClick={handleRoutes}
							>
								<MenuItem>{route.title}</MenuItem>
							</LinkContainer>
						)
					})}
				</Menu>
			</SidebarContent>
		)
	}

	const renderSidebar = () => {
		return (
			<ProSidebar collapsed={collapsed}>
				<SidebarHeader>
					<span
						className='toggle-button'
						role='button'
						onClick={handleCollapsed}
					>
						<MenuIcon />
					</span>
				</SidebarHeader>
				{sidebarContent()}
			</ProSidebar>
		)
	}

	const renderRoutes = () => {
		return (
			<div className='main'>
				{routes.map((route) => {
					return (
						<PageRoute path={path + route.path} key={route.id}>
							{getComponentsBasedOnRouteId(route.id)}
						</PageRoute>
					)
				})}
			</div>
		)
	}

	const renderSteps = () => {
		return (
			<IsySteps
				steps={steps}
				isEnabled={
					sideNavbarPreferenceData &&
					sideNavbarPreferenceData.status === HelpModuleStatus.INPROGRESS
				}
				initialStep={
					sideNavbarPreferenceData && sideNavbarPreferenceData.currentStep
				}
				updateSteps={true}
				onExit={handleOnExit}
			/>
		)
	}

	return (
		<div className='side-nav-menu'>
			{renderSidebar()}
			{renderRoutes()}
			{renderSteps()}
		</div>
	)
}
