declare module 'react-router-bootstrap/lib/LinkContainer' {
	import { ComponentClass } from 'react'
	import { NavLinkProps } from 'react-router-dom'

	export interface LinkContainerProps extends NavLinkProps {
		icon?: React.ReactNode
	}

	type LinkContainer = ComponentClass<LinkContainerProps>
	declare const LinkContainer: LinkContainer

	export default LinkContainer
}
