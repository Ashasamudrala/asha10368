import React from 'react'
import {
	isArray,
	isNil,
	isString,
	get,
	isFunction,
	set,
	isEmpty,
	cloneDeep,
	find,
} from 'lodash'
import { IsyInput } from '../IsyInput/IsyInput'
import { IsyTextarea } from '../IsyTextarea/IsyTextarea'
import { IsyDropDown, IsyDropDownOptionProps } from '../IsyDropDown/IsyDropDown'
import { IsyChipInput } from '../IsyChipInput/IsyChipInput'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import {
	IsyRadioGroup,
	IsyRadioGroupItemProps,
} from '../IsyRadioGroup/IsyRadioGroup'
import { IsyCheckbox } from '../IsyCheckbox/IsyCheckbox'
import { IsyButton } from '../IsyButton/IsyButton'
import { IsyGrid } from '../IsyGrid/IsyGrid'
import { MultiselectOptionProps } from 'multiselect-react-dropdown'
import { IsyChipSelect } from '../IsyChipSelect/IsyChipSelect'
import IsyAccordion, { IsyAccordionProps } from '../IsyAccordion/IsyAccordion'
import './IsyFormBuilder.scss'
import IsyAnimatedInput from '../IsyAnimatedInput/IsyAnimatedInput'
import './IsyFormBuilder.scss'
import { IsyRadio } from '../IsyRadio/IsyRadio'
import { IsyFileInput } from '../IsyFileInput/IsyFileInput'
import { ImageBuffer } from '../../infrastructure/imageBuffer'
import {
	PLATFORM_TRANSLATIONS,
	COMMON_SELECT_ALL,
	COMMON_CLEAR_ALL,
} from '../../utilities/constants'
import { useTranslation } from 'react-i18next'

export enum IsyFormBuilderFormTypes {
	SECTION,
	ACCORDION_SECTION,
	ARRAY_SECTION,
	GRID_SECTION,
	ANIMATE_STRING,
	ANIMATE_PASSWORD,
	STRING,
	FILE_SELECT,
	PASSWORD,
	NUMBER,
	TEXT_AREA,
	EMAIL,
	SELECT,
	CHIP_INPUT,
	CHIP_SELECT,
	RADIO_GROUP,
	CHECKBOX,
	RADIO,
	SWITCH,
	COMPONENT,
	ANY_OF_ONE,
	CHECKBOX_LIST,
	BUTTON,
	INFO,
}

export interface IsyFormBuilderCFormProps {
	type: IsyFormBuilderFormTypes
	className?: string
	viewClassName?: string
	title?: string
	dataRef: string
	placeholder?: string
	isRequired?: boolean
	disabled?: boolean
	isHidden?: boolean
	onChange?: (val: any, ref: string) => void
}

export interface IsyFormBuilderComponentItemProps
	extends Omit<
		IsyFormBuilderCFormProps,
		'isRequired' | 'disabled' | 'onChange' | 'title' | 'viewClassName'
	> {
	type: IsyFormBuilderFormTypes.COMPONENT
	className?: string
	dataRef: string
	componentType: string
}

export interface IsyFormBuilderSectionProps
	extends Omit<
		IsyFormBuilderCFormProps,
		'isRequired' | 'disabled' | 'onChange' | 'dataRef'
	> {
	type: IsyFormBuilderFormTypes.SECTION
	forms?: IsyFormBuilderFormItemProps[]
	dataRef?: string
}

export interface IsyFormBuilderAnyOfOneProps
	extends Pick<
		IsyFormBuilderCFormProps,
		'type' | 'isHidden' | 'dataRef' | 'title'
	> {
	className?: null
	type: IsyFormBuilderFormTypes.ANY_OF_ONE
	getChoice: (data: any) => number
	forms: IsyFormBuilderControlItemProps[]
}

export interface IsyFormBuilderAccordionSectionProps
	extends Omit<IsyFormBuilderSectionProps, 'type'> {
	type: IsyFormBuilderFormTypes.ACCORDION_SECTION
	isExpandByDefault?: boolean
	openDataRef?: string
}

export interface IsyFormBuilderArraySectionProps
	extends Omit<
		IsyFormBuilderCFormProps,
		'isRequired' | 'disabled' | 'onChange'
	> {
	type: IsyFormBuilderFormTypes.ARRAY_SECTION
	addLabel?: string
	addDefault?: any
	fixedLengthArray?: boolean
	forms?: IsyFormBuilderFormItemProps[]
	minimum?: number
	maximum?: number
	multiLabel?: boolean
	onAdd?: (ref: string, index: number) => void
	onDelete?: (ref: string, index: number) => void
}

export interface IsyFormBuilderGridSectionProps
	extends Omit<
		IsyFormBuilderCFormProps,
		'isRequired' | 'disabled' | 'onChange'
	> {
	type: IsyFormBuilderFormTypes.GRID_SECTION
	addLabel?: string
	addDefault?: any
	addButtonAtEnd?: boolean
	fixedLengthArray?: boolean
	isRowDisabled?: (data: any) => boolean
	forms?: Array<IsyFormBuilderControlItemProps | IsyFormBuilderAnyOfOneProps>
	minimum?: number
	maximum?: number
	onAdd?: (ref: string, index: number) => void
	onDelete?: (ref: string, index: number) => void
}
export interface IsyFormBuilderInputProps extends IsyFormBuilderCFormProps {
	placeholder?: string
	autoFocus?: boolean
}

export interface IsyFormBuilderStringProps extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.STRING
	onBlur?: (value: string) => void
}

export interface IsyFormBuilderFileSelectProps
	extends Omit<IsyFormBuilderInputProps, 'placeholder'> {
	type: IsyFormBuilderFormTypes.FILE_SELECT
	accept?: string
	multiple?: boolean
}

export interface IsyFormBuilderAnimateStringProps
	extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.ANIMATE_STRING
	onKeyUp?: (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => void
}

export interface IsyFormBuilderNumberProps extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.NUMBER
}

export interface IsyFormBuilderPasswordProps extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.PASSWORD
}

export interface IsyFormBuilderAnimatePasswordProps
	extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.ANIMATE_PASSWORD
	onKeyUp?: (
		event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
	) => void
}

export interface IsyFormBuilderEmailProps extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.EMAIL
}

export interface IsyFormBuilderTextAreaProps extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.TEXT_AREA
	resize?: boolean
}

export interface IsyFormBuilderChipSelectProps
	extends IsyFormBuilderCFormProps {
	type: IsyFormBuilderFormTypes.CHIP_SELECT
	placeholder?: string
	options: Array<string | MultiselectOptionProps>
}

export interface IsyFormBuilderChipInputProps extends IsyFormBuilderInputProps {
	type: IsyFormBuilderFormTypes.CHIP_INPUT
	chipLength?: number
}

export interface IsyFormBuilderSelectProps extends IsyFormBuilderCFormProps {
	type: IsyFormBuilderFormTypes.SELECT
	options: Array<IsyDropDownOptionProps<string>> | string
}

export interface IsyFormBuilderCheckboxProps
	extends Omit<IsyFormBuilderCFormProps, 'isRequired' | 'title'> {
	type: IsyFormBuilderFormTypes.CHECKBOX
	label: string
}

export interface IsyFormBuilderCheckboxListItemProps {
	label: string
	value: any
	disabled?: boolean
}

export interface IsyFormBuilderCheckboxListProps
	extends Omit<IsyFormBuilderCFormProps, 'isRequired'> {
	type: IsyFormBuilderFormTypes.CHECKBOX_LIST
	items: IsyFormBuilderCheckboxListItemProps[] | string
}

export interface IsyFormBuilderRadioProps
	extends Omit<IsyFormBuilderCFormProps, 'isRequired' | 'title'> {
	type: IsyFormBuilderFormTypes.RADIO
	forms?: IsyFormBuilderFormItemProps[]
	label: string
}

export interface IsyFormBuilderSwitchProps
	extends Omit<IsyFormBuilderCFormProps, 'isRequired' | 'title'> {
	type: IsyFormBuilderFormTypes.SWITCH
	label: string
}

export interface IsyFormBuilderRadioGroupProps
	extends IsyFormBuilderCFormProps {
	type: IsyFormBuilderFormTypes.RADIO_GROUP
	options: Array<IsyRadioGroupItemProps<string>>
}

export interface IsyFormBuilderButtonProps
	extends Omit<
		IsyFormBuilderCFormProps,
		'isRequired' | 'title' | 'placeholder' | 'dataRef'
	> {
	type: IsyFormBuilderFormTypes.BUTTON
	label: string
	onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export interface IsyFormBuilderInfoProps
	extends Pick<IsyFormBuilderCFormProps, 'type' | 'isHidden'> {
	type: IsyFormBuilderFormTypes.INFO
	text: string
	className?: string
}

export type IsyFormBuilderInputItemProps =
	| IsyFormBuilderStringProps
	| IsyFormBuilderFileSelectProps
	| IsyFormBuilderAnimateStringProps
	| IsyFormBuilderNumberProps
	| IsyFormBuilderPasswordProps
	| IsyFormBuilderAnimatePasswordProps
	| IsyFormBuilderEmailProps
	| IsyFormBuilderTextAreaProps
	| IsyFormBuilderSelectProps
	| IsyFormBuilderChipInputProps
	| IsyFormBuilderRadioGroupProps
	| IsyFormBuilderChipSelectProps

export type IsyFormBuilderControlItemProps =
	| IsyFormBuilderInputItemProps
	| IsyFormBuilderCheckboxProps
	| IsyFormBuilderRadioProps
	| IsyFormBuilderSwitchProps

export type IsyFormBuilderFormItemProps =
	| IsyFormBuilderSectionProps
	| IsyFormBuilderAccordionSectionProps
	| IsyFormBuilderArraySectionProps
	| IsyFormBuilderGridSectionProps
	| IsyFormBuilderControlItemProps
	| IsyFormBuilderComponentItemProps
	| IsyFormBuilderAnyOfOneProps
	| IsyFormBuilderCheckboxListProps
	| IsyFormBuilderButtonProps
	| IsyFormBuilderInfoProps

export interface IsyFormBuilderProps<T> {
	forms: IsyFormBuilderFormItemProps[]
	data: T
	errors?: Object
	onChange: (data: T) => void
	isViewMode?: boolean
	componentRender?: (
		componentType: string,
		data: any,
		isViewMode: boolean,
		updateCallback: (data: any) => void
	) => React.ReactNode
	optionsObject?: {
		[key: string]: Array<
			IsyDropDownOptionProps<any> | IsyFormBuilderCheckboxListItemProps
		>
	}
}

export function IsyFormBuilder<T>(props: IsyFormBuilderProps<T>) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const getMainForms = () => {
		if (isArray(props.forms)) {
			return props.forms
		}
		return []
	}

	const getSubForms = (
		section:
			| IsyFormBuilderSectionProps
			| IsyFormBuilderArraySectionProps
			| IsyFormBuilderAccordionSectionProps
			| IsyFormBuilderRadioProps
	) => {
		if (isArray(section.forms)) {
			return section.forms
		}
		return []
	}

	const getSectionClassName = (form: IsyFormBuilderFormItemProps) => {
		if (isString(form.className)) {
			return form.className
		}
		return ''
	}

	const getSectionRef = (
		section:
			| IsyFormBuilderSectionProps
			| IsyFormBuilderArraySectionProps
			| IsyFormBuilderGridSectionProps
			| IsyFormBuilderAccordionSectionProps
			| IsyFormBuilderComponentItemProps,
		ref: string
	) => {
		return isEmpty(section.dataRef)
			? ref
			: (isEmpty(ref) ? '' : ref + '.') + section.dataRef
	}

	const getCheckboxListData = (
		input: IsyFormBuilderCheckboxListProps,
		ref: string
	) => {
		let data = getFormValue(input, ref)
		return isArray(data) ? data : []
	}

	const getCheckboxListItems = (
		input: IsyFormBuilderCheckboxListProps
	): IsyFormBuilderCheckboxListItemProps[] => {
		if (
			isString(input.items) &&
			!isNil(props.optionsObject) &&
			isArray(props.optionsObject[input.items])
		) {
			return props.optionsObject[
				input.items
			] as IsyFormBuilderCheckboxListItemProps[]
		} else if (isArray(input.items)) {
			return input.items
		}
		return []
	}

	const getFormValue = (
		form:
			| IsyFormBuilderControlItemProps
			| IsyFormBuilderComponentItemProps
			| IsyFormBuilderCheckboxListProps
			| IsyFormBuilderAnyOfOneProps
			| IsyFormBuilderAccordionSectionProps,
		ref: string,
		isFile?: boolean,
		isControlledAccordion?: boolean
	) => {
		const tempRef = isControlledAccordion
			? (form as IsyFormBuilderAccordionSectionProps).openDataRef
			: form.dataRef
		const access = isEmpty(tempRef)
			? ref
			: (isEmpty(ref) ? '' : ref + '.') + tempRef
		const data = get(props.data, access)
		if (isFile) {
			if (isArray(data)) {
				return data.map(ImageBuffer.Instance.retrieveImage)
			}
			return ImageBuffer.Instance.retrieveImage(data)
		}
		return data
	}

	const getArrayValue = (ref: string) => {
		const val = get(props.data, ref)
		if (isArray(val)) {
			return val
		}
		return []
	}

	const getErrorString = (
		form: IsyFormBuilderControlItemProps,
		ref: string
	) => {
		const access = (isEmpty(ref) ? '' : ref + '.') + form.dataRef
		return get(props.errors, access)
	}

	const handleUpdateSectionValue = (
		form:
			| IsyFormBuilderControlItemProps
			| IsyFormBuilderComponentItemProps
			| IsyFormBuilderCheckboxListProps
			| IsyFormBuilderAccordionSectionProps,
		ref: string,
		value: any,
		isFile?: boolean,
		isControlledAccordion?: boolean
	) => {
		const tempRef = isControlledAccordion
			? (form as IsyFormBuilderAccordionSectionProps).openDataRef
			: form.dataRef
		const access = isEmpty(tempRef)
			? ref
			: (isEmpty(ref) ? '' : ref + '.') + tempRef
		const updatedData = cloneDeep(props.data as any)
		let data = value
		if (isFile && !isNil(value)) {
			if (isArray(value)) {
				data = value.map(ImageBuffer.Instance.storeImage)
			} else {
				data = ImageBuffer.Instance.storeImage(value)
			}
		}
		set(updatedData, access, data)
		if (isFunction((form as any).onChange)) {
			;(form as any).onChange(value, access)
			return
		}
		if (isFunction(props.onChange)) {
			props.onChange(updatedData)
		}
	}

	const handleDeleteArraySectionItem = (
		section: IsyFormBuilderArraySectionProps | IsyFormBuilderGridSectionProps,
		ref: string,
		index: number
	) => {
		let data = [...getArrayValue(ref)]
		if (isFunction(section.onDelete)) {
			section.onDelete(ref, index)
		}
		if (data.length > index && isFunction(props.onChange)) {
			data.splice(index, 1)
			const updatedData = cloneDeep(props.data as any)
			set(updatedData as any, ref, data)
			if (isFunction(props.onChange)) {
				props.onChange(updatedData)
			}
		}
	}

	const handleAddArraySectionItem = (
		section: IsyFormBuilderArraySectionProps | IsyFormBuilderGridSectionProps,
		ref: string
	) => {
		let data = [...getArrayValue(ref)]
		if (isFunction(section.onAdd)) {
			section.onAdd(ref, data.length)
		}
		if (isFunction(props.onChange)) {
			data.push(section.addDefault)
			const updatedData = cloneDeep(props.data as any)
			set(updatedData as any, ref, data)
			if (isFunction(props.onChange)) {
				props.onChange(updatedData)
			}
		}
	}

	const handleUpdateCheckboxListItem = (
		input: IsyFormBuilderCheckboxListProps,
		ref: string,
		value: any,
		isSelected: boolean
	) => {
		const data = [...getCheckboxListData(input, ref)]
		const index = data.indexOf(value)
		if (isSelected) {
			if (index === -1) {
				data.push(value)
			}
		} else {
			if (index !== -1) {
				data.splice(index, 1)
			}
		}
		handleUpdateSectionValue(input, ref, data)
	}

	const handleCheckboxListSelectAll = (
		input: IsyFormBuilderCheckboxListProps,
		ref: string
	) => {
		const allIds = getCheckboxListItems(input).map((d) => d.value)
		handleUpdateSectionValue(input, ref, allIds)
	}

	const handleCheckboxListClearAll = (
		input: IsyFormBuilderCheckboxListProps,
		ref: string
	) => {
		const ids: any = []
		getCheckboxListItems(input).forEach((d) => {
			if (d.disabled) {
				ids.push(d.value)
			}
		})
		handleUpdateSectionValue(input, ref, ids)
	}

	const renderStringInput = (
		input: IsyFormBuilderStringProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}

		return (
			<IsyInput<string>
				type='text'
				autoFocus={input.autoFocus}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				onBlur={input.onBlur}
				value={getFormValue(input, ref)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderFileSelectInput = (
		input: IsyFormBuilderFileSelectProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}

		return (
			<IsyFileInput
				multiple={input.multiple}
				accept={input.accept}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value, true)}
				value={getFormValue(input, ref, true)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderNumberInput = (
		input: IsyFormBuilderNumberProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}
		return (
			<IsyInput<number>
				type='number'
				autoFocus={input.autoFocus}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderEmailInput = (
		input: IsyFormBuilderEmailProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}
		return (
			<IsyInput<string>
				type='email'
				autoFocus={input.autoFocus}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderChipSelect = (
		input: IsyFormBuilderChipSelectProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			const val = getFormValue(input, ref)
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{isArray(val) ? val.join(', ') : val}
				</div>
			)
		}
		return (
			<IsyChipSelect
				options={input.options}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderChipInput = (
		input: IsyFormBuilderChipInputProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			const val = getFormValue(input, ref)
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{isArray(val) ? val.join(', ') : val}
				</div>
			)
		}
		return (
			<IsyChipInput
				autoFocus={input.autoFocus}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				chipLength={input.chipLength}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderPasswordInput = (
		input: IsyFormBuilderPasswordProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{new Array(getFormValue(input, ref).length + 1).join('*')}
				</div>
			)
		}
		return (
			<IsyInput<string>
				type='password'
				autoFocus={input.autoFocus}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderTextArea = (
		input: IsyFormBuilderTextAreaProps,
		ref: string,
		isDisabled?: boolean
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}
		return (
			<IsyTextarea
				autoFocus={input.autoFocus}
				placeholder={input.placeholder}
				disabled={input.disabled || isDisabled}
				resize={input.resize}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderSelect = (
		input: IsyFormBuilderSelectProps,
		ref: string,
		isDisabled?: boolean
	) => {
		let options: IsyDropDownOptionProps<any>[] = []
		if (isString(input.options)) {
			if (
				!isNil(props.optionsObject) &&
				isArray(props.optionsObject[input.options])
			) {
				options = props.optionsObject[input.options] as Array<
					IsyDropDownOptionProps<any>
				>
			}
		} else {
			options = input.options
		}
		if (props.isViewMode) {
			const value = getFormValue(input, ref)
			const selectedOption = find(options, (item) => item.id === value)
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{isNil(selectedOption) ? '' : selectedOption.name}
				</div>
			)
		}
		return (
			<IsyDropDown
				options={options}
				disabled={input.disabled || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				placeholder={input.placeholder}
				className={isNil(getErrorString(input, ref)) ? '' : 'error'}
			/>
		)
	}

	const renderRadioGroup = (
		input: IsyFormBuilderRadioGroupProps,
		ref: string,
		isDisabled?: boolean
	) => {
		return (
			<IsyRadioGroup
				options={input.options}
				disabled={input.disabled || props.isViewMode || isDisabled}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
			/>
		)
	}

	const renderRadio = (
		input: IsyFormBuilderRadioProps,
		index: number,
		ref: string
	) => {
		return (
			<IsyRadio
				key={index}
				label={input.label}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				selected={getFormValue(input, ref)}
				disabled={input.disabled || props.isViewMode}
				children={getSubForms(input).map((v, i) =>
					renderSection(v, i, ref, false)
				)}
			/>
		)
	}

	const renderCheckbox = (
		input: IsyFormBuilderCheckboxProps,
		index: number,
		ref: string
	) => {
		return (
			<IsyCheckbox
				disabled={input.disabled || props.isViewMode}
				key={index}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				label={input.label}
			/>
		)
	}

	const renderCheckboxList = (
		input: IsyFormBuilderCheckboxListProps,
		index: number,
		ref: string
	) => {
		const data = getCheckboxListData(input, ref)
		return (
			<div
				className={
					'isy-form-builder-checkbox-list ' + getSectionClassName(input)
				}
				key={index}
			>
				{renderInputHeader(input)}
				<div className='select-clear-all-holder'>
					<span
						className='select-all'
						onClick={() => handleCheckboxListSelectAll(input, ref)}
					>
						{t(COMMON_SELECT_ALL)}
					</span>
					<span
						className='clear-all'
						onClick={() => handleCheckboxListClearAll(input, ref)}
					>
						{t(COMMON_CLEAR_ALL)}
					</span>
				</div>
				{getCheckboxListItems(input).map((v, i) => {
					return (
						<IsyCheckbox
							disabled={v.disabled || props.isViewMode}
							key={index}
							onChange={(isSelected) =>
								handleUpdateCheckboxListItem(input, ref, v.value, isSelected)
							}
							value={data.indexOf(v.value) !== -1}
							label={v.label}
						/>
					)
				})}
			</div>
		)
	}

	const renderAnimateString = (
		input: IsyFormBuilderAnimateStringProps,
		index: number,
		ref: string
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}
		return (
			<div className='isy-animation-input'>
				<IsyAnimatedInput<string>
					key={index}
					type='text'
					value={getFormValue(input, ref)}
					label={input.title}
					onChange={(value) => handleUpdateSectionValue(input, ref, value)}
					placeholder={input.placeholder}
					onKeyUp={input.onKeyUp}
					autoFocus={input.autoFocus}
					disabled={input.disabled}
					className={isNil(getErrorString(input, ref)) ? '' : 'error'}
				/>
				<span className='error-label'>{getErrorString(input, ref)}</span>
			</div>
		)
	}

	const renderAnimatePassword = (
		input: IsyFormBuilderAnimatePasswordProps,
		index: number,
		ref: string
	) => {
		if (props.isViewMode) {
			return (
				<div className={'isy-form-builder-view-content ' + input.viewClassName}>
					{getFormValue(input, ref)}
				</div>
			)
		}
		return (
			<div className='isy-animation-input'>
				<IsyAnimatedInput<string>
					key={index}
					type='password'
					value={getFormValue(input, ref)}
					label={input.title}
					onChange={(value) => handleUpdateSectionValue(input, ref, value)}
					placeholder={input.placeholder}
					onKeyUp={input.onKeyUp}
					autoFocus={input.autoFocus}
					disabled={input.disabled}
					className={isNil(getErrorString(input, ref)) ? '' : 'error'}
				/>
				<span className='error-label'>{getErrorString(input, ref)}</span>
			</div>
		)
	}

	const renderSwitch = (
		input: IsyFormBuilderSwitchProps,
		index: number,
		ref: string
	) => {
		return (
			<IsyCheckbox
				disabled={input.disabled || props.isViewMode}
				key={index}
				onChange={(value) => handleUpdateSectionValue(input, ref, value)}
				value={getFormValue(input, ref)}
				label={input.label}
			/>
		)
	}

	const renderButton = (input: IsyFormBuilderButtonProps, index: number) => {
		return (
			<IsyButton
				className={input.className}
				onClick={input.onClick}
				disabled={input.disabled}
				key={index}
			>
				{input.label}
			</IsyButton>
		)
	}

	const renderInfo = (input: IsyFormBuilderInfoProps, index: number) => {
		return (
			<div className={'isy-form-builder-info ' + input.className} key={index}>
				{input.text}
			</div>
		)
	}

	const renderInputContent = (
		input: IsyFormBuilderInputItemProps,
		ref: string,
		isDisabled?: boolean
	) => {
		switch (input.type) {
			case IsyFormBuilderFormTypes.STRING:
				return renderStringInput(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.FILE_SELECT:
				return renderFileSelectInput(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.NUMBER:
				return renderNumberInput(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.PASSWORD:
				return renderPasswordInput(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.EMAIL:
				return renderEmailInput(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.CHIP_INPUT:
				return renderChipInput(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.TEXT_AREA:
				return renderTextArea(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.SELECT:
				return renderSelect(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.RADIO_GROUP:
				return renderRadioGroup(input, ref, isDisabled)
			case IsyFormBuilderFormTypes.CHIP_SELECT:
				return renderChipSelect(input, ref, isDisabled)
			default:
				return null
		}
	}

	const renderInputHeader = (
		input: IsyFormBuilderInputItemProps | IsyFormBuilderCheckboxListProps
	) => {
		if (!isNil(input.title)) {
			return (
				<span className='isy-form-builder-input-header' title={input.title}>
					{input.title}
					{(input as IsyFormBuilderInputItemProps).isRequired &&
						!props.isViewMode && (
							<span className='isy-form-builder-input-required'>*</span>
						)}
				</span>
			)
		}
		return null
	}

	const renderFormInput = (
		input: IsyFormBuilderInputItemProps,
		index: number,
		ref: string,
		isGrid: boolean,
		isDisabled?: boolean
	) => {
		return (
			<div
				className={'isy-form-builder-input ' + getSectionClassName(input)}
				key={index + (input as any)}
			>
				{!isGrid && renderInputHeader(input)}
				{renderInputContent(input, ref, isDisabled)}
				<span className='error-label'>{getErrorString(input, ref)}</span>
			</div>
		)
	}

	const renderSectionHeader = (
		section:
			| IsyFormBuilderSectionProps
			| IsyFormBuilderArraySectionProps
			| IsyFormBuilderGridSectionProps
			| IsyFormBuilderAccordionSectionProps
	) => {
		if (!isNil(section.title)) {
			return (
				<span className='isy-form-builder-section-header' title={section.title}>
					{section.title}
				</span>
			)
		}
		return null
	}

	const renderObjectSection = (
		section: IsyFormBuilderSectionProps,
		index: number,
		pRef: string
	) => {
		const ref = getSectionRef(section, pRef)
		return (
			<div
				className={'isy-form-builder-section ' + getSectionClassName(section)}
				key={index}
			>
				{renderSectionHeader(section)}
				{getSubForms(section).map((v, i) => renderSection(v, i, ref, false))}
			</div>
		)
	}

	const renderAnyOfOne = (
		section: IsyFormBuilderAnyOfOneProps,
		_: number,
		pRef: string,
		isGrid: boolean,
		isDisabled?: boolean
	): any => {
		const data = getFormValue(section, pRef)
		const renderIndex = section.getChoice(data)
		if (renderIndex < section.forms.length) {
			return renderSection(
				section.forms[renderIndex],
				renderIndex,
				pRef,
				isGrid,
				isDisabled
			)
		}
		return null
	}

	const renderAccordionSection = (
		section: IsyFormBuilderAccordionSectionProps,
		index: number,
		pRef: string
	) => {
		const ref = getSectionRef(section, pRef)
		let controlProps: Partial<IsyAccordionProps> = {}
		if (!isNil(section.openDataRef)) {
			controlProps = {
				isOpen: getFormValue(section, ref, false, true),
				onOpenToggle: (isExpanded: boolean) => {
					handleUpdateSectionValue(section, ref, isExpanded, false, true)
				},
			}
		}
		return (
			<div
				className={
					'isy-form-builder-accordion-section ' + getSectionClassName(section)
				}
				key={index}
			>
				<IsyAccordion
					header={renderSectionHeader(section)}
					isExpandByDefault={section.isExpandByDefault}
					{...controlProps}
				>
					{getSubForms(section).map((v, i) => renderSection(v, i, ref, false))}
				</IsyAccordion>
				{}
			</div>
		)
	}

	const renderArraySectionDelete = (
		section: IsyFormBuilderArraySectionProps | IsyFormBuilderGridSectionProps,
		ref: string,
		index: number,
		isRowDisabled?: boolean
	) => {
		if (props.isViewMode || section.fixedLengthArray || isRowDisabled) {
			return null
		}
		let data = getArrayValue(ref)
		if (!isNil(section.minimum) && data.length <= section.minimum) {
			return null
		}
		return (
			<div className='delete-icon'>
				<img
					src='/images/Delete.svg'
					alt='delete-icon'
					tabIndex={0}
					onClick={() => {
						handleDeleteArraySectionItem(section, ref, index)
					}}
				/>
			</div>
		)
	}

	const renderArraySectionAddButton = (
		section: IsyFormBuilderArraySectionProps | IsyFormBuilderGridSectionProps,
		ref: string
	) => {
		if (props.isViewMode || section.fixedLengthArray) {
			return null
		}
		let data = getArrayValue(ref)
		if (!isNil(section.maximum) && section.maximum <= data.length) {
			return null
		}
		return (
			<IsyButton
				className='secondary-btn isy-form-builder-array-section-add'
				onClick={() => handleAddArraySectionItem(section, ref)}
			>
				<AddOutlinedIcon />
				{section.addLabel}
			</IsyButton>
		)
	}

	const renderArraySectionForm = (
		section: IsyFormBuilderArraySectionProps,
		ref: string
	) => {
		const children = []
		let data = getArrayValue(ref)
		for (let i = 0, iLen = data.length; i < iLen; i++) {
			const cRef = ref + '[' + i + ']'
			children.push(
				<div className='isy-form-builder-array-section-item' key={i}>
					{section.multiLabel && renderSectionHeader(section)}
					{getSubForms(section).map((v, i) => renderSection(v, i, cRef, false))}
					{renderArraySectionDelete(section, ref, i)}
				</div>
			)
		}
		return children
	}

	const renderArraySection = (
		section: IsyFormBuilderArraySectionProps,
		index: number,
		pRef: string
	) => {
		const ref = getSectionRef(section, pRef)
		return (
			<div
				className={
					'isy-form-builder-array-section ' + getSectionClassName(section)
				}
				key={index}
			>
				{!section.multiLabel && renderSectionHeader(section)}
				{renderArraySectionForm(section, ref)}
				{renderArraySectionAddButton(section, ref)}
			</div>
		)
	}

	const renderGridSectionForm = (
		section: IsyFormBuilderGridSectionProps,
		ref: string
	) => {
		let data = getArrayValue(ref)
		if (data.length === 0) {
			return null
		}
		const config = (section.forms || []).map(
			(
				form: IsyFormBuilderControlItemProps | IsyFormBuilderAnyOfOneProps,
				i: number
			) => {
				return {
					header: (form as any).title,
					dataRef: (_: any, __: any, rowIndex: number) => {
						let isRowDisabled = false
						if (isFunction(section.isRowDisabled)) {
							isRowDisabled = section.isRowDisabled(data[rowIndex])
						}
						return renderSection(
							form,
							rowIndex,
							ref + '[' + rowIndex + ']',
							true,
							isRowDisabled
						)
					},
				}
			}
		)
		if (!props.isViewMode) {
			config.push({
				header: '',
				dataRef: (_: any, __: any, rowIndex: number) => {
					let isRowDisabled = false
					if (isFunction(section.isRowDisabled)) {
						isRowDisabled = section.isRowDisabled(data[rowIndex])
					}
					return renderArraySectionDelete(section, ref, rowIndex, isRowDisabled)
				},
			})
		}

		return <IsyGrid data={data} config={config} hideHeader={props.isViewMode} />
	}

	const renderGridSection = (
		section: IsyFormBuilderGridSectionProps,
		index: number,
		pRef: string
	) => {
		const ref = getSectionRef(section, pRef)
		return (
			<div
				className={
					'isy-form-builder-array-section ' + getSectionClassName(section)
				}
				key={index}
			>
				{renderSectionHeader(section)}
				{!section.addButtonAtEnd && renderArraySectionAddButton(section, ref)}
				{renderGridSectionForm(section, ref)}
				{section.addButtonAtEnd && renderArraySectionAddButton(section, ref)}
			</div>
		)
	}

	const renderComponent = (
		section: IsyFormBuilderComponentItemProps,
		index: number,
		pRef: string
	) => {
		return (
			<div
				className={'isy-form-builder-component ' + getSectionClassName(section)}
				key={index}
			>
				{isFunction(props.componentRender)
					? props.componentRender(
							section.componentType,
							getFormValue(section, pRef),
							props.isViewMode || false,
							(value) => handleUpdateSectionValue(section, pRef, value)
					  )
					: null}
			</div>
		)
	}

	const renderSection = (
		form: IsyFormBuilderFormItemProps,
		index: number,
		ref: string,
		isGrid: boolean,
		isDisabled?: boolean
	) => {
		if (form.isHidden) {
			return null
		}
		switch (form.type) {
			case IsyFormBuilderFormTypes.SECTION:
				return renderObjectSection(form, index, ref)
			case IsyFormBuilderFormTypes.ACCORDION_SECTION:
				return renderAccordionSection(form, index, ref)
			case IsyFormBuilderFormTypes.ARRAY_SECTION:
				return renderArraySection(form, index, ref)
			case IsyFormBuilderFormTypes.GRID_SECTION:
				return renderGridSection(form, index, ref)
			case IsyFormBuilderFormTypes.COMPONENT:
				return renderComponent(form, index, ref)
			case IsyFormBuilderFormTypes.ANY_OF_ONE:
				return renderAnyOfOne(form, index, ref, isGrid, isDisabled)
			case IsyFormBuilderFormTypes.CHECKBOX_LIST:
				return renderCheckboxList(form, index, ref)
			case IsyFormBuilderFormTypes.STRING:
			case IsyFormBuilderFormTypes.FILE_SELECT:
			case IsyFormBuilderFormTypes.NUMBER:
			case IsyFormBuilderFormTypes.PASSWORD:
			case IsyFormBuilderFormTypes.EMAIL:
			case IsyFormBuilderFormTypes.CHIP_INPUT:
			case IsyFormBuilderFormTypes.TEXT_AREA:
			case IsyFormBuilderFormTypes.SELECT:
			case IsyFormBuilderFormTypes.RADIO_GROUP:
			case IsyFormBuilderFormTypes.CHIP_SELECT:
				return renderFormInput(form, index, ref, isGrid, isDisabled)
			case IsyFormBuilderFormTypes.ANIMATE_PASSWORD: {
				return renderAnimatePassword(form, index, ref)
			}
			case IsyFormBuilderFormTypes.ANIMATE_STRING: {
				return renderAnimateString(form, index, ref)
			}
			case IsyFormBuilderFormTypes.CHECKBOX:
				return renderCheckbox(form, index, ref)
			case IsyFormBuilderFormTypes.RADIO:
				return renderRadio(form, index, ref)
			case IsyFormBuilderFormTypes.SWITCH:
				return renderSwitch(form, index, ref)
			case IsyFormBuilderFormTypes.BUTTON:
				return renderButton(form, index)
			case IsyFormBuilderFormTypes.INFO:
				return renderInfo(form, index)
			default:
				return null
		}
	}

	const render = () => {
		return (
			<div
				className={
					'isy-form-builder-container ' + (props.isViewMode ? 'view-mode' : '')
				}
			>
				{getMainForms().map((v, i) => renderSection(v, i, '', false))}
			</div>
		)
	}

	return render()
}
