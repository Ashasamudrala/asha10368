import React from 'react'
import { isArray, isEmpty, isFunction, isNil } from 'lodash'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import './IsyDropDown.scss'

export interface IsyDropDownOptionProps<T> {
	id: T
	name?: string
	icon?: React.ReactNode
}

export interface IsyDropDownProps<T> {
	title?: string
	required?: boolean
	value: T
	placeholder?: string
	options: Array<IsyDropDownOptionProps<T>>
	disabled?: boolean
	className?: string
	onChange: (value: any) => void
}

export function IsyDropDown<T>(props: IsyDropDownProps<T>) {
	const getValue = isEmpty(props.value) ? 'none' : [props.value]
	const getOptions = () => {
		if (isArray(props.options)) {
			return props.options
		}
		return []
	}

	const handleOnChange = (
		evt: React.ChangeEvent<{ name?: string; value: unknown }>
	) => {
		if (isFunction(props.onChange)) {
			evt.stopPropagation()
			props.onChange(evt.target.value)
		}
	}

	const renderTitle = () => {
		if (isEmpty(props.title)) {
			return null
		}
		return (
			<label className='isy-dropdown-label'>
				{props.title}
				{props.required && <span className='isy-dropdown-required'> *</span>}
			</label>
		)
	}

	const renderOption = (option: IsyDropDownOptionProps<T>) => {
		return (
			<MenuItem
				key={option.id as any}
				value={option.id as any}
				className={`${props.className || ''} isy-dropdown-option`}
			>
				{!isNil(option.icon) && option.icon}
				{!isNil(option.name) && (
					<span className='isy-dropdown-option-label'>{option.name}</span>
				)}
			</MenuItem>
		)
	}

	const renderSelect = () => {
		return (
			<Select
				onChange={handleOnChange}
				className={`isy-dropdown-control ${props.className || ''}`}
				value={getValue}
				disabled={props.disabled}
			>
				{props.placeholder && (
					<MenuItem
						value='none'
						className={`${props.className || ''} isy-dropdown-option`}
						disabled
						selected
						hidden
					>
						{props.placeholder}
					</MenuItem>
				)}
				{getOptions().map(renderOption)}
			</Select>
		)
	}

	return (
		<div className='isy-dropdown'>
			{renderTitle()}
			{renderSelect()}
		</div>
	)
}
