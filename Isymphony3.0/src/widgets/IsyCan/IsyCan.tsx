import { isFunction } from 'lodash'
import { useSelector } from 'react-redux'
import { selectGetLogInUserInfo } from '../../authAndPermissions/loginUserDetails.selectors'
import { IsyPermissionTypes } from '../../authAndPermissions/loginUserDetails.types'

export interface IsyCanProps {
	action: IsyPermissionTypes
	yes?: () => React.ReactNode
	no?: () => React.ReactNode
}

export function IsyCan(props: IsyCanProps) {
	const userDetails = useSelector(selectGetLogInUserInfo)
	const check = () => {
		if (!userDetails) {
			return false
		} else if (
			(userDetails.permissions &&
				userDetails.permissions.includes(props.action)) ||
			(userDetails.permissions as any) === 'All'
		) {
			return true
		}
		return false
	}

	const renderYes = (): React.ReactNode => {
		if (isFunction(props.yes)) {
			return props.yes()
		}
		return null
	}

	const renderNo = (): React.ReactNode => {
		if (isFunction(props.no)) {
			return props.no()
		}
		return null
	}

	return <>{check() ? renderYes() : renderNo()}</>
}
