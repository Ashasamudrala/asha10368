import { mount } from 'enzyme'
import { IsyCan, IsyCanProps } from './IsyCan'
import { FrameworkSettings } from '../../features/admin/core/framework/frameworkSettings/FrameworkSettings'
import * as reactRedux from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { IsyPermissionTypes } from '../../authAndPermissions/loginUserDetails.types'
import { IsyLogoHeader } from '../IsyLogoHeader/IsyLogoHeader'

describe('canList Test', () => {
	let wrapper: any
	let store: any
	const props: IsyCanProps = {
		action: IsyPermissionTypes.CREATE_FRAMEWORK,
		yes: () => <IsyLogoHeader />,
	}

	beforeEach(() => {
		store = configureStore([thunk])({
			loginUserDetails: {
				loginInfo: { permissions: ['CREATE_FRAMEWORK'] },
			},
		})

		jest
			.spyOn(reactRedux, 'useSelector')
			.mockImplementation((callback: (arg: any) => any) =>
				callback(store.getState())
			)

		wrapper = mount(
			<Provider store={store}>
				<IsyCan {...props} />
			</Provider>
		)
	})

	afterEach(() => {
		jest.spyOn(reactRedux, 'useSelector').mockClear()
	})

	it('should render IsyLogoHeader ', () => {
		expect(wrapper.contains(<IsyLogoHeader />)).toBe(true)
	})
	it('should not render FrameworkSettings ', () => {
		expect(wrapper.contains(<FrameworkSettings />)).toBe(false)
	})
})
