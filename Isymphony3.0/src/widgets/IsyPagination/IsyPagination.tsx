import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import TablePagination, {
	TablePaginationBaseProps,
	TablePaginationTypeMap,
} from '@material-ui/core/TablePagination'
import { OverridableComponent } from '@material-ui/core/OverridableComponent'
import './isyPagination.scss'

interface TablePaginationProps extends TablePaginationBaseProps {
	component: string
}
const TablePaginationBar = withStyles({
	toolbar: {
		minHeight: 32,
		height: 32,
		overflow: 'hidden',
	},
	actions: {
		marginLeft: '4px',
	},
	selectRoot: {
		marginRight: '8px',
		marginLeft: '0',
	},
	select: {
		marginTop: '4px',
		'&:focus': {
			backgroundColor: '#ffffff',
		},
	},
	menuItem: {
		fontSize: '12px',
	},
})(
	TablePagination as OverridableComponent<
		TablePaginationTypeMap<{}, React.ComponentType<TablePaginationProps>>
	>
)

export interface IsyPaginationProps {
	labelName: string
	totalCount: number
	currentPage: number
	recordsPerPage: number
	rowsPerPageOptions: number[]
	onChangeRowsPerPage: (value: number) => void
	onChangePage: (page: number) => void
	className?: string
}

export function IsyPagination(props: IsyPaginationProps) {
	const {
		labelName,
		totalCount,
		currentPage,
		recordsPerPage,
		rowsPerPageOptions,
		className,
	} = props

	const handleChangeRowsPerPage = (
		event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
	) => {
		props.onChangeRowsPerPage(parseInt(event.target.value))
	}

	const handleChangePage = (
		_: React.MouseEvent<HTMLButtonElement> | null,
		page: number
	) => {
		props.onChangePage(page)
	}
	return (
		<TablePaginationBar
			labelRowsPerPage={labelName}
			count={totalCount}
			component='div'
			page={currentPage}
			rowsPerPage={recordsPerPage}
			rowsPerPageOptions={rowsPerPageOptions}
			onChangeRowsPerPage={handleChangeRowsPerPage}
			onChangePage={handleChangePage}
			className={`table-pagination ${className}`}
		/>
	)
}
