import React from 'react'
import { shallow } from 'enzyme'
import { IsyTextarea, IsyTextareaProps } from './IsyTextarea'

describe('<IsyTextarea />', () => {
	let wrapper: any

	const props: IsyTextareaProps = {
		title: 'textarea',
		required: true,
		value: 'name',
		resize: false,
		placeholder: 'Enter textarea data',
		disabled: false,
		autoFocus: false,
		className: 'text-area-container',
		heightBasedOnContent: false,
		onChange: jest.fn(),
	}

	describe('IsyTextarea component', () => {
		beforeEach(() => {
			wrapper = shallow(<IsyTextarea {...props} />)
		})

		it('should change value when OnChange was called', () => {
			const mockEvent = { target: { value: 'this is for just test' } }
			wrapper.find('textarea').simulate('change', mockEvent)
			expect(props.onChange).toHaveBeenCalledWith(mockEvent.target.value)
		})
		it('renders an `.textarea` with classname', () => {
			expect(wrapper.find('textarea').hasClass('text-area-container')).toEqual(
				true
			)
		})
		it('checks the placeholder of textarea', () => {
			expect(wrapper.find('textarea').props().placeholder).toEqual(
				'Enter textarea data'
			)
		})
		it('checks the value of textarea', () => {
			expect(wrapper.find('textarea').props().value).toEqual('name')
		})
		it('checks the value of textarea is disabled', () => {
			expect(wrapper.find('textarea').props().disabled).toEqual(false)
		})
		it('checks the  textarea label is displayed', () => {
			expect(wrapper.find('.isy-textarea-label').text()).toEqual('textarea *')
		})
	})
})
