import React from 'react'
import './IsyTextarea.scss'
import { isEmpty, isFunction, isNil } from 'lodash'

export interface IsyTextareaProps {
	title?: string
	required?: boolean
	value: string
	resize?: boolean
	placeholder?: string
	disabled?: boolean
	autoFocus?: boolean
	className?: string
	heightBasedOnContent?: boolean
	onChange: (value: string) => void
}

export class IsyTextarea extends React.Component<IsyTextareaProps> {
	element: HTMLTextAreaElement | null = null

	componentDidMount() {
		this.setHeightOfTextArea()
	}

	componentDidUpdate() {
		this.setHeightOfTextArea()
	}

	setHeightOfTextArea = () => {
		const e = this.element
		if (this.props.heightBasedOnContent && !isNil(e)) {
			e.style.height = '1px'
			e.style.height = 25 + e.scrollHeight + 'px'
		}
	}

	handleRef = (i: HTMLTextAreaElement | null) => {
		this.element = i
	}

	handleOnChange = (evt: React.ChangeEvent<HTMLTextAreaElement>) => {
		if (isFunction(this.props.onChange)) {
			this.props.onChange(evt.target.value as string)
		}
	}

	renderTitle = () => {
		if (isEmpty(this.props.title)) {
			return null
		}
		return (
			<label className='isy-textarea-label'>
				{this.props.title}
				{this.props.required && (
					<span className='isy-textarea-required'> *</span>
				)}
			</label>
		)
	}

	renderTextArea = () => {
		return (
			<textarea
				ref={this.handleRef}
				onKeyUp={this.setHeightOfTextArea}
				className={`isy-textarea-control ${
					this.props.className ? this.props.className : ''
				}`}
				style={this.props.resize ? {} : { resize: 'none' }}
				value={this.props.value}
				onChange={this.handleOnChange}
				disabled={this.props.disabled}
				placeholder={this.props.placeholder}
				autoFocus={this.props.autoFocus}
			/>
		)
	}

	render() {
		return (
			<div className='isy-textarea'>
				{this.renderTitle()}
				{this.renderTextArea()}
			</div>
		)
	}
}
