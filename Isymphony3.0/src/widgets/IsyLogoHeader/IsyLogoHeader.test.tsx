import React from 'react'
import { shallow } from 'enzyme'
import { IsyLogoHeader } from './IsyLogoHeader'

describe('testing image is displaying or not', () => {
	it('Test click event', () => {
		const wrapper = shallow(<IsyLogoHeader />)
		expect(wrapper.find('img').prop('src')).toEqual(`/images/iSymphonyLogo.svg`)
	})
})
