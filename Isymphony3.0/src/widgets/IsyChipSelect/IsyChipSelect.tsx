import React from 'react'
import { isEmpty, isFunction, isNil } from 'lodash'
import ExpandMore from '@material-ui/icons/ExpandMore'
import { Multiselect, MultiselectOptionProps } from 'multiselect-react-dropdown'
import './IsyChipSelect.scss'

export interface IsyChipSelectProps {
	title?: string
	required?: boolean
	value: string[]
	options: Array<string | MultiselectOptionProps>
	disabled?: boolean
	className?: string
	placeholder?: string
	onChange: (value: string[]) => void
}
export function IsyChipSelect(props: IsyChipSelectProps) {
	const multiSelectItem = React.createRef<Multiselect>()

	const handleMultiSelect = () => {
		// workaround the multi select component don't have support for dropdown icon.
		if (
			!isNil(multiSelectItem) &&
			!isNil(multiSelectItem.current) &&
			!isNil(multiSelectItem.current.searchBox) &&
			!isNil(multiSelectItem.current.searchBox.current)
		) {
			if (multiSelectItem.current.state.toggleOptionsList) {
				multiSelectItem.current.searchBox.current.blur()
			} else {
				multiSelectItem.current.searchBox.current.focus()
			}
		}
	}

	const handleOnSelect = (selectedItems: string[]) => {
		if (isFunction(props.onChange)) {
			props.onChange(selectedItems)
		}
	}

	const renderTitle = () => {
		if (isEmpty(props.title)) {
			return null
		}
		return (
			<label className='isy-chip-select-label'>
				{props.title}
				{props.required && <span className='isy-chip-select-required'> *</span>}
			</label>
		)
	}

	const renderChipSelect = () => {
		return (
			<Multiselect
				className={`isy-chip-select-control ${props.className || ''}`}
				options={props.options}
				onSelect={handleOnSelect}
				onRemove={handleOnSelect}
				placeholder={props.placeholder}
				hidePlaceholder={true}
				ref={multiSelectItem as any}
				selectedValues={props.value}
				isObject={false}
			/>
		)
	}

	const renderChipSelectIcon = () => {
		return (
			<ExpandMore
				className='isy-chip-input-control-icon'
				onClick={handleMultiSelect}
			/>
		)
	}

	return (
		<div className='isy-chip-select'>
			{renderTitle()}
			{renderChipSelect()}
			{renderChipSelectIcon()}
		</div>
	)
}
