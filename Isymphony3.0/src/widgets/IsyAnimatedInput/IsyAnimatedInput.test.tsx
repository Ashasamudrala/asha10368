import React from 'react'
import { mount } from 'enzyme'
import IsyAnimatedInput from './IsyAnimatedInput'
import { IsyAnimatedInputProps } from './IsyAnimatedInput'

describe('<IsyAnimatedInput />', () => {
	let wrapper: any

	const props: IsyAnimatedInputProps<string> = {
		type: 'text',
		placeholder: 'password',
		onChange: jest.fn(),
		onKeyUp: jest.fn(),
		label: 'password',
		autoFocus: true,
		disabled: true,
		value: '',
		className: 'isy-animated',
	}

	beforeEach(() => {
		wrapper = mount(<IsyAnimatedInput {...props} />)
	})

	it('IsyAnimatedInputProps props with type is equal to text', () => {
		expect(wrapper.props().type).toEqual('text')
	})

	it('IsyAnimatedInputProps props with placeholder is equal to password', () => {
		expect(wrapper.props().placeholder).toEqual('password')
	})
	it('should change value when OnChange was called', () => {
		const mockEvent = { target: { value: 'this is for just test' } }
		wrapper.find('input').simulate('change', mockEvent)
		expect(props.onChange).toHaveBeenCalledWith(mockEvent.target.value)
	})
	// it('should change value when onkeypress was called', () => {
	//     const mockEvent = { target: { value: 'this is for just test' } }
	//     wrapper.find('input').simulate('keypress', mockEvent)
	//     expect(props.onKeyUp).toHaveBeenCalledWith(mockEvent)
	// })
	it('checks the value of animated input is disabled', () => {
		expect(wrapper.find('input').props().disabled).toEqual(true)
	})
	it('renders an `.isy-animated` with classname', () => {
		expect(wrapper.childAt(0).hasClass('isy-animated')).toEqual(true)
	})
})

describe('Content input', () => {
	const props: IsyAnimatedInputProps<string> = {
		type: 'password',
		placeholder: 'password',
		onChange: jest.fn(),
		onKeyUp: jest.fn(),
		label: 'password',
		autoFocus: true,
		disabled: true,
		value: 'data',
		className: 'isy-animated',
	}

	const setState = jest.fn()
	const useStateMock: any = (initState: any) => [initState, setState]

	// let wrapper =  mount(<IsyAnimatedInput {...props} />)

	it('Should call set state  onClick', () => {
		jest.spyOn(React, 'useState').mockImplementation(useStateMock)
		let wrapper = mount(<IsyAnimatedInput {...props} />)
		wrapper.find('button').simulate('click')
		expect(setState).toBeTruthy()
	})
})
