import React from 'react'
import Breadcrumbs from '@material-ui/core/Breadcrumbs'
import Link from '@material-ui/core/Link'
import { NavigateNext } from '@material-ui/icons'
import './isyBreadCrumbPage.scss'

import { Container } from '@material-ui/core'
import { Route, useRouteMatch } from 'react-router-dom'
import { getComponentsBasedOnRouteId } from '../../utilities/routes'
import { RouteProps } from '../../common.types'

/**
 * This component is to create the sideBar.
 */

function PageRoute({
	children,
	className,
	...rest
}: {
	children: any
	className?: string
	path: string
}) {
	return (
		<Route {...rest}>
			<Container className={className} fixed={false}>
				{children}
			</Container>
		</Route>
	)
}

export interface IsyBreadCrumbPageProps {
	route: RouteProps
}

export function IsyBreadCrumbPage(props: IsyBreadCrumbPageProps) {
	const { route } = props
	const { path } = useRouteMatch()

	const getCurrentActiveOne = () => {
		const { pathname } = window.location
		const splitter = pathname.split('/')
		return splitter[splitter.length - 1]
	}

	const renderBreadCrumb = () => {
		return (
			<div className='notification-container'>
				<div className='breadCrumb-container'>
					<Breadcrumbs
						aria-label='breadcrumb'
						separator={<NavigateNext fontSize='small' />}
					>
						<Link color='inherit'>{route.title}</Link>
						<Link color='inherit'>{getCurrentActiveOne()}</Link>
					</Breadcrumbs>
				</div>
			</div>
		)
	}

	const renderRoutes = () => {
		return (route.children || []).map((route) => {
			return (
				<PageRoute path={path + route.path} key={route.id}>
					{getComponentsBasedOnRouteId(route.id)}
				</PageRoute>
			)
		})
	}

	return (
		<div className={'content-container ' + route.containerClass}>
			{renderBreadCrumb()}
			{renderRoutes()}
		</div>
	)
}
