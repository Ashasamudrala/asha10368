import React, { useState } from 'react'
import { isFunction, find, isNil, isNumber, findIndex } from 'lodash'
import './isyTabs.scss'
import ChevronLeftSharpIcon from '@material-ui/icons/ChevronLeftSharp'
import ChevronRightSharpIcon from '@material-ui/icons/ChevronRightSharp'
import { IsyPopover } from '../IsyPopover/IsyPopover'

export interface IsyTabItemProps<T> {
	id: T
	header:
		| string
		| ((tab: IsyTabItemProps<T>) => React.ReactNode)
		| React.ReactNode
	content?: React.ReactNode | ((tab: IsyTabItemProps<T>) => React.ReactNode)
}

export interface IsyTabsProps<T> {
	tabs: IsyTabItemProps<T>[]
	maxTabsCountToShow?: number
	className?: string
	activeTab: T
	onTabChange: (tab: IsyTabItemProps<T>) => void
}

export function IsyTabs<T>(props: IsyTabsProps<T>) {
	const { tabs, activeTab } = props

	const [
		popoverLeftAnchorEl,
		setLeftPopoverAnchorEl,
	] = useState<Element | null>(null)

	const [
		popoverRightAnchorEl,
		setRightPopoverAnchorEl,
	] = useState<Element | null>(null)

	const getLegalTabsIndexes = () => {
		let start = 0
		let end = tabs.length
		if (isNumber(props.maxTabsCountToShow)) {
			const currentSelectedTabIndex = findIndex(
				tabs,
				(t: IsyTabItemProps<T>) => t.id === activeTab
			)
			if (currentSelectedTabIndex >= props.maxTabsCountToShow) {
				if (currentSelectedTabIndex === tabs.length - 1) {
					end = tabs.length
					start = tabs.length - props.maxTabsCountToShow
				} else {
					const mean = props.maxTabsCountToShow / 2
					start = props.maxTabsCountToShow - Math.floor(mean)
					end = props.maxTabsCountToShow + Math.ceil(mean)
					if (end > tabs.length) {
						start = start - (end - tabs.length)
					}
				}
			} else if (end > props.maxTabsCountToShow) {
				end = props.maxTabsCountToShow
			}
		}
		return { start, end }
	}

	const getTabHeaderContent = (tab: IsyTabItemProps<T>) => {
		if (isFunction(tab.header)) {
			return tab.header(tab)
		}
		if (isNil(tab.header)) {
			return ''
		}
		return tab.header
	}

	const getActiveTabContent = () => {
		const tab = find(tabs, (t) => t.id === activeTab)
		if (isNil(tab) || isNil(tab.content)) {
			return ''
		}
		if (isFunction(tab.content)) {
			tab.content(tab)
		}
		return tab.content
	}

	const handleTabChange = (tab: IsyTabItemProps<T>) => {
		if (isFunction(props.onTabChange)) {
			props.onTabChange(tab)
		}
	}

	const handleShowLeftMenu = (e: React.MouseEvent<SVGSVGElement>) => {
		e.stopPropagation()
		setLeftPopoverAnchorEl(e.currentTarget)
	}

	const handleCloseLeftMenu = () => {
		setLeftPopoverAnchorEl(null)
	}

	const handleShowRightMenu = (e: React.MouseEvent<SVGSVGElement>) => {
		e.stopPropagation()
		setRightPopoverAnchorEl(e.currentTarget)
	}

	const handleCloseRightMenu = () => {
		setRightPopoverAnchorEl(null)
	}

	const handleTabSelectedFromList = (
		_: React.MouseEvent<HTMLLIElement, MouseEvent>,
		key: string
	) => {
		const tab = find(tabs, (t) => (t.id as any) === key)
		setLeftPopoverAnchorEl(null)
		setRightPopoverAnchorEl(null)
		if (!isNil(tab)) {
			handleTabChange(tab)
		}
	}

	const renderTab = (tab: IsyTabItemProps<T>) => {
		return (
			<div
				key={tab.id as any}
				className={'isy-tab-header ' + (tab.id === activeTab ? 'active' : '')}
				onClick={() => handleTabChange(tab)}
			>
				{getTabHeaderContent(tab)}
			</div>
		)
	}

	const renderLeftList = (list: IsyTabItemProps<T>[]) => {
		if (list.length === 0) {
			return null
		}
		return (
			<>
				<span className='left-icon'>
					<ChevronLeftSharpIcon className='icon' onClick={handleShowLeftMenu} />
				</span>
				{!isNil(popoverLeftAnchorEl) && (
					<IsyPopover
						anchorEl={popoverLeftAnchorEl}
						actions={list.reverse().map((t) => ({
							name: getTabHeaderContent(t) as any,
							key: t.id as any,
						}))}
						onSelect={handleTabSelectedFromList}
						onClose={handleCloseLeftMenu}
						className='isy-tabs-menu'
					/>
				)}
			</>
		)
	}

	const renderRightList = (list: IsyTabItemProps<T>[]) => {
		if (list.length === 0) {
			return null
		}
		return (
			<>
				<span className='right-icon'>
					<ChevronRightSharpIcon
						className='icon'
						onClick={handleShowRightMenu}
					/>
				</span>
				{!isNil(popoverRightAnchorEl) && (
					<IsyPopover
						anchorEl={popoverRightAnchorEl}
						actions={list.map((t) => ({
							name: getTabHeaderContent(t) as any,
							key: t.id as any,
						}))}
						onSelect={handleTabSelectedFromList}
						onClose={handleCloseRightMenu}
						className='isy-tabs-menu'
					/>
				)}
			</>
		)
	}

	const renderMainList = (
		list: IsyTabItemProps<T>[],
		isRight: boolean,
		isLeft: boolean
	) => {
		let className = 'isy-tabs-root'
		if (isRight && isLeft) {
			className = className + ' right-left'
		} else if (isRight) {
			className = className + ' right'
		} else if (isLeft) {
			className = className + ' left'
		}
		return <div className={className}>{list.map(renderTab)}</div>
	}

	const renderTabs = () => {
		const indexes = getLegalTabsIndexes()
		const tabsToRenderInView = tabs.slice(indexes.start, indexes.end)
		const leftList = indexes.start === 0 ? [] : tabs.slice(0, indexes.start)
		const rightList = indexes.end === tabs.length ? [] : tabs.slice(indexes.end)

		return (
			<>
				{renderLeftList(leftList)}
				{renderMainList(
					tabsToRenderInView,
					rightList.length !== 0,
					leftList.length !== 0
				)}
				{renderRightList(rightList)}
			</>
		)
	}

	const renderContent = () => {
		return <div className='isy-tabs-content'>{getActiveTabContent()}</div>
	}

	return (
		<div className={'isy-tabs-container ' + props.className || ''}>
			{renderTabs()}
			{renderContent()}
		</div>
	)
}
