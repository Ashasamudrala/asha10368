import React from 'react'
import { isArray, isEmpty, isFunction, isNumber } from 'lodash'
import ChipInput, { ChipRendererArgs } from 'material-ui-chip-input'
import { Chip } from '@material-ui/core'
export interface IsyChipInputProps {
	title?: string
	required?: boolean
	value: string[]
	className?: string
	placeholder?: string
	disabled?: boolean
	autoFocus?: boolean
	chipLength?: number
	onChange: (value: string[]) => void
}

export function IsyChipInput(props: IsyChipInputProps) {
	const getValue = () => {
		if (isArray(props.value)) {
			return props.value
		}
		return []
	}

	const getInputProps = () => {
		if (isNumber(props.chipLength)) {
			return {
				maxLength: props.chipLength,
			}
		}
		return {}
	}

	const handleAddChip = (chip: string) => {
		if (isFunction(props.onChange)) {
			const value = chip.split(' ').join('')
			props.onChange([...getValue(), value])
		}
	}

	const handleRemoveChip = (_: string, index: number) => {
		if (isFunction(props.onChange) && index < getValue().length) {
			const value = [...getValue()]
			value.splice(index, 1)
			props.onChange(value)
		}
	}

	const renderTitle = () => {
		if (isEmpty(props.title)) {
			return null
		}
		return (
			<label className='isy-chip-input-label'>
				{props.title}
				{props.required && <span className='isy-chip-input-required'> *</span>}
			</label>
		)
	}

	const chipRenderer = (args: ChipRendererArgs, key: any) => {
		return (
			<Chip
				className={`isy-chip-input-chip ${args.className} ${
					args.isFocused ? 'focus' : ''
				}`}
				key={key}
				label={args.chip}
				onClick={args.handleClick}
				onDelete={args.handleDelete}
			/>
		)
	}

	const renderChipInput = () => {
		return (
			<ChipInput
				value={getValue()}
				className={`isy-chip-input-control ${props.className || ''}`}
				chipRenderer={chipRenderer}
				onAdd={handleAddChip}
				onDelete={handleRemoveChip}
				disabled={props.disabled}
				InputProps={{
					inputProps: getInputProps(),
					autoFocus: props.autoFocus,
				}}
				placeholder={props.placeholder}
				disableUnderline={true}
				newChipKeys={['Enter', ',']}
			/>
		)
	}

	return (
		<div className='isy-chip-input'>
			{renderTitle()}
			{renderChipInput()}
		</div>
	)
}
