import React from 'react'
import { shallow } from 'enzyme'
import { IsyInput, IsyInputProps } from './IsyInput'

describe('<IsyInput />', () => {
	let wrapper: any

	const props: IsyInputProps<String> = {
		value: 'name',
		type: 'text',
		placeholder: 'Enter details about the framework',
		disabled: false,
		autoFocus: true,
		className: 'input',
		onChange: jest.fn(),
		onBlur: jest.fn(),
	}

	describe('Isyinput component', () => {
		beforeEach(() => {
			wrapper = shallow(<IsyInput {...props} />)
		})

		it('should change value when OnChange was called', () => {
			const mockEvent = { target: { value: 'this is for just test' } }
			wrapper.find('input').simulate('change', mockEvent)
			expect(props.onChange).toHaveBeenCalledWith(mockEvent.target.value)
		})
		it('should change value when onBlur was called', () => {
			const mockEvent = { target: { value: 'this is for just test' } }
			wrapper.find('input').simulate('blur', mockEvent)
			expect(props.onBlur).toHaveBeenCalledWith(mockEvent.target.value)
		})
		it('renders an `.input`', () => {
			expect(wrapper.find(props.className)).toHaveLength(1)
		})
		it('checks the placeholder of input', () => {
			expect(wrapper.find('.input').at(0).props().placeholder).toEqual(
				'Enter details about the framework'
			)
		})
		it('checks the value of input', () => {
			expect(wrapper.find('input').props().value).toEqual('name')
		})
		it('checks the value of input is disabled', () => {
			expect(wrapper.find('input').props().disabled).toEqual(false)
		})
	})
})
