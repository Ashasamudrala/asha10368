import React from 'react'
import { isFunction, isBoolean, isString } from 'lodash'
import './isyInput.scss'

export interface IsyInputProps<T> {
	value: T
	type?: string
	placeholder?: string
	disabled?: boolean
	autoFocus?: boolean
	className?: string
	onChange?: (value: T) => void
	onBlur?: (value: T) => void
}

export interface IsyInputInternalState<T> {
	value: T
}

export class IsyInput<T> extends React.Component<
	IsyInputProps<T>,
	IsyInputInternalState<T>
> {
	constructor(props: IsyInputProps<T>) {
		super(props)
		this.state = {
			value: props.value,
		}
		this.handleOnChange = this.handleOnChange.bind(this)
		this.handleOnBlur = this.handleOnBlur.bind(this)
	}

	// eslint-disable-next-line react/no-deprecated
	componentWillReceiveProps(newProps: IsyInputProps<T>) {
		if (newProps.value !== this.state.value) {
			this.setState({
				value: newProps.value,
			})
		}
	}

	getInputValue(): any {
		if (isFunction(this.props.onChange)) {
			return this.props.value
		}
		return this.state.value
	}

	getIsDisabled() {
		if (isBoolean(this.props.disabled)) {
			return this.props.disabled
		}
		return false
	}

	getPlaceholder() {
		if (isString(this.props.placeholder)) {
			return this.props.placeholder
		}
		return ''
	}

	handleOnChange(evt: React.ChangeEvent<HTMLInputElement>) {
		if (isFunction(this.props.onChange)) {
			this.props.onChange(evt.target.value as any)
		} else {
			this.setState({
				value: evt.target.value as any,
			})
		}
	}

	handleOnBlur(evt: React.FocusEvent<HTMLInputElement>) {
		if (isFunction(this.props.onBlur)) {
			this.props.onBlur(evt.target.value as any)
		}
	}

	render() {
		console.log('tata', this.props)
		return (
			<input
				type={this.props.type || 'text'}
				className={'isy_input ' + this.props.className}
				value={this.getInputValue()}
				placeholder={this.getPlaceholder()}
				disabled={this.getIsDisabled()}
				onChange={this.handleOnChange}
				onBlur={this.handleOnBlur}
				autoFocus={this.props.autoFocus || false}
			/>
		)
	}
}
