import React from 'react'
import { Radio } from '@material-ui/core'
import { isFunction, isNil } from 'lodash'
import './IsyRadio.scss'

export interface IsyRadioProps {
	label: string
	selected: boolean
	disabled?: boolean
	children?:
		| React.ReactNode
		| React.ReactNodeArray
		| (() => React.ReactNode | React.ReactNodeArray)
	onChange: (value: boolean) => void
}

export function IsyRadio(props: IsyRadioProps) {
	const handleOnChange = () => {
		if (isFunction(props.onChange) && !props.disabled) {
			props.onChange(!props.selected)
		}
	}

	const renderRadio = () => {
		return (
			<div className='isy-radio-button'>
				<Radio
					className='isy-radio-class'
					checked={props.selected}
					onChange={(e) => handleOnChange()}
					disabled={props.disabled}
				/>
				<span className='isy-radio-label' onClick={() => handleOnChange()}>
					{props.label}
				</span>
			</div>
		)
	}

	const renderChildren = () => {
		if (props.selected) {
			if (isFunction(props.children)) {
				return (
					<div className='isy-radio-button-children'>{props.children()}</div>
				)
			}
			if (!isNil(props.children)) {
				return <div className='isy-radio-button-children'>{props.children}</div>
			}
		}
		return null
	}

	return (
		<>
			{renderRadio()}
			{renderChildren()}
		</>
	)
}
