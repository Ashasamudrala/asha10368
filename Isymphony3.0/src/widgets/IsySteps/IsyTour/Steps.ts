import {
	isFunction,
	forEach,
	isString,
	isUndefined,
	isNull,
	cloneDeep,
	isNil,
	isEqual,
} from 'lodash'
import './IsyTour.scss'

export interface IsyStepItemProps {
	element?: string | HTMLElement
	description?: string
	position?: ToolTipPosition
	tooltipClass?: string
	highlightClass?: string
	title?: string
	zIndexNotSupport?: boolean
	hideNextButton?: boolean
	addNewButton?: boolean
	addCompleteDetailsButton?: boolean
	addSkipButton?: boolean
	addStartButton?: boolean
	hidePrevButton?: boolean
	showStepNumbers?: boolean
	scrollTo?: string
	step?: number
	disableInteraction?: boolean
	dummyElement?: string
}

export enum ToolTipPosition {
	TOP = 'top',
	RIGHT = 'right',
	BOTTOM = 'bottom',
	LEFT = 'left',
	BOTTOM_LEFT_ALIGNED = 'bottom-left-aligned',
	BOTTOM_MIDDLE_ALIGNED = 'bottom-middle-aligned',
	BOTTOM_RIGHT_ALIGNED = 'bottom-right-aligned',
	TOP_LEFT_ALIGNED = 'top-left-aligned',
	TOP_MIDDLE_ALIGNED = 'top-middle-aligned',
	TOP_RIGHT_ALIGNED = 'top-right-aligned',
	Auto = 'auto',
	Floating = 'floating',
}

export interface IsyStepsOptionProps {
	nextLabel?: string
	prevLabel?: string
	skipLabel?: string
	doneLabel?: string
	hidePrev?: boolean
	hideNext?: boolean
	tooltipClass?: string
	highlightClass?: string
	showButtons?: boolean
	showBullets?: boolean
	scrollTo?: string
	steps?: IsyStepItemProps[]
	disableInteraction?: boolean
	tooltipPosition?: string
	keyboardNavigation?: boolean
	exitOnEsc?: boolean
	exitOnOverlayClick?: boolean
	showProgress?: boolean
	overlayOpacity?: number
	scrollToElement?: boolean
	buttonClass?: string
	helperElementPadding?: number
	showStepNumbers?: boolean
	nextToDone?: boolean
	scrollPadding?: number
	positionPrecedence?: string[]
	hidePrevButton?: boolean
}

export enum CustomButtonType {
	START = 'start',
	SKIP = 'skip',
	COMPLETE = 'complete',
	EXIT = 'exit',
}

export class IsySteps {
	targetElement: HTMLElement
	stepsItems: IsyStepItemProps[]
	options: IsyStepsOptionProps
	currentStep: number
	isEnabled: boolean
	initialStep: number
	direction: string
	lastShowElementTimer: number | null
	domObj: DOMEvent
	stepsBeforeChangeCallback: null | ((nextStep: number) => boolean)
	stepsExit: null | (() => void)
	stepsBeforeExit: null | (() => boolean)
	stepsBeforeChange: null | ((nextStep: number) => boolean)
	stepsAfterChange: null | (() => void)
	stepsChange: null | ((step: number) => {})
	stepsComplete: null | (() => {})
	stepsSkipCallback: null | (() => {})
	stepsAfterChangeCallback: null | ((step: number) => {})
	getAttribute: null | ((step: string) => number)
	stepCustomButton: null | ((step: number) => number)
	buttonType: null | string
	constructor(te: HTMLElement) {
		this.targetElement = document.body
		this.stepsItems = []
		this.currentStep = -1
		this.initialStep = -1
		this.isEnabled = false
		this.direction = ''
		this.lastShowElementTimer = null
		this.domObj = new DOMEvent(this)
		this.stepsExit = null
		this.stepsBeforeExit = null
		this.stepsBeforeChange = null
		this.stepsAfterChange = null
		this.stepsChange = null
		this.stepsComplete = null
		this.stepsBeforeChangeCallback = null
		this.getAttribute = null
		this.stepsSkipCallback = null
		this.stepsAfterChangeCallback = null
		this.stepCustomButton = null
		this.buttonType = null
		this.options = {
			nextLabel: 'Next',
			prevLabel: 'Back',
			scrollTo: 'element',
			skipLabel: '×',
			doneLabel: 'Done',
			exitOnEsc: true,
			disableInteraction: false,
			keyboardNavigation: false,
			exitOnOverlayClick: false,
			showProgress: false,
			overlayOpacity: 0.5,
			buttonClass: 'steps-button',
			scrollToElement: true,
			helperElementPadding: 10,
			showStepNumbers: false,
			scrollPadding: 30,
			nextToDone: true,
			positionPrecedence: ['bottom', 'top', 'right', 'left'],
		}
	}

	/**
	 * To change the scroll of `window` after highlighting an element
	 *
	 * @api private
	 * @param {String} scrollTo
	 * @param {Object} targetElement
	 * @param {Object} tooltipLayer
	 */

	scrollTo = (
		scrollTo: string,
		ref?: IsyStepItemProps,
		tooltipLayer?: HTMLElement
	) => {
		let element: HTMLElement | undefined
		let rect

		if (ref !== undefined && !isString(ref.element)) {
			element = ref.element
		}

		if (scrollTo === 'off') return
		if (!this.options.scrollToElement) return

		if (scrollTo === 'tooltip' && !isUndefined(tooltipLayer)) {
			rect = tooltipLayer.getBoundingClientRect()
		} else if (!isUndefined(element)) {
			rect = element.getBoundingClientRect()
		}

		if (
			!isUndefined(element) &&
			!this.elementInViewport(element) &&
			!isUndefined(rect)
		) {
			let winHeight = this.getWinSize().height
			let top = rect.bottom - (rect.bottom - rect.top) // TODO (afshinm): do we need scroll padding now?
			// I have changed the scroll option and now it scrolls the window to
			// the center of the target element or tooltip.

			if (top < 0 || element.clientHeight > winHeight) {
				let scrollPadding = this.options.scrollPadding
					? this.options.scrollPadding
					: 0

				window.scrollBy(
					0,
					rect.top - (winHeight / 2 - rect.height / 2) - scrollPadding
				) // 30px padding from edge to look nice
				//Scroll down
			} else {
				let scrollPadding = this.options.scrollPadding
					? this.options.scrollPadding
					: 0
				window.scrollBy(
					0,
					rect.top - (winHeight / 2 - rect.height / 2) + scrollPadding
				) // 30px padding from edge to look nice
			}
		}
	}

	getWinSize = () => {
		if (window.innerWidth !== undefined) {
			return {
				width: window.innerWidth,
				height: window.innerHeight,
			}
		} else {
			let D = document.documentElement
			return {
				width: D.clientWidth,
				height: D.clientHeight,
			}
		}
	}

	elementInViewport = (el: HTMLElement) => {
		if (HTMLElement !== undefined) {
			let rect = el.getBoundingClientRect()
			return (
				rect.top >= 0 &&
				rect.left >= 0 &&
				rect.bottom + 80 <= window.innerHeight && // add 80 to get the text right
				rect.right <= window.innerWidth
			)
		}
		return
	}

	removeChild = (element: Element, animate?: boolean) => {
		if (!element || !element.parentElement) return
		let parentElement = element.parentElement

		if (animate) {
			this.setStyle(element as HTMLElement, {
				opacity: '0',
			})
			window.setTimeout(function () {
				parentElement.removeChild(element)
			}, 500)
		} else {
			parentElement.removeChild(element)
		}
	}

	setStyle = (element: HTMLElement, style: { [key: string]: any }) => {
		let cssText = ''

		if (element && element.style) {
			cssText += element.style.cssText
		}

		for (let rule in style) {
			cssText += ''.concat(rule, ':').concat(style[rule], ';')
		}

		if (element && element.style) {
			element.style.cssText = cssText
		}
	}

	removeShowElement = () => {
		let elms = document.querySelectorAll('.steps-showElement')
		if (elms && elms.length) {
			for (let i = 0, len = elms.length; i < len; i++) {
				this.removeClass(elms[i], /steps-[a-zA-Z]+/g)
			}
		}
	}

	removeClass = (element: Element, classNameRegex: string | RegExp) => {
		if (element instanceof SVGElement) {
			let pre = element.getAttribute('class') || ''
			element.setAttribute(
				'class',
				pre.replace(classNameRegex, '').replace(/^\s+|\s+$/g, '')
			)
		} else {
			element.className = element.className
				.replace(classNameRegex, '')
				.replace(/^\s+|\s+$/g, '')
		}
	}

	onResize = () => {
		this.refresh()
	}

	refresh = () => {
		// re-align steps
		let helperLayer: HTMLElement | null = document.querySelector(
			'.steps-helperLayer'
		)
		let toolTipLayer: HTMLElement | null = document.querySelector(
			'.steps-tooltipReferenceLayer'
		)
		let disableInteractionLayer: HTMLElement | null = document.querySelector(
			'.steps-disableInteraction'
		)
		if (!isNull(helperLayer)) {
			this.setHelperLayerPosition(helperLayer)
		}
		if (!isNull(toolTipLayer)) {
			this.setHelperLayerPosition(toolTipLayer)
		}
		if (!isNull(disableInteractionLayer)) {
			this.setHelperLayerPosition(disableInteractionLayer)
		}

		if (this.currentStep !== undefined && this.currentStep !== null) {
			let oldArrowLayer: HTMLElement | null = document.querySelector(
				'.steps-arrow'
			)
			let oldtooltipContainer: HTMLElement | null = document.querySelector(
				'.steps-tooltip'
			)
			if (
				oldtooltipContainer &&
				oldArrowLayer &&
				this.stepsItems[this.currentStep].element &&
				!isString(this.stepsItems[this.currentStep].element)
			) {
				this.placeTooltip(
					this.stepsItems[this.currentStep].element as HTMLElement,
					oldtooltipContainer,
					oldArrowLayer
				)
			}
		}
		return this
	}

	isFixed = (element: HTMLElement) => {
		let p = element.parentNode

		if (!p || p.nodeName === 'HTML') {
			return false
		}

		if (this.getPropValue(element, 'position') === 'fixed') {
			return true
		}
	}

	exit = () => {
		this.exitSteps(this.targetElement)
	}

	setOptions = (options: IsyStepsOptionProps) => {
		this.options.steps = options.steps
		return this
	}

	getOffset = (element: HTMLElement) => {
		let body = document.body
		let docEl = document.documentElement
		let scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop
		let scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft
		let x = element.getBoundingClientRect()
		return {
			top: x.top + scrollTop,
			width: x.width,
			height: x.height,
			left: x.left + scrollLeft,
		}
	}

	exitSteps = (targetElement: HTMLElement, force?: boolean) => {
		let continueExit: boolean = true

		if (isFunction(this.stepsBeforeExit)) {
			continueExit = this.stepsBeforeExit()
		}

		if (!force && continueExit === false) return // remove overlay layers from the page

		let overlayLayers = targetElement.querySelectorAll('.steps-overlay')

		if (overlayLayers && overlayLayers.length) {
			overlayLayers.forEach((overlayLayer) => {
				return this.removeChild(overlayLayer)
			})
		}
		//remove all helper layers

		let overlayLayersParts = targetElement.querySelectorAll(
			'.steps-parts-overlay'
		)

		if (overlayLayersParts && overlayLayersParts.length) {
			overlayLayersParts.forEach((overlayLayer) => {
				return this.removeChild(overlayLayer)
			})
		}

		let helperLayer = targetElement.querySelector('.steps-helperLayer')
		if (helperLayer !== null) {
			this.removeChild(helperLayer)
		}

		let referenceLayer = targetElement.querySelector(
			'.steps-tooltipReferenceLayer'
		)
		if (referenceLayer) {
			this.removeChild(referenceLayer)
		}

		//remove disableInteractionLayer
		let disableInteractionLayer = targetElement.querySelector(
			'.steps-disableInteraction'
		)

		if (disableInteractionLayer) {
			this.removeChild(disableInteractionLayer)
		}

		//remove steps floating element
		let floatingElement = document.querySelector('.stepsFloatingElement')
		if (floatingElement) {
			this.removeChild(floatingElement)
		}
		this.removeShowElement() //clean listeners
		this.domObj.off(window, 'resize', true) //check if any callback is defined

		if (!isUndefined(this.stepsExit) && isFunction(this.stepsExit)) {
			this.stepsExit()
		}

		//set the step to zero
		this.currentStep = -1
	}

	//To update description whenever the text is changing for the current step
	updateStepsData = (stepsData: IsyStepItemProps[]) => {
		const currentStepData = this.stepsItems[this.currentStep]
		if (stepsData && this.stepsItems.length === stepsData.length) {
			Object.values(stepsData).forEach((data, index) => {
				if (this.stepsItems[index].description) {
					this.stepsItems[index].description = data.description
				}
			})
		} else {
			this.updateStepsInfo()
		}
		let nextStep = this.stepsItems[this.currentStep || 0]
		if (isEqual(nextStep, currentStepData)) return

		if (this.stepsItems.length <= this.currentStep) {
			//end of the step
			//check if any callback is defined
			this.exitSteps(this.targetElement)
			return
		}
		this.showElement(nextStep)
	}

	addOverlayLayer = (targetElm: HTMLElement) => {
		let overlayLayer: HTMLElement = this.createElement('div', {
			className: 'steps-overlay',
		})

		// check if the target element is body, we should calculate the size of overlay layer in a better way

		if (!targetElm.tagName || targetElm.tagName.toLowerCase() === 'body') {
			this.setStyle(overlayLayer, {
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				position: 'fixed',
			})
		} else {
			// set overlay layer position
			let elementPosition = this.getOffset(targetElm)

			if (elementPosition) {
				this.setStyle(overlayLayer, {
					width: ''.concat(elementPosition.width.toString(), 'px'),
					height: ''.concat(elementPosition.height.toString(), 'px'),
					top: ''.concat(elementPosition.top.toString(), 'px'),
					left: ''.concat(elementPosition.left.toString(), 'px'),
				})
			}
		}

		targetElm.appendChild(overlayLayer)

		if (this.options.exitOnOverlayClick === true) {
			this.setStyle(overlayLayer, {
				cursor: 'pointer',
			})

			overlayLayer.onclick = () => {
				this.exitSteps(targetElm)
			}
		}

		return true
	}

	getScrollParent = (element: HTMLElement) => {
		let style = window.getComputedStyle(element)
		let excludeStaticParent = style.position === 'absolute'
		let overflowRegex = /(auto|scroll)/
		if (style.position === 'fixed') return document.body

		for (
			let parent: null | HTMLElement = element;
			(parent = parent.parentElement);

		) {
			style = window.getComputedStyle(parent)

			if (excludeStaticParent && style.position === 'static') {
				continue
			}

			if (
				overflowRegex.test(style.overflow + style.overflowY + style.overflowX)
			)
				return parent
		}

		return document.body
	}

	scrollParentToElement = (targetElement: IsyStepItemProps) => {
		let element = targetElement.element
		if (!this.options.scrollToElement) return
		if (element !== undefined && !isString(element)) {
			let parent = this.getScrollParent(element)
			if (parent === document.body) return
			parent.scrollTop = element.offsetTop - parent.offsetTop
		}
	}

	getPropValue = (element: HTMLElement, propName: string) => {
		let propValue = ''

		if (document.defaultView && document.defaultView.getComputedStyle) {
			//Others
			propValue = document.defaultView
				.getComputedStyle(element, null)
				.getPropertyValue(propName)
		} //Prevent exception in IE

		if (propValue && propValue.toLowerCase) {
			return propValue.toLowerCase()
		} else {
			return propValue
		}
	}

	addClass = (element: HTMLElement, className: string) => {
		if (element instanceof SVGElement) {
			// svg
			let pre = element.getAttribute('class') || ''

			if (!pre.match(className)) {
				// check if element doesn't already have className
				element.setAttribute('class', ''.concat(pre, ' ').concat(className))
			}
		} else {
			if (!isUndefined(element.classList)) {
				// check for modern classList property
				let classes = className.split(' ')
				forEach(classes, function (cls) {
					element.classList.add(cls)
				})
			} else if (!element.className.match(className)) {
				// check if element doesn't already have className
				element.className += ' '.concat(className)
			}
		}
	}

	setHelperLayerPosition = (helperLayer: HTMLElement) => {
		if (helperLayer) {
			//prevent error when `this.currentStep` in undefined
			if (!this.stepsItems[this.currentStep]) return
			let currentElement = this.stepsItems[this.currentStep]
			let elementPosition
			if (
				typeof currentElement.element !== 'string' &&
				currentElement.element !== undefined
			) {
				elementPosition = this.getOffset(currentElement.element)
			}
			let widthHeightPadding: number | undefined = this.options
				.helperElementPadding // If the target element is fixed, the tooltip should be fixed as well.
			// Otherwise, remove a fixed class that may be left over from the previous
			// step.

			if (
				typeof currentElement.element !== 'string' &&
				currentElement.element !== undefined
			) {
				if (this.isFixed(currentElement.element)) {
					this.addClass(helperLayer, 'steps-fixedTooltip')
				} else {
					this.removeClass(helperLayer, 'steps-fixedTooltip')
				}
			}

			if (currentElement.position === ToolTipPosition.Floating) {
				widthHeightPadding = 0
			} //set new position to helper layer

			if (widthHeightPadding && elementPosition) {
				widthHeightPadding = 0
				this.setStyle(helperLayer, {
					width: ''.concat(
						(elementPosition.width + widthHeightPadding).toString(),
						'px'
					),
					height: ''.concat(
						(elementPosition.height + widthHeightPadding).toString(),
						'px'
					),
					top: ''.concat(
						(elementPosition.top - widthHeightPadding / 2).toString(),
						'px'
					),
					left: ''.concat(
						(elementPosition.left - widthHeightPadding / 2).toString(),
						'px'
					),
				})
			}
		}
	}

	checkLeft = (
		targetOffset: {
			top: number
			width: number
			height: number
			left: number
		},
		tooltipLayerStyleRight: number,
		tooltipOffset: {
			top: number
			width: number
			height: number
			left: number
		},
		tooltipLayer: HTMLElement
	) => {
		if (
			targetOffset.left +
				targetOffset.width -
				tooltipLayerStyleRight -
				tooltipOffset.width <
			0
		) {
			// off the left side of the window
			tooltipLayer.style.left = ''.concat((-targetOffset.left).toString(), 'px')
			return false
		}

		tooltipLayer.style.right = ''.concat(
			(tooltipLayerStyleRight as unknown) as string,
			'px'
		)
		return true
	}

	checkRight = (
		targetOffset: {
			top: number
			width: number
			height: number
			left: number
		},
		tooltipLayerStyleLeft: number,
		tooltipOffset: {
			top: number
			width: number
			height: number
			left: number
		},
		windowSize: { width: number; height: number },
		tooltipLayer: HTMLElement
	) => {
		if (
			targetOffset.left + tooltipLayerStyleLeft + tooltipOffset.width >
			windowSize.width
		) {
			// off the right side of the window
			tooltipLayer.style.left = ''.concat(
				(windowSize.width - tooltipOffset.width - targetOffset.left).toString(),
				'px'
			)
			return false
		}
		tooltipLayer.style.left = ''.concat(
			(tooltipLayerStyleLeft as unknown) as string,
			'px'
		)
		return true
	}

	removeEntry = (stringArray: string[], stringToRemove: string) => {
		if (stringArray.includes(stringToRemove)) {
			stringArray.splice(stringArray.indexOf(stringToRemove), 1)
		}
	}

	determineAutoAlignment = (
		offsetLeft: number,
		tooltipWidth: number,
		ref: { width: number; height: number },
		desiredAlignment: string
	) => {
		let width = ref.width
		let halfTooltipWidth = tooltipWidth / 2
		let winWidth = Math.min(width, window.screen.width)
		let possibleAlignments = [
			'-left-aligned',
			'-middle-aligned',
			'-right-aligned',
		]
		let calculatedAlignment = '' // valid left must be at least a tooltipWidth
		// away from right side

		if (winWidth - offsetLeft < tooltipWidth) {
			this.removeEntry(possibleAlignments, '-left-aligned')
		} // valid middle must be at least half
		// width away from both sides

		if (
			offsetLeft < halfTooltipWidth ||
			winWidth - offsetLeft < halfTooltipWidth
		) {
			this.removeEntry(possibleAlignments, '-middle-aligned')
		} // valid right must be at least a tooltipWidth
		// width away from left side

		if (offsetLeft < tooltipWidth) {
			this.removeEntry(possibleAlignments, '-right-aligned')
		}

		if (possibleAlignments.length) {
			if (possibleAlignments.includes(desiredAlignment)) {
				// the desired alignment is valid
				calculatedAlignment = desiredAlignment
			} else {
				// pick the first valid position, in order
				calculatedAlignment = possibleAlignments[0]
			}
		} else {
			// if screen width is too small
			// for ANY alignment, middle is
			// probably the best for visibility
			calculatedAlignment = '-middle-aligned'
		}

		return calculatedAlignment
	}

	determineAutoPosition = (
		targetElement: HTMLElement,
		tooltipLayer: HTMLElement,
		desiredTooltipPosition: string
	) => {
		// Take a clone of position precedence. These will be the available
		let possiblePositions =
			this.options.positionPrecedence && this.options.positionPrecedence.slice()

		let windowSize = this.getWinSize()
		let tooltipHeight = this.getOffset(tooltipLayer).height + 10
		let tooltipWidth = this.getOffset(tooltipLayer).width + 20
		let targetElementRect = targetElement.getBoundingClientRect() // If we check all the possible areas, and there are no valid places for the tooltip, the element
		// must take up most of the screen real estate. Show the tooltip floating in the middle of the screen.

		let calculatedPosition = 'floating'
		/*
		 * auto determine position
		 */
		// Check for space below

		if (
			targetElementRect.bottom + tooltipHeight > windowSize.height &&
			!isUndefined(possiblePositions)
		) {
			this.removeEntry(possiblePositions, 'bottom')
		} // Check for space above

		if (
			targetElementRect.top - tooltipHeight < 0 &&
			!isUndefined(possiblePositions)
		) {
			this.removeEntry(possiblePositions, 'top')
		} // Check for space to the right

		if (
			targetElementRect.right + tooltipWidth > windowSize.width &&
			!isUndefined(possiblePositions)
		) {
			this.removeEntry(possiblePositions, 'right')
		} // Check for space to the left

		if (
			targetElementRect.left - tooltipWidth < 0 &&
			!isUndefined(possiblePositions)
		) {
			this.removeEntry(possiblePositions, 'left')
		} // @let {String}  ex: 'right-aligned'

		let desiredAlignment = (function (pos) {
			let hyphenIndex = pos.indexOf('-')

			if (hyphenIndex !== -1) {
				// has alignment
				return pos.substr(hyphenIndex)
			}

			return ''
		})(desiredTooltipPosition || '') // strip alignment from position

		if (desiredTooltipPosition) {
			// ex: "bottom-right-aligned"
			// should return 'bottom'
			desiredTooltipPosition = desiredTooltipPosition.split('-')[0]
		}

		if (possiblePositions && possiblePositions.length) {
			if (
				desiredTooltipPosition !== 'auto' &&
				possiblePositions.includes(desiredTooltipPosition)
			) {
				// If the requested position is in the list, choose that
				calculatedPosition = desiredTooltipPosition
			} else {
				// Pick the first valid position, in order
				calculatedPosition = possiblePositions[0]
			}
		} // only top and bottom positions have optional alignments
		else {
			//If the position is floating, decreases the width recursively
			tooltipLayer.style.width = ''.concat(
				(tooltipLayer.clientWidth - 10).toString() + 'px'
			)
			calculatedPosition = this.determineAutoPosition(
				targetElement,
				tooltipLayer,
				desiredTooltipPosition
			)
		}

		if (['top', 'bottom'].includes(calculatedPosition)) {
			calculatedPosition += this.determineAutoAlignment(
				targetElementRect.left,
				tooltipWidth,
				windowSize,
				desiredAlignment
			)
		}

		return calculatedPosition
	}

	placeTooltip = (
		targetElement: HTMLElement,
		tooltipLayer: HTMLElement,
		arrowLayer: HTMLElement,
		hintMode?: boolean | undefined | null | Element
	) => {
		let tooltipCssClass: string = ''
		let currentStepObj
		let tooltipOffset
		let targetOffset
		let windowSize
		let currentTooltipPosition
		hintMode = hintMode || false //reset the old style

		tooltipLayer.style.top = ''
		tooltipLayer.style.right = ''
		tooltipLayer.style.bottom = ''
		tooltipLayer.style.left = ''
		tooltipLayer.style.marginLeft = ''
		tooltipLayer.style.marginTop = ''
		tooltipLayer.style.width = ''
		arrowLayer.style.display = 'inherit' //prevent error when `this.currentStep` is undefined

		if (!this.stepsItems[this.currentStep]) return //if we have a custom css class for each step

		currentStepObj = this.stepsItems[this.currentStep]

		if (isString(currentStepObj.tooltipClass)) {
			tooltipCssClass = currentStepObj.tooltipClass
		} else if (this.options.tooltipClass) {
			tooltipCssClass = this.options.tooltipClass
		}

		if (tooltipCssClass) {
			tooltipLayer.className = 'steps-tooltip '
				.concat(tooltipCssClass)
				.replace(/^\s+|\s+$/g, '')
		}
		tooltipLayer.setAttribute('role', 'dialog')
		currentTooltipPosition = this.stepsItems[this.currentStep].position
		// Floating is always valid, no point in calculating

		if (currentTooltipPosition && currentTooltipPosition !== 'floating') {
			currentTooltipPosition = this.determineAutoPosition(
				targetElement,
				tooltipLayer,
				currentTooltipPosition
			)
		}

		let tooltipLayerStyleLeft: number
		let tooltipLayerStyleRight: number
		targetOffset = this.getOffset(targetElement)
		tooltipOffset = this.getOffset(tooltipLayer)
		windowSize = this.getWinSize()

		if (currentTooltipPosition)
			this.addClass(tooltipLayer, 'steps-'.concat(currentTooltipPosition))

		switch (currentTooltipPosition) {
			case ToolTipPosition.TOP_RIGHT_ALIGNED:
				arrowLayer.className = 'steps-arrow bottom-right'
				tooltipLayerStyleRight = 0
				this.checkLeft(
					targetOffset,
					tooltipLayerStyleRight,
					tooltipOffset,
					tooltipLayer
				)
				tooltipLayer.style.bottom = ''.concat(
					(targetOffset.height + 20).toString(),
					'px'
				)
				break

			case ToolTipPosition.TOP_MIDDLE_ALIGNED:
				arrowLayer.className = 'steps-arrow bottom-middle'
				let tooltipLayerStyleLeftRight =
					targetOffset.width / 2 - tooltipOffset.width / 2 // a fix for middle aligned hints

				if (hintMode) {
					tooltipLayerStyleLeftRight += 5
				}

				if (
					this.checkLeft(
						targetOffset,
						tooltipLayerStyleLeftRight,
						tooltipOffset,
						tooltipLayer
					)
				) {
					tooltipLayer.style.right = ''
					this.checkRight(
						targetOffset,
						tooltipLayerStyleLeftRight,
						tooltipOffset,
						windowSize,
						tooltipLayer
					)
				}

				tooltipLayer.style.bottom = ''.concat(
					(targetOffset.height + 20).toString(),
					'px'
				)
				break

			case ToolTipPosition.TOP_LEFT_ALIGNED: // top-left-aligned is the same as the default top
			case ToolTipPosition.TOP:
				arrowLayer.className = 'steps-arrow bottom'
				tooltipLayerStyleLeft = hintMode ? 0 : 15
				this.checkRight(
					targetOffset,
					tooltipLayerStyleLeft,
					tooltipOffset,
					windowSize,
					tooltipLayer
				)
				tooltipLayer.style.bottom = ''.concat(
					(targetOffset.height + 20).toString(),
					'px'
				)
				break

			case ToolTipPosition.RIGHT:
				tooltipLayer.style.left = ''.concat(
					(targetOffset.width + 20).toString(),
					'px'
				)

				if (targetOffset.top + tooltipOffset.height > windowSize.height) {
					// In this case, right would have fallen below the bottom of the screen.
					// Modify so that the bottom of the tooltip connects with the target
					arrowLayer.className = 'steps-arrow left-bottom'
					tooltipLayer.style.top = '-'.concat(
						(tooltipOffset.height - targetOffset.height - 20).toString(),
						'px'
					)
				} else {
					arrowLayer.className = 'steps-arrow left'
				}

				break

			case ToolTipPosition.LEFT:
				if (!hintMode && this.options.showStepNumbers === true) {
					tooltipLayer.style.top = '15px'
				}

				if (targetOffset.top + tooltipOffset.height > windowSize.height) {
					// In this case, left would have fallen below the bottom of the screen.
					// Modify so that the bottom of the tooltip connects with the target
					tooltipLayer.style.top = '-'.concat(
						(tooltipOffset.height - targetOffset.height - 20).toString(),
						'px'
					)
					arrowLayer.className = 'steps-arrow right-bottom'
				} else {
					arrowLayer.className = 'steps-arrow right'
				}

				tooltipLayer.style.right = ''.concat(
					(targetOffset.width + 20).toString(),
					'px'
				)
				break

			case ToolTipPosition.Floating:
				arrowLayer.style.display = 'none' //we have to adjust the top and left of layer manually for steps items without element

				tooltipLayer.style.left = '50%'
				tooltipLayer.style.top = '50%'
				tooltipLayer.style.marginLeft = '-'.concat(
					(tooltipOffset.width / 2).toString(),
					'px'
				)
				tooltipLayer.style.marginTop = '-'.concat(
					(tooltipOffset.height / 2).toString(),
					'px'
				)
				break

			case ToolTipPosition.BOTTOM_RIGHT_ALIGNED:
				arrowLayer.className = 'steps-arrow top-right'
				tooltipLayerStyleRight = 0
				this.checkLeft(
					targetOffset,
					tooltipLayerStyleRight,
					tooltipOffset,
					tooltipLayer
				)
				tooltipLayer.style.top = ''.concat(
					(targetOffset.height + 20).toString(),
					'px'
				)
				break

			case ToolTipPosition.BOTTOM_MIDDLE_ALIGNED:
				arrowLayer.className = 'steps-arrow top-middle'
				if (
					this.checkLeft(
						targetOffset,
						targetOffset.width / 2 - tooltipOffset.width / 2,
						tooltipOffset,
						tooltipLayer
					)
				) {
					tooltipLayer.style.right = ''
					this.checkRight(
						targetOffset,
						targetOffset.width / 2 - tooltipOffset.width / 2,
						tooltipOffset,
						windowSize,
						tooltipLayer
					)
				}

				tooltipLayer.style.top = ''.concat(
					(targetOffset.height + 20).toString(),
					'px'
				)
				break

			default:
				arrowLayer.className = 'steps-arrow top'
				tooltipLayerStyleLeft = 0
				this.checkRight(
					targetOffset,
					tooltipLayerStyleLeft,
					tooltipOffset,
					windowSize,
					tooltipLayer
				)
				tooltipLayer.style.top = ''.concat(
					(targetOffset.height + 20).toString(),
					'px'
				)
		}
	}

	appendChild = (
		parentElement: HTMLElement,
		element: HTMLElement,
		animate?: boolean | undefined
	) => {
		if (animate) {
			let existingOpacity = element.style.opacity || '1'
			this.setStyle(element, {
				opacity: '0',
			})
			window.setTimeout(() => {
				this.setStyle(element, {
					opacity: existingOpacity,
				})
			}, 10)
		}

		parentElement.appendChild(element)
	}

	setAnchorAsButton = (anchor: HTMLElement) => {
		anchor.setAttribute('role', 'button')
		anchor.tabIndex = 0
	}

	getProgress = () => {
		// Steps are 0 indexed
		let currentStep = parseInt((this.currentStep + 1).toString(), 10)
		return (currentStep / this.stepsItems.length) * 100
	}

	previousStep = () => {
		this.direction = 'backward'

		if (this.currentStep === 0) {
			return false
		}

		--this.currentStep
		let nextStep = this.stepsItems[this.currentStep]
		let continueStep = true

		if (isFunction(this.stepsBeforeChange) && nextStep && nextStep.step) {
			continueStep = this.stepsBeforeChange(nextStep.step)
		}

		if (continueStep === false) {
			++this.currentStep
			return false
		}

		this.showElement(nextStep)
	}

	disableInteraction = () => {
		let disableInteractionLayer: HTMLElement | null = document.querySelector(
			'.steps-disableInteraction'
		)

		if (isNull(disableInteractionLayer)) {
			disableInteractionLayer = this.createElement('div', {
				className: 'steps-disableInteraction',
			})

			this.targetElement.appendChild(disableInteractionLayer)
		}

		this.setHelperLayerPosition(disableInteractionLayer)
	}

	setShowElement = (ref: IsyStepItemProps) => {
		let element = ref.element

		if (element !== undefined && !isString(element)) {
			let currentElementPosition = this.getPropValue(element, 'position')
			this.addClass(element, 'steps-showElement')
			if (
				currentElementPosition !== 'absolute' &&
				currentElementPosition !== 'relative' &&
				currentElementPosition !== 'sticky' &&
				currentElementPosition !== 'fixed'
			) {
				//change to new step item
				this.addClass(element, 'steps-relativePosition')
			}
		}
	}

	goToStep = (step: number) => {
		//because steps starts with zero
		this.currentStep = step - 2

		if (!isUndefined(this.stepsItems)) {
			this.nextStep()
		}
	}

	createElement = (tagname: string, attrs?: { className: string }) => {
		let element = document.createElement(tagname)
		if (attrs) {
			element.className = attrs.className
		}
		return element
	}

	updateOverlay = () => {
		let elementPosition
		if (!this.stepsItems[this.currentStep]) {
			return
		}
		let overlayLayersParts = this.targetElement.querySelectorAll(
			'.steps-parts-overlay'
		)

		if (overlayLayersParts && overlayLayersParts.length) {
			overlayLayersParts.forEach((overlayLayer) => {
				return this.removeChild(overlayLayer)
			})
		}
		let currentStep = this.stepsItems[this.currentStep]
		let overlayLayer: HTMLElement | null = this.targetElement.querySelector(
			'.steps-overlay'
		)
		let helperLayer: HTMLElement | null = document.querySelector(
			'.steps-helperLayer'
		)
		if (currentStep.zIndexNotSupport && !isNil(overlayLayer) && helperLayer) {
			this.setStyle(overlayLayer, { visibility: 'hidden' })
			this.setStyle(helperLayer, { visibility: 'hidden' })
			let currentElement = this.stepsItems[this.currentStep]
			if (
				typeof currentElement.element !== 'string' &&
				currentElement.element !== undefined
			) {
				elementPosition = this.getOffset(currentElement.element)
			}
			let overlayLayerTop = this.createElement('div', {
				className: 'steps-overlay-top steps-parts-overlay',
			})
			if (elementPosition) {
				this.setStyle(overlayLayerTop, {
					top: 0,
					height: elementPosition.top + 'px',
					left: 0,
					right: 0,
					position: 'fixed',
					zIndex: 999999,
					background: 'rgba(33,33,33,0.8)',
				})
				let overlayLayerBottom = this.createElement('div', {
					className: 'steps-overlay-bottom steps-parts-overlay',
				})
				this.setStyle(overlayLayerBottom, {
					top: elementPosition.top + elementPosition.height + 'px',
					bottom: 0,
					left: 0,
					right: 0,
					position: 'fixed',
					zIndex: 999999,
					background: 'rgba(33,33,33,0.8)',
				})
				let overlayLayerLeft = this.createElement('div', {
					className: 'steps-overlay-bottom steps-parts-overlay',
				})
				this.setStyle(overlayLayerLeft, {
					top: elementPosition.top + 'px',
					height: elementPosition.height + 'px',
					left: 0,
					width: elementPosition.left + 'px',
					position: 'fixed',
					zIndex: 999999,
					background: 'rgba(33,33,33,0.8)',
				})
				let overlayLayerRight = this.createElement('div', {
					className: 'steps-overlay-bottom steps-parts-overlay',
				})
				this.setStyle(overlayLayerRight, {
					top: elementPosition.top + 'px',
					height: elementPosition.height + 'px',
					left: elementPosition.left + elementPosition.width + 'px',
					right: 0,
					position: 'fixed',
					zIndex: 999999,
					background: 'rgba(33,33,33,0.8)',
				})
				this.targetElement.appendChild(overlayLayerTop)
				this.targetElement.appendChild(overlayLayerBottom)
				this.targetElement.appendChild(overlayLayerLeft)
				this.targetElement.appendChild(overlayLayerRight)
			}
		} else if (!isNil(overlayLayer) && helperLayer) {
			this.setStyle(overlayLayer, { visibility: 'visible' })
			this.setStyle(helperLayer, { visibility: 'visible' })
		}
	}

	showElement = (targetElement: IsyStepItemProps) => {
		if (!isUndefined(this.stepsChange)) {
			if (isFunction(this.stepsChange) && targetElement.step) {
				this.stepsChange(targetElement.step)
			}
		}
		if (isString(targetElement.dummyElement)) {
			targetElement.element = document.querySelector(
				targetElement.dummyElement
			) as HTMLElement
		}
		if (isUndefined(targetElement.element) || isNull(targetElement.element)) {
			let floatingElementQuery = document.querySelector('.stepsFloatingElement')
			if (isNull(floatingElementQuery)) {
				floatingElementQuery = this.createElement('div', {
					className: 'stepsFloatingElement',
				})
			}
			targetElement.element = floatingElementQuery as HTMLElement
			targetElement.position = ToolTipPosition.Floating
		}

		let self = this
		let oldHelperLayer: HTMLElement | null = document.querySelector(
			'.steps-helperLayer'
		)
		let oldReferenceLayer: HTMLElement | null = document.querySelector(
			'.steps-tooltipReferenceLayer'
		)
		let highlightClass = 'steps-helperLayer'
		let nextTooltipButton: HTMLElement | null
		let prevTooltipButton: HTMLElement | null
		let skipTooltipButton: HTMLElement | null
		let customTooltipButton: HTMLElement | null
		let customSkipButton: HTMLElement | null
		let customStartButton: HTMLElement | null
		let completeDetailsButton: HTMLElement | null

		if (isString(targetElement.highlightClass)) {
			highlightClass += ' '.concat(targetElement.highlightClass)
		} //check for options highlight class

		if (isString(this.options.highlightClass)) {
			highlightClass += ' '.concat(this.options.highlightClass)
		}

		if (!isNull(oldHelperLayer) && !isNull(oldReferenceLayer)) {
			let oldHelperNumberLayer = oldReferenceLayer.querySelector(
				'.steps-helperNumberLayer'
			)
			let oldtooltipLayer: HTMLElement | null = oldReferenceLayer.querySelector(
				'.steps-tooltiptext'
			)
			let oldTooltipTitleLayer = oldReferenceLayer.querySelector(
				'.steps-tooltip-title'
			)
			let oldArrowLayer: HTMLElement | null = oldReferenceLayer.querySelector(
				'.steps-arrow'
			)
			let oldtooltipContainer: HTMLElement | null = oldReferenceLayer.querySelector(
				'.steps-tooltip'
			)

			skipTooltipButton = oldReferenceLayer.querySelector('.steps-skipbutton')
			nextTooltipButton = oldReferenceLayer.querySelector('.steps-nextbutton') //update or reset the helper highlight class
			prevTooltipButton = oldReferenceLayer.querySelector('.steps-showElement')
			customTooltipButton = oldReferenceLayer.querySelector(
				'.step-custom-button'
			)
			customSkipButton = oldReferenceLayer.querySelector(
				'.step-custom-skip-button'
			)
			customStartButton = oldReferenceLayer.querySelector(
				'.step-custom-start-button'
			)
			completeDetailsButton = oldReferenceLayer.querySelector(
				'.step-complete-button'
			)

			oldHelperLayer.className = highlightClass //hide the tooltip
			if (oldtooltipContainer) {
				oldtooltipContainer.style.opacity = '0'
				oldtooltipContainer.style.display = 'none' // if the target element is within a scrollable element
			}

			this.scrollParentToElement(targetElement) // set new position to helper layer

			this.setHelperLayerPosition(oldHelperLayer)
			this.setHelperLayerPosition(oldReferenceLayer) //remove old classes if the element still exist

			this.removeShowElement() //we should wait until the CSS3 transition is competed (it's 0.3 sec) to prevent incorrect `height` and `width` calculation

			if (self.lastShowElementTimer) {
				window.clearTimeout(self.lastShowElementTimer)
			}
			let currentStep = this.currentStep + 1
			const oldStepsLayer = oldReferenceLayer.querySelector('.steps')
			if (!isNil(oldStepsLayer)) {
				if (targetElement.showStepNumbers) {
					oldStepsLayer.innerHTML = `Step ${currentStep}/${this.stepsItems.length}`
				} else {
					oldStepsLayer.innerHTML = ''
				}
			} else {
				if (targetElement.showStepNumbers) {
					let stepsCount = this.createElement('a', { className: 'steps' })
					stepsCount.innerHTML = `Step ${this.currentStep + 1}/${
						this.stepsItems.length
					}`
					const buttonLayer = oldReferenceLayer.querySelector(
						'.steps-tooltipbuttons'
					)
					if (!isNil(buttonLayer)) {
						buttonLayer.insertBefore(stepsCount, buttonLayer.firstChild)
					}
				}
			}

			self.lastShowElementTimer = window.setTimeout(() => {
				// set current step to the label
				if (!isNull(oldHelperNumberLayer) && targetElement.step) {
					const stepNumber = targetElement.step.toString()
					oldHelperNumberLayer.innerHTML = ''
						.concat(stepNumber, ' of ')
						.concat(this.stepsItems.length.toString())
				} // set current tooltip text
				if (oldtooltipLayer && isString(targetElement.description)) {
					oldtooltipLayer.innerHTML = targetElement.description // set current tooltip title
				}

				if (oldTooltipTitleLayer && isString(targetElement.title)) {
					oldTooltipTitleLayer.innerHTML = targetElement.title //set the tooltip position
				}

				if (oldtooltipContainer) {
					oldtooltipContainer.style.display = 'block'
				}

				if (
					oldtooltipContainer &&
					oldArrowLayer &&
					targetElement.element &&
					!isString(targetElement.element)
				)
					this.placeTooltip(
						targetElement.element,
						oldtooltipContainer,
						oldArrowLayer
					)
				if (oldtooltipContainer) {
					oldtooltipContainer.style.opacity = '1' //reset button focus
				}

				if (
					!isUndefined(nextTooltipButton) &&
					!isNull(nextTooltipButton) &&
					/steps-donebutton/gi.test(nextTooltipButton.className)
				) {
					// skip button is now 'done' button
					nextTooltipButton.focus()
				} else if (
					!isUndefined(nextTooltipButton) &&
					!isNull(nextTooltipButton)
				) {
					//still in the tour, focus on next
					nextTooltipButton.focus()
				} // change the scroll of the window, if needed

				if (oldtooltipLayer && targetElement.scrollTo) {
					this.scrollTo(targetElement.scrollTo, targetElement, oldtooltipLayer) //end of new element if-else condition
				}
			}, 350) // end of old element if-else condition
		} else {
			let helperLayer = this.createElement('div', {
				className: highlightClass,
			})
			let referenceLayer = this.createElement('div', {
				className: 'steps-tooltipReferenceLayer',
			})
			let arrowLayer = this.createElement('div', {
				className: 'steps-arrow',
			})
			let tooltipLayer = this.createElement('div', {
				className: 'steps-tooltip',
			})
			let tooltipTextLayer = this.createElement('div', {
				className: 'steps-tooltiptext',
			})
			let tooltipHeaderLayer = this.createElement('div', {
				className: 'steps-tooltip-header',
			})
			let tooltipTitleLayer = this.createElement('h1', {
				className: 'steps-tooltip-title',
			})

			let buttonsLayer = this.createElement('div')
			if (self.options.overlayOpacity) {
				this.setStyle(helperLayer, {
					'box-shadow': '0 0 0 0 rgba(33, 33, 33, 0.8), rgba(33, 33, 33, '.concat(
						self.options.overlayOpacity.toString(),
						') 0 0 0 5000px'
					),
				}) // target is within a scrollable element
			}

			this.scrollParentToElement(targetElement) //set new position to helper layer

			this.setHelperLayerPosition(helperLayer)
			this.setHelperLayerPosition(referenceLayer) //add helper layer to target element

			this.appendChild(this.targetElement, helperLayer, true)
			this.appendChild(this.targetElement, referenceLayer)
			if (targetElement.description) {
				tooltipTextLayer.innerHTML = targetElement.description
			}
			if (targetElement.title) {
				tooltipTitleLayer.innerHTML = targetElement.title
			}
			buttonsLayer.className = 'steps-tooltipbuttons'

			if (this.options.showButtons === false) {
				buttonsLayer.style.display = 'none'
			}

			tooltipHeaderLayer.appendChild(tooltipTitleLayer)
			tooltipLayer.appendChild(tooltipHeaderLayer)
			tooltipLayer.appendChild(tooltipTextLayer)
			// tooltipLayer.appendChild(progressLayer) // add helper layer number

			tooltipLayer.appendChild(arrowLayer)
			referenceLayer.appendChild(tooltipLayer) //next button

			// custom exit button
			customTooltipButton = this.createElement('a', {
				className: 'step-custom-button',
			})
			customTooltipButton.onclick = () => {
				if (this.stepCustomButton !== null && targetElement.step) {
					this.buttonType = CustomButtonType.EXIT
					this.stepCustomButton(targetElement.step)
				}
			}
			customTooltipButton.innerHTML = 'Exit'
			this.setAnchorAsButton(customTooltipButton)
			buttonsLayer.appendChild(customTooltipButton)

			customSkipButton = this.createElement('a', {
				className: 'step-custom-skip-button',
			})
			customSkipButton.onclick = () => {
				if (this.stepCustomButton !== null && targetElement.step) {
					this.buttonType = CustomButtonType.SKIP
					this.stepCustomButton(targetElement.step)
				}
			}
			customSkipButton.innerHTML = 'Skip'
			this.setAnchorAsButton(customSkipButton)
			buttonsLayer.appendChild(customSkipButton)

			customStartButton = this.createElement('a', {
				className: 'step-custom-skip-button',
			})
			customStartButton.onclick = () => {
				if (this.stepCustomButton !== null && targetElement.step) {
					this.buttonType = CustomButtonType.START
					this.stepCustomButton(targetElement.step)
				}
			}
			customStartButton.innerHTML = 'Start'
			this.setAnchorAsButton(customStartButton)
			buttonsLayer.appendChild(customStartButton)
			// custom detailscompleted button
			completeDetailsButton = this.createElement('a', {
				className: 'step-complete-button',
			})
			completeDetailsButton.onclick = () => {
				if (this.stepCustomButton !== null && targetElement.step) {
					this.buttonType = CustomButtonType.COMPLETE
					this.stepCustomButton(targetElement.step)
				}
			}
			completeDetailsButton.innerHTML = 'Complete the Details'
			this.setAnchorAsButton(completeDetailsButton)

			buttonsLayer.appendChild(completeDetailsButton)
			tooltipLayer.appendChild(buttonsLayer) //set proper position

			nextTooltipButton = this.createElement('a')

			if (!isNil(nextTooltipButton) && this.options.nextLabel) {
				nextTooltipButton.onclick = () => {
					if (self.stepsItems.length - 1 !== self.currentStep) {
						this.nextStep()
					} else if (
						nextTooltipButton &&
						/steps-donebutton/gi.test(nextTooltipButton.className)
					) {
						if (isFunction(self.stepsComplete)) {
							self.stepsComplete()
						}

						this.exitSteps(self.targetElement)
					}
				}

				this.setAnchorAsButton(nextTooltipButton)
				nextTooltipButton.innerHTML = this.options.nextLabel //previous button
			}

			prevTooltipButton = this.createElement('a')

			prevTooltipButton.onclick = () => {
				if (self.currentStep !== 0) {
					this.previousStep()
				}
			}
			if (targetElement.showStepNumbers) {
				let stepsCount = this.createElement('a', { className: 'steps' })
				stepsCount.innerHTML = `Step ${this.currentStep + 1}/${
					this.stepsItems.length
				}`
				buttonsLayer.appendChild(stepsCount)
			}
			if (this.options.prevLabel) {
				prevTooltipButton.innerHTML = this.options.prevLabel
			}

			//skip button
			skipTooltipButton = this.createElement('a', {
				className: 'steps-skipbutton',
			})
			this.setAnchorAsButton(skipTooltipButton)
			if (this.options.skipLabel) {
				skipTooltipButton.innerHTML = this.options.skipLabel
			}

			skipTooltipButton.onclick = () => {
				if (
					self.stepsItems.length - 1 === self.currentStep &&
					isFunction(self.stepsComplete)
				) {
					self.stepsComplete()
				}

				if (isFunction(self.stepsSkipCallback)) {
					self.stepsSkipCallback()
				}

				this.exitSteps(self.targetElement)
			}

			tooltipHeaderLayer.appendChild(skipTooltipButton) //in order to prevent displaying previous button always

			if (this.stepsItems.length > 1) {
				buttonsLayer.appendChild(prevTooltipButton)
			} // we always need the next button because this
			// button changes to 'Done' in the last step of the tour

			buttonsLayer.appendChild(nextTooltipButton)
			tooltipLayer.appendChild(buttonsLayer) //set proper position
			if (targetElement.element && !isString(targetElement.element))
				this.placeTooltip(targetElement.element, tooltipLayer, arrowLayer) // change the scroll of the window, if needed
			if (targetElement.scrollTo) {
				this.scrollTo(targetElement.scrollTo, targetElement, tooltipLayer)
			}
			//end of new element if-else condition
		} // removing previous disable interaction layer

		this.updateOverlay()

		let disableInteractionLayer = self.targetElement.querySelector(
			'.steps-disableInteraction'
		)

		if (!isNil(disableInteractionLayer) && disableInteractionLayer.parentNode) {
			disableInteractionLayer.parentNode.removeChild(disableInteractionLayer)
		} //disable interaction

		if (targetElement.disableInteraction) {
			this.disableInteraction()
		} // when it's the first step of tour

		if (
			this.currentStep === 0 &&
			this.stepsItems.length > 1 &&
			this.options.nextLabel
		) {
			if (
				!isUndefined(nextTooltipButton) &&
				!isNull(nextTooltipButton) &&
				this.options.buttonClass
			) {
				nextTooltipButton.className = ''.concat(
					this.options.buttonClass,
					' steps-nextbutton'
				)
				nextTooltipButton.innerHTML = this.options.nextLabel
			}

			if (this.options.hidePrev === true) {
				if (
					!isUndefined(prevTooltipButton) &&
					!isNull(prevTooltipButton) &&
					this.options.buttonClass
				) {
					prevTooltipButton.className = ''.concat(
						this.options.buttonClass,
						' steps-showElement steps-hidden'
					)
				}

				if (!isUndefined(nextTooltipButton) && !isNull(nextTooltipButton)) {
					this.addClass(nextTooltipButton, 'steps-fullbutton')
				}
			} else {
				if (
					!isUndefined(prevTooltipButton) &&
					!isNull(prevTooltipButton) &&
					this.options.buttonClass
				) {
					prevTooltipButton.className = ''.concat(
						this.options.buttonClass,
						' steps-showElement steps-fullbutton'
					)
				}
			}
			if (this.currentStep === 0) {
				if (
					!isUndefined(prevTooltipButton) &&
					!isNull(prevTooltipButton) &&
					this.options.buttonClass
				) {
					prevTooltipButton.className = ''.concat(
						this.options.buttonClass,
						' steps-showElement steps-hidden'
					)
				}
			}
		} else if (
			this.stepsItems.length - 1 === this.currentStep ||
			this.stepsItems.length === 1
		) {
			// last step of tour
			if (
				!isUndefined(prevTooltipButton) &&
				!isNull(prevTooltipButton) &&
				this.options.buttonClass
			) {
				prevTooltipButton.className = ''.concat(
					this.options.buttonClass,
					' steps-showElement'
				)
			}
			if (this.options.hideNext === true) {
				if (
					!isUndefined(nextTooltipButton) &&
					!isNull(nextTooltipButton) &&
					this.options.buttonClass
				) {
					nextTooltipButton.className = ''.concat(
						this.options.buttonClass,
						' steps-nextbutton steps-hidden'
					)
				}

				if (!isUndefined(prevTooltipButton) && !isNull(prevTooltipButton)) {
					this.addClass(prevTooltipButton, 'steps-fullbutton')
				}
			} else {
				if (
					!isUndefined(nextTooltipButton) &&
					!isNull(nextTooltipButton) &&
					this.options.buttonClass
				) {
					if (this.options.nextToDone === true && this.options.doneLabel) {
						nextTooltipButton.innerHTML = this.options.doneLabel
						nextTooltipButton.className = ''.concat(
							this.options.buttonClass,
							' steps-nextbutton steps-donebutton'
						)
					} else {
						nextTooltipButton.className = ''.concat(
							this.options.buttonClass,
							' steps-nextbutton steps-fullbutton'
						)
					}
				}
			}
		} else {
			if (
				!isUndefined(prevTooltipButton) &&
				!isNull(prevTooltipButton) &&
				this.options.buttonClass
			) {
				prevTooltipButton.className = ''.concat(
					this.options.buttonClass,
					' steps-showElement'
				)
			}
			if (
				!isUndefined(nextTooltipButton) &&
				!isNull(nextTooltipButton) &&
				this.options.buttonClass &&
				this.options.nextLabel
			) {
				nextTooltipButton.className = ''.concat(
					this.options.buttonClass,
					' steps-nextbutton'
				)
				nextTooltipButton.innerHTML = this.options.nextLabel
			}
		}
		if (targetElement.hideNextButton) {
			if (
				!isUndefined(nextTooltipButton) &&
				!isNull(nextTooltipButton) &&
				this.options.buttonClass
			) {
				nextTooltipButton.className = ''.concat(
					' steps-nextbutton ',
					'steps-hidden'
				)
			}
		}

		if (!targetElement.addNewButton) {
			if (
				!isUndefined(customTooltipButton) &&
				!isNull(customTooltipButton) &&
				this.options.buttonClass
			) {
				customTooltipButton.className = ''.concat(
					' step-custom-button ',
					'steps-hidden'
				)
			}
		} else {
			if (
				!isUndefined(customTooltipButton) &&
				!isNull(customTooltipButton) &&
				this.options.buttonClass
			) {
				customTooltipButton.className = 'step-custom-button'
			}
		}
		if (!targetElement.addSkipButton) {
			if (
				!isUndefined(customSkipButton) &&
				!isNull(customSkipButton) &&
				this.options.buttonClass
			) {
				customSkipButton.className = ''.concat(
					' step-custom-skip-button ',
					'steps-hidden'
				)
			}
		} else {
			if (
				!isUndefined(customSkipButton) &&
				!isNull(customSkipButton) &&
				this.options.buttonClass
			) {
				customSkipButton.className = 'step-custom-skip-button'
			}
		}
		if (!targetElement.addStartButton) {
			if (
				!isUndefined(customStartButton) &&
				!isNull(customStartButton) &&
				this.options.buttonClass
			) {
				customStartButton.className = ''.concat(
					' step-custom-start-button ',
					'steps-hidden'
				)
			}
		} else {
			if (
				!isUndefined(customStartButton) &&
				!isNull(customStartButton) &&
				this.options.buttonClass
			) {
				customStartButton.className = 'step-custom-start-button'
			}
		}
		if (!targetElement.addCompleteDetailsButton) {
			if (
				!isUndefined(completeDetailsButton) &&
				!isNull(completeDetailsButton) &&
				this.options.buttonClass
			) {
				completeDetailsButton.className = ''.concat(
					' step-complete-button ',
					'steps-hidden'
				)
			}
		} else {
			if (
				!isUndefined(completeDetailsButton) &&
				!isNull(completeDetailsButton) &&
				this.options.buttonClass
			) {
				completeDetailsButton.className = 'step-complete-button'
			}
		}

		if (targetElement.hidePrevButton === true) {
			if (
				!isUndefined(prevTooltipButton) &&
				!isNull(prevTooltipButton) &&
				this.options.buttonClass
			) {
				prevTooltipButton.className = ''.concat(
					this.options.buttonClass,
					' steps-showElement steps-hidden'
				)
			}
		}

		if (
			!isUndefined(nextTooltipButton) &&
			!isNull(nextTooltipButton) &&
			this.options.buttonClass
		) {
			nextTooltipButton.setAttribute('role', 'button')
		}

		if (
			!isUndefined(prevTooltipButton) &&
			!isNull(prevTooltipButton) &&
			this.options.buttonClass
		) {
			prevTooltipButton.setAttribute('role', 'button')
		}

		if (!isUndefined(skipTooltipButton) && !isNull(skipTooltipButton)) {
			skipTooltipButton.setAttribute('role', 'button')
		} //Set focus on 'next' button, so that hitting Enter always moves you onto the next step

		if (
			!isUndefined(nextTooltipButton) &&
			!isNull(nextTooltipButton) &&
			this.options.buttonClass
		) {
			nextTooltipButton.focus()
		}

		this.setShowElement(targetElement)

		if (
			!isUndefined(this.stepsAfterChangeCallback) &&
			isFunction(this.stepsAfterChangeCallback) &&
			targetElement.step
		) {
			this.stepsAfterChangeCallback(targetElement.step)
		}
	}

	nextStep = (currentStepNumber?: undefined | number) => {
		this.direction = 'forward'

		if (!isUndefined(currentStepNumber)) {
			this.currentStep = currentStepNumber - 1
		} else {
			++this.currentStep
		}

		let nextStep = this.stepsItems[this.currentStep]
		let continueStep = true

		if (isFunction(this.stepsBeforeChange) && nextStep && nextStep.step) {
			continueStep = this.stepsBeforeChange(nextStep.step)
		} // if `onbeforechange` returned `false`, stop displaying the element

		if (continueStep === false) {
			--this.currentStep
			return false
		}

		if (this.stepsItems.length <= this.currentStep) {
			//end of the step
			//check if any callback is defined
			if (isFunction(this.stepsComplete)) {
				this.stepsComplete()
			}
			this.exitSteps(this.targetElement)
			return
		}
		this.showElement(nextStep)
	}

	updateStep = (currentStepNumber: number) => {
		this.updateStepsInfo()
		if (currentStepNumber === this.currentStep) return
		if (isUndefined(this.currentStep)) {
			this.currentStep = 0
		} else {
			this.currentStep = currentStepNumber
		}

		let nextStep = this.stepsItems[currentStepNumber]

		if (this.stepsItems.length <= this.currentStep) {
			//end of the step
			//check if any callback is defined
			this.exitSteps(this.targetElement)
			return
		}
		this.showElement(nextStep)
	}

	updateStepsInfo = () => {
		let stepsItems: IsyStepItemProps[] = []
		if (this.options.steps) {
			this.options.steps.forEach((step) => {
				let currentItem: IsyStepItemProps = cloneDeep(step)
				currentItem.step = stepsItems.length + 1
				currentItem.title = currentItem.title || ''
				if (isString(currentItem.element))
					currentItem.dummyElement = currentItem.element
				if (isUndefined(currentItem.element) || isNull(currentItem.element)) {
					currentItem.element = step.element
					currentItem.position = step.position
				}
				currentItem.scrollTo = currentItem.scrollTo || this.options.scrollTo
				if (isUndefined(currentItem.disableInteraction)) {
					currentItem.disableInteraction = this.options.disableInteraction
				}
				if (!isNull(currentItem.element)) {
					stepsItems.push(currentItem)
				}
			})

			let tempStepItems = []

			for (let z = 0; z < stepsItems.length; z++) {
				if (stepsItems[z]) {
					// copy non-falsy values to the end of the array
					tempStepItems.push(stepsItems[z])
				}
			}

			stepsItems = tempStepItems //Ok, sort all items with given steps

			stepsItems.sort() //set it to the steps object

			this.stepsItems = stepsItems
		}
	}

	stepForElement = (targetElm: HTMLElement) => {
		this.updateStepsInfo()
		if (this.addOverlayLayer(targetElm)) {
			//for window resize
			this.domObj.on(window, 'resize', true)
		}

		return false
	}

	start = () => {
		this.stepForElement(this.targetElement)
		return this
	}

	goToStepNumber = (stepNumber: number) => {
		if (!isUndefined(this.stepsItems)) {
			this.nextStep(stepNumber)
		}
	}

	onExit = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepsExit = providedCallback
		} else {
			throw new Error('Provided callback for onexit was not a function.')
		}

		return this
	}

	onCustomButtonClick = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepCustomButton = providedCallback
		} else {
			throw new Error('Provided callback for onexit was not a function.')
		}

		return this
	}

	onBeforeExit = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepsBeforeExit = providedCallback
		} else {
			throw new Error('Provided callback for onbeforeexit was not a function.')
		}

		return this
	}

	onBeforeChange = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepsBeforeChange = providedCallback
		} else {
			throw new Error('Provided callback for onbeforechange was not a function')
		}

		return this
	}

	onAfterChange = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepsAfterChange = providedCallback
		} else {
			throw new Error('Provided callback for onafterchange was not a function')
		}

		return this
	}

	onChange = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepsChange = providedCallback
		} else {
			throw new Error('Provided callback for onafterchange was not a function')
		}

		return this
	}

	onComplete = (providedCallback: () => void) => {
		if (isFunction(providedCallback)) {
			this.stepsComplete = providedCallback
		} else {
			throw new Error('Provided callback for onafterchange was not a function')
		}

		return this
	}
}

export function isySteps() {
	return new IsySteps(document.body)
}

export class DOMEvent {
	stepReference: IsySteps
	constructor(stepsObj: IsySteps) {
		this.stepReference = stepsObj
	}

	stamp = (...obj: any) => {
		let keys: any = {}
		let key = obj.length > 1 && obj[1] !== undefined ? obj[1] : 'steps-stamp'
		// each group increments from 0
		keys[key] = keys[key] || 0 // stamp only once per object

		if (obj[key] === undefined) {
			// increment key for each new object
			obj[key] = keys[key]++
		}
		return obj[key]
	}

	id = (_: any, type: string, context: IsySteps) => {
		let listener = this.stepReference.onResize
		return (
			type +
			this.stamp(listener) +
			(context ? '_'.concat(this.stamp(context)) : '')
		)
	}

	on = (obj: any, type: string, useCapture: boolean) => {
		let events_key = 'steps_event'
		let listener = this.stepReference.onResize

		let id = this.id(obj, type, this.stepReference)
		let handler = function handler() {
			return listener()
		}

		if ('addEventListener' in obj) {
			obj.addEventListener(type, handler, useCapture)
		} else if ('attachEvent' in obj) {
			obj.attachEvent('on'.concat(type), handler)
		}
		obj[events_key] = obj[events_key] || {}
		obj[events_key][id] = handler
	}

	off = (obj: any, type: string, useCapture: boolean) => {
		let events_key = 'steps_event'
		let id = this.id(obj, type, this.stepReference)
		let handler = obj[events_key] && obj[events_key][id]
		if (!handler) {
			return
		}

		if ('removeEventListener' in obj) {
			obj.removeEventListener(type, handler, useCapture)
		} else if ('detachEvent' in obj) {
			obj.detachEvent('on'.concat(type), handler)
		}
		obj[events_key][id] = null
	}
}
