import { RootState } from '../../base.types'
import { NotificationPopupState } from './notificationPopup.types'

export const sliceName = 'notificationPopup'

export const selectSlice = (state: RootState): NotificationPopupState =>
	state[sliceName]

export const selectNotification = (state: RootState): NotificationPopupState =>
	selectSlice(state)
