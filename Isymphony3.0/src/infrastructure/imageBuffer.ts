import { v4 } from 'uuid'

export class ImageBuffer {
	private static _instance: ImageBuffer
	private imageBufferObject: { [key: string]: File } = {}

	public static get Instance() {
		return this._instance || (this._instance = new this())
	}

	public storeImage = (file: File): string => {
		const id = v4()
		this.imageBufferObject[id] = file
		return id
	}

	public retrieveImage = (id: string): File | null => {
		return this.imageBufferObject[id] || null
	}
}
