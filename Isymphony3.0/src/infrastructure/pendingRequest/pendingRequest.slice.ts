import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import buildCacheKey from '../buildCacheKey'
import { sliceName } from './pendingRequest.selectors'
import { PendingRequestState } from './pendingRequest.types'

const initialState: PendingRequestState = {}

const slice = createSlice<
	PendingRequestState,
	SliceCaseReducers<PendingRequestState>,
	string
>({
	name: sliceName,
	initialState,
	reducers: {
		addPendingRequest(state, action) {
			state[buildCacheKey(action.payload)] = {
				turnSpinnerOff: false,
			}
		},
		deletePendingRequest(state, action) {
			delete state[buildCacheKey(action.payload)]
		},
		setBusySpinner(state, action) {
			const { turnSpinnerOff } = action.payload

			state[buildCacheKey(action.payload)].turnSpinnerOff = turnSpinnerOff
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
