interface ConfigDataProps {
	backendEndPoint: string
	version: string
}

export class Configurator {
	private static _instance: Configurator
	private configData: ConfigDataProps = {} as any

	public static get Instance() {
		return this._instance || (this._instance = new this())
	}

	public fetch(): Promise<boolean> {
		return fetch('/manifest.json').then((response) => {
			return response
				.json()
				.then((data) => {
					this.configData = data
					return true
				})
				.catch(() => {
					return false
				})
		})
	}

	public getBackendEndPoint(): string {
		return this.configData.backendEndPoint
	}

	public getVersion(): string {
		return this.configData.version
	}
}
