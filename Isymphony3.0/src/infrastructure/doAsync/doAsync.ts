import http from '../http'
import { fileUploadService } from '../http/http'
import { addRequestToCache, tryToFindRequestInCache } from '../httpCache'
import { REDUX_CACHE_HIT_RECEIVED_ASYNC } from './doAsync.actionTypes'
import {
	buildHeaders,
	cleanUpPendingRequests,
	handleError,
	logError,
	requestIsAlreadyPending,
	validateInput,
} from './doAsyncLogic'
import {
	incrementBusyIndicator,
	decrementBusyIndicator,
} from '../../widgets/IsyBusyIndicator'
import { actions } from '../notificationPopup'
import { Dispatch } from '@reduxjs/toolkit'

export interface DoAsyncProps {
	url: string
	httpMethod?: string
	errorMessage?: string
	httpConfig?: RequestInit
	onError?: (error: any) => void
	successMessage?: string
	noBusySpinner?: boolean
	propagateAPIError?: boolean
	busyIndicatorName?: string
	useCaching?: boolean
	stubSuccess?: any
	stubError?: any
	dispatch: Dispatch<any>
	getState: () => any
	rejectWithValue: (error: string) => void
}

export const cacheHit = (
	url: string,
	method: string,
	noBusySpinner?: boolean
) => ({
	type: REDUX_CACHE_HIT_RECEIVED_ASYNC,
	payload: {
		url,
		method,
		noBusySpinner,
	},
})

const doAsync = (options: DoAsyncProps) => {
	const {
		url,
		httpMethod = 'get',
		errorMessage = '',
		httpConfig,
		onError,
		successMessage,
		noBusySpinner,
		busyIndicatorName,
		useCaching = false,
		stubSuccess,
		stubError,
		dispatch,
		getState,
		rejectWithValue,
		propagateAPIError = false,
	} = options
	if (!getState || typeof getState !== 'function') {
		throw new Error(
			'getState is required and must have a getState method defined on it'
		)
	}

	if (!dispatch || typeof dispatch !== 'function') {
		throw new Error('dispatch is required and must be a function')
	}

	try {
		validateInput(url, httpMethod)

		if (
			requestIsAlreadyPending({
				noBusySpinner,
				url,
				httpMethod,
				httpConfig,
				busyIndicatorName,
				dispatch,
				getState,
			})
		) {
			return rejectWithValue('Request is already pending.')
		}

		if (useCaching) {
			if (
				tryToFindRequestInCache(
					getState(),
					url,
					httpMethod,
					httpConfig && httpConfig.body
				)
			) {
				dispatch(cacheHit(url, httpMethod, noBusySpinner))
				cleanUpPendingRequests({
					url,
					httpMethod,
					busyIndicatorName,
					dispatch,
					getState,
				})
				return rejectWithValue('Request found in cache.')
			}

			const requestConfig: any = {}

			if (
				httpConfig &&
				httpConfig.body &&
				(httpMethod.toLowerCase() === 'post' ||
					httpMethod.toLowerCase() === 'put')
			) {
				requestConfig.body = httpConfig.body
			}

			dispatch(addRequestToCache({ url, httpMethod, config: requestConfig }))
		}

		if (!noBusySpinner) {
			dispatch(incrementBusyIndicator(busyIndicatorName))
		}

		const httpConfigUpdated = {
			...httpConfig,
			...buildHeaders(url, httpConfig),
		}

		return (http as any)
			[httpMethod](url, httpConfigUpdated, { stubSuccess, stubError })
			.then((body: any) => {
				if (successMessage) {
					dispatch(actions.notifySuccess(successMessage))
				}

				return Promise.resolve(body)
			})
			.catch((exception: Error) => {
				if (!propagateAPIError) {
					handleError(
						exception,
						onError,
						dispatch,
						httpMethod,
						url,
						httpConfigUpdated,
						errorMessage
					)
				} else {
					return Promise.resolve(exception)
				}
			})
			.then((response: any) => {
				cleanUpPendingRequests({
					url,
					httpMethod,
					busyIndicatorName,
					dispatch,
					getState,
				})
				if (!noBusySpinner) {
					dispatch(decrementBusyIndicator(busyIndicatorName))
				}
				return response
			})
	} catch (exception) {
		logError(dispatch, httpMethod, url, httpConfig, {
			exception,
			errorMessage: null,
		})
		throw exception
	}
}

export default doAsync

export const doAsyncUpload = (url: string, fileData: File) => {
	return fileUploadService(url, fileData).then((body) => {
		return Promise.resolve(body)
	})
}
