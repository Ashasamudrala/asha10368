const buildCacheKey = ({
	url,
	httpMethod,
}: {
	url: string
	httpMethod: string
}) => `${httpMethod}|${url}`

export default buildCacheKey
