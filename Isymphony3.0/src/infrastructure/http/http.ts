import { isFunction, isNil } from 'lodash'
import { Configurator } from '../configFileLoader'

const ASYNC_DELAY = 2000

export interface HttpOptions {
	stubSuccess: () => any
	stubError: () => any
}

function get(url: string, config: Partial<RequestInit>, options: HttpOptions) {
	return doFetch(url, config, options)
}

function post(url: string, config: Partial<RequestInit>, options: HttpOptions) {
	config = {
		...config,
		method: 'POST',
	}

	return doFetch(url, config, options)
}

function put(url: string, config: Partial<RequestInit>, options: HttpOptions) {
	config = {
		...config,
		method: 'PUT',
	}

	return doFetch(url, config, options)
}

function patch(
	url: string,
	config: Partial<RequestInit>,
	options: HttpOptions
) {
	config = {
		...config,
		method: 'PATCH',
	}

	return doFetch(url, config, options)
}

function callDelete(
	url: string,
	config: Partial<RequestInit>,
	options: HttpOptions
) {
	config = {
		...config,
		method: 'DELETE',
	}

	return doFetch(url, config, options)
}

function doFetch(
	url: string,
	config: Partial<RequestInit>,
	options: HttpOptions
) {
	if (!url) {
		throw new Error('You must specify a url')
	}

	if (process.env.NODE_ENV !== 'test') {
		if (isFunction(options.stubSuccess)) {
			return new Promise((resolve) =>
				setTimeout(() => {
					console.warn(`Stubbed service call made to url: ${url}`)
					resolve(options.stubSuccess)
				}, ASYNC_DELAY)
			)
		}

		if (isFunction(options.stubError)) {
			return new Promise((_, reject) =>
				setTimeout(() => {
					console.warn(`Stubbed service error was returned from url: ${url}`)
					reject(options.stubError)
				}, ASYNC_DELAY)
			)
		}
	}
	return fetch(buildUrl(url), addJwtToken(config)).then((response) => {
		if (response.headers) {
			const authHeader = response.headers.get('Authorization')

			setJwtTokenFromHeaderResponse(authHeader)
			// updateSessionToken(parseJwtTokenFromHeader(authHeader));
		}
		let contentType: string = ''
		response.headers.forEach((value, key) => {
			if (key === 'content-type') {
				contentType = value
			}
		})
		if (response.ok) {
			if (contentType.includes('stream')) {
				return response.blob()
			}
			if (response.statusText === 'No Content') {
				return {}
			}
			return response.json()
		}

		const unauthorized = 401
		if (response.status === unauthorized) {
			// All else failed so redirect user ot FMS to reauthenticate
			localStorage.removeItem('jwtToken')
			// response.json().then(() => redirectToSignOut());
		}
		return response.json().then((json) => {
			throw json
		})
	})
}

function buildUrl(url: string) {
	const endPoint = Configurator.Instance.getBackendEndPoint()
	return endPoint ? `${endPoint}/${url}` : url
}

function addJwtToken(config: Partial<RequestInit>): RequestInit {
	const jwtToken = sessionStorage.getItem('accessToken')
	if (!jwtToken || !config) {
		return config
	}

	const authorization = `Bearer ${jwtToken}`
	return {
		...config,
		headers: {
			...config.headers,
			Authorization: authorization,
		},
	}
}

function setJwtTokenFromHeaderResponse(authorizationHeader: string | null) {
	const jwtToken = parseJwtTokenFromHeader(authorizationHeader)
	if (jwtToken) {
		localStorage.setItem('jwtToken', jwtToken)
	} else {
		localStorage.removeItem('jwtToken')
	}
}

function parseJwtTokenFromHeader(authorizationHeader: string | null) {
	if (!authorizationHeader) {
		return
	}
	const tokens = authorizationHeader.match(/\S+/g)
	return !isNil(tokens) && tokens.length > 1 ? tokens[1] : null
}

export const fileUploadService = (url: string, fileData: File) => {
	const jwtToken = sessionStorage.getItem('accessToken')
	const formData = new FormData()
	formData.append('file', fileData)
	const endPoint = Configurator.Instance.getBackendEndPoint()
	return fetch(endPoint ? `${endPoint}/${url}` : url, {
		method: 'put',
		headers: {
			Authorization: `Bearer ${jwtToken}`,
		},
		body: formData,
	})
}

const methods = {
	get,
	post,
	put,
	patch,
	delete: callDelete,
}

export default methods
