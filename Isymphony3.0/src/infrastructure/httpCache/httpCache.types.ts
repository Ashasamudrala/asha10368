export interface HttpCacheState {
	[key: string]: any
}
