export enum AppsSortBy {
	LAST_MODIFIED = 'lastModifiedTimestamp',
	CREATED_ON = 'creationTimestamp',
	NAME = 'name',
}

export enum OrderBy {
	DESC = 'DESC',
	ASC = 'ASC',
}

export enum AppsFilterBy {
	WEB = 'WEB',
	MOBILE = 'MOBILE',
}

export enum AcceleratorSortBy {
	LAST_MODIFIED = 'lastModifiedTimestamp',
	CREATED_ON = 'creationTimestamp',
	OWNER = 'owner',
	NAME = 'name',
}

export enum AppType {
	WEB = 'Web',
	MOBILE = 'Mobile',
}
export enum DatabaseRelationShipType {
	ONE_TO_ONE = 'OneToOne',
	ONE_TO_MANY = 'OneToMany',
	MANY_TO_ONE = 'ManyToOne',
	MANY_TO_MANY = 'ManyToMany',
}

export enum DatabaseContainmentType {
	COMPOSITION = 'Composition',
	AGGREGATION = 'Aggregation',
}

export enum DatabaseAttributeConstraintDataTypes {
	BOOLEAN = 'Boolean',
	INTEGER = 'Integer',
	STRING = 'String',
}

export enum DatabaseVendorOptionsTypes {
	MYSQL = 'MySQL',
	ORACLE = 'Oracle',
	POSTGRESQL = 'PostgreSQL',
	SQLSERVER = 'SQLServer',
	DB2 = 'DB2',
}

export enum WebServiceRequestMethodsLookUpType {
	GET = 'GET',
	POST = 'POST',
	PUT = 'PUT',
	DELETE = 'DELETE',
	HEAD = 'HEAD',
	PATCH = 'PATCH',
	OPTIONS = 'OPTIONS',
}

export enum WebServiceAuthorizationLookUpType {
	NONE = 'None',
	BEARER = 'Bearer',
	BASIC = 'Basic',
}

export enum WebServiceRequestBodyLookUpType {
	JSON = 'application/json',
	XML = 'application/xml',
	FORM_DATA = 'multipart/form-data',
	URL_ENCODED_DATA = 'application/x-www-form-urlencoded',
	HTML_PLAIN = 'text/html',
	NONE = 'none',
}
