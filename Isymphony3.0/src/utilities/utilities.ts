import { isNil } from 'lodash'

export const getUniqueName = (
	names: string[],
	name: string,
	spacer: string
) => {
	let count = 1
	while (true) {
		if (names.indexOf(name + spacer + count) === -1) {
			return name + spacer + count
		} else {
			count++
		}
	}
}

const hasClass = (element: Element, className: string[]) => {
	const classList = (' ' + element.classList.toString() + ' ').replace(
		/[\n\t]/g,
		' '
	)
	for (let i = 0, iLen = className.length; i < iLen; i++) {
		if (classList.indexOf(' ' + className[i] + ' ') > -1) {
			return true
		}
	}
	return false
}

export const isParentUntil = (
	element: Element,
	className: string[]
): boolean => {
	if (hasClass(element, className)) {
		return true
	}
	if (isNil(element.parentElement)) {
		return false
	}
	return isParentUntil(element.parentElement, className)
}

export const isMac = (): boolean => {
	return (
		!isNil(navigator.platform) &&
		navigator.platform.toLowerCase().indexOf('mac') > -1
	)
}
