import React, { Suspense } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import createStore from './createStore'
import './index.scss'
import App from './App'
import './i18n'
import './infrastructure/LICENSE'

const store = createStore()

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<Suspense fallback='loading'>
				<App />
			</Suspense>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
)
