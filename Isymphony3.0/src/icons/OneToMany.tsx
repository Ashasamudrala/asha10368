import React from 'react'

export type OneToManyIconProps = React.SVGProps<SVGSVGElement>

export function OneToManyIcon(props: OneToManyIconProps) {
	return (
		<svg {...props}>
			<g strokeWidth='1' fill='none' fillRule='evenodd'>
				<path
					d='M9,16.3846154 L25,16.3846154 M25,21 L15.0952381,16.3846154 M25,11 L15.0952381,16.3846154'
					strokeWidth='1.6'
					strokeLinecap='round'
					strokeLinejoin='round'
				/>
			</g>
		</svg>
	)
}
