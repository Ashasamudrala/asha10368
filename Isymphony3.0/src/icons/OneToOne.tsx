import React from 'react'

export type OneToOneIconProps = React.SVGProps<SVGSVGElement>

export function OneToOneIcon(props: OneToOneIconProps) {
	return (
		<svg {...props}>
			<g strokeWidth='1' fill='none' fillRule='evenodd'>
				<line
					x1='11'
					y1='15.5'
					x2='27'
					y2='15.5'
					strokeWidth='1.6'
					strokeLinecap='round'
					strokeLinejoin='round'
				/>
			</g>
		</svg>
	)
}
