import React from 'react'

export type RelationshipIconProps = React.SVGProps<SVGSVGElement>

export function RelationshipIcon(props: RelationshipIconProps) {
	return (
		<svg {...props}>
			<g
				stroke='none'
				strokeWidth='1'
				fill='none'
				fillRule='evenodd'
				opacity='0.600000024'
				strokeLinecap='round'
				strokeLinejoin='round'
			>
				<g stroke='#000000' strokeWidth='1.6'>
					<g>
						<g>
							<path d='M1,8.84615385 L15,8.84615385 M1,13 L9.66666667,8.84615385 M1,4 L9.66666667,8.84615385'></path>
						</g>
					</g>
				</g>
			</g>
		</svg>
	)
}
