import React from 'react'

export type NotConfigIconProps = React.SVGProps<SVGSVGElement>

export function NotConfigIcon(props: NotConfigIconProps) {
	return (
		<svg
			xmlns='http://www.w3.org/2000/svg'
			height='24'
			viewBox='0 0 24 24'
			width='24'
			{...props}
		>
			<g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'>
				<polygon id='Path' points='0 0 24 0 24 24 0 24' />
				<path
					d='M12,4 C7.584,4 4,7.584 4,12 C4,16.416 7.584,20 12,20 C16.416,20 20,16.416 20,12 C20,7.584 16.416,4 12,4 Z'
					fill='#FFFFFF'
				/>
				<path
					d='M12,4 C7.584,4 4,7.584 4,12 C4,16.416 7.584,20 12,20 C16.416,20 20,16.416 20,12 C20,7.584 16.416,4 12,4 Z M12.8,16 L11.2,16 L11.2,14.4 L12.8,14.4 L12.8,16 Z M12.8,12.8 L11.2,12.8 L11.2,8 L12.8,8 L12.8,12.8 Z'
					fill='#EE7B09'
					fill-rule='nonzero'
				/>
			</g>
		</svg>
	)
}
