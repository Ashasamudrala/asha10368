import React from 'react'
import { SvgIcon, SvgIconProps } from '@material-ui/core'

/**
 * writtin wropper for  SvgIcon.
 */
export type HelpIconProps = SvgIconProps

export function HelpIcon(props: HelpIconProps) {
	return (
		<SvgIcon {...props}>
			<path
				d='M11.2,16.8 L12.8,16.8 L12.8,15.2 L11.2,15.2 L11.2,16.8 Z M12,7.2 C10.232,7.2 8.8,8.632 8.8,10.4 L10.4,10.4 C10.4,9.52 11.12,8.8 12,8.8 C12.88,8.8 13.6,9.52 13.6,10.4 C13.6,12 11.2,11.8 11.2,14.4 L12.8,14.4 C12.8,12.6 15.2,12.4 15.2,10.4 C15.2,8.632 13.768,7.2 12,7.2 Z'
				id='Shape'
				fill='#FFFFFF'
				fillRule='nonzero'
			></path>
		</SvgIcon>
	)
}
