import React from 'react'

export type ManyToManyIconProps = React.SVGProps<SVGSVGElement>

export function ManyToManyIcon(props: ManyToManyIconProps) {
	return (
		<svg {...props}>
			<g strokeWidth='1' fill='none' fillRule='evenodd'>
				<path
					d='M9,16.3846154 L25,16.3846154 M9,21 L17.3825675,16.3846154 M9,11 L17.3825675,16.3846154 M16.6168522,16.3846154 L24.9994198,21 M16.6168522,16.3846154 L24.9994198,11'
					strokeWidth='1.6'
					strokeLinecap='round'
					strokeLinejoin='round'
				/>
			</g>
		</svg>
	)
}
