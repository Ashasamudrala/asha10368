import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Container } from '@material-ui/core'
import { Routes } from './Routes'
import { Dialogs } from './features/dialogs/Dialogs'
import { Configurator } from './infrastructure/configFileLoader'
import { useDispatch } from 'react-redux'
import './app.scss'
import { showDialog, DialogTypes } from './features/dialogs'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	INVALID_CONFIGURATOR_JSON_ERROR,
} from './utilities/constants'
import { isMac } from './utilities/utilities'
import { ActionCreators } from 'redux-undo'

function App() {
	const themeNames = { dark: `dark-theme`, light: `light-theme` }
	const [themeName] = useState(themeNames.light)
	const [isConfigLoaded, setConfigLoaded] = useState<boolean>(false)
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()

	useEffect(() => {
		window.document.addEventListener('keydown', handleKeyDown)
		return () => {
			window.document.removeEventListener('keydown', handleKeyDown)
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		Configurator.Instance.fetch().then((isLoaded) => {
			if (isLoaded) {
				setConfigLoaded(isLoaded)
			} else {
				dispatch(
					showDialog({
						type: DialogTypes.ALERT,
						data: {
							message: t(INVALID_CONFIGURATOR_JSON_ERROR),
						},
					})
				)
			}
		})
	}, [dispatch]) // eslint-disable-line react-hooks/exhaustive-deps

	const handleKeyDown = (e: KeyboardEvent) => {
		switch (true) {
			case e.key === 'z' && e.metaKey && e.shiftKey && isMac():
			case e.key === 'y' && e.ctrlKey && !isMac(): {
				if (e.target && (e.target as any).tagName === 'BODY') {
					dispatch(ActionCreators.redo())
					// To enable custom functionality by stopping browser default behavior
					e.preventDefault()
				}
				break
			}
			case e.key === 'z' && e.metaKey === true && isMac():
			case e.key === 'z' && e.ctrlKey === true && !isMac(): {
				if (e.target && (e.target as any).tagName === 'BODY') {
					dispatch(ActionCreators.undo())
					// To enable custom functionality by stopping browser default behavior
					e.preventDefault()
				}
				break
			}
		}
	}

	return (
		<div className={themeName}>
			<div className='main-container'>
				{isConfigLoaded && (
					<Router>
						<Container className='page'>
							<Routes />
						</Container>
					</Router>
				)}
				<Dialogs />
			</div>
		</div>
	)
}

export default App
