import { createAsyncThunk } from '@reduxjs/toolkit'
import { RootState } from '../base.types'
import i18n from 'i18next'
import doAsync from '../infrastructure/doAsync'
import {
	GET_APP_BY_ID,
	LOGIN_AUTH,
	LOGIN_REFRESH,
	LOGOUT_AUTH,
	USER_GET_PROFILE,
	GET_ALL_PREFERENCES,
	UPDATE_ALL_PREFERENCES,
} from '../utilities/apiEndpoints'
import { LOGIN_ERROR, PROFILE_LOADING_ERROR } from '../utilities/constants'
import {
	selectGetLoginUserId,
	getUpdatedPreferences,
} from './loginUserDetails.selectors'
import { isNil } from 'lodash'

export const logInUser = createAsyncThunk(
	'login/getAll',
	async (data: { username: string; password: string }, thunkArgs) => {
		let userLoginDetails =
			encodeURIComponent('username') + '=' + data.username + '&'
		userLoginDetails += encodeURIComponent('password') + '=' + data.password
		return await doAsync({
			url: LOGIN_AUTH,
			httpMethod: 'post',
			noBusySpinner: true,
			errorMessage: i18n.t(LOGIN_ERROR),
			httpConfig: {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
				body: userLoginDetails,
			},
			...thunkArgs,
		})
	}
)

export const logInUserOnRefresh = createAsyncThunk(
	'login/refresh',
	async (_: undefined, thunkArgs) => {
		return await doAsync({
			url: LOGIN_REFRESH,
			httpMethod: 'get',
			errorMessage: '',
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const loadUserProfileInfo = createAsyncThunk(
	'users/me',
	async (_: undefined, thunkArgs) => {
		const userId = selectGetLoginUserId(thunkArgs.getState() as RootState)
		if (isNil(userId)) {
			return
		}
		return await doAsync({
			url: USER_GET_PROFILE(userId),
			errorMessage: i18n.t(PROFILE_LOADING_ERROR),
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const logOutUser = createAsyncThunk(
	'auth/logout',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: LOGOUT_AUTH,
			httpMethod: 'post',
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const setApplicationNameById = createAsyncThunk(
	'auth/setApplicationNameById',
	async (appId: string, thunkArgs) => {
		if (isNil(appId)) {
			return
		}
		return await doAsync({
			url: GET_APP_BY_ID(appId),
			noBusySpinner: true,
			...thunkArgs,
		})
	}
)

export const fetchAllPreferences = createAsyncThunk(
	'isyTour/getAllPreferences',
	async (_: undefined, thunkArgs) =>
		await doAsync({
			url: GET_ALL_PREFERENCES,
			noBusySpinner: true,
			...thunkArgs,
		})
)

export const updatePreferences = createAsyncThunk(
	'isyTour/updatePreferences',
	async (_: undefined, thunkArgs) => {
		const preferences = getUpdatedPreferences(thunkArgs.getState() as RootState)
		return await doAsync({
			url: UPDATE_ALL_PREFERENCES,
			httpMethod: 'put',
			noBusySpinner: true,
			httpConfig: {
				body: JSON.stringify({ data: JSON.stringify(preferences) }),
			},
			...thunkArgs,
		})
	}
)
