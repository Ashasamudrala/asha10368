import { createSlice, SliceCaseReducers } from '@reduxjs/toolkit'
import { isNil, set } from 'lodash'
import { Action } from '../common.types'
import * as asyncActions from './loginUserDetails.asyncActions'
import { sliceName } from './loginUserDetails.selectors'
import {
	HelpModuleStatus,
	LoginUserDetailsState,
	PreferenceHelpStatusProps,
} from './loginUserDetails.types'

export const defaultConfig = {
	apps: {
		currentStep: 0,
		status: HelpModuleStatus.INCOMPLETE,
		landingPage: false,
	},
	database: {
		currentStep: 0,
		status: HelpModuleStatus.INCOMPLETE,
		landingPage: false,
	},
	accelerators: {
		currentStep: 0,
		status: HelpModuleStatus.INCOMPLETE,
		landingPage: false,
	},
	webServices: {
		currentStep: 0,
		status: HelpModuleStatus.INCOMPLETE,
		landingPage: false,
	},
	sideNavbar: {
		currentStep: 0,
		status: HelpModuleStatus.INCOMPLETE,
		landingPage: false,
	},
}

const initialState: LoginUserDetailsState = {
	loginInfo: null,
	selectedAppName: null,
	selectedAppId: null,
	preferences: defaultConfig,
}

const slice = createSlice<
	LoginUserDetailsState,
	SliceCaseReducers<LoginUserDetailsState>,
	string
>({
	name: sliceName,
	initialState,

	reducers: {
		clearApp(state) {
			state.selectedAppName = null
			state.selectedAppId = null
		},
		updateDatabasePreferences(
			state: LoginUserDetailsState,
			action: Action<PreferenceHelpStatusProps>
		) {
			let processedData = state.preferences
			processedData = set(processedData, 'database', {
				...processedData.database,
				...action.payload,
			})
			state.preferences = processedData
		},
		updateAppsPreferences(
			state: LoginUserDetailsState,
			action: Action<PreferenceHelpStatusProps>
		) {
			let processedData = state.preferences
			processedData = set(processedData, 'apps', {
				...processedData.apps,
				...action.payload,
			})
			state.preferences = processedData
		},
		updateAcceleratorsPreferences(
			state: LoginUserDetailsState,
			action: Action<PreferenceHelpStatusProps>
		) {
			let processedData = state.preferences
			processedData = set(processedData, 'accelerators', {
				...processedData.accelerators,
				...action.payload,
			})
			state.preferences = processedData
		},
		updateWebServicesPreferences(
			state: LoginUserDetailsState,
			action: Action<PreferenceHelpStatusProps>
		) {
			let processedData = state.preferences
			processedData = set(processedData, 'webServices', {
				...processedData.webServices,
				...action.payload,
			})
			state.preferences = processedData
		},
		updateSideNavbarPreferences(
			state: LoginUserDetailsState,
			action: Action<PreferenceHelpStatusProps>
		) {
			let processedData = state.preferences
			processedData = set(processedData, 'sideNavbar', {
				...processedData.sideNavbar,
				...action.payload,
			})
			state.preferences = processedData
		},
		clearData: () => initialState,
	},
	extraReducers: (builder) => {
		// asynchronous action
		builder.addCase(
			asyncActions.logInUserOnRefresh.fulfilled,
			(state: LoginUserDetailsState, action) => {
				if (action.payload) {
					state.loginInfo = action.payload
				}
			}
		)
		builder.addCase(
			asyncActions.logInUser.fulfilled,
			(state: LoginUserDetailsState, action) => {
				if (action.payload) {
					sessionStorage.setItem('accessToken', action.payload.accessToken)
					state.loginInfo = action.payload
				}
			}
		)
		builder.addCase(
			asyncActions.loadUserProfileInfo.fulfilled,
			(state: LoginUserDetailsState, action) => {
				if (action.payload) {
					if (!isNil(state.loginInfo)) {
						state.loginInfo = {
							...state.loginInfo,
							profilePic: action.payload.profilePic,
						}
					}
				}
			}
		)
		builder.addCase(
			asyncActions.logOutUser.fulfilled,
			(state: LoginUserDetailsState, action) => {
				if (action.payload) {
					sessionStorage.removeItem('accessToken')
					state.loginInfo = null
				}
			}
		)
		builder.addCase(
			asyncActions.setApplicationNameById.fulfilled,
			(state: LoginUserDetailsState, action) => {
				state.selectedAppId = action.meta.arg
				if (action.payload) {
					state.selectedAppName = action.payload.name
				}
			}
		)
		builder.addCase(
			asyncActions.fetchAllPreferences.fulfilled,
			(state: LoginUserDetailsState, action) => {
				if (action.payload) {
					if (JSON.parse(action.payload.data) !== null) {
						state.preferences = JSON.parse(action.payload.data)
					} else {
						state.preferences = {
							...defaultConfig,
							apps: {
								...defaultConfig.apps,
								landingPage: true,
							},
							webServices: {
								...defaultConfig.webServices,
								landingPage: true,
							},
							accelerators: {
								...defaultConfig.accelerators,
								landingPage: true,
							},
							sideNavbar: {
								...defaultConfig.accelerators,
								status: HelpModuleStatus.INPROGRESS,
							},
						}
					}
				}
			}
		)
	},
})

export default slice

export const { name, actions, reducer } = slice
