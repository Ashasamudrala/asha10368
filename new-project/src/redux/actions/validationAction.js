import {FETCH_POSTS,GENERIC_START,GENERIC_FAILED} from './types';
import axios from 'axios';
export const fetchPosts =(getoptions)=>dispatch=>{
        dispatch({ type: GENERIC_START });    
        axios(getoptions).then(result => { 
            dispatch({
                type: FETCH_POSTS,
                payload: result
            });
        }).catch(request => {
            dispatch({
                type: GENERIC_FAILED,
                payload: request.response
            });
        });
}