import {FETCH_POSTS , REDUXDATA} from '../actions/types';
const initialState={
    items:[],
    item:{},
    data:[]
}
export default function(state=initialState,action){
    switch(action.type){
        case FETCH_POSTS:
        return {
            ...state,
            items:action.payload
        }
        case REDUXDATA:

            return {
                ...state,
                data:action.payload
            }

      default:
      return state;
    }
}