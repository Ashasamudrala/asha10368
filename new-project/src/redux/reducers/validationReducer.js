import {FETCH_POSTS,GENERIC_FAILED} from '../actions/types';
const initialState={
    items:[],
    validation:{}
}
export default function(state=initialState,action){
    switch(action.type){
        case FETCH_POSTS:
        action.payload["validationStatus"] = false;
        return {
            ...state,
            items:action.payload,
        }
        case GENERIC_FAILED:
        action.payload["validationStatus"] = true;
        return {
            ...state,
            items:action.payload,
        }
      default:
      return state;
    }
}