import { combineReducers } from 'redux';
import postReducer from './postReducer';
import fetchOrgDetails from './fetchOrgDetails';
import validationList from './validationReducer';

const rootReducer = combineReducers({
    posts: postReducer,
    fetchOrgDetails:fetchOrgDetails,
    validationList:validationList,
    errorList:[]
});
export default rootReducer;