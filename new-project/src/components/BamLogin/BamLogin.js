import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_STR_USERNAME, RA_STR_PASSWORD, RA_STR_NEWPASSWORD, RA_STR_CONFIRMPASSWORD, RA_STR_INITIAL, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import './BamLogin.css';
import Footer from '../Footer';

class BamLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginError: false,
      hideCredentials: true,
      usernameError: false,
      passwordError: false,
      orgName: (JSON.parse(localStorage.getItem('orgName')) !== null) ? JSON.parse(localStorage.getItem('orgName')) : '',
      username: '',
      password: '',
      userAuth: false,
      newPassword: '',
      confirmPassword: '',
      hidePasswords: false,
      passwordExpiry: false,
      newPasswordError: false,
      confirmPasswordError: false,
      secondLogin: false,
      wrongPassword: false,
      data: {},
      errorData: ''
    };
  }
  handleCredentials = (e, msg) => {
    const { target: { value = null } = {} } = e || {};
    if (value === null) return;
    if (msg === RA_STR_USERNAME) {
      this.setState({ username: value });
      if (value.length) {
        this.setState({ usernameError: false });
      } else {
        this.setState({ usernameError: true });
      }
    } else if (msg === RA_STR_PASSWORD) {
      this.setState({ password: value });
      if (value.length) {
        this.setState({ passwordError: false });
      } else {
        this.setState({ passwordError: true });
      }
    } else if (msg === RA_STR_NEWPASSWORD) {
      this.setState({ newPassword: value, errorData: '' });
      if (value.length) {
        this.setState({ newPasswordError: false });
      } else {
        this.setState({ newPasswordError: true });
      }
    }
    else if (msg === RA_STR_CONFIRMPASSWORD) {
      this.setState({ confirmPassword: value, errorData: '' });
      if (value.length) {
        this.setState({ confirmPasswordError: false });
      } else {
        this.setState({ confirmPasswordError: true });
      }
    }
  }
  onRedirectNext = async () => {
    const match = this.props.match;
    const { hidePasswords = false, passwordExpiry = false, newPassword, confirmPassword, username } = this.state;
    if (username && this.state.password && match.path === RA_API_URL['bamloginUrl'] && !hidePasswords && !passwordExpiry) {
      const checkAdmin = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['adminUrl']}`,
        data: {
          "orgName": this.state.orgName,
          "userName": username,
          "userPassword": this.state.password
        }
      };
      const checkAdminStatus = await getService(checkAdmin);
      this.setState({ data: checkAdminStatus.data });
      if (checkAdminStatus && checkAdminStatus.status === 200) {
        localStorage.setItem('profiledata', JSON.stringify(checkAdminStatus.data));
        if (checkAdminStatus.data.orgIdleTimeout) {
          localStorage.setItem('orgIdleTimeout', checkAdminStatus.data.orgIdleTimeout);
        }
        localStorage.setItem('userName', JSON.stringify(checkAdminStatus.data.userProfile.userName));
        if (checkAdminStatus.data.userProfile.authStatus === RA_STR_INITIAL && !this.state.secondLogin) {
          this.setState({ hidePasswords: true, password: '' });
          return;
        } else {
          window.location.href = (process.env.REACT_APP_ROUTER_BASE || '') + RA_API_URL['usersUrl'];
        }
      } else if (checkAdminStatus && checkAdminStatus.status === 403) {
        localStorage.setItem('profiledata', JSON.stringify(checkAdminStatus.data));
        if (!this.state.secondLogin) {
          this.setState({ hidePasswords: true, passwordExpiry: true, password: '' });
        }
      } else {
        if (this.refs) {
          this.refs.userPassword.value = '';
        }
        this.setState({ userAuth: true });
      }
    }
    if ((username && this.state.password && newPassword && confirmPassword && match.path === RA_API_URL['bamloginUrl'] && hidePasswords) || (username && this.state.password && newPassword && confirmPassword && match.path === RA_API_URL['bamloginUrl'] && hidePasswords && passwordExpiry)) {
      const { wrongPassword } = this.state;
      // if (newPassword !== confirmPassword) return this.setState({ wrongPassword: true });
      const updateNewPassword = {
        method: 'PUT',
        url: `${serverUrl}${RA_API_URL['changepasswordUrl']}`,
        data: { newPassword, confirmPassword }
      };

      const updateNewPasswordStatus = await getService(updateNewPassword);
      if (updateNewPasswordStatus.status === 200) {
        //this.props.history.push('/users');
        return this.setState({ hideCredentials: false, hidePasswords: false, passwordExpiry: false, secondLogin: true, password: '' });
        //window.location.reload();
      } else if (updateNewPasswordStatus.status === 400) {
        const { data: { errorList: errorData = "" } = {} } = updateNewPasswordStatus || {};
        this.setState({ errorData });
      }
    }
    if (username === '') {
      this.setState({ usernameError: true });
    }
    if (this.state.password === '') {
      this.setState({ passwordError: true });
    }
    if (this.state.newPassword === '' && hidePasswords) {
      this.setState({ newPasswordError: true });
    }
    if (this.state.confirmPassword === '' && hidePasswords) {
      this.setState({ confirmPasswordError: true });
    }
    return;
  }

  keyPress = (e) => {
    if (e.keyCode === 13 && this.state.username && this.state.password) {
      this.onRedirectNext();
    }
  }

  onPassworClick = () => this.setState({ wrongPassword: false })

  render() {
    const { orgName = '', usernameError, passwordError, userAuth, newPasswordError, confirmPasswordError, wrongPassword } = this.state;
    return (
      <div className="login-parent">
        <div className="eupopup-parent"></div>
        <div className="wrapper">
          <div className="">
            <div className="clearfloat edl-login-box">
              <div id="brand-holder">
                <div>
                  <img src="images/ca-logo-new.png" alt="caLogo" height="50"></img>
                  <p className="ecc-h1">{RA_STR.administrationConsole}</p>
                </div>
              </div>
              <div id="form-holder">
                <div className="form-header" id="signin-header">{RA_STR.signIn}</div>
                <form name="bamForm" method="post" >
                </form>
                <div id="loginbox">
                  {(this.state.hidePasswords && !this.state.passwordExpiry) ?
                    <div className="edl_error_feedback">
                      {RA_STR.pleaseResetPassword}
                    </div>
                    : ''
                  }
                  {(this.state.hidePasswords && this.state.passwordExpiry) ?
                    <div className="edl_error_feedback">
                      {RA_STR.passwordExpiry}
                    </div>
                    : ''
                  }
                  {this.state.secondLogin ?
                    <div className="edl_error_feedback margin-top-8">
                      {RA_STR.successfulLogin}
                    </div> : ''
                  }
                  <div>
                    {this.state.orgName ? this.state.orgName.toUpperCase() : ''}
                    {(this.state.hideCredentials && !this.state.hidePasswords && !this.state.passwordExpiry) ?
                      <div>
                        <div className="body-font">{RA_STR.signInUsername}</div>
                        <div className="edl_login_input">
                          <input name="userName" title={`${!this.state.username.length ? 'Please fill out this field.' : ''}`} className={`loginTextBox ${usernameError ? ' error' : ''}`} autoComplete="off" type="text" onChange={e => this.handleCredentials(e, RA_STR_USERNAME)} />
                          <div className={`${usernameError ? 'error-msg' : 'no-msg'}`}>{RA_STR.usernameErrmsg}</div>
                        </div>
                        <div className="body-font">{RA_STR.signInPassword}</div>
                        <div className="edl_login_input">
                          <input name="userPassword" title={`${!this.state.password.length ? 'Please fill out this field.' : ''}`} ref="userPassword" className={`loginTextBox ${passwordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, RA_STR_PASSWORD)} onKeyDown={this.keyPress} />
                          <div className={`${passwordError ? 'error-msg' : 'no-msg'}`}>{RA_STR.passwordErrmsg}</div>
                        </div>
                        <div className={`${userAuth ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">{RA_STR.authFailed}</div>
                      </div> : ''
                    }
                    {(this.state.hidePasswords || (this.state.hidePasswords && this.state.passwordExpiry)) ?
                      <div>
                        <div>
                          <div className="body-font">{RA_STR.signInUsername}</div>
                          <div className="edl_login_input">
                            <input name="userName" className={`loginTextBox ${usernameError ? ' error' : ''}`} type="text" value={`${this.state.username.toUpperCase()}`} disabled />
                          </div>
                          <div className="body-font">{RA_STR.signInPassword}</div>
                          <div className="edl_login_input">
                            <input name="userPassword" title="Please enter password." ref="userPassword" className={`loginTextBox ${passwordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, RA_STR_PASSWORD)} />
                            <div className={`${passwordError ? 'error-msg' : 'no-msg'}`}>{RA_STR.passwordErrmsg}</div>
                          </div>
                        </div>
                        <div style={{ height: '63.5px' }}>
                          <div className="body-font">{RA_STR.signInNewPassword}</div>
                          <div className="passwordtooltip">
                            <div className="edl_login_input">
                              <input name="newPassword" title="Please enter new password." ref="newPassword" className={`loginTextBox ${newPasswordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, RA_STR_NEWPASSWORD)} onClick={this.onPassworClick} />
                              <div className={`${newPasswordError ? 'error-msg' : 'no-msg'}`}>Please enter new password.</div>
                              <span className="tooltiptext">{RA_STR.LOGIN_PWD_RULES.TITLE}
                                <ul>
                                  {RA_STR.LOGIN_PWD_RULES.LIST.map((list, i) => {
                                    return (
                                      <li key={i}>{list}</li>
                                    )
                                  })}
                                </ul>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div style={{ height: '63.5px' }}>
                          <div className="body-font">{RA_STR.signInConfirmPassword}</div>
                          <div className="passwordtooltip">
                            <div className="edl_login_input">
                              <input name="confirmPassword" title="Please confirm your password." ref="confirmPassword" className={`loginTextBox ${confirmPasswordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, RA_STR_CONFIRMPASSWORD)} onKeyDown={this.keyPress} onClick={this.onPassworClick} />
                              <div className={`${confirmPasswordError ? 'error-msg' : 'no-msg'}`}>Please confirm your password.</div>
                              <span className="tooltiptext">{RA_STR.LOGIN_PWD_RULES.TITLE}
                                <ul>
                                  {RA_STR.LOGIN_PWD_RULES.LIST.map((list, i) => {
                                    return (
                                      <li key={i}>{list}</li>
                                    )
                                  })}
                                </ul>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className={`${this.state.errorData ? 'edl_error_feedback' : 'no-msg'}`}>
                          {(this.state.errorData !== '') ? this.state.errorData.map(errorData => <div className={`${this.state.errorData ? 'edl_error_feedback' : 'no-msg'}`}>{errorData.errorMessage}</div>) : ''}
                        </div>
                      </div> : ''
                    }
                    {(this.state.secondLogin && !this.state.hideCredentials && !this.state.hidePasswords && !this.state.passwordExpiry) ?
                      <div>
                        <div className="body-font">{RA_STR.signInUsername}</div>
                        <div className="edl_login_input">
                          <input name="userName" title="Please enter username." className={`loginTextBox ${usernameError ? ' error' : ''}`} value={`${this.state.username.toUpperCase()}`} autoComplete="off" type="text" onChange={e => this.handleCredentials(e, RA_STR_USERNAME)} />
                          <div className={`${usernameError ? 'error-msg' : 'no-msg'}`}>{RA_STR.usernameErrmsg}</div>
                        </div>
                        <div className="body-font">{RA_STR.signInPassword}</div>
                        <div className="edl_login_input">
                          <input name="userPassword" title="Please enter password." ref="userPassword" className={`loginTextBox ${passwordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, RA_STR_PASSWORD)} onKeyDown={this.keyPress} />
                          <div className={`${passwordError ? 'error-msg' : 'no-msg'}`}>{RA_STR.passwordErrmsg}</div>
                        </div>
                        <div className={`${userAuth ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">{RA_STR.authFailed}</div>
                      </div>
                      : ''
                    }
                    {((this.state.hidePasswords && this.state.wrongPassword) || (this.state.hidePasswords && this.state.passwordExpiry && this.state.wrongPassword)) ?
                      <div className="edl_error_feedback">The values in New Password and Confirm Password do not match.</div>

                      : ''}
                    <input className="button edl_btn_main left_align margin-top-8 user-sign-in" onClick={this.onRedirectNext} type="submit" value="SIGN IN" />

                  </div>
                  <br className="clearfloat" />
                  <br />
                  <div className="eupopup"></div>
                </div>
              </div>
            </div>
            <div className="push"></div>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}
export default BamLogin;
