import React, { Component } from 'react';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_OPTIONDETAILS, RA_STR_SELECTOPTION } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';

class UpdateCustomRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: [],
      roleDescription: '',
      roleName: '',
      parentRole: '',
      selectOptions: [],
      optionDetails: [],
      optionList: [],
      emptyList: [],
      options: [],
      orderId: '',
      errordata: '',
      successdata: '',
      predefinedRoles: [],
      roleBased: '',
      optDetails: []
    }
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleRolesChange = (e) => {
    if (e.target.value === '') {
      this.setState({ roleDescription: '', roleName: '', orderId: '', successdata: '', errordata: '', optionDetails: [], selectOptions: [], parentRole: '', roleBased: '' });
    }
    else {
      let rolesMenu = JSON.parse(e.target.value);
      let index;
      if (this.state.predefinedRoles) {
        for (let i = 0; i < this.state.predefinedRoles.length; i++) {
          if (this.state.predefinedRoles[i].roleName === rolesMenu.parentRole) {
            index = i;
            break;
          }
        }
      }
      this.setState({ [e.target.name]: e.target.value });
      this.getPrivileges(this.state.predefinedRoles[index].listOfPrivileges, RA_STR_OPTIONDETAILS, rolesMenu);
      this.getPrivileges(rolesMenu.turnedOffPrivileges, RA_STR_SELECTOPTION, rolesMenu);
      this.getParentRole(rolesMenu.parentRole);
      this.setState({ roleDescription: rolesMenu.roleDescription, roleName: rolesMenu.displayName, orderId: rolesMenu.orderId, successdata: '', errordata: '', roleBased: rolesMenu });
    }
  }

  getAvaliablePrivileges = () => {
    const { selectOptions } = this.state;
    let item = [];
    if (selectOptions.length !== 0) {
      for (let i = 0; i < Object.keys(selectOptions[0]).length; i++) {
        item = Object.keys(selectOptions[0]);
        for (let y = 0; y < selectOptions[0][item[i]].length; y++) {
          for (let k = 0; k < this.state.optionDetails[0][item[i]].length; k++) {
            if (selectOptions[0][item[i]][y].privilegeId === this.state.optionDetails[0][item[i]][k].privilegeId) {
              this.state.optionDetails[0][item[i]].splice(k, 1);
              break;
            }
          }
        }
      }
    }
    this.setState(this.state.optionDetails);
  }

  getParentRole = (parentRole) => {
    for (let i = 0; i < this.state.predefinedRoles.length; i++) {
      if (this.state.predefinedRoles[i].roleName === parentRole) {
        this.setState({ parentRole: this.state.predefinedRoles[i].displayName });
        break;
      }
    }
  }

  handleLeftChange = (node) => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let selectedValues = CA_Utils.getselectedValues(node);
    let multiselectObject = {};
    if (selectedValues.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handleLeftChange(optionDetails, selectOptions, 'update', node);
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  getKeys = (optionDetails) => {
    Object.keys(optionDetails[0]).filter(function (item) {
      if (optionDetails[0][item].length !== 0) {
        return item;
      }
    });
  }
  handleRightChange = (node) => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let selectedValues = CA_Utils.getselectedValues(node);
    let multiselectObject = {};
    if (selectedValues.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handleRightChange(optionDetails, selectOptions, 'update', node);
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  setSelectedOptionObject = (selectOptions) => {
    let emptyList = JSON.parse(JSON.stringify(this.state.emptyList));
    return CA_Utils.setSelectedOptionObject(selectOptions, emptyList);
  }
  setSelectedLeftObject = (optionDetails) => {
    let { options } = this.state;
    let selectedOptions = CA_Utils.setSelectedLeftObject(optionDetails, options);
    return selectedOptions;
  }
  setSelectedRightObject = (selectOptions) => {
    let { optDetails } = this.state;
    let selectedOptions = CA_Utils.setSelectedRightObject(selectOptions, optDetails);
    return selectedOptions;
  }
  handledoubleRightChange = () => {
    let multiselectObject = {};
    let { optionDetails, options, selectOptions, optDetails } = this.state;
    if (this.state.optionDetails.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handledoubleRightChange(optionDetails, selectOptions, 'update');
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
      this.setState(multiselectObject);
    }
  }
  handledoubleLeftChange = () => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let multiselectObject = {};
    if (this.state.selectOptions.length !== 0) {
      optionDetails = this.setSelectedOptionObject(optionDetails);
      selectOptions = this.setSelectedOptionObject(selectOptions);
      multiselectObject = CA_Utils.handledoubleLeftChange(optionDetails, selectOptions, 'update');
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  getPrivileges = (turnedOffPrivileges, optionValue, rolesMenu) => {
    let multiselectObject = CA_Utils.objectFormation(turnedOffPrivileges);
    if (optionValue === RA_STR_SELECTOPTION) {
      this.setState({ selectOptions: multiselectObject['optionDetails'], optDetails: multiselectObject['data'] }, () => { this.getAvaliablePrivileges() });
    }
    else {
      this.setState({ optionDetails: multiselectObject['optionDetails'], emptyList: multiselectObject['selectOptions'], options: multiselectObject['data'] });
    }
  }
  componentDidMount = async () => {
    const getPredefinedRoles = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getCustomRoles']}`
    };
    const getPredefinedRolesData = await getService(getPredefinedRoles);
    if (getPredefinedRolesData.status === 200) {
      for (let index = 0; index < getPredefinedRolesData.data.roles.length; index++) {
        this.state.roleOptions.push(getPredefinedRolesData.data.roles[index]);
      }
    }
    const getPredefinedRole = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getAllPredefinedRoles']}`
    };
    const getAllPredefinedRoles = await getService(getPredefinedRole);
    if (getAllPredefinedRoles.status === 200) {
      for (let index = 0; index < getAllPredefinedRoles.data.roles.length; index++) {
        this.state.predefinedRoles.push(getAllPredefinedRoles.data.roles[index]);
      }
      this.setState({ predefinedRoles: this.state.predefinedRoles, roleOptions: this.state.roleOptions })
    }
  }

  showErrorList = () => {
    window.scrollTo(0, 0);
  }
  handleUpdateCustomRole = async () => {
    let turnedOffPrivileges = [];
    let parentRoleKey = '';
    let { parentRole } = this.state;
    // Getting Key based on parent Role
    for (let i = 0; i < this.state.predefinedRoles.length; i++) {
      if (this.state.predefinedRoles[i].displayName === this.state.parentRole) {
        parentRoleKey = this.state.predefinedRoles[i].roleName;
        break;
      }
    }
    // Getting Privileges in selectedOptions
    if (this.state.selectOptions.length !== 0) {
      for (let i = 0; i < Object.keys(this.state.selectOptions[0]).length; i++) {
        let item = Object.keys(this.state.selectOptions[0])
        for (let y = 0; y < this.state.selectOptions[0][item[i]].length; y++) {
          let data = this.state.selectOptions[0][item[i]][y];
          turnedOffPrivileges.push({ 'privilegeId': data['privilegeId'] });
        }
      }
    }
    if (!(turnedOffPrivileges.length === 0 && parentRole === '')) {
      const UpdateRole = {
        method: 'PUT',
        url: `${serverUrl}${RA_API_URL['getCustomRoles']}`,
        data: {
          "displayName": this.state.roleName,
          "orderId": this.state.orderId,
          "parentRole": parentRoleKey,
          "roleDescription": this.state.roleDescription,
          "roleName": this.state.roleBased.roleName,
          "turnedOffPrivileges": turnedOffPrivileges
        }
      };

      const updateCustomRoleStatus = await getService(UpdateRole);

      if (updateCustomRoleStatus.status === 200) {
        let roleOptions = [];
        const getPredefinedRoles = {
          method: 'GET',
          url: `${serverUrl}${RA_API_URL['getCustomRoles']}`
        };
        const getPredefinedRolesData = await getService(getPredefinedRoles);
        if (getPredefinedRolesData.status === 200) {
          for (let index = 0; index < getPredefinedRolesData.data.roles.length; index++) {
            roleOptions.push(getPredefinedRolesData.data.roles[index]);
          }
          let roleName = this.state.roleName;
          let flag;
          roleOptions.forEach((data, index) => {
            if (data.displayName === roleName) {
              flag = index;
              return;
            }
          });
          this.setState({
            roleOptions: roleOptions, roleBased: roleOptions[flag]
          });
          this.props.activateSuccessList(true, updateCustomRoleStatus.data);
          this.props.activateErrorList(false, '');
        }

      } else {
        this.props.activateErrorList(true, updateCustomRoleStatus.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
    else {
      this.props.activateErrorList(true, [{ errorMessage: RA_STR.updateCustumrolerrormsg }]);
      this.props.activateSuccessList(false, '');
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  render() {
    const { roleDescription, roleName, parentRole, roleOptions } = this.state;
    return <div className="main">
      <h2 className="title">{RA_STR.updateCustomroletitle}</h2>
      <p className="desc">{RA_STR.updateCustomroledesc}</p>
      <div className="col-sm-8">
        <span className="ecc-h1 row">{RA_STR.customroledetails}</span>
        <div className="form-group dynamic-form-select">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.customrolename} </label>
            <div className="col-sm-8">
              <select
                name='roleBased'
                onChange={this.handleRolesChange}
                value={JSON.stringify(this.state.roleBased)}
                className="form-select form-control">
                <option value=''>--Select--</option>
                {roleOptions.map((opt, index) => {
                  return (
                    <option
                      key={opt.roleName}
                      value={JSON.stringify(opt)}>{opt.roleName}</option>
                  );
                })}
              </select>
            </div>
          </div>
        </div>
        <div className="dynamic-form-input form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.customroledisplayname} </label>
          <div className="col-sm-8 input-group input-group-unstyled">
            <input
              maxLength="512"
              className="form-input form-control"
              autoComplete="off"
              name={'roleName'}
              disabled={roleName !== '' ? false : true}
              type={'text'}
              value={roleName}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="dynamic-form-input form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.customroledescription} </label>
          <div className="col-sm-8 input-group input-group-unstyled">
            <input
              maxLength="512"
              className="form-input form-control"
              autoComplete="off"
              name={'roleDescription'}
              disabled={roleDescription !== '' ? false : true}
              type={'text'}
              value={roleDescription}
              onChange={this.handleChange}
            />
          </div>
        </div>
        <div className="dynamic-form-input form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.customrolebased} </label>
          <div className="col-sm-8 input-group input-group-unstyled">
            <input
              maxLength="512"
              className="form-input form-control"
              autoComplete="off"
              name={'parentRole'}
              disabled={true}
              type={'text'}
              value={parentRole}
              onChange={this.handleChange}
            />
          </div>
        </div>

        <span className="ecc-h1 row">{RA_STR.customroleprivileges}</span>
      </div>

      <SwappingSelectBox selectOptions={this.state.selectOptions}
        optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={true} rightMenuName='Unavailable Privileges' leftMenuName='available Privileges' />
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="UPDATE" onClick={this.handleUpdateCustomRole}></input>
      </div>
    </div>;
  }
}

export default UpdateCustomRole;
