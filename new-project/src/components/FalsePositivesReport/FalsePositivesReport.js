import React, { Component } from "react";
import "./FalsePositivesReport.scss";
import { serverUrl, RA_STR_SELECT, RA_API_URL } from "../../shared/utlities/constants";
import { RA_STR } from "../../shared/utlities/messages";

import Select from '../../shared/components/Select/Select';
import AutoSuggest from "../../shared/components/Autosuggest/Autosuggest";

import { getService } from "../../shared/utlities/RestAPI";
import DateRange from '../../shared/components/DateRange/DateRange';
import DateTimeRange from "../../shared/components/DateTimeRange/DateTimeRange";
import DynamicTable from "../../shared/components/dynamicTable/dynamicTable";
import CA_Utils from '../../shared/utlities/commonUtils';
import { saveAs } from "file-saver";

import C3Chart from 'react-c3js';
import 'c3/c3.css';

class FalsePositivesReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      originalOrganizations: [],
      organizationNames: [],
      defaultOrganizationNme: RA_STR_SELECT,
      defaultChannel: 'All',
      channelList: [],
      selectedChannels: '',
      fromDate: '',
      toDate: '',
      showTable: false,
      showGraph: false,
      fraudList: [],
      columns: [
        { title: 'Rule Name', field: 'matchedRule' },
        { title: 'Advice', field: 'advice' },
        { title: 'Transaction Count', field: 'txnCount' },
        { title: 'Alerted Transactions', field: 'caseCount' },
        { title: 'Total Fraud Amount (USD)', field: 'confirmedFraudAmount' },
        { title: 'Fraud', field: 'confirmedFraud' },
        { title: 'Genuine', field: 'notFraud' },
        { title: 'Undetermined', field: 'undetermined' }
      ]
    };
  }
  componentDidMount() {
    this.getOrganizations();
  }

  getOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };

  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find((element) => { if (element.content === value) return element.key });
      this.setState({ defaultOrganizationNme: orgId.key }, () => console.log(this.state.defaultOrganizationNme))
      this.getChannels(orgId.key);
    }
  }

  getChannels = async (selectedOrg) => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['common']}/${selectedOrg}${RA_API_URL['channels']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      const channelList = [{
        content: 'All Channels',
        key: 'All'
      }];
      response.data.forEach((obj) => {
        channelList.push({
          'content': obj.displayName,
          'key': obj.name
        })
      })
      this.setState({ channelList: channelList });
    }
  }

  displayReport = async () => {
    let allChannels = '';
    if (this.state.defaultChannel === 'All') {
      for (let i = 1; i < this.state.channelList.length; i++) {
        allChannels += this.state.channelList[i].content + ', ';
      }
      this.setState({ selectedChannels: allChannels.slice(0, -2) });
    }
    else {
      this.setState({ selectedChannels: this.state.defaultChannel });
    }
    const getDateItems = this.refs.daterange.getDates();
    this.setState({ fromDate: getDateItems[0].startDate, toDate: getDateItems[0].endDate });
    const payload = {
      dateRange: "dateRange",
      fromDate: getDateItems[0].startDate,
      ichDrillDown: 0,
      month: 0,
      orgName: this.state.defaultOrganizationNme,
      pageNo: 1,
      pageSize: 10,
      queueDrillDown: 0,
      queueId: 0,
      selectedChannel: this.state.defaultChannel,
      timePeriod: "By Date Range",
      toDate: getDateItems[0].endDate,
      year: 0
    }
    const url = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['falsePositive']}`,
      data: payload
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      const fraudStatisticsReport = response.data.uiruleList || [];
      this.setState({ fraudList: fraudStatisticsReport, showTable: true });
      this.props.activateSuccessList(true, response.data);
      this.props.activateErrorList(false, '');
    } else {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }

  export = async () => {
    const payload = {
      dateRange: "dateRange",
      fromDate: this.state.fromDate,
      ichDrillDown: 0,
      month: 0,
      orgName: this.state.defaultOrganizationNme,
      pageNo: 1,
      pageSize: 10,
      queueDrillDown: 0,
      queueId: 0,
      selectedChannel: this.state.defaultChannel,
      timePeriod: "By Date Range",
      toDate: this.state.toDate,
      year: 0
    }

    const url = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['falsePositiveReport']}`,
      data: payload
    };
    const response = await getService(url);
    const blob = new Blob([response.data], { type: 'application/octet-stream' });
    saveAs(blob, 'FalsePositiveReport.csv');
  }

  newReport = async () => {
    this.setState({ showTable: false, showGraph: false });
  }

  render() {
    const { organizationNames, defaultOrganizationNme, showTable, defaultChannel, channelList, selectedChannels, fromDate, toDate, showGraph, columns, fraudList, data, grid } = this.state;
    return (
      <div className="main">
        <h2 className="title">False Positives Report</h2>
        {!showTable ?
          <div>
            <p className="desc"> This report provides data to track how effective each rule is in predicting fraud. Specify the criteria to generate the report.</p>
            <hr />
          </div>
          : <div className="form-group form-submit-button row">
            <div className="col-sm-8">
              <p className='desc'>This report provides data to track how effective each rule is in predicting fraud, based on the GMT time zone.</p>
            </div>
            <div className="col-sm-4">
              <input className="secondary-btn" type="submit" value="EXPORT" onClick={this.export}></input>
              <input className="secondary-btn ml-3" type="submit" value="NEW REPORT" onClick={this.newReport}></input>
            </div>
          </div>}

        {!showTable ?
          <div className="col-md-8">
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}</label>
              <div className="col-sm-8">
                <AutoSuggest orgOptions={organizationNames} getSelectedOrgDetails={this.onOrgSelect} enableAutosuggest={true} defaultOrganizationPlaceholder={defaultOrganizationNme} />
              </div>
            </div>
          </div> : ''}

        {(defaultOrganizationNme !== '--Select--' && !showTable) ?
          <div>
            <div className='col-sm-8'>
              <Select
                name={'channel'}
                title={'Select Channel'}
                required={true}
                selectedOption={defaultChannel}
                // placeholder={'-All Channels-'}
                options={channelList ? channelList : ['']}
                controlFunc={this.onChannelSelect} />
            </div>

            <div className="row">
              <div className="col-sm-3">
                <label className="col-sm-8 col-form-label mt-3"> {RA_STR.timePeriod} </label>
              </div>
              <DateRange ref="daterange"></DateRange>
              {/* <DateTimeRange ref="datetimerangelist" startPrior={''} endPrior={''}></DateTimeRange> */}
            </div>

            <div className="form-group form-submit-button row">
              <input className="secondary-btn" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
            </div>
          </div> : ''}

        {showTable ?
          <div className="display-report">
            <table>
              <tbody>
                <tr className="row">
                  <td className="col-sm-4">{RA_STR.FRAUD_STAT_REPORT.ORGANIZATION_LABEL}</td>
                  <td className="col-sm-8"> {defaultOrganizationNme} </td>
                </tr>
                <tr className="row">
                  <td className="col-sm-4"> {RA_STR.FRAUD_STAT_REPORT.CHANNEL} </td>
                  <td className="col-sm-8"> {selectedChannels} </td>
                </tr>
                <tr className="row">
                  <td className="col-sm-4">{RA_STR.timePeriod}:</td>
                  <td className="col-sm-8"> {RA_STR.FRAUD_STAT_REPORT.DATRE_BY_RANGE} </td>
                </tr>
                <tr className="row">
                  <td className="col-sm-4">{RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL} {fromDate}</td>
                  <td className="col-sm-8">{RA_STR.EXCEPTION_USER_REPORT.TO_LABEL} {toDate}</td>
                </tr>
              </tbody>
            </table>
            <div className="tabele-buttons">
              <DynamicTable columns={columns} data={fraudList} enablePagination={false} selection={false} />
            </div>
          </div>
          : ''}

        {showGraph ? <C3Chart data={data} grid={grid} /> : ''}


      </div>
    );
  }
}

export default FalsePositivesReport;
