import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, serverUrl, LOADING_TEXT, RA_STR_SELECT } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import './SearchCases.scss';
import SearchbyCriteria from './SearchbyCriteria';
import Loader from '../../shared/utlities/loader';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import { saveAs } from 'file-saver';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import CA_Utils from '../../shared/utlities/commonUtils';

class SearchCases extends Component {
  constructor(props) {
    super(props);
    this.state = {
      organizationList: [],
      originalOrganizations: [],
      selectedTab: "",
      startDate: new Date(),
      endDate: new Date(),
      toDate: new Date(),
      fromDate: new Date(),
      SelectOrganization: '',
      caseId: '',
      reportType: '',
      decrypt: true,
      userName: '',
      caseStatus: '',
      caseStatusOptions: [],
      lastCaseOptions: [30, 60, 120, 180],
      caseDataType: 0,
      pageLoaded: false,
      activePageNum: 1,
      pageSize: 10,
      totalCount: 0,
      pageNo: 0,
      currentPageNo: 1,
      caseSummaryData: []
    }
    //onClick={this.showCaseDetail}
    this.caseSummarycolumns = [
      {
        title: 'Case ID', field: 'caseID', render: rowData =>
          <a className="orgName" onClick={() => this.goToInboundPage(rowData)}>{rowData.caseID}</a>
      },
      { title: 'Username', field: 'userName' },
      { title: 'Organization', field: 'orgName' },
      { title: 'Case Status', field: 'caseStatus' },
      { title: 'Advice ID', field: 'adviceID' },
      { title: 'Queue Name', field: 'queueID' },
      { title: 'Matched Rule', field: 'matchedRule' },
      { title: 'Details', field: 'transactionId', render: rowData => <a className="orgName" onClick={() => this.getCaseHistory(rowData)}>Details</a> },
    ];
    this.caseHistorycolumns = [
      { title: 'TimeStamp (GMT)', field: 'timeValue' },
      { title: 'Activity', field: 'activity' },
      { title: 'Workflow', field: 'workFlowValue' },
      { title: 'Administrator ID', field: 'adminName' },
      { title: 'Administrator Organization', field: 'adminOrgName' },
      { title: 'Previous Case Status', field: 'prevcaseStatusValue' },
      { title: 'Case Status', field: 'caseStatusValue' },
      { title: 'Queue Name', field: 'queueName' },
      { title: 'Fraud Status', field: 'fraudStatusValue' },
      { title: 'Fraud Type', field: 'fraudTypeValue' }
    ];
  }
  getOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    let pageLoaded = true;
    if (this.savecaseID) {
      pageLoaded = false;
    }
    const response = await getService(url);
    if (response && response.status === 200) {
      let organizations = [RA_STR_SELECT, ...CA_Utils.objToArray(response.data.organizations, 'string')];
      this.setState({
        organizationList: organizations,
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object'),
        pageLoaded
      });
    }
  };

  clearToastBar() {
    if (typeof this.props.activateErrorList === 'function') {
      this.props.activateErrorList(false, '');
      this.props.activateSuccessList(false, '');
    }
  }
  async getStatusofSearchCases() {
    const url = serverUrl + RA_API_URL.getCaseStatuses;
    const response = await getService({ method: 'GET', url });
    if (response && response.status === 200) {
      let caseStatusOptions = response.data.statusList.map(option => { return { key: option, content: option } });
      this.setState({ caseStatusOptions });
    }
  }
  componentWillUnmount() {
    this.clearToastBar();
  }
  componentWillMount() {
    const urlParams = new URLSearchParams(window.location.search);
    const SelectOrganization = urlParams.get(RA_STR.orgName);
    const pageName = urlParams.get(RA_STR.getPageName);
    const caseId = urlParams.get(RA_STR.getCaseId);
    this.savecaseID = caseId;
    if (SelectOrganization && pageName) {
      if (pageName === RA_STR.caseSummaryPage) {
        this.setState({ caseId, SelectOrganization }, () => {
          this.searchCasesSubmit(RA_STR.searchByCaseIDTab)
        })
      }
      else if (pageName === RA_STR.caseHistoryPage) {
        this.setState({ caseId, SelectOrganization }, () => {
          this.getCaseHistory()
        })
      }
      this.getOrganizations();
    } else {
      this.getOrganizations();
    }
    this.getStatusofSearchCases();

    this.lastCaseOptions = this.state.lastCaseOptions.map(option => {
      return {
        key: option,
        content: option + RA_STR.mins
      }
    })
  }
  selectTab = (Tab) => {
    this.setState({ selectedTab: Tab });
  }
  goToInboundPage = (rowData) => {
    const routeToRedirect = '/case-manage/manage-inbound';
    this.props.history.push({
      pathname: `${routeToRedirect}`,
      search: `?caseId=${rowData.caseID}&fromPage=caseManagement`
    })
  }
  goToTabs = () => {
    const routeToRedirect = '/case-manage/search-case';
    this.props.history.push({
      pathname: `${routeToRedirect}`,
      search: ``
    })
    this.setState({ reportType: '', selectedTab: '', SelectOrganization: '' });
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  onchange = (e) => {
    this.setState({ [e.target.name]: e.target.checked });
  }
  showCaseDetail = async () => {
    this.clearToastBar();
    let { SelectOrganization, caseId } = this.state;
    const routeToRedirect = '/case-manage/search-case';
    this.props.history.push({
      pathname: `${routeToRedirect}`,
      search: `?orgname=${SelectOrganization}&caseID=${caseId}&page=caseAlerts`
    })
    this.setState({ reportType: RA_STR.caseAlertsPage });
    this.clearToastBar();
  }
  getCaseHistory = async (type) => {
    this.clearToastBar();
    this.setState({ pageLoaded: false });
    let { SelectOrganization, caseId } = this.state;
    const url = serverUrl + RA_API_URL.getOrgUrl + '/' + SelectOrganization + RA_API_URL.searchCases + caseId + RA_API_URL.searchHistory;
    const searchhistoryObj = await getService({ method: 'GET', url });
    if (searchhistoryObj && searchhistoryObj.status === 200) {
      const routeToRedirect = '/case-manage/search-case';
      this.props.history.push({
        pathname: `${routeToRedirect}`,
        search: `?orgname=${SelectOrganization}&caseID=${caseId}&page=caseHistory`
      })
      this.setState({ caseSummaryData: searchhistoryObj.data.casesList, reportType: 'caseHistory', pageLoaded: true })
    } else if (searchhistoryObj && searchhistoryObj.status === 400) {
      window.scrollTo(0, 0);
    }
    this.clearToastBar();
  }
  searchCasesSubmit = async (param) => {
    var dateranges = {};
    if (this.refs.searchCriteriaRef) {
      dateranges = this.refs.searchCriteriaRef.getDateRanges();
      this.prevstateObj = dateranges.saveDateObj;
      dateranges = dateranges.data;
    }
    let { SelectOrganization, caseId, selectedTab, userName, caseStatus, decrypt } = this.state;
    this.setState({ pageLoaded: false });
    if (selectedTab === RA_STR.searchByCaseIDTab || param === RA_STR.searchByCaseIDTab) {
      const url = serverUrl + RA_API_URL.searchByCaseId;
      const data = {
        orgName: SelectOrganization,
        caseId: caseId,
        decrypt
      }
      const response = await getService({ method: 'POST', url, data });
      if (response && response.status === 400) {
        this.props.activateSuccessList(false, '');
        this.props.activateErrorList(true, response.data.errorList);
        this.setState({ pageLoaded: true });
        window.scrollTo(0, 0);
      } else if (response && response.status === 200) {
        const routeToRedirect = '/case-manage/search-case';
        this.props.history.push({
          pathname: `${routeToRedirect}`,
          search: `?orgname=${SelectOrganization}&caseID=${caseId}&page=caseSummary`
        })
        this.setState({ caseSummaryData: [response.data.caseView], reportType: 'caseSummary', pageLoaded: true });
        this.clearToastBar();
      }
    } else {
      let { fromDate, toDate } = dateranges;
      const data = {
        orgName: SelectOrganization, userName, caseStatus, latestTime: 0, pageNo: 0,
        pageSize: 10,
        fromDate,
        toDate,
        decrypt
      }
      const url = serverUrl + RA_API_URL.searchByCriteria;
      const response = await getService({ method: 'POST', url, data });
      if (response && response.status === 200) {
        this.props.activateSuccessList(false, '');
        this.props.activateErrorList(true, '');
        this.setState({ caseSummaryData: [response.data.caseView], reportType: 'caseSummary', pageLoaded: true })
      } else if (response && response.status === 400) {
        this.props.activateSuccessList(false, '');
        this.props.activateErrorList(true, response.data.errorList);
      }
      window.scrollTo(0, 0);
    }
    this.setState({ pageLoaded: true });
  }
  exportReport = async () => {
    let { SelectOrganization, caseId } = this.state;
    const data = { caseId, orgName: SelectOrganization };
    const url = serverUrl + RA_API_URL.getOrgUrl + '/' + SelectOrganization + RA_API_URL.searchCases + caseId + RA_API_URL.export;
    const exportReport = await getService({ method: 'POST', url, data });
    if (exportReport.status === 200) {
      if (exportReport && exportReport.data) {
        const blob = new Blob([exportReport.data], { type: 'application/octet-stream' });
        saveAs(blob, RA_STR.caseExcelSheetName + caseId + RA_STR.csvFormat);
      }
    }
  }
  getActivePage = (getPageNo) => {
    let { selectedTab } = this.state;
    let pageCount = getPageNo - 1;
    this.setState({
      pageNo: pageCount,
      currentPageNo: getPageNo
    }, () => {
      if (selectedTab === RA_STR.searchCriteriaTab) {
        this.searchCasesSubmit(RA_STR.searchCriteriaTab);
      }
    })

  }
  getActivePage = (getPageNo) => {
    let pageCount = getPageNo - 1;
    this.setState({
      pageNo: pageCount,
      currentPageNo: getPageNo

    }, () => {
      // if (selectedTab === 'SearchbyCriteria') {
      //   this.searchCasesSubmit('SearchbyCriteria');
      // }
    })
  }
  onOrgSelect = (value, type) => {
    if (type === 'click' || type === 'blur') {
      let { selectedTab, originalOrganizations } = this.state;
      const orgId = originalOrganizations.find((element) => { if (element.content === value) return element.key });
      if (selectedTab === '' && orgId) {
        selectedTab = RA_STR.searchCriteriaTab;
      }
      if ((type === 'blur' && !orgId && this.refs.autoSuggestionBox) || (value === RA_STR_SELECT && this.refs.autoSuggestionBox)) {
        this.refs.autoSuggestionBox.setvalue('');
      }
      this.setState({
        SelectOrganization: orgId ? orgId.key : '',
        selectedTab
      })
    }
  }

  loadCaseReports() {
    let { caseSummaryData, reportType, SelectOrganization, caseId, startDate, endDate, pageSize, activePageNum, selectedTab } = this.state;
    switch (reportType) {
      case RA_STR.caseSummaryPage:
        return <div>
          <h2 className='title'>Cases Summary</h2>
          <p className='mt-4'>Number of cases matching the search criteria: {caseSummaryData && caseSummaryData.length}</p>
          <p className='mt-6'>{RA_STR.caseHistoryMessage}</p>
          <div className="tabele-buttons">
            {
              selectedTab === 'SearchbyCriteria' ?
                <DynamicTable columns={this.caseSummarycolumns} data={caseSummaryData} enablePagination={true} pageSize={pageSize} totalCount={caseSummaryData.length} activePage={() => this.getActivePage} activePageNumNew={activePageNum} />
                :
                <DynamicTable columns={this.caseSummarycolumns} data={caseSummaryData} />
            }
          </div>
          <div className="form-group row ReportButtons mt-4 ml-0">
            <input className="secondary-btn" id="searchButton" type="submit" value="CANCEL" onClick={this.goToTabs} ></input>
          </div>
        </div>
      case RA_STR.caseHistoryPage:
        return <div>
          <h2 className='title'>{RA_STR.caseHistoryHeader}</h2>
          <p className='mt-2'>{RA_STR.caseHistorydesc}</p>
          <div className='row'>
            <div className='col-sm-2'>{RA_STR.orgzName}</div>
            <div className='col-sm-4'>{SelectOrganization}</div>
          </div>
          <div className='row mt-4 mb-4'>
            <div className='col-sm-2'>{RA_STR.caseID}</div>
            <div className='col-sm-4'>{caseId}</div>
          </div>
          <div className="form-group row mt-4 ml-0">
            <input id="searchButton" type="submit" value="BACK" onClick={() => this.searchCasesSubmit('searchbyCaseId')}></input>
            <input className="ml-2" id="searchButton" type="submit" value="EXPORT" onClick={this.exportReport}></input>
          </div>
          <div className="tabele-buttons caseScrollSet">
            <DynamicTable columns={this.caseHistorycolumns} data={caseSummaryData} />
          </div>
        </div>
      case '':
        return '';
    }
  }

  render() {
    let { organizationList, selectedTab, SelectOrganization, caseId, reportType, decrypt, pageLoaded } = this.state;
    this.SelectTheOrganization = <div className="form-group row">
      <label className="col-sm-4 col-form-label">{RA_STR.selectOrg}</label>
      <div className='col-sm-8' ref='autoSuggestContainer'>
        <AutoSuggest ref="autoSuggestionBox" orgOptions={organizationList} getSelectedOrgDetails={this.onOrgSelect} enableAutosuggest={true} defaultOrganizationPlaceholder={RA_STR_SELECT} value={SelectOrganization} />
      </div>
    </div>
    return <div className='main searchCases'>
      {!pageLoaded ? <div><Loader show='true' />{LOADING_TEXT}</div>
        :
        (reportType !== '' ? this.loadCaseReports() : <div>
          <h2 className='title'>{RA_STR.searchCases}</h2>
          <p className='desc'>{RA_STR.specifySearchCriteria}</p>
          {SelectOrganization === '' && selectedTab === '' ? < div className='col-sm-5'>
            {this.SelectTheOrganization}
          </div>
            :
            <div>
              <div>
                <ul className="nav nav-pills form-inline">
                  <li className={"nav-item " + (selectedTab === 'SearchbyCriteria' && 'selectedTab')} onClick={() => this.selectTab('SearchbyCriteria')}>
                    {RA_STR.searchCriteriaheader}
                  </li>
                  <li className={"nav-item " + (selectedTab === 'searchbyCaseId' && 'selectedTab')} onClick={() => this.selectTab('searchbyCaseId')}>
                    {RA_STR.searchBycaseidHeader}
                  </li>
                </ul>
              </div>
              <div className="tabui">
                {selectedTab === 'SearchbyCriteria' ?
                  < SearchbyCriteria accessParent={this} ref="searchCriteriaRef"></SearchbyCriteria>
                  :
                  <div className="searchbyCaseIdTab">
                    <div className="row">
                      <div className="col-sm-6">
                        {this.SelectTheOrganization}
                        <SingleInput
                          title={'Case ID'}
                          inputType={'text'}
                          name={'caseId'}
                          content={caseId}
                          controlFunc={this.handleChange} />
                      </div>
                    </div>
                  </div>
                }
              </div>
              <div className="row mt-5 caseSubmit tabui">
                <div className="col-sm-4">
                  <div className="custom-control custom-checkbox">
                    <label key='decryptionNeeded' className='form-label capitalize col-sm-12' >
                      <input
                        className="form-checkbox custom-control-input"
                        type='checkbox'
                        id='decryptionNeeded'
                        name='decrypt'
                        onChange={this.onchange}
                        value={decrypt}
                        checked={decrypt}
                      />
                      <label className="form-label custom-control-label" htmlFor='decryptionNeeded'> {RA_STR.DecryptInfo}</label>
                    </label>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="form-group row">
                    <input className="secondary-btn" id="searchButton" type="submit" value="SUBMIT" onClick={() => this.searchCasesSubmit('caseSummary')}></input>
                  </div>
                </div>
              </div >
            </div>
          }
        </div>
        )
      }
    </div >

  }
}

export default SearchCases;
