import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import DatePickerWithTimer from './DateRangeWithTimer'


class SearchbyCriteria extends Component {
    constructor(props) {
        super(props);
    }
    getDateRanges() {
        let dateranges = {};
        if (this.refs.DatepickerTimerRef) {
            dateranges = this.refs.DatepickerTimerRef.getDateRanges();
        }
        return dateranges;
    }
    render() {
        let { userName, caseDataType, caseStatus, caseStatusOptions } = this.props.accessParent.state;
        return <div className="searchCasesTab">
            <div className="row">
                <div className="col-sm-6">
                    {this.props.accessParent.SelectTheOrganization}
                    <Select
                        name='caseStatus'
                        title='Case Status'
                        options={caseStatusOptions}
                        placeholder={'--Select--'}
                        selectedOption={caseStatus}
                        controlFunc={this.props.accessParent.handleChange} />
                </div>
            </div>
            <div className="row">
                <div className="col-sm-6">
                    <SingleInput
                        title={'Enter Username'}
                        inputType={'text'}
                        name={'userName'}
                        content={userName}
                        controlFunc={this.props.accessParent.handleChange} />
                </div>
            </div>
            <div className="noMarginrow mt-6">
                <div className="col-sm-2">
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                        <input type="radio" className="custom-control-input "
                            id="caseDataTypeparent"
                            name="caseDataType"
                            value='0'
                            checked={caseDataType == '0'}
                            onChange={this.props.accessParent.handleChange} />
                        <label className="custom-control-label" htmlFor="caseDataTypeparent">Case Date Range</label>
                    </div>
                </div>
                <DatePickerWithTimer ref='DatepickerTimerRef' disableAll={caseDataType == '1'} prevstateObj={this.props.accessParent.prevstateObj}></DatePickerWithTimer>
            </div>
            <div className="noMarginrow mt-4">
                <div className="col-sm-2">
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                        <input type="radio" className="custom-control-input "
                            id="caseDataTypeChild"
                            name="caseDataType"
                            value='1'
                            checked={caseDataType == '1'}
                            onChange={this.props.accessParent.handleChange} />
                        <label className="custom-control-label" htmlFor="caseDataTypeChild">Last Cases</label>
                    </div>
                </div>
                <div className="col-sm-5 disableLight">
                    <Select
                        name='SelectChannel'
                        title=''
                        options={this.props.accessParent.lastCaseOptions}
                        placeholder={'--Select--'}
                        selectedOption={''}
                        controlFunc={this.props.accessParent.handleChange}
                        disabled={caseDataType == '0'} />
                </div>
            </div>
        </div>
    }
}
export default SearchbyCriteria;