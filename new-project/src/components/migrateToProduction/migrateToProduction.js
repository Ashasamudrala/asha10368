import React, { Component, Fragment } from 'react';
import DynamicListRender from '../../shared/components/DynamicListRender/DynamicListRender';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_PROFILEDATA } from '../../shared/utlities/constants';
import './migrateToProduction.css'

class MigrateToProduction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isoLangs: ["Select All Rulesets"],
      selectedOptions: [],
      selectAll: false,
      allOptions: [],
      migrateSucess: false,
      errorMsg: null,
      successMsg: null,
      confirmSuccess: false,
      isoLangsSelected: []
    }
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    const orgName = urlParams.get('orgname');
    if (this.props.match.path === '/server-config/ra/migrate-prod') {
      this.setState({ orgName: 'SYSTEM' }, this.getRuleSets);
    } else
      this.setState({ orgName }, this.getRuleSets);
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  getRuleSets = async () => {
    const { orgName } = this.state;
    const { activateErrorList, activateSuccessList } = this.props;
    if (orgName !== null) {
      const { token = null } = JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA));
      const getOptions = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL.getOrgUrl}/${orgName}/migrate`
      };
      const getOptionsUpdate = await getService(getOptions);
      if (getOptionsUpdate.status === 200) {
        if (getOptionsUpdate.data.message !== null) {
          activateSuccessList(true, getOptionsUpdate.data);
          activateErrorList(false, '');
        }
        this.setState({ allOptions: getOptionsUpdate.data.configList })
      } else if (getOptionsUpdate.status === 400) {
        activateSuccessList(false, '');
        activateErrorList(true, getOptionsUpdate.data.errorList);
      }
    }
  }


  onSelectAll = () => {
    let { selectAll, allOptions, isoLangsSelected } = this.state;
    let selectedOptions = [];
    isoLangsSelected = [];
    if (!selectAll) {
      selectedOptions = allOptions.map((value) => value);
      isoLangsSelected = ['Select All Rulesets'];
    }
    this.setState({ selectAll: !selectAll, selectedOptions, isoLangsSelected });
  }

  onOptionClick = ({ target: { value, selectedOptions } }) => {
    selectedOptions = Array.from(selectedOptions, option => option.value);
    this.setState({ selectedOptions });
  }

  onMigrateClick = async () => {
    const { activateErrorList, activateSuccessList } = this.props;
    const { selectedOptions: selectedRulesetList, orgName } = this.state;
    this.setState({ confirmSuccess: false });
    window.scrollTo(0, 0);
    activateSuccessList(false, '');
    if (Array.isArray(selectedRulesetList) && selectedRulesetList.length === 0) {
      activateErrorList(true, [{ errorMessage: 'Select at least one Ruleset to migrate.' }]);
      return;
    }
    const data = {
      ruleSetId: 0,
      selectedRulesetList: this.state.selectedOptions,
      global: true,
      configName: null,
      configList: null,
      selectAllConfigs: true,
      action: "migrate"
    };
    if (orgName !== null) {
      const { token = null } = JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA));
      const postMigrateOptions = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL.getOrgUrl}/${orgName}/migrate`,
        data
      };
      const postMigrateOptionsUpdate = await getService(postMigrateOptions);
      if (postMigrateOptionsUpdate.status === 200) {
        activateErrorList(false, '');
        this.setState({ migrateSucess: true });
      } else if (postMigrateOptionsUpdate.status === 400) {
        activateSuccessList(false, '');
        activateErrorList(true, postMigrateOptionsUpdate.data.errorList);
      }
    }
  }

  onConfirmClick = async () => {
    const { activateErrorList, activateSuccessList } = this.props;
    if (this.state.configList !== this.state.selectAllConfigs) {
      return this.setState({ selectAllConfigs: false });
    }
    const { selectedOptions: selectedRulesetList, orgName } = this.state;
    const data = {
      ruleSetId: 0,
      selectedRulesetList: this.state.selectedOptions,
      global: true,
      configName: null,
      configList: null,
      selectAllConfigs: this.state.selectAllConfigs,
      action: ""
    };
    if (orgName !== null) {
      const { token = null } = JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA));
      const postMigrateOptions = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL.getOrgUrl}/${orgName}/migrate`,
        data
      };
      const postMigrateOptionsUpdate = await getService(postMigrateOptions);
      if (postMigrateOptionsUpdate.status === 200) {
        postMigrateOptionsUpdate.data.message = "The proposed data has been successfully migrated to Production.";
        if (postMigrateOptionsUpdate.data.message !== null) {
          activateSuccessList(true, postMigrateOptionsUpdate.data);
          activateErrorList(false, '');
        }
        this.setState({ migrateSucess: false, errorMsg: null, confirmSuccess: true, successMsg: null, selectedOptions: [], selectAll: false });
      } else if (postMigrateOptionsUpdate.status === 400) {
        activateSuccessList(false, '');
        activateErrorList(true, postMigrateOptionsUpdate.data.errorList);
        this.setState({ errorMsg: postMigrateOptionsUpdate.errmsg });
      }
    }
  }

  onCancelClick = () => {
    window.scrollTo(0, 0);
    this.setState({ migrateSucess: false, selectedOptions: [], selectAll: false });
  }

  render() {
    const match = this.props.match.url;
    const { selectedOptions, selectAll, allOptions, migrateSucess, errorMsg, confirmSuccess, successMsg, orgName, isoLangsSelected } = this.state;
    return <div className="main">
      {!migrateSucess ? (
        <div className="no-padding">
          <h2 className="title">Migrate to Production</h2>
          <p className="desc">When you configure the data for a Ruleset, it is not available for an administrator to use unless you make it Active. For this, you must migrate it to production. After migrating, you must then refresh the server cache.</p>
          <div className="col-md-12 form-group row no-padding">
            <Checkbox
              type={'checkbox'}
              options={this.state.isoLangs}
              controlFunc={this.onSelectAll}
              selectedOptions={isoLangsSelected}
            />
            <span className="seperatText">OR</span>
            <div className="col-md-9">
              <div className="floatleft">
                Select Ruleset(s)
                <br></br>
                <select className="select-options" name="selectedRulesetList" multiple={true} onChange={this.onOptionClick} value={selectedOptions} >
                  {allOptions.map((value, index) => (<option value={value} key={index} disabled={selectAll}>{orgName}-{value}</option>))}
                </select>
              </div>
            </div>
          </div>
        </div>
      ) : (
          <div>
            <h2 className="title">Migrate to Production</h2>
            <p className="desc">This step will move all the "Proposed" Ruleset data to "Active" state, and will be available for all server instances after refresh.</p>
            <div className="bordered_table">Selected Ruleset(s)</div>
            <div className="migrate-rulelist">
              {Array.isArray(selectedOptions) ? selectedOptions.map((oneRule, i) => <div key={i}>{orgName} {oneRule}</div>) : null}
            </div>
          </div>
        )
      }
      {migrateSucess ? (
        <Fragment>
          <div className="form-group form-submit-button">
            <input className="secondary-btn" id="createRoleButton" type="submit" value="CONFIRM" onClick={this.onConfirmClick} />
            <input className="tertiary-btn migrate-btn" id="createRoleButton" readOnly value="CANCEL" onClick={this.onCancelClick} />
          </div>
        </Fragment>
      ) : (
          <div className="form-group form-submit-button">
            <input className="secondary-btn" id="createRoleButton" type="submit" value="MIGRATE" onClick={this.onMigrateClick} />
          </div>
        )}
    </div>;
  }
}

export default MigrateToProduction;
