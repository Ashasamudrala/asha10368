// npm dependencies
import React, { Component } from 'react';
import * as _ from 'underscore';

// Service imports
import { getService } from '../../shared/utlities/RestAPI';

// Component dependencies
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';

// Internal Dependency imports
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl, RA_API_URL, RA_API_STATUS } from '../../shared/utlities/constants';

/**
 * Class component: This component is for protocol configuration.
 */
class ProtocolConfig extends Component {
  /**
	 * Constructor for AssociateEndPoint Component
	 * @class
	 * @param 				{Object} 	props		Initialization props
	 * @description		It initialises the state, from props
	 */
  constructor(props) {
    super(props);

    // initializing the local state
    this.state = {

      // Variables for storing form values
      instanceName: '',
      protocolId: '',
      port: '',
      actionType: '',
      minThreads: '',
      maxThreads: '',
      clientType: '',
      serverInstance: '',
      startUpTime: '',
      upTime: '',
      serverType: '',
      protocol: '',
      transportType: '',
      serverCertChain: [],
      serverPrivateKey: [],

      // booleans for conditional rendering
      hsmSelected: false,
      updateServer: false,
      protocolEnable: true,
      transportEnable: true,
      serverKeyEnable: true,
      currentPage: 1,

      // Variables for storing data
      selectedServer: {},
      searchData: [],
      instanceData: [],
      protocolData: [],
      clientOptions: [],
      protocolDetails: [],
      transportOptions: [],
      serverInstanceData: [],
      actionOptions: [{ key: -1, content: "Select" }, { key: 1, content: "Enable" }, { key: 0, content: "Disable" }],
      columnData: [
        {
          title: 'Protocol Name', field: 'protocolName', render: (rowData) =>
            <div className="request-id" onClick={this.getProtocolDetails.bind(this, rowData.protocolID)}>{rowData.protocolName}</div>
        },
        { title: 'Port', field: 'port' },
        { title: 'Protocol Status', field: 'enabled' },
      ],
    }
  }

  /**
  * componentDidMount is invoked immediately after this component is mounted, so all initialization goes here.
  * @description   This method contains the get call for categories.
  */
  componentDidMount() {
    this.getInstances();
  };

  /**
   * componentWillUnmount is invoked immediately before a component is unmounted and destroyed.
   * @description   This method takes back the error/success message to initial state.
   */
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  /**
   * @description   This method gets all the Instances.
   */
  getInstances = async () => {
    // Variable that contains method,url declaration that goes to backend.
    const instances = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getInstances'] + '?seclevelid=RiskFort&toplevelid=Srv_Config'}`
    }

    const response = await getService(instances);
    if (response && response.status === RA_API_STATUS['200']) {
      const data = CA_Utils.objToArray(response.data.instanceNames, 'object');
      this.setState({ instanceData: data });
    }
  }

  /**
   * @param  {string}   name  It carries the name of selected instance.
   * @description       This method gets all the instance details.
   */
  getInstanceDetails = async (name) => {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    // Variable that contains method,url declaration that goes to backend.
    const instanceDetails = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getInstanceDetails'] + '?instanceName=' + name}`
    }
    const response = await getService(instanceDetails);
    if (response && response.status === RA_API_STATUS['200']) {
      const data = response.data.serverInstanceDetails;
      data.protocolDetails.forEach(element => {
        element.enabled = element.enabled == 1 ? 'Enabled' : 'Disabled';
      })
      this.setState({
        protocolData: data.protocolDetails,
        startUpTime: data.startupTime,
        upTime: data.serverUpTime,
        serverType: data.serverType
      });
    }
  }

  /**
   * @param  {string}   protocolId  It carries the selected protocol ID.
   * @description       This method gets all the protocol details by ID.
   */
  getProtocolDetails = async (protocolId) => {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    // Variable that contains method,url declaration that goes to backend.
    const protocolDetails = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['protocolDetails'] + '?instanceName=' + this.state.instanceName + '&protocolId=' + protocolId}`
    }
    const response = await getService(protocolDetails);
    if (response && response.status === RA_API_STATUS['200'] && response.data) {
      this.setState({
        protocolDetails: response.data.serverInstanceDetails.protocolDetails[0],
        selectedServer: response.data.serverInstanceDetails
      })
      const data = this.state.protocolDetails;
      const transportData = CA_Utils.objToArray(data.supportedPortTypes, 'object');
      const clientData = CA_Utils.objToArray(data.availableClientRoots, 'object');
      transportData.forEach(element => {
        if (element.content == data.transport) {
          this.setState({ transportType: element.key })
          if (element.content == 'TCP') {
            this.setState({ transportEnable: false, hsmSelected: false });
          } else {
            clientData.forEach(element => {
              if (element.content = data.sslStoreName) {
                this.setState({ clientType: element.key });
              }
            })
            this.setState({ transportEnable: true, hsmSelected: false });
          }
        }
      })
      this.setState({
        protocolstatus: data.enabled == 1 ? "enabled" : "disabled",
        protocolId: protocolId,
        currentPage: 2,
        protocol: data.protocolName,
        port: data.port,
        minThreads: data.minThreads,
        maxThreads: data.maxThreads,
        transportOptions: transportData,
        clientOptions: CA_Utils.objToArray(data.availableClientRoots, 'object')
      })
    }
  }

  /**
	 * Handler method to handle user input.
	 * @param 	{*}		e 	The HTML event generated by the form.
	 * @description				It handles the change in values.
	 */
  handleChange = e => {
    if (e.target.name === 'serverInstance') {
      const data = [...this.state.instanceData];
      if (e.target.value) {
        data.forEach(async (element) => {
          if (e.target.value == element.key) {
            this.setState({ instanceName: element.content });
            this.getInstanceDetails(element.content);
          }
        })
      } else {
        this.setState({ instanceName: '' });
      }
    } else if (e.target.name === 'protocolEnable') {
      this.setState({ protocolEnable: !this.state.protocolEnable })
    } else if (e.target.name === 'transportType') {
      const data = [...this.state.transportOptions];
      if (e.target.value) {
        data.forEach(async (element) => {
          if (e.target.value == element.key) {
            this.setState({ transportType: e.target.value });
            if (element.content == 'TCP') {
              this.setState({ transportEnable: false, hsmSelected: false });
            } else {
              this.setState({ transportEnable: true, hsmSelected: false });
            }
          }
        })
      }
    } else if (e.target.name === 'hsmSelected') {
      if (e.target.checked) {
        this.setState({
          serverKeyEnable: false
        })
      } else {
        this.setState({
          serverKeyEnable: true
        })
      }
    } else if (e.target.name === 'serverCertChain') {
      let reader = new FileReader();
      let file = e.target.files[0];
      if (file) {
        reader.onload = () => {
          const intArray = new Int8Array(reader.result);
          this.setState({
            serverCertChain: intArray
          })
        }
        reader.readAsArrayBuffer(file)
      }
      else {
        this.setState({
          serverCertChain: []
        })
      }
    } else if (e.target.name === 'serverPrivateKey') {
      let reader = new FileReader();
      let file = e.target.files[0];
      if (file) {
        reader.onload = () => {
          const intArray = new Int8Array(reader.result);
          this.setState({
            serverPrivateKey: intArray
          })
        }
        reader.readAsArrayBuffer(file)
      }
      else {
        this.setState({
          serverPrivateKey: []
        })
      }
    } else if (e.target.name === 'clientType') {
      this.setState({ clientType: e.target.value });
    } else {
      this.setState({
        [e.target.name]: e.target.value
      })
    }
  }

  /**
   * @description     This method binds all the form data and sends to backend.
   */
  handleSave = async () => {
    // Disabling error/success messages
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    const transportName = '';
    const data = [...this.state.transportOptions];
    data.forEach(element => {
      if (this.state.transportType == element.key) {
        this.transportName = element.content;
      }
    })
    const clientName = '';
    const data1 = [...this.state.clientOptions];
    data1.forEach(element => {
      if (this.state.clientType == element.key) {
        this.clientName = element.content;
      }
    });
    // Variable that contains method,url declaration that goes to backend.
    const updateProtocolDetails = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_URL['protocolDetails']}`,
      data: {
        "hostName": this.state.selectedServer.hostName,
        "protocolID": this.state.protocolId,
        "protocolName": this.state.protocol,
        "displayName": this.state.protocolDetails.displayName,
        "portNumber": this.state.port,
        "minThreads": this.state.minThreads,
        "maxThreads": this.state.maxThreads,
        "transport": this.transportName,
        "enabled": this.state.actionType ? +this.state.actionType : this.state.protocolDetails.enabled,
        "supportedPortTypes": this.state.protocolDetails.supportedPortTypes,
        "instanceName": this.state.instanceName,
        "instanceID": this.state.selectedServer.instanceID,
        "serverCertChain": this.transportName == 'TCP' ? [] : Object.values(this.state.serverCertChain),
        "serverPrivateKey": this.transportName == 'TCP' ? [] : Object.values(this.state.serverPrivateKey),
        "certPresent": 0,
        "certSubject": "",
        "certIssuer": "",
        "certValidFrom": "",
        "certValidTo": "",
        "sslStoreName":this.transportName == 'TCP' ? '' : this.clientName,
        "serverType": this.state.serverType,
        "hsmSelected": this.transportName == 'TCP' ? false : this.state.hsmSelected,
        "infoList": ""
      }
    };
    const response = await getService(updateProtocolDetails);
    if (response && response.status === RA_API_STATUS['200'] && response.data) {
      this.setState({ protocolEnable: true, actionType: -1 })
      this.getProtocolDetails(this.state.protocolId)
      this.props.activateSuccessList(true, response.data);
      this.props.activateErrorList(false, '');
      window.scrollTo(0, 0);
    } else if (response && response.data.errorList) {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  }

  /**
   * @description   This method takes back to first page.
   */
  handleBack = () => {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    this.setState({
      currentPage: 1
    })
  }

  render() {
    const { instanceData,
      instanceName,
      startUpTime,
      upTime,
      serverType,
      columnData,
      protocolData,
      currentPage,
      protocol,
      protocolstatus,
      actionOptions,
      actionType,
      port,
      minThreads,
      maxThreads,
      clientType,
      clientOptions,
      transportType,
      transportOptions,
      transportEnable,
      protocolEnable,
      hsmSelected,
      serverKeyEnable
    } = this.state;
    return <div className="main">
      {
        (currentPage == 1)
          ? <div>
            <h2 className="title">{RA_STR.PROTOCOL_CONFIG.TITLE}</h2>
            <p className="desc">{RA_STR.PROTOCOL_CONFIG.DESCRIPTION}</p>
            <div className="col-sm-6">
              <Select
                name={'serverInstance'}
                title={'Server Instance'}
                options={instanceData}
                placeholder={'Select'}
                controlFunc={this.handleChange}
                 />
              {
                (instanceName)
                  ? <div>
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">{RA_STR.PROTOCOL_CONFIG.STARTUP_TIME}</label>
                      <div className="col-sm-8">
                        {startUpTime}
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">{RA_STR.PROTOCOL_CONFIG.UP_LABEL}</label>
                      <div className="col-sm-8">
                        {upTime}
                      </div>
                    </div>
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">{RA_STR.PROTOCOL_CONFIG.SERVER_TYPE_LABEL}</label>
                      <div className="col-sm-8">
                        {serverType}
                      </div>
                    </div>
                  </div>
                  : null
              }
            </div>
            <div className="col-sm-12">
              {
                (instanceName)
                  ? <DynamicTable
                    columns={columnData}
                    data={protocolData}
                  />
                  : null
              }
            </div>
          </div>
          : <div className="col-sm-8">
            <h2 className="title">{instanceName} : {protocol}</h2>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">{RA_STR.PROTOCOL_CONFIG.PROTOCOL_STATUS}</label>
              <div className="col-sm-6">
                {protocolstatus}
              </div>
              <label className="col-sm-4 col-form-label">{RA_STR.PROTOCOL_CONFIG.CHANGE_PROTOCOL}</label>
              <div className="col-sm-6">
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    name='protocolEnable'
                    id="customCheck1"
                    onChange={this.handleChange}
                    checked={!protocolEnable} />
                  <label className="custom-control-label" htmlFor="customCheck1"></label>
                </div>
              </div>
              <div className="col-sm-8">
                <Select
                  name={'actionType'}
                  title={'Action'}
                  selectedOption={actionType.toString()}
                  disabled={protocolEnable}
                  options={actionOptions}
                  controlFunc={this.handleChange}
                />
                <SingleInput
                  title={'Port'}
                  required={false}
                  inputType={'text'}
                  name={'port'}
                  controlFunc={this.handleChange}
                  content={port} />
                <SingleInput
                  title={'Minimum Threads'}
                  required={false}
                  inputType={'text'}
                  name={'minThreads'}
                  controlFunc={this.handleChange}
                  content={minThreads} />
                <SingleInput
                  title={'Maximum Threads'}
                  required={false}
                  inputType={'text'}
                  name={'maxThreads'}
                  controlFunc={this.handleChange}
                  content={maxThreads} />
                <Select
                  name={'transportType'}
                  title={'Transport'}
                  selectedOption={transportType.toString()}
                  required={false}
                  options={transportOptions ? transportOptions : ['']}
                  controlFunc={this.handleChange} />
              </div>
              <div className="col-sm-4"></div>
              <label className="col-sm-4 col-form-label">{RA_STR.PROTOCOL_CONFIG.HSM_KEY_LABEL}</label>
              <div className="col-sm-6">
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    name='hsmSelected'
                    id="customCheck2"
                    disabled={!transportEnable}
                    onChange={this.handleChange}
                    defaultChecked={hsmSelected} />
                  <label className="custom-control-label" htmlFor="customCheck2"></label>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label">{RA_STR.PROTOCOL_CONFIG.SERVER_CERT_LABEL}</label>
                <div className="col-md-9">
                  <input
                    type="file"
                    name={'serverCertChain'}
                    onChange={this.handleChange}
                    disabled={!transportEnable}
                  /><br />
                  <p>{RA_STR.PROTOCOL_CONFIG.SERVER_CERT_DESC_LABEL}</p>
                </div>
                <label className="col-sm-3 col-form-label">{RA_STR.PROTOCOL_CONFIG.SERVER_KEY_LABEL}</label>
                <div className="col-md-9">
                  <input
                    type="file"
                    name={'serverPrivateKey'}
                    onChange={this.handleChange}
                    disabled={!transportEnable || !serverKeyEnable}
                  /><br />
                  <p>{RA_STR.PROTOCOL_CONFIG.SERVER_KEY_DESC_LABEL}</p>
                </div>
              </div>
              <div className="col-sm-8">
                <Select
                  name={'clientType'}
                  title={'Select Client Store'}
                  placeholder={'Select'}
                  selectedOption={clientType.toString()}
                  required={false}
                  disabled={!transportEnable}
                  options={clientOptions ? clientOptions : ['']}
                  controlFunc={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group form-submit-button row">
              <input className="secondary-btn ml-3 mr-2" type="button" value="SAVE" onClick={this.handleSave}></input>
              <input className="custom-secondary-btn" type="submit" value="BACK" onClick={this.handleBack}></input>
            </div>
          </div>
      }
    </div>
  }
}
// Default export
export default ProtocolConfig;
