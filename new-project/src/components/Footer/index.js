import React from 'react';
import './styles.css';

const Footer = () => (
  <div className="footer">
    <div className="edl-footer">
      Copyright ©
    <span id="footerCurrentYear">{new Date().getFullYear()}</span>
      CA. All rights reserved.
    </div>
  </div>
);
export default Footer;