import React, { Component, Fragment } from 'react';
import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import CA_Utils from '../../shared/utlities/commonUtils';
import './AverageCaseLifeReport.scss';
import { saveAs } from 'file-saver';
import { RA_STR } from '../../shared/utlities/messages';
import DateRange from '../../shared/components/DateRange/DateRange';
import { async } from 'q';
var moment = require('moment');
class AverageCaseLifeReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultOrganizationNme: RA_STR_SELECT,
      organizationNames: [],
      originalOrganizations: [],
      selectedTimePeriod: '',
      timePeriodList: [{ key: 'By Month', content: 'By Month' }, { key: 'Last 7 Days', content: 'Last 7 Days' }, { key: 'By Date Range', content: 'By Date Range' }],
      showTable: false,
      yearList: [],
      monthList: [],
      selectedfromDate: '',
      selectedtoDate: '',
      selectedyear: '',
      selectedMonth: '',
      selectedDateRange: '',
      displatReportStatus: false,
      reportList: [],
      tableData: [{
        drillDownEnabled: false,
        casesClosedQueue: 0,
        casesClosedIch: 4,
        casesExpired: 0,
        activityQueue: 0,
        activityIch: 0,
        activityExpired: 0,
        avgActivityTimeQueue: null,
        avgActivityTimeIch: null,
        avgActivityTimeExpired: null,
        caseStatus: 0,
        workflow: 0,
        deltaTimeZone: 0,
        queueName: 'Inbound Calls',
        orgName: null,
        period: '07/01/2019 - 12/01/2019',
        txnFromDBToDateStr: null,
        txnToDBToDateStr: null,
        txnFromDBToDate: null,
        txnToDBToDate: null,
        sign: "",
        drillDownString: null,
        dateRangeType: null,
        queueId: -1
      },
      {
        drillDownEnabled: false,
        casesClosedQueue: 0,
        casesClosedIch: 4,
        casesExpired: 0,
        activityQueue: 0,
        activityIch: 0,
        activityExpired: 0,
        avgActivityTimeQueue: null,
        avgActivityTimeIch: null,
        avgActivityTimeExpired: null,
        caseStatus: 0,
        workflow: 0,
        deltaTimeZone: 0,
        queueName: 'Calls',
        orgName: null,
        period: '07/01/2019 - 12/01/2019',
        txnFromDBToDateStr: null,
        txnToDBToDateStr: null,
        txnFromDBToDate: null,
        txnToDBToDate: null,
        sign: "",
        drillDownString: null,
        dateRangeType: null,
        queueId: 1
      }],
      columns: [
        { title: 'Cases Handled Through', field: 'adviceId' },
        { title: 'Period', field: 'countFraud' },
        { title: 'Cases Closed', field: 'countAssumedGenuine' },
        { title: 'Case Activity Count', field: 'countAssucountUnknownmedGenuine' },
        { title: 'Average Time Required for Case Closure', field: 'total' }
      ]
    }
    this.displayReport.bind(this);
  }
  componentDidMount() {
    this.getAllOrganizations();
    var year = moment().year();
    var months = moment.months();
    let currentMonth = moment().month();
    var index;
    var validMonths;
    if (currentMonth >= 5) {
      index = currentMonth - 5;
    } else {
      index = 0;
    }
    validMonths = months.slice(index, currentMonth + 1);

    let monthList = validMonths.map((month, index) => {
      return {
        key: moment().month(month).format("M"),
        content: month
      }
    })
    this.setState({
      yearList: [{ key: year, content: year }],
      monthList
    })
    this.setState({
      selectedTimePeriod: 'By Month',
      selectedyear: year,
      selectedMonth: monthList[0].key,

    });
  }
  getAllOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };
  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find((element) => { if (element.content === value) return element.key });
      this.setState({
        defaultOrganizationNme: orgId.key
      });
    }
  }
  TimeperoidChange = (event) => {
    const period = event.target.value;
    this.setState({
      selectedTimePeriod: period
    });
  }
  onMonthSelect = (event) => {
    const month = event.target.value;
    this.setState({
      selectedMonth: month
    });
  }
  onYearSelect = (event) => {
    const year = event.target.value;
    this.setState({
      selectedyear: year
    });
  }
  displayReport = (e) => {
    if (this.state.selectedTimePeriod === 'By Date Range') {
      const getDateItems = this.refs.daterange.getDates();
      this.setState({
        selectedfromDate: getDateItems[0].startDate,
        selectedtoDate: getDateItems[0].endDate,
        selectedDateRange: getDateItems[0].dateRange,
      }, () => {
        const payload = {
          orgName: this.state.defaultOrganizationNme,
          timePeriod: this.state.selectedTimePeriod,
          year: this.state.selectedyear,
          month: this.state.selectedMonth,
          dateRange: this.state.selectedDateRange,
          fromDate: this.state.selectedfromDate,
          toDate: this.state.selectedtoDate,

        }
        this.postTableData(payload);
      });
    } else if (this.state.selectedTimePeriod === 'By Month') {
      this.setState({
        selectedfromDate: '',
        selectedtoDate: '',
        selectedDateRange: '',
        selectedMonth: this.state.selectedMonth,
        selectedyear: this.state.selectedyear,
      }, () => {
        const payload = {
          orgName: this.state.defaultOrganizationNme,
          timePeriod: this.state.selectedTimePeriod,
          year: this.state.selectedyear,
          month: this.state.selectedMonth,
          dateRange: this.state.selectedDateRange,
          fromDate: this.state.selectedfromDate,
          toDate: this.state.selectedtoDate,
        }
        this.postTableData(payload);
      });
    } else {
      this.setState({
        selectedfromDate: '',
        selectedtoDate: '',
        selectedDateRange: '',
      }, () => {
        const payload = {
          orgName: this.state.defaultOrganizationNme,
          timePeriod: this.state.selectedTimePeriod,
          year: this.state.selectedyear,
          month: this.state.selectedMonth,
          dateRange: this.state.selectedDateRange,
          fromDate: this.state.selectedfromDate,
          toDate: this.state.selectedtoDate,
        }
        this.postTableData(payload);
      });
    }
  }
  newReport = () => {
    this.props.activateSuccessList(false, '');
    this.props.activateErrorList(false, '');
    this.setState({
      displatReportStatus: false,
      defaultOrganizationNme: RA_STR_SELECT,
      selectedTimePeriod: 'By Month',
    });
  }
  exportReport = async () => {
    const data = {
      orgName: this.state.defaultOrganizationNme,
      timePeriod: this.state.selectedTimePeriod,
      year: this.state.selectedyear,
      month: this.state.selectedMonth,
      dateRange: this.state.selectedDateRange,
      fromDate: this.state.selectedfromDate,
      toDate: this.state.selectedtoDate
    }
    const exportReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['exportAvgReport']}`,
      data: data
    }
    const exportSuccessData = await getService(exportReport);
    if (exportSuccessData && exportSuccessData.status === 200) {
      this.downloadCSV(exportSuccessData);
    } else {
      this.props.activateErrorList(true, exportSuccessData.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  downloadCSV(response) {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Average_CaseLife_Report.csv');
    }
  }
  postTableData = async (payload) => {
    const url = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['caseAvgLifeDisplayTable']}`,
      data: payload
    };
    const response = await getService(url);
    if (response && response.status === 200 && response.data.avgCaseLifeReportRequest != null) {
      this.setState({
        tableData: response.data.avgCaseLifeReportRequest,
      });
      this.props.activateErrorList(false, '');
      this.setState({ displatReportStatus: true });
    } else {
      this.props.activateSuccessList(false, '');
      this.props.activateErrorList(true, response.data.errorList);
    }
    // this.graph();
  }
  render() {
    const { organizationNames, selectedfromDate, selectedtoDate, selectedyear, defaultOrganizationNme, reportList, selectedMonth, timePeriodList, selectedTimePeriod, showTable, columns, yearList, monthList, tableData } = this.state;
    let tableRow;
    let tabrow
    tableData.map((item) => {
      if (item.queueId === -1) {
        return tableRow = <Fragment>
          <tr>
            <td colSpan='5'> {RA_STR.TableInner1}</td>
          </tr>
          <tr>
            <td>
              {item.queueName}
            </td>
            <td>
              {item.period}
            </td>
            <td>
              {item.casesClosedIch}
            </td>
            <td>
              {item.activityIch}
            </td>
            <td>
              {item.avgActivityTimeIch}
            </td>
          </tr>
        </Fragment>

      } else if (item.queueId != -1) {
        return tabrow = <Fragment>
          <tr>
            <td>
              {item.queueName}
            </td>
            <td>
              {item.period}
            </td>
            <td>
              {item.casesClosedQueue}
            </td>
            <td>
              {item.activityQueue}
            </td>
            <td>
              {item.avgActivityTimeQueue}
            </td>
          </tr>
          <tr>
            <td colSpan='5'> {RA_STR.TableInner2}</td>
          </tr>
          <tr>
            <td>
              {item.queueName}
            </td>
            <td>
              {item.period}
            </td>
            <td>
              {item.casesExpired}
            </td>
            <td>
              {item.activityExpired}
            </td>
            <td>
              {item.avgActivityTimeExpired}
            </td>
          </tr>
        </Fragment>

      }
    })
    return (
      <div className='main case-life-report-container'>
        {this.state.displatReportStatus === false && (
          <div>
            <h2 className='title'>{RA_STR.averageLifeReport}</h2>
            <p className='desc'>{RA_STR.avgDesc}</p>
            <hr />
            <div className='col-sm-8'>
              <div className="form-group dynamic-form-select">
                <div className="form-group row">
                  <label className="col-sm-2 col-form-label">{RA_STR.orgzName}</label>
                  <div className='col-sm-4'>
                    <AutoSuggest orgOptions={organizationNames} getSelectedOrgDetails={this.onOrgSelect} enableAutosuggest={true} defaultOrganizationPlaceholder={defaultOrganizationNme} />
                  </div>
                </div>
              </div>
            </div>
            <div className='col-sm-8'>
              <div className="form-group dynamic-form-select">
                <div className="form-group row">
                  <div className=' col-sm-6'>
                    <Select
                      name={'timeperiod'}
                      title={'TimePeriod'}
                      required={false}
                      selectedOption={selectedTimePeriod}
                      // placeholder={'By Month'}
                      options={timePeriodList ? timePeriodList : ['']}
                      controlFunc={this.TimeperoidChange} />
                  </div>
                </div>
              </div>
            </div>
            <div>
              {this.state.selectedTimePeriod === 'By Date Range' && (
                <div className="row">
                  <div className=" offset-sm-2 col-sm-12">
                    <DateRange ref="daterange"></DateRange>
                  </div>
                </div>
              )}
              {this.state.selectedTimePeriod === 'By Month' &&
                (<div className='offset-sm-2 col-sm-8'>
                  <div className="form-group dynamic-form-select">
                    <div className="form-group row">
                      <div className='col-sm-3'>
                        <Select
                          name={'year'}
                          title={'Year'}
                          required={false}
                          selectedOption={selectedyear}
                          // placeholder={'By Date Range'}
                          options={yearList ? yearList : ['']}
                          controlFunc={this.onYearSelect} />
                      </div>
                      <div className='col-sm-3'>
                        <Select
                          name={'month'}
                          title={'Month'}
                          required={false}
                          selectedOption={selectedMonth}
                          // placeholder={'By Date Range'}
                          options={monthList ? monthList : ['']}
                          controlFunc={this.onMonthSelect} />
                      </div>
                    </div>
                  </div>
                </div>)}
            </div>
            <div className="form-group form-submit-button">
              <input className="secondary-btn" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
            </div>
          </div>

        )}
        {this.state.displatReportStatus === true && (
          <div>
            <div className="row">
              <div className="col-sm-8">
                <div>
                  <h2 className="title">{RA_STR.averageLifeReport}</h2>
                </div>
                <div>
                  <p className="desc">{RA_STR.avgDesc}</p>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="row">
                  <div className=" offset-sm-3 col-sm-3">
                    <input className="secondary-btn" type="button" value="EXPORT" onClick={this.exportReport}></input>
                  </div>
                  <div className="col-sm-6">
                    <input className="secondary-btn" type="submit" value="NEWREPORT" onClick={this.newReport}></input>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-2">
                <label className="col-form-label">{RA_STR.orgzName}</label>
              </div>
              <div className="col-sm-1">
                <div className="pt-2">{defaultOrganizationNme}</div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-2">
                <label className="col-form-label">{RA_STR.timePeriod}</label>
              </div>
              <div className="col-sm-2">
                <label className="col-form-label">{selectedTimePeriod}</label>
              </div>
            </div>
            {selectedTimePeriod === 'By Date Range' && (
              <div>
                <div className="row">
                  <div className="offset-sm-2 col-sm-5">
                    <label className="col-form-label">{RA_STR.fromDate}</label><span className="date">{selectedfromDate}</span>
                    <label className="col-form-label">{RA_STR.toDate} </label><span className="date">{selectedtoDate}</span>
                  </div>
                </div>
              </div>
            )}
            {selectedTimePeriod === 'By Month' && (
              <div>
                <div className="row">
                  <div className="offset-sm-2 col-sm-5">
                    <label className="col-form-label">{RA_STR.year}</label><span className="date">{selectedyear}</span>
                    <label className="col-form-label">{RA_STR.month} </label><span className="date">{selectedMonth}</span>
                  </div>
                </div>
              </div>
            )}
            <div className="row">
              <table className='rule-effect-table'>
                <thead>
                  <tr className='main-header'>
                    <th className='col-head' >
                      {RA_STR.Tablehead1}
                    </th>
                    <th className='col-head' >
                      {RA_STR.Tablehead2}
                    </th>
                    <th className='col-head' >
                      {RA_STR.Tablehead3}
                    </th>
                    <th className='col-head' >
                      {RA_STR.Tablehead4}
                    </th>
                    <th className='col-head' >
                      {RA_STR.Tablehead5}
                    </th>
                  </tr>

                </thead>
                <tbody>
                  {tableRow}
                  {tabrow}
                </tbody>
              </table>
            </div>
          </div>
        )}
      </div>

    )
  }
}

export default AverageCaseLifeReport;
