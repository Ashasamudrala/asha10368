import React, { Component } from 'react';
import { serverUrl , RA_API_URL, RA_STR_CHECKBOX, RA_API_STATUS } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { orgData } from '../../shared/utlities/renderer';
import './enableDataFeedConfig.css';

class EnableDataFeedConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgDetails: '',
      getDataList: '',
      enableDatafeed:''
    }
  }
  handleChange = async (e) => {
    let dataFeed = this.state.getDataList.datafeed;
    if(e.target.type === RA_STR_CHECKBOX){
      this.setState({
        enableDatafeed: !this.state.enableDatafeed
      })
    }
    let params = {
      actionId: 5,
      category: dataFeed['category'],
      connectionConfig: dataFeed['connectionConfig'],
      dfendpoint: dataFeed['dfendpoint'],
      plugin: dataFeed['plugin'],
      protocol: dataFeed['protocol'],
      sortOrder: dataFeed['sortOrder'],
      enableDatafeed: e.target.checked,
      specificConfigMap: dataFeed.specificConfigMap
    }
    const saveDataFeed = {
      method: 'POST',
      url: `${serverUrl}/organization/${this.state.orgDetails.orgName}${RA_API_URL['datafeed']}`,
      data: params,
    };
    const saveDataFeedStatus = await getService(saveDataFeed);
    if (saveDataFeedStatus && saveDataFeedStatus.status === RA_API_STATUS['400']) {
      this.props.activateErrorList(true, saveDataFeedStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
    else {
      this.props.activateSuccessList(true,saveDataFeedStatus.data);
      this.props.activateErrorList(false,'');
      this.showErrorList();
    }
  }

  showErrorList = () => {
    window.scrollTo(0, 0);
  }

  componentDidMount = async () => {
    this.state.orgDetails = orgData('');
    const getDataFeed = {
      method: 'GET',
      url: `${serverUrl}/organization/${this.state.orgDetails.orgName}${RA_API_URL['datafeed']}${RA_API_URL['queryParams']}`
    };
    const getDataFeedStatus = await getService(getDataFeed);
    if (getDataFeedStatus && getDataFeedStatus.status === RA_API_STATUS['400']) {
      this.props.activateErrorList(true, getDataFeedStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
    else {
      this.setState({
        getDataList: getDataFeedStatus.data,
        enableDatafeed: getDataFeedStatus.data.datafeed.enableDatafeed
      })
      this.props.activateSuccessList(true,getDataFeedStatus.data);
      this.props.activateErrorList(false,'');  
      this.showErrorList();
    }
  }

  componentWillUnmount(){
    this.props.activateErrorList(false,'');
    this.props.activateSuccessList(false,'');
  }

  render() {
    const { getDataList } = this.state;
    return <div className="main">
      <div className="no-padding">
        <h2 className="title">Enable Data Feed Configuration</h2>
        <p className="desc">Enable the data feed configuration for the organization. Refresh the cache for the Risk Analytics Server changes to take effect. The Transaction DataFeed Application changes will take effect after 2 hours.</p>
        {getDataList ?
          <div className='row margin'>
            <div className="custom-control custom-checkbox ml-3">
              <input type="checkbox" className="custom-control-input"
                id="enableDatafeed" name="enableDatafeed"
                defaultValue={this.state.enableDatafeed}
                checked={this.state.enableDatafeed}
                onChange={this.handleChange} />
              <label className="custom-control-label" htmlFor="enableDatafeed"></label>
            </div>
            <label className="col-form-label">Enable data feed configuration</label>
          </div>
          : ''}
        <div className='summary'>Summary</div>
        {getDataList ?
          <div className='enable-data'>
            <div className="col-sm-6">
              <div className="form-group row">
                <label className="col-sm-6 col-form-label ml-6">Plugin</label>
                <div className="col-sm-6">
                  <input type="text" className="form-control"
                    name="plugin"
                    defaultValue={getDataList.datafeed.plugin} readOnly />
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group row">
                <label className="col-sm-6 col-form-label ml-6">Protocol</label>
                <div className="col-sm-6">
                  <input type="text" className="form-control"
                    name="protocol"
                    defaultValue={getDataList.datafeed.protocol} readOnly />
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group row">
                <label className="col-sm-6 col-form-label ml-6">Data Feed Endpoint</label>
                <div className="col-sm-6">
                  <input type="text" className="form-control"
                    name="dfendpoint"
                    defaultValue={getDataList.datafeed.dfendpoint} readOnly />
                </div>
              </div>
            </div>
            <div className='summary'>Endpoint Configuration</div>
            <div className="col-sm-6">
              <div className="form-group row">
                <label className="col-sm-6 col-form-label ml-6">Config Name</label>
                <div className="col-sm-6">
                  <input type="text" className="form-control"
                    name="dfendpoint"
                    defaultValue={getDataList.dfEpConfigName} readOnly />
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group row">
                <label className="col-sm-6 col-form-label ml-6">Connection Configuration</label>
                <div className="col-sm-6">
                  <input type="text" className="form-control"
                    name="dfendpoint"
                    defaultValue={getDataList.datafeed.connectionConfig} readOnly />
                </div>
              </div>
            </div>
            <div className='mrg-tp'>
              <div className='row no-margin'>
                {
                  Object.keys(getDataList.datafeed.specificConfigMap).map((obj, i) => {
                    return (
                      <div className="col-sm-6" key={i}>
                        <div className="form-group row">
                          <label className="col-sm-6 col-form-label ml-6">{obj}</label>
                          <div className="col-sm-6">
                            <input type="text" className="form-control"
                              name="dfendpoint"
                              defaultValue={getDataList.datafeed.specificConfigMap[obj]} readOnly />
                          </div>
                        </div>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </div>
          : ''}
      </div>

    </div >;
  }
}

export default EnableDataFeedConfig;
