import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl, RA_API_URL, RA_STR_SELECT, RA_API_STATUS } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
import DateRange from '../../shared/components/DateRange/DateRange';
import { saveAs } from 'file-saver';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';

class RiskSummaryReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultOrganizationName: RA_STR_SELECT,
      organizationNames: [],
      defaultChannel: 'All',
      channelList: [],
      orgDetails: false,
      dateRangeOptions: [],
      columns: [
        { title: 'Channel', field: 'channelDisplayKey' },
        { title: 'Allow', field: 'allowCount' },
        { title: 'Increase Authentication', field: 'incAuthCount' },
        { title: 'Alert', field: 'alertCount' },
        { title: 'Deny', field: 'denyCount' },
        { title: 'Total', field: 'totalCount' }
      ],
      data: {},
      columnsSecond: [
        { title: 'Channel', field: 'channelDisplayKey' },
        { title: 'Success', field: 'successCount' },
        { title: 'Failure', field: 'failureCount' },
        { title: 'Undetermined', field: 'unknownCount' },
        { title: 'Total', field: 'totalCount' }
      ]
    };
  }

  componentDidMount() {
    this.getAllOrganizations();
  }

  // Get Organisation Details
  getAllOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };

  // On select of Org Name enabling Channels Dropdown
  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find(element => {
        if (element.content === value) return element.key;
      });
      this.setState({
        defaultOrganizationName: orgId.key,
        orgDetails: true
      });
      this.getChannels(orgId.key);
    }
  };

  //Getting Channels
  getChannels = async selectedOrg => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['common']}/${selectedOrg}${RA_API_URL['channels']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      let channelList = [];
      if (response.data.length > 1) {
        channelList = [
          {
            content: 'All Channels',
            key: 'All'
          }
        ];
      }
      response.data.forEach(obj => {
        channelList.push({
          content: obj.displayName,
          key: obj.name
        });
      });
      this.setState({ channelList: channelList });
    }
  };

  // Setting Default Channel
  onChannelSelect = event => {
    const channel = event.target.value;
    this.setState({ defaultChannel: channel });
  };

  displayReport = async () => {
    const getDateItems = this.refs.daterange.getDates();
    this.setState({ fromDate: getDateItems[0].startDate, toDate: getDateItems[0].endDate });

    const displayData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['riskSummaryDisplay']}`,
      data: {
        organization: this.state.defaultOrganizationName,
        channel: this.state.defaultChannel,
        fromDate: getDateItems[0].startDate,
        toDate: getDateItems[0].endDate
      }
    };
    const response = await getService(displayData);
    if (response && response.status === RA_API_STATUS['200']) {
      this.props.activateErrorList(false, '');
      this.setState({
        showTable: true,
        apiData: response.data.adviceSummaryList,
        apiDataSecond: response.data.secAuthStatusList
      });
    } else {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
      this.setState({ showTable: false });
      window.scrollTo(0, 0);
    }
  };

  // Redirect to new Page
  newReport = () => {
    this.setState({
      defaultOrganizationName: this.state.defaultOrganizationName ,
      showTable: false,
      orgDetails: true,
      channel: this.state.defaultChannel,
    });
  };

  // Download Report
  downloadCSV(response) {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Advice_Summary_Report_' + this.state.defaultOrganizationName + '.csv');
    }
  }

  // For Export
  reportExport = async () => {
    const reportExpo = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['riskSummaryExport']}`,
      data: {
        channel: this.state.defaultChannel,
        fromDate: this.state.fromDate,
        toDate: this.state.toDate,
        organization: this.state.defaultOrganizationName
      }
    };
    const exportSuccessData = await getService(reportExpo);
    if (exportSuccessData && exportSuccessData.status === RA_API_STATUS['200']) {
      this.downloadCSV(exportSuccessData);
    } else if (
      exportSuccessData &&
      exportSuccessData.status === RA_API_STATUS['400'] &&
      exportSuccessData.data.errorList
    ) {
      this.props.activateErrorList(true, exportSuccessData.data.errorList);
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  };
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }


  render() {
    let { dateRangeOptions } = this.state;
    const {
      organizationNames,
      defaultOrganizationName,
      channelList,
      defaultChannel,
      channnels,
      fromDate,
      toDate,
      columns,
      apiData,
      columnsSecond,
      apiDataSecond
    } = this.state;
    return (
      <div className='main'>
        {!this.state.showTable ? (
          <div>
            <h2 className='title'>{RA_STR.riskSummaryTitle}</h2>
            <p className='desc'>{RA_STR.riskCriteria}</p>
            <hr />
            <div className='col-sm-6'>
              <div className='form-group dynamic-form-select'>
                <div className='form-group row'>
                  <label className='col-sm-4 col-form-label'>{RA_STR.orgzName}</label>
                  <div className='col-sm-8'>
                    <AutoSuggest
                      orgOptions={organizationNames}
                      getSelectedOrgDetails={this.onOrgSelect}
                      enableAutosuggest={true}
                      defaultOrganizationPlaceholder={defaultOrganizationName}
                    />
                  </div>
                </div>
              </div>
            </div>
            {this.state.orgDetails ? (
              <div>
                <div className='col-sm-6'>
                  <Select
                    name={'channel'}
                    title={RA_STR.channel}
                    required={false}
                    selectedOption={defaultChannel}
                    options={channelList ? channelList : ['']}
                    controlFunc={this.onChannelSelect}
                  />
                </div>

                <div className='row'>
                  <div className='col-sm-3'>
                    <label className='col-sm-8 col-form-label mt-3'> {RA_STR.timePeriod} </label>
                  </div>
                  <DateRange ref='daterange'></DateRange>
                </div>

                <div className='form-group form-submit-button row'>
                  <input
                    className='secondary-btn'
                    type='submit'
                    value='DISPLAY REPORT'
                    onClick={this.displayReport}
                  ></input>
                </div>
              </div>
            ) : (
              <React.Fragment />
            )}
          </div>
        ) : (
          <React.Fragment />
        )}
        {this.state.showTable ? (
          <div>
            <div>
              <h2 className='title'>{RA_STR.riskSummaryTitle}</h2>
              <div className='form-group row ReportButtons'>
                <p className='col-sm-8 desc'>{RA_STR.riskExportDesc}</p>
                <input className='secondary-btn' type='submit' value='EXPORT' onClick={this.reportExport}></input>
                <input
                  className='secondary-btn'
                  id='searchButton'
                  type='submit'
                  value='NEW REPORT'
                  onClick={this.newReport}
                ></input>
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='form-group row'>
                <label className='col-sm-4 col-form-label'>{RA_STR.orgzName}</label>
                <div className='col-sm-8 col-form-label'>{defaultOrganizationName}</div>
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='form-group row'>
                <div className='col-sm-4 col-form-label'>
                  {RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL} {fromDate}
                </div>
                <div className='col-sm-8 col-form-label'>
                  {RA_STR.EXCEPTION_USER_REPORT.TO_LABEL} {toDate}
                </div>
              </div>
            </div>
            <div style={{ maxWidth: '100%', clear: 'both' }}>
              <DynamicTable columns={columns} data={apiData} enablePagination={false} selection={false} />
            </div>
            <div className='mt-5'>
              <h2 className='title'>{RA_STR.riskResults}</h2>
              <p className='col-sm-11 desc'>{RA_STR.riskResultsDesc}</p>
              <div style={{ maxWidth: '100%', clear: 'both' }}>
                <DynamicTable columns={columnsSecond} data={apiDataSecond} enablePagination={false} selection={false} />
              </div>
            </div>
          </div>
        ) : (
          <React.Fragment />
        )}
      </div>
    );
  }
}

export default RiskSummaryReport;
