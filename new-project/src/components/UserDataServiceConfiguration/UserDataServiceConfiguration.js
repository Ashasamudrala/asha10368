import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { serverUrl, RA_API_URL,RA_API_STATUS } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import SuccessList from '../../shared/components/SuccessList/SuccessList';
import {RA_STR} from '../../shared/utlities/messages';

class UserDataServiceConfiguration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            udsOptions:[],
            count:'',
            interval:'',
            period:'',
            errorMessage:'',
            successMessage:'',
            lConInSize:'',
            lConMaxSize:'',
            lConPrSize:'',
            lConTimeOut:'',
            cacheInterval:false,
            refresh:'',
            udsData:'',
          };
          this.handleInputChange = this.handleInputChange.bind(this);
    }
    componentDidMount() {
        this.getUdsData();
    }
    handleInputChange = (e) =>{
        this.setState({
          [e.target.name]: e.target.value
        });
      }
    getUdsData= async () => {
        const getUdsData = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['udsConfig']}`,
        }
        const udsSuccessData = await getService(getUdsData);
        if (udsSuccessData && udsSuccessData.status === 200) {
            let udsOptions=udsSuccessData.data.udsConfigDetails;

            this.setState({count:udsOptions.defaultSearchRetCount,
                           interval:udsOptions.tokenPurgeInterval,
                           period:udsOptions.authTokenValidityPeriod,
                           lConInSize:udsOptions.ldapConnPoolInitSize,
                           lConMaxSize:udsOptions.ldapConnPoolMaxSize,
                           lConPrSize:udsOptions.ldapConnPoolPreferredSize,
                           lConTimeOut:udsOptions.ldapConnPoolTimeout,
                           cacheInterval:udsOptions.cacheRefreshInterval,
                           refresh:udsOptions.autoCacheRefresh});
        } 
        // localStorage.setItem('count',this.state.count);
      }

    onSave = async() => {
                const udsStatus = {
                    method: 'PUT',
                    url: `${serverUrl}${RA_API_URL['udsConfig']}`,
                    data: {
                     authTokenValidityPeriod: this.state.period,
                     autoCacheRefresh: this.state.refresh,
                     cacheRefreshInterval: this.state.cacheInterval,
                     defaultSearchRetCount: this.state.count,
                     ldapConnPoolInitSize: this.state.lConInSize,
                     ldapConnPoolMaxSize: this.state.lConMaxSize,
                     ldapConnPoolPreferredSize: this.state.lConPrSize,
                     ldapConnPoolTimeout: this.state.lConTimeOut,
                     tokenPurgeInterval: this.state.interval
                    },
                }
            const udsUpdate = await getService(udsStatus);
            if(udsUpdate && udsUpdate.status === RA_API_STATUS['200']){
                this.props.activateSuccessList(true, udsUpdate.data);
                this.props.activateErrorList(false, '');
                this.showErrorList();
              }else{
                this.props.activateErrorList(true, udsUpdate.data.errorList);
                this.props.activateSuccessList(false, '');
                this.showErrorList();
              }
}
showErrorList = () => {
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
    render() {
        const { 
            count,
            interval,
            period,
            lConInSize,
            lConMaxSize,
            lConPrSize,
            lConTimeOut,
            udsData,
            cacheInterval,
            refresh
                    } = this.state;
        return (
            <div className="main">
               <ErrorList validationDataList={this.state.errorMessage} />
               <SuccessList validationData={this.state.successMessage} />

        <h2 className="title">{RA_STR.userData}</h2>
                <p className="desc">{RA_STR.configureData}<br />
                    <b>{RA_STR.note}</b>{RA_STR.refresh}</p>
                <span className="ecc-h1">{RA_STR.searchConfig}</span>
                <div className="col-sm-8">
                            <SingleInput
                                title={RA_STR.maximumSearch}
                                inputType={'text'}
                                name={'count'}
                                required={'true'}
                                controlFunc={this.handleInputChange}
                                content = {count} 
                                />
                </div>
                <div className="div-seperator">
                    <span className="ecc-h1">{RA_STR.ldapConfig}</span>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">{RA_STR.ldapInitialSize}</label>
                            <div className="col-sm-3">
                            {lConInSize ? lConInSize :'NA'}
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">{RA_STR.ldapMaxSize}</label>
                            <div className="col-sm-3">
                            {lConMaxSize ? lConMaxSize :'NA'}
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">{RA_STR.ldapPreferSize}</label>
                            <div className="col-sm-3">
                            {lConPrSize ? lConPrSize :'NA'}
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">{RA_STR.ldapTimeout}</label>
                            <div className="col-sm-3">
                            {lConTimeOut ? lConTimeOut :'NA'}
                        </div>
                        </div>
                    </div>
                </div>
        <span className="ecc-h1">{RA_STR.auth} <br />{RA_STR.tokenValidity}</span>
                <div className="div-seperator">
                    <div className="col-sm-8">
                                <SingleInput
                                    title={RA_STR.purgeInterval}
                                    inputType={'text'}
                                    name={'interval'}
                                    required={'true'}
                                    controlFunc={this.handleInputChange} 
                                    content = {interval} />
                    </div>
                    <div className="col-sm-8">
                                <SingleInput
                                    title={RA_STR.validity}
                                    inputType={'text'}
                                    name={'period'}
                                    required={'true'}
                                    controlFunc={this.handleInputChange} 
                                    content = {period}/>
                    </div>
                </div>
                <input className="secondary-btn" id="manageTokenButton" type="submit" value="SAVE" onClick={this.onSave}></input>
            </div>
        );
    }
}

export default UserDataServiceConfiguration;