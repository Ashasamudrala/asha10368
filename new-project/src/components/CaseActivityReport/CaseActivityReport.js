import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import { RA_STR } from '../../shared/utlities/messages';
import { getService } from '../../shared/utlities/RestAPI';
import {
  serverUrl, RA_API_URL, RA_API_STATUS
} from '../../shared/utlities/constants';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import "react-datepicker/dist/react-datepicker.css";
import DateRange from '../../shared/components/DateRange/DateRange';
import './CaseActivityReport.css';
import _ from 'underscore';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import FileSaver from 'file-saver';
var moment = require('moment');

class CaseActivityReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgList: [],
      timePeriod: [],
      orgName: '',
      caseActForm: {},
      timePeriodValue: 'By Month',
      currentYear: [],
      currentMonth: [],
      getMonths: [],
      apiData: [],
      parentChildData: [],
      selectedActivity: {},
      dateRange: '',
      year: new Date().getFullYear(),
      displayTable: false,
      showDateRange: false,
      showTimePeriod: true,
      totalCases: '',
      getDateItems: [],
      drillDownQueueId: null,
      index: 0,
      columns: [
        {
          title: 'Cases Handled Through', field: 'queueName', render: (rowData) =>
            <span>{rowData.queueName}</span>
        },
        {
          title: 'Period', field: 'period', render: (rowData) =>
            <div>
              <span>{rowData.period}</span>
              {rowData.queueId !== 0 ?
                <img src="images/dropdown.png" alt="period" onClick={this.reportDrillDown.bind(this, rowData)} />
                : ''}
            </div>
        },
        {
          title: 'Cases Available', field: 'casesAvailable', render: (rowData) =>
            <span>{rowData.casesAvailable}</span>
        },
        {
          title: 'Cases Opened by CSR', field: 'casesOpenIch', render: (rowData) =>
            <span>{rowData.casesOpenIch}</span>
        },
        {
          title: 'Cases Closed', field: 'casesClosedIch', render: (rowData) =>
            <span>{rowData.casesClosedIch}</span>
        },
        {
          title: 'Case Activity Count', field: 'activityCountIch', render: (rowData) =>
            <span>{rowData.activityCountIch}</span>
        },
      ]
    }
  }

  showErrorList = () => {
    window.scrollTo(0, 0);
  }

  componentDidMount = async () => {
    this.dateSetting();
    this.setState({
      currentYear: moment().format('YYYY'),
      currentMonth: moment().format('MMMM'),
    })
    let months = _.rest(moment.months(), [6]);
    this.setState({
      getMonths: months
    })
    const getOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`,
    };
    const getOrgStatus = await getService(getOrg);
    if (getOrgStatus && getOrgStatus.status === RA_API_STATUS['200']) {
      let objectValues = Object.keys(getOrgStatus.data.organizations);
      this.setState({
        orgList: objectValues
      });
    }
    const getTimePeriods = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getTimePeriods']}`,
    };
    const getTimePeriodStatus = await getService(getTimePeriods);
    if (getTimePeriodStatus && getTimePeriodStatus.status === RA_API_STATUS['200']) {
      let objectValues = Object.values(getTimePeriodStatus.data.timePeriod);
      for (let i = 0; i < objectValues.length; i++) {
        this.state.timePeriod.push({
          'key': objectValues[i],
          'content': objectValues[i]
        })
        this.setState({
          timePeriod: this.state.timePeriod
        });
      }
    }
  }

  getSelectedOrgDetails = (getSelectedOrgDetails) => {
    this.setState({
      orgName: getSelectedOrgDetails
    })
  }

  handleChange = (e) => {
    const caseActForm = this.state.caseActForm;
    const { monthyears } = this.state;
    const { name, value } = e.target;
    var fieldValue;
    if (value === 'By Month') {
      this.setState({
        showTimePeriod: true,
        showDateRange: false
      })
    }
    else if (value === 'By Date Range') {
      this.setState({
        showDateRange: true,
        showTimePeriod: false
      })
    }
    else if (value === 'Yesterday' || value === 'Last 7 Days') {
      this.setState({
        showDateRange: false,
        showTimePeriod: false
      })
    }
    else if (name === 'year') {
      this.setState({ index: e.target.selectedIndex, month: this.state.monthyears[e.target.selectedIndex][0].key })
    }
    else if (name == 'month') {
      this.setState({ month: value })
    }
    fieldValue = value;
    caseActForm[name] = fieldValue;
    this.setState({
      timePeriodValue: value,
      currentMonth: value,
      caseActForm
    })
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }


 dateSetting = () => {
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let monthsdata = [];
    let monthyears = [];
    let years = [];

    [1, 2, 3, 4, 5, 6].map((data) => {
      var d = new Date();
      d.setMonth((d.getMonth() + data) - 6);
      monthsdata.push({ 'content': months[d.getMonth()], 'key': months.indexOf(months[d.getMonth()]) })
    });
    var d = new Date();
    if (d.getMonth() < 5) {
      years.push(d.getFullYear())
      years.push(d.getFullYear() - 1);
    }
    else {
      years.push(d.getFullYear());
    }
    if(d.getMonth() < 5) {
    switch (d.getMonth()) {
      case 0:
        monthyears.push([monthsdata.pop()]);
        monthyears.push(monthsdata.slice(0, 5));
        break;
      case 1:
        monthyears.push(monthsdata.slice(4));
        monthyears.push(monthsdata.slice(0, 4));
        break;
      case 2:
        monthyears.push(monthsdata.slice(3));
        monthyears.push(monthsdata.slice(0, 3));
        break;
      case 3:
        monthyears.push(monthsdata.slice(2));
        monthyears.push(monthsdata.slice(0, 2));
        break;
      case 4:
        monthyears.push(monthsdata.slice(1));
        monthyears.push([monthsdata[0]]);
        break;
    }
  }
    if (d.getMonth() >= 5) {
      monthyears.push(monthsdata)
    }
    this.setState({ monthyears: monthyears, years: years, month: monthyears[0][0].key })
  }
  displayReport = async () => {
    let getDateItems = '', dateRangeParams = {};
    if (this.state.showDateRange === true) {
      getDateItems = this.refs.daterange.getDates();
      dateRangeParams = {
        dateRange: this.state.dateRange || getDateItems[0].getSelectedText,
        fromDate: getDateItems[0].startDate,
        toDate: getDateItems[0].endDate,
        orgName: this.state.orgName,
        year: this.state.year,
        timePeriod: 'By Month',
        ...this.state.caseActForm,
        month: parseInt(this.state.caseActForm['month']) - 1 ? parseInt(this.state.caseActForm['month']) - 1 : parseInt(moment().month(this.state.currentMonth).format('MM')) - 1 || parseInt(this.state.caseActForm['month'])
      }
    } else {
      getDateItems = this.state.getDateItems;
      dateRangeParams = {
        orgName: this.state.orgName,
        year: this.state.year,
        dateRange: this.state.dateRange,
        timePeriod: 'By Month',
        ...this.state.caseActForm,
        month: this.state.month
      }
    }
    const displayRep = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['displayReport']}`,
      data: dateRangeParams
    };
    const displayRepStatus = await getService(displayRep);
    if (displayRepStatus && displayRepStatus.status === RA_API_STATUS['200']) {
      this.setState({
        apiData: displayRepStatus.data.activityReports,
        selectedActivity: displayRepStatus.data.caseMgmtActivityRprtRqstBody,
        totalCases: displayRepStatus.data.totalCases,
        displayTable: true
      })
      this.props.activateSuccessList(true, displayRepStatus.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
    }
    else {
      this.props.activateErrorList(true, displayRepStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  }

  reportDrillDown = async (getData, e) => {
    this.setState({
      drillDownQueueId: getData.queueId
    })
    let params = {};
    if (getData.queueId === -1 || getData.queueId === null) {
      params = {
        queueDrillDown: 0,
        ichDrillDown: 1
      }
    }
    else {
      params = {
        queueDrillDown: 1,
        ichDrillDown: 0
      }
    }
    const repDrillData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['reportDataDrillDown']}`,
      data: {
        ...params,
        queueId: getData.queueId === null ? -1 : getData.queueId,
        timePeriod: this.state.selectedActivity['timePeriod'],
        orgName: this.state.selectedActivity['orgName'],
        year: this.state.selectedActivity['year'],
        month: this.state.selectedActivity['month']
      }
    };
    const repDrillDataStatus = await getService(repDrillData);
    if (repDrillDataStatus && repDrillDataStatus.status === RA_API_STATUS['200']) {
      this.state.apiData.splice([getData.tableData.id + 1], 0, ...repDrillDataStatus.data.activityReports)
      this.props.activateSuccessList(true, repDrillDataStatus.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
    }
    else {
      this.props.activateErrorList(true, repDrillDataStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  }

  newReport = () => {
    this.setState({
      displayTable: false
    })
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  export = async (getData, e) => {
    let params = {};
    if (getData.queueId === -1 || getData.queueId === null) {
      params = {
        queueDrillDown: 0,
        ichDrillDown: 1
      }
    }
    else {
      params = {
        queueDrillDown: 1,
        ichDrillDown: 0
      }
    }
    const exportFile = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['exportReportData']}`,
      data: {
        ...params,
        queueId: this.state.drillDownQueueId,
        timePeriod: this.state.selectedActivity['timePeriod'],
        orgName: this.state.selectedActivity['orgName'],
        year: this.state.selectedActivity['year'],
        month: this.state.selectedActivity['month'],
        dateRange: ''
      }
    };
    const exportFileStatus = await getService(exportFile);
    if (exportFileStatus && exportFileStatus.status === 400) {
      this.props.activateErrorList(true, exportFileStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
    else {
      this.setState({
        csvData: exportFileStatus.data,
      })
      var blob = new Blob([this.state.csvData], { type: "text/plain;charset=utf-8" });
      FileSaver.saveAs(blob, `Case_Activity_Report_${getData.orgName}.csv`);
      this.props.activateErrorList(true, exportFileStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  }

  render() {
    const { timePeriod, orgList, showTimePeriod, currentYear, selectedActivity, showDateRange, apiData, displayTable, totalCases, index, monthyears, years } = this.state;
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.caseActivityTitle}</h2>
        {displayTable === false ?
          <div>
            <p className='desc'>{RA_STR.caseActivityText}</p>
            <hr />
            <div className='col-sm-8'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Select Organization</label>
                <div className='col-sm-8'>
                  <AutoSuggest orgOptions={orgList} enableAutosuggest={true}
                    getSelectedOrgDetails={this.getSelectedOrgDetails} />
                </div>
              </div>
              <Select
                name={'timePeriod'}
                title={'Time Period'}
                options={timePeriod ? timePeriod : ['']}
                controlFunc={this.handleChange} />
            </div>
            {showDateRange === true ?
              <div className='col-sm-12'>
                <div className="form-group row case-report-range">
                  <label className="col-sm-3 col-form-label"></label>
                  <DateRange ref="daterange" />
                </div>
              </div>
              : ''
            }
            <div className='col-sm-8'>
              {showTimePeriod === true ?
                <div className="form-group dynamic-form-select">
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label"></label>
                    <div className='col-sm-8 row'>
                      <label className="col-form-label ml-3">year</label>
                      <div className="col-sm-3 ">
                        {currentYear ?
                          <select className='form-control' name='year'
                            onChange={this.handleChange}
                            style={{ 'background': `url(${process.env.REACT_APP_ROUTER_BASE || ''}/images/dropdown.png) no-repeat 97% 50%` }}>
                            {
                              years && years.map((year, i) => {
                                return (
                                  <option key={i} value={year}>{year}</option>
                                )
                              })
                            }

                          </select>
                          : ''}
                      </div>
                      <label className="col-form-label ">Month</label>
                      <div className='col-sm-3 row ml-1 form-group'>
                        <select className='form-control'
                          name='month'
                          style={{ 'background': `url(${process.env.REACT_APP_ROUTER_BASE || ''}/images/dropdown.png) no-repeat 97% 50%` }}
                          onChange={this.handleChange}
                        >
                          {
                            monthyears && monthyears[index].map((month, i) => {
                              return (
                                <option key={i} value={month.key}>{month.content}</option>
                              )
                            })
                          }
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                : ''}
              <div className="form-group form-submit-button">
                <input className="secondary-btn" type="button" value="DISPLAY REPORT" onClick={this.displayReport} />
              </div>
            </div>
          </div>
          :
          <div>
            <div className='row'>
              <div className='col-sm-7'>
                <p className='desc'>{RA_STR.caseReportData}</p>
              </div>
              <div className='col-sm-5 text-right'>
                <div className="form-group form-submit-button">
                  <input className="secondary-btn" type="button" value="EXPORT" onClick={this.export.bind(this, selectedActivity)} />
                  <input className="secondary-btn ml-3" type="button" value="NEW REPORT" onClick={this.newReport} />
                </div>
              </div>
            </div>
            <div className='col-sm-12'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Organization Name</label>
                <div className='col-sm-8'>
                  <label className="col-form-label">{selectedActivity.orgName}</label>
                </div>
              </div>
            </div>
            <div className='col-sm-12'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Time Period</label>
                <div className='col-sm-8'>
                  <label className="col-form-label">{selectedActivity.timePeriod}</label>
                </div>
              </div>
            </div>
            <div className='col-sm-12'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label"></label>
                <div className='col-sm-8'>
                  <label className="col-form-label">Year: {selectedActivity.year}</label>
                  <label className="col-form-label ml-3">Month: {selectedActivity.month}</label>
                </div>
              </div>
            </div>
            <div className='col-sm-12'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Total Cases Generated:</label>
                <div className='col-sm-8'>
                  <label className="col-form-label">{totalCases}</label>
                </div>
              </div>
            </div>
            <DynamicTable columns={this.state.columns} data={apiData}
              enablePagination={false} selection={false} />
          </div>
        }
      </div >
    )
  }
}

export default CaseActivityReport;