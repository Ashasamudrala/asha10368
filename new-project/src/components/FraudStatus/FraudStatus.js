import React, { Component } from 'react';
import { serverUrl, RA_STR_SELECT, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import DateTimeRange from '../../shared/components/DateTimeRange/DateTimeRange';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';

class FraudStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNo: 0,
      pageSize: 10,
      activePageNum: 1,
      totalCount: 100,
      orgName: '',
      orgOptions: [],
      enableOrg: true,
      showTable: false,
      showDates: false,
      enableDetailTable: false,
      columns: [
        { title: RA_STR.fraudReviewer, field: 'reviewer' },
        { title: RA_STR.fraudOrg, field: 'organization' },
        { title: RA_STR.fraudAlert, field: 'totalAlertsHandled' },
        { title: RA_STR.fraudMark, field: 'markedAsUndetermined' },
        { title: RA_STR.fraudTotal, field: 'markedAsTotalFraud' },
        { title: RA_STR.fraudGenuine, field: 'markedAsConfirmedGenuine' },
        { title: RA_STR.fraudAssumed, field: 'markedAsAssumedGenuine' },
        { title: RA_STR.fraudUnique, field: 'uniqueTxnsHandled' },
        { title: RA_STR.fraudCases, field: 'uniqueCasesHandled' },
        { title: RA_STR.confirmPer, field: 'confirmedPercentage' },
        { title: RA_STR.assumePer, field: 'assumedPercentage' },
        { title: RA_STR.nonContactable, field: 'nonContactablePercentage' }
      ],
      detailTable: [
        { title: RA_STR.fraudReviewer, field: 'reviewer' },
        { title: RA_STR.fraudOrg, field: 'organization' },
        { title: RA_STR.txId, field: 'txId' },
        { title: RA_STR.fraudStatus, field: 'fraudStatus' },
        { title: RA_STR.fraudType, field: 'fraudType' },
        { title: RA_STR.caseId, field: 'caseId' },
        { title: RA_STR.dateTime, field: 'dateTime' }
      ],
      displayList: [],
      displayDetailList: []
    }
  }
  componentDidMount = async () => {
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      this.setState({ orgOptions: orgOptions });
    }
  }
  getSelectedOrgDetails = (getSelectedOrgDetails,method) => {
    this.setState({
      orgName: getSelectedOrgDetails,
      showDates: (method === 'click') || (method === 'down') ? true : false
    })
  }

  displayReport = async () => {
    var getDateItems = this.refs.datetimerangelist.getDatesTimes();
    if (getDateItems[0].startDate <= getDateItems[0].endDate) {
      const displayReport = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['getReviewerEfficiencyFraudStatusReport']}`,
        data: {
          "fromDate": `${getDateItems[0].startDate} ${getDateItems[0].startHours}:${getDateItems[0].startMins}:00`,
          "orgName": this.state.orgName ? this.state.orgName : '',
          "toDate": `${getDateItems[0].endDate} ${getDateItems[0].endHours}:${getDateItems[0].endMins}:00`
        }
      }
      const displayReportStatus = await getService(displayReport);
      if (displayReportStatus.status === 200) {
        this.setState({ displayList: displayReportStatus.data, showTable: true });
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, displayReportStatus.data.errorList);
      }
    } else {
      alert(RA_STR.ruleAlert);
    }
  }
  downloadCSV = (response) => {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Reviewer Efficiency Report (Fraud Status).csv');
    }
  }
  exportData = async () => {
    const { displayList,enableDetailTable } = this.state;
    let apiURL='';
    if(!enableDetailTable){
      apiURL=`${serverUrl}${RA_API_URL['exportReviewerEfficiencyFraudStatusReport']}`;
    }else{
      apiURL=`${serverUrl}${RA_API_URL['exportReviewerEfficiencyFraudStatusDetailedReport']}`;
    }
    const exportReport = {
      method: 'POST',
      url: apiURL,
      data: {
        "fromDate": displayList.fromDate,
        "orgName": displayList.orgName,
        "toDate": displayList.toDate
      }
    }
    const exportReportStatus = await getService(exportReport);
    if (exportReportStatus.status === 200) {
      this.downloadCSV(exportReportStatus);
    }
  }
  reset = () => {
    window.location.reload();
  }
  getActivePage = (getPageNum) => {
    let pageNumCount = getPageNum - 1;
    this.setState({
      pageNo: pageNumCount, activePageNum: getPageNum
    }, () => {
      this.detailReport();
    })
  }
  detailReport = async() => {
    const {displayList,pageNo,pageSize}=this.state;
    if (displayList.fromDate <= displayList.toDate) {
      const displayDetailReport = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['getReviewerEfficiencyFraudStatusDetailedReport']}`,
        data: {
          "fromDate": `${displayList.fromDate ? displayList.fromDate:''}`,
          "orgName": `${displayList.orgName ? displayList.orgName:''}`,
          "toDate": `${displayList.toDate ? displayList.toDate:''}`,
          "pageNo": pageNo,
          "pageSize": pageSize,
        }
      }
      const displayDetailReportStatus = await getService(displayDetailReport);
      if (displayDetailReportStatus.status === 200) {
        this.setState({ displayDetailList: displayDetailReportStatus.data, enableDetailTable: true,totalCount: displayDetailReportStatus.data.numberOfRecords});
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, displayDetailReportStatus.data.errorList);
      }
    } else {
      alert(RA_STR.ruleAlert);
    }
  }
  render() {
    const { orgOptions, enableOrg, showTable, displayList, columns, showDates, detailTable,displayDetailList,enableDetailTable,pageSize,totalCount,activePageNum } = this.state;
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.reviewerFraudTitle}</h2>
        <p className='desc'>{RA_STR.reviewerFraudBody}</p>
        <hr />
        {!showTable ?
          <div>
            <div className="col-md-6">
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}</label>
                <div className='col-sm-8'>
                  <AutoSuggest orgOptions={orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={enableOrg} defaultOrganizationPlaceholder={RA_STR_SELECT} />
                </div>
              </div>
            </div>
            {showDates ? <div><div className="col-md-12 row">
              <label className="col-sm-2 col-form-label">By Date Range</label>
              <DateTimeRange ref="datetimerangelist" startPrior={''} endPrior={''}></DateTimeRange>
            </div>
              <input className="secondary-btn" id="RESET" type="submit" value="Display Reports" onClick={this.displayReport}></input></div> : ''}
          </div> : <div>
            <div>
              <div className="col-md-12">
                <div className="float-right">
                  <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                  <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.reset}></input>
                  {!enableDetailTable ? <input className="secondary-btn ml-3" type="submit" value="DETAIL REPORT" onClick={this.detailReport}></input>:''}
                </div>
              </div>
              <div className="col-md-8">
                {!enableDetailTable ?
                  <div className="form-group form-inline">
                    <label className="col-sm-4 col-form-label">{RA_STR.noOfRecords}</label>
                    <div className="col-sm-8">
                      {displayList.numberOfRecords}
                    </div>
                  </div>
                  : ''}
                <div className="form-group form-inline">
                  <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}:</label>
                  <div className="col-sm-8">
                    {enableDetailTable ? displayDetailList.orgName : displayList.orgName}
                  </div>
                </div>
                <div className="form-group form-inline">
                  <label className="col-sm-4 col-form-label">{RA_STR.dateRange}:</label>
                  <div className="col-sm-8">
                  {enableDetailTable ? `From:${displayDetailList.fromDate} To:${displayDetailList.toDate}` : `From:${displayList.fromDate} To:${displayList.toDate}`}
                  </div>
                </div>
              </div>
            </div>
          </div>}
        {showTable ?
          <div className="tabele-buttons">
            <DynamicTable columns={enableDetailTable ? detailTable:columns } data={enableDetailTable ? displayDetailList.reportList  : displayList.reportList } enablePagination={enableDetailTable ? true :false} pageSize={pageSize} totalCount={totalCount} activePage={this.getActivePage} activePageNumNew={activePageNum}/></div> : ''}
      </div>
    )
  }
}

export default FraudStatus;
