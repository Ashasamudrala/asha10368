import React, { Component } from 'react';
import InlineDateRange from '../../shared/InlineDateRange/InlineDateRange';
import './InstanceManagementReport.scss'
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';
class InstanceManagementReport extends Component {
  constructor() {
    super();
    this.state = {
      selectedfromDate: '',
      selectedtoDate: '',
      displatReportStatus: false,
      eventsToDisplayList: [{ key: '0', content: 'All Events' }],
      selectedEvent: '',
      numberOfRecords: '',
      fromDate: '',
      toDate: '',
      orgData: '',
      pageNum: 0,
      activePageNum: 1,
      pageSize: 10,
      userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null,
      columns: [
        { title: 'Instance Name', field: 'instancename' },
        { title: 'Server Type', field: 'servertype' },
        { title: 'Activity Type', field: 'activitytype' },
        { title: 'Activity Time', field: 'activityTime' },
        { title: 'Instance Configuration', field: 'instanceConfig' },
        { title: 'Organizations Refreshed', field: 'orgsrefreshed' }
      ]
    }
  }
  componentDidMount = async () => {
  }
  eventChange = (event) => {
    const storecontactData = JSON.parse(JSON.stringify(this.state.orgData));
    const eventValue = event.target.value;
    const nameToFileter = this.state.eventsToDisplayList.find((element) => { if (element.key === eventValue) return element.key });
    if (eventValue != 0) {
      let filteredContacts = storecontactData.filter((eachContact) => {
        return eachContact.activitytype === nameToFileter.content
      })
      this.setState({
        reportList: filteredContacts
      });
    } else {
      this.setState({
        reportList: storecontactData
      });
    }
  }
  getActivePage = (getPageNum) => {
    let pageNumCount = getPageNum - 1;
    this.setState({
      pageNum: pageNumCount, selectedOrgNames: [], activePageNum: getPageNum
    }, () => {
      this.displayReport();
    })
  }
  downloadCSV(response) {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Instance_Management_Report.csv');
    }
  }
  exportReport = async () => {
    let data = {
      adminPreferredTimezone: this.state.userData.userProfile.timeZone,
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
    }
    const exportReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['ExportdisplayInstanceMangmentReport']}`,
      data: data
    }
    const exportSuccessData = await getService(exportReport);
    if (exportSuccessData && exportSuccessData.status === 200) {
      this.downloadCSV(exportSuccessData);
    } else {
      this.props.activateErrorList(true, exportSuccessData.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  newReport = () => {
    this.setState({
      displatReportStatus: false,
      eventsToDisplayList: [{ key: '0', content: 'All Events' }],
    });
  }
  displayReport = () => {
    this.setState({
      eventsToDisplayList: [{ key: '0', content: 'All Events' }],
    });
      const getDateItems = this.refs.daterange.getDates();
      this.setState({
        selectedfromDate: getDateItems[0].startDate,
        selectedtoDate: getDateItems[0].endDate,
      }, () => {
        let data = {
          adminPreferredTimezone: this.state.userData.userProfile.timeZone,
          fromDate: this.state.selectedfromDate,
          toDate: this.state.selectedtoDate,
          pageNo: this.state.pageNum,
          pageSize: this.state.pageSize
        }
        this.getTableData(data);
      });
  }
  getTableData = async (payload) => {

    const displayData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL.displayInstanceMangmentReport}`,
      data: payload
    }
    const response = await getService(displayData);
    if (response && response.status === 200) {
      let data = [...this.state.eventsToDisplayList, ...CA_Utils.objToArray(response.data.displayEvents, 'object')]
      this.setState({
        displatReportStatus: true,
        eventsToDisplayList: data,
        numberOfRecords: response.data.numberOfRecords,
        fromDate: response.data.fromDate,
        toDate: response.data.toDate,
        reportList: response.data.reportList,
        orgData: response.data.reportList
      })
    } else {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  // getDate=()=>{
  //   const getDateItems = this.refs.daterange.getDates();
  //   this.setState({
  //     selectedfromDate:getDateItems[0].startDate,
  //     selectedtoDate:getDateItems[0].endDate,
  //   })
  // }

  render() {
    const { eventsToDisplayList, selectedEvent, fromDate, toDate, reportList, columns } = this.state;
    return <div className="main instance-manage-report">
      {this.state.displatReportStatus === false && (
        <div>
          <div>
            <h2 className="title">{RA_STR.instanceTitle}</h2>
          </div>
          <div>
            <p className="desc">{RA_STR.instanceDesc}</p>
          </div>
          <hr />
          <div className="row">
            <div className="col-sm-12">
              <InlineDateRange ref="daterange"></InlineDateRange>
            </div>
          </div>
          <div className="display-report">
            <input className="secondary-btn" id="searchButton" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
          </div>
        </div>)}
      {this.state.displatReportStatus === true && (
        <div>
          <div className="row">
            <div className="col-sm-8">
              <div>
                <h2 className="title">{RA_STR.instanceTitle}</h2>
              </div>
              <div>
                <p className="desc">{RA_STR.instanceDisplayDec}</p>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="row">
                <div className=" offset-sm-3 col-sm-3">
                  <input className="secondary-btn" type="button" value="EXPORT" onClick={this.exportReport}></input>
                </div>
                <div className="col-sm-6">
                  <input className="secondary-btn" type="submit" value="NEWREPORT" onClick={this.newReport}></input>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-2">
              <label className="col-form-label">{RA_STR.noOfRecords}</label>
            </div>
            <div className="col-sm-1">
              <div className="pt-2">{this.state.numberOfRecords}</div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-2">
              <label className="col-form-label">{RA_STR.from_date}</label>
              <span className="ml-2">{this.state.fromDate}</span>
            </div>
            <div className="col-sm-2">
              <label className="col-form-label">{RA_STR.to_date}</label>
              <span className="ml-2">{this.state.toDate}</span>
            </div>
          </div>
          <div className="row">
            <div className='col-sm-8'>
              <div className=' col-sm-9'>
                <Select
                  name={'eventstodisplay'}
                  title={'Events to Display'}
                  required={false}
                  selectedOption={selectedEvent}
                  //  placeholder={'All Events'}
                  options={eventsToDisplayList ? eventsToDisplayList : ['']}
                  controlFunc={this.eventChange} />
              </div>
            </div>
          </div>
          <div>
            <div className="tabele-buttons">
              <DynamicTable columns={columns} data={reportList} enablePagination={true} pageSize={this.state.pageSize} totalCount={this.state.numberOfRecords} activePage={this.getActivePage} activePageNumNew={this.state.activePageNum} /></div>
          </div>
        </div>
      )}
    </div>;
  }
}

export default InstanceManagementReport;
