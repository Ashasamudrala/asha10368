import React, { Component } from 'react';
import CalloutConfig from '../CalloutConfig/CalloutConfig';

class ServerCalloutConfig extends Component {
    render() {
        return (
            <div>
                <CalloutConfig {...this.props} />
            </div>
        );
    }
}

export default ServerCalloutConfig;