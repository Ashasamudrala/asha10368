import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import TextArea from '../../shared/components/TextArea/TextArea';
import { RA_STR_CHECKBOX } from '../../shared/utlities/constants';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';
import { RA_STR } from '../../shared/utlities/messages';
import './CreateOrganization.css';

class CreateOrganization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionTrue: false,
      mfaEnabled: false,
      referGlobalKeyLabel: true,
      referGlobalConfig: true,
      userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null,
      dateTimeFormat: 'MM/dd/yyyy HH:mm:ss (z)',
      authMechanism: '',
      authMechanismSelected: '',
      selectedLocale: '',
      localeOptions: [],
      repositoryTypes: [],
      repositoryTypesSeleced: '',
      mfaMethodType: [],
      mfaMethodSelected: '',
      mfaTypes: [],
      mfaTypesSeleced: '',
      createOrgFields: {},
      orgIdleTimeOut: 60,
      customAttributes: [
        {
          id: '1',
          name: '',
          value: ''
        },
      ],
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  componentDidMount = async () => {
    let jwtToken = this.state.userData ? this.state.userData.token : null;
    const getAuthMechanism = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getAuthMechanisms']}`,
      headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' }
    };
    const getAuthMechanismStatus = await getService(getAuthMechanism);
    if (getAuthMechanismStatus && getAuthMechanismStatus.status === 200) {
      this.setState({
        authMechanism: getAuthMechanismStatus.data.types,
        authMechanismSelected: (Object.keys(JSON.parse(JSON.stringify(getAuthMechanismStatus.data.types))).sort())[0]
      })
    }
    const checkLocale = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['localeUrl']}`,
      headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' }
    };
    const getLocale = await getService(checkLocale);
    if (getLocale.status === 200) {
      let objectKeys = Object.keys(getLocale.data.locales);
      let objectValues = Object.values(getLocale.data.locales);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.localeOptions.push({
          'key': objectKeys[i],
          'content': objectValues[i]
        })
      }
      this.setState({ localeOptions: this.state.localeOptions, selectedLocale: objectKeys[0] });
    }
    const getMfaMethods = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getMfaMethods']}`,
      headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' }
    };
    const getMfaMethodsStatus = await getService(getMfaMethods);
    if (getMfaMethodsStatus.status === 200) {
      let objectKeys = Object.keys(getMfaMethodsStatus.data.mfaList);
      let objectValues = Object.values(getMfaMethodsStatus.data.mfaList);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.mfaMethodType.push({
          'key': objectValues[i],
          'content': objectValues[i]
        })
      }
      this.setState({ mfaMethodType: this.state.mfaMethodType, mfaMethodSelected: objectValues[0] });
    }
    const getRespositoryTypes = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getRepositoryTypes']}`,
      headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' }
    };
    const respositoryTypesStatus = await getService(getRespositoryTypes);
    if (respositoryTypesStatus.status === 200) {
      let objectKeys = Object.keys(respositoryTypesStatus.data.types);
      let objectValues = Object.values(respositoryTypesStatus.data.types);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.repositoryTypes.push({
          'key': objectKeys[i],
          'content': objectValues[i]
        })
      }
      this.setState({ repositoryTypes: this.state.repositoryTypes, repositoryTypesSeleced: objectKeys[0] });
    }

    const getMfaTypes = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getMfaTypes']}`,
      headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' }
    };
    const getMfaTypesStatus = await getService(getMfaTypes);
    if (getMfaTypesStatus.status === 200) {
      let objectKeys = Object.keys(getMfaTypesStatus.data.mfaList);
      let objectValues = Object.values(getMfaTypesStatus.data.mfaList);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.mfaTypes.push({
          'key': objectValues[i],
          'content': objectValues[i]
        })
      }
      this.setState({ mfaTypes: this.state.mfaTypes, mfaTypesSeleced: objectValues[0] });
    }
    if (this.props.location.state !== undefined) {
      if (this.props.location.state.orgName) {
        const getOrgStatus = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['status']}`,
          data: {
            "orgNameList": [
              this.props.location.state.orgName]
            ,
            "status": "Active"
          },
          headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' }
        };
        const getCreateDetails = await getService(getOrgStatus);
        if (getCreateDetails.status === 200) {
          this.props.activateSuccessList(true, getCreateDetails.data);
          this.props.activateErrorList(false, '');
        } else {
          this.props.activateSuccessList(false, '');
          this.props.activateErrorList(true, getCreateDetails.data.errorList);
        }
      }
    }
  }
  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var adminAttributes = this.state.customAttributes.slice();
    var newadminAttributes = adminAttributes.map(function (attribute) {
      for (var key in attribute) {
        if ((key === item.name || key === item.value) && attribute.id == item.id) {
          attribute[key] = item.value;
        }
      }
      return attribute;
    });
    this.setState({ customAttributes: newadminAttributes });

  }

  handleRowDel(product) {
    if (this.state.customAttributes.length === 1) {
      return
    }
    var index = this.state.customAttributes.indexOf(product);
    this.state.customAttributes.splice(index, 1);
    this.setState(this.state.customAttributes);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      name: '',
      value: ''
    }
    this.state.customAttributes.push(product);
    this.setState(this.state.customAttributes);
  }

  handleChange = (e) => {
    if (e.target.type === RA_STR_CHECKBOX) {
      this.state.createOrgFields[e.target.name] = e.target.checked;
      this.setState({ [e.target.name]: e.target.checked, createOrgFields: this.state.createOrgFields }
      );
    } else {
      if (e.target.name === 'emailDomains') {
        if ('emailDomains') {
          this.state.createOrgFields[e.target.name] = e.target.value;
        } else {
          this.state.createOrgFields[e.target.name] = null;
        }
      } else {
        this.state.createOrgFields[e.target.name] = e.target.value;
      }
      this.setState({ [e.target.name]: e.target.value, createOrgFields: this.state.createOrgFields })
    }
  }
  createOrg = async (e) => {
    const { description, displayName, orgName, emailDomains, key_label } = this.state.createOrgFields;
    const createPage = true;
    const { mfaEnabled, authMechanismSelected, selectedLocale, dateTimeFormat, mfaMethodSelected, repositoryTypesSeleced, mfaTypesSeleced, referGlobalKeyLabel, referGlobalConfig, orgIdleTimeOut } = this.state;
    let customaAtributes = {}
    for (let i = 0; i < this.state.customAttributes.length; i++) {
      customaAtributes[this.state.customAttributes[i].name] = this.state.customAttributes[i].value
    }
    let jwtToken = this.state.userData ? this.state.userData.token : null;
    const createOrganisation = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['createOrg']}`,
      headers: { 'Authorization': 'Bearer ' + jwtToken, 'Content-Type': 'application/json' },
      data: {
        "orgName": orgName !== undefined ? orgName : null,
        "description": description !== undefined ? description : null,
        "displayName": displayName !== undefined ? displayName : null,
        "authMechanismType": authMechanismSelected,
        "preferredLocale": selectedLocale,
        "preferredDateTimeFormat": dateTimeFormat,
        "orgIdleTimeout": orgIdleTimeOut !== undefined ? orgIdleTimeOut : null,
        "repoName": repositoryTypesSeleced,
        "keyLabel": key_label !== undefined ? key_label : null,
        "storage": "Software",
        "referGlobalKeyLabel": referGlobalKeyLabel,
        "referGlobalConfig": referGlobalConfig,
        "customaAtributes": customaAtributes,
        "mfaMethodType": mfaTypesSeleced,
        "mfaChannelType": mfaMethodSelected,
        "mfaEnabled": mfaEnabled,
        "emailDomains": mfaEnabled ? emailDomains : null
      }
    };
    const createOrgStatus = await getService(createOrganisation);
    if (createOrgStatus && createOrgStatus.status === 200) {
      const getConfigAttributes = {
        method: 'GET',
        url: `${serverUrl}/organization/${orgName}`
      };
      const orgStatus = await getService(getConfigAttributes);
      if (orgStatus && orgStatus.status === 200) {
        const match = this.props.match;
        this.props.history.push({
          pathname: `${match.path}${'/attribute-encryption'}`,
          state: {
            data: orgStatus.data.org,
            basePath: match.path,
            createPage: createPage
          }
        })
        this.props.activateErrorList(false, '');
      }
    } else if (createOrgStatus && createOrgStatus.status !== 200) {
      this.props.activateErrorList(true, createOrgStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  render() {
    return <div className={`${this.props.orgName ? '' : 'main'}`}><h2 className="title">  {RA_STR.createorgnisationtitle}</h2>
      <p className="desc">{RA_STR.createorgnisationsubheading1}</p>
      <span dangerouslySetInnerHTML={{ __html: RA_STR.createorgnisationsubheading2 }}></span>
      <div className="col-sm-12">
        <div className="div-seperator create-org-form">
          <span className="ecc-h1 row orgSectionLabels"> {RA_STR.organisationInfotitile}</span>
          <SingleInput
            maxLength = {64}
            title={'Organization Name'}
            inputType={'text'}
            required={true}
            name={'orgName'}
            maxLength={'64'}
            controlFunc={this.handleChange} />
          <SingleInput
            maxLength = {64}
            title={'Display Name'}
            required={true}
            inputType={'text'}
            name={'displayName'}
            maxLength={'128'}
            controlFunc={this.handleChange} />
          <SingleInput
            maxLength = {64}
            title={'Description'}
            inputType={'text'}
            name={'description'}
            maxLength={'64'}
            controlFunc={this.handleChange} />
          <div className="form-group dynamic-form-select">
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">{`${'Administrator Authentication Mechanism'}:`}{true ? <span>*</span> : ""}</label>
              <div className="col-sm-8">
                <select
                  name='authMechanismSelected'
                  defaultValue={this.authMechanismSelected}
                  onChange={this.handleChange}
                  className="form-select form-control">
                  {Object.keys(this.state.authMechanism).map((value) => (
                    <option value={value} key={value} >{this.state.authMechanism[value]}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="div-seperator create-org-form">
          <span className="ecc-h1 row orgSectionLabels">{RA_STR.organisationLabel}</span>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.globalKeyLabel}</label>
            <div className="col-sm-8">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" name='referGlobalKeyLabel' id="customCheck1" onChange={this.handleChange} checked={this.state.referGlobalKeyLabel} />
                <label className="custom-control-label" htmlFor="customCheck1"></label>
              </div>
            </div>
          </div>
          <SingleInput
            maxlength={128}
            title={'Key Label'}
            required={true}
            disabled={this.state.referGlobalKeyLabel}
            inputType={'text'}
            name={'key_label'}
            maxLength={'128'}
            controlFunc={this.handleChange} />
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.storageLabel}</label>
            <div className="col-sm-8 form-inline">
              <div className="custom-control custom-radio">
                <span className="form-label capitalize">
                  <input
                    className="form-radio custom-control-input"
                    id={'title'}
                    name='Software'
                    type='radio'
                    value="option1"
                    checked={true}
                    onChange={this.handleChange} /><label className="form-label custom-control-label" >{RA_STR.Software}</label>
                </span>
              </div>
              <div className="px-md-5">
                <div className="custom-control custom-radio">
                  <span className="form-label capitalize">
                    <input
                      className="form-radio custom-control-input"
                      id={'Hardware'}
                      name='Hardware'
                      type='radio'
                      disabled={true}
                      value="option2"
                      onChange={this.handleChange} /><label className="form-label custom-control-label" >{RA_STR.Hardware}</label>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="div-seperator create-org-form">
          <span className="ecc-h1 row orgSectionLabels">{RA_STR.localizationLabel}</span>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.globalConfigurationLabel}</label>
            <div className="col-sm-8">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" onChange={this.handleChange} id="customCheck2" name="referGlobalConfig" checked={this.state.referGlobalConfig} />
                <label className="custom-control-label" htmlFor="customCheck2"></label>
              </div>
            </div>
          </div>
          <SingleInput
            title={'Date Time Format'}
            content={this.state.dateTimeFormat}
            required={true}
            disabled={this.state.referGlobalConfig}
            inputType={'text'}
            name={'dateTimeFormat'}
            controlFunc={this.handleChange} />
          <Select
            name={'preffered'}
            value={this.state.selectedLocale}
            controlFunc={this.handleChange}
            title={'Preferred Locale'}
            required={true}
            options={this.state.localeOptions} />
        </div>

        <div className="div-seperator create-org-form">
          <span className="ecc-h1 row orgSectionLabels">{RA_STR.userDataLabel}</span>
          <Select
            name={'repositoryTypesSeleced'}
            title={'Repository Type'}
            controlFunc={this.handleChange}
            required={true}
            value={this.state.repositoryTypesSeleced}
            options={this.state.repositoryTypes}
          />
        </div>
        <div className="div-seperator create-org-form">
          <span className="ecc-h1 row orgSectionLabels">{RA_STR.sessionLabel}</span>
          <SingleInput
            title={'Idle Timeout (in seconds)'}
            required={true}
            inputType={'text'}
            content={this.state.orgIdleTimeOut}
            controlFunc={this.handleChange}
            name={'orgIdleTimeOut'} />
        </div>
        <div className="div-seperator multifactorlevel">
          <span className="ecc-h1 row orgSectionLabels">{RA_STR.multifactorLabel}</span>
          <div className="form-group row">
            <label className="col-form-label">{RA_STR.enablemultifactorLabel}</label>
            <div className="">
              <div className="custom-control custom-checkbox ml-2">
                <input type="checkbox" className="custom-control-input" id="customCheck3" name='mfaEnabled' onChange={this.handleChange} checked={this.state.mfaEnabled} />
                <label className="custom-control-label" htmlFor="customCheck3"></label>
              </div>
            </div>
            <div className={"mfa-label " + (this.state.mfaEnabled ? 'mfaEnabled' : 'mfaDisabled')} dangerouslySetInnerHTML={{ __html: RA_STR.organisationencryptionLabel }}>
            </div>
          </div>
        </div>
        <div className="div-seperator create-org-form">
          <TextArea
            title={'Accepted Domains for Email Factor (.com)'}
            resize={false}
            required={this.state.mfaEnabled}
            name={'emailDomains'}
            controlFunc={this.handleChange}
            placeholder={''} />
          <Select
            name={'Delivery-channel'}
            title={'Select the 2FA method'}
            required={this.state.mfaEnabled}
            controlFunc={this.handleChange}
            value={this.state.mfaTypesSeleced}
            options={this.state.mfaTypes} />
          <Select
            name={'mfaMethodSelected'}
            title={'Delivery-channel'}
            required={this.state.mfaEnabled}
            value={this.state.mfaMethodSelected}
            controlFunc={this.handleChange}
            options={this.state.mfaMethodType}
          />
        </div>
      </div>
      <div className="col-sm-5">
        <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)}
          onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)}
          products={this.state.customAttributes} />
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="NEXT" onClick={this.createOrg}></input>
      </div>
    </div>;
  }
}

export default CreateOrganization;
