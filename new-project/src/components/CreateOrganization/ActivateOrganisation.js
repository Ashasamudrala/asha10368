import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import './ActivateOrganisation.scss';

class ActivateOrganisation extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    moveConfigureAccountCustomeAttributes = () => {
        let resultValue = false;
        resultValue = window.confirm('Are you sure want to activate the organization?');
        if (resultValue) {
            this.props.history.push({
                pathname: `/org/createOrg`,
                state: { orgName: this.props.location.state.orgName }
            })
        }
    }

    render() {
        return (
            <div className='main activate-org'>
                <h2 className="title">{RA_STR.activateorganisationtitle}</h2>
                <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.activateorganisationdesc }}></p>
                <p className="organisation-title">{RA_STR.activateorganisationsubtitle}</p>
                <div className="form-group form-submit-button row mt-2 ml-2">
                    <input className="custom-secondary-btn" id="custom-secondary-btn" type="buton" value="CANCEL" onClick={this.moveConfigureAccountCustomeAttributes}></input>
                    <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="ENABLE" onClick={this.moveConfigureAccountCustomeAttributes}></input>
                </div>
            </div>
        );
    }
}

export default ActivateOrganisation;