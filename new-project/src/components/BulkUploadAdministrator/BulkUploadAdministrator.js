import React, { Component } from 'react';
import '../BulkUploadOrganisation/BulkUploadOrganisation.css';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import { saveAs } from 'file-saver';

class BulkUploadAdministrator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadFileBinaryData: '',
            format: ''
        }
    }

    downloadCSV(response) {
        if (response && response.data) {
            const blob = new Blob([response.data], { type: 'application/octet-stream' });
            saveAs(blob, 'Bulk_Upload_Administrator.csv');
        }
    }
    uploadFile = async () => {
        const { format } = this.state;
        const bulkUpload = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['exportAdministrations']}`,
            data: {
                "uploadFile": Object.values(this.state.uploadFileBinaryData),
                'format': format
            }
        }
        const bulkUploadSuccessData = await getService(bulkUpload);
        if (bulkUploadSuccessData && bulkUploadSuccessData.status === 200) {
            if (this.fileInput && this.fileInput.value !== null) {
                this.fileInput.value = "";
            }
            this.setState({ uploadFileBinaryData: '', format: '' })
            this.props.activateSuccessList(true, bulkUploadSuccessData.data);
            this.props.activateErrorList(false, '');
        } else if (bulkUploadSuccessData && bulkUploadSuccessData.status !== 200) {
            if (this.fileInput && this.fileInput.value !== null) {
                this.fileInput.value = "";
            }
            this.setState({ uploadFileBinaryData: '', format: '' })
            this.props.activateErrorList(true, bulkUploadSuccessData.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    handleChange = (e) => {
        let reader = new FileReader();
        let file = e.target.files[0];
        if (file) {
            const lastDot = file.name.lastIndexOf('.');
            const fileName = file.name.substring(lastDot + 1);
            reader.onload = () => {
                const intArray = new Int8Array(reader.result);
                this.setState({
                    uploadFileBinaryData: intArray,
                    format: fileName
                })
            }
            reader.readAsArrayBuffer(file);
        } else {
            this.setState({
                uploadFileBinaryData: '',
                format: ''
            })
        }
    }

    handleFileChange = async () => {
        const bulkUpload = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['downloadAdministratorCSV']}`
        }
        const bulkUploadSuccessData = await getService(bulkUpload);
        if (bulkUploadSuccessData && bulkUploadSuccessData.status === 200) {
            this.downloadCSV(bulkUploadSuccessData);
        }
    }
    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }
    render() {
        return (<div className='main'>
            <h2 className="title">{RA_STR.bulkuploadadministratortitle}</h2>
            <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.bulkuploadadministratordesc2 }}></p>
            <div className="row"  >
                <div className="col-sm-5 uploadcsv" >
                    <img src="images/step1.png" alt="caLogo" ></img>
                    <div className="buDivider">
                        <h3> {RA_STR.bulkuploadadministratortemplateheading}</h3>
                        <div className="">{RA_STR.bulkuploadadministratortemplatedescription}</div>
                    </div>
                    <div className="buTemplate" align="center">
                        <div>
                            <img src="images/csv-icon.png" />
                            <input type="button" className="bulkUploadTemplateButton" id="bulkUploadTemplateButton" name="bulkUploadTemplateButton" value="Bulk Upload Template for Organizations" onClick={this.handleFileChange} />
                        </div>
                        <img src="images/saveimage.png" className="buDwnIcon" />
                    </div>
                </div>
                <div className="col-sm-5 uploadcsv" >
                    <img src="images/step2.png" alt="caLogo" ></img>
                    <div className="buDivider">
                        <h3>
                            {RA_STR.bulkuploadadministratorcsvtitle}
                        </h3>
                        <div className="" onClick={this.handleFileChange}>
                            {RA_STR.bulkuploadadministratorcsvdesc}
                        </div>
                    </div>
                    <input type="file" name="myFile" onChange={this.handleChange} ref={ref => this.fileInput = ref} className="mt-3" />
                    <div>
                        <input type="button" className="secondary-btn mt-3" value="Upload" onClick={this.uploadFile} ></input>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

export default BulkUploadAdministrator;
