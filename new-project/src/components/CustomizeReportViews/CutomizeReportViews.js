import React, { Component } from 'react';
import './CustomizeReportViews.css';
import ReactDOM from 'react-dom';
import { serverUrl, RA_API_URL, RA_STR_CHECKBOX } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { orgData } from '../../shared/utlities/renderer';
import { RA_STR } from '../../shared/utlities/messages';
import _ from 'underscore';

class CustomizeReportViews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orgDetails: '',
            reportData: [],
            allChecked: false,
            selectMenu: [],
            rightMenu: [],
            reportForm: {},
            elements: [],
            checkedListAll: [],
            orderedElements: []
        }
    }

    showErrorList = () => {
        window.scrollTo(0, 0);
    }

    componentDidMount = async () => {
        this.state.orgDetails = orgData('');
        this.getCustomReport();
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    getCustomReport = async () => {
        const test = [];
        const getCustomViews = {
            method: 'GET',
            url: `${serverUrl}/organization/${this.state.orgDetails.orgName}${RA_API_URL['customizereport']}`
        };
        const getCustomViewstDetails = await getService(getCustomViews);
        if (getCustomViewstDetails && getCustomViewstDetails.status === 200) {
            this.setState({
                reportData: getCustomViewstDetails.data.elements,
                selectMenu: getCustomViewstDetails.data.orderedElements
            });
            this.props.activateSuccessList(true, getCustomViewstDetails.data);
            this.props.activateErrorList(false, '');
            this.showErrorList();
            return this.state.reportData.map(list => {
                return list.elementDetails.map(item => {
                    test.push(item);
                    this.setState({
                        // selectMenu: test,
                        rightMenu: [...test]
                    })
                })
            })
        }
        else {
            this.props.activateErrorList(true, getCustomViewstDetails.data.errorList);
            this.props.activateSuccessList(false, '');
            this.showErrorList();
        }
    }

    listbox_move(selectedArrow) {
        let node = ReactDOM.findDOMNode(this.refs['multiGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });
        var selectedValues = selected.map(function (option) {
            return { "value": option.value, "id": option.id };
        });
        if (selectedValues.length > 0) {
            if (selectedArrow === 'up') {
                for (var i = 0; i < options.length; i++) {
                    if (parseInt(selectedValues[0].id) !== 0) {
                        if (options[i].selected) {
                            options[i].selected = false;
                        } else {
                            if (parseInt(selectedValues[0].id) - 1 !== -1)
                                if (i === parseInt(selectedValues[0].id) - 1) {
                                    options[i].selected = true;
                                }
                        }
                    }
                }
                if (parseInt(selectedValues[0].id) - 1 !== -1)
                    this.insertAndShift(parseInt(selectedValues[0].id), parseInt(selectedValues[0].id) - 1);
            } else {
                for (var i = 0; i < options.length; i++) {
                    if (parseInt(selectedValues[0].id) + 1 !== options.length) {
                        if (options[i].selected) {
                            options[i].selected = false;
                        } else {
                            if (i === parseInt(selectedValues[0].id) + 1) {
                                options[i].selected = true;
                            }
                        }
                    }
                }
                if (parseInt(selectedValues[0].id) + 1 !== options.length)
                    this.insertAndShift(selectedValues[0].id, parseInt(selectedValues[0].id) + 1)
            }
        }

    }


    // Remove an element from array and insert in to a given index 
    insertAndShift = (from, to) => {
        let cutOut = this.state.selectMenu.splice(from, 1)[0];
        this.state.selectMenu.splice(to, 0, cutOut);
        this.setState({ selectMenu: this.state.selectMenu });
    }

    handleChange = (data, e) => {
        const reportForm = this.state.reportForm;
        const { name, value } = e.target;
        var feildValue;
        if (e.target.type === RA_STR_CHECKBOX) {
            data.elementDetails.map((item, i) => {
                if (item.elementName === e.target.name) {
                    item.selected = !item.selected;
                }
            })
            if (e.target.checked === false && name !== 'selectAll') {
                data.elementDetails.map((item, i) => {
                    if (item.elementName === e.target.name) {
                        var output = this.state.selectMenu.filter(x => x.elementName !== item.elementName);
                        this.setState({
                            selectMenu: output
                        })
                    }
                })
            }
            else if(e.target.checked === true && name !== 'selectAll'){
                var selectItem = '';
                data.elementDetails.map((item, i) => {
                    if (item.elementName === e.target.name) {
                        selectItem = data.elementDetails[i];
                    }
                })
                this.setState({
                    selectMenu: this.state.selectMenu.concat(selectItem)
                })
            }
            else if (name === 'selectAll' && e.target.checked === true) {
                var reportData = this.state.reportData;
                reportData.map(item => {
                    if (item.displayName === data.displayName) {
                        data.elementDetails.map(list => {
                            list.selected = true;
                        })
                        item.elementDetails = data.elementDetails;
                    }
                })
                this.setState({
                    reportData: reportData,
                    selectMenu: this.state.selectMenu.concat(data.elementDetails)
                })
            }
            else if (name === 'selectAll' && e.target.checked === false) {
                var output = this.state.selectMenu.filter((el) => {
                    return data.elementDetails.findIndex((elem) => {
                        return elem.elementName == el.elementName;
                    }) == -1;
                });
                this.setState({
                    selectMenu: output
                })
                this.state.reportData.map(item => {
                    if (item.displayName === data.displayName) {
                        data.elementDetails.map(list => {
                            list.selected = false;
                        })
                        item.elementDetails = data.elementDetails;
                    }
                })
                this.setState({
                    reportData: this.state.reportData
                })
            }
            else {
                this.state.reportData.map((item, i) => {
                    const selectedList = item.elementDetails;
                    selectedList.map((list) => {
                        if (list.displayName === data.displayName) {
                            if ((selectedList[i].selected && selectedList[i + 1].selected) === true) {
                                item.selected = true;
                            }
                            else if ((selectedList[i].selected || selectedList[i + 1].selected) === false) {
                                item.selected = false;
                            }
                            else {
                                item.selected = false;
                            }
                        }
                    })
                })
                if (name !== 'selectAll') {
                    this.setState({
                        reportData: this.state.reportData
                    })
                }
            }
        }
        (e.target.type === RA_STR_CHECKBOX) ?
            feildValue = e.target.checked
            :
            feildValue = value;
        reportForm[name] = feildValue;
        this.state.elements.push(data);
    }

    onSubmit = async () => {
        let params = {
            elements: this.state.reportData || this.state.elements,
            orderedElements: this.state.selectMenu

        }
        const submitReport = {
            method: 'POST',
            url: `${serverUrl}/organization/${this.state.orgDetails.orgName}${RA_API_URL['customizereport']}`,
            data: params
        };
        const submitReportStatus = await getService(submitReport);
        if (submitReportStatus && submitReportStatus.status === 400) {
            this.props.activateErrorList(true, submitReportStatus.data.errorList);
            this.props.activateSuccessList(false, '');
            this.showErrorList();
        }
        else {
            this.props.activateSuccessList(true, submitReportStatus.data);
            this.props.activateErrorList(false, '');
            this.showErrorList();
        }
    }

    reset = () => {
        if (window.confirm(RA_STR.customRepMsg)) {
            this.getCustomReport();
            window.location.reload();
        }
    }

    render() {
        const { reportData } = this.state;
        return (
            <div className="main">
                <h2 className="title">{RA_STR.customRepTitle}</h2>
                <p className="desc">{RA_STR.customRepText}</p>
                {reportData.map((item, index) => {
                    return (
                        <div key={index}>
                            <div className="div-seperator">
                                <div className="col-md-12 form-inline row">
                                    <h6 className="apply-bold col-md-5 row">{item.displayName}</h6>
                                    <span className="custom-control custom-checkbox">
                                        <label key={index} className='form-label capitalize col-sm-12' >
                                            <input
                                                className="form-checkbox custom-control-input"
                                                name='selectAll'
                                                onChange={this.handleChange.bind(this, item)}
                                                id={index}
                                                type='checkbox'
                                                defaultValue={this.state.allChecked || item.selected}
                                                defaultChecked={this.state.allChecked || item.selected} />
                                            <label className="form-label custom-control-label" htmlFor={index}>Select All</label>
                                        </label>
                                    </span>
                                </div>
                                <div className="custom-control custom-checkbox">
                                    <div className="custom-flex">
                                        {item.elementDetails.map((opt, index) => {
                                            return (
                                                <label key={index} className='form-label capitalize col-sm-12' >
                                                    <input
                                                        className="form-checkbox custom-control-input"
                                                        name={opt.elementName}
                                                        onChange={this.handleChange.bind(this, item)}
                                                        id={opt.elementName}
                                                        checked={opt.selected}
                                                        defaultValue={opt.selected}
                                                        type='checkbox' />
                                                    <label className="form-label custom-control-label" htmlFor={opt.elementName}>{opt.displayName}</label>
                                                </label>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )

                })}

                <p className="desc">{RA_STR.customRepOrderText}</p>
                <div className="div-order">
                    <select className="menuHeight mt-2" ref="multiGrpSel" multiple>
                        {this.state.selectMenu.map((opt, index) => {
                            return <option key={index} value={opt.displayName} id={index}>{opt.displayName}</option>;
                        })}
                    </select>
                </div>
                <select className="currentOrder mt-2" multiple readOnly>
                    {this.state.rightMenu.map((opt, index) => {
                        return <option key={index} value={opt.displayName}>{opt.displayName}</option>;
                    })}
                </select>
                <div className="flex-container-button mt-2">
                    <div >
                        <input className="custom-secondary-btn" type="button" value="UP" onClick={() => this.listbox_move('up')}></input>
                        <input className="custom-secondary-btn" type="button" value="DOWN" onClick={() => this.listbox_move('down')}></input>
                    </div>

                </div>
                <div className="form-group form-submit-button row mt-5">
                    <input className="secondary-btn" type="submit" value="SUBMIT" onClick={this.onSubmit}></input>
                    <input className="secondary-btn ml-3" type="submit" value="RESET" onClick={this.reset}></input>
                </div>

            </div>

        );
    }
}

export default CustomizeReportViews;
