import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl, RA_API_URL, RA_STR_SELECT, RA_API_STATUS } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import DateRange from '../../shared/components/DateRange/DateRange';
import { saveAs } from 'file-saver';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import C3Chart from 'react-c3js';
import 'c3/c3.css';
import './DeviceSummaryReport.scss';

class DeviceSummaryReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultOrganizationName: RA_STR_SELECT,
      organizationNames: [],
      defaultChannel: 'ALL',
      channelList: [],
      orgDetails: false,
      dateRangeOptions: [],
      showGraph: false,
      columns: [
        { title: 'Device Type', field: 'deviceType' },
        { title: 'DeviceID Read', field: 'deviceidRead' },
        { title: 'New Device', field: 'newDevice' },
        { title: 'Reverse Lookup', field: 'reverseLookup' },
        { title: 'Total', field: 'total' }
      ],
      data: {
        x:'x',
        columns: [
        ],
        type: 'bar',
        // labels: true,
      },
      axis: {
        rotated: true,
        x: {
          type: 'category',
        }
      },
      grid: {
        x: {
          show: true
        },
        y: {
            show: true
        }
    },
      bar: {
        width: 15
      },
      // legend: {
      //   show: true,
      //   position: 'inset',
      //   inset: {
      //     anchor: 'top-left',
      //     x: 250,
      //     y: 50,
      //     step: 1
      //   }
      // },
      color: {
        pattern: ['#224654', '#3d8da0','#53bbd4']
    },
    };
  }
  componentDidMount() {
    this.getAllOrganizations();
  }

  // Get Organisation Details
  getAllOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };

  // On select of Org Name enabling Channels Dropdown
  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find(element => {
        if (element.content === value) return element.key;
      });
      this.setState({
        defaultOrganizationName: orgId.key,
        orgDetails: true
      });
      this.getChannels(orgId.key);
    }
  };

  //Getting Channels
  getChannels = async selectedOrg => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['deviceChannels']}/${selectedOrg}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        dataList:CA_Utils.objToArray(response.data, 'object')||{}
      })
      let channelList = [];
      if (response.data.length > 1) {
        channelList = [
          {
            content: 'All Channels',
            key: 'All'
          }
        ];
      }
      this.state.dataList.forEach(obj => {
        channelList.push({
          content: obj.content,
          key: obj.key
        });
      });
        this.setState({ channelList: channelList });
    }
  };

  // Setting Default Channel
  onChannelSelect = event => {
    const channel = event.target.value;
      this.setState({ defaultChannel: channel });
    
  };

  displayReport = async () => {
    const getDateItems = this.refs.daterange.getDates();
    this.setState({ fromDate: getDateItems[0].startDate, toDate: getDateItems[0].endDate });
    const displayData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['deviceDisplay']}`,
      data: {
        adminPreferredTimezone: 'GMT',
        pageNo: 0,
        pageSize: 10,
        channelName:this.state.dataList > 1? this.state.defaultChannel : this.state.dataList[0].content,
        orgName: this.state.defaultOrganizationName,
        fromDate: getDateItems[0].startDate,
        toDate: getDateItems[0].endDate
      }
    };
    const response = await getService(displayData);
    if (response && response.status === RA_API_STATUS['200']) {
      this.props.activateErrorList(false, '');
      this.setState({
        showTable: true,
        apiData: response.data.reportList,
        channnels: response.data.channelName,
        orgName: response.data.orgName
      });
    if(this.state.apiData.length){
      let renderData=this.state.data;
      // deviceType=response.data.reportList.map(({ deviceType }) => deviceType);
      let deviceType = response.data.reportList.map(a => a.deviceType);
      // gives array of ['x','PC']
      deviceType.splice(0,0,'x');
      
      const tempArray=[];
      tempArray.push(deviceType); 
      tempArray.push(['DeviceID Read'])
      tempArray.push(['New Device'])
      tempArray.push(['Reverse Lookup'])
       
      this.state.apiData.forEach((obj, index) => {
        tempArray[1].push(obj.deviceidRead);
        tempArray[2].push(obj.newDevice);
        tempArray[3].push(obj.reverseLookup);
      })

      renderData.columns = tempArray;
      this.setState({ data: renderData });
    }
  
  } else {
    this.props.activateErrorList(true, response.data.errorList);
    this.props.activateSuccessList(false, '');
    this.setState({ showTable: false });
    window.scrollTo(0, 0);
  }
  };

  newReport = () => {
    this.setState({
      defaultOrganizationName: RA_STR_SELECT,
      showTable: false,
      orgDetails: false,
      showGraph: false
    });
  };

  // For Export
  reportExport = async () => {
    const reportExpo = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['deviceExport']}`,
      data: {
        adminPreferredTimezone: 'GMT',
        channelName: this.state.defaultChannel,
        decryptionNeeded: true,
        orgName: this.state.defaultOrganizationName,
        pageNo: 0,
        pageSize: 0,
        fromDate: this.state.fromDate,
        toDate: this.state.toDate
      }
    };
    const exportSuccessData = await getService(reportExpo);
    if (exportSuccessData && exportSuccessData.status === RA_API_STATUS['200']) {
      this.downloadCSV(exportSuccessData);
    } else if (
      exportSuccessData &&
      exportSuccessData.status === RA_API_STATUS['400'] &&
      exportSuccessData.data.errorList
    ) {
      this.props.activateErrorList(true, exportSuccessData.data.errorList);
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  };

  visualiseReport = async () => {
    this.state.showGraph = !this.state.showGraph;
    this.setState({ showGraph: this.state.showGraph });
  };
  // Download Report
  downloadCSV(response) {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'DeviceSummaryReport_' + this.state.defaultOrganizationName + '.csv');
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  render() {
    const {
      organizationNames,
      defaultOrganizationName,
      channelList,
      defaultChannel,
      channnels,
      fromDate,
      toDate,
      columns,
      apiData,
      showGraph,
      data,
      axis,
      bar,
      legend,
      color,x,
      grid
    } = this.state;
    return (
      <div className='main'>
        {!this.state.showTable ? (
          <div>
            <h2 className='title'>{RA_STR.deviceTitle}</h2>
            <p className='desc'>{RA_STR.instanceDesc}</p>
            <hr />
            <div className='col-sm-6'>
              <div className='form-group dynamic-form-select'>
                <div className='form-group row'>
                  <label className='col-sm-4 col-form-label'>{RA_STR.orgzName}</label>
                  <div className='col-sm-8'>
                    <AutoSuggest
                      orgOptions={organizationNames}
                      getSelectedOrgDetails={this.onOrgSelect}
                      enableAutosuggest={true}
                      defaultOrganizationPlaceholder={defaultOrganizationName}
                    />
                  </div>
                </div>
              </div>
            </div>
            {this.state.orgDetails ? (
              <div>
                <div className='col-sm-6'>
                <Select
                    name={'channel'}
                    title={RA_STR.channel}
                    required={false}
                    selectedOption={defaultChannel}
                    options={channelList ? channelList : ['']}
                    controlFunc={this.onChannelSelect}
                  />
                </div>

                <div className='row'>
                  <div className='col-sm-3'>
                    <label className='col-sm-8 col-form-label mt-3'> {RA_STR.timePeriod} </label>
                  </div>
                  <DateRange ref='daterange'></DateRange>
                </div>

                <div className='form-group form-submit-button row'>
                  <input
                    className='secondary-btn'
                    type='submit'
                    value='DISPLAY REPORT'
                    onClick={this.displayReport}
                  ></input>
                </div>
              </div>
            ) : (
              <React.Fragment />
            )}
          </div>
        ) : (
          <React.Fragment />
        )}
        {this.state.showTable ? (
          <div>
            <div>
              <h2 className='title'>{RA_STR.deviceTitle}</h2>
              <div className='form-group row ReportButtons'>
                <p className='col-sm-8 desc'>{RA_STR.deviceDesc}</p>
                <input className='secondary-btn' type='submit' value='EXPORT' onClick={this.reportExport}></input>
                <input
                  className='secondary-btn'
                  id='searchButton'
                  type='submit'
                  value='NEW REPORT'
                  onClick={this.newReport}
                ></input>
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='form-group row'>
                <label className='col-sm-4 col-form-label'>{RA_STR.orgzName}</label>
                <div className='col-sm-8 col-form-label'>{defaultOrganizationName}</div>
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='form-group row'>
                <label className='col-sm-4 col-form-label'>{RA_STR.fraudChannel}:</label>
                <div className='col-sm-8 col-form-label'>{channnels}</div>
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='form-group row'>
                <div className='col-sm-4 col-form-label'>
                  {RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL} {fromDate}
                </div>
                <div className='col-sm-8 col-form-label'>
                  {RA_STR.EXCEPTION_USER_REPORT.TO_LABEL} {toDate}
                </div>
              </div>
            </div>
            <div style={{ maxWidth: '100%', clear: 'both' }}>
              <DynamicTable columns={columns} data={apiData} enablePagination={false} selection={false} />
            </div>
            <div className='form-group form-submit-button row'>
              <input
                className='secondary-btn'
                type='submit'
                value={!showGraph ? 'VISUALISE REPORT' : 'HIDE CHART'}
                onClick={this.visualiseReport}
              ></input>
            </div>
          </div>
        ) : (
          <React.Fragment />
        )}
        {showGraph ? <C3Chart data={data} axis={axis} bar={bar} legend={legend} color={color} x={x} grid={grid}/> : ''}
      </div>
    );
  }
}

export default DeviceSummaryReport;
