import React, { Component } from 'react';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import {RA_STR} from '../../shared/utlities/messages';
import './AssignChannelsConfig.css';
class AssignChannelsConfig extends Component {
  constructor(props) {
    super(props);
    const urlParams = new URLSearchParams(window.location.search);
    const orgName = urlParams.get('orgname');
    this.state = {
      actualdata: [],
      channelUrl: `/organization/${orgName}/assignchannels`,
      accountTypes: `/organization/${orgName}/assignchannels/accountTypes`,
      columns: [
        {
          title: 'Select Channels to Associate', field: 'associated', render: (rowData) =>
            <div className="checkbox">
              <input id={`customCheckbox${rowData.channelId}`} type="checkbox" checked={rowData.associated} onChange={this.handleCheckbox.bind(this, rowData.channelId)} />
              <label htmlFor={`customCheckbox${rowData.channelId}`}></label>
            </div>
        },
        { title: 'Channel', field: 'name' },
        {
          title: 'Default Account Type', field: 'account-type', render: rowData =>
            <div className="dynamic-form-select"><select className="form-select form-control custom-select-sm" onChange={this.getSelectedValue.bind(this, rowData.channelId)} defaultValue={rowData.accountType}><option value="">Username</option>{
              rowData.accountGroup.map((eachvalue, key) => {
                return <option key={key} value={eachvalue.accountType}>{eachvalue.displayName}</option>
              })
            }
            </select></div>
        },
        {
          title: 'Select Default Channel',
          field: 'defaultChannel',
          render: rowData =>
            <div className="radio">
              <input type="radio" id={`customRadio${rowData.channelId}`} name="customRadio" className="custom-control-input" defaultChecked={rowData.defaultChannel} onChange={this.handleRadioButtons.bind(this, rowData.channelId)} />
              <label className="" htmlFor={`customRadio${rowData.channelId}`}></label>
            </div>
        },
      ],
      apiData: [
      ]
    }
  }
  componentDidMount = async () => {
    this.getAssignChannelsData();
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  getAssignChannelsData = async () => {
    const { channelUrl, accountTypes } = this.state;
    const getAccountType = {
      method: 'GET',
      url: `${serverUrl}${accountTypes}`
    };
    const getgetAccountType = await getService(getAccountType);
    const getAssignChannels = {
      method: 'GET',
      url: `${serverUrl}${channelUrl}`
    };
    const getAssignData = await getService(getAssignChannels);
    if (getAssignData && getAssignData.status === 200) {
      this.setState({ actualdata: JSON.parse(JSON.stringify(getAssignData.data)) });
      getAssignData.data && getAssignData.data.map((eachvalue) => {
        eachvalue.accountGroup = getgetAccountType.data;
        if(eachvalue.associated){
          eachvalue.checkDefault = true;
        }
      })
      this.setState({ apiData: getAssignData.data });
    }
  }
  handleCheckbox = (getkey, e) => {
    let getactualdata = this.state.actualdata;
    let getapiData =this.state.apiData;
    if (e.target.checked) {
      getactualdata[getkey - 1].associated = true;
      getapiData[getkey - 1].associated = true;
    } else {
      let dialogResult = '';
      if(getapiData[getkey - 1].checkDefault){
        dialogResult = window.confirm(RA_STR.channelassignconfirm);
      }
      if(dialogResult || dialogResult === ''){
        getactualdata[getkey - 1].associated = false;
        getapiData[getkey - 1].associated = false;
      }
    }
    this.setState({ actualdata: getactualdata,apiData:getapiData});
  }
  handleRadioButtons = (getkey) => {
    let getactualdata = this.state.actualdata;
    getactualdata.map((dataValue) => {
      if (dataValue.channelId === getkey) {
        dataValue.defaultChannel = true;
      } else {
        dataValue.defaultChannel = false;
      }
    })
    this.setState({ actualdata: getactualdata });
  }
  getSelectedValue = (getkey, e) => {
    let getactualdata = this.state.actualdata;
    getactualdata[getkey - 1].accountType = e.target.selectedOptions[0].value;
    this.setState({ actualdata: getactualdata });
  }
  submitChannels = async () => {
    const { channelUrl } = this.state;
    const postChannels = {
      method: 'POST',
      url: `${serverUrl}${channelUrl}`,
      data: {
        "assignChannels": this.state.actualdata
      }
    };
    const getAssignData = await getService(postChannels);
    if (getAssignData && getAssignData.status === 200) {
      this.props.activateSuccessList(true, getAssignData.data);
      this.props.activateErrorList(false, '');
      this.getAssignChannelsData();
    } else {
      this.props.activateErrorList(true, getAssignData.data.errorList);
      this.props.activateSuccessList(false, '');
    }

  }
  render() {
    return <div className="main">
      <div className="no-padding assignChannelParent">
        <h2 className="title">{RA_STR.channelsssign}</h2>
        <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.channelassigntext }}></p>
        <DynamicTable columns={this.state.columns} data={this.state.apiData} enablePagination={false} selection={false} />
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Save" onClick={this.submitChannels}></input>
      </div>
    </div>;
  }
}

export default AssignChannelsConfig;
