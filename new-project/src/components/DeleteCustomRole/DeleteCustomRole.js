import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import { connect } from 'react-redux';
import { fetchPosts } from '../../redux/actions/validationAction';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
class DeleteCustomRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: [],
      storeRoles: [],
      selectedroleBased: '',
      storePredefinedRoles: [],
      roleDisplayName: '',
      roleDesc: '',
      roleBased: ''
    }
  }
  componentDidMount = async () => {
    this.getRoleData();
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  getRoleData = async () => {
    const getPredefinedRoleData = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['predefinedRoles']}`
    };
    const getPredefinedRoleDataStatus = await getService(getPredefinedRoleData);
    if (getPredefinedRoleDataStatus.status === 200) {
      this.setState({ storePredefinedRoles: getPredefinedRoleDataStatus.data.roles });
    }
    const getRoleData = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['customRoleDelete']}`
    };
    const getRoleDataStatus = await getService(getRoleData);
    if (getRoleDataStatus.status === 200) {
      this.setState({ storeRoles: getRoleDataStatus.data ? getRoleDataStatus.data.roles : '' });
    }
    var arrNames = [];
    getRoleDataStatus.data.roles && Object.keys(getRoleDataStatus.data.roles).forEach(function (key) {
      var roleName = getRoleDataStatus.data.roles[key]["roleName"];
      arrNames.push({
        'content': roleName,
        'key': roleName
      });
    });
    this.setState({ roleOptions: arrNames });
  }
  deleteRoles = async () => {
    let dialogResult = '';
    if (this.state.selectedroleBased) {
      dialogResult = window.confirm(RA_STR.deletecustomroleconfirm);
    }
    if (dialogResult || dialogResult === '') {
      const checkDeleteRole = {
        method: 'DELETE',
        url: `${serverUrl}${RA_API_URL['customRoleDelete']}`,
        data: this.state.selectedroleBased ? this.state.selectedroleBased : {}
      }
      const checkDeleteRoleStatus = await getService(checkDeleteRole);
      if (checkDeleteRoleStatus.status !== 200) {
        this.props.activateErrorList(true, checkDeleteRoleStatus.data.errorList);
        this.props.activateSuccessList(false, '');
      } else if (checkDeleteRoleStatus.status === 200 && checkDeleteRoleStatus.data.message) {
        this.props.activateSuccessList(true, checkDeleteRoleStatus.data);
        this.props.activateErrorList(false, '');
        this.setState({
          roleDisplayName: '', roleDesc: '', roleBased: '', selectedroleBased: '', roleOptions: [], storeRoles: [], storePredefinedRoles: [],
        });
        this.getRoleData();
      }
    }
  }
  handleChange = (e) => {
    let getSelectedValue = e.target.value;
    if (getSelectedValue) {
      var selectedRole = this.state.storeRoles.filter(function (option) {
        return option.roleName === getSelectedValue;
      });
      this.setState({ selectedroleBased: selectedRole[0] });
      var selectedParentRoleName = this.state.storePredefinedRoles.filter(function (option) {
        return option.roleName === selectedRole[0].parentRole;
      });
      this.setState({
        roleDisplayName: selectedRole[0].displayName,
        roleDesc: selectedRole[0].roleDescription,
        roleBased: selectedParentRoleName[0].displayName
      });
    } else {
      this.setState({
        roleDisplayName: '',
        roleDesc: '',
        roleBased: '',
        selectedroleBased: ''
      })
    }
  }

  render() {
    const { roleOptions, roleDisplayName, roleDesc, roleBased } = this.state;
    return <div className="main"><h2 className="title">{RA_STR.deletecustomrole}</h2>
      <p className="desc">{RA_STR.deletecustomrolebody}</p>
      <div className="col-sm-5">
        <span className="ecc-h1 row">{RA_STR.deletecustomrolesub}</span>
        <Select
          name={'role-name'}
          title={'Role Name'}
          required={true}
          options={roleOptions ? roleOptions : ''}
          placeholder={'Select'}
          controlFunc={this.handleChange}
        />
        <SingleInput
          title={'Role Display Name'}
          disabled={true}
          inputType={'text'}
          content={roleDisplayName}
          name={'role-display-name'} />
        <SingleInput
          title={'Role Description'}
          disabled={true}
          inputType={'text'}
          content={roleDesc}
          name={'role-description'} />
        <SingleInput
          title={'Role Based On'}
          disabled={true}
          inputType={'text'}
          content={roleBased}
          name={'role-based-on'} />
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Delete" onClick={this.deleteRoles}></input>
      </div>
    </div>;
  }
}
const mapStateToProps = state => ({
  validationItems: state.validationList.items
})
export default connect(mapStateToProps, { fetchPosts })(DeleteCustomRole);
