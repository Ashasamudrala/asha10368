import React, { Component } from 'react';
import SwappableGrid from '../../shared/components/SwapGrid/swappingComponent';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import './ConfigureFieldsRelay.scss';

class ConfigureFieldsRelay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            configurationInfo: [],
            activiateEditList: false,
            activateCreateList: true,
            lefttransactionFields: [],
            newConfiguration: '',
            configurationFieldName: '',
            selectValue: '',
            selectConfigurationname:''
        }
    }

    componentDidMount = () => {
        this.getOrganisations();
        this.getConfigurations();
        this.getTransactionFields();
    }

    getConfigurations = async () => {
        const getConfigurationData = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getConfigurations']}`
        }
        const getConfigurationStatus = await getService(getConfigurationData);
        if (getConfigurationStatus && getConfigurationStatus.status === 200) {
            this.setState({ configurationInfo: getConfigurationStatus.data });
        }
    }

    getTransactionFields = async () => {
        let itemObj = {}
        const getTransactionFields = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getTransactionFields']}`
        }

        const getTransactionFieldsStatus = await getService(getTransactionFields);

        if (getTransactionFieldsStatus && getTransactionFieldsStatus.status === 200) {
            getTransactionFieldsStatus.data.forEach((item) => {
                itemObj[item] = item;
            })
            let leftItemsObj = { leftItems: itemObj, leftStaticArray: itemObj };
            this.refs.swappableGridFieldRef.setLeftItems(leftItemsObj);
            this.refs.swappableGridFieldRef.setRightItems({ rightItems: [], rightStaticArray: [] });
            this.setState({ lefttransactionFields: getTransactionFieldsStatus.data, leftObjTransactionFields: itemObj });
        }
    }

    handleCreateField = (e) => {
        this.setState({ [e.target.name]: e.target.value , selectConfigurationname : e.target.value});
    }

    getOrganisations = async () => {
        const getOrg = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
        }

        const getOrganization = await getService(getOrg);
        if (getOrganization && getOrganization.status === 200) {
            this.setState({ leftItems: getOrganization.data.organizations, orgDetails: getOrganization.data.organizations });
        }
        this.refs.swappableGridRef.setLeftItems({ leftItems: getOrganization.data.organizations, leftStaticArray: getOrganization.data.organizations });
        this.refs.swappableGridRef.setRightItems({ rightItems: [], rightStaticArray: [] });

    }

    showEditDialogBox(id) {
        let modal = document.getElementById(id);
        modal.style.display = "block";
    }

    handleClose = (id) => {
        let modal = document.getElementById(id);
        modal.style.display = "none";
    }

    saveConfiguration = async () => {
        const { lefttransactionFields } = this.state;
        let params =
        {
            "availableTransactionFields": Object.values(this.refs.swappableGridFieldRef.getLeftItems()),
            "configurationName": this.state.newConfiguration,
            "orgNames": Object.values(this.refs.swappableGridRef.getRightItems()),
            "selectedTransactionFields": Object.values(this.refs.swappableGridFieldRef.getRightItems())
        }

        const createConfiguration = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['createConfiguration']}`,
            data: params
        }

        const createConfigurationStatus = await getService(createConfiguration);
        if (createConfigurationStatus && createConfigurationStatus.status === 200) {
            this.handleClose('createModal');
            this.getConfigurations();
            let leftItemsObj = { leftItems: lefttransactionFields, leftStaticArray: lefttransactionFields };
            this.refs.swappableGridFieldRef.setLeftItems(leftItemsObj);
            this.refs.swappableGridFieldRef.setRightItems({ rightItems: [], rightStaticArray: [] });
            this.props.activateSuccessList(true, createConfigurationStatus.data);
            this.props.activateErrorList(false, '');
        } else if (createConfigurationStatus && createConfigurationStatus.status !== 200) {
            this.handleClose('createModal');
            this.props.activateErrorList(true, createConfigurationStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    updateConfiguration = async (e) => {
        const { configurationFieldName } = this.state;
        let params =
        {
            "availableTransactionFields": Object.values(this.refs.swappableupdateGridRef.getLeftItems()),
            "configurationName": configurationFieldName,
            "orgNames": Object.values(this.refs.swappableGridRef.getRightItems()),
            "selectedTransactionFields": Object.values(this.refs.swappableupdateGridRef.getRightItems())
        }
        const updateConfigurationInfo = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['createConfiguration']}`,
            data: params
        }

        const updateConfigurationStatus = await getService(updateConfigurationInfo);
        if (updateConfigurationStatus && updateConfigurationStatus.status === 200) {
            this.handleClose('editModal');
            this.getConfigurations();
            this.props.activateSuccessList(true, updateConfigurationStatus.data);
            this.props.activateErrorList(false, '');
        } else if (updateConfigurationStatus && updateConfigurationStatus.status !== 200) {
            this.handleClose('editModal');
            this.props.activateErrorList(true, updateConfigurationStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }

    }

    saveConfigurationInfo = async () => {
        const { configurationFieldName , selectValue , selectConfigurationname} = this.state;
        let params = (selectValue === '') ?
            {
                "availableTransactionFields": Object.values(this.refs.swappableGridFieldRef.getLeftItems()),
                "configurationName": selectConfigurationname ,
                "orgNames": Object.values(this.refs.swappableGridRef.getRightItems()),
                "selectedTransactionFields": Object.values(this.refs.swappableGridFieldRef.getRightItems())
            } :
            {
                "availableTransactionFields": Object.values(this.refs.swappableupdateGridRef.getLeftItems()),
                "configurationName": configurationFieldName,
                "orgNames": this.refs.swappableGridRef.getRightItems(),
                "selectedTransactionFields": Object.values(this.refs.swappableupdateGridRef.getRightItems())


            }
        const saveConfigurationInfo = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['saveUpdateExtFieldConfig']}`,
            data: params
        }
        const saveConfigurationStatus = await getService(saveConfigurationInfo);

        if (saveConfigurationStatus && saveConfigurationStatus.status === 200) {
            this.props.activateSuccessList(true, saveConfigurationStatus.data);
            this.props.activateErrorList(false, '');
            this.getConfigurations();
            this.setState({selectConfigurationname : '' })
        } else if (saveConfigurationStatus && saveConfigurationStatus.status !== 200) {
            this.props.activateErrorList(true, saveConfigurationStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }

    }


    handleChange = (e) => {
        let { activateCreateList, activiateEditList, lefttransactionFields, configurationInfo, orgDetails } = this.state;
        let leftupdatetransactionFields;
        if (e.target.value === '') {
            activateCreateList = true;
            activiateEditList = false;
            this.refs.swappableGridRef.setLeftItems({ leftItems: orgDetails, leftStaticArray: orgDetails });
            this.refs.swappableGridRef.setRightItems({ rightItems: [], rightStaticArray: [] });

            this.setState({ activiateEditList: activiateEditList, activateCreateList: activateCreateList, selectValue: e.target.value })
        }
        else {
            let objleftupdatetransactionFields = {};
            let leftorgObj = {};
            let leftorgDetails;
            leftupdatetransactionFields = lefttransactionFields.filter(function (item) {
                return configurationInfo[e.target.selectedIndex - 1]['selectedTransactionFields'].indexOf(item) === -1;
            });
            leftupdatetransactionFields.forEach((item) => {
                objleftupdatetransactionFields[item] = item;
            })

            leftorgDetails = Object.keys(orgDetails).filter(function (item) {
                return configurationInfo[e.target.selectedIndex - 1]['orgNames'].indexOf(item) === -1;
            });
            leftorgDetails.forEach((item) => {
                leftorgObj[item] = item;
            })
            activateCreateList = false;
            activiateEditList = true;
            this.refs.swappableupdateGridRef.setLeftItems({ leftItems: objleftupdatetransactionFields, leftStaticArray: objleftupdatetransactionFields });
            this.refs.swappableGridRef.setLeftItems({ leftItems: leftorgObj, leftStaticArray: leftorgObj });
            this.refs.swappableGridRef.setRightItems({ rightItems: configurationInfo[e.target.selectedIndex - 1]['orgNames'], rightStaticArray: configurationInfo[e.target.selectedIndex - 1]['orgNames'] });
            this.refs.swappableupdateGridRef.setRightItems({ rightItems: configurationInfo[e.target.selectedIndex - 1]['selectedTransactionFields'], rightStaticArray: configurationInfo[e.target.selectedIndex - 1]['selectedTransactionFields'] });
            this.setState({ activiateEditList: activiateEditList, activateCreateList: activateCreateList, configurationFieldName: configurationInfo[e.target.selectedIndex - 1]['configurationName'], selectValue: e.target.value })
        }

    }

    render() {
        return (
            <div className="main configure-fields ">
                <h2 className="title">{RA_STR.configurationtitle}</h2>
                <p className="desc">{RA_STR.configurationtab1}</p>
                <p dangerouslySetInnerHTML={{ __html: RA_STR.configurationtab2 }}></p>
                <div className="div-seperator">
                    <div>Configuration Name</div>
                    <div className="form-group dynamic-form-select">
                        <div className="form-group row">
                            <div className="col-sm-4">
                                <select
                                    name={'props.name'}
                                    className="form-select form-control " onChange={this.handleChange} >
                                    <option value=""> {RA_STR.selectConstantValue}</option>
                                    {this.state.configurationInfo.map((opt, index) => {
                                        return (
                                            <option
                                                key={index}
                                                value={opt.configurationName}>{opt.configurationName}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <a className={(this.state.activateCreateList ? 'relayLinks' : 'disabled')} onClick={() => this.showEditDialogBox('createModal')} >{RA_STR.createModalconst}</a>
                            <div id="createModal" className="modal">
                                <div className="modal-content">
                                    <div className="dialog-header" className="successheader">
                                        <div className="dialog-title"><span className="label">{RA_STR.createconfigurationmodaltitle}</span></div>
                                        <div className="dialog-close" className="close" onClick={() => this.handleClose('createModal')}> <span className="close">&times;</span></div>
                                    </div>
                                    <div className="modal-body">
                                        <div className='pt-3 pb-3'> {RA_STR.createconfigurationmodalheading}</div>
                                        <div className='pb-3'>{RA_STR.createconfigurationmodaltextheading}</div>
                                        <input type='text' className='form-control' name="newConfiguration" maxLength="32" style={{ maxWidth: '250px' }} value={this.state.newConfiguration} onChange={this.handleCreateField} />
                                        <div className='pt-3'><b>{RA_STR.createswappingheading}</b></div>
                                        <div className='row' >
                                            <SwappableGrid ref="swappableGridFieldRef" leftHeader={'Avaliable Fields'} rightHeader={'Selected Fields'} ></SwappableGrid>
                                        </div>
                                        <div className="form-inline">
                                            <div className="form-group form-submit-button mt-2">
                                                <input className="custom-secondary-btn" id="RESET" type="button" value="CANCEL" onClick={() => this.handleClose('createModal')} />
                                            </div>
                                            <div className="form-group form-submit-button ml-2 mt-3">
                                                <input className="secondary-btn" id="Active" type="submit" value="SAVE" onClick={this.saveConfiguration} ></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            <a className={(this.state.activiateEditList ? 'relayLinks' : 'disabled')} onClick={() => this.showEditDialogBox('editModal')}>| {RA_STR.editModalconst}</a>
                            <div id="editModal" className="modal">
                                <div className="modal-content">
                                    <div className="dialog-header" className="successheader">
                                        <div className="dialog-title"><span className="label">{RA_STR.editconfigurationmodaltitle}</span></div>
                                        <div className="dialog-close" className="close" onClick={() => this.handleClose('editModal')}> <span className="close">&times;</span></div>
                                    </div>
                                    <div className="modal-body">
                                        <div className='pt-3 pb-3'> {RA_STR.editconfigurationmodalheading}</div>
                                        <div className='pb-3'>{RA_STR.createconfigurationmodaltextheading}</div>
                                        <input type='text' className='form-control' style={{ maxWidth: '250px' }} value={this.state.configurationFieldName} disabled='disabled' />
                                        <div className='pt-3'><b>{RA_STR.createswappingheading}</b></div>
                                        <div className='row' >
                                            <SwappableGrid ref="swappableupdateGridRef" leftHeader={'Avaliable Fields'} rightHeader={'Selected Fields'}  ></SwappableGrid>
                                        </div>
                                        <div className="form-inline">
                                            <div className="form-group form-submit-button mt-2">
                                                <input className="custom-secondary-btn" id="RESET" type="button" value="CANCEL" onClick={() => this.handleClose('editModal')} />
                                            </div>
                                            <div className="form-group form-submit-button ml-2 mt-3">
                                                <input className="secondary-btn" id="Active" type="submit" value="SAVE" onClick={this.updateConfiguration} ></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div> <b> {RA_STR.organisationHeading}</b></div>
                <div className='row' >
                    <SwappableGrid ref="swappableGridRef" leftHeader={'Avaliable organisations'} rightHeader={'Selected Organizations'}  ></SwappableGrid>
                </div>
                <input className="secondary-btn mt-5" id="Active" type="submit" value="SAVE" onClick={this.saveConfigurationInfo} ></input>
            </div>
        )
    }
}

export default ConfigureFieldsRelay;