import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';

class WorkOnCases extends Component {
  render() {
    const localeOptions = [];
    return (
      <div className='main'>
        <h2 className='title'>Work on Cases</h2>
        {/* <p className='desc'>Specify the criteria to generate the report. The dates used here are in GMT.</p> */}
        <hr />
        <div className='col-sm-8'>
        <Select
            name={'orgName'}
            title={'Organization Name'}
            options={localeOptions ? localeOptions : ['']}
            controlFunc={this.handleChange} />
        </div>
      </div>
    )
  }
}

export default WorkOnCases;
