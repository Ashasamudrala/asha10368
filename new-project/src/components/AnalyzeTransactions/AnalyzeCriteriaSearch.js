import React, { Component } from 'react';
import DatePickerWithTimer from '../SearchCases/DateRangeWithTimer';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, serverUrl, RA_STR_SELECT } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';

class CriteriabasedSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            riskAdviceData: [],
            secondaryAuthList: [],
            fraudStatusData: [],
            rulesList: [],
            accountTypesData: [],
            lastCaseOptions: [30, 60, 120, 180],
            ruleImBuneList: [],
            ipListOptions: [{ key: 1, content: RA_STR.Exact }, { key: 2, content: RA_STR.ipLess }]
        }
        this.lastCaseOptions = [];
    }
    componentWillMount() {
        let lastCaseOptions = this.state.lastCaseOptions.map(option => {
            return {
                key: option,
                content: option + RA_STR.mins
            }
        })
        this.setState({ lastCaseOptions });
    }
    getDateRanges() {
        let dateranges = {};
        if (this.refs.DatepickerTimerRef) {
            dateranges = this.refs.DatepickerTimerRef.getDateRanges();
        }
        return dateranges;
    }
    async getRuleimbunes() {
        let { SelectOrganization } = this.props.accessParent.state;
        const url = RA_API_URL.getRuleimbunes.replace(RA_STR.Org_Rep, SelectOrganization)
        const response = await getService({ method: 'GET', url: serverUrl + url });
        if (response && response.status === 200) {
            let ruleImBuneList = response.data.ruleImBuneList.map(obj => { return { key: obj.displayname, content: obj.displayname } });
            ruleImBuneList = [{ key: RA_STR.select, content: RA_STR.Select }, ...ruleImBuneList];
            this.setState({ ruleImBuneList });
        }
    }

    async getAccountTypes() {
        let { SelectOrganization } = this.props.accessParent.state;
        const url = RA_API_URL.getAccountTypes.replace(RA_STR.Org_Rep, SelectOrganization)
        const response = await getService({ method: 'GET', url: serverUrl + url });
        if (response && response.status === 200) {
            let accountTypesData = response.data.map(obj => { return { key: obj.accountType, content: obj.displayName } });
            this.setState({ accountTypesData });
        }
    }
    async getRules() {
        let { SelectOrganization } = this.props.accessParent.state;
        const url = RA_API_URL.getRules.replace(RA_STR.Org_Rep, SelectOrganization)
        const response = await getService({ method: 'GET', url: serverUrl + url });
        if (response && response.status === 200) {
            let rulesList = response.data.map(obj => { return { key: obj.ruleDisplay, content: obj.ruleDisplay } });
            rulesList = [{ key: '-1', content: RA_STR.allRules }, ...rulesList]
            this.setState({ rulesList });
        }
    }


    async getRiskAdviceList() {
        let { SelectOrganization } = this.props.accessParent.state;
        const url = RA_API_URL.getAdviceData.replace(RA_STR.Org_Rep, SelectOrganization)
        const response = await getService({ method: 'GET', url: serverUrl + url });
        if (response && response.status === 200) {
            let riskadviceKeys = Object.keys(response.data);
            let riskAdviceData = Object.values(response.data).map((obj, index) => { return { key: riskadviceKeys[index], content: obj } })
            riskAdviceData = [{ key: '-1', content: RA_STR.allAdvices }, ...riskAdviceData];
            this.setState({ riskAdviceData });
        }
    }

    async getSecondaryAuthDataList() {
        let { SelectOrganization } = this.props.accessParent.state;
        const url = RA_API_URL.getSecondaryAuthData.replace(RA_STR.Org_Rep, SelectOrganization)
        const response = await getService({ method: 'GET', url: serverUrl + url });
        if (response && response.status === 200) {
            let secondaryAuthKeys = Object.keys(response.data);
            let secondaryAuthList = Object.values(response.data).map((obj, index) => { return { key: secondaryAuthKeys[index], content: obj } })
            secondaryAuthList = [{ key: '-1', content: RA_STR.allStatuses }, ...secondaryAuthList];
            this.setState({ secondaryAuthList });
        }
    }

    async getFraudStatusList() {
        let { SelectOrganization } = this.props.accessParent.state;
        const url = RA_API_URL.getFraudStatus.replace(RA_STR.Org_Rep, SelectOrganization);
        const response = await getService({ method: 'GET', url: serverUrl + url });
        if (response && response.status === 200) {
            let fraudStatusKeys = Object.keys(response.data);
            let fraudStatusData = Object.values(response.data).map((obj, index) => { return { key: fraudStatusKeys[index], content: obj } })
            fraudStatusData = [{ key: '-1', content: RA_STR.allStatuses }, ...fraudStatusData];
            this.setState({ fraudStatusData });
        }
    }
    getDynamicUIElement(elementsdata) {
        let AllElements = [];
        let { partialMapData } = this.props.accessParent.state;
        let { ruleImBuneList, ipListOptions } = this.state;
        if (elementsdata) {
            elementsdata.map((data, index) => {
                if (!data.value) {
                    data.value = '';
                }
                if (!data.partialMatch) {
                    data.partialMatch = '';
                }
                if (data.displayType === RA_STR.textBox) {
                    if (data.elementName === RA_STR.RA_COM_EXEMPTIONCLAIMED) {
                        AllElements = [...AllElements, <div className="setInputWidth" key={index}><Select key={index}
                            name={data.elementName}
                            title={data.displayNameStr}
                            options={ruleImBuneList}
                            placeholder={RA_STR_SELECT}
                            selectedOption={data.value}
                            controlFunc={this.props.accessParent.handleDynamicInputs}
                        /></div >]
                    } else if (data.elementName == RA_STR.CLIENTIPADDRESS) {
                        if (data.showSelectInput === undefined) {
                            data.showSelectInput = true;
                            data.ipselectoption = 1;
                        }
                        AllElements = [...AllElements, <div className="row mini-select" key={index}>
                            <div className="col-sm-4 pl-0">{data.displayNameStr}</div>
                            <div className="col-sm-8 row">
                                <div className="col-sm-5">
                                    <Select
                                        name={data.elementName}
                                        options={ipListOptions}
                                        content={data.ipselectoption}
                                        controlFunc={this.props.accessParent.handleDynamicInputs}
                                    />
                                </div>
                                {data.showSelectInput && <div className="col-sm-7">
                                    <SingleInput
                                        title={''}
                                        name={data.elementName}
                                        inputType={'text'}
                                        content={data.value}
                                        controlFunc={this.props.accessParent.handleDynamicInputs}
                                    />
                                </div>}
                            </div>
                        </div>]
                    } else {
                        AllElements = [...AllElements, <div className="setInputWidth" key={index}> <SingleInput key={index}
                            name={data.elementName}
                            title={data.displayNameStr}
                            inputType={'text'}
                            content={data.value}
                            controlFunc={this.props.accessParent.handleDynamicInputs} /></div>]
                    }
                } else if (data.displayType === RA_STR.TEXTBOXPARTIAL) {
                    AllElements = [...AllElements, <div className="row mini-select" key={index}>
                        <div className="col-sm-4 pl-0">{data.displayNameStr}</div>
                        <div className="col-sm-8 row">
                            <div className="col-sm-5">
                                <Select
                                    name={data.elementName}
                                    options={partialMapData}
                                    placeholder={RA_STR_SELECT}
                                    selectedOption={data.partialMatch}
                                    controlFunc={this.props.accessParent.handleDynamicInputs}
                                />
                            </div>
                            <div className="col-sm-7">
                                <SingleInput
                                    title={''}
                                    name={data.elementName}
                                    inputType={'text'}
                                    content={data.value}
                                    controlFunc={this.props.accessParent.handleDynamicInputs}
                                />
                            </div>
                        </div>
                    </div>]
                } else if (data.displayType === RA_STR.DROPDOWN && data.transactionSummaryBean.searchCriteria.customDropDownMap) {
                    let Allkeys = Object.keys(data.transactionSummaryBean.searchCriteria.customDropDownMap);
                    let Alloptions = Object.values(data.transactionSummaryBean.searchCriteria.customDropDownMap).map((obj, index) => {
                        return { key: Allkeys[index], content: obj }
                    });
                    AllElements = [...AllElements, <div className="setInputWidth" key={index}><Select
                        name={data.elementName}
                        title={data.displayNameStr}
                        options={Alloptions}
                        placeholder={RA_STR_SELECT}
                        selectedOption={data.value}
                        controlFunc={this.props.accessParent.handleDynamicInputs}
                    /></div>]
                }
            })
            return AllElements;
        }
    }
    componentDidMount() {
        this.getRiskAdviceList();
        this.getSecondaryAuthDataList();
        this.getFraudStatusList();
        this.getRules();
        this.getAccountTypes();
        this.getRuleimbunes();
    }

    render() {
        let { riskAdvices, riskAdviceList, secAuthStatusList, fraudStatusList, caseDataType, userName, selectedChannel, otherSearchExtElements, channelsList, lastTxnTime } = this.props.accessParent.state;
        let { riskAdviceData, secondaryAuthList, fraudStatusData, lastCaseOptions, accountTypesData } = this.state;
        return (
            <div className="criteriaTab">
                <div className="row">
                    <div className="col-sm-6">
                        {this.props.accessParent.SelectTheOrganization}
                        <Select
                            name='selectedChannel'
                            title='Select Channel'
                            options={channelsList}
                            selectedOption={selectedChannel}
                            controlFunc={this.props.accessParent.handleChange} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <SingleInput
                            title={'Enter Card Number'}
                            inputType={'text'}
                            name={'userName'}
                            content={userName}
                            controlFunc={this.props.accessParent.handleChange} />
                    </div>
                    {accountTypesData.length > 0 && <div className="col-sm-5">
                        <Select
                            name='SelectChannel'
                            title=''
                            options={accountTypesData}
                            placeholder={'--Select Search Type--'}
                            selectedOption={''}
                            controlFunc={this.props.accessParent.handleChange}
                        />
                    </div>}
                </div>
                <div className="noMarginrow mt-6">
                    <div className="col-sm-2">
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input"
                                id="caseDataTypeparent"
                                name="caseDataType"
                                value='0'
                                checked={caseDataType == '0'}
                                onChange={this.props.accessParent.handleChange} />
                            <label className="custom-control-label" htmlFor="caseDataTypeparent">{RA_STR.transDateRange}</label>
                        </div>
                    </div>
                    <DatePickerWithTimer ref='DatepickerTimerRef' disableAll={caseDataType == '1'} ></DatePickerWithTimer>
                </div>
                <div className="noMarginrow mt-4">
                    <div className="col-sm-2">
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="caseDataTypeChild"
                                name="caseDataType"
                                value='1'
                                checked={caseDataType == '1'}
                                onChange={this.props.accessParent.handleChange}
                            />
                            <label className="custom-control-label" htmlFor="caseDataTypeChild">{RA_STR.lastTransactions}</label>
                        </div>
                    </div>
                    <div className="col-sm-5 disableLight">
                        <Select
                            name='lastTxnTime'
                            title=''
                            options={lastCaseOptions}
                            selectedOption={lastTxnTime}
                            controlFunc={this.props.accessParent.handleChange}
                            disabled={caseDataType == '0'}
                        />
                    </div>
                </div>
                <div className="row multiple-select-zone">
                    <div className="col-sm-3 pl-0">
                        <label className="col-sm-12 pl-0">{RA_STR.RiskAdvice}</label>
                        <Select
                            name='riskAdviceList'
                            title=''
                            multiple='true'
                            options={riskAdviceData}
                            content={riskAdvices}
                            selectedOption={riskAdviceList}
                            controlFunc={this.props.accessParent.handleMultipleSelect}
                        />
                    </div>
                    <div className="col-sm-3 margin-minus second">
                        <label className="col-sm-12 pl-0">{RA_STR.secAuthStatus}</label>
                        <Select
                            name='secAuthStatusList'
                            title=''
                            multiple='true'
                            options={secondaryAuthList}
                            selectedOption={secAuthStatusList}
                            controlFunc={this.props.accessParent.handleMultipleSelect}
                        />
                    </div>
                    <div className="col-sm-3 margin-minus">
                        <label className="col-sm-12 pl-0">{RA_STR.fraudStatus}</label>
                        <Select
                            name='fraudStatusList'
                            title=''
                            multiple='true'
                            options={fraudStatusData}
                            selectedOption={fraudStatusList}
                            controlFunc={this.props.accessParent.handleMultipleSelect}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-8">
                        {this.getDynamicUIElement(otherSearchExtElements)}
                    </div>
                </div>
            </div>
        )
    }
}
export default CriteriabasedSearch;