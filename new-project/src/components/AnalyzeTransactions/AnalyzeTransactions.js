import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, serverUrl, RA_STR_SELECT, LOADING_TEXT } from '../../shared/utlities/constants';
import './AnalyzeTransactions.scss';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import Loader from '../../shared/utlities/loader';
import '../SearchCases/SearchCases.scss';
import CriteriabasedSearch from './AnalyzeCriteriaSearch.js';
import AnalyzeReports from './reports.js';
import { saveAs } from 'file-saver';

class AnalyzeTransactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      organizationList: [],
      originalOrganizations: [],
      selectedTab: "",
      decrypt: true,
      SelectOrganization: "",
      pageLoaded: true,
      riskAdvices: [],
      setOrganizations: [],
      reportPage: '',
      riskAdviceList: [-1],
      secAuthStatusList: [-1],
      fraudStatusList: [-1],
      selectedRiskRule: '-1',
      selectedChannel: RA_STR.All,
      userName: '',
      accountTypesData: '',
      otherSearchExtElements: [],
      partialMapData: [],
      caseDataType: 1,
      decryptionNeeded: true,
      channelsList: [],
      transactionList: [],
      lastTxnTime: '30',
      txnID: ''
    }
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  handleDynamicInputs = (e) => {
    let { name, value, type } = e.target;
    let { otherSearchExtElements } = this.state;
    otherSearchExtElements.map(obj => {
      if (obj.displayNameStr === name || obj.elementName === name) {
        if (type === RA_STR.TexT || obj.displayType === RA_STR.DROPDOWN) {
          obj.value = value;
        } else if (name === RA_STR.CLIENTIPADDRESS) {
          if (value == 1) {
            obj.showSelectInput = true;
            obj.value = '';
          } else {
            obj.showSelectInput = false;
            obj.value = RA_STR.UNSPECIFIED;
          }
          obj.ipselectoption = value;
        } else {
          obj.partialMatch = value;
        }
      }
    });
    this.setState({ otherSearchExtElements });
  }
  onchange = (e) => {
    this.setState({ [e.target.name]: e.target.checked });
  }
  async setSearchCriteria() {
    let { SelectOrganization, selectedChannel } = this.state;
    const url = RA_API_URL.setSearchCriteria.replace(RA_STR.Org_Rep, SelectOrganization).replace(RA_STR.Channel_rep, selectedChannel);
    const response = await getService({ method: 'GET', url: serverUrl + url });
    if (response && response.status === 200) {
      let otherSearchExtElements = response.data.searchCriteria.otherSearchExtElements;
      let partialMapkeys = Object.values(response.data.searchCriteria.partialMap);
      let partialMapData = Object.keys(response.data.searchCriteria.partialMap).map((data, index) => {
        return { key: partialMapkeys[index], content: data }
      })
      this.setState({ otherSearchExtElements, partialMapData });
    }
  }
  async getChannels() {
    let { SelectOrganization } = this.state;
    const url = RA_API_URL.getChannels.replace(RA_STR.Org_Rep, SelectOrganization)
    const response = await getService({ method: 'GET', url: serverUrl + url });
    if (response && response.status === 200) {
      let channelsList = response.data.map(obj => { return { key: obj.channelName, content: obj.channelName } })
      channelsList = [{ key: RA_STR.All, content: RA_STR.allChannels }, ...channelsList]
      this.setState({ channelsList });
    }
  }
  getOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL.searchactions}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      let organizations = [RA_STR_SELECT, ...CA_Utils.objToArray(response.data.organizations, 'string')];
      this.setState({
        organizationList: organizations,
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object'),
      });
    }
  };
  componentWillMount() {
    this.getOrganizations();
  }
  selectTab = (Tab) => {
    this.setState({ selectedTab: Tab });
  }
  exportReports = async () => {
    let { SelectOrganization, otherSearchExtElements, decryptionNeeded, selectedChannel, lastTxnTime, caseDataType, selectedTab, txnID } = this.state;
    var dateObj = {};
    if (this.refs.criteriabasedRef && caseDataType == 0) {
      dateObj = this.refs.criteriabasedRef.getDateRanges();
      this.savePreviousDateObj = dateObj.saveDateObj;
      dateObj = dateObj.analyzedata;
      lastTxnTime = null;
    } else {
      dateObj = {
        transactionFromDate: null,
        transactionToDate: null,
        transactionFromHrs: null,
        transactionFromMins: null,
        transactionFromSecs: null,
        transactionToHrs: null,
        transactionToMins: null,
        transactionToSecs: null,
        lastTxnTime
      }
    }
    let data;
    let url = RA_API_URL.exportAnalyzeTrans;
    if (selectedTab === RA_STR.Criteria) {
      data = {
        searchCriteria: {
          ...dateObj,
          whereClauseList: null,
          otherSearchExtElements
        },
        selectedOrg: SelectOrganization,
        decryptionNeeded: decryptionNeeded ? 1 : 0,
        selectedChannel,
        startIndex: 0
      }
    } else {
      data = {
        searchCriteria: {
          otherSearchExtElements: [],
          txnID: txnID ? parseInt(txnID) : 0,
          filterType: 1
        },
        selectedOrg: SelectOrganization,
        decryptionNeeded: decryptionNeeded ? 1 : 0,
        selectedChannel,
        startIndex: 0
      }
    }
    const response = await getService({ method: 'POST', url: serverUrl + url, data });
    if (response && response.status === 400) {
      this.props.activateErrorList(true, response.data.errorList);
    } else if (response.status === 200) {
      if (response && response.data) {
        const blob = new Blob([response.data], { type: 'application/octet-stream' });
        let { transactionFromDate, transactionToDate } = dateObj;
        let exportName;
        if (transactionFromDate) {
          exportName = RA_STR.Transaction_Summary + transactionFromDate + transactionToDate
        } else {
          exportName = RA_STR.Transaction_Summary;
        }
        saveAs(blob, exportName + '.csv');
      }
    }
  }
  submitAnalyzeReport = async (e, ShowThisData) => {
    let { SelectOrganization, otherSearchExtElements, decryptionNeeded, selectedChannel, lastTxnTime, caseDataType, selectedTab, txnID } = this.state;
    var dateObj = {};
    if (this.refs.criteriabasedRef && caseDataType == 0) {
      dateObj = this.refs.criteriabasedRef.getDateRanges();
      this.savePreviousDateObj = dateObj.saveDateObj;
      dateObj = dateObj.analyzedata;
      lastTxnTime = null;
    } else {
      dateObj = {
        transactionFromDate: null,
        transactionToDate: null,
        transactionFromHrs: null,
        transactionFromMins: null,
        transactionFromSecs: null,
        transactionToHrs: null,
        transactionToMins: null,
        transactionToSecs: null,
        lastTxnTime
      }
    }
    this.toDate = dateObj.transactionFromDate;
    this.endDate = dateObj.transactionToDate;
    let data;
    let url = RA_API_URL.postTransactions;
    if (selectedTab === RA_STR.Criteria) {
      data = {
        searchCriteria: {
          ...dateObj,
          whereClauseList: null,
          otherSearchExtElements
        },
        selectedOrg: SelectOrganization,
        decryptionNeeded: decryptionNeeded ? 1 : 0,
        selectedChannel,
        startIndex: 0
      }
      if (ShowThisData) {
        data = ShowThisData;
        let { transactionFromDate, transactionToDate } = data.searchCriteria;
        this.toDate = transactionFromDate;
        this.endDate = transactionToDate;
        url = RA_API_URL.showRelatedTrans;
      }
    } else {
      if (ShowThisData) {
        data = ShowThisData;
      } else {
        data = {
          searchCriteria: {
            otherSearchExtElements: [],
            txnID: txnID ? parseInt(txnID) : 0,
            filterType: 1
          },
          selectedOrg: SelectOrganization,
          decryptionNeeded: decryptionNeeded ? 1 : 0,
          selectedChannel,
          startIndex: 0

        }
      }

    }
    const response = await getService({ method: 'POST', url: serverUrl + url, data });
    if (response && response.status === 200) {
      this.props.activateErrorList(false, '');
      let tabledata = [], getColumns = [];
      response.data.transactionList.map((obj, index) => {
        let childData = {
          txnID: obj.txnID,
          instanceID: obj.instanceID,
          selectedChannel: response.data.selectedChannel,
          alertStatus: obj.alertStatus,
          userName: obj.userName
        };
        obj.summaryExtElements.map(innerobj => {
          if (index === 0) {
            if (innerobj.displayNameStr === RA_STR.ruleResults || innerobj.displayNameStr === RA_STR.deviceId) {
              getColumns.push({ title: innerobj.displayNameStr, field: innerobj.elementName, render: rowData => <span title={innerobj.elementName && rowData[innerobj.elementName]}>{innerobj.elementName && rowData[innerobj.elementName] && rowData[innerobj.elementName].substring(0, 35)}....</span> });
            } else {
              getColumns.push({ title: innerobj.displayNameStr, field: innerobj.elementName });
            }
          }
          childData[innerobj.elementName] = innerobj.value;
        });
        tabledata.push({ ...childData, index: index + 1 });
      })
      this.setState({ reportPage: RA_STR.TransactionPage }, () => {
        if (this.refs.reportsRef) {
          this.refs.reportsRef.setColumns(getColumns, tabledata);
        }
      })
    } else if (response && response.status === 400) {
      // this.setState({ reportPage: '' });
      this.props.activateErrorList(true, response.data.errorList);
    } else if (response && response.status === 500) {
      this.props.activateErrorList(true, response.data.errorList);
    }
  }

  onOrgSelect = (value, type) => {
    if (type === RA_STR.click || type === RA_STR.blur) {
      let { selectedTab, originalOrganizations } = this.state;
      const orgId = originalOrganizations.find((element) => { if (element.content === value) return element.key });
      if (selectedTab === '' && orgId) {
        selectedTab = RA_STR.Criteria;
      }
      if ((type === RA_STR.blur && !orgId && this.refs.autoSuggestionBox) || (value === RA_STR_SELECT && this.refs.autoSuggestionBox)) {
        this.refs.autoSuggestionBox.setvalue('');
      } else {
        this.setState({
          SelectOrganization: orgId ? orgId.key : '',
          selectedTab,
          selectedChannel: RA_STR.All,
          accountTypesData: ''
        }, () => {
          this.setSearchCriteria();
          if (this.refs.criteriabasedRef) {
            this.refs.criteriabasedRef.getAccountTypes();
            this.getChannels();
          }
        })
      }
    }
  }

  handleMultipleSelect = (e) => {
    let { name, selectedOptions } = e.target;
    if (selectedOptions) {
      this.setState({ [name]: Array.from(selectedOptions, (item) => item.value) });
    }
  }
  render() {
    let { organizationList, selectedTab, decryptionNeeded, SelectOrganization, pageLoaded, reportPage, channelsList, selectedChannel, txnID } = this.state;
    this.SelectTheOrganization = <div className="form-group row">
      <label className="col-sm-4 col-form-label">{RA_STR.selectOrg}</label>
      <div className='col-sm-8' ref='autoSuggestContainer'>
        <AutoSuggest ref="autoSuggestionBox" orgOptions={organizationList} getSelectedOrgDetails={this.onOrgSelect} enableAutosuggest={true} defaultOrganizationPlaceholder={RA_STR_SELECT} value={SelectOrganization} />
      </div>
    </div>
    return (
      <div className='main AnalyzeTransactions searchCases'>
        {!pageLoaded ? <div><Loader show='true' />{LOADING_TEXT}</div>
          :
          <div>
            {SelectOrganization === '' && selectedTab === '' ?
              <div>
                <h2 className='title'>{RA_STR.analyzeTransactions}</h2>
                <p className='desc'>{RA_STR.analyzeTransDesc}</p>
                <div className='col-sm-5'>
                  {this.SelectTheOrganization}
                </div>
              </div>
              :
              (reportPage !== '' ? <AnalyzeReports accessParent={this} ref="reportsRef"></AnalyzeReports> :
                <div>
                  <div>
                    <h2 className='title'>{RA_STR.analyzeTransactions}</h2>
                    <p className='desc'>{RA_STR.analyzeTransDesc}</p>
                    <ul className="nav nav-pills form-inline">
                      <li className={"nav-item " + (selectedTab === RA_STR.Criteria && 'selectedTab')} onClick={() => this.selectTab(RA_STR.Criteria)}> {RA_STR.criteriaBasedSearch} </li>
                      <li className={"nav-item " + (selectedTab !== RA_STR.Criteria && 'selectedTab')} onClick={() => this.selectTab('Transaction')}>
                        {RA_STR.transcBasedSearch} </li>
                    </ul>
                  </div>
                  <div className="tabui">
                    {selectedTab === RA_STR.Criteria ?
                      <CriteriabasedSearch accessParent={this} ref='criteriabasedRef'></CriteriabasedSearch>
                      :
                      <div className="searchbyCaseIdTab transactionBasedSearch">
                        <div className="row">
                          <div className="col-sm-6">
                            {this.SelectTheOrganization}
                            <Select
                              name='selectedChannel'
                              title='Select Channel'
                              options={channelsList}
                              selectedOption={selectedChannel}
                              controlFunc={this.handleChange}
                            />
                            <SingleInput
                              title={'Transaction ID'}
                              inputType={'text'}
                              name={'txnID'}
                              content={txnID}
                              controlFunc={this.handleChange} />
                          </div>
                        </div>
                      </div>
                    }
                    <div className="row mt-5 caseSubmit tabui">
                      <div className="col-sm-9 row pl-0">
                        <div className="col-sm-5 pl-0">
                          <div className="custom-control custom-checkbox">
                            <label key='decryptionNeeded' className='form-label capitalize col-sm-12' >
                              <input
                                className="form-checkbox custom-control-input"
                                type='checkbox'
                                id='decryptionNeeded'
                                name='decryptionNeeded'
                                onChange={this.onchange}
                                value={decryptionNeeded}
                                checked={decryptionNeeded}
                              />
                              <label className="form-label custom-control-label" htmlFor='decryptionNeeded'> {RA_STR.DecryptInfo}</label>
                            </label>
                          </div>
                        </div>
                        <div className="col-sm-8 buttons-tran">
                          <div className="form-group row">
                            <input className="grey-button" id="searchButton" type="submit" value="EXPORT" onClick={this.exportReports}></input>
                            <input className="grey-button" id="searchButton" type="submit" value="EXPORT CUSTOM" onClick={this.exportReports}></input>
                            <input className="secondary-button" id="searchButton" type="submit" value="SUBMIT" onClick={this.submitAnalyzeReport}></input>
                          </div>
                        </div>
                      </div>
                    </div >
                  </div>
                </div>
              )
            }
          </div>
        }
      </div>
    )
  }
}

export default AnalyzeTransactions;
