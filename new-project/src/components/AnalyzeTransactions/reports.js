import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import DatePickerWithTimer from '../SearchCases/DateRangeWithTimer';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, serverUrl } from '../../shared/utlities/constants';
import ReactDOM from 'react-dom';

class AnalyzeReports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    title: 'check', field: 'transactionId', render: rowData =>
                        <input type="checkbox" className="ml-2" name="te" style={{ zoom: '1.5' }} disabled={rowData.alertStatus == 1 || !rowData.userName || true} />
                },
                { title: 'No.', field: 'index' },
                { title: 'Details', field: 'transactionId', render: rowData => <a className="orgName" onClick={() => this.getDetailsOfTransactions(rowData)}>Detail</a> },
            ],
            transactionList: [],
            detailLayouts: [],
            detailExtElements: [],
            selectedTxnDetails: {},
            showRelatedExtElements: [],
            caseDataType: 0,
            lastCaseOptions: [30, 60, 120, 180],
            lastTxnTime: '30',
            selectedTab: RA_STR.threeDSecure
        }
    }
    componentWillUnmount() { }
    checkAll = (e) => {
        let { checked, value } = e.target;
    }
    setCheckboxInHeader() {
        const currentDom = ReactDOM.findDOMNode(this);
        if (currentDom) {
            let getTableNode = currentDom.querySelectorAll('th.MuiTableCell-root');
            if (getTableNode && getTableNode[0]) {
                getTableNode[0].innerHTML = '<input class="checkALL ml-2" type="checkbox" name="te" style="zoom: 1.5;"/>';
                currentDom.querySelector(".checkALL") && currentDom.querySelector(".checkALL").addEventListener("change", this.checkAll);
            }
        }
    }
    componentWillMount() {
        let { lastTxnTime } = this.props.accessParent.state;
        if (this.state.lastCaseOptions) {
            let lastCaseOptions = this.state.lastCaseOptions.map(option => {
                return {
                    key: option,
                    content: option + RA_STR.mins
                }
            })
            this.setState({ lastCaseOptions, lastTxnTime });
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    handleCheckbox = (e) => {
        let { name, checked } = e.target;
        let { showRelatedExtElements, detailExtElements } = this.state;
        if (showRelatedExtElements) {
            showRelatedExtElements.map(element => {
                if (element.elementName === name) {
                    element.searchOnThis = checked;
                    var findExtElement = detailExtElements.filter(elementObj => elementObj.elementName === name)[0];
                    if (findExtElement) {
                        element.value = findExtElement.value;
                    }
                }
            })
            this.setState({ showRelatedExtElements });
        }
    }
    getDetailsOfTransactions = async (obj) => {
        let { SelectOrganization, decryptionNeeded } = this.props.accessParent.state;
        let { txnID, instanceID, selectedChannel } = obj;
        let data = {
            selectedOrg: SelectOrganization,
            selectedChannel,
            decryptionNeeded: decryptionNeeded ? 1 : 0,
            searchCriteria: {
                txnID,
                instanceID,
            }
        }
        const response = await getService({ method: 'POST', url: serverUrl + RA_API_URL.analyzeTransDetails, data });
        if (response && response.status === 200) {
            this.setState(
                {
                    detailLayouts: response.data.channelBean.detailLayouts,
                    detailExtElements: response.data.selectedTxnDetails.detailExtElements,
                    selectedTxnDetails: response.data.selectedTxnDetails,
                    showRelatedExtElements: response.data.selectedTxnDetails.showRelatedExtElements
                });
        }
    }
    setColumns = (addcolumns, transactionList) => {
        let { columns } = this.state;
        this.setState({ columns: [...columns, ...addcolumns], transactionList });
        this.setCheckboxInHeader();
    }
    setPage = () => {
        this.props.accessParent.setState({ reportPage: '' });
    }
    getcheckboxselection = (getData) => {
        let groupOrgNames = [];
        if (getData.length) {
            getData.forEach((eachData) => {
                groupOrgNames.push(eachData.orgName);
            })
        } else {
            groupOrgNames = [];
        }
        this.setState({ selectedOrgNames: groupOrgNames });
    }
    showReport = (e, type) => {
        let { SelectOrganization, decryptionNeeded, selectedChannel } = this.props.accessParent.state;
        let { showRelatedExtElements, caseDataType, lastTxnTime } = this.state;
        let { toDate, endDate } = this.props.accessParent;

        var dateObj = {};
        if (this.refs.DatepickerTimerID && caseDataType == '0') {
            dateObj = this.refs.DatepickerTimerID.getDateRanges();
            dateObj = dateObj.analyzedata;
            lastTxnTime = null;
        } else {
            dateObj = {
                transactionFromDate: null,
                transactionToDate: null,
                transactionFromHrs: null,
                transactionFromMins: null,
                transactionFromSecs: null,
                transactionToHrs: null,
                transactionToMins: null,
                transactionToSecs: null,
                lastTxnTime
            }
        }
        if (type) {
            selectedChannel = RA_STR.DEFAULT;
            dateObj = {
                transactionFromDate: endDate,
                transactionToDate: toDate,
                transactionFromHrs: null,
                transactionFromMins: null,
                transactionFromSecs: null,
                transactionToHrs: null,
                transactionToMins: null,
                transactionToSecs: null,
                lastTxnTime
            }
        }
        const data = {
            searchCriteria: {
                ...dateObj,
                whereClauseList: null
            },
            selectedTxnDetails: { showRelatedExtElements },
            selectedOrg: SelectOrganization,
            decryptionNeeded: decryptionNeeded ? 1 : 0,
            selectedChannel,
            startIndex: 0
        }
        this.setState({
            detailLayouts: [],
            detailExtElements: [],
            selectedTxnDetails: {},
            showRelatedExtElements: []
        });
        this.props.accessParent.submitAnalyzeReport(e, data);
    }
    setBoxRows = (layoutData) => {
        let { detailExtElements } = this.state;

        if (detailExtElements) {
            detailExtElements = detailExtElements.filter(extElement => {
                if (extElement.categoryID == layoutData.boxID && extElement) {
                    return extElement;
                };
            });
        }
        return <>
            <div className="col-sm-5 details-card">

                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {(index % 2 === 0) && extElement.categoryID == layoutData.boxID &&
                            <div>
                                <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label>
                                <label className="col-sm-5 col-form-label pl-0 break-all">{extElement.value}</label>
                            </div>
                        }
                    </>
                )}
            </div>
            <div className="col-sm-5 ml-2 details-card">
                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {index % 2 === 1 && extElement.categoryID == layoutData.boxID &&
                            <div>  <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label> {extElement.value} </div>
                        }
                    </>
                )}
            </div>
        </>
    }
    setAdditionalrows = (layoutData) => {
        let { detailExtElements, selectedTxnDetails } = this.state;

        if (detailExtElements) {
            detailExtElements = detailExtElements.filter(extElement => {
                if (extElement.categoryID == layoutData.boxID && extElement && (extElement.isStoredExtElement == '1' && extElement.isExtensible == '1') || (extElement.isStoredExtElement == '2' && extElement.isExtensible == '1')) {
                    return extElement;
                };
            })
        }

        return <>
            <div className="col-sm-5 details-card">

                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {(index % 2 === 0) &&
                            <div>  <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label> {selectedTxnDetails.blobExtensibleElementsMap[extElement.elementName]} </div>
                        }
                    </>
                )}
            </div>
            <div className="col-sm-5 ml-2 details-card">
                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {index % 2 === 1 &&
                            <div>  <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label> {selectedTxnDetails.blobExtensibleElementsMap[extElement.elementName]}</div>
                        }
                    </>
                )}
            </div>
        </>
    }
    setRiskruleSetRows = () => {
        let { selectedTxnDetails } = this.state;
        return <>
            <div className="col-sm-5 details-card">
                <div>
                    <label className="col-sm-9 col-form-label">
                        {RA_STR.mfpMatch}
                    </label> {selectedTxnDetails.mfpMatchPercentageString}
                </div>
                {selectedTxnDetails && selectedTxnDetails.addOnRuleResultMap && Object.keys(selectedTxnDetails.addOnRuleResultMap).map((extElement, index) =>
                    <>
                        {(index % 2 === 1) &&
                            <div>  <label className="col-sm-9 col-form-label">{extElement}</label> {selectedTxnDetails.addOnRuleResultMap[extElement]} </div>
                        }
                    </>
                )}
            </div>
            <div className="col-sm-5 ml-2 details-card">
                {selectedTxnDetails && selectedTxnDetails.addOnRuleResultMap && Object.keys(selectedTxnDetails.addOnRuleResultMap).map((extElement, index) =>
                    <>
                        {index % 2 === 0 &&
                            <div>  <label className="col-sm-9 col-form-label">{extElement}</label> {selectedTxnDetails.addOnRuleResultMap[extElement]}</div>
                        }
                    </>
                )}
            </div>
        </>
    }
    selectTab = (Tab) => {
        this.setState({ selectedTab: Tab });
        if (Tab !== RA_STR.threeDSecure) {
            this.showReport('', 'default')
        }
    }
    render() {
        let { savePreviousDateObj, toDate, endDate } = this.props.accessParent;
        let tranXtime = this.props.accessParent.state.lastTxnTime;
        let { SelectOrganization } = this.props.accessParent.state;
        let { columns, transactionList, detailLayouts, selectedTxnDetails, showRelatedExtElements, caseDataType, lastCaseOptions, lastTxnTime, selectedTab } = this.state;
        return <div>
            <h2 className='title mb-2'>{RA_STR.TransactionPage}</h2>
            <ul className="nav nav-pills form-inline mb-4">
                <li className={"nav-item " + (selectedTab !== RA_STR.threeDSecure && 'selectedTab')} onClick={() => this.selectTab('')}> {RA_STR.default} </li>
                <li className={"nav-item " + (selectedTab === RA_STR.threeDSecure && 'selectedTab')} onClick={() => this.selectTab(RA_STR.threeDSecure)}>
                    {RA_STR.threeDSecure} </li>
            </ul>
            {selectedTab === RA_STR.threeDSecure &&
                <>
                    <div className='row'>
                        <div className='col-sm-3'>{RA_STR.Organization}</div>
                        <div className='col-sm-4'>{SelectOrganization}</div>
                    </div>
                    <div className='row mt-4 mb-4'>
                        <div className='col-sm-3'>{toDate ? RA_STR.dataRange : RA_STR.lastTrx}</div>
                        {toDate ? <div className='col-sm-4'>{RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL} {toDate} 00:00 {RA_STR.EXCEPTION_USER_REPORT.TO_LABEL}  {endDate} 00:00</div> :
                            <div className='col-sm-4'> {tranXtime + ' ' + RA_STR.mins}</div>}
                    </div>
                    <div className='row mt-4 mb-4'>
                        <div className='col-sm-3'>{RA_STR.transactionsMade}</div>
                        <div className='col-sm-4'>{transactionList.length}</div>
                    </div>
                    <p>{RA_STR.clickCorresponding}</p>
                    <div className="transactionButtons">
                        <div className="form-group row">
                            <input className="grey-button ml-3" id="searchButton" type="submit" value="CANCEL" onClick={() => this.setPage(1)}></input>
                            <input className="secondary-button" id="searchButton" type="submit" value="EXPORT" onClick={this.props.accessParent.exportReports}></input>
                            <input className="secondary-button" id="searchButton" type="submit" value="EXPORT CUSTOM" onClick={this.props.accessParent.exportReports}></input>
                            <input className="secondary-button" id="searchButton" type="submit" value="Mark For Investigation"  ></input>
                        </div>
                    </div>
                    <DynamicTable columns={columns} data={transactionList} enablePagination={true} pageSize={10} totalCount={3} selection={false} activePage={this.getActivePage} handleCheckboxSelection={this.getcheckboxselection} activePageNumNew={1} />

                    {detailLayouts && detailLayouts.length > 0 &&
                        <div className="detailsSummary ml-3 mt-4">
                            {detailLayouts && detailLayouts.map(layoutData =>
                                <>
                                    {layoutData.box != RA_STR.RISKRULESRESULT && layoutData.box != RA_STR.EXTELEMENTS && layoutData.rowOrder != '6' && layoutData.rowOrder != '7' &&
                                        <div className="row">
                                            <p className="title">{layoutData.box}</p>
                                            {this.setBoxRows(layoutData)}
                                        </div>
                                    }
                                    {layoutData.box == RA_STR.RISKRULESRESULT &&
                                        <div className="row">
                                            <p className="title">{layoutData.box}</p>
                                            {this.setRiskruleSetRows(layoutData)}
                                        </div>
                                    }
                                    {layoutData.box == RA_STR.ADDITIONALELEMENTS &&
                                        <div className="row">
                                            <p className="title">{layoutData.box}</p>
                                            {this.setAdditionalrows(layoutData)}
                                        </div>
                                    }
                                </>
                            )}
                            <div className="row">
                                <p className="title">{RA_STR.histbasedEle}</p>
                                {selectedTxnDetails && selectedTxnDetails.historyBasedRulesJson &&
                                    <>
                                        <div className="col-sm-5 details-card">
                                            {Object.keys(selectedTxnDetails.historyBasedRulesJson).map((extElement, index) =>
                                                <>
                                                    {(index % 2 === 0) &&
                                                        <div>  <label className="col-sm-7 col-form-label">{extElement}</label> {selectedTxnDetails.historyBasedRulesJson[extElement]} </div>
                                                    }
                                                </>
                                            )}
                                        </div>
                                        <div className="col-sm-5 ml-2 details-card">
                                            {selectedTxnDetails.historyBasedRulesJson && Object.keys(selectedTxnDetails.historyBasedRulesJson).map((extElement, index) =>
                                                <>
                                                    {index % 2 === 1 &&
                                                        <div>  <label className="col-sm-7 col-form-label">{extElement}</label> {selectedTxnDetails.historyBasedRulesJson[extElement]}</div>
                                                    }
                                                </>
                                            )}
                                        </div>
                                    </>
                                }
                            </div>
                            {toDate &&
                                <>
                                    <div className="row details-block">
                                        <p>{RA_STR.showRelated}</p>
                                        <div className="custom-control custom-checkbox">
                                            {showRelatedExtElements && showRelatedExtElements.map(opt => {
                                                return (
                                                    <label key={opt.displayNameStr} className='form-label capitalize col-sm-3' >
                                                        <input
                                                            type='checkbox'
                                                            className="form-checkbox custom-control-input"
                                                            id={opt.displayNameStr}
                                                            name={opt.elementName}
                                                            onChange={this.handleCheckbox}
                                                            value={opt}
                                                        />
                                                        <label className="form-label custom-control-label" htmlFor={opt.displayNameStr}>{'Same ' + opt.displayNameStr}</label>
                                                    </label>
                                                );
                                            })}
                                        </div>
                                        <div className="row mt-6">
                                            <div className="col-sm-3">
                                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                                    <input type="radio" className="custom-control-input"
                                                        id="caseDataTypeparent"
                                                        name="caseDataType"
                                                        value='0'
                                                        checked={caseDataType == '0'}
                                                        onChange={this.handleChange} />
                                                    <label className="custom-control-label" htmlFor="caseDataTypeparent">{RA_STR.transDateRange}</label>
                                                </div>
                                            </div>
                                            <DatePickerWithTimer ref='DatepickerTimerID' disableAll={caseDataType == '1'} prevstateObj={savePreviousDateObj} ></DatePickerWithTimer>
                                        </div>
                                        <div className="row mt-4">
                                            <div className="col-sm-6 pr-0">
                                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                                    <input type="radio" className="custom-control-input "
                                                        id="caseDataTypeChild"
                                                        name="caseDataType"
                                                        value='1'
                                                        checked={caseDataType == '1'}
                                                        onChange={this.handleChange}
                                                    />
                                                    <label className="custom-control-label" htmlFor="caseDataTypeChild">{RA_STR.lastTransactions}</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-5 disableLight">
                                                <Select
                                                    name='lastTxnTime'
                                                    title=''
                                                    options={lastCaseOptions}
                                                    selectedOption={lastTxnTime}
                                                    controlFunc={this.handleChange}
                                                    disabled={caseDataType == '0'}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group row mt-4">
                                        <input className="secondary-button" id="searchButton" type="submit" value="SHOW" onClick={this.showReport} ></input>
                                    </div>
                                </>
                            }
                        </div>

                    }
                </>
            }
        </div >
    }
};
export default AnalyzeReports;