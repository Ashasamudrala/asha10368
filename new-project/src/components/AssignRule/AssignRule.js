import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_RADIO, RA_STR_SELECT, RA_STR_SELFORG } from '../../shared/utlities/constants';
import { orgData } from '../../shared/utlities/renderer';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import SuccessList from '../../shared/components/SuccessList/SuccessList';

class AssignRule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orgDetails: '',
            assignRuleForm: {},
            ruleSetData: {},
            configList: [],
            rulesetExecutionMode: '',
            errorMsg: '',
            successMsg: '',
            getRuleAction: 0,
            presentInOrgRelation: '',
            activeRuleSetForOrganization: '',
            self: '',
            parent: '',
            userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null,
        }
    }

    showErrorList = () => {
        window.scrollTo(0, 0);
    }

    componentDidMount = async () => {
        this.state.orgDetails = orgData('');
        let jwtToken = this.state.userData ? this.state.userData.token : null;
        const getRuleSetNames = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getOrgUrl']}/${this.state.orgDetails.orgName}${RA_API_URL['ruleSet']}/${this.state.getRuleAction}`
        };
        const getRuleSetStatus = await getService(getRuleSetNames);
        if (getRuleSetStatus && getRuleSetStatus.status === 200) {
            this.setState({
                configList: getRuleSetStatus.data.configList,
                ruleSetData: getRuleSetStatus.data,
                rulesetExecutionMode: getRuleSetStatus.data.rulesetExecutionMode
            });
        }
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    handleChange = (e) => {
        const assignRuleForm = this.state.assignRuleForm;
        const { name, value, id } = e.target;
        var feildValue;
        (e.target.type === RA_STR_RADIO) ?
            feildValue = (value === true) ? 1 : 0
            :
            feildValue = value;
        assignRuleForm[name] = feildValue;
        if (e.target.type === RA_STR_RADIO) {
            if (e.target.id === RA_STR_SELFORG) {
                this.setState({
                    rulesetExecutionMode: 0
                })
            }
            else {
                this.setState({
                    rulesetExecutionMode: 1
                })
            }
        }
        this.setState({
            assignRuleForm
        });
    }

    handleSubmit = async () => {
        let jwtToken = this.state.userData ? this.state.userData.token : null;
        let params;
        if (this.state.ruleSetData.presentInOrgRelation === true) {
            params = {
                ...this.state.assignRuleForm,
                presentInOrgRelation: this.state.ruleSetData.presentInOrgRelation,
                rulesetExecutionMode: this.state.rulesetExecutionMode,
                configName: this.state.ruleSetData.activeRuleSetForOrganization
            }
        }
        else {

            params = {
                ...this.state.assignRuleForm,
                presentInOrgRelation: this.state.ruleSetData.presentInOrgRelation,
                rulesetExecutionMode: this.state.rulesetExecutionMode
            }
        }

        const assignRuleSet = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['getOrgUrl']}/${this.state.orgDetails.orgName}${RA_API_URL['ruleSet']}${RA_API_URL['assignment']}`,
            data: params
        };
        const assignRuleSetStatus = await getService(assignRuleSet);
        if (assignRuleSetStatus && assignRuleSetStatus.status === 400) {
            this.props.activateErrorList(true, assignRuleSetStatus.data.errorList);
            this.props.activateSuccessList(false, '');
            this.showErrorList();
        }
        else {
            this.props.activateSuccessList(true, assignRuleSetStatus.data);
            this.props.activateErrorList(false, '');
            this.showErrorList();

        }
    }

    render() {
        return (
            <div className="main">
                <h2 className="title">Assign Ruleset</h2>
                <p className="desc">Select the ruleset that must be used during Risk Evaluation. The rulesets that have been migrated to production are available in the list. Alternatively, you can select the parent organization's ruleset. Refresh the cache for the changes to take effect.</p>

                {this.state.ruleSetData.presentInOrgRelation ?
                    <div className="row">
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-3">Ruleset Execution Mode</label>
                                <div className="col-sm-4 col-form-label flex justify-between">
                                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                        <input type="radio" className="custom-control-input "
                                            id="self"
                                            name="rulesetExecutionMode"
                                            defaultChecked={this.state.ruleSetData.rulesetExecutionMode === 0 ? true : false}
                                            onChange={this.handleChange} />
                                        <label className="custom-control-label" htmlFor="self">Self</label>
                                    </div>
                                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                        <input type="radio" className="custom-control-input "
                                            id="parent"
                                            name="rulesetExecutionMode"
                                            defaultChecked={this.state.ruleSetData.rulesetExecutionMode === 1 ? true : false}
                                            onChange={this.handleChange} />
                                        <label className="custom-control-label" htmlFor="parent">Parent</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    : ''
                }
                {(this.state.rulesetExecutionMode === 1) ?
                    <div className="row">
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-3">Parent Child Chain</label>
                                <div className="col-sm-4 col-form-label">
                                    <textarea rows="3" cols="45" onChange={this.handleChange}
                                        value={this.state.ruleSetData.childParentChain} />
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-3">Ruleset Assigned By Parent</label>
                                <div className="col-sm-4 col-form-label">{this.state.ruleSetData.rulesetAssignedByParent}</div>
                            </div>
                        </div>
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-3">Active Ruleset for Organization</label>
                                <div className="col-sm-4 col-form-label">{this.state.ruleSetData.activeRuleSetForOrganization}</div>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="row">
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-3">Active Ruleset for Organization</label>
                                <div className="col-sm-4 col-form-label">{this.state.ruleSetData.activeRuleSetForOrganization}</div>
                            </div>
                        </div>
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-3">Select Ruleset to assign</label>
                                <div className="col-sm-4 col-form-label">
                                    <select className="form-control"
                                        name="configName"
                                        onChange={this.handleChange}>
                                        <option value="">{RA_STR_SELECT}</option>
                                        {this.state.configList ? this.state.configList.map((opt, i) => {
                                            return (
                                                <option key={i}
                                                    value={opt}>{opt}</option>
                                            );
                                        }) : ''}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                <div className="form-group form-submit-button row">
                    <input className="secondary-btn ml-2" id="manageTokenButton"
                        type="submit" value="SAVE" onClick={this.handleSubmit} />
                </div>

            </div >
        );
    }

}

export default AssignRule;