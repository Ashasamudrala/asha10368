import React, { Component } from 'react';
import './AdminLogout.css';
import { RA_SESSION_EXPIRED, bamloginUrl, masteradminUrl } from '../../shared/utlities/constants';

class AdminLogout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectingUrl: ''
    }
  }

  componentDidMount = () => {
    const getProfiledataUsername = JSON.parse(localStorage.getItem('profiledata'));
    window.localStorage.removeItem('profiledata');
    let setRedirectionUrl = (process.env.REACT_APP_ROUTER_BASE || '');
    if ((getProfiledataUsername && getProfiledataUsername.userProfile.userName === "MASTERADMIN") || !getProfiledataUsername) {
      setRedirectionUrl += masteradminUrl;
    } else {
      setRedirectionUrl += bamloginUrl;
    }
    this.setState({ redirectingUrl: setRedirectionUrl });
  }
  render() {
    const { redirectingUrl } = this.state;
    return <div className="admin-logout"><div className="clearfloat edl-login-box">
      <div id="brand-holder">
        <img src="images/ca-logo-new.png" alt="caLogo" height="50"></img>
        <p className="ecc-h1">Administration Console</p>
      </div>
      <div className="form-header">
        {RA_SESSION_EXPIRED}<a href={redirectingUrl} id="login-url">log in</a> again.
	</div>
    </div></div>
  }
}

export default AdminLogout;
