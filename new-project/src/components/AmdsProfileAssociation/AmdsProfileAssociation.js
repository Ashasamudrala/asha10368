import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import './AmdsProfileAssociation.scss';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import SwappableGrid from '../../shared/components/SwapGrid/swappingComponent';
import { async } from 'q';
class AmdsProfileAssociation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        providername:'',
        customid:'',
        fromemail:'',
        fromnumber:'',
      },
      defaultLeftItems: {},
      defaultRightItems: {},
      scopeAll: false,
      selectedService: '--Select--',
      profileName: '',
      serviceList: [{ key: '--Select--', content: '--Select--' }],
    }
  }

  componentDidMount() {
    this.getAllOrganizations();
    this.getProfileInfo();
  }
  serviceChange = (e) => {
    let selectedType = e.target.value;
    this.setState({
      selectedService: selectedType
    });
  }
  handleChange = (e) => {
    const { fields } = this.state;
    fields[e.target.name] = e.target.value;
    this.setState({ fields: fields });
  }
  saveProfileDetails = async (e) => {
    const selectedRightItems = this.refs.swappableGridRef.getRightItems();
    let data = {
      associatedOrganizations: [...selectedRightItems],
      customId: this.state.fields.customid != '--Select--' ? this.state.fields.customid : '',
      fromEmail: this.state.selectedService == 'EMAIL' ? this.state.fields.fromemail : '',
      fromNumber: this.state.selectedService == 'SMS' || this.state.selectedService == 'VOICE' ? this.state.fields.fromnumber : '',
      profileId: this.state.profileName,
      providerName: this.state.fields.providername != '' ? this.state.fields.providername : '',
      serviceType: this.state.selectedService
    }
    const setProfileData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL.saveAmdsprflData}`,
      data: data
    }
    const updateStatus = await getService(setProfileData);
    if (updateStatus.status === 200) {
      this.props.activateSuccessList(true, updateStatus.data);
      this.props.activateErrorList(false, '');
      this.restDetails();
    } else {
      this.props.activateErrorList(true, updateStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  restDetails = () => {
    // window.location.reload();
   this.setState({
    fields: {
      providername:'',
      customid:'',
      fromemail:'',
      fromnumber:'',
    },
    selectedService:'--Select--',
   })
   this.refs.swappableGridRef.setRightItems({rightItems:[]});
  }
  getProfileInfo = async () => {
    const profileUrl = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getAmdProfileInfo']}`
    }
    const profileInfo = await getService(profileUrl);
    if (profileInfo && profileInfo.status === 200) {
      this.setState({
        serviceList: [...this.state.serviceList, ...CA_Utils.covertObjectToArray(profileInfo.data.serviceTypes)],
        profileName: profileInfo.data.profileId,
      });

    }
  }
  getAllOrganizations = async () => {
    const OrgUrl = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const availableOrgs = await getService(OrgUrl);
    if (availableOrgs && availableOrgs.status === 200) {
      this.setState({
        defaultLeftItems: CA_Utils.objToArray(availableOrgs.data.organizations, 'string')
      })

    }
  }
  _handleCancel=()=>{
    this.restDetails();
  }
  render() {
    const { selectedService, serviceList, profileName, defaultLeftItems, defaultRightItems, fields, scopeAll } = this.state;
    return <div className="main amds-profile">
      <div>
  <h2 className="title">{RA_STR.amdsProfile}</h2>
      </div>
      <div>
  <p className="desc">{RA_STR.profiledesc}</p>
      </div>
      <div className="row">
        <div className="col-sm-2 mb-2">
  <label className="col-form-label">{RA_STR.Profilelabel}</label>
        </div>
        <div className="col-sm-3 profile-label">
          {this.state.profileName}
        </div>
      </div>
      <div className="row">
        <div className='col-sm-11'>
          <div className=' col-sm-5'>
            <Select
              name={'servicetype'}
              title={'Service Type'}
              required={true}
              selectedOption={selectedService}
              // placeholder={'By Month'}
              options={serviceList ? serviceList : ['']}
              controlFunc={this.serviceChange} />
          </div>
        </div>
      </div>
      <div className="row">
        <div className='col-sm-11'>
          <div className="col-sm-5">
            <SingleInput
              title={'Provider Name'}
              inputType={'text'}
              required={true}
              name={'providername'}
              content={fields.providername}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Custom ID'}
              inputType={'text'}
              name={'customid'}
              content={fields.customid}
              // required={true}
              controlFunc={this.handleChange} />
            {this.state.selectedService === 'SMS' && (
              <SingleInput
                title={'From Number'}
                inputType={'text'}
                name={'fromnumber'}
                content={fields.fromnumber}
                required={true}
                controlFunc={this.handleChange} />
            )}
            {this.state.selectedService === "VOICE" && (
              <SingleInput
                title={'From Number'}
                inputType={'text'}
                name={'fromnumber'}
                content={fields.fromnumber}
                required={true}
                controlFunc={this.handleChange} />
            )}
            {this.state.selectedService === 'EMAIL' && (
              <SingleInput
                title={'From Email'}
                inputType={'text'}
                name={'fromemail'}
                content={fields.fromemail}
                required={true}
                controlFunc={this.handleChange} />
            )}
          </div>
          <div className="row">
            <SwappableGrid ref="swappableGridRef" leftItems={defaultLeftItems} rightItems={defaultRightItems} scopeAll={this.state.scopeAll} leftHeader={'Available Organizations'} rightHeader={'Selected Organizations'} hideLeftIcons={false} checkMaxLimit={true}></SwappableGrid>
          </div>
        </div>
      </div>
      <div className="row">
        <div className=" col-sm-1 form-group form-submit-button  mt-5 ">
          <input className="secondary-btn" id="searchButton" type="submit" value="SAVE" onClick={this.saveProfileDetails}></input>

        </div>
        <div className=" col-sm-1 form-group form-submit-button mt-5 ">
          <input className="custom-secondary-btn" type="button" value="CANCEL" onClick={this._handleCancel} />
        </div>
      </div>


    </div>;
  }
}

export default AmdsProfileAssociation;
