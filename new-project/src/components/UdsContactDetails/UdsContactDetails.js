import React, { Component } from 'react';
import EmailConfiguration from '../EmailConfiguration/EmailConfiguration';
import { serverUrl, RA_API_VIEW_BULK_REQUEST } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import './UdsContactDetails.css';
import {RA_STR} from '../../shared/utlities/messages';

class UdsContactDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
        contactData: [],
        telephoneData: [],
        orgname:''
    }
}
componentDidMount =async()=> {
this.state.orgname = JSON.parse(window.localStorage.getItem('profiledata') || '');
  const details ={
    method: 'GET',
    url: `${serverUrl}${RA_API_VIEW_BULK_REQUEST.getUdsContactDetails}`
  }
  const contactDetailsInfo =await getService(details);
  if (contactDetailsInfo.status === 200) {
    let filteredContacts = contactDetailsInfo.data.emailContactInfoList.filter((eachContact) => {
      return eachContact.contactType === "E-MAIL"
  })
  filteredContacts = filteredContacts.map((eachFilter) => {
      var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
      eachFilter.disableRow = true;
      eachFilter.id = id;
      eachFilter.priority = parseInt(eachFilter.priority);
      return eachFilter;
  })
  filteredContacts.sort((a, b) => a.priority - b.priority);
  this.setState({
      contactData: filteredContacts
  }, () => {
      let telephoneContacts = contactDetailsInfo.data.phoneContactInfoList.filter((eachContact) => {
          return eachContact.contactType === "TELEPHONE"
      })
      telephoneContacts = telephoneContacts.map((eachFilter) => {
        var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
        eachFilter.disableRow = true;
          eachFilter.id = id;
          eachFilter.priority = parseInt(eachFilter.priority);
          return eachFilter;
      })
      this.setState({ telephoneData: telephoneContacts })
  })
}
  
} 
componentWillUnmount() {
  this.props.activateErrorList(false, '');
  this.props.activateSuccessList(false, '');
}
handleRowDel = (eachContact) => {
  if (eachContact.contactType === "E-MAIL") {
      if (this.state.contactData.length === 1) {
          return
      }
      var index = this.state.contactData.indexOf(eachContact);
      this.state.contactData.splice(index, 1);
      this.setState(this.state.contactData);
  } else {
      if (this.state.telephoneData.length === 1) {
          return
      }
      var index = this.state.telephoneData.indexOf(eachContact);
      this.state.telephoneData.splice(index, 1);
      this.setState(this.state.telephoneData);
  }
}
saveConfigureData = async () => {
  const storecontactData = JSON.parse(JSON.stringify(this.state.contactData));
  const storetelephoneData = JSON.parse(JSON.stringify(this.state.telephoneData));
  const myprop = storecontactData.map(function (props) {
      delete props.id;
      delete props.disableRow;
      props.priority = props.priority.toString();
      return props;
  });
  const mypropTelephone = storetelephoneData.map(function (props) {
      delete props.id;
      delete props.disableRow;
      props.priority = props.priority.toString();
      return props;
  });
 
  let data = {
    orgName:this.state.orgname.userProfile.organization.toString(),
    contactType: [...myprop, ...mypropTelephone]
  }
  const saveContact = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_VIEW_BULK_REQUEST.updateUdsContactDetails}`,
      data: data
  }
  const saveContactStatus = await getService(saveContact);
  if (saveContactStatus.status === 200) {
    var disableContacts = this.state.contactData.map((element) => {
      element.disableRow = true;
      element.newContact = false;
      return element;
      });
     var disableTelContacts = this.state.telephoneData.map((element) => {
      element.disableRow = true;
      element.newContact = false;
      return element;
      })
        this.setState({contactData:disableContacts,telephoneData:disableTelContacts});
    this.props.activateSuccessList(true,saveContactStatus.data);
    this.props.activateErrorList(false,'');
     
  }else{
    this.props.activateErrorList(true,saveContactStatus.data.errorList);
    this.props.activateSuccessList(false,'');
  }
}
handleAddEvent = (getdata) => {
  var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
  if (getdata === 'email') {
      var emailData = {
          id: id,
          contactType: "E-MAIL",
          contactName: "",
          displayName: "",
          priority: this.state.contactData.length + 1,
          mandatory: "0",
          newContact: true
      }
      this.setState({contactData: [...this.state.contactData, emailData]});
  } else {
      var telephoneDataGroup = {
          id: id,
          contactType: "TELEPHONE",
          contactName: "",
          displayName: "",
          priority: this.state.telephoneData.length + 1,
          mandatory: "0",
          newContact:true
      }
      this.setState({telephoneData: [...this.state.telephoneData, telephoneDataGroup]});
  }
}
handleContactDetails(evt) {
  var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
  };
  var contactData = '';
  if (evt.target.dataset.contacttype === "E-MAIL") {
      contactData = this.state.contactData.slice();
  } else if (evt.target.dataset.contacttype === "TELEPHONE") {
      contactData = this.state.telephoneData.slice();
  }
  var newContactData = contactData.map( emailTelephone => {
    if (emailTelephone.id === item.id) {
      emailTelephone[item.name] = item.value;          
    }
    return emailTelephone;
  });
 
  if (evt.target.dataset.contacttype === "E-MAIL") {
      this.setState({ contactData: newContactData });
  } else if (evt.target.dataset.contacttype === "TELEPHONE") {
      this.setState({ telephoneData: newContactData });
  }
}
getPriorityData = (getClickStatus, getProduct) => {
  let getStatusChanges = '';
  let totalLength = '';
  if ((getProduct.contactType === "E-MAIL" && getProduct.priority <= this.state.contactData.length) || (getProduct.contactType === "TELEPHONE" && getProduct.priority <= this.state.telephoneData.length)) {
      if (getProduct.contactType === "E-MAIL") {
          totalLength = this.state.contactData.length;
      } else if (getProduct.contactType === "TELEPHONE") {
          totalLength = this.state.telephoneData.length;
      }
  }
  if (getClickStatus === 'inc' && getProduct.priority !== 1) {
      getStatusChanges = getProduct.priority - 1;
  } else if (getClickStatus === 'dec' && getProduct.priority !== totalLength) {
      getStatusChanges = getProduct.priority + 1;
  }

  if (getProduct.contactType === "E-MAIL") {
      let emailContacts = getStatusChanges && this.state.contactData.map((eachContact, key) => {
          if (eachContact.id === getProduct.id) {
              eachContact.priority = getStatusChanges;
              if (getClickStatus === 'inc') {
                  this.state.contactData[key - 1].priority = getProduct.priority + 1;
              } else if (getClickStatus === 'dec') {
                  this.state.contactData[key + 1].priority = getProduct.priority - 1;
              }
          }
          return eachContact
      })
      if (emailContacts.length) {
          emailContacts && emailContacts.sort((a, b) => a.priority - b.priority);
          this.setState({ contactData: emailContacts });
      }
  } else if (getProduct.contactType === "TELEPHONE") {
      let telephoneContacts = getStatusChanges && this.state.telephoneData.map((eachContact, key) => {
          if (eachContact.id === getProduct.id) {
              eachContact.priority = getStatusChanges;
              if (getClickStatus === 'inc') {
                  this.state.telephoneData[key - 1].priority = getProduct.priority + 1;
              } else if (getClickStatus === 'dec') {
                  this.state.telephoneData[key + 1].priority = getProduct.priority - 1;
              }
          }
          return eachContact
      })
      if (telephoneContacts.length) {
          telephoneContacts && telephoneContacts.sort((a, b) => a.priority - b.priority);
          this.setState({ telephoneData: telephoneContacts });
      }
  }

}
render() {
  return (<div className="main">
          <div>
          <h2>{RA_STR.configureEmail_Tel_Tittle}</h2>
          <span>{RA_STR.configureEmail_Tel_Desc_label}</span>
          <p className="desc">{RA_STR.configureEmail_Tel_Desc}</p>
          <span>{RA_STR.configureEmail_label}</span>
          <EmailConfiguration onProductTableUpdate={this.handleContactDetails.bind(this)} onRowAdd={this.handleAddEvent} onRowDel={this.handleRowDel} products={this.state.contactData} tableType={'email'} getPriorityStatus={this.getPriorityData} />
          <span>{RA_STR.configure_Tel_label}</span>
          <EmailConfiguration onProductTableUpdate={this.handleContactDetails.bind(this)} onRowAdd={this.handleAddEvent} onRowDel={this.handleRowDel} products={this.state.telephoneData} tableType={'telephone'} getPriorityStatus={this.getPriorityData} />
         
         
         <div className="form-group form-submit-button row mt-2 ml-2">
              <input className="secondary-btn ml-3"  type="submit" value="SAVE" onClick={this.saveConfigureData}></input>
        </div> 
        
      </div>

  </div>)
}}

export default UdsContactDetails;
