import React, { Component } from 'react';
import { serverUrl, RA_STR_SELECT, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import DateTimeRange from '../../shared/components/DateTimeRange/DateTimeRange';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';
class FraudReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgName: '',
      orgOptions: [],
      enableOrg: true,
      showTable: false,
      showDates:false,
      columns: [
        { title: RA_STR.perspective, field: 'perspective' },
        { title: RA_STR.fraudChannel, field: 'channelname' },
        { title: RA_STR.RuleMnemonic, field: 'rulemnemonic' },
        { title: RA_STR.triggerCount, field: 'triggerCount' },
        { title: RA_STR.matchedCount, field: 'matchedCount' },
        { title: RA_STR.assumeFraud, field: 'assumedFraudCount' },
        { title: RA_STR.assumeGenuine, field: 'assumedGenuineCount' },
        { title: RA_STR.confirmFraud, field: 'confirmedFraudCount' },
        { title: RA_STR.confirmGenuine, field: 'confirmedGenuineCount' },
        { title: RA_STR.undeterminedCount, field: 'undeterminedCount' }
      ],
      displayList: []
    }
  }
  componentDidMount = async () => {
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      this.setState({ orgOptions: orgOptions });
    }
  }
  getSelectedOrgDetails = (getSelectedOrgDetails) => {
    this.setState({
      orgName: getSelectedOrgDetails,
      showDates:true
    })
  }
  displayReport = async () => {
    var getDateItems = this.refs.datetimerangelist.getDatesTimes();
    if (getDateItems[0].startDate <= getDateItems[0].endDate) {
      const displayReport = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['fraudReport']}`,
        data: {
          "fromDate": `${getDateItems[0].startDate} ${getDateItems[0].startHours}:${getDateItems[0].startMins}:00`,
          "orgName": this.state.orgName ? this.state.orgName : '',
          "toDate": `${getDateItems[0].endDate} ${getDateItems[0].endHours}:${getDateItems[0].endMins}:00`
        }
      }
      const displayReportStatus = await getService(displayReport);
      if (displayReportStatus.status === 200) {
        this.setState({ displayList: displayReportStatus.data, showTable: true });
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, displayReportStatus.data.errorList);
      }
    } else {
      alert(RA_STR.ruleAlert);
    }
  }
  downloadCSV = (response) => {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Rule Effectiveness(Fraud) Report.csv');
    }
  }
  exportData = async () => {
    const { displayList } = this.state;
    const exportReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['fraudReport']}/export`,
      data: {
        "fromDate": displayList.fromDate,
        "orgName": displayList.orgName,
        "toDate": displayList.toDate
      }
    }
    const exportReportStatus = await getService(exportReport);
    if (exportReportStatus.status === 200) {
      this.downloadCSV(exportReportStatus);
    }
  }
  reset = () => {
    window.location.reload();
  }
  render() {
    const { orgOptions, enableOrg, showTable, displayList, columns ,showDates } = this.state;
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.fraudTitle}</h2>
        {!showTable ?
          <div>
            <p className='desc'>{RA_STR.fraudText}</p>
            <hr />
            <div className="col-md-6">
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}</label>
                <div className='col-sm-8'>
                  <AutoSuggest orgOptions={orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={enableOrg} defaultOrganizationPlaceholder={RA_STR_SELECT} />
                </div>
              </div>
            </div>
            {showDates ? <div><div className="col-md-12 row">
              <label className="col-sm-2 col-form-label">By Date Range</label>
              <DateTimeRange ref="datetimerangelist" startPrior={'15'} endPrior={'14'}></DateTimeRange>
            </div>
              <input className="secondary-btn" id="RESET" type="submit" value="Display Report" onClick={this.displayReport}></input></div> : ''}
          </div> : <div>
            <div>
              <div className="col-md-12 row">
                <div className="col-md-9 no-padding">
                  <p>{RA_STR.fraudReportDesc}</p>
                </div>
                <div className="float-right col-md-3">
                  <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                  <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.reset}></input>
                </div>
              </div>
              <div className="col-md-8">
                <div className="form-group form-inline">
                  <label className="col-sm-4 col-form-label">{RA_STR.noOfRecords}</label>
                  <div className="col-sm-8">
                    {displayList.numberOfRecords}
                  </div>
                </div>
                <div className="form-group form-inline">
                  <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}:</label>
                  <div className="col-sm-8">
                    {displayList.orgName}
                  </div>
                </div>
                <div className="form-group form-inline">
                  <label className="col-sm-4 col-form-label">From:{displayList.fromDate}</label>
                  <div className="col-sm-8">
                    To:{displayList.toDate}
                  </div>
                </div>
              </div>
            </div>
          </div>}
        {showTable ?
          <div className="tabele-buttons">
            <DynamicTable columns={columns} data={displayList.reportList} /></div> : ''}
      </div>
    )
  }
}
export default FraudReport;
