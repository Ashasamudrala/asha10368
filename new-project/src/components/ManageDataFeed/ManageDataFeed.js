import React, { Component } from 'react';
import {
  serverUrl, RA_API_URL, RA_API_STATUS, RA_STR_SELECT
} from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import { getService } from '../../shared/utlities/RestAPI';
import Select from '../../shared/components/Select/Select';
import '../ReportsSummary/ReportsSummary.css';
import './ManageDataFeed.css'
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import _ from 'underscore';

class ManageDataFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      getDataFeedList: [],
      getConfigList: [],
      endPointList: [],
      specificConfig: JSON.parse(localStorage.getItem('selectedSpecificConfig')) || '',
      configName: '',
      dfendpoint: '',
      enableConfig: JSON.parse(localStorage.getItem('enableConfig')) || false,
      actionData: '',
      manageDataForm: {},
      configData: '',
      disableBtn: false,
      selectedEndPoint: JSON.parse(localStorage.getItem('selectedEndPoint')) || '',
      selectedConfig: JSON.parse(localStorage.getItem('selectedConfig')) || '',
      selectedConnecConfig: JSON.parse(localStorage.getItem('selectedConnecConfig')) || '',
      newConfig: ''
    }
  }

  showErrorList = () => {
    window.scrollTo(0, 0);
  }

  getEndPoint = async () => {
    const getDataFeed = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getDatafeedEndpoints']}`,
    };
    const getDataFeedStatus = await getService(getDataFeed);
    if (getDataFeedStatus && getDataFeedStatus.status === RA_API_STATUS['200']) {
      let objectKeys = Object.keys(getDataFeedStatus.data.endpoints);
      let objectValues = Object.values(getDataFeedStatus.data.endpoints);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.getDataFeedList.push({
          'key': objectValues[i],
          'content': objectValues[i]
        })
        this.setState({
          getDataFeedList: this.state.getDataFeedList
        });
      }
    }
  }

  componentDidMount() {
    this.getEndPoint();
    if (this.state.selectedEndPoint) {
      this.getConfig(this.state.selectedEndPoint);
      if (this.state.selectedConfig) {
        this.getConfig(this.state.selectedConfig);
      }
    }
  }

  addEndPointConfig = async () => {
    this.setState({
      specificConfig: {},
      endPointList: []
    })
    const addDataFeed = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getEndpointConfigurationOnAdd']}/${this.state.dfendpoint || this.state.selectedEndPoint}`,
    };
    const addDataFeedStatus = await getService(addDataFeed);
    if (addDataFeedStatus && addDataFeedStatus.status === RA_API_STATUS['200']) {
      let endpointValues = Object.values(addDataFeedStatus.data.endPointList);
      const spec = {
        specificConfigMap: addDataFeedStatus.data.configs
      }
      this.setState({
        specificConfig: spec,
        enableConfig: true
      })
      localStorage.setItem('enableConfig', JSON.stringify(this.state.enableConfig));
      for (let i = 0; i < endpointValues.length; i++) {
        this.state.endPointList.push({
          'key': endpointValues[i],
          'content': endpointValues[i]
        })
        this.setState({
          endPointList: this.state.endPointList
        });
      }
    }
  }

  getConfig = async (val) => {
    const getConfig = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getEndpointConfiguration']}/${val}`,
    };
    const getConfigStatus = await getService(getConfig);
    if (getConfigStatus && getConfigStatus.status === RA_API_STATUS['200']) {
      this.setState({
        endPointList: []
      })
      let objectKeys = Object.keys(getConfigStatus.data.configs);
      let objectValues = getConfigStatus.data.configs[objectKeys];
      let endpointValues = Object.values(getConfigStatus.data.endPointList);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.getConfigList.push({
          'key': objectKeys[i],
          'content': objectKeys[i]
        })
        this.setState({
          configData: getConfigStatus.data.configs,
          getConfigList: this.state.getConfigList,
          actionData: objectValues
        });
      }
      for (let i = 0; i < endpointValues.length; i++) {
        this.state.endPointList.push({
          'key': endpointValues[i],
          'content': endpointValues[i]
        })
        this.setState({
          endPointList: this.state.endPointList
        });
      }
    }
  }
  handleChange = async (val, e) => {
    const manageDataForm = this.state.manageDataForm;
    const { name, value } = e.target;
    var fieldValue;
    fieldValue = value;
    manageDataForm[name] = fieldValue;
    this.setState({
      configName: val
    })
    if (name === '' && value === '') {
      this.setState({
        specificConfig: {},
        endPointList: [],
        configName: ''
      })
    }
    else if (name === 'dfendpoint' && e.target.value !== '') {
      this.setState({
        dfendpoint: e.target.value,
        getConfigList: [],
        specificConfig: {},
        endPointList: [],
        disableBtn: false
      })
      localStorage.setItem('selectedEndPoint', JSON.stringify(e.target.value));
      this.getConfig(e.target.value);
    }
    else if (val === 'configName' && e.target.type === 'text') {
      this.setState({
        newConfig: value
      })
    }
    else if (val === 'configName' && e.target.type === 'select-one' && value === RA_STR_SELECT) {
      this.setState({
        specificConfig: {},
        disableBtn: false
      })
    }
    else if (name === 'configName' && e.target.type === 'select-one') {
      this.setState({
        specificConfig: this.state.configData[e.target.value],
        disableBtn: true
      })
      localStorage.setItem('selectedConfig', JSON.stringify(e.target.value));
    }
    else if (val === 'configName' && value !== '') {
      this.setState({
        specificConfig: this.state.specificConfig,
        endPointList: this.state.endPointList,
      })
    }
    else if (val === 'connectionConfig' && (value !== '' || value === '')) {
      this.state.specificConfig['connectionConfig'] = value;
      localStorage.setItem('selectedConnecConfig', JSON.stringify(e.target.value));
    }
    this.setState({
      manageDataForm,
    })
  }


  configChange = (val, e) => {
    const specificConfig = this.state.specificConfig;
    const { name, value } = e.target;
    var fieldValue;
    fieldValue = value;
    specificConfig.specificConfigMap[val] = fieldValue;
    this.setState({
      specificConfig,
    })
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    this.setState({
      getDataFeedList: [],
      getConfigList: [],
      specificConfig: {},
      configName: '',
      selectedEndPoint: '',
      enableConfig: false,
    })
    localStorage.removeItem('selectedEndPoint');
    localStorage.removeItem('selectedConfig');
    localStorage.removeItem('selectedSpecificConfig');
    localStorage.removeItem('enableConfig');
    localStorage.removeItem('selectedConnecConfig');
  }

  handleSubmit = async () => {
    let specificParams = {
      ...this.state.specificConfig.specificConfigMap,
      ...this.state.manageDataForm
    }
    let fields = {
      connectionConfig: this.state.manageDataForm['connectionConfig'] ? this.state.manageDataForm['connectionConfig'] : this.state.specificConfig['connectionConfig'],
      // connectionConfig: specificParams['connectionConfig'] || this.state.specificConfig['connectionConfig'],
      configName: specificParams['configName'] || '',
      dfendpoint: specificParams['dfendpoint'],
    }
    var updatedList = JSON.stringify(specificParams, function (key, value) { return (value === null) ? "" : value });

    let params = {
      ...this.state.specificConfig,
      ...fields,
      specificConfigMap: JSON.parse(updatedList),
    }
    delete params.specificConfigMap['configName'];
    delete params.specificConfigMap['dfendpoint'];
    delete params.specificConfigMap['connectionConfig'];
    const saveDataFeed = {
      method: this.state.enableConfig ? 'POST' : 'PUT',
      url: `${serverUrl}${RA_API_URL['insertDataFeedConfig']}`,
      data: params
    };
    const saveDataFeedStatus = await getService(saveDataFeed);
    this.setState({
      getConfigList: []
    })
    if (saveDataFeedStatus && saveDataFeedStatus.status === RA_API_STATUS['200']) {
      this.props.activateSuccessList(true, saveDataFeedStatus.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
      this.setState({
        enableConfig: false,
        disableBtn: true,
        selectedConfig: this.state.newConfig
      })
      this.getConfig(this.state.dfendpoint);

    }
    else {
      this.props.activateErrorList(true, saveDataFeedStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  }
  handleReset = async () => {
    this.setState({
      getDataFeedList: [],
      getConfigList: [],
      specificConfig: {},
      configName: '',
      selectedEndPoint: '',
      enableConfig: false,
      manageDataForm: {}
    })
    localStorage.removeItem('selectedEndPoint');
    localStorage.removeItem('selectedConfig');
    localStorage.removeItem('selectedSpecificConfig');
    localStorage.removeItem('enableConfig');
    localStorage.removeItem('selectedConnecConfig');
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    this.getEndPoint();
  }

  renderer() {
    const { getConfigList, configName, disableBtn, selectedEndPoint, selectedConfig } = this.state;
    return (
      <div>
        {
          configName !== '' ?
            <div className="form-group dynamic-form-select">
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Config Name:</label>
                <div className="col-sm-6">

                  <select className="form-control"
                    name="configName"
                    defaultValue={selectedConfig}
                    onChange={this.handleChange.bind(this, 'configName')}>
                    <option value={RA_STR_SELECT}>{RA_STR_SELECT}</option>
                    {getConfigList.map(opt => {
                      return (
                        <option
                          key={opt.key}
                          value={opt.key}>{opt.content}</option>
                      );
                    })}
                  </select>

                </div>
                {disableBtn !== true ?
                  <input className="secondary-btn col-sm-2" type="submit" value="ADD" onClick={this.addEndPointConfig} />
                  : ''
                }
              </div>
            </div>
            : ''
        }
      </div>
    )
  }

  render() {
    const { getDataFeedList, specificConfig, selectedConnecConfig,
      endPointList, enableConfig, selectedEndPoint } = this.state;
    localStorage.setItem('selectedSpecificConfig', JSON.stringify(specificConfig));
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.manageDataTitle}</h2>
        <p className='desc'>{RA_STR.manageDataText}</p>
        <div className='col-sm-8'>
          {getDataFeedList.length ?
            <Select
              name={'dfendpoint'}
              title={'Data Feed Endpoint'}
              options={getDataFeedList ? getDataFeedList : ['']}
              defaultOption={selectedEndPoint}
              placeholder={RA_STR_SELECT}
              controlFunc={this.handleChange.bind(this, 'dfendpoint')} />
            : ''
          }
          {!enableConfig ? this.renderer() :
            <SingleInput
              title={'Config Name'}
              inputType={'text'}
              name={'configName'}
              controlFunc={this.handleChange.bind(this, 'configName')} />
          }
        </div>
        {specificConfig.specificConfigMap ?
          <div className='col-sm-12 no-padding'>
            <div className='end-config'>{RA_STR.manageEndpoint}</div>
            <div className='row no-margin'>
              {Object.keys(specificConfig.specificConfigMap).map((obj, i) => {
                return (
                  <div className="col-sm-6" key={i}>
                    <div className="form-group row">
                      <label className="col-sm-6 col-form-label ml-6">{obj}</label>
                      <div className="col-sm-6">
                        <input type="text" className="form-control"
                          name={obj}
                          value={specificConfig.specificConfigMap[obj]}
                          onChange={this.configChange.bind(this, obj)} />
                      </div>
                    </div>
                  </div>
                )
              })}
              <div className='col-sm-9'>
                {endPointList.length ?
                  <Select
                    name={'connectionConfig'}
                    title={'Connection Configuration'}
                    options={endPointList ? endPointList : ['']}
                    defaultOption={selectedConnecConfig || specificConfig.connectionConfig}
                    placeholder={RA_STR_SELECT}
                    controlFunc={this.handleChange.bind(this, 'connectionConfig')} />
                  : ''}
              </div>
            </div>
            <div className="form-group form-submit-button row mt-5 ml-2 text-left">
              <input className="custom-secondary-btn" type="button" value="CANCEL"
                onClick={this.handleReset} />
              <input className="secondary-btn ml-3" type="submit"
                value="SAVE" onClick={this.handleSubmit}></input>
            </div>
          </div>
          : ''}
      </div>
    )
  }
}

export default ManageDataFeed;
