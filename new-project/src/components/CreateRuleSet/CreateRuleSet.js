import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
class CreateRuleSet extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (<div className="main">
            <h2 className="title">Create Ruleset</h2>
            <p className="desc">Use this screen to create a new Ruleset. A Ruleset can be created by copying an existing System level Ruleset. If not copied, the Ruleset will be created with factory settings. The new Ruleset should be migrated to production before it can be "Assigned".</p>
            <div className="col-sm-6 ">
                <SingleInput
                    title={'Username'}
                    inputType={'text'}
                    name={'Name'} />
            </div>
            <div className="row">


            </div>
        </div>);
    }
}

export default CreateRuleSet;