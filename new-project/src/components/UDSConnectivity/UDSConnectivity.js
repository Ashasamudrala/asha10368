import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';


class UDSConnectivity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: [],
      loader: false,
      userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null,
      protocolArray: [{ 'key': 'TCP', 'content': 'TCP' }, { 'key': '1-WAY', 'content': 'One-Way SSL' }, { 'key': '2-WAY', 'content': 'Two-Way SSL' }],
      udsConnectivityDetails: ''
    }
  }

  componentDidMount = async () => {
    let jwtToken = this.state.userData ? this.state.userData.token : null;
    const getUdsConnectivityDetails = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getUdsConnectivityDetails']}`
    };
    const getUdsConnectivityDetailsStatus = await getService(getUdsConnectivityDetails);
    if (getUdsConnectivityDetailsStatus && getUdsConnectivityDetailsStatus.status === 200) {
      this.setState({ udsConnectivityDetails: getUdsConnectivityDetailsStatus.data.udsConfigConnectivityDetails, loader: true })
    }
  }

  handleChange = (e) => {
    const udsConnectivityDetails = this.state.udsConnectivityDetails;
    var feildValue;
    feildValue = e.target.value;
    udsConnectivityDetails[e.target.name] = feildValue;
    if (e.target.value === 'TCP') {
      udsConnectivityDetails['serverRootCert'] = '';
      udsConnectivityDetails['clientCert'] = '';
      udsConnectivityDetails['clientPrivateKey'] = '';
    } else if (e.target.value === '1-WAY') {
      udsConnectivityDetails['clientCert'] = '';
      udsConnectivityDetails['clientPrivateKey'] = '';
    }
    this.setState({
      udsConnectivityDetails
    });
  }
  handleFileChange = (e) => {
    const udsConnectivityDetails = this.state.udsConnectivityDetails;
    let reader = new FileReader();
    let file = e.target.files[0];
    if(file){
    let fileName = e.target.name;
    reader.onload = () => {
      let intArray = [];
      intArray = new Int8Array(reader.result);
      udsConnectivityDetails[fileName] = intArray;
      this.setState({
        udsConnectivityDetails: udsConnectivityDetails
      })
    }
    reader.readAsArrayBuffer(file);
  }
  }

  base64ToByteArray = (dataURI) => {
    
    var raw = window.atob(dataURI);
    var rawLength = dataURI.length;
    var array = new Int8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  saveUdsDetails = async () => {
    let { appContext, connectionTimeout, host, idleTimeout, poolMaxConnections, poolMinConnections, port, protocol, readTimeout, clientCert, clientPrivateKey, serverRootCert } = this.state.udsConnectivityDetails;
    if(serverRootCert){
    if ((isNaN(Object.values(serverRootCert)[0]))) {
      serverRootCert = this.base64ToByteArray(serverRootCert);
    }
  }
  if(clientCert){
    if ((isNaN(Object.values(clientCert)[0]))) {
      clientCert = this.base64ToByteArray(clientCert);
    }
  }
  if(clientPrivateKey){
    if ((isNaN(Object.values(clientPrivateKey)[0]))) {
      clientPrivateKey = this.base64ToByteArray(clientPrivateKey);
    }
  }
    const updateUdsConnectivity = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_URL['udsConnectivityConfig']}`,
      data: {
        "appContext": appContext,
        "clientCert": protocol === '2-WAY' ? (clientCert) ? Object.values(clientCert) : null : null,
        "clientPrivateKey": protocol === '2-WAY' ? (clientPrivateKey) ? Object.values(clientPrivateKey) : null : null,
        "clientPrivateKeyHSMKeyLabel": null,
        "connectionTimeout": connectionTimeout,
        "host": host,
        "idleTimeout": idleTimeout,
        "poolMaxConnections": poolMaxConnections,
        "poolMinConnections": poolMinConnections,
        "port": port,
        "protocol": protocol,
        "readTimeout": readTimeout,
        "serverRootCert": protocol === '1-WAY' || protocol === '2-WAY' ? (serverRootCert) ? Object.values(serverRootCert) : null : null,
        "software": true
      }
    };
    const updateUdsConnectivityStatus = await getService(updateUdsConnectivity);
    if (updateUdsConnectivityStatus && updateUdsConnectivityStatus.status === 200) {
      this.props.activateSuccessList(true, updateUdsConnectivityStatus.data);
      this.props.activateErrorList(false, '');
    }
    else {
      this.props.activateErrorList(true, updateUdsConnectivityStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }

  }
  render() {
    const { host, poolMaxConnections, poolMinConnections, port, readTimeout, protocol, appContext, connectionTimeout, idleTimeout } = this.state.udsConnectivityDetails;
    const{loader}=this.state;
    return <div className="main">
      {
        (loader) ?
          <React.Fragment>
            <h2 className="title">User Data Service Connectivity Configuration</h2>
            <p className="desc">Configure the User Data Service (UDS) to access user information.</p>
            <p className="desc">
              <b>Note 1:</b>It is optional to configure SSL between UDS and CA Products.</p>
            <p className="desc">
              <b>Note 2:</b>You must refresh the system cache of product servers for this change to take effect.</p>
            <span class="ecc-h1">User Data
Service Connectivity Configuration</span>
            <div className="col-sm-7">
              <Select
                name={'protocol'}
                title={'Protocol '}
                required={true}
                selectedOption={protocol}
                options={this.state.protocolArray}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'host'}
                required={true}
                inputType={'text'}
                name={'host'}
                content={host}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Port'}
                required={true}
                inputType={'text'}
                name={'port'}
                content={port}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Application Context Root'}
                required={true}
                inputType={'text'}
                name={'appContext'}
                content={appContext}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Connection Timeout (in milliseconds)'}
                required={true}
                inputType={'number'}
                content={connectionTimeout}
                name={'connectionTimeout'}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Read Timeout (in milliseconds)'}
                required={true}
                inputType={'number'}
                content={readTimeout}
                name={'readTimeout'}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Idle Timeout (in milliseconds)'}
                required={true}
                inputType={'number'}
                content={idleTimeout}
                name={'idleTimeout'}
                controlFunc={this.handleChange} />
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Server Root Certificate</label>
                <div className="col-md-8">
                  <input
                    type="file"
                    name={`serverRootCert`}
                    onChange={this.handleFileChange}
                    disabled={protocol === '1-WAY' || protocol === '2-WAY' ? false : true}
                  />
                </div></div>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Client Certificate</label>
                <div className="col-md-8">
                  <input
                    type="file"
                    name={`clientCert`}
                    onChange={this.handleFileChange}
                    disabled={protocol === '2-WAY' ? false : true}
                  />
                </div></div>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Client Private Key</label>
                <div className="col-md-8">
                  <input
                    type="file"
                    name={`clientPrivateKey`}
                    onChange={this.handleFileChange}
                    disabled={protocol === '2-WAY' ? false : true}
                  />
                </div></div>
              <SingleInput
                title={'Minimum Connections'}
                inputType={'number'}
                required={true}
                content={poolMinConnections}
                name={'poolMinConnections'}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Maximum Connections'}
                inputType={'number'}
                content={poolMaxConnections}
                required={true}
                controlFunc={this.handleChange}
                name={'poolMaxConnections'} />
              <div className="form-group form-submit-button row">
                <input className="secondary-btn" id="riskButton" type="submit" value="SAVE" onClick={this.saveUdsDetails}></input>
              </div>
            </div>
          </React.Fragment> : <React.Fragment></React.Fragment>}</div>;
  }
}

export default UDSConnectivity;
