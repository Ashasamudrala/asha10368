import React, { Component, Fragment } from 'react';
import Select from '../../shared/components/Select/Select';
import { RA_STR_SELECT, serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import './RulesDataReport.scss';
import CA_Utils from '../../shared/utlities/commonUtils';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';

class RulesDataReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      organizationList: [],
      originalOrganizations: [],
      ruleNames: [],
      listTypes: [],
      listoptions: ['ACTIVE', 'PROPOSED'],
      configState: ['ACTIVE'],
      orgName: '',
      selectedListType: '',
      dataSetName: '',
      datasetlist: [],
      decryptionNeeded: true,
      showDisplayReport: false,
      hideSensitiveInfoCheckbox: true,
      displayReportData: {},
      columns: [
        { title: RA_STR.rulesReportData, field: RA_STR.rulesReportFieldData },
        { title: RA_STR.rulesReportDateadded, field: RA_STR.rulesReportFieldDateadded }
      ],
      activePageNum: 1,
      pageNo: 0,
      pageSize: 10,
      totalCount: 0
    }
  }
  componentWillUnmount() {
    if (typeof this.props.activateErrorList === RA_STR.function) {
      this.props.activateErrorList(false, '');
      this.props.activateSuccessList(false, '');
    }
  }
  handleChange = (e) => {
    let { name, value } = e.target;
    if (name !== RA_STR.configState) {
      this.setState({ [name]: value }, () => {
        if (name === RA_STR.selectedListType) {
          this.getDataSetList();
        }
        if (name === RA_STR.dataSetName) {
          this.checkTheDataSet();
        }
      });
    } else if (name === RA_STR.configState) {
      this.setState({ [name]: [value] });
    }
  }
  newReport = (e) => {
    this.setState({
      ruleNames: [],
      listTypes: [],
      orgName: '',
      selectedListType: '',
      dataSetName: '',
      datasetlist: [],
      configState: ['ACTIVE'],
      decryptionNeeded: true,
      showDisplayReport: false,
      hideSensitiveInfoCheckbox: true
    })
  }
  onchange = (e) => {
    this.setState({ [e.target.name]: e.target.checked });
  }
  exportReports = async () => {
    let { configState, configurationName, dataSetName, orgName, selectedListType, decryptionNeeded } = this.state;
    let data = {
      configState: configState[0],
      configurationName,
      dataSetName,
      decryptionNeeded,
      orgName,
      selectedListType
    };
    const response = await getService({ method: 'POST', url: serverUrl + RA_API_URL.exportRuledataReport, data });
    if (response && response.status === 400) {
      this.props.activateErrorList(true, response.data.errorList);
    } else if (response.status === 200) {
      if (response && response.data) {
        const blob = new Blob([response.data], { type: 'application/octet-stream' });
        saveAs(blob, RA_STR.ruleDataRep + orgName + '.csv');
      }
    }
  }
  displayReport = async () => {
    let { configState, configurationName, dataSetName, orgName, selectedListType, decryptionNeeded, pageNo, pageSize, activePageNum } = this.state;
    let data = {
      configState: configState[0],
      configurationName,
      dataSetName,
      decryptionNeeded,
      orgName,
      pageNo,
      pageSize,
      activePageNum,
      selectedListType
    };
    const response = await getService({ method: 'POST', url: serverUrl + RA_API_URL.displayRuledataReport, data });
    if (response && response.data && response.status === 200) {
      this.setState({ displayReportData: response.data, showDisplayReport: true, totalCount: response.data.noOfRecords });
      this.props.activateErrorList(false, []);
    } else if (response && response.status === 400) {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, []);
    } else if (response && response.status === 500) {
      this.props.activateErrorList(true, response.data.errorList);
    }
  }

  getDataSetList = async () => {
    let { configState, configurationName, dataSetName, orgName, selectedListType, decryptionNeeded } = this.state;
    let data = {
      configState: configState[0],
      configurationName,
      dataSetName,
      decryptionNeeded,
      orgName,
      pageNo: 0,
      pageSize: 10,
      selectedListType
    };
    const response = await getService({ method: 'POST', url: serverUrl + RA_API_URL.getDataSetList, data });
    if (response && response.data && response.status === 200) {
      let datasetlist = response.data.datasetlist.map(obj => { return { key: obj, content: obj } });
      if (datasetlist.length === 1) {
        dataSetName = datasetlist[0].key;
      }
      this.setState({ datasetlist, hideSensitiveInfoCheckbox: response.data.hideSensitiveInfoCheckbox, dataSetName });
      this.props.activateErrorList(false, []);
    } else if (response && response.status === 400) {
      this.setState({ datasetlist: [], hideSensitiveInfoCheckbox: true });
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, []);
    }
  }
  getRulesetNames = async () => {
    const response = await getService({ method: 'GET', url: serverUrl + RA_API_URL.getRulesetNames + this.state.orgName });
    if (response && response.data && response.status === 200) {
      let { configurationName } = this.state;
      let ruleNames = response.data.ruleNames.map(obj => { return { key: obj, content: obj } });
      if (ruleNames.length === 1) {
        configurationName = ruleNames[0].key;
      }
      this.setState({ ruleNames, configurationName });
    }
  }
  getListTypes = async () => {
    const response = await getService({ method: 'GET', url: serverUrl + RA_API_URL.getRulesData });
    if (response && response.data && response.status === 200) {
      if (response.data.ruleListNames) {
        let data = Object.values(response.data.ruleListNames);
        let ruleListNames = Object.keys(response.data.ruleListNames).map((obj, index) => { return { key: data[index], content: obj } });
        this.setState({ listTypes: ruleListNames });
      }
    }
  }
  getOrganizations = async () => {
    const response = await getService({ method: 'GET', url: serverUrl + RA_API_URL.searchactions });
    if (response && response.status === 200) {
      let organizations = [RA_STR_SELECT, ...CA_Utils.objToArray(response.data.organizations, RA_STR.string)];
      this.setState({
        organizationList: organizations,
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, RA_STR.object)
      });
    }
  };
  checkTheDataSet = async () => {
    let { configurationName, orgName, pageNo, pageSize, selectedListType, dataSetName } = this.state;
    const data = { configurationName, orgName, pageNo, pageSize, selectedListType, dataSetName };
    const response = await getService({ method: 'POST', url: serverUrl + RA_API_URL.checkDataSetList, data });
    if (response && response.status === 200) {
      this.setState({ hideSensitiveInfoCheckbox: response.data });
    }
  }
  componentWillMount() {
    this.getOrganizations();
  }
  onOrgSelect = (value, type) => {
    if (type === RA_STR.click || type === RA_STR.blur) {
      let { selectedTab, originalOrganizations, listTypes } = this.state;
      const orgId = originalOrganizations.find((element) => { if (element.content === value) return element.key });
      if (selectedTab === '' && orgId) {
        selectedTab = RA_STR.searchCriteriaTab;
      }
      if ((type === RA_STR.blur && !orgId && this.refs.autoSuggestionBox) || (value === RA_STR_SELECT && this.refs.autoSuggestionBox)) {
        this.refs.autoSuggestionBox.setvalue('');
      }
      let orgName = orgId ? orgId.key : '';
      this.setState({ orgName, datasetlist: [], dataSetName: ' ', selectedListType: ' ', hideSensitiveInfoCheckbox: true }, () => {
        if (listTypes.length === 0) {
          this.getListTypes();
        }
        this.getRulesetNames();
      });
    }
  }
  getActivePage = (getPageNum) => {
    let pageNumCount = getPageNum - 1;
    this.setState({ pageNo: pageNumCount, activePageNum: getPageNum }, () => {
      this.displayReport();
    })
  }
  render() {
    let { organizationList, ruleNames, listTypes, listoptions, displayReportData, datasetlist, showDisplayReport, decryptionNeeded, configState, hideSensitiveInfoCheckbox, orgName, configurationName, dataSetName, selectedListType, columns, pageSize, totalCount, activePageNumNew } = this.state;
    return (
      <div className='main rulesData'>
        <h2 className='title'>{RA_STR.rulesDataReport}</h2>
        {!showDisplayReport ? <div>
          <p className='desc'>{RA_STR.instanceDesc}</p>
          <hr />
          <div className='col-sm-8 mt-2'>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">{RA_STR.organizationHead}</label>
              <div ref='autoSuggestContainer'>
                <AutoSuggest ref="autoSuggestionBox" orgOptions={organizationList} getSelectedOrgDetails={this.onOrgSelect} enableAutosuggest={true} defaultOrganizationPlaceholder={RA_STR_SELECT} value={orgName} />
              </div>
            </div>
            {ruleNames.length !== 1 ? <Select
              name={'configurationName'}
              title={'Ruleset Name'}
              options={ruleNames}
              controlFunc={this.handleChange}
              placeholder={RA_STR_SELECT}
              selectedOption={configurationName}
            /> : <div className="row mt-4 mb-3"><label className="col-sm-4 pl-0">Ruleset Name</label>{configurationName}</div>}
            <Select
              name={'selectedListType'}
              title={'Select List Type'}
              options={listTypes}
              controlFunc={this.handleChange}
              selectedOption={selectedListType}
              placeholder={RA_STR_SELECT}
            />
            {datasetlist.length !== 1 ? <Select
              name={'dataSetName'}
              title={'Select List'}
              options={datasetlist}
              controlFunc={this.handleChange}
              selectedOption={dataSetName}
              placeholder={RA_STR_SELECT} /> : <div className="row mt-4 mb-3"><label className="col-sm-4 pl-0">Select List	</label>{dataSetName}</div>}
            <div className='row margin'>
              <div className='col-sm-8 no-padd setCustomLabelWidth mt-2'>
                <RadioGroup
                  setName='configState'
                  type={'radio'}
                  title=''
                  options={listoptions}
                  selectedOptions={configState}
                  controlFunc={this.handleChange}
                />
              </div>
            </div>
            <div className="row mt-5">
              {!hideSensitiveInfoCheckbox && <div className="col-sm-5 pl-0">
                <div className="custom-control custom-checkbox pa-6">
                  <label key='decryptionNeeded' className='form-label capitalize col-sm-12' >
                    <input
                      className="form-checkbox custom-control-input"
                      type='checkbox'
                      id='decryptionNeeded'
                      name='decryptionNeeded'
                      onChange={this.onchange}
                      value={decryptionNeeded}
                      checked={decryptionNeeded}
                    />
                    <label className="form-label custom-control-label" htmlFor='decryptionNeeded'>{RA_STR.DecryptInfo}</label>
                  </label>
                </div>
              </div>}
              {orgName !== '' && <div className="col-sm-5">
                <div className="form-group row ">
                  <input className="secondary-btn" id="searchButton" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
                </div>
              </div>}
            </div >
          </div>
        </div>
          :
          <div>
            <div className="col-md-12 row">
              <div className="col-md-9 no-padding">
                <p>{RA_STR.descOfrules}</p>
              </div>
              <div className="float-right col-md-3">
                <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportReports}></input>
                <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.newReport}></input>
              </div>
            </div>

            {displayReportData.request && <Fragment>
              <div className="mt-2 detailsOfReport">
                <p>Organization Name: {displayReportData.request.orgName}</p>
                <p>List Name: {displayReportData.request.dataSetName}</p>
                <p>Rule Set: {displayReportData.request.configurationName} </p>
                <p>State: {displayReportData.request.configState}</p>
                <p>List Type: {displayReportData.request.selectedListType}</p>
              </div>
              <DynamicTable columns={columns} data={displayReportData.simpleListResponse} enablePagination={true} pageSize={pageSize} totalCount={totalCount} selection={false} activePage={this.getActivePage} activePageNumNew={activePageNumNew} />
            </Fragment>}
          </div>
        }
      </div>
    )
  }
}
export default RulesDataReport;
