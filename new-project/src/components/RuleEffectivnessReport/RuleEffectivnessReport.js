import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_SELECT, RA_API_STATUS } from '../../shared/utlities/constants';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';
import './RuleEffectivnessReport.scss';

class RuleEffectivnessReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultOrganizationName: RA_STR_SELECT,
      organizationNames: [],
      defaultChannel: 'All',
      channelList: [],
      orgDetails: false,
    

      showTable: false,
      apiData: []
    };
  }

  componentDidMount() {
    this.getAllOrganizations();
  }

  // Get Organisation Details
  getAllOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };

  // On select of Org Name enabling Channels Dropdown
  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find(element => {
        if (element.content === value) return element.key;
      });
      this.setState({
        defaultOrganizationName: orgId.key,
        orgDetails: true
      });
      this.getChannels(orgId.key);
    }
  };

  //Getting Channels
  getChannels = async selectedOrg => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['common']}/${selectedOrg}${RA_API_URL['channels']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      let channelList = [];
      if (response.data.length > 1) {
       channelList = [
        {
          content: 'All Channels',
          key: 'All'
        }
      ];
    }
      response.data.forEach(obj => {
        channelList.push({
          content: obj.displayName,
          key: obj.name
        });
      });
      this.setState({ channelList: channelList });
    }
  };

  // Setting Default Channel
  onChannelSelect = event => {
    const channel = event.target.value;
    this.setState({ defaultChannel: channel });
  };

  // For Report Display
  displayReport = async () => {
    const display = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['ruleEffectiveness']}`,
      data: {
        orgName: this.state.defaultOrganizationName,
        selectedChannel: this.state.defaultChannel
      }
    };
    const response = await getService(display);
    if (response && response.status === RA_API_STATUS['200']) {
      this.props.activateErrorList(false, '');
      this.setState({
        showTable: true,
        apiData: response.data.txnsByMatchedRuleList,
        channnels: response.data.channelString
      });
    } else if (response && response.status === RA_API_STATUS['400'] && response.data.errorList) {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
      this.setState({ showTable: false });
      window.scrollTo(0, 0);
    }
  };

  // Redirect to new Page
  newReport = () => {
    this.setState({
      showTable: false,
      orgDetails:false,
      defaultOrganizationName:RA_STR_SELECT,
    });
  };

  // Download Report
  downloadCSV(response) {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'RuleImpactReport'+this.state.defaultOrganizationName+'.csv');
    }
  }
  // For Export
  reportExport = async () => {
    const reportExpo = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['ruleEffectExport']}`,
      data: {
        orgName: this.state.defaultOrganizationName,
        selectedChannel: this.state.defaultChannel
      }
    };
    const exportSuccessData = await getService(reportExpo);
    if (exportSuccessData && exportSuccessData.status === RA_API_STATUS['200']) {
      this.downloadCSV(exportSuccessData);
    } else if (exportSuccessData && exportSuccessData.status === RA_API_STATUS['400'] && exportSuccessData.data.errorList) {
      this.props.activateErrorList(true, exportSuccessData.data.errorList);
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  };

  render() {
    const { organizationNames, defaultOrganizationName, channelList, defaultChannel, channnels, apiData } = this.state;
    return (
      <div className='main'>
        {!this.state.showTable ? (
        <div>
          <h2 className='title'>{RA_STR.ruleTitle}</h2>
          <p className='desc'>{RA_STR.reportRules}</p>
          <hr />
          <div className='col-sm-6'>
            <div className='form-group dynamic-form-select'>
              <div className='form-group row'>
                <label className='col-sm-4 col-form-label'>{RA_STR.orgzName}</label>
                <div className='col-sm-8'>
                  <AutoSuggest
                    orgOptions={organizationNames}
                    getSelectedOrgDetails={this.onOrgSelect}
                    enableAutosuggest={true}
                    defaultOrganizationPlaceholder={defaultOrganizationName}
                  />
                </div>
              </div>
            </div>
          </div>
          {this.state.orgDetails ? (
            <div className='col-sm-6'>
              <Select
                name={'channel'}
                title={RA_STR.channel}
                required={false}
                selectedOption={defaultChannel}
                options={channelList ? channelList : ['']}
                controlFunc={this.onChannelSelect}
              />
            </div>
          ) : (
            <div></div>
          )}
          {this.state.orgDetails ? (
            <div className='col-sm-6'>
              <div className='form-group form-submit-button row mt-5'>
                <input
                  className='secondary-btn'
                  type='submit'
                  value='DISPLAY REPORT'
                  onClick={this.displayReport}
                ></input>
              </div>
            </div>
          ) : (
            <div></div>
          )}
        </div>
           ) : (
            <React.Fragment />
          )}
        {this.state.showTable ? (
          <div>
            <h2>{RA_STR.ruleTitle}</h2>
            <div className='form-group row ReportButtons'>
              <p className='col-sm-7 desc'>{RA_STR.reportRules}</p>
              <input className='col-sm-1 secondary-btn' type='submit' value='EXPORT' onClick={this.reportExport}></input>
              <input
                className='col-sm-1 secondary-btn'
                id='searchButton'
                type='submit'
                value='NEW REPORT'
                onClick={this.newReport}
              ></input>
            </div>
            <div className='col-sm-6'>
              <div className='form-group row'>
                <label className='col-sm-4 col-form-label'>{RA_STR.orgName}</label>
                <div className='col-sm-8'>{defaultOrganizationName}</div>
              </div>
              <div className='form-group row'>
                <label className='col-sm-4 col-form-label'>{RA_STR.channel}:</label>
                <div className='col-sm-8'>{channnels}</div>
              </div>
            </div>
            <div style={{ maxWidth: '100%', clear: 'both' }}>
              <table className='rule-effect-table' style={{ maxWidth: '100%', clear: 'both' }}>
                <thead>
                  <tr className='main-header'>
                    <th className='col-head' rowspan='2'>
                      {RA_STR.ruleName}
                    </th>
                    <th className='col-head' rowspan='2'>
                      {RA_STR.advice}
                    </th>
                    <th className='col-head' rowspan='2'>
                      {RA_STR.txYesCount}
                    </th>
                    <th className='col-head' colspan='2'>
                      {RA_STR.days_7}
                    </th>
                    <th className='col-head' colspan='2'>
                      {RA_STR.days_14}
                    </th>
                  </tr>
                  <tr className='main-header'>
                    <th className='col-head child-head' colspan='1'>
                      {RA_STR.txCount}
                    </th>
                    <th className='col-head child-head' colspan='1'>
                      {RA_STR.dailyAvg}
                    </th>
                    <th className='col-head child-head' colspan='1'>
                      {RA_STR.txCount}
                    </th>
                    <th className='col-head child-head' colspan='1'>
                      {RA_STR.dailyAvg}
                    </th>
                  </tr>
                </thead>
                {apiData.map((item,value) => {
                      return (
                        <tr key={value}>
                          <td>{item.matchedRule}</td>
                          <td>{item.advice}</td>
                          <td>{item.txnYesterday}</td>
                          <td>{item.txnLast7Days}</td>
                          <td>{item.txn7DaysAvg}</td>
                          <td>{item.txnLast14Days}</td>
                          <td>{item.txn14DaysAvg}</td>
                        </tr>
                      );
                    })}
              </table>
            </div>
          </div>
        ) : (
          <React.Fragment />
        )}
      </div>
    );
  }
}

export default RuleEffectivnessReport;
