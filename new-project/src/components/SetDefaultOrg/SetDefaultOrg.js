import React, { Component } from 'react';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import CA_Utils from '../../shared/utlities/commonUtils';

class SetDefaultOrg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgOptions: [],
      selectedOrgName: ''
    };
  }

  componentDidMount() {
    this.getDefaultOrgs();
    this.getSelectedOrgName();
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  getSelectedOrgName = async () => {
    const selectedOrgName = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['setDefaultOrg']}`
    };
    const defaultOrganizationsData = await getService(selectedOrgName);
    const orgNames = Object.keys(defaultOrganizationsData.data.organizations);
    this.setState({ selectedOrgName: orgNames[0] });
  };

  getDefaultOrgs = async () => {
    const defaultOrganizations = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getDefaultOrgs']}`
    };
    const defaultOrganizationsData = await getService(defaultOrganizations);
    const orgNames = Object.values(defaultOrganizationsData.data.organizations);
    this.setState({ 
      orgOptions: orgNames, 
      originalOrganizations: CA_Utils.objToArray(defaultOrganizationsData.data.organizations, 'object')
    });
  };

  setDefaultOrg = async () => {
    const setDefaultOrganization = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_URL['setDefaultOrg']}/${this.state.selectedOrgName}`
    };
    const updateDefaultOrg = await getService(setDefaultOrganization);
    if (updateDefaultOrg && updateDefaultOrg.status === 200) {
      this.props.activateSuccessList(true, updateDefaultOrg.data);
      this.props.activateErrorList(false, '');
    } else {
      this.props.activateErrorList(true, updateDefaultOrg.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  };

  getSelectedOrgDetails = (getSelectedOrgDetail, type) => {
    if (type === 'click' && getSelectedOrgDetail !== null) {
    const orgId = this.state.originalOrganizations.find(element => {
      if (element.content === getSelectedOrgDetail) return element.key;
    });
    if (orgId !== null) {
    this.setState({
      selectedOrgName: orgId.key
    });
  }
}
  };

  render() {
    return (
      <div className='main'>
        <h2 className='title'>Set Default Organization</h2>
        <p className='desc'>Set the default organization from the available list of organizations.</p>
        <p className='desc'>
          <b>Note:</b> You must refresh the system cache of product servers for this change to take effect.
        </p>
        <div className='col-sm-6 div-seperator'>
          <div className='form-group row'>
            <label className='col-sm-4 col-form-label'>Default Organization</label>
            <div className='col-sm-8'>
              <AutoSuggest
                name={'selectedOrgName'}
                orgOptions={this.state.orgOptions}
                getSelectedOrgDetails={this.getSelectedOrgDetails}
                enableAutosuggest={true}
                defaultOrganizationPlaceholder={this.state.selectedOrgName}
              />
            </div>
          </div>
        </div>
        <div className='form-group form-submit-button'>
          <input
            className='secondary-btn'
            id='createRoleButton'
            type='submit'
            value='Save'
            onClick={this.setDefaultOrg}
          ></input>
        </div>
      </div>
    );
  }
}

export default SetDefaultOrg;
