import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';

class ManageQueue extends Component {
  render() {
    const localeOptions = [];
    return (
      <div className='main'>
        <h2 className='title'>Manage Queues</h2>
        <p className='desc'>Assign administrators who will work on Cases. Use 'Order By' to change the order in which Cases are displayed to the Case Handlers. Specify 'Criteria' for the cases to be assigned to the queue.
        </p>
        <p>For the changes to take effect, perform cache refresh.</p>
        <div className='col-sm-8'>
          <Select
            name={'orgName'}
            title={'Select Organization'}
            options={localeOptions ? localeOptions : ['']}
            controlFunc={this.handleChange} />
        </div>
      </div>
    )
  }
}

export default ManageQueue;
