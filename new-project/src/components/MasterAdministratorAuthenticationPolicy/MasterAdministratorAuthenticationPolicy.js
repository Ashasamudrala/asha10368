import React, { Component } from 'react';
import './MasterAdministratorAuthentication.css'
import BasicAuthenticationPolicy from '../BasicAuthenticationPolicy/BasicAuthenticationPolicy';

class MasterAdministratorAuthenticationPolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 1
        }
    }
    render() {
        return (
            <BasicAuthenticationPolicy {...this.props} />
        )

    }

}

export default MasterAdministratorAuthenticationPolicy;
