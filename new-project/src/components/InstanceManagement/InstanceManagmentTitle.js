import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import './InstanceManagement.scss';
import * as _ from 'underscore';

class InstanceManagmentTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            instanceData: this.props.location.state.data,
            instancenamecheckbox: true,
            logLevelData: [{ 'content': 'FATAL', 'key': '0' }, { 'content': 'WARNING', 'key': '1' }, { 'content': 'INFO', 'key': '2' }, { 'content': 'DETAIL', 'key': '3' }],
            brokerList: []
        }
    }
    componentDidMount = async () => {
        let brokerList = [{ 'key': '0', 'content': '--Select--' }];
        const getBrokerList = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['brokerList']}`
        }
        const getBrokerListStatus = await getService(getBrokerList);

        if (getBrokerListStatus && getBrokerListStatus.status === 200) {
            let data = _.mapObject(getBrokerListStatus.data.brokerList, function (val, key) {
                return val;
            });
            [data].map((data, index) => {
                brokerList.push({
                    'key': index + 1,
                    'content': Object.values(data)
                })
            })
            this.setState({ brokerList: brokerList })
        }
    }
    handleChange = (e) => {
        const { instanceData } = this.state;
        if (e.target.name === 'instancenamecheckbox') {
            this.setState({ instancenamecheckbox: !(this.state.instancenamecheckbox) })
        }
        else if ((e.target.name).startsWith("broker")) {
            instanceData['brokerArray'][parseInt(e.target.name.substring(6))] = e.target.value;
        } else {
            if (e.target.type === 'checkbox') {
                instanceData[e.target.name] = e.target.checked;
            } else {
                instanceData[e.target.name] = e.target.value;
            }
            this.setState({ instanceData: instanceData })
        }
    }
    redirectToHelp = (e) => {
        this.props.history.push({
            pathname: `/server-config/ra/instance-manage`
        });
    }
    saveCaseManagement = async () => {
        const saveUrl = (this.props.location.state.instanceType === 'rfInstance') ? RA_API_URL['updateRiskfortInstance'] : RA_API_URL['updateCasemanagementInstance'];
        const { instanceData } = this.state;
        const rfInstanceDataSave = {
            method: 'POST',
            url: `${serverUrl}${saveUrl}`,
            data: {
                "actionID": 0,
                "allOrgsSelected": true,
                "argusArray": instanceData.argusArray,
                "autoRevert": true,
                "avlOrgs": [{}],
                "backupLogFileDirectory": instanceData.backupLogFileDirectory,
                "brokerArray": instanceData.brokerArray,
                "canUserShutdown": true,
                "dateCreated": instanceData.dateCreated,
                "dateModified": instanceData.dateModified,
                "dbConnRetrySleepMilliSeconds": instanceData.dbConnRetrySleepMilliSeconds,
                "dbQueryTimeOutSecs": instanceData.dbQueryTimeOutSecs,
                "editName": instanceData.editName,
                "enableAllConfigCacheLogging": instanceData.enableAllConfigCacheLogging,
                "enableConnectivityStats": instanceData.enableConnectivityStats,
                "enableThreadStats": instanceData.enableThreadStats,
                "enableTraceLogging": instanceData.enableTraceLogging,
                "hostName": instanceData.hostName,
                "incDBConnections": instanceData.incDBConnections,
                "instanceArcotHome": instanceData.instanceArcotHome,
                "instanceID": instanceData.instanceID,
                "instanceIP": instanceData.instanceIP,
                "instanceName": instanceData.instanceName,
                "instanceType": instanceData.instanceType,
                "instanceVersion": instanceData.instanceVersion,
                "lastRefreshTime": instanceData.lastRefreshTime,
                "lastServerShutdownTime": instanceData.lastServerShutdownTime,
                "logDirectory": instanceData.logDirectory,
                "logFileSize": instanceData.logFileSize,
                "logLevel": instanceData.logLevel,
                "logQueryDetails": instanceData.logQueryDetails,
                "logTimeGMT": instanceData.logTimeGMT,
                "masterAdmin": instanceData.masterAdmin,
                "maxDBConnections": instanceData.maxDBConnections,
                "maxWaitOnPoolWhenFull": instanceData.maxWaitOnPoolWhenFull,
                "minDBConnections": instanceData.minDBConnections,
                "monitorSleepTimeFailureSecs": instanceData.monitorSleepTimeFailureSecs,
                "monitorSleepTimeSecs": instanceData.monitorSleepTimeSecs,
                "newInstanceName": instanceData.newInstanceName,
                "proactivePool": instanceData.proactivePool,
                "rebuildAllQueues": instanceData.rebuildAllQueues,
                "scopeAll": instanceData.scopeAll,
                "selected": instanceData.selected,
                "selectedOrgNames": instanceData.selectedOrgNames,
                "selectedOrgs": [
                    {}
                ],
                "serverShutDown": instanceData.serverShutDown,
                "serverType": instanceData.serverType,
                "serverUpTime": instanceData.serverUpTime,
                "startupTime": instanceData.startupTime,
                "statsPeriodMins": instanceData.statsPeriodMins,
                "status": instanceData.status,
                "systemRefresh": instanceData.systemRefresh
            }
        }
        const rfInstanceDataSaveStatus = await getService(rfInstanceDataSave);
        if (rfInstanceDataSaveStatus && rfInstanceDataSaveStatus.status === 200) {
            this.props.activateSuccessList(true, rfInstanceDataSaveStatus.data);
            this.props.activateErrorList(false, '');
        } else {
            this.props.activateErrorList(true, rfInstanceDataSaveStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }
    render() {
        const { instanceData, instancenamecheckbox, brokerList } = this.state;
        return (
            <div className="main instance-management">
                <h2 className="title">{RA_STR.casemanagementInstanceField} {instanceData.instanceName}</h2>
                <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.caseinstancemanagmentheading }}></p>
                <span className="ecc-h1">{RA_STR.caseinstancemanagmentinstanceattributes}</span>
                <div className="col-sm-6">
                    <div className="div-seperator">
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmentinstancename}</label>
                            <div className="col-sm-8 checkbox">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="instancenamecheckbox" name="instancenamecheckbox" onChange={this.handleChange} />
                                    <label className="custom-control-label" htmlFor="instancenamecheckbox"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="div-seperator">
                        <SingleInput
                            title={'New Instance Name'}
                            inputType={'text'}
                            name={'newInstanceName'}
                            content={instanceData.newInstanceName}
                            controlFunc={this.handleChange}
                            disabled={instancenamecheckbox} />
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmentservertype}</label>
                        <div className="col-sm-8 mt-2">
                            {instanceData.serverType}
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmenthostname}</label>
                        <div className="col-sm-8 mt-2">
                            {instanceData.hostName}
                        </div>
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmentarcothome}</label>
                        <div className="col-sm-8 mt-2">
                            {instanceData.instanceArcotHome}
                        </div>
                    </div>
                </div>
                <div className="div-seperator">
                    <span className="ecc-h1">{RA_STR.caseinstancemanagmentloggingconfiguration}</span>
                    <p className="desc"><i>{RA_STR.caseinstancemanagmentabsolutepath}</i></p>
                </div>
                <div className="col-sm-6">
                    <SingleInput
                        title={'Transaction Log Directory'}
                        inputType={'text'}
                        name={'logDirectory'}
                        content={instanceData.logDirectory}
                        controlFunc={this.handleChange} />
                    <SingleInput
                        title={'Rollover After (in Bytes)'}
                        inputType={'text'}
                        maxLength={'12'}
                        name={'logFileSize'}
                        content={instanceData.logFileSize}
                        controlFunc={this.handleChange} />
                    <SingleInput
                        title={'Transaction Log Backup Directory'}
                        inputType={'text'}
                        name={'backupLogFileDirectory'}
                        content={instanceData.backupLogFileDirectory}
                        controlFunc={this.handleChange} />
                    <div className='instance-select'>
                        <Select
                            name={'logLevel'}
                            selectedOption={instanceData.logLevel}
                            controlFunc={this.handleChange}
                            title={'Log Level'}
                            options={this.state.logLevelData} />
                    </div>

                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmenttimestamp}</label>
                        <div className="col-sm-8 checkbox">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="logTimeGMT" name="logTimeGMT" onChange={this.handleChange} checked={instanceData.logTimeGMT} />
                                <label className="custom-control-label" htmlFor="logTimeGMT"></label>
                            </div>
                        </div>
                    </div>


                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmentenabletrace}</label>
                        <div className="col-sm-8 checkbox">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="enableTraceLogging" name="enableTraceLogging" checked={instanceData.enableTraceLogging} onChange={this.handleChange} />
                                <label className="custom-control-label" htmlFor="enableTraceLogging"></label>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="div-seperator">
                    <span className="ecc-h1">{RA_STR.caseinstancemanagmentdatabase}</span>
                </div>
                <div className="col-sm-6">
                    <SingleInput
                        title={'Minimum Connections'}
                        maxLength={'4'}
                        inputType={'text'}
                        name={'minDBConnections'}
                        content={instanceData.minDBConnections}
                        controlFunc={this.handleChange} />
                    <SingleInput
                        title={'Maximum Connections'}
                        inputType={'text'}
                        maxLength={'4'}
                        name={'maxDBConnections'}
                        content={instanceData.maxDBConnections}
                        controlFunc={this.handleChange} />
                    <SingleInput
                        title={'Increment Connections by'}
                        inputType={'text'}
                        maxLength={'4'}
                        name={'incDBConnections'}
                        content={instanceData.incDBConnections}
                        controlFunc={this.handleChange} />
                    <SingleInput
                        title={'Monitor Thread Sleep Time (in Seconds)'}
                        inputType={'text'}
                        maxLength={'4'}
                        name={'monitorSleepTimeSecs'}
                        content={instanceData.monitorSleepTimeSecs}
                        controlFunc={this.handleChange} />
                    <SingleInput
                        title={'Monitor Thread Sleep Time in Fault Conditions (in Seconds)'}
                        inputType={'text'}
                        name={'firstName'}
                        maxLength={'4'}
                        content={instanceData.monitorSleepTimeFailureSecs}
                        controlFunc={this.handleChange} />
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmentlogquery}</label>
                        <div className="col-sm-8 checkbox">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="logQueryDetails" name="logQueryDetails" checked={instanceData.logQueryDetails} onChange={this.handleChange} />
                                <label className="custom-control-label" htmlFor="logQueryDetails"></label>
                            </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.casemanagementdatabaseconnectivity}</label>
                        <div className="col-sm-8 checkbox">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="proactivePool" name="proactivePool" onChange={this.handleChange} checked={instanceData.proactivePool} />
                                <label className="custom-control-label" htmlFor="proactivePool"></label>
                            </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.caseinstancemanagmentautorevert}</label>
                        <div className="col-sm-8 checkbox">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="autoRevert" name="autoRevert" checked={instanceData.autoRevert} onChange={this.handleChange} />
                                <label className="custom-control-label" htmlFor="autoRevert"></label>
                            </div>
                        </div>
                    </div>
                </div>
                {this.props.location.state.instanceType === 'rfInstance' ?
                    <React.Fragment>
                        <div className="div-seperator">
                            <span class="ecc-h1">{RA_STR.caseinstancemanagmentbroker}</span>
                        </div>
                        <div className="col-sm-6">
                            <Select
                                name={'broker0'}
                                value={instanceData.brokerArray[0] || '0'}
                                controlFunc={this.handleChange}
                                title={'Broker  1'}
                                options={brokerList} />
                            <Select
                                name={'broker1'}
                                value={instanceData.brokerArray[1]}
                                controlFunc={this.handleChange}
                                title={'Broker  2'}
                                options={brokerList} />
                            <Select
                                name={'broker2'}
                                value={instanceData.brokerArray[2]}
                                controlFunc={this.handleChange}
                                title={'Broker  3'}
                                options={brokerList} />
                            <Select
                                name={'broker3'}
                                value={instanceData.brokerArray[3]}
                                controlFunc={this.handleChange}
                                title={'Broker  4'}
                                options={brokerList} />
                        </div>
                    </React.Fragment>
                    : <React.Fragment />}
                <div className='row'>
                    <input className="secondary-btn ml-2" id="createRoleButton" type="submit" value="SAVE" onClick={this.saveCaseManagement}></input>
                    <input className="custom-secondary-btn ml-3" id="custom-secondary-btn" type="button" value="BACK" onClick={this.redirectToHelp}></input>
                </div>
            </div>
        );
    }

}

export default InstanceManagmentTitle;