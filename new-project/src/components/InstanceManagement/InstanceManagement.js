import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';

class InstanceManagement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [
                { title: RA_STR.casemanagementInstanceField, field:RA_STR.casemanagementInstanceTitle, render: rowData => this.getInstanceName(rowData, 'rfInstance') },
                {
                    title: RA_STR.casemanagementLastStartUpTimeField, field: RA_STR.casemanagementLastStartUpTimeTitle, render: (rowData) =>
                        <div>{rowData.startupTime}</div>
                },
                {
                    title: RA_STR.casemangementLastShutDownTimeField, field: RA_STR.casemangementLastShutDownTimeTitle, render: (rowData) =>
                        <div>{rowData.lastServerShutdownTime}</div>
                },
                {
                    title: RA_STR.casemanagementLastRefreshTimeField, field: RA_STR.casemanagementLastRefreshTimeTitle, render: (rowData) =>
                        <div>{rowData.lastRefreshTime}</div>
                },
                {
                    title: RA_STR.casemanagementUptimeField, field: RA_STR.casemanagementUptimeTitle, render: (rowData) =>
                    this.convertServerUpTime(rowData)
                },
                {
                    title: RA_STR.casemanagementStatusField, field: RA_STR.casemanagementStatusTitle, render: (rowData) =>
                        <div>{rowData.statusInfo}</div>
                },
                {
                    title: RA_STR.casemanagementOrganisationRefreshField, field: RA_STR.bulkreportviewfieldbatchType, render: (rowData) =>
                        <div className="dynamic-form-select"><select className="form-select form-control custom-select-sm disabled"> <option value="">ALL</option>
                        </select></div>
                },
                {
                    title: RA_STR.casemanagementSystemCacheField, field: RA_STR.casemanagementSystemCacheTitle, render: (rowData) =>
                        <div className="checkbox">
                            <input id={`customCheckbox${rowData.indexId}`} type="checkbox" checked={rowData.systemRefresh} onChange={this.handleCheckbox.bind(this, rowData.indexId)} />
                            <label htmlFor={`customCheckbox${rowData.indexId}`}></label>
                        </div>
                }
            ],
            casemanagmentcolumns: [
                { title: RA_STR.casemanagementInstanceField, field: RA_STR.casemanagementInstanceTitle, render: rowData => this.getInstanceName(rowData, 'cacheInstance') },
                {
                    title: RA_STR.casemanagementLastStartUpTimeField, field: RA_STR.casemanagementLastStartUpTimeTitle, render: (rowData) =>
                        <div>{rowData.startupTime}</div>
                },
                {
                    title: RA_STR.casemangementLastShutDownTimeField, field: RA_STR.casemangementLastShutDownTimeTitle, render: (rowData) =>
                        <div>{rowData.lastServerShutdownTime}</div>
                },
                {
                    title: RA_STR.casemanagementLastRefreshTimeField, field: RA_STR.casemanagementLastRefreshTimeTitle, render: (rowData) =>
                        <div>{rowData.lastRefreshTime}</div>
                },
                {
                    title: RA_STR.casemanagementUptimeField, field: RA_STR.casemanagementUptimeTitle, render: (rowData) =>
                    this.convertServerUpTime(rowData)
                },
                {
                    title: RA_STR.casemanagementStatusField, field: RA_STR.casemanagementStatusTitle, render: (rowData) =>
                        <div>{rowData.statusInfo}</div>
                },
                {
                    title: RA_STR.casemanagementOrganisationRefreshField, field: RA_STR.bulkreportviewfieldbatchType, render: (rowData) =>
                        <div className="dynamic-form-select"><select className="form-select form-control custom-select-sm disabled"> <option value="">ALL</option>
                        </select></div>
                },
                {
                    title: RA_STR.casemanagementSystemCacheField, field: RA_STR.casemanagementSystemCacheTitle, render: (rowData) =>
                        <div className="checkbox">
                            <input id={`customCheckbox${rowData.indexId}`} type="checkbox" checked={rowData.systemRefresh} onChange={this.handlecaseInstanceCheckbox.bind(this, rowData.indexId)} />
                            <label htmlFor={`customCheckbox${rowData.indexId}`}></label>
                        </div>
                }
            ],
            apiData: [],
            apiCaseManagmentData: [],
            selectedInstances: []
        }
    }

    getInstanceName = (rowData, instanceType) => {
        return <div className="orgName" onClick={() => this.data(rowData, instanceType)}>{rowData.instanceName}</div>
    }

    data = (rowData, instanceType) => {
        this.props.history.push({
            pathname: `/server-config/ra/instance-manage-title`,
            state: {
                data: rowData,
                instanceType: instanceType
            }
        });
    }

    getcheckboxselection = (selectedInstanceData) => {
        this.setState({ selectedInstances: selectedInstanceData });
    }
    convertServerUpTime = (rowData) => {
        let serverUpTime = rowData.serverUpTime ;
        serverUpTime = serverUpTime.replace(/Optional/gi, '').replace(/[\[\]']+/g,'');
        return <div>{serverUpTime}</div>
    }
    componentDidMount = async () => {
        const getConfigurationData = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['rfInstances']}`
        }
        const getConfigurationStatus = await getService(getConfigurationData);
        if (getConfigurationStatus && getConfigurationStatus.status === 200) {
            let rfInstanceList = getConfigurationStatus.data.rfInstanceList;
            let caseMgmtInstanceList = getConfigurationStatus.data.caseMgmtInstanceList
            rfInstanceList.map((data, index) => {
                data['indexId'] = index 
                data['statusInfo'] = 'Running'
            })
            caseMgmtInstanceList.map((data, index) => {
                data['indexId'] = index
                data['statusInfo'] = 'Running'
            })
            this.setState({ apiData: rfInstanceList, apiCaseManagmentData: caseMgmtInstanceList });
        }
    }
    handleCheckbox = (data, e) => {
        this.state.apiData[data].systemRefresh = e.target.checked;
        this.setState(this.state.apiData);
    }

    handlecaseInstanceCheckbox = (data, e) => {
        this.state.apiCaseManagmentData[data].systemRefresh = e.target.checked;
        this.setState(this.state.apiCaseManagmentData);
    }

    saveShutdownInfo = async () => {
        let instanceShutdown = [];
        this.state.selectedInstances.map((instanceData) => {
            let data = {
                "host": instanceData.hostName,
                "instanceID": instanceData.instanceID,
                "instanceName": instanceData.instanceName,
                "serverType": instanceData.serverType,
            }
            instanceShutdown.push(data);
        });
        const rfInstanceShutdown = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['rfInstanceShutdown']}`,
            data: { 'instanceShutdown': instanceShutdown }
        }
        const rfInstanceStatus = await getService(rfInstanceShutdown);
        if (rfInstanceStatus && rfInstanceStatus.status === 200) {
            this.props.activateSuccessList(true, rfInstanceStatus.data);
            this.props.activateErrorList(false, '');
        } else {
            this.props.activateErrorList(true, rfInstanceStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }

    saveRefreshInfo = async () => {
        let refreshInstance = [];
        this.state.selectedInstances.map((instanceData) => {
            let data = {
                "allOrgsRefresh": instanceData.allOrgsSelected,
                "host": instanceData.hostName,
                "instanceID": instanceData.instanceID,
                "instanceName": instanceData.instanceName,
                "orgNames": instanceData.avlOrgs,
                "serverType": instanceData.serverType,
                "systemRefresh": instanceData.systemRefresh
            }
            refreshInstance.push(data);
        });
        const rfInstanceRefresh = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['rfInstanceRefresh']}`,
            data: { 'refreshInstance': refreshInstance }
        }
        const rfInstanceRefreshStatus = await getService(rfInstanceRefresh);
        if (rfInstanceRefreshStatus && rfInstanceRefreshStatus.status === 200) {
            this.props.activateSuccessList(true, rfInstanceRefreshStatus.data);
            this.props.activateErrorList(false, '');
        } else {
            this.props.activateErrorList(true, rfInstanceRefreshStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    savecaseInstanceShutdownInfo = async () => {
        let instanceShutdown = [];
        this.state.selectedInstances.map((instanceData) => {
            let data = {
                "host": instanceData.hostName,
                "instanceID": instanceData.instanceID,
                "instanceName": instanceData.instanceName,
                "serverType": instanceData.serverType,
            }
            instanceShutdown.push(data);
        });
        const casemanagementInstanceShutdown = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['casemanagementInstanceShutdown']}`,
            data: { 'instanceShutdown': instanceShutdown }
        }
        const casemanagementStatus = await getService(casemanagementInstanceShutdown);
        if (casemanagementStatus && casemanagementStatus.status === 200) {
            this.props.activateSuccessList(true, casemanagementStatus.data);
            this.props.activateErrorList(false, '');
        } else {
            this.props.activateErrorList(true, casemanagementStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }

    }

    savecaseInstanceRefreshInfo = async () => {
        let refreshInstance = [];
        this.state.selectedInstances.map((instanceData) => {
            let data = {
                "allOrgsRefresh": instanceData.allOrgsSelected,
                "host": instanceData.hostName,
                "instanceID": instanceData.instanceID,
                "instanceName": instanceData.instanceName,
                "orgNames": instanceData.avlOrgs,
                "serverType": instanceData.serverType,
                "systemRefresh": instanceData.systemRefresh
            }
            refreshInstance.push(data);
        });
        const caseManagementInstanceRefresh = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['caseManagementInstanceRefresh']}`,
            data: { 'refreshInstance': refreshInstance }
        }
        const caseManagementInstanceStatus = await getService(caseManagementInstanceRefresh);
        if (caseManagementInstanceStatus && caseManagementInstanceStatus.status === 200) {
            this.props.activateSuccessList(true, caseManagementInstanceStatus.data);
            this.props.activateErrorList(false, '');
        } else {
            this.props.activateErrorList(true, caseManagementInstanceStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }
    render() {
        return (
            <div className="main">
                <h2 className="title">{RA_STR.instancemanagmenttitle}</h2>
                <p className="desc">{RA_STR.instancemanagmentheading}</p>
                <DynamicTable columns={this.state.columns} data={this.state.apiData} enablePagination={false} selection={true} handleCheckboxSelection={this.getcheckboxselection} />
                <input className="secondary-btn mt-5" id="Active" type="submit" value="REFRESH" onClick={this.saveRefreshInfo} />
                <input className="secondary-btn mt-5 ml-2" id="Active" type="submit" value="SHUT DOWN" onClick={this.saveShutdownInfo} />
                <h2 className="title mt-5">{RA_STR.casemanagmenttitle}</h2>
                <p className="desc">{RA_STR.casemanagmentheading}</p>
                <DynamicTable columns={this.state.casemanagmentcolumns} data={this.state.apiCaseManagmentData} enablePagination={false} selection={true} handleCheckboxSelection={this.getcheckboxselection} />
                <input className="secondary-btn mt-5" id="Active" type="submit" value="REFRESH" onClick={this.savecaseInstanceRefreshInfo} />
                <input className="secondary-btn mt-5 ml-2" id="Active" type="submit" value="SHUT DOWN" onClick={this.savecaseInstanceShutdownInfo} />

            </div>
        );

    }
}
export default InstanceManagement;