import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import { RA_API_URL, serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import './EditList.css';
import ErrorList from '../../shared/components/ErrorList/ErrorList';

class EditList extends Component {
    constructor(props) {
        super(props);
        this.editRuleList = {};
        this.state = {
            ruleList: [],
            selectedRulelist: '',
            editRuleList: {},
            errorMessage: [],
            successMessage: '',
            listTypes: ["General Purpose", "Blacklist", "Whitelist"],
            linkingmodechanged: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.editTheList = this.editTheList.bind(this);
    }
    componentWillUnmount() {
        if (typeof this.props.activateErrorList === 'function') {
            this.props.activateErrorList(false, '');
            this.props.activateSuccessList(false, '');
        }
    }
    handleChange(e) {
        if (e.target.name === 'selectedRulelist') {
            this.setState({ [e.target.name]: e.target.value }, () => {
                this.getTheSelectedRulelist();
            });
        }
        else {
            let { editRuleList, linkingmodechanged } = this.state;
            editRuleList[e.target.name] = e.target.value;
            if (e.target.name === 'associatePreDefined') {
                linkingmodechanged = true;
            }
            this.setState({ editRuleList, linkingmodechanged });
        }
    }
    async getRulelistOptions() {
        const listUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.GetListOfRules;
        const getRuleList = await getService({ method: 'GET', url: listUrl });
        if (getRuleList && getRuleList.status === 200) {
            let ruleList = [];
            if (getRuleList.data) {
                ruleList = getRuleList.data.map((data) => {
                    data.key = data.listRefId;
                    data.content = data.listName;
                    return data;
                })
            }
            this.setState({ ruleList });
        }
    }
    async editTheList() {
        const listUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.getOrgRules + '/' + this.selectedRuleList;
        const updateList = await getService({ method: 'PUT', url: listUrl, data: this.state.editRuleList });
        if (updateList && updateList.status === 400 && updateList.data) {
            this.props.activateSuccessList(false, '');
            this.props.activateErrorList(true, updateList.data.errorList);
            window.scrollTo(0, 0);
        } else if (updateList && updateList.status === 200) {
            this.props.activateSuccessList(true, updateList.data);
            this.props.activateErrorList(false, '');
            window.scrollTo(0, 0);
        }
    }

    async getTheSelectedRulelist() {
        if (this.state.selectedRulelist !== '') {
            this.selectedRuleList = this.state.selectedRulelist;
            const specifiedlistUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.getOrgRules + '/' + this.state.selectedRulelist + RA_API_URL.getRulelistDetails;
            const getRuleListDetails = await getService({ method: 'GET', url: specifiedlistUrl });
            if (getRuleListDetails && getRuleListDetails.status === 200 && getRuleListDetails.data) {
                if (getRuleListDetails.data.parentHierarchyListStubs) {
                    getRuleListDetails.data.parentHierarchyListStubs = getRuleListDetails.data.parentHierarchyListStubs.map((data) => {
                        return {
                            key: data.listName,
                            content: data.listName
                        };
                    })
                }
                this.editRuleList = JSON.parse(JSON.stringify(getRuleListDetails.data));
                this.setState({ editRuleList: getRuleListDetails.data, selectedRulelist: '' });
            } else {
                this.setState({ editRuleList: {}, selectedRulelist: '' });
            }
        } else {
            this.selectedRuleList = '';
            this.setState({ editRuleList: {}, selectedRulelist: '' });
        }
    }
    componentWillMount() {
        this.orgName = new URLSearchParams(window.location.search).get('orgname');
        this.getRulelistOptions();
    }
    render() {
        const { ruleList, selectedRulelist, editRuleList, listTypes, linkingmodechanged } = this.state;
        return <div className="main">
            <h2 className="title">Edit List</h2>
            <p className="desc">Edit an existing data list. Only specific properties of the list can be edited. For predefined lists (Negative Country List, Untrusted IP List, and Trusted IP/Aggregator List), you can edit the association with the parent list and the linking mode. For custom lists, you can edit the link to the parent list and the linking mode.</p>
            <div className="row">
                <div className="col-sm-5 mt-1 dynamic-form-select">
                    <Select
                        name={'selectedRulelist'}
                        title={''}
                        options={ruleList}
                        placeholder={'--Select List--'}
                        selectedOption={selectedRulelist}
                        controlFunc={this.handleChange} />
                </div>
            </div>
            {Object.keys(editRuleList).length > 0 && <div className="editList">
                {!editRuleList.preDefinedList && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">List Type</label>
                    <div className="col-sm-9 mt-1">
                        ListData
                    </div>
                </div>}
                <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">List Name</label>
                    <div className="col-sm-9 mt-1">
                        {editRuleList.listName}
                    </div>
                </div>
                {this.editRuleList.hierarchyOrg && this.editRuleList.rulesetExecMode !== 0 && this.editRuleList.configName === "@SYSTEM@" && !this.editRuleList.preDefinedList && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">Link to Parent Organization's List</label>
                    <div className="col-sm-5">
                        <Select
                            name={"hierarchicalListName"}
                            title={""}
                            options={editRuleList.parentHierarchyListStubs || []}
                            placeholder={'--select--'}
                            selectedOption={editRuleList.hierarchicalListName || ''}
                            controlFunc={this.handleChange} />
                    </div>
                </div>}
                {this.editRuleList.hierarchyOrg && this.editRuleList.preDefinedList && <div className="row">
                    <label className="col-sm-3">Associate This list with Parent's</label>
                    <div className="col-sm-9 form-inline pl-0">
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="associatelinkingself"
                                name="associatePreDefined"
                                checked={editRuleList.associatePreDefined === 'Y'}
                                onChange={this.handleChange}
                                value='Y' />
                            <label className="custom-control-label" htmlFor="associatelinkingself">Yes</label>
                        </div>
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="associatelinkingparent"
                                name="associatePreDefined"
                                value='N'
                                checked={editRuleList.associatePreDefined === 'N'}
                                onChange={this.handleChange} />
                            <label className="custom-control-label" htmlFor="associatelinkingparent">No</label>
                        </div>
                    </div>
                </div>}
                {((editRuleList.associatePreDefined === 'Y')) && <div className="row">
                    <label className="col-sm-3">Linking Mode</label>
                    <div className="col-sm-9 form-inline pl-0">
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="linkingself"
                                name="priorityMode"
                                checked={editRuleList.priorityMode === 'O' || editRuleList.priorityMode === null}
                                onChange={this.handleChange}
                                value='O' />
                            <label className="custom-control-label" htmlFor="linkingself">Override</label>
                        </div>
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="linkingparent"
                                name="priorityMode"
                                value='A'
                                checked={editRuleList.priorityMode === 'A'}
                                onChange={this.handleChange} />
                            <label className="custom-control-label" htmlFor="linkingparent">Append</label>
                        </div>
                    </div>
                </div>}
                {!editRuleList.preDefinedList && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">Scope</label>
                    <div className="col-sm-9 mt-1">
                        {editRuleList.configName === '@SYSTEM@' ? 'ORG' : 'RULESET - ' + editRuleList.configName}
                    </div>
                </div>}
                <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">Element Name</label>
                    <div className="col-sm-9 mt-1">
                        {editRuleList.elementName == '@UNSPECIFIED@' ? '(Unspecified)' : editRuleList.elementName}
                    </div>
                </div>
                {!editRuleList.preDefinedList && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">List Usage</label>
                    <div className="col-sm-9 mt-1">
                        {listTypes[editRuleList.listUsage] ? listTypes[editRuleList.listUsage] : ''}
                    </div>
                </div>}
                {/* <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">HotList</label>
                    <div className="col-sm-9 mt-1">
                        {editRuleList.extListStatus ? 'Yes' : 'No'}
                    </div>
                </div> */}
                {!this.editRuleList.preDefinedList && this.editRuleList.extListStatus === 1 && this.editRuleList.enableListEdit && < div className="row pl-0">
                    <label className="col-sm-3 col-form-label pl-3">Hot List</label>
                    <div className="col-sm-9 form-inline pl-0">
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="hotself"
                                name="extListStatus"
                                checked={editRuleList.extListStatus == "1"}
                                onChange={this.handleChange}
                                value='1'
                            />
                            <label className="custom-control-label" htmlFor="hotself">Yes</label>
                        </div>
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="hotparent"
                                name="extListStatus"
                                value='0'
                                checked={editRuleList.extListStatus == "0"}
                                onChange={this.handleChange}
                            />
                            <label className="custom-control-label" htmlFor="hotparent">No</label>
                        </div>
                    </div>
                </div>}
                {!this.editRuleList.preDefinedList && !(this.editRuleList.extListStatus === 1 && this.editRuleList.enableListEdit) && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">Hot List</label>
                    <div className="col-sm-9 mt-1">
                        {editRuleList.extListStatus == "1" ? 'Yes' : 'No'}
                    </div>
                </div>}
                {!this.editRuleList.preDefinedList && !(this.editRuleList.extListStatus === 1 && this.editRuleList.enableListEdit) && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">Preferred List</label>
                    <div className="col-sm-9 mt-1">
                        {editRuleList.preferredStatus == "1" ? 'Yes' : 'No'}
                    </div>
                </div>}
                {!this.editRuleList.preDefinedList && this.editRuleList.extListStatus === 1 && this.editRuleList.enableListEdit && <div className="row pl-0">
                    <label className="col-sm-3 col-form-label pl-3">Preferred List</label>
                    <div className="col-sm-9 form-inline pl-0">
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="prefself"
                                name="preferredStatus"
                                checked={editRuleList.preferredStatus == "1"}
                                onChange={this.handleChange}
                                value='1' />
                            <label className="custom-control-label" htmlFor="prefself">Yes</label>
                        </div>
                        <div className="custom-control custom-radio radioTopMargin validityPeriod">
                            <input type="radio" className="custom-control-input "
                                id="prefparent"
                                name="preferredStatus"
                                value='0'
                                checked={editRuleList.preferredStatus == "0"}
                                onChange={this.handleChange}
                            />
                            <label className="custom-control-label" htmlFor="prefparent">No</label>
                        </div>
                    </div>
                </div>}
                {!editRuleList.preDefinedList && !editRuleList.hierarchyOrg && editRuleList.orgSharingInfoList && editRuleList.orgSharingInfoList.length > 0 && <div className="row">
                    <label className="col-sm-3 col-form-label pl-3">Sharing & Privileges</label>
                    <div className="col-sm-9 mt-1">
                        <table className="edl_table">
                            <thead>
                                <tr>
                                    <th>Organization</th>
                                    <th>Permissions</th>
                                </tr>
                            </thead>
                            <tbody id="sharedOrgTable">
                                {editRuleList.orgSharingInfoList && editRuleList.orgSharingInfoList.map((org, index) =>
                                    <tr key={index}>
                                        <td>{org.sharedOrgName}</td>
                                        <td>{org.permission}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>}
                {((editRuleList.enableListEdit && editRuleList.extListStatus == 1) || (editRuleList.hierarchyOrg && editRuleList.rulesetExecMode != 0 && editRuleList.configName === "@SYSTEM@") &&
                    (editRuleList.enableListEdit && editRuleList.extListStatus == 0)) && <div className="form-group form-submit-button">
                        <input className="secondary-btn" id="createButton" type="submit" value="SAVE" onClick={this.editTheList}></input>
                    </div>}
            </div>
            }
        </div>
    }
}

export default EditList;