import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, serverUrl } from '../../shared/utlities/constants';
import ReactDOM from 'react-dom';
import DatePicker from "react-datepicker";
var moment = require('moment');
class ManageTables extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: moment().subtract(1, 'months').add(1, 'days').toDate(),
            endDate: new Date(),
            toDate: new Date(),
            columns: [
                {
                    title: 'check', field: 'transactionId', render: rowData =>
                        <input type="checkbox" name="te" className="ml-2 mr-2" style={{ zoom: '1.5' }} onChange={(e) => this.getcheckboxselection(rowData, e, RA_STR.transactionList)} checked={rowData.checked} />
                },
                { title: 'No.', field: 'index' },
                {
                    title: 'Mark Selected As', field: 'index', render: rowData => this.setFraudSelection(rowData, RA_STR.transactionList)
                },
                {
                    title: 'Fraud Type', field: 'index', render: rowData => this.setFraudTypes(rowData, RA_STR.transactionList)
                },
            ],
            userTranscolumns: [
                {
                    title: 'check', field: 'transactionId', render: rowData =>
                        <input type="checkbox" name="te" className="ml-2 mr-2" style={{ zoom: '1.5' }} onChange={(e) => this.getcheckboxselection(rowData, e, RA_STR.userTransactionList)} checked={rowData.checked} />
                },
                { title: 'No.', field: 'index' },
                {
                    title: 'Mark Selected As', field: 'index', render: rowData => this.setFraudSelection(rowData, RA_STR.userTransactionList)
                },
                {
                    title: 'Fraud Type', field: 'index', render: rowData => this.setFraudTypes(rowData, RA_STR.userTransactionList)
                },
            ],
            transactionList: [],
            userTransactionList: [],
            detailLayouts: [],
            detailExtElements: [],
            selectedTxnDetails: {},
            showRelatedExtElements: [],
            caseDataType: 0,
            lastCaseOptions: [30, 60, 120, 180],
            lastTxnTime: '30',
            selectedTab: RA_STR.threeDSecure,
            hideUseTransactions: false
        }
    }

    showTransactionSummary = (rowData) => {
        this.props.accessParent.setTransactionPage(rowData.txnID);
    }
    getUpdatedData() {
        let { manageInboundData, fraudStatusList } = this.props.accessParent.state;
        var tempList = {
            userTxns: 'userTransactionList',
            caseTxns: 'transactionList'
        }
        var transactionDone = ['userTxns', 'caseTxns'];
        let inputDataTxs = { userTxns: [], caseTxns: [] };
        transactionDone.forEach(arrayName => {
            let updatedlist = this.state[tempList[arrayName]];
            let dataObj = {};
            manageInboundData[arrayName].map((obj) => {
                dataObj = { txnID: obj.txnID, selected: true, alertStatus: 1, channelName: obj.channel };
                obj.summaryExtElements.forEach(insideObj => {
                    if (insideObj.elementName == "FRAUDTYPE") {
                        let filteredObj = updatedlist.filter(obj => {
                            if (obj[insideObj.elementName] && obj.checked) {
                                return true;
                            }
                        })[0];
                        if (filteredObj) {
                            insideObj.value = filteredObj.FRAUDTYPE;
                            dataObj.fraudType = parseInt(filteredObj.FRAUDTYPE);
                        } else {
                            dataObj.fraudType = '';
                        }
                    }
                    if (insideObj.elementName == "FRAUDSTATUS") {
                        let filteredObj = updatedlist.filter(obj => {
                            if (obj[insideObj.elementName] && obj.checked) {
                                return true;
                            }
                        })[0];
                        if (filteredObj) {
                            let fraudObj = fraudStatusList.filter(obj => obj.key == filteredObj.FRAUDSTATUS)[0];
                            if (fraudObj) {
                                insideObj.value = fraudObj.content;
                                dataObj.fraudStatus = parseInt(filteredObj.FRAUDSTATUS);
                                dataObj.selected = true;
                            }
                        } else {
                            dataObj.fraudStatus = '';
                        }
                    }
                });
                inputDataTxs[arrayName].push(dataObj);
            });

        })
        // let { userTxns, caseTxns } = manageInboundData;
        // return { userTxns, caseTxns };
        return inputDataTxs;
    }
    setCaseTransactions = () => {
        let { manageInboundData } = this.props.accessParent.state;
        let { userTranscolumns } = this.state;
        let { columns } = this.state;
        let dataTrans = [], userTransactionList = [];
        manageInboundData.caseTxns.map((obj, index) => {
            let data = {
                index: index + 1,
                checked: false,
                fraudStatusDisabled: true,
                fraudTypeDisabled: true,
                txnID: obj.txnID
            }
            obj.summaryExtElements.forEach(insideObj => {
                if (insideObj.elementName !== RA_STR.FRAUDSTATUS && insideObj.elementName !== RA_STR.FRAUDTYPE) {
                    if (insideObj.elementName !== 'SESSIONID') {
                        columns.push({ title: insideObj.elementName, field: insideObj.elementName })
                    } else {
                        columns.push({
                            title: 'SESSIONID', field: 'SESSIONID', render: rowData => <a className="orgName" onClick={() => this.showTransactionSummary(rowData)} title={rowData.SESSIONID}>{rowData.SESSIONID}</a>
                        })
                    }
                    data[insideObj.elementName] = insideObj.value;
                }
            })

            dataTrans.push({ ...data });
        });
        manageInboundData.userTxns.map((obj, index) => {
            let data = {
                index: index + 1,
                checked: false,
                fraudStatusDisabled: true,
                fraudTypeDisabled: true,
                txnID: obj.txnID,
                fraudStatus: obj.fraudStatus,
                fraudType: obj.fraudType,
                FRAUDTYPE: obj.fraudType,
                FRAUDSTATUS: obj.fraudStatus
            }
            obj.summaryExtElements.forEach(insideObj => {
                if (insideObj.elementName !== RA_STR.FRAUDSTATUS && insideObj.elementName !== RA_STR.FRAUDTYPE) {
                    if (insideObj.elementName !== 'SESSIONID') {
                        userTranscolumns.push({ title: insideObj.elementName, field: insideObj.elementName })
                    } else {
                        userTranscolumns.push({
                            title: 'SESSIONID', field: 'SESSIONID', render: rowData => <a className="orgName" title={rowData.SESSIONID} onClick={() => this.showTransactionSummary(rowData)} >{rowData.SESSIONID}</a>
                        })
                    }
                    data[insideObj.elementName] = insideObj.value;
                }
            })
            userTransactionList.push({ ...data });
        });
        // let temp = JSON.parse(JSON.stringify(dataTrans));
        // temp[0].index = 2;
        // dataTrans = [...dataTrans, ...temp] //userTranscolumns
        this.setState({ transactionList: dataTrans, columns, userTranscolumns, userTransactionList: userTransactionList })
    }
    handleThisChange = (e, rowData, arrayName) => {
        let updatedArray = this.state[arrayName];
        let { value, name } = e.target;
        updatedArray.forEach(inobj => {
            if (inobj.index == rowData.index) {
                inobj[name] = value;
                if (inobj.FRAUDSTATUS == '1') {
                    inobj.fraudTypeDisabled = false;
                } else {
                    inobj.fraudTypeDisabled = true;
                    inobj.FRAUDTYPE = '0';
                }
            }
        })
        this.setState({ [arrayName]: updatedArray });
    }
    setFraudSelection = (rowData, arrayName) => {
        let { fraudStatusList } = this.props.accessParent.state;
        return <div className="mt-2"> <Select
            name='FRAUDSTATUS'
            options={fraudStatusList || []}
            disabled={rowData.fraudStatusDisabled}
            selectedOption={rowData.FRAUDSTATUS || ''}
            controlFunc={(e) => this.handleThisChange(e, rowData, arrayName)}
        /></div>
    }
    setFraudTypes = (rowData, arrayName) => {
        let { fraudTypeList } = this.props.accessParent.state;
        return <div className="mt-2"> <Select
            name='FRAUDTYPE'
            disabled={rowData.fraudTypeDisabled}
            selectedOption={rowData.FRAUDTYPE || ''}
            options={fraudTypeList || []}
            controlFunc={(e) => this.handleThisChange(e, rowData, arrayName)}
        /></div>
    }
    changeHeaderCheckbox(checked, arrayName) {
        const currentDom = ReactDOM.findDOMNode(this);
        let selectorClass;
        if (arrayName === RA_STR.transactionList) {
            selectorClass = '.checkALL';
        } else {
            selectorClass = '.usercheckALL';
        }
        if (currentDom) {
            if (currentDom.querySelector(selectorClass)) {
                currentDom.querySelector(selectorClass).checked = checked;
            }
        }
    }
    getcheckboxselection = (data, event, arrayName) => {
        let allCheckBoxesChecked = true;
        let { checked } = event.target;
        let updatedArray = this.state[arrayName];
        if (data) {
            updatedArray.forEach(innObj => {
                if (data.index == innObj.index && checked) {
                    innObj.checked = true;
                    innObj.fraudStatusDisabled = false;
                } else if (data.index == innObj.index && !checked) {
                    innObj.checked = false;
                    innObj.fraudStatusDisabled = true;
                    innObj.fraudTypeDisabled = true;
                }
                if (innObj.FRAUDSTATUS == '1' && checked) {
                    innObj.fraudTypeDisabled = false;
                } else {
                    innObj.fraudTypeDisabled = true;
                    innObj.FRAUDTYPE = '0';
                }
                if (!innObj.checked) {
                    allCheckBoxesChecked = innObj.checked;
                }
            });
        }
        this.changeHeaderCheckbox(allCheckBoxesChecked, arrayName);
        this.setState({ [arrayName]: updatedArray });
    }

    componentWillUnmount() { }

    checkAll = (e, arrayName) => {
        let { checked, className } = e.target;
        if (className) {
            if (className.indexOf('usercheckALL') > -1) {
                arrayName = RA_STR.userTransactionList;
            } else {
                arrayName = RA_STR.transactionList;
            }
        }
        let updatedArray = this.state[arrayName];
        updatedArray.forEach(obj => {
            obj.checked = checked;
            obj.fraudStatusDisabled = !checked;
            if (obj.checked) {
                if (obj.FRAUDSTATUS == '1') {
                    obj.fraudTypeDisabled = false;
                } else {
                    obj.fraudTypeDisabled = true;
                    obj.FRAUDTYPE = '0';
                }
            } else {
                obj.fraudTypeDisabled = true;
            }
        })
        this.setState({ [arrayName]: updatedArray });
    }

    hideUserEventList = (e) => {
        let { innerHTML } = e.target;
        const currentDom = ReactDOM.findDOMNode(this);
        let { userTransactionList } = this.state;
        let temp = userTransactionList.filter(obj => obj.checked === true);
        if ((currentDom && temp && temp.length > 0) || innerHTML) {
            let eventListNode = currentDom.querySelector('.userTransheaderImgSelect');
            if (eventListNode && eventListNode.style.display === RA_STR.NONE) {
                eventListNode.style.display = RA_STR.BLOCK;
            } else if (eventListNode) {
                eventListNode.style.display = RA_STR.NONE;
            }
        }
        if (innerHTML) {
            this.markSelectChange(innerHTML, true, RA_STR.userTransactionList);
        }
    }
    hideEventList = (e) => {
        let { innerHTML } = e.target;
        const currentDom = ReactDOM.findDOMNode(this);
        let { transactionList } = this.state;
        let temp = transactionList.filter(obj => obj.checked === true);
        if ((currentDom && temp && temp.length > 0) || innerHTML) {
            let eventListNode = currentDom.querySelector('.headerImgSelect');
            if (eventListNode && eventListNode.style.display === RA_STR.NONE) {
                eventListNode.style.display = RA_STR.BLOCK;
            } else if (eventListNode) {
                eventListNode.style.display = RA_STR.NONE;
            }
        }
        if (innerHTML) {
            this.markSelectChange(innerHTML, true, RA_STR.transactionList);
        }
    }
    async setEventPopupData() {
        let { fraudStatusList } = this.props.accessParent.state;
        if (fraudStatusList) {
            var field = '<div class="headerImgSelect" style="display:none">'
            fraudStatusList.forEach((eachType) => {
                field += `<a href='javascript:void(0)' value='${eachType.content}'>${eachType.content}</a>`;
            })
            field += '</div></div> '
            this.field = field;
        }
    }
    markSelectChange = (value, type, arrayName) => {
        let updatedArray = this.state[arrayName];
        let { fraudStatusList } = this.props.accessParent.state;
        let selectedObj = {};
        if (type) {
            let obj = fraudStatusList.filter(obj => obj.content == value)[0];
            if (obj) {
                selectedObj = obj;
            }
        }
        updatedArray.forEach(innObj => {
            if (innObj.checked) {
                if (selectedObj.key) {
                    innObj.FRAUDSTATUS = selectedObj.key
                } else {
                    innObj.FRAUDSTATUS = value;
                }
                if (innObj.FRAUDSTATUS == '1') {
                    innObj.FRAUDSTATUS = '1';
                    innObj.fraudTypeDisabled = false;
                } else {
                    innObj.fraudTypeDisabled = true;
                    innObj.FRAUDTYPE = '0';
                }
            }
        });
        this.setState({ [arrayName]: updatedArray });
    }
    setEventDropdownIntable() {
        const currentDom = ReactDOM.findDOMNode(this);
        let { manageInboundData } = this.props.accessParent.state;

        if (currentDom) {
            if (manageInboundData.caseTxns && manageInboundData.caseTxns.length > 0) {
                let getTableNode = currentDom.querySelectorAll('.alertTable th.MuiTableCell-root');
                if (getTableNode && getTableNode[0] && getTableNode[2]) {
                    getTableNode[0].innerHTML = '<input class="checkALL ml-2 zoomCheckbox" type="checkbox" name="te"/>';
                    getTableNode[2].innerHTML = '<div class="ml-2">Mark Selected As <img src="images/dropdown.png" class="clickDownArrow" />' + this.field;
                }
                currentDom.querySelector(".checkALL") && currentDom.querySelector(".checkALL").addEventListener("change", this.checkAll);
                currentDom.querySelector(".headerImgSelect") && currentDom.querySelector(".headerImgSelect").addEventListener("click", this.hideEventList);
                currentDom.querySelector(".clickDownArrow") && currentDom.querySelector(".clickDownArrow").addEventListener("click", this.hideEventList);
            }
            if (manageInboundData.userTxns && manageInboundData.userTxns.length > 0) {
                let getTableSecNode = currentDom.querySelectorAll('.fraudTable th.MuiTableCell-root');
                if (getTableSecNode && getTableSecNode[0] && getTableSecNode[2]) {
                    getTableSecNode[0].innerHTML = '<input class="usercheckALL ml-2 zoomCheckbox" type="checkbox" name="te" />';
                    getTableSecNode[2].innerHTML = 'Mark Selected As <img src="images/dropdown.png" class="UserTransclickDownArrow" />' + this.field.replace('headerImgSelect', 'userTransheaderImgSelect');
                    currentDom.querySelector(".usercheckALL") && currentDom.querySelector(".usercheckALL").addEventListener("change", this.checkAll);
                    currentDom.querySelector(".userTransheaderImgSelect") && currentDom.querySelector(".userTransheaderImgSelect").addEventListener("click", this.hideUserEventList);
                    currentDom.querySelector(".UserTransclickDownArrow") && currentDom.querySelector(".UserTransclickDownArrow").addEventListener("click", this.hideUserEventList);
                }
            }
        }
    }
    showTransactions = async () => {
        let { orgName, caseID, userName } = this.props.accessParent.state.manageInboundData;
        let { startDate, endDate } = this.state;
        let userTransactionList = [];
        startDate = moment(startDate).format("MM/DD/YYYY");
        endDate = moment(endDate).format("MM/DD/YYYY");
        let data = {
            orgName,
            userName,
            "workFlow": 4,
            "fromDate": startDate,
            "toDate": endDate,
            "fetchAlerts": false,
            caseID
        }
        const checkOrg = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL.getChannelTransactions}`,
            data
        };
        const response = await getService(checkOrg);
        if (response && response.status === 200 && response.data.length > 0) {
            let selectedData = response.data;
            selectedData.map((obj, index) => {
                let data = {
                    index: index + 1,
                    checked: false,
                    fraudStatusDisabled: true,
                    fraudTypeDisabled: true
                }
                obj.summaryExtElements.forEach(insideObj => {
                    if (insideObj.elementName !== RA_STR.FRAUDSTATUS && insideObj.elementName !== RA_STR.FRAUDTYPE) {
                        data[insideObj.elementName] = insideObj.value;
                    }
                })
                userTransactionList.push({ ...data });
            });
            this.setState({ userTransactionList, hideUseTransactions: false });
        }
    }
    componentDidMount() {
        this.setEventPopupData();
        this.setEventDropdownIntable();
        this.setCaseTransactions();
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    openCalendar = (calendarType) => {
        const currentDom = ReactDOM.findDOMNode(this);
        if (calendarType && currentDom && currentDom.querySelector(calendarType)) {
            currentDom.querySelector(calendarType).click();
        }
    }
    startDateChange = (date) => {
        this.setState({ startDate: date });
    }
    endDateChange = (date) => {
        this.setState({ endDate: date });
    }
    hideTransactions = () => {
        this.setState({ hideUseTransactions: true });
    }
    render() {
        let { columns, userTranscolumns, transactionList, startDate, endDate, userTransactionList, hideUseTransactions } = this.state;
        let { manageInboundData } = this.props.accessParent.state;
        return <div>
            {manageInboundData.caseTxns && manageInboundData.caseTxns.length > 0 ?
                <>
                    <label className="col-form-label mt-2">{RA_STR.alertsOncase}</label>
                    <div className="table-x alertTable">
                        <DynamicTable columns={columns} data={transactionList} enablePagination={true} pageSize={10} totalCount={3} selection={false} activePage={this.getActivePage} handleCheckboxSelection={this.getcheckboxselection} activePageNumNew={1} />
                    </div>
                </>
                :
                <p className="mt-4 mb-4 noAlerts">{RA_STR.noAlerts}</p>
            }
            <div className="row mt-3">
                <div className="col-sm-2">
                    <label className="col-form-label">{RA_STR.dateRange}</label>
                </div>
                <div className="col-sm-5 changeInput">
                    <label className="col-form-label">{RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL}</label>
                    <DatePicker
                        selected={startDate}
                        todayButton="today"
                        onChange={this.startDateChange}
                        showMonthDropdown={true}
                        showYearDropdown={true}
                        scrollableYearDropdown={false}
                        dropdownMode="select"
                        className="form-control startDate"
                    /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind" onClick={() => this.openCalendar('.startDate')} />
                    <label className="col-form-label ml-2">{RA_STR.EXCEPTION_USER_REPORT.TO_LABEL}</label>
                    <DatePicker
                        selected={endDate}
                        todayButton="today"
                        onChange={this.endDateChange}
                        showMonthDropdown={true}
                        showYearDropdown={true}
                        scrollableYearDropdown={false}
                        dropdownMode="select"
                        className="form-control endDate"
                    /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind" onClick={() => this.openCalendar('.endDate')} />
                </div>
                <div className="col-md-5">
                    <input className="grey-button" id="searchButton" type="submit" value="SHOW TRANSACTIONS" onClick={this.showTransactions}></input>
                    <input className="grey-button ml-4" id="searchButton" type="submit" value="HIDE TRANSACTIONS" onClick={this.hideTransactions}></input>
                </div>
            </div>
            {manageInboundData.userTxns && manageInboundData.userTxns.length > 0 &&
                <div className={`table-x fraudTable ${hideUseTransactions ? RA_STR.hide : RA_STR.show}`}>
                    <DynamicTable columns={userTranscolumns} data={userTransactionList} enablePagination={true} pageSize={10} totalCount={3} selection={false} activePage={this.getActivePage} handleCheckboxSelection={this.getcheckboxselection} activePageNumNew={1} />
                </div>
            }
        </div >
    }
};
export default ManageTables;