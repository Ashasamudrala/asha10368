import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import ReactDOM from "react-dom";
var moment = require('moment');

class DatePickerWithTimer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fromDate: new Date(this.props.fromDate ? this.props.fromDate : '00'),
            toDate: new Date(this.props.toDate ? this.props.toDate : '00'),
            fromMinutes: this.props.fromMinutes ? this.props.fromMinutes : '00' ,
            fromHours: this.props.fromHours ? this.props.fromHours : '00',
            toHours: '00',
            toMinutes: '00'
        }
    }
    componentDidMount() {
        if (this.props.prevstateObj) {
            this.setState(this.props.prevstateObj)
        }
    }
    openCalendar = (calendarType) => {
        const currentDom = ReactDOM.findDOMNode(this);
        if (calendarType && currentDom && currentDom.querySelector(calendarType)) {
            currentDom.querySelector(calendarType).click();
        }
    }
    startDateChange = (date) => {
        this.setState({
            fromDate: date
        });
    }
    endDateChange = (date) => {
        this.setState({
            toDate: date
        });
    }
    handleSelectChanges = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    getDateRanges() {
        let obj = { ...this.state };
        obj.fromHours = obj.fromHours !== 'HRS' ? obj.fromHours : '00';
        obj.fromMinutes = obj.fromMinutes !== 'Mins' ? obj.fromMinutes : '00';
        obj.toHours = obj.toHours !== 'HRS' ? obj.toHours : '00';
        obj.toMinutes = obj.toMinutes !== 'Mins' ? obj.toMinutes : '00';
        obj.fromDate = moment(obj.fromDate).format('L') + ' ' + obj.fromHours + ':' + obj.fromMinutes + ':' + '00';
        obj.toDate = moment(obj.toDate).format('L') + ' ' + obj.toHours + ':' + obj.toMinutes + ':' + '00';

        let analyzedata = {
            transactionFromDate: moment(this.state.fromDate).format('L'),
            transactionToDate: moment(this.state.toDate).format('L'),
            transactionFromHrs: obj.fromHours !== '00' ? obj.fromHours : null,
            transactionFromMins: obj.fromMinutes !== '00' ? obj.fromMinutes : null,
            transactionFromSecs: obj.fromHours !== '00' ? obj.fromHours : null,
            transactionToHrs: obj.toHours !== '00' ? obj.toHours : null,
            transactionToMins: obj.toMinutes !== '00' ? obj.toMinutes : null
        }

        return { data: obj, saveDateObj: this.state, analyzedata };
    }
    setHrs(length) {
        let data;
        if (this.props.hideDateField !== true) {
            data = "<option value='HRS'>HRS</option>"
        }
        for (let j = 0; j < length; j++) {
            if (j < 10) {
                data += `<option value='0${j}'>0${j}</option>`;
            } else {
                data += `<option value='${j}'>${j}</option>`;
            }
        }
        return data;
    }

    setMin(length) {
        let data;
        if (this.props.hideDateField !== true) {
            data = "<option value='Mins'>Mins</option>"
        }
        for (let j = 0; j <= length; j++) {
            if (j < 2) {
                data += `<option value='0${j * 5}'>0${j * 5}</option>`;
            } else {
                data += `<option value='${j * 5}'>${j * 5}</option>`;
            }
        }
        return data;
    }
    render() {
        let { fromDate, toDate , fromMinutes , fromHours} = this.state;
        let hours = this.setHrs(24);
        let minutes = this.setMin(11);
        return (<div className={"col-sm-9 ml-3"}>
            {
                this.props.hideDateField ? <React.Fragment /> : <label className="col-form-label">From </label>
            }
            <DatePicker
                selected={fromDate}
                todayButton="today"
                onChange={this.startDateChange}
                showMonthDropdown={true}
                showYearDropdown={true}
                scrollableYearDropdown={false}
                dropdownMode="select"
                className="form-control startDate"
                disabled={this.props.disableAll}
                minDate={moment(fromDate).add(-2, 'year').startOf('year').toDate()}
                maxDate={moment(fromDate).add(+2, 'year').startOf('year').toDate()}
                onYearChange={this.startDateChange}
            /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind" onClick={() => this.openCalendar('.startDate')} />
            {
                this.props.hideDateRangeField ? '' : <select name="fromHours" className="small" dangerouslySetInnerHTML={{ __html: hours }} onChange={this.handleSelectChanges} disabled={this.props.disableAll} value = {fromHours} ></select>
            }
            {
                this.props.hideDateField ? 'Hrs' : <React.Fragment />
            }
            {
                this.props.hideDateRangeField ? '' : <select name="fromMinutes" className="small" dangerouslySetInnerHTML={{ __html: minutes }} onChange={this.handleSelectChanges} disabled={this.props.disableAll} value = {fromMinutes}></select>
            }
            {
                this.props.hideDateField ? 'Mins' : <React.Fragment />
            }
            {
                this.props.hideDateField ? <React.Fragment /> :
                    <React.Fragment>
                        <label className="col-form-label ml-2">To </label>
                        <DatePicker
                            selected={toDate}
                            todayButton="today"
                            onChange={this.endDateChange}
                            showMonthDropdown={true}
                            showYearDropdown={true}
                            scrollableYearDropdown={false}
                            dropdownMode="select"
                            className="form-control endDate"
                            disabled={this.props.disableAll}
                            minDate={moment(toDate).add(-2, 'year').startOf('year').toDate()}
                            maxDate={moment(toDate).add(+2, 'year').startOf('year').toDate()}
                            onYearChange={this.endDateChange}
                        /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind" onClick={() => this.openCalendar('.endDate')} />
                        {
                            this.props.hideDateRangeField ? '' : <select name="toHours" className="small" dangerouslySetInnerHTML={{ __html: hours }} onChange={this.handleSelectChanges} disabled={this.props.disableAll}></select>
                        }
                        {
                            this.props.hideDateRangeField ? '' : <select name="toMinutes" className="small" dangerouslySetInnerHTML={{ __html: minutes }} onChange={this.handleSelectChanges} disabled={this.props.disableAll}></select>
                        }
                    </React.Fragment>
            }
        </div>
        )
    }
};
export default DatePickerWithTimer;