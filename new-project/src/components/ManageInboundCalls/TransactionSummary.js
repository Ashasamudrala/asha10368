import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_API_STATUS, LOADING_TEXT } from '../../shared/utlities/constants';
import _ from 'underscore';
import './ManageInboundCalls.scss';
import Loader from '../../shared/utlities/loader';

class TransactionSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            detailLayouts: [],
            detailExtElements: [],
            selectedTxnDetails: {},
            showRelatedExtElements: [],
            loader: true
        }
    }

    componentDidMount = async () => {
        this.getSummary();
    }

    getSummary = async () => {
        let { txnID } = this.props.accessParent.state;
        let params = {
            "orgName": "TESTORGFORDEVICELESS",
            "channel": "3DSECURE",
            txnID
        }
        const showTransc = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['transactionsummary']}`,
            data: params
        };
        const showTranscStatus = await getService(showTransc);
        if (showTranscStatus && showTranscStatus.status === RA_API_STATUS['200']) {
            this.setState({
                detailLayouts: showTranscStatus.data.channelBean.detailLayouts,
                detailExtElements: showTranscStatus.data.selectedTxnDetails.detailExtElements,
                selectedTxnDetails: showTranscStatus.data.selectedTxnDetails,
                showRelatedExtElements: showTranscStatus.data.selectedTxnDetails.showRelatedExtElements,
                loader: false
            })
        }
    }

    setBoxRows = (layoutData) => {
        let { detailExtElements } = this.state;

        if (detailExtElements) {
            detailExtElements = detailExtElements.filter(extElement => {
                if (extElement.categoryID == layoutData.boxID && extElement) {
                    return extElement;
                };
            })
        }
        return <>
            <div className="col-sm-5 details-card no-padd">

                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {(index % 2 === 0) && extElement.categoryID == layoutData.boxID &&
                            <div>
                                <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label>
                                <label className="col-sm-5 col-form-label pl-0 break-all">{extElement.value}</label>
                            </div>
                        }
                    </>
                )}
            </div>
            <div className="col-sm-5 ml-2 details-card no-padd">
                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {index % 2 === 1 && extElement.categoryID == layoutData.boxID &&
                            <div>  <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label> {extElement.value} </div>
                        }
                    </>
                )}
            </div>
        </>
    }
    setAdditionalrows = (layoutData) => {
        let { detailExtElements, selectedTxnDetails } = this.state;

        if (detailExtElements) {
            detailExtElements = detailExtElements.filter(extElement => {
                if (extElement.categoryID == layoutData.boxID && extElement && (extElement.isStoredExtElement == '1' && extElement.isExtensible == '1') || (extElement.isStoredExtElement == '2' && extElement.isExtensible == '1')) {
                    return extElement;
                };
            });
        }
        return <>
            <div className="col-sm-5 details-card no-padd">

                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {(index % 2 === 0) &&
                            <div>  <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label> {selectedTxnDetails.blobExtensibleElementsMap[extElement.elementName]} </div>
                        }
                    </>
                )}
            </div>
            <div className="col-sm-5 ml-2 details-card no-padd">
                {detailExtElements && detailExtElements.map((extElement, index) =>
                    <>
                        {index % 2 === 1 &&
                            <div>  <label className="col-sm-7 col-form-label">{extElement.displayNameStr}</label> {selectedTxnDetails.blobExtensibleElementsMap[extElement.elementName]}</div>
                        }
                    </>
                )}
            </div>
        </>
    }

    setRiskruleSetRows = () => {
        let { selectedTxnDetails } = this.state;
        return <>
            <div className="col-sm-5 details-card  no-padd">
                <div>
                    <label className="col-sm-9 col-form-label">
                        {RA_STR.mfpMatch}
                    </label> {selectedTxnDetails.mfpMatchPercentageString}
                </div>
                {selectedTxnDetails && selectedTxnDetails.addOnRuleResultMap && Object.keys(selectedTxnDetails.addOnRuleResultMap).map((extElement, index) =>
                    <>
                        {(index % 2 === 1) &&
                            <div>  <label className="col-sm-9 col-form-label">{extElement}</label> {selectedTxnDetails.addOnRuleResultMap[extElement]} </div>
                        }
                    </>
                )}
            </div>
            <div className="col-sm-5 ml-2 details-card no-padd">
                {selectedTxnDetails && selectedTxnDetails.addOnRuleResultMap && Object.keys(selectedTxnDetails.addOnRuleResultMap).map((extElement, index) =>
                    <>
                        {index % 2 === 0 &&
                            <div>  <label className="col-sm-9 col-form-label">{extElement}</label> {selectedTxnDetails.addOnRuleResultMap[extElement]}</div>
                        }
                    </>
                )}
            </div>
        </>
    }

    handleBack = () => {
        this.props.accessParent.setState({ showTransactionPage: false });
    }

    render() {
        const { detailLayouts, selectedTxnDetails, showRelatedExtElements, loader } = this.state;
        return (
            <div className="Transac-inbound">
                {loader ? <><Loader show='true' />{LOADING_TEXT}</> :
                    <>
                        <h2 className="title">{RA_STR.transactionTitle}</h2>
                        <p className="desc">{RA_STR.transactionDesc}</p>
                        {detailLayouts && detailLayouts.length > 0 &&
                            <div className="detailsSummary ml-3 mt-4">
                                {detailLayouts && detailLayouts.map(layoutData =>
                                    <>
                                        {layoutData.box != RA_STR.RISKRULESRESULT && layoutData.box != RA_STR.EXTELEMENTS && layoutData.rowOrder != '6' && layoutData.rowOrder != '7' &&
                                            <div className="row margin">
                                                <p className="title"><u>{layoutData.box}</u></p>
                                                {this.setBoxRows(layoutData)}
                                            </div>
                                        }
                                        {layoutData.box == RA_STR.RISKRULESRESULT &&
                                            <div className="row margin">
                                                <p className="title"><u>{layoutData.box}</u></p>
                                                {this.setRiskruleSetRows(layoutData)}
                                            </div>
                                        }
                                        {layoutData.box == RA_STR.ADDITIONALELEMENTS &&
                                            <div className="row margin">
                                                <p className="title"><u>{layoutData.box}</u></p>
                                                {this.setAdditionalrows(layoutData)}
                                            </div>
                                        }
                                    </>
                                )}
                                <div className="row margin">
                                    <p className="title"><u>{RA_STR.histbasedEle}</u></p>
                                    {selectedTxnDetails && selectedTxnDetails.historyBasedRulesJson &&
                                        <>
                                            <div className="col-sm-5 details-card no-padd">
                                                {Object.keys(selectedTxnDetails.historyBasedRulesJson).map((extElement, index) =>
                                                    <>
                                                        {(index % 2 === 0) &&
                                                            <div>  <label className="col-sm-7 col-form-label">{extElement}</label> {selectedTxnDetails.historyBasedRulesJson[extElement]} </div>
                                                        }
                                                    </>
                                                )}
                                            </div>
                                            <div className="col-sm-5 ml-2 details-card no-padd">
                                                {Object.keys(selectedTxnDetails.historyBasedRulesJson).map((extElement, index) =>
                                                    <>
                                                        {index % 2 === 1 &&
                                                            <div>  <label className="col-sm-7 col-form-label">{extElement}</label> {selectedTxnDetails.historyBasedRulesJson[extElement]}</div>
                                                        }
                                                    </>
                                                )}
                                            </div>
                                        </>
                                    }
                                </div>

                            </div>

                        }
                        <div className="form-group form-submit-button margin text-left">
                            <input className="secondary-btn" type="submit"
                                value="BACK" onClick={this.handleBack} />
                        </div>
                    </>
                }
            </div>
        );
    }
}

export default TransactionSummary;