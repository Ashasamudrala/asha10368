import React, { Component } from 'react';
import { serverUrl, RA_API_URL, RA_STR_SELECT, LOADING_TEXT } from '../../shared/utlities/constants';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import '../ReportsSummary/ReportsSummary.css';
import ManageInboundCallsInformation from './ManageInboundCallsInformation';
import TransactionSummary from './TransactionSummary';
import CA_Utils from '../../shared/utlities/commonUtils';
import Loader from '../../shared/utlities/loader';

class ManageInboundCalls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgOptions: [],
      enableOrg: true,
      inboundData: '',
      searchData: [],
      manageInboundData: {},
      showInboundPage: false,
      accountType: '-1000',
      showTransactionPage: false,
      accountType: '-1000',
      orgName: '',
      originalOrganizations: [],
      loader: false
    }
  }

  setUserAction = async () => {
    let searchData = [];
    let { orgName } = this.state;
    const getUserDetails = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['getUserAction']}`,
      data: {
        orgName,
        "workFlow": 4
      }
    };
    const getUserDetailsStatus = await getService(getUserDetails);
    if (getUserDetailsStatus && getUserDetailsStatus.status === 200) {
      searchData.push({
        'content': '--Select Search Type--',
        'key': '-1000'
      });
      searchData.push({
        'content': 'Username',
        'key': '-1000'
      });
      if (getUserDetailsStatus.data.accountTypeList !== null) {
        getUserDetailsStatus.data.accountTypeList.map((searchdata) => {
          searchData.push({
            'content': searchdata.displayName,
            'key': searchdata.accountType
          });
        })
      }
      this.setState({ inboundData: getUserDetailsStatus.data, searchData: searchData })
    } else if (getUserDetailsStatus && getUserDetailsStatus.status !== 200) {
    }
  }
  getSelectedOrgDetails = async (value, type) => {
    if (type === 'click' || type === 'blur') {
      let { originalOrganizations } = this.state;
      const orgId = originalOrganizations.find((element) => { if (element.content === value) return element.key });

      if ((type === 'blur' && !orgId && this.refs.autoSuggestionBox) || (value === RA_STR_SELECT && this.refs.autoSuggestionBox)) {
        this.refs.autoSuggestionBox.setvalue('');
      }
      this.setState({
        orgName: orgId ? orgId.key : '',
      }, () => {
        if (orgId) {
          this.setUserAction();
        }
      })
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  setInboundPage = async () => {
    if (this.caseID) {
      this.setState({ loader: true })
      const url = serverUrl + RA_API_URL.getCaseByCaseId.replace('#caseID', this.caseID);
      const data = { caseID: this.caseID };
      const response = await getService({ method: 'POST', url, data });
      if (response && response.data && response.status === 200) {
        this.setState({ manageInboundData: response.data, showInboundPage: true, loader: false });
      }
    }
  }

  componentDidMount = async () => {
    const urlParams = new URLSearchParams(window.location.search);
    this.caseID = urlParams.get('caseId');
    this.redirectedFrom = urlParams.get('fromPage')
    if (this.caseID && this.redirectedFrom) {
      this.setInboundPage();
    }
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions && getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      const originalOrganizations = CA_Utils.objToArray(getOrgOptions.data.organizations, 'object');
      this.setState({ orgOptions: orgOptions, originalOrganizations });
    }
  }
  componentWillMount() {

  }
  getManageInbound = async () => {
    const { orgName, accountId, accountType } = this.state;
    let data = {
      "orgName": orgName,
      "accountId": accountType === '-1000' ? '' : accountId,
      "accountType": accountType === '-1000' ? '' : accountType,
      "userName": accountType === '-1000' ? accountId : '',
      "workFlow": 4
    }
    const manageInbound = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['getInboundCase']}`,
      data: data
    };
    this.setState({ loader: true });
    const manageInboundStatus = await getService(manageInbound);
    if (manageInboundStatus && manageInboundStatus.status === 200) {
      this.props.activateSuccessList(true, manageInboundStatus.data);
      this.props.activateErrorList(false, '');
      this.setState({ manageInboundData: manageInboundStatus.data, showInboundPage: true, loader: false });
    } else if (manageInboundStatus && manageInboundStatus.status !== 200) {
      this.props.activateSuccessList(false, '');
      this.props.activateErrorList(true, manageInboundStatus.data.errorList);
      this.setState({ loader: false });
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  render() {
    const { orgOptions, enableOrg, inboundData, searchData, showInboundPage, showTransactionPage, orgName, loader } = this.state;
    return <div className='main manageInbound'>
      {loader ? <><Loader show='true' />{LOADING_TEXT}</> : <>
        {showInboundPage ? (showTransactionPage ? <TransactionSummary accessParent={this}></TransactionSummary> : <ManageInboundCallsInformation accessParent={this} ref="manageInboundRef"></ManageInboundCallsInformation>) :
          <>
            <h2 className='title'>{RA_STR.manageInboundtitle}</h2>
            <p className='desc'>{RA_STR.manageInbounddesc}</p>
            <div className='col-sm-8'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">{RA_STR.casestatusorglabel}</label>
                <div className="col-sm-4 no-padd">
                  <AutoSuggest ref="autoSuggestionBox" orgOptions={orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={enableOrg} defaultOrganizationPlaceholder={RA_STR_SELECT} value={orgName} />
                </div>
              </div>
              {
                (inboundData && inboundData.accountTypeList && inboundData.accountTypeList.length !== 0) ?
                  <div>
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">{RA_STR.manageInboundidentification}</label>
                      <div className="col-sm-4 no-padding">
                        <input
                          className="form-input form-control"
                          name='accountId'
                          type='text'
                          onChange={this.handleChange}
                        />
                      </div>
                      <div className="col-sm-4">
                        <div className="form-group dynamic-form-select">
                          <select
                            name='accountType'
                            onChange={this.handleChange}
                            className="form-select form-control">
                            {searchData.map((opt, index) => {
                              return (
                                <option
                                  key={index}
                                  value={opt.key}>{opt.content}</option>
                              );
                            })}
                          </select>
                        </div>
                      </div>
                      <input className="secondary-btn" id="createRoleButton" type="submit" value="SUBMIT" onClick={this.getManageInbound}></input>
                    </div>
                  </div> : <React.Fragment />
              }
              <div>
                {
                  (inboundData && inboundData.accountTypeList === null) ?
                    <>
                      <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.manageInboundusername}</label>
                        <div className="col-sm-4 no-padding">
                          <input
                            className="form-input form-control"
                            name='accountId'
                            type='text'
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      <div className="manage-button">
                        <input className="secondary-btn" id="createRoleButton" type="submit" value="SUBMIT" onClick={this.getManageInbound}></input></div></> : <React.Fragment />}
              </div>
            </div>
          </>
        }
      </>
      }
    </div>
  }
}

export default ManageInboundCalls;
