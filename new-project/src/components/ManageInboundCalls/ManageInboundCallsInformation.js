import React, { Component } from 'react';
import { serverUrl, RA_API_URL, RA_STR_CHECKBOX, RA_STR_SELECT } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import { getService } from '../../shared/utlities/RestAPI';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import DatePickerWithTimer from './ManageInboundDatePicker';
import TextArea from '../../shared/components/TextArea/TextArea';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import './ManageInboundCalls.scss'
import '../SearchCases/SearchCases.scss';
import ManageTables from './ManageTableData';
import ReactDOM from "react-dom";
var moment = require('moment');

class ManageInboundCallsInformation extends Component {
    constructor(props) {
        super(props);
        let { manageInboundData } = props.accessParent.state;
        this.state = {
            manageInboundData: manageInboundData,
            startDate: new Date(),
            endDate: new Date(),
            toDate: new Date(),
            fromDate: new Date(),
            caseStatusList: [],
            noteList: [],
            userStateList: [],
            nextActionDate: false,
            exceptionEndTimeList: [],
            userStateOption: false,
            exceptionList: false,
            userRadioData: '0',
            userStateRadioData1: '0',
            caseNoteList: manageInboundData.caseNoteList,
            userStateOption: false,
            caseNote: '',
            userStateReason: '',
            caseStatusID: manageInboundData.caseStatusID,
            exceptionUserReason: '',
            userStateSelect: 'UNDEFINED',
            exceptionUser: '0',
            userStateEndTime: ''
        }
    }

    componentDidMount = () => {
        this.setDropDownValues();
    }
    setDropDownValues = () => {
        let { manageInboundData } = this.state;
        let dropDowns = ['caseStatusList', 'noteList', 'userStateList', 'exceptionEndTimeList', 'fraudStatusList', 'fraudTypeList'];
        let allDropdownListData = {};

        dropDowns.forEach(arrayName => {
            if (manageInboundData[arrayName]) {
                allDropdownListData[arrayName] = manageInboundData[arrayName].map((data) => {
                    return {
                        'content': data.label,
                        'key': data.value
                    };
                })
            }
        })
        if (Object.keys(allDropdownListData).length > 0) {
            this.setState({ caseStatusList: allDropdownListData.caseStatusList, noteList: allDropdownListData.noteList, userStateList: allDropdownListData.userStateList, exceptionEndTimeList: allDropdownListData.exceptionEndTimeList.sort(function (a, b) { return a.key - b.key }), fraudStatusList: allDropdownListData.fraudStatusList, fraudTypeList: allDropdownListData.fraudTypeList });
        }
    }
    getMoreCaseNotes = async () => {
        const { caseID, numberOfCaseNotes, orgName } = this.state.manageInboundData;
        const getCaseNotes = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['getMoreCaseNotes']}`,
            data: {
                caseID,
                numberOfCaseNotes,
                orgName
            }
        };
        const getCaseNotesStatus = await getService(getCaseNotes);
        if (getCaseNotesStatus && getCaseNotesStatus.status === 200) {
            this.setState({ caseNoteList: getCaseNotesStatus.data })
        }
    }

    handleChange = (e) => {
        if (e.target.type === RA_STR_CHECKBOX) {
            if (e.target.name === 'exceptionList') {
                this.setState({ userRadioData: '0' });
            }
            else if (e.target.name === 'userStateOption') {
                this.setState({ userStateRadioData1: '0' });
            }
            this.setState({ [e.target.name]: e.target.checked });
        }
        else if (e.target.type === 'radio') {
            if (e.target.name === 'userRadioData') {
                this.setState({ userRadioData: e.target.value });
            }
            else if (e.target.name === 'userRadioData1') {
                this.setState({ userStateRadioData1: e.target.value });
            }
            else if (e.target.name === 'exceptionUser') {
                this.setState({ exceptionUser: e.target.value });
            }
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    }

    startDateChange = (date) => {
        this.setState({
            startDate: date
        });
    }
    endDateChange = (date) => {
        this.setState({
            endDate: date
        });
    }

    componentWillUnmount() {
        this.props.accessParent.props.activateErrorList(false, '');
        this.props.accessParent.props.activateSuccessList(false, '');
    }

    saveCaseData = async (e) => {
        let tableData = {};
        if (this.refs.getTableRef) {
            tableData = this.refs.getTableRef.getUpdatedData();
        }
        const { manageInboundData, userStateOption, userStateSelect, caseNote, userStateReason, exceptionList, exceptionUserReason, caseStatusID, nextActionDate, userStateEndTime } = this.state;

        let nextActionDateOption = this.refs.NextActionTimerRef.getDateRanges().saveDateObj.fromDate.toLocaleDateString() || moment(new Date(manageInboundData.nextActionDate)).format('L');
        let userStateFromDate = this.refs.userStateDatepickerRef.getDateRanges().saveDateObj.fromDate.toLocaleDateString() || moment(new Date(manageInboundData.exceptionUserFromDate)).format('L');
        let userStateToDate = this.refs.userStateDatepickerRef.getDateRanges().saveDateObj.toDate.toLocaleDateString() || moment(new Date(manageInboundData.exceptionUserToDate)).format('L');

        let exceptionUserFromDate = this.refs.userStateDatepickerRef.getDateRanges().saveDateObj.fromDate.toLocaleDateString() || moment(new Date(manageInboundData.exceptionUserFromDate)).format('L');
        let exceptionUserToDate = this.refs.userStateDatepickerRef.getDateRanges().saveDateObj.toDate.toLocaleDateString() || moment(new Date(manageInboundData.exceptionUserToDate)).format('L');

        const saveCaseData = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL.saveCase}`,
            data: {
                "orgName": manageInboundData.orgName,
                "caseNoteID": caseStatusID,
                "caseNote": caseNote,
                "nextActionDate": nextActionDateOption,
                "userName": manageInboundData.userName,
                "txnInputs": tableData.caseTxns,
                "userTxnInputs": tableData.userTxns,
                "nextActionDateOption": nextActionDate === false ? '0' : '1',
                "nextActionDateHrs": this.refs.NextActionTimerRef.getDateRanges().saveDateObj.fromHours,
                "nextActionDateMins": this.refs.NextActionTimerRef.getDateRanges().saveDateObj.fromMinutes,

                "userStateOption": userStateOption === '' ? '-1000' : (userStateOption === false ? '0' : '1'),
                "userState": userStateSelect,
                "userStateFromDate": userStateFromDate,
                "userStateFromHour": "00",
                "userStateFromMinute": "00",

                "userStateToDate": userStateToDate,
                "userStateToHour": "00",
                "userStateToMinute": "00",
                "userStateReason": userStateReason,
                "userStateEndTime": userStateEndTime,

                "exceptionUserOption": exceptionList === false ? '0' : '1',
                "exceptionUserFromDate": exceptionUserFromDate,
                "exceptionUserToDate": exceptionUserToDate,
                "exceptionUserReason": exceptionUserReason

            }
        };
        const saveCaseDataStatus = await getService(saveCaseData);
        let pathname = '';
        if (window.location) {
            pathname = window.location.pathname;
        }
        if (pathname.indexOf(RA_STR.managetheInbound) > 0) {
            if (saveCaseDataStatus && saveCaseDataStatus.status === 200) {
                this.props.accessParent.props.activateSuccessList(true, saveCaseDataStatus.data);
                this.props.accessParent.props.activateErrorList(false, '');
            } else {
                if (saveCaseDataStatus.data && saveCaseDataStatus.data.errorList) {
                    this.props.accessParent.props.activateSuccessList(false, '');
                    this.props.accessParent.props.activateErrorList(true, saveCaseDataStatus.data.errorList);
                }
            }
        }
    }
    openCalendar = (calendarType) => {
        const currentDom = ReactDOM.findDOMNode(this);
        if (calendarType && currentDom && currentDom.querySelector(calendarType)) {
            currentDom.querySelector(calendarType).click();
        }
    }
    setTransactionPage(txnID) {
        this.props.accessParent.setState({ showTransactionPage: true, txnID })
    }
    getBack = () => {
        if (this.props.accessParent.redirectedFrom) {
            let { manageInboundData } = this.state;
            let caseID = this.props.accessParent.caseID;
            const routeToRedirect = '/case-manage/search-case';
            this.props.accessParent.props.history.push({
                pathname: `${routeToRedirect}`,
                search: `?orgname=${manageInboundData.orgName}&caseID=${caseID}&page=caseSummary`
            })
        } else {
            this.props.accessParent.setState({ showInboundPage: false, inboundData: {} });
        }
    }
    render() {
        const { manageInboundData, caseStatusList, noteList, userStateList, nextActionDate, exceptionEndTimeList, userStateOption, exceptionList, userRadioData, caseNoteList, userStateRadioData1, userStateSelect, caseStatusID, } = this.state;
        return (
            <div >
                <div className='row'>
                    <div className='col-sm-6 mt-5'>
                        <div className='row'>
                            <div className='col-sm-3'>{RA_STR.inboundusername}</div>
                            <div className='col-sm-3'>{manageInboundData.userName}</div>
                        </div>
                        <div className='row mt-4'>
                            <div className='col-sm-3'>{RA_STR.inboundorganisation}</div>
                            <div className='col-sm-3'>{manageInboundData.orgName}</div>
                        </div>
                        <div className='row mt-4'>
                            <div className='col-sm-3'>{RA_STR.inboundnextactiondate}</div>
                            <div className='col-sm-4'>{manageInboundData.nextActionDate}</div>
                        </div>
                        <div className='row mt-4'>
                            <div className='col-sm-3'>{RA_STR.inboundnextcaseid}</div>
                            <div className='col-sm-3'>{manageInboundData.caseID}</div>
                        </div>
                    </div>
                    <div className='col-sm-6 mt-5 case-note-bgcolor '>
                        <div className='row mt-3'>
                            <div className='col-sm-3'>
                                {RA_STR.managecasehistory}
                            </div>
                            <div className="col-sm-8  case-note-scroll mb-2">
                                {(caseNoteList && caseNoteList.length > 0) ? caseNoteList.map((noteData) => {
                                    return <>
                                        <p className='case-data'>{noteData.adminName + RA_STR.manageupdatecase + (noteData.creationDateFormatted ? noteData.creationDateFormatted : '')}</p>
                                        <p className='case-data'> <b>{RA_STR.manageinboundcasenote}</b> {manageInboundData.noteList && manageInboundData.noteList[noteData.caseNoteID] && manageInboundData.noteList[noteData.caseNoteID]['label'] + (noteData.caseNote ? noteData.caseNote : "")}
                                        </p>
                                    </>
                                }) :
                                    RA_STR.None
                                }
                                {(manageInboundData.moreCaseNotesPresent && caseNoteList.length <= 1) ?
                                    <span className="orgName" onClick={this.getMoreCaseNotes}>{RA_STR.manageinboundtext}</span> : ''
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {caseStatusList.length > 0 && <ManageTables accessParent={this} ref='getTableRef'></ManageTables>}
                <div className="mt-4 ">
                    <hr></hr>
                    <div className="row mt-6 ml-1">
                        <div className="col-sm-6">
                            <Select
                                selectedOption={caseStatusID}
                                name={'caseStatusID'}
                                title={'Case Status'}
                                options={caseStatusList}
                                controlFunc={this.handleChange}
                                placeholder='--Select--'
                            />
                        </div>
                    </div>
                    <div className="col-sm-6 ml-1">
                        <Select
                            name={'userStateOption'}
                            title={'Note'}
                            options={noteList}
                            placeholder='--Select--'
                            controlFunc={this.handleChange}
                        />
                    </div>
                    <div className="col-sm-6 ml-1">
                        <TextArea
                            title={'Additional Note'}
                            resize={false}
                            name={'caseNote'}
                            controlFunc={this.handleChange}
                            placeholder={''} />
                    </div>
                    <div className="row mt-4 searchCases">
                        <div className='col-sm-2 no-padd'>
                            <Checkbox
                                type={'checkbox'}
                                setName="nextActionDate"
                                controlFunc={this.handleChange}
                                options={["Next Action Date(GMT)"]}
                            />
                        </div>
                        <DatePickerWithTimer ref='NextActionTimerRef' hideDateField={true} fromDate={manageInboundData.nextActionDate} fromHours={manageInboundData.nextActionDateHrs} fromMinutes={manageInboundData.nextActionDateMins} toDate={manageInboundData.exceptionUserToDate} disableAll={(nextActionDate === true) ? false : true} ></DatePickerWithTimer>
                    </div>
                    <div className="row  mt-4">
                        <Checkbox
                            type={'checkbox'}
                            setName="userStateOption"
                            controlFunc={this.handleChange}
                            options={["Select User State"]} />
                        <div className="col-sm-8 select-user-state no-padd">
                            <div className="form-group dynamic-form-select">
                                <div className="form-group  row">
                                    <label className="col-form-label col-sm-1"></label>
                                    <select
                                        name='userStateSelect'
                                        disabled={!(userStateOption)}
                                        onChange={this.handleChange}
                                        className="form-select form-control ml-4 col-sm-5">
                                        {userStateList.map((opt, index) => {
                                            return (
                                                <option
                                                    key={opt.index}
                                                    value={opt.value}>{opt.content}</option>
                                            );
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-2">
                        <div className='col-sm-2 no-padd'>
                            <div className="custom-control custom-radio ml-3">
                                <input type="radio" className="custom-control-input "
                                    id="userstatedata1"
                                    name="userRadioData1"
                                    value='0'
                                    checked={userStateRadioData1 == '0'}
                                    disabled={userStateSelect === 'UNDEFINED' || !(this.state.userStateOption)}
                                    onChange={this.handleChange}
                                />
                                <label className="custom-control-label" htmlFor="userstatedata1">Date Range</label>
                            </div>
                        </div>
                        <DatePickerWithTimer ref='userStateDatepickerRef' hideDateRangeField={true} fromDate={manageInboundData.exceptionUserFromDate} toDate={manageInboundData.exceptionUserToDate}
                            disableAll={userStateSelect === 'UNDEFINED' || !(this.state.userStateOption) || userStateRadioData1 === '1'} ></DatePickerWithTimer>
                    </div>
                    <div className="row mt-2 ">
                        <div className="custom-control custom-radio ml-3">
                            <input type="radio" className="custom-control-input"
                                id="userstateparent1"
                                name="userRadioData1"
                                value='1'
                                checked={userStateRadioData1 == '1'}
                                onChange={this.handleChange}
                                disabled={userStateSelect === 'UNDEFINED' || !(this.state.userStateOption)} />
                            <label className="custom-control-label" htmlFor="userstateparent1">{RA_STR.managepredefinedduration}</label>
                        </div>

                        <div className="col-sm-8 select-user-state">
                            <div className="form-group dynamic-form-select">
                                <div className="form-group  row">
                                    <label className="col-form-label  col-sm-1 "></label>
                                    <select
                                        name='userStateEndTime'
                                        disabled={userStateSelect === 'UNDEFINED' || !(this.state.userStateOption)}
                                        onChange={this.handleChange}
                                        className="form-select form-control  col-sm-5 ml-4">
                                        <option value="">{RA_STR_SELECT}</option>
                                        {exceptionEndTimeList.map((opt, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    value={opt.key}>{opt.content}</option>
                                            );
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 ml-1 mt-2">
                        <TextArea
                            title={'Reason'}
                            resize={false}
                            name={'userStateReason'}
                            controlFunc={this.handleChange}
                            placeholder={''}
                            disabled={!(userStateOption)} />
                    </div>
                    <div className="row mt-4">
                        <Checkbox
                            type={'checkbox'}
                            setName="exceptionList"
                            value={this.state.scopeAll}
                            controlFunc={this.handleChange}
                            options={["Add User to exception list"]}
                        />
                    </div>
                    <div className="row mt-4">
                        <div className='col-sm-2 no-padd'>
                            <div className="custom-control custom-radio ml-3 ">
                                <input type="radio" className="custom-control-input "
                                    id="caseDataTypeChild"
                                    name="userRadioData"
                                    value='0'
                                    checked={userRadioData == '0'}
                                    disabled={!(exceptionList)}
                                    onChange={this.handleChange}
                                />
                                <label className="custom-control-label" htmlFor="caseDataTypeChild">{RA_STR.managedateRange}</label>
                            </div>
                        </div>
                        <DatePickerWithTimer ref='DatepickerTimerRef' hideDateRangeField={true} fromDate={manageInboundData.exceptionUserFromDate} toDate={manageInboundData.exceptionUserToDate} disableAll={!(exceptionList) || userRadioData == '1'}></DatePickerWithTimer>
                    </div>
                    <div className="row mt-4 ">
                        <div className="custom-control custom-radio ml-3">
                            <input type="radio" className="custom-control-input"
                                id="caseDataTypeparent"
                                name="userRadioData"
                                value='1'
                                checked={userRadioData == '1'}
                                onChange={this.handleChange}
                                disabled={!(exceptionList)} />
                            <label className="custom-control-label" htmlFor="caseDataTypeparent">{RA_STR.managepredefinedduration}</label>
                        </div>
                        <div className="col-sm-8 select-user-state">
                            <div className="form-group dynamic-form-select">
                                <div className="form-group  row">
                                    <label className="col-form-label  col-sm-1 "></label>
                                    <select
                                        name='nnm'
                                        disabled={!(exceptionList)}
                                        className="form-select form-control ml-3 col-sm-5">
                                        <option value="">{RA_STR_SELECT}</option>
                                        {exceptionEndTimeList.map((opt, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    value={opt.value}>{opt.content}</option>
                                            );
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 ml-1">
                        <SingleInput
                            title={'Reason'}
                            inputType={'text'}
                            name='exceptionUserReason'
                            controlFunc={this.handleChange}
                            disabled={!(exceptionList)} />
                    </div>
                    <div className="form-group row mt-4 ml-0 ReportButtons">
                        <input className="secondary-btn" id="searchButton" type="submit" value="CANCEL" onClick={this.getBack}></input>
                        <input className="secondary-btn ml-4" id="searchButton" type="submit" value="SAVE" onClick={this.saveCaseData}></input>
                    </div>
                </div>
            </div>)
    }
}
export default ManageInboundCallsInformation;

