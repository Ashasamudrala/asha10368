import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { serverUrl , RA_STR_MASTERADMIN ,RA_STR_PROFILEDATA ,RA_STR_CHECKBOX , RA_STR_SELECT , RA_API_URL , RA_STR_MA}  from '../../shared/utlities/constants';
import ReactTooltip from 'react-tooltip';
import './myProfile.css';

class MyProfile extends Component {
  constructor(props) {
    super(props);
    const  userprofiledata = (JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA))  !== null) ? JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA)).userProfile:'';
    this.state = {
      localeOptions: [],
      orgOptions: [],
      timeZoneOptions: [],
      profiledata:(JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA))  !== null) ? JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA)):'',
      firstName: (userprofiledata !== null)  ? userprofiledata.userName !== RA_STR_MASTERADMIN ? userprofiledata.firstName : '' : '',
      middleName:(userprofiledata !== null)  ? userprofiledata.userName !== RA_STR_MASTERADMIN ? userprofiledata.middleName : '' :'',
      lastName: (userprofiledata !== null)  ?userprofiledata.userName !== RA_STR_MASTERADMIN ? userprofiledata.lastName : '' :'',
      email: (userprofiledata !== null)  ?userprofiledata.userName !== RA_STR_MASTERADMIN ? userprofiledata.email : '' : '',
      phoneNumber: (userprofiledata !== null)  ?userprofiledata.userName !== RA_STR_MASTERADMIN ? userprofiledata.phoneNumber : '' : '',
      currPassword: '',
      newPassword: '',
      confirmPassword: '',
      dateTime: (userprofiledata !== null)  ? (userprofiledata.dateTimeFormat !== null) ?userprofiledata.dateTimeFormat : 'MM/dd/yyyy HH:mm:ss (z)' : 'MM/dd/yyyy HH:mm:ss (z)' ,
      locale: (userprofiledata !== null)  ? (userprofiledata.locale !== null) ? userprofiledata.locale :'' :'',
      timeZone: (userprofiledata !== null)  ?  (userprofiledata.timeZone !== null ) ? userprofiledata.timeZone : '':'',
      defaultOrganizationForUserCreation: (userprofiledata !== null)  ? userprofiledata.preferredOrganization !== null ? userprofiledata.preferredOrganization : RA_STR_SELECT : '',
      enableOrg: false ,
      organisationList:''

    }
  }
  showErrorList = () => {
    window.scrollTo(0, 0);
  }
  componentWillMount = async () => {
    let orgOptions = [];
    let jwtToken = this.state.profiledata ? this.state.profiledata.token : null;
    const checkLocale = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['localeUrl']}`
    };
    const getLocale = await getService(checkLocale);
    if(getLocale.status === 200) {
    let objectKeys = Object.keys(getLocale.data.locales);
    let objectValues = Object.values(getLocale.data.locales);
    for (let i = 0; i < objectKeys.length; i++) {
      this.state.localeOptions.push({
        'key': objectKeys[i],
        'content': objectValues[i]
      })
    }
      this.setState({ localeOptions: this.state.localeOptions  });
    }
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if(getOrgOptions.status === 200 ) {
    orgOptions = Object.values(getOrgOptions.data.organizations);
    orgOptions.unshift(RA_STR_SELECT);

    this.setState({ orgOptions: orgOptions ,organisationList : getOrgOptions.data.organizations});
    }
    const checktimezone = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['timezoneUrl']}`
    };

    const getTimeZoneOptions = await getService(checktimezone);
    if(getTimeZoneOptions.status === 200) {
    for (let index = 0; index < getTimeZoneOptions.data.timeZones.length; index++) {
      this.state.timeZoneOptions.push({
        'content': getTimeZoneOptions.data.timeZones[index].displayName,
        'key': getTimeZoneOptions.data.timeZones[index].timezoneId
      })
    }
      this.setState({ timeZoneOptions: this.state.timeZoneOptions  });
  } 
}

  getSelectedOrgDetails = (getSelectedOrgDetails) => {
    this.setState({
      defaultOrganizationForUserCreation: getSelectedOrgDetails
    })
  }

  handleChange = (e) => {
    if (e.target.type === RA_STR_CHECKBOX) {
      this.setState({ [e.target.name]: e.target.checked } 
      );
    } else {
      this.setState({ [e.target.name]: e.target.value })
  } 
}

  updateProfile = async() => {
    const profiledata = JSON.parse(JSON.stringify(this.state.profiledata.userProfile));
    const tempProfileData = JSON.parse(JSON.stringify(this.state.profiledata));
    let jwtToken = this.state.profiledata ? this.state.profiledata.token : null;
    let myprofileUrl ;
    const {confirmPassword , currPassword , email , firstName  ,lastName , timeZone , middleName , locale , newPassword , phoneNumber , dateTime , defaultOrganizationForUserCreation , enableOrg} = this.state
    let orgKeys = Object.keys(this.state.organisationList) ;
    let indexKey ;
    let orgKey
    for(let index = 0; index < orgKeys.length ; index++) {
        if(this.state.orgOptions[index] === this.state.defaultOrganizationForUserCreation) {
          indexKey = index;
        }
    }
    if( indexKey === 0) {
      orgKey =  RA_STR_SELECT;
    }
    else {
      orgKey = orgKeys[indexKey-1];
    }
    if(profiledata.role === RA_STR_MA ) {
      myprofileUrl = RA_API_URL['masterProfileUrl'] ;
    } 
    else {
      myprofileUrl = RA_API_URL['globalProfileUrl'] ;
    }
    const updateProfile = {
      method: 'PUT',
      url: `${serverUrl}${myprofileUrl}`,
      data: {
        "address": "",
        "confirmPassword": confirmPassword ? confirmPassword : null,
        "curPassword": currPassword ? currPassword : null ,
        "defaultOrganizationForUserCreation":enableOrg ? orgKey : null,
        "email": email,
        "firstName": firstName,
        "lastName": lastName,
        "localTimeZone": timeZone ? timeZone : this.state.timeZoneOptions[0].key,
        "locale": locale ? locale : this.state.localeOptions[0].key,
        "middleName": middleName,
        "newPassword": newPassword ? newPassword : null ,
        "orgName": profiledata.organization,
        "phone": phoneNumber,
        "preferredDateTimeFormat": dateTime,
        "userName": profiledata.userName
      }
    }
    const updateProfileStatus = await getService(updateProfile);
    if (updateProfileStatus && updateProfileStatus.status === 200) {
      this.showErrorList();
      if (profiledata.userName === RA_STR_MASTERADMIN) {
        profiledata.timeZone = timeZone
        profiledata.locale = locale
        profiledata.dateTimeFormat = dateTime 
      } else {
        profiledata.email =email 
        profiledata.firstName = firstName 
        profiledata.middleName = middleName 
        profiledata.lastName = lastName 
        profiledata.phoneNumber = phoneNumber 
        profiledata.locale = locale 
        profiledata.dateTimeFormat = dateTime 
        profiledata.timeZone = timeZone 
        profiledata.preferredOrganization = defaultOrganizationForUserCreation 
      }
      delete tempProfileData['userProfile'];
      tempProfileData['userProfile']  = profiledata;
      if(JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA)) !== null) {
      localStorage.setItem('profiledata', JSON.stringify(tempProfileData));
      }
      this.setState({
        profiledata: tempProfileData 
      });
      this.props.activateSuccessList(true, updateProfileStatus.data);
      this.props.activateErrorList(false, '');
      
    } else {
      this.showErrorList();
      this.props.activateSuccessList(false, '');
      this.props.activateErrorList(true, updateProfileStatus.data.errorList);
      this.setState({  newPassword:'',confirmPassword:''});
    }
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  render() {
    const { localeOptions, timeZoneOptions, locale, timeZone ,dateTime , firstName , lastName , middleName , email , phoneNumber , confirmPassword , newPassword} = this.state;
    const profiledata = JSON.parse(JSON.stringify(this.state.profiledata.userProfile));
    return <div className="main">
          <div className="row">
        {(profiledata) ?
        profiledata.userName !== RA_STR_MASTERADMIN ?
          <div className="col-md-12 form-inline row">
            <div className="col-md-4">Username: {profiledata.userName}</div>
            <div className="col-md-4">Organization: {profiledata.organization}</div>
            <div className="col-md-4">Role: {profiledata.roleDisplayName}</div>
          </div>
          : '' : ''}
      </div>
      <h2 className="title">My Profile</h2>
      <p className="desc">Update your personal details and preferences.</p>
      <div className="col-sm-7">
        {profiledata ? profiledata.userName !== RA_STR_MASTERADMIN ?
          <div>
            <span className="ecc-h1 row">Personal Information</span>
            <SingleInput
              title={'First Name'}
              inputType={'text'}
              required={true}
              name={'firstName'}
              content={firstName}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Middle Name'}
              inputType={'text'}
              name={'middleName'}
              content={middleName}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Last Name'}
              required={true}
              inputType={'text'}
              name={'lastName'}
              content={lastName}
              controlFunc={this.handleChange} />
            <div className="div-seperator">
              <span className="ecc-h1 row">Email Address(es) Telephone Number(s)</span>
              <SingleInput
                title={'Email'}
                inputType={'text'}
                required={true}
                name={'email'}
                content={email}
                controlFunc={this.handleChange} />
              <SingleInput
                title={'Phone Number'}
                required={true}
                inputType={'text'}
                name={'phoneNumber'}
                content={phoneNumber}
                controlFunc={this.handleChange} />
            </div>
          </div>
          : '' :''}
        <div className="div-seperator">
          <span className="ecc-h1 row">Change Password</span>
          <SingleInput
            title={'Current Password'}
            inputType={'password'}
            name={'currPassword'}
            controlFunc={this.handleChange} />
          <span data-tip data-for='password-criteria'>
            <SingleInput
              title={'New Password'}
              inputType={'password'}
              name={'newPassword'}
              content={newPassword}
              controlFunc={this.handleChange} />
          </span>
          <ReactTooltip id='password-criteria' place="right" effect="solid" type="dark" >
            <ul className="password-criteria">
              Password must be at least 6 characters long, and must include
              <li>At least 1 numeric characters</li>
              <li>At least 4 alphabetic characters( 0 LowerCase, 0 UpperCase)</li>
              <li>At least 1 special characters  ( Allowed special characters are    !@#$%^&amp;*()_+ )</li></ul>
          </ReactTooltip>
          <span data-tip data-for='password-criteria1'>
          <SingleInput
              title={'Confirm Password'}
              inputType={'password'}
              name={'confirmPassword'}
              content={confirmPassword}
              controlFunc={this.handleChange} />
          </span>
          <ReactTooltip id='password-criteria1' place="right" effect="solid" type="dark" >
            <ul className="password-criteria1">
              Password must be at least 6 characters long, and must include
              <li>At least 1 numeric characters</li>
              <li>At least 4 alphabetic characters( 0 LowerCase, 0 UpperCase)</li>
              <li>At least 1 special characters  ( Allowed special characters are    !@#$%^&amp;*()_+ )</li></ul>
          </ReactTooltip>

        </div>
        <div className="div-seperator ">
          <span className="ecc-h1 row">Administrator Preferences</span>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Enable Preferred Organization</label>
            <div className="col-sm-8 checkbox">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="customCheck1" name="enableOrg" onChange={this.handleChange} />
                <label className="custom-control-label" htmlFor="customCheck1"></label>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Preferred Organization:</label>
            <div className="col-sm-8" >
              <AutoSuggest orgOptions={this.state.orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={this.state.enableOrg} defaultOrganizationPlaceholder={this.state.defaultOrganizationForUserCreation} />
            </div>
          </div>
          <SingleInput
            title={'Date Time Format'}
            inputType={'text'}
            name={'dateTime'}
            content = {dateTime}
            imgActivate={true}
            controlFunc={this.handleChange}
          />
          <Select
            name={'locale'}
            title={'Locale'}
            options={localeOptions ? localeOptions : ['']}
            selectedOption={locale}
            controlFunc={this.handleChange} />
          <Select
            name={'timeZone'}
            title={'Time Zone'}
            selectedOption={timeZone}
            options={timeZoneOptions ? timeZoneOptions : ['']}
            controlFunc={this.handleChange} />
        </div>

      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Save" onClick={this.updateProfile}></input>
      </div>
    </div>;
  }
}

export default MyProfile;
