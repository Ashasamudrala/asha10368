import React, { Component } from 'react';
import './ModelConfiguration.css'
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';

class ModelConfiguration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showForm: false,
      ruleList: [],
      selectedRuleSet: '',
      modelcheck: false,
      orgName: ''
    }
  }
  componentDidMount() {
    let orgname = new URLSearchParams(window.location.search).get('orgname');
    this.setState({ orgName: orgname })
    this.getAllRuleset(orgname);
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  getAllRuleset = async (orgname) => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL.getRuleset + '/' + orgname}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({ ruleList: CA_Utils.objToArray(response.data.configNamesList, 'object') });
    }
  };

  handleChange = (e) => {
    if (e.target.name === 'ruleset') {
      this.setState({ selectedRuleSet: e.target.value })
    } else if (e.target.name === 'enableToken') {
      this.setState({ modelcheck: e.target.checked });
    }
  }

  updateConfiguration = async () => {
    if (!this.state.selectedRuleSet) {
      alert('Select a RuleSet');
    } else {
      this.props.activateErrorList(false, '');
      this.props.activateSuccessList(false, '');
      let config = ''
      this.state.ruleList.map(item => {
        if (item.key == this.state.selectedRuleSet) {
          config = item.content;
          return item
        }
      })
      const updateRules = {
        method: 'PUT',
        url: `${serverUrl}${RA_API_URL['ruleSet']}`,
        data: {
          configName: config,
          orgName: this.state.orgName,
          scoreData: this.state.modelcheck ? 'GDP;5;1;30###' : 'GDP;5;0;30###'
        }
      }
      const response = await getService(updateRules);
      if (response && response.status === 200) {
        this.props.activateSuccessList(true, response.data);
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, response.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
  }
  showModal = () => {
    this.setState({ showForm: !this.state.showForm });
  }
  render() {
    const { ruleList, selectedRuleSet, modelcheck, showForm } = this.state;
    return (<div className='main modelchange'>
      <div className='row'>
        <div className="col-sm-12">
          <h2 className="title">{RA_STR.modelConfig} </h2>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12">
          <span className="enablemodel" onClick={this.showModal} > {RA_STR.model_enable} </span>
        </div>
      </div>
      {
        (showForm)
          ? <div>
            <div className="desc">
              <p>{RA_STR.modelConfig_Desc}</p>
            </div>
            <div className='col-sm-8'>
              <div className="form-group dynamic-form-select">
                <div className="form-group row">
                  <div className=' col-sm-6'>
                    <Select
                      name={'ruleset'}
                      title={'Ruleset'}
                      required={false}
                      selectedOption={selectedRuleSet}
                      placeholder={RA_STR_SELECT}
                      options={ruleList ? ruleList : ['']}
                      controlFunc={this.handleChange} />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-8">
              <div className='form-group row'>
                <label className='col-sm-2 col-form-label'>{RA_STR.model_lable}</label>
                <div className='col-sm-8'>
                  <div className='custom-control custom-checkbox'>
                    <input
                      type='checkbox'
                      className='custom-control-input'
                      id='tokenEnable'
                      name='enableToken'
                      checked={modelcheck}
                      onChange={this.handleChange}
                    />
                    <label className='custom-control-label' htmlFor='tokenEnable'></label>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12">
              <div className="form-group form-submit-button row">
                <input className="secondary-btn" type="submit" value="SAVE" onClick={this.updateConfiguration}></input>
              </div>
            </div>
          </div>
          : null
      }
    </div>)
  }
}

export default ModelConfiguration;
