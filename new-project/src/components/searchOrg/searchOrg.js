import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as reduxaction from '../../redux/actions/ActionCreator';
import './searchOrg.css';
class SearchOrg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTable: false,
      disableRefresh: true,
      redirectUrl: '/org/searchOrg/basicOrg/org-details',
      selectedOrgNames: [],
      buttonActivation: [],
      statusGroup: ["Active"],
      inputData: "",
      pageNum: 0,
      activePageNum: 1,
      pageSize: 10,
      totalCount: [],
      columns: [
        { title: 'Organization', field: 'orgName', render: rowData => this.getSelectedOrgDetails(rowData) },
        { title: 'Display Name', field: 'orgDisplayName' },
        { title: 'Description', field: 'orgDescription' },
        { title: 'Status', field: 'statusCode' },
      ],
      apiData: [
      ]
    };
    this.data = this.data.bind(this);
  }
  componentDidMount = () => {
    let getOrgRedirectStatus = this.props.history.location.state ? this.props.history.location.state.redirectToSearchOrg : '';
    if (getOrgRedirectStatus) {
      const orgDetails = JSON.parse(localStorage.getItem('orgDetails'));
      let getStatusGroup = this.state.StatusGroup;
      getStatusGroup = orgDetails.statusList;
      this.setState({
        inputData: orgDetails.orgName, pageNo: orgDetails.pageNo + 1, activePageNum: orgDetails.pageNo + 1, statusGroup: getStatusGroup
      }, () => {
        this.submitSearch(this, orgDetails);
      })
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  getSelectedOrgDetails = (rowData) => {
    return <div className="orgName" onClick={() => this.data(rowData)}>{rowData.orgName}</div>
  }
  data = (rowData) => {
    let storeOrgData = {
      "orgName": this.state.inputData,
      "statusList": this.state.statusGroup,
      "pageNo": this.state.pageNum,
      "pageSize": this.state.pageSize
    }
    localStorage.setItem('orgDetails', JSON.stringify(storeOrgData));
    this.props.reduxaction.setDefaultData(storeOrgData);
    this.props.history.push(`${this.state.redirectUrl}?orgname=${rowData.orgName}&status=${rowData.statusCode}`);
  }
  handleChange = (e) => {
    let getValue = e.target;
    let getallStatus = this.state.statusGroup;
    if (getValue.checked) {
      getallStatus.push(getValue.value);
    } else {
      let filtered = getallStatus.indexOf(getValue.value);
      getallStatus.splice(filtered, 1);
    }
    this.setState({ statusGroup: getallStatus });
  }
  handleInputChange = (e) => {
    this.setState({ inputData: e.target.value });
  }
  submitSearch = async (e = '', getStoreData = '') => {
    let count = 0;
    if ((e && e.type === "click") || (e && e.keyCode === 13)) {
      this.setState({ pageNum: count, activePageNum: 1 });
    } else {
      count = this.state.pageNum;
    }
    let dataPayload = '';
    if (getStoreData) {
      dataPayload = getStoreData;
    } else {
      dataPayload = {
        "orgName": this.state.inputData,
        "statusList": this.state.statusGroup,
        "pageNo": count,
        "pageSize": this.state.pageSize
      }
    }
    const getSearch = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['postsearch']}`,
      data: dataPayload

    };
    const getSearchStatus = await getService(getSearch);
    if (getSearchStatus.status === 200 && !getSearchStatus.data.errorList.length) {
      let checkButtonStatus = [];
      getSearchStatus.data && getSearchStatus.data.orgList.forEach((eachOrgList) => {
        if (!checkButtonStatus.includes(eachOrgList.statusCode)) {
          checkButtonStatus.push(eachOrgList.statusCode);
        }
      })
      this.setState({ totalCount: getSearchStatus.data.resultCount, apiData: getSearchStatus.data.orgList, showTable: true, buttonActivation: checkButtonStatus });
      this.props.activateErrorList(false, '');
      this.props.activateSuccessList(false, '');
    } else {
      this.setState({ apiData: [], showTable: false });
      this.props.activateErrorList(true, getSearchStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  getActivePage = (getPageNum) => {
    let pageNumCount = getPageNum - 1;
    this.setState({
      pageNum: pageNumCount, selectedOrgNames: [], activePageNum: getPageNum
    }, () => {
      this.submitSearch();
    })
  }
  getcheckboxselection = (getData) => {
    let groupOrgNames = [];
    if (getData.length) {
      getData.forEach((eachData) => {
        groupOrgNames.push(eachData.orgName);
      })
    } else {
      groupOrgNames = [];
    }
    this.setState({ selectedOrgNames: groupOrgNames });
  }
  keyPress = (e) => {
    if (e.keyCode === 13) {
      this.submitSearch(e);
    }
  };
  manipulateSearch = async (e) => {
    let getClickedButton = e.target;
    if (this.state.selectedOrgNames.length) {
      let dialogResult = window.confirm(`Are you sure you want to ${getClickedButton.value.toLowerCase()} the selected Organization(s)?`);
      if (dialogResult) {
        const putSearch = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['status']}`,
          data: {
            "orgNameList": this.state.selectedOrgNames,
            "status": getClickedButton.id
          }
        };
        const putSearchStatus = await getService(putSearch);
        if (putSearchStatus.data && putSearchStatus.data.message) {
          this.props.activateSuccessList(true, putSearchStatus.data);
          this.setState({ apiData: [], showTable: false });
        }
      }
    } else {
      alert(`Please select one or more Organizations to ${getClickedButton.value.toLowerCase()}.`);
    }
  }
  render() {
    const { buttonActivation, totalCount } = this.state;
    return <div className="main SearchOrg">
      <h2 className="title">Search Organization</h2>
      <div>
        {this.state.showTable ? (
          <div>
            <p className="desc">Click the Organization Name link to view or edit the details of the selected Organization. To enable, disable, or delete multiple organizations, select the required Organizations and click the applicable button.</p>
            <p><b>Tip:</b> You can also reorder the list of organizations by clicking the Organization column heading.</p>
          </div>
        ) : (
            <p className="desc">Enter the full Display Name or first few characters of the Organization Display Name that you want to search.</p>
          )}
      </div>
      <div>
      </div>
      <div className="col-sm-8 div-seperator">
        <SingleInput
          title={'Organization'}
          inputType={'text'}
          name={'Organization-name'}
          content={this.state.inputData}
          enterFunc={this.keyPress}
          controlFunc={this.handleInputChange}
        />
      </div>
      <div className="form-group row col-sm-8">
        <label className="col-sm-4 col-form-label">
          Status
          </label>
        <div className="col-sm-8 form-inline">
          <Checkbox
            title={'Initial'}
            setName={'Initial'}
            type={'checkbox'}
            options={['Initial']}
            controlFunc={this.handleChange}
            selectedOptions={this.state.statusGroup.includes('Initial') ? ['Initial']:''} />
          <Checkbox
            title={'Active'}
            setName={'Active'}
            type={'checkbox'}
            controlFunc={this.handleChange}
            options={['Active']}
            selectedOptions={this.state.statusGroup.includes('Active') ? ['Active']:''} />
          <Checkbox
            title={'Inactive'}
            setName={'Inactive'}
            type={'checkbox'}
            controlFunc={this.handleChange}
            options={['Inactive']}
            selectedOptions={this.state.statusGroup.includes('Inactive') ? ['Inactive']:''} />
          <Checkbox
            title={'Deleted'}
            setName={'Deleted'}
            type={'checkbox'}
            controlFunc={this.handleChange}
            options={['Deleted']}
            selectedOptions={this.state.statusGroup.includes('Deleted') ? ['Deleted']:''} />
        </div>
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Search" onClick={this.submitSearch}></input>
      </div>
      <div>
        {this.state.showTable ? <div className="tabele-buttons"><DynamicTable columns={this.state.columns} data={this.state.apiData} enablePagination={true} pageSize={this.state.pageSize} totalCount={totalCount} selection={true} activePage={this.getActivePage} handleCheckboxSelection={this.getcheckboxselection} title={"Select Organizations to Modify"} activePageNumNew={this.state.activePageNum} />
          <div className="form-inline">
            <div className="form-group form-submit-button">
              <input className="secondary-btn" id="Active" type="submit" value="ACTIVATE" onClick={this.manipulateSearch} disabled={(buttonActivation.includes("Active")) || (buttonActivation.includes("Deleted") && buttonActivation.length === 1) ? true : false}></input>
            </div>
            <div className="form-group form-submit-button p-2">
              <input className="secondary-btn" id="Inactive" type="submit" value="DEACTIVATE" onClick={this.manipulateSearch} disabled={(buttonActivation.includes("Inactive")) || (buttonActivation.includes("Deleted") && buttonActivation.length === 1) || (buttonActivation.includes("Initial") && buttonActivation.length === 1) ? true : false}></input>
            </div>
            <div className="form-group form-submit-button p-2">
              <input className="secondary-btn" id="Deleted" type="submit" value="DELETE" onClick={this.manipulateSearch} disabled={buttonActivation.includes("Deleted") && buttonActivation.length === 1 ? true : false}></input>
            </div>
            <div className="form-group form-submit-button p-2" >
              <input className="secondary-btn" id="refreshcache" type="submit" value="REFRESHCACHE" onClick={this.manipulateSearch} disabled={this.state.disableRefresh === true ? true : false}></input>
            </div>
          </div>
        </div>
          : ''}
      </div>
    </div>
      ;
  }
}
function mapStateToProps(state) {
  return {
    details: state.fetchOrgDetails ? state.fetchOrgDetails.data : '',
  };
}
function mapDispatchToProps(dispatch) {
  return {
    reduxaction: bindActionCreators(reduxaction, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchOrg);

