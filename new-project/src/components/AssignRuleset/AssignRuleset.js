import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
class AssignRuleset extends Component {
    constructor(props) {
        super(props);
        this.state = { roleOptions: [] }
    }
    render() {
        return (<div className="main">
            <h2 className="title">Assign Ruleset</h2>
            <p className="desc">Select the ruleset that must be used during Risk Evaluation. The rulesets that have been migrated to production are available in the list. Alternatively, you can select the parent organization's ruleset. Refresh the cache for the changes to take effect.</p>
            <div className="col-sm-7 ">
                <div className="row">
                    <label className="col-sm-4 col-form-label">Active Ruleset for Organization</label>
                    <div className="col-sm-8 mt-1">
                        ListData
                    </div>
                </div>
                <Select
                    name={'req-id'}
                    title={'Request ID'}
                    options={this.state.roleOptions}
                    placeholder={'Select'} />
            </div>
        </div>);
    }
}

export default AssignRuleset;