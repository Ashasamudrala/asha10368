import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_STR_SELECT, RA_API_URL } from '../../shared/utlities/constants';
import {RA_STR} from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';
class RuleConfigReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgOptions: [],
      selectedOrg: '',
      RulesetOptions: [],
      activeOptions: ["ACTIVE"],
      selectedRuleset: '',
      proposedOptions: [],
      storeReportData: [],
      columns: [
        { title: RA_STR.ruleName, field: 'ruleName' },
        { title: RA_STR.ruleEnable, field: 'enabled' },
        { title: RA_STR.rulePriority, field: 'priority' },
        { title: RA_STR.ruleScore, field: 'score' },
        { title: RA_STR.ruleAdvice, field: 'advice' },
        { title: RA_STR.ruleExpression, field: 'ruleExpression' },
        { title: RA_STR.ruleChannels, field: 'channels' },
        { title: RA_STR.ruleActions, field: 'actions' },
        { title: RA_STR.ruleMnemonic, field: 'ruleMnemonic' },
        { title: RA_STR.ruleDesc, field: 'description' },
        { title: RA_STR.ruleExemption, field: '' },
        { title: RA_STR.ruleQualifier, field: '' },
      ],
      enableOrg: true,
      showTable: false
    }
  }
  componentWillUnmount=()=>{
    this.props.activateErrorList(false, '');
  }
  componentDidMount = async () => {
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      this.setState({ orgOptions: orgOptions });
    }
  }
  getSelectedOrgDetails = async (getSelectedOrgDetails) => {
    const getRuleset = {
      method: 'GET',
      url: `${serverUrl}/riskanalytics/rulesetnames/${getSelectedOrgDetails}`
    };
    const rulesetOptions = await getService(getRuleset);
    if (rulesetOptions.status === 200) {
      this.setState({
        RulesetOptions: CA_Utils.objToArray(rulesetOptions.data.ruleNames, 'object'),
        selectedOrg: getSelectedOrgDetails
      })
    }

  }
  handleChange = (e) => {
    let activeOptions = [];
    let proposedOptions = [];
    if (e.target.type === 'radio') {
      if (e.target.value === 'ACTIVE') {
        activeOptions.push('ACTIVE');
      }
      else {
        proposedOptions.push('PROPOSED');
      }
      this.setState({ activeOptions: activeOptions, proposedOptions: proposedOptions });
    }
  }
  handleSelectChange = (e) => {
    this.setState({ selectedRuleset: e.target.selectedOptions[0].innerHTML ? e.target.selectedOptions[0].innerHTML :'' });
  }
  togglePage = () => {
    window.location.reload();
  }
  exportData = async () => {
    const exportReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL.exportRuleReport}`,
      data: {
        "configState": this.state.activeOptions.includes("ACTIVE") ? "ACTIVE" : "PROPOSED",
        "configurationName": this.state.selectedRuleset ? this.state.selectedRuleset : "Select",
        "orgName": this.state.selectedOrg
      }
    }
    const exportReportStatus = await getService(exportReport);
    if (exportReportStatus.status === 200) {
      this.downloadCSV(exportReportStatus);
    }
  }
  downloadCSV = (response) => {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Rules_Report.csv');
    }
  }
  displayReport = async () => {
    const showRuleReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL.showRuleReport}`,
      data: {
        "configState": this.state.activeOptions.includes("ACTIVE") ? "ACTIVE" : "PROPOSED",
        "configurationName": this.state.selectedRuleset ? this.state.selectedRuleset : "Select",
        "orgName": this.state.selectedOrg
      }
    }
    const showRuleReportStatus = await getService(showRuleReport);
    if (showRuleReportStatus.status === 200) {
      this.setState({ showTable: true, storeReportData: showRuleReportStatus.data });
      this.props.activateErrorList(false, '');
    } else {
      this.props.activateErrorList(true, showRuleReportStatus.data.errorList);
    }

  }
  render() {
    const { orgOptions, RulesetOptions, activeOptions, columns, proposedOptions, showTable, storeReportData } = this.state;
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.ruleHeader}</h2>
        {!showTable ?
          <div>
            <p className='desc'>{RA_STR.ruleDesc}</p>
            <hr />
            <div className='col-sm-6'>
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}</label>
                <div className="col-sm-8" >
                  <AutoSuggest orgOptions={orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={this.state.enableOrg} defaultOrganizationPlaceholder={RA_STR_SELECT} />
                </div>
              </div>
              <Select
                name={'ruleset'}
                title={'Ruleset Name'}
                options={RulesetOptions ? RulesetOptions : ['']}
                placeholder={RA_STR_SELECT}
                controlFunc={this.handleSelectChange} />
              <div className='row margin'>
                <div className='col-sm-4 no-padd'>
                  <RadioGroup
                    setName={'active'}
                    title={'ACTIVE'}
                    type={'radio'}
                    options={["ACTIVE"]}
                    selectedOptions={activeOptions}
                    controlFunc={this.handleChange}
                  />
                </div>
                <div className='col-sm-8 no-padd'>
                  <RadioGroup
                    setName={'proposed'}
                    title={'PROPOSED'}
                    type={'radio'}
                    options={["PROPOSED"]}
                    selectedOptions={proposedOptions}
                    controlFunc={this.handleChange}
                  />
                </div>
              </div>
            </div>
            <div className="form-group form-submit-button">
              <input className="secondary-btn" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
            </div>
          </div> :
          <div>
            <div className="col-md-12 row">
              <div className="col-md-9 no-padding">
                <p>{RA_STR.ruleConfigDesc}</p>
              </div>
              <div className="float-right col-md-3">
                <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.togglePage}></input>
              </div>
            </div>
            <div className="col-md-8">
              <div className="form-group form-inline">
                <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}</label>
                <div className="col-sm-8">
                  {storeReportData.organization}
                </div>
              </div>
              <div className="form-group form-inline">
                <label className="col-sm-4 col-form-label">{RA_STR.ruleState}</label>
                <div className="col-sm-8">
                  {storeReportData.state}
                </div>
              </div>
              <div className="form-group form-inline">
                <label className="col-sm-4 col-form-label">{RA_STR.ruleSet}</label>
                <div className="col-sm-8">
                  {storeReportData.ruleset}
                </div>
              </div>
              <div className="form-group form-inline">
                <label className="col-sm-4 col-form-label">{RA_STR.modelConfig}</label>
                <div className="col-sm-8">
                  {storeReportData.modelConfig}
                </div>
              </div>
            </div>
          </div>
        }
        {showTable ?
          <div className="tabele-buttons">
            <DynamicTable columns={columns} data={storeReportData.rulesReport} />
          </div> : ''}
      </div>
    )
  }
}

export default RuleConfigReport;
