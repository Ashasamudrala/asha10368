import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import MaterialTable from "material-table";
import { serverUrl, RA_API_URL ,LOADING_TEXT } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import Loader from '../../shared/utlities/loader';
import './BulkUploadActivityReport.scss';
import { saveAs } from 'file-saver';

class BulkUploadActivityReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
          showTable : false ,
          dataLoading : true ,
          apiData: [
          ],
            columns: [
                {
                  title: RA_STR.bulkreportviewtitlebatchId, field: RA_STR.bulkreportviewtitlebatchname, render: rowData => this.getActivityReportId(rowData) 
                },
                {
                  title: RA_STR.bulkreportviewtitlebatchType, field: RA_STR.bulkreportviewfieldbatchType, render: (rowData) =>
                  <div>{rowData.batchType}</div>
                },
                {
                    title: RA_STR.bulkreportviewtitleuploadedby, field: RA_STR.bulkreportviewfielduploadedby, render: (rowData) =>
                    <div>{rowData.uploadedBy}</div>
                },
                {
                    title: RA_STR.bulkreportviewtitleuploadedtimestamp, field: RA_STR.bulkreportviewfielduploadedtimestamp, render: (rowData) =>
                    <div>{rowData.dateFormatForUI}</div>
                  },
                  {
                    title: RA_STR.bulkreportviewtitlestatus, field: RA_STR.bulkreportviewfieldstatus, render: (rowData) =>
                    <div>{rowData.status}</div>
                  },
                  {
                    title: RA_STR.bulkreportviewtitleaction, field: RA_STR.bulkreportviewfieldaction, render: rowData =>
                      <div className="dynamic-form-input row">
                       <input className="secondary-btn ml-2
                       " id="" type="button" value="VIEW"  onClick={this.handleView.bind(this, rowData)}  ></input>
                       <input className="secondary-btn ml-2" id="" type="button" value="DOWNLOAD"  onClick={this.handleDownload.bind(this, rowData)} ></input>
                      </div>
                  }
            ]
        }

        this.data = this.data.bind(this);
    }

    handleView = (val, e) => {
      this.props.history.push({
        pathname: `/reports/ac/report-view`,
        state: {
        viewData :val
        }
      });
    }
    downloadCSV(response , fileType) {
      if (response && response.data) {
          const blob = new Blob([response.data], { type: 'application/octet-stream' });
          saveAs(blob, fileType);
      }
  }
    handleDownload = async(val , e) => {
      let fileType ;
      if(val.batchType === 'Administrator'){
        fileType = 'BulkUploadAdminReport.csv';
      }
      else {
        fileType = 'BulkUploadOrgReport.csv';
      }
      const getRequestIds = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL['getdownloadReport']}${val.batchID}`
      };
      const getRequestIdStatus = await getService(getRequestIds);
      if( getRequestIdStatus && getRequestIdStatus.status === 200 ) {
        this.downloadCSV(getRequestIdStatus , fileType);
      } 
    }
    
     getActivityReportId = (rowData) => {
        return <div className="orgName" onClick={() => this.data(rowData)}>{rowData.batchID}</div>
      }
      componentDidMount = async () => {
        const getRequestIds = {
          method: 'GET',
          url: `${serverUrl}${RA_API_URL['getBulkReportViews']}`
        };
        const getBulkActivityStatus = await getService(getRequestIds);
        if (getBulkActivityStatus && getBulkActivityStatus.status === 200) {
          this.setState({ apiData: getBulkActivityStatus.data.listBulkUploadBatchBeans , showTable: true , dataLoading : false});
        } 
        else if(getBulkActivityStatus && getBulkActivityStatus.status !== 200){
          this.setState({ showTable: false  , dataLoading : false});
        }
      }
      data = (rowData) => {
    this.props.history.push({
      pathname: `/reports/ac/report-view`,
      state: {
      viewData :rowData
      }
    }
    );
      }
    render() {
        return (
         <div>
           {this.state.dataLoading ? <div className='main'><Loader show={this.state.dataLoading } />{LOADING_TEXT} </div> :
            
        <div className="main bulk-report"> 
           <Loader show={this.state.dataLoading } />
            <h2 className="title">{RA_STR.bulkuploadActivityTitle}</h2>
            <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.bulkuploadactivitydescription }}></p>
            <hr/>
            {
        this.state.showTable ?
          <div  className="bulk-table table-height" >
            <MaterialTable
              columns={this.state.columns}
              data={this.state.apiData}
              options={{
                selection: false,
                paging: false,
                search: false,
                sorting: false,
              }}
            />
            </div>
             : <React.Fragment/>
            }
        </div>}
        </div>);
    }

}

export default BulkUploadActivityReport ;