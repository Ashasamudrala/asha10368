import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import MaterialTable from "material-table";
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages'; 
import { saveAs } from 'file-saver';
import './BulkUploadActivityReport.scss';

class BulkUploadActivityViewReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reportData : this.props.location.state.viewData,
            columns: [
                { title: RA_STR.bulkreportNo, field: RA_STR.bulkreporttitleNo },
                { title: RA_STR.bulkreportUserID, field: RA_STR.bulkreporttitleuserId},
                { title: RA_STR.bulkreportFirstName, field: RA_STR.bulkreporttitlefirstName},
                { title: RA_STR.bulkreportMiddleName, field: RA_STR.bulkreporttitlemiddleName},
                { title: RA_STR.bulkreportLastName, field: RA_STR.bulkreporttitlelastName },
                { title: RA_STR.bulkreportEmailAddr, field: RA_STR.bulkreporttitleemailAddr },
                { title: RA_STR.bulkreportTelephoneNo, field: RA_STR.bulkreporttitletelephoneNo },
                { title: RA_STR.bulkreportPassword, field: RA_STR.bulkreporttitlepassword },
                { title: RA_STR.bulkreportTargetOrgNames, field: RA_STR.bulkreporttitletargetOrgNames },
                { title: RA_STR.bulkreportRole, field: RA_STR.bulkreporttitlerole },
                { title: RA_STR.bulkreportOrgOfThisAdmin, field: RA_STR.bulkreporttitleorgOfThisAdmin  },
                { title: RA_STR.bulkreportCustomAttributes, field: RA_STR.bulkreporttitlecustomAttributes },
                { title: RA_STR.bulkreportStatus, field: RA_STR.bulkreporttitlestatus },
                { title: RA_STR.bulkreportReason, field: RA_STR.bulkreporttitlereason }

              ],
              orgColumns : [
                { title: RA_STR.bulkreporttitleorgno, field: RA_STR.bulkreportfieldsno },
                { title: RA_STR.bulkreporttitleorgOrgName, field: RA_STR.bulkreportfieldorgName },
                { title: RA_STR.bulkreporttitleorgOrgDisplayName, field: RA_STR.bulkreportfieldorgDisplayName },
                { title: RA_STR.bulkreporttitleorgCopyFromOrgName, field: RA_STR.bulkreportfieldcopyFromOrgName },
                { title: RA_STR.bulkreporttitleorgStatus, field: RA_STR.bulkreportfieldstatus},
                { title: RA_STR.bulkreporttitleorgReason, field: RA_STR.bulkreportfieldreason }
              ],
              apiData: [],
              showTable : false ,
              bulkUploadBatchBean : '',
              orgcolumn : false ,
              totalRecords : '' ,
              sucessCount : '',
              failureCount  : ''
        }}

        componentDidMount = async () =>{
            const getRequestIds = {
                method: 'GET',
                url: `${serverUrl}${RA_API_URL['getReportViews']}/${this.state.reportData.batchID}`
              };
              let listBulkdata ;
              const getRequestIdStatus = await getService(getRequestIds);
              if( getRequestIdStatus && getRequestIdStatus.status === 200 ) {
                if(getRequestIdStatus.data.listBulkUploadAdminsBeans !== null){
                getRequestIdStatus.data.listBulkUploadAdminsBeans.forEach(function (value , index) {
                    value['sno'] = index+1;
                  }); 
                  listBulkdata = getRequestIdStatus.data.listBulkUploadAdminsBeans
                  this.setState({ showTable: true, apiData: listBulkdata , bulkUploadBatchBean :getRequestIdStatus.data.bulkUploadBatchBean , orgcolumn : false ,totalRecords : getRequestIdStatus.data.totalRecords ,sucessCount : getRequestIdStatus.data.sucessCount ,failureCount  : getRequestIdStatus.data.failureCount});
                }
                  else if(getRequestIdStatus.data.listBulkUploadOrgBeans !== null) {
                    getRequestIdStatus.data.listBulkUploadOrgBeans.forEach(function (value , index) {
                        value['sno'] = index+1;
                      }); 
                      listBulkdata = getRequestIdStatus.data.listBulkUploadOrgBeans  
                      this.setState({ showTable: true, apiData: listBulkdata , bulkUploadBatchBean :getRequestIdStatus.data.bulkUploadBatchBean , orgcolumn : true ,totalRecords : getRequestIdStatus.data.totalRecords ,sucessCount : getRequestIdStatus.data.sucessCount ,failureCount  : getRequestIdStatus.data.failureCount });
                  }
              } 
        }
        downloadCSV(response) {
            const { orgcolumn} = this.state ;
            if (response && response.data) {
                const blob = new Blob([response.data], { type: 'application/octet-stream' });
                let fileType = orgcolumn ? 'BulkUploadOrgReport.csv' : 'BulkUploadAdminReport.csv'
                saveAs(blob, fileType);
            }
        }
        handleBack = () =>{
            this.props.history.push({
             pathname: `/reports/ac/bulk-upload-activity`});
        }
        handleDownload = async() => {
            const { reportData } = this.state ;
            const getRequestIds = {
              method: 'GET',
              url: `${serverUrl}${RA_API_URL['getdownloadReport']}${reportData.batchID}`
            };
            const getRequestIdStatus = await getService(getRequestIds);
            if( getRequestIdStatus && getRequestIdStatus.status === 200 ) {
              this.downloadCSV(getRequestIdStatus);
            } 
          }

        render(){
            const { bulkUploadBatchBean , orgcolumn , totalRecords  ,sucessCount , failureCount } = this.state ;
            let columns = orgcolumn ? this.state.orgColumns : this.state.columns
            return <div className='main bulk-report'> 
            <h2 className="title">{RA_STR.bulkuploadActivityTitle}</h2>
            <hr className='report-hr'/>
            <div className='report-row-data'>
            <div className='ml-2 text-center'>{bulkUploadBatchBean.batchID}</div>
            <div className='text-center ml-2'> |<span className = 'bulkupload-space'>{bulkUploadBatchBean.batchType}</span> </div>
            <div className='text-center ml-2 '>| <span className = 'bulkupload-space'>{bulkUploadBatchBean.uploadedBy}</span></div>
            <div className='text-center ml-2'>| <span className = 'bulkupload-space'>{bulkUploadBatchBean.dateFormatForUI}</span></div>
            <div className='text-center ml-2 '>| <span className = 'bulkupload-space'>TOTAL RECORDS :{totalRecords}</span></div>
            <div className='text-center ml-2  report-success-color'> | <span className = 'bulkupload-space'>SUCCESS :{sucessCount}</span></div>
            <div className='text-center ml-2  report-failed-color'> | <span className = 'bulkupload-space'>FAILED :{failureCount}</span></div>
            </div>
            
            <hr className='report-hr'/>
            {
        this.state.showTable ?
          <div className='bulk-report-table'>
            <MaterialTable
              columns={columns}
              data={this.state.apiData}
              options={{
                selection: false,
                paging: false,
                search: false,
                sorting: false,
              }}
            />
            <div className="bulk-report-button mt-4">
                 <input className="secondary-btn "  type="button" value="BACK" onClick={this.handleBack}
                  ></input>
                 <input className="secondary-btn ml-4"  type="button" value="DOWNLOAD"  onClick={this.handleDownload}></input>
            </div>
            </div> : <React.Fragment/>
        }
        </div>
        }
}
export default BulkUploadActivityViewReport;