import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
var moment = require('moment');

class DateTimeRangeView extends Component {
  constructor() {
    super();
    this.state = {
      startDate: '',
      endDate: '',
      fromHours: '00',
      fromMinutes: '00',
      toHours: '00',
      toMinutes: '00',
      disableInput: false
    }
  }
  componentDidMount = () => {
    var date1 = new Date();
    var date2 = new Date();
    var startDaysPrior = this.props.startPrior;
    var endDaysPrior = this.props.endPrior;
    date1.setDate(date1.getDate() - startDaysPrior);
    date2.setDate(date2.getDate() - endDaysPrior);
    this.setState({startDate:date1,endDate:date2});
  }
  startDateChange = (date) => {
    this.setState({
      startDate: date
    });
  }
  endDateChange = (date) => {
    this.setState({
      endDate: date
    });
  }
  getDatesTimes() {
    const { fromHours, fromMinutes, toHours, toMinutes } = this.state;
    let groupDates = [];
    var obj = {
      startDate: moment(this.state.startDate).format('L'),
      endDate: moment(this.state.endDate).format('L'),
      startHours: fromHours,
      endHours: toHours,
      startMins: fromMinutes,
      endMins: toMinutes
    }
    groupDates.push(obj);
    return groupDates
  }
  handleSelectChanges = (e) => {
    if (e.target.className === 'from-hours') {
      this.setState({ fromHours: e.target.value });
    } else if (e.target.className === 'from-minutes') {
      this.setState({ fromMinutes: e.target.value });
    }
    else if (e.target.className === 'to-hours') {
      this.setState({ toHours: e.target.value });
    }
    else if (e.target.className === 'to-minutes') {
      this.setState({ toMinutes: e.target.value });
    }
  }
  render() {
    let { startDate, endDate, disableInput } = this.state;
    var hours = '', minutes = '';
    for (var i = 0; i < 60; i += 5) {
      if (i < 10) {
        minutes += "<option value = '0" + i + "' >0" + i + "</option>";
      } else {
        minutes += "<option value = '" + i + "' >" + i + "</option>";
      }

    }
    for (var j = 0; j < 24; j++) {
      if (j < 10) {
        hours += "<option value = '0" + j + "' >0" + j + "</option>";
      } else {
        hours += "<option value = '" + j + "' >" + j + "</option>";
      }

    }
    return <div className="col-sm-10">
        <label className="col-form-label">From (MM/dd/yyyy)</label>
        <DatePicker
          selected={startDate}
          todayButton="today"
          onChange={this.startDateChange}
          showMonthDropdown={true}
          showYearDropdown={true}
          scrollableYearDropdown={false}
          disabled={disableInput}
          dropdownMode="select"
          className="form-control startDate"
          ref={input => this.inputElement = input}
        /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind"/>
        <select className="from-hours" dangerouslySetInnerHTML={{ __html: hours }} onChange={this.handleSelectChanges}></select>
        <select className="from-minutes" dangerouslySetInnerHTML={{ __html: minutes }} onChange={this.handleSelectChanges}></select>
        <label className="col-form-label">To (MM/dd/yyyy)</label>
        <DatePicker
          selected={endDate}
          todayButton="today"
          onChange={this.endDateChange}
          showMonthDropdown={true}
          showYearDropdown={true}
          scrollableYearDropdown={false}
          disabled={disableInput}
          dropdownMode="select"
          className="form-control endDate"
        /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind"/>
        <select className="to-hours" dangerouslySetInnerHTML={{ __html: hours }} onChange={this.handleSelectChanges}></select>
        <select className="to-minutes" dangerouslySetInnerHTML={{ __html: minutes }} onChange={this.handleSelectChanges}></select>
    </div>;
  }
}

export default DateTimeRangeView;
