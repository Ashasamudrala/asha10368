import React, { Component } from 'react';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { RA_STR } from '../../shared/utlities/messages';
import { saveAs } from 'file-saver';
import { serverUrl, RA_API_URL ,  RA_STR_SELECT} from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
class CaseStatusReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayList: this.props.location.state.data,
            columns: [
                { title: RA_STR.casestatusreviewer, field: 'reviewer' },
                { title: RA_STR.casestatusorganization, field: 'organization' },
                { title: RA_STR.casestatuscasesskipped, field: 'casesSkipped' },
                { title: RA_STR.casestatuscasesclosed, field: 'casesClosed' },
                { title: RA_STR.casestatuscasesopen, field: 'casesOpen' },
                { title: RA_STR.casestatuscasesonhold, field: 'casesOnHold' },
                { title: RA_STR.casestatuscasestimedout, field: 'casesTimedOut' },
                { title: RA_STR.casestatuscasesworked, field: 'casesWorked' },
                { title: RA_STR.casestatusuniquecaseshandled, field: 'uniqueCases' },

            ]

        }
    }

    downloadCSV = (response) => {
        const { displayList } = this.state;
        if (response && response.data) {
          const blob = new Blob([response.data], { type: 'application/octet-stream' });
          saveAs(blob, `${RA_STR.casestatusfileconst}${displayList.fromDate}${displayList.toDate}.csv`);
        }
    }

    exportData = async () => {
        const { displayList } = this.state;
        const exportReport = {
          method: 'POST',
          url: `${serverUrl}${RA_API_URL['getReviewerEfficiencyCaseStatusReport']}/export`,
          data: {
            "fromDate": displayList.fromDate,
            "orgName": displayList.orgName,
            "toDate": displayList.toDate
          }
        }
        const exportReportStatus = await getService(exportReport);
        if (exportReportStatus.status === 200) {
          this.downloadCSV(exportReportStatus);
        }
      }

    getDetailReport = async () => {
        const { displayList } = this.state;
        const basePath = this.props.location.state.basePath ;
        const routeToRedirect = '/reviewer-efficiency-report-view' ;
        const data = {
            "decryptionNeeded": true,
            "fromDate": displayList.fromDate,
            "orgName": displayList.orgName,
            "pageNo": 0,
            "pageSize": 10,
            "toDate": displayList.toDate
          }
          const detailedReport = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['getReviewerEfficiencyCaseStatusDetailedReport']}`,
            data: data
          }
          const detailedReportStatus = await getService(detailedReport);
          if( detailedReportStatus.status === 200 ) {
            this.props.history.push({
                pathname:`${basePath}${routeToRedirect}`,
                state: {
                    data: detailedReportStatus.data ,
                    basePath : basePath
                }
            })
          }
         
    }
    redirectToHelp = () => {
        const basePath = this.props.location.state.basePath ;
        this.props.history.push({
          pathname: `${basePath}`,
        })
      }

    render() {
        const { displayList , columns} = this.state;
        return (<div className='main'>
            <div className="col-md-12 row">
                <div className="col-md-8 ">
                    <h2 className='title'>{RA_STR.caseefficiencyreporttitile}</h2>
                    <p>{RA_STR.caseefficiencyreportdesc}</p>
                </div>
                <div className="float-right col-md-4">
                    <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                    <input className="secondary-btn ml-2" id="EXPORT" type="submit" value="NEW REPORT" onClick={this.redirectToHelp}></input>
                    <input className="secondary-btn ml-3" id="detailReport" type="submit" value="DETAIL REPORT" onClick={this.getDetailReport}></input>
                </div>
            </div>
            <div className="col-md-8">
                <div className="form-group form-inline">
                    <label className="col-sm-4 col-form-label">{RA_STR.noOfRecords}</label>
                    <div className="col-sm-8">
                        {displayList.numberOfRecords}
                    </div>
                </div>
                <div className="form-group form-inline">
                    <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}:</label>
                    <div className="col-sm-8">
                        {displayList.orgName}
                    </div>
                </div>
                <div className="form-group form-inline">
                    <label className="col-sm-4 col-form-label">Date Range:</label>
                    <label className="col-sm-4 col-form-label ml-3">From:{displayList.fromDate}</label>
                    <div className="col-sm-3">
                        To:{displayList.toDate}
                    </div>
                </div>
            </div>
            <Checkbox
                type={'checkbox'}
                setName="scopeAll"
                controlFunc={this._handleChange}
                options={["Decrypt Sensitive Information"]}
                selectedOptions={['Decrypt Sensitive Information']}
            />

            <div className="tabele-buttons">
                <DynamicTable columns={columns} data={displayList.reportList} />
            </div>
        </div>)
    }
}

export default CaseStatusReport;