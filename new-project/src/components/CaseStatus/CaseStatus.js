import React, { Component } from 'react';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import DateTimeRangeView from '../CaseStatus/DateTimeRangeView';


class CaseStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgName: '',
      orgOptions: [],
      enableOrg: true,
      showTable: false,
      showDates: false,
      displayList: []
    }
  }
  getSelectedOrgDetails = (getSelectedOrgDetails) => {
    this.setState({
      orgName: getSelectedOrgDetails,
      showDates: true
    })
  }
  componentDidMount = async () => {
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      this.setState({ orgOptions: orgOptions });
    }
  }

  displayReport = async () => {
    var getDateItems = this.refs.datetimerangelist.getDatesTimes();
    if (getDateItems[0].startDate <= getDateItems[0].endDate) {
      const displayReport = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['getReviewerEfficiencyCaseStatusReport']}`,
        data: {
          "fromDate": `${getDateItems[0].startDate} ${getDateItems[0].startHours}:${getDateItems[0].startMins}:00`,
          "orgName": this.state.orgName ? this.state.orgName : '',
          "toDate": `${getDateItems[0].endDate} ${getDateItems[0].endHours}:${getDateItems[0].endMins}:00`
        }
      }
      const displayReportStatus = await getService(displayReport);
      const routeToRedirect = '/reviewer-efficiency-report' ;
      const basePath = this.props.location.pathname ;
      if (displayReportStatus.status === 200) {
        this.props.history.push({
          pathname:  `${basePath}${routeToRedirect}`,
          state: {
            data: displayReportStatus.data ,
            basePath : basePath
          }
        })
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, displayReportStatus.data.errorList);
      }
    } else {
      alert(RA_STR.ruleAlert);
    }
  }


  render() {
    const { showDates, orgOptions, enableOrg } = this.state;
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.casestatustitle}</h2>
        <p className='desc'>{RA_STR.casestatusdesc}</p>
        <hr />
        <div className="col-md-6">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.casestatusorglabel}</label>
            <div className='col-sm-8'>
              <AutoSuggest orgOptions={orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={enableOrg} defaultOrganizationPlaceholder={RA_STR_SELECT} />
            </div>
          </div>
        </div>

        {showDates ? <div className = 'row ml-1'>
          <label className="col-sm-2 col-form-label">{RA_STR.casestatusdatarange}</label>
          <DateTimeRangeView ref="datetimerangelist" startPrior={'1'} endPrior={'0'}></DateTimeRangeView>
        
          <input className="secondary-btn" id="RESET" type="submit" value="Display Reports" onClick={this.displayReport}></input></div> : ''}

      </div>
    )
  }
}

export default CaseStatus;
