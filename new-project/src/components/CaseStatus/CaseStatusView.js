import React, { Component } from 'react';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { RA_STR } from '../../shared/utlities/messages';
import { saveAs } from 'file-saver';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
class CaseStatusReportView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayList: this.props.location.state.data,
            pageNo: 0,
            activePageNum: 1,
            pageSize: 10,
            currentPageNo: 1,
            totalCount: this.props.location.state.data.numberOfRecords,
            columns: [
                { title: RA_STR.casestatusreviewer, field: 'reviewer' },
                { title: RA_STR.casestatusorganization, field: 'organization' },
                { title: RA_STR.viewcasestatus, field: 'caseStatus' },
                { title: RA_STR.casestatusid, field: 'caseId' },
                { title: RA_STR.casestatususername, field: 'username'},
                { title: RA_STR.casestatusdatetime, field: 'dateTime' }
            ]

        }
    }

    downloadCSV = (response) => {
        const { displayList } = this.state;
        if (response && response.data) {
          const blob = new Blob([response.data], { type: 'application/octet-stream' });
          saveAs(blob, `${RA_STR.casestatusfileconst}${displayList.fromDate}${displayList.toDate}.csv`);
        }
    }

    exportData = async () => {
        const { displayList } = this.state;
        const exportReport = {
          method: 'POST',
          url:  `${serverUrl}${RA_API_URL['exportReviewerEfficiencyCaseStatusDetailedReport']}`,
          data: 
          {
            "decryptionNeeded": true,
            "fromDate": displayList.fromDate,
            "orgName": displayList.orgName,
            "pageNo": 0,
            "pageSize": 10,
            "toDate": displayList.toDate
          }
        }
        const exportReportStatus = await getService(exportReport);
        if (exportReportStatus.status === 200) {
          this.downloadCSV(exportReportStatus);
        }
      }
      redirectToHelp = () => {
        const basePath = this.props.location.state.basePath ;
        this.props.history.push({
          pathname: `${basePath}`,
        })
      }
    getDetailReport = async () => {
        const { displayList ,pageNo , pageSize } = this.state;
        const data = {
            "decryptionNeeded": true,
            "fromDate": displayList.fromDate,
            "orgName": displayList.orgName,
            "pageNo": pageNo,
            "pageSize": pageSize,
            "toDate": displayList.toDate
          }
          const detailedReport = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['getReviewerEfficiencyCaseStatusDetailedReport']}`,
            data: data
          }
          const detailReportStatus = await getService(detailedReport);
          if( detailReportStatus && detailReportStatus.status === 200 ) {
              this.setState({displayList : detailReportStatus.data})
          }
    }

    getActivePage = (getPageNo) => {
        let pageCount = getPageNo - 1;
        this.setState({
            pageNo: pageCount,
            currentPageNo: getPageNo,

        }, () => {
            this.getDetailReport();
        })
    }

    render() {
        console.log(this.state);
        const { displayList , columns , totalCount} = this.state;
        return (<div className='main'>
            <div className="col-md-12 row">
                <div className="col-md-8 ">
                    <h2 className='title'>{RA_STR.caseefficiencyreporttitile}</h2>
                    <p>{RA_STR.caseefficiencyreportdesc}</p>
                </div>
                <div className="float-right col-md-4">
                    <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                    <input className="secondary-btn ml-2" id="EXPORT" type="submit" value="NEW REPORT" onClick={this.redirectToHelp}></input>
                </div>
            </div>
            <div className="col-md-8">
                <div className="form-group form-inline">
                    <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}:</label>
                    <div className="col-sm-8">
                        {displayList.orgName}
                    </div>
                </div>
                <div className="form-group form-inline">
                    <label className="col-sm-4 col-form-label">Date Range:</label>
                    <label className="col-sm-4 col-form-label ml-3">From:{displayList.fromDate}</label>
                    <div className="col-sm-3">
                        To:{displayList.toDate}
                    </div>
                </div>
            </div>
            <div className="tabele-buttons">
                <DynamicTable columns={columns} data={displayList.reportList} enablePagination={true} totalCount={totalCount} pageSize={this.state.pageSize} activePageNumNew={this.state.currentPageNo}
                    activePage={this.getActivePage}   />
            </div>
        </div>)
    }
}

export default CaseStatusReportView;