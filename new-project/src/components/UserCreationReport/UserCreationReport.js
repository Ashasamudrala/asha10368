import React, { Component, Fragment } from 'react';
import AdminReport from '../Reports-admin/adminReport';
import { RA_API_URL, serverUrl } from '../../shared/utlities/constants';

class UserCreationReport extends Component {
  componentWillMount() {
    this.columns = [
      { title: 'Date Created', field: 'dateCreated' },
      { title: 'User ID', field: 'userId' },
      { title: 'Organization', field: 'organization' },
      { title: 'User Status', field: 'userStatus' },
      { title: 'First Name', field: 'fristName' },
      { title: 'Middle Name', field: 'middleName' },
      { title: 'Last Name', field: 'lastName' },
      { title: 'Email Address', field: 'emailAddrs' },
      { title: 'Telephone Number', field: 'telePhoneNumber' },
      // { title: 'From Date', field: 'fromDate' },
      // { title: 'To Date', field: 'toDate' },
      // { title: 'No of Records', field: 'noOfRecords' }
    ];
    this.displayReportText = 'This report displays the details of users created in the system.';
  }
  render() {
    return <Fragment>
      <AdminReport reportType="user-creation-report" columns={this.columns} displayreportUrl={serverUrl + RA_API_URL.userCreationReport} exportreportUrl={serverUrl + RA_API_URL.exportUserCreationReportInCSV} displayReportText={this.displayReportText}></ AdminReport>
    </Fragment>

  }
}

export default UserCreationReport;
