import React, { Component } from 'react';
import './FraudStaticsReport.scss';

import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';

import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';

import DateRange from '../../shared/components/DateRange/DateRange';
import CA_Utils from '../../shared/utlities/commonUtils';

import { saveAs } from 'file-saver';

import C3Chart from 'react-c3js';
import 'c3/c3.css';

class FraudStaticsReport extends Component {

  constructor(props) {
    super(props);
    this.state = {
      defaultOrganizationNme: RA_STR_SELECT,
      organizationNames: [],
      originalOrganizations: [],
      defaultChannel: 'All',
      channelList: [],
      selectedChannels: '',
      fromDate: '',
      toDate: '',
      showTable: false,
      showGraph: false,
      fraudList: [],
      columns: [
        { title: 'Risk Advice', field: 'adviceId', render: (rowData) => <span className={rowData.adviceId}>{rowData.adviceId}</span> },
        { title: 'Fraud', field: 'countFraud' },
        { title: 'Genuine', field: 'countAssumedGenuine' },
        { title: 'Undetermined', field: 'countUnknown' },
        { title: 'Total', field: 'total' }
      ],
      data: {
        columns: [],
        type: 'bar',
      },
      grid: {
        x: {
          show: true
        },
        y: {
          show: true
        }
      },
      mountNode: document.getElementById('react-c3js')
    }
  }
  componentDidMount() {
    this.getAllOrganizations();
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  getAllOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };

  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find((element) => { if (element.content === value) return element.key });
      this.setState({
        defaultOrganizationNme: orgId.key
      })
      this.getChannels(orgId.key);
    }
  }

  getChannels = async (selectedOrg) => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['common']}/${selectedOrg}${RA_API_URL['channels']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      const channelList = [{
        content: 'All Channels',
        key: 'All'
      }];
      response.data.forEach((obj) => {
        channelList.push({
          'content': obj.displayName,
          'key': obj.name
        })
      })
      this.setState({ channelList: channelList });
    }
  }

  onChannelSelect = (event) => {
    const channel = event.target.value;
    this.setState({ defaultChannel: channel });
  }

  displayReport = async () => {
    let allChannels = '';
    if (this.state.defaultChannel === 'All') {
      for (let i = 1; i < this.state.channelList.length; i++) {
        allChannels += this.state.channelList[i].content + ', ';
      }
      this.setState({ selectedChannels: allChannels.slice(0, -2) });
    }
    else {
      this.setState({ selectedChannels: this.state.defaultChannel });
    }
    const getDateItems = this.refs.daterange.getDates();
    this.setState({ fromDate: getDateItems[0].startDate, toDate: getDateItems[0].endDate });
    const payload = {
      orgName: this.state.defaultOrganizationNme,
      selectedChannel: this.state.defaultChannel,
      fromDate: getDateItems[0].startDate,
      toDate: getDateItems[0].endDate
    }
    const url = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['caseFraudstatistics']}`,
      data: payload
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      const fraudStatisticsReport = response.data.fraudStatisticsReport || [];
      this.setState({ fraudList: fraudStatisticsReport, showTable: true });
      if (fraudStatisticsReport.length) {
        let renderData = this.state.data;
        const tempArray = [['Alert'], ['Increase Authentication'], ['Allow'], ['Deny']];
        fraudStatisticsReport.forEach((obj, index) => {
          tempArray[index].push(obj.countFraud);
          tempArray[index].push(obj.countAssumedGenuine);
          tempArray[index].push(obj.countUnknown);
          tempArray[index].push(obj.total);
        })
        renderData.columns = tempArray;
        this.setState({ data: renderData });
        this.props.activateSuccessList(true, response.data);
        this.props.activateErrorList(false, '');
      }
    } else {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }

  export = async () => {
    const payload = {
      orgName: this.state.defaultOrganizationNme,
      selectedChannel: this.state.defaultChannel,
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
    }

    const url = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['caseFraudstatisticsExport']}`,
      data: payload
    };
    const response = await getService(url);
    const blob = new Blob([response.data], { type: 'application/octet-stream' });
    saveAs(blob, 'FraudStatistics.csv');
  }

  newReport = async () => {
    this.setState({ showTable: false, showGraph: false });
  }

  visualiseReport = async () => {
    this.state.showGraph = !this.state.showGraph;
    this.setState({ showGraph: this.state.showGraph });
  }


  render() {
    const { organizationNames, defaultOrganizationNme, channelList, defaultChannel, selectedChannels, fromDate, toDate, showTable, showGraph, columns, fraudList, mountNode, data, grid } = this.state;

    return (
      <div className='main case-fraud-report-container'>
        <h2 className='title'>Fraud Statistics Report</h2>
        {!showTable ?
          <div>
            <p className='desc'>This report provides statistics on Risk Advice vs. Fraud Status. Specify the criteria to generate the report.</p>
            <hr />
          </div>
          : <div className="form-group form-submit-button row">
            <div className="col-sm-8">
              <p className='desc'>This report provides statistics on Risk Advice vs. Fraud Status, based on the GMT time zone.</p>
            </div>
            <div className="col-sm-4">
              <input className="secondary-btn" type="submit" value="EXPORT" onClick={this.export}></input>
              <input className="secondary-btn ml-3" type="submit" value="NEW REPORT" onClick={this.newReport}></input>
            </div>
          </div>}

        {!showTable ?
          <div className='col-sm-8'>
            <div className="form-group dynamic-form-select">
              <div className="form-group row">
                <label className="col-sm-4 col-form-label">Organization Name:</label>
                <div className='col-sm-8'>
                  <AutoSuggest orgOptions={organizationNames} getSelectedOrgDetails={this.onOrgSelect} enableAutosuggest={true} defaultOrganizationPlaceholder={defaultOrganizationNme} />
                </div>
              </div>
            </div>
          </div> : ''}

        {(defaultOrganizationNme !== '--Select--' && !showTable) ?
          <div>
            <div className='col-sm-8'>
              <Select
                name={'channel'}
                title={'Select Channel'}
                required={true}
                selectedOption={defaultChannel}
                // placeholder={'-All Channels-'}
                options={channelList ? channelList : ['']}
                controlFunc={this.onChannelSelect} />
            </div>

            <div className="row">
              <div className="col-sm-3">
                <label className="col-sm-8 col-form-label mt-3"> {RA_STR.timePeriod} </label>
              </div>
              <DateRange ref="daterange"></DateRange>
            </div>

            <div className="form-group form-submit-button row">
              <input className="secondary-btn" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
            </div>
          </div> : ''}

        {showTable ?
          <div>
            <table>
              <tbody>
                <tr className="row">
                  <td className="col-sm-4">{RA_STR.FRAUD_STAT_REPORT.ORGANIZATION_LABEL}</td>
                  <td className="col-sm-8"> {defaultOrganizationNme} </td>
                </tr>
                <tr className="row">
                  <td className="col-sm-4"> {RA_STR.FRAUD_STAT_REPORT.CHANNEL} </td>
                  <td className="col-sm-8"> {selectedChannels} </td>
                </tr>
                <tr className="row">
                  <td className="col-sm-4">{RA_STR.timePeriod}:</td>
                  <td className="col-sm-8"> {RA_STR.FRAUD_STAT_REPORT.DATRE_BY_RANGE} </td>
                </tr>
                <tr className="row">
                  <td className="col-sm-4">{RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL} {fromDate}</td>
                  <td className="col-sm-8">{RA_STR.EXCEPTION_USER_REPORT.TO_LABEL} {toDate}</td>
                </tr>
              </tbody>
            </table>
            <div className="tabele-buttons">
              <DynamicTable columns={columns} data={fraudList} enablePagination={false} selection={false} />
            </div>
            <div className="form-group form-submit-button row">
              <input className="secondary-btn" type="submit" value={!showGraph ? "VISUALISE REPORT" : "HIDE CHART"} onClick={this.visualiseReport}></input>
            </div>
          </div> : ''}
        {showGraph ? <C3Chart data={data} grid={grid} /> : ''}

      </div >
    )
  }
}

export default FraudStaticsReport;
