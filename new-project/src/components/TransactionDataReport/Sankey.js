import React, { Fragment } from "react";
import * as d3 from "d3";
import { sankey, sankeyLinkHorizontal } from "d3-sankey";

const SankeyNode = ({ name, x0, x1, y0, y1, color }) => (
    <Fragment>
    <rect x={x0} y={y0} width={x1 - x0} height={y1 - y0} fill={color}>
      <title>Transaction Count</title>
    </rect>
    <text x={x1} y={y1-30} >{name}</text>
    </Fragment>
);


const SankeyLink = ({ link, color }) => (
  <path
    d={sankeyLinkHorizontal()(link)}
    style={{
      fill: "none",
      strokeOpacity: ".3",
      stroke: color,
      strokeWidth: Math.max(1, link.width)
    }}
  />
);

const Sankey = ({ data, width, height }) => {
  const { nodes, links } = sankey()
    .nodeWidth(25)
    .nodePadding(10)
    .extent([[1, 1], [width - 1, height - 5]])(data);
  // const colorScale = d3
  //   .scaleLinear()
  //   .domain([0, nodes.length])
  //   .range([0, 1]);

  return (
    <g style={{ mixBlendMode: "multiply" ,fontSize:15,fontWeight:'bold'}}>
      {nodes.map((node, i) => (
        <SankeyNode
          {...node}
          color={'#afbacc'}
          key={node.name}
        />
      ))}
      {links.map((link, i) => (
        <SankeyLink
          link={link}
        //   color={color(colorScale(link.source.index)).hex()}
        color={'#d3d9e3'}
        onmouseover={link.value}
        // onmouseover={sankeyTooltip()}
        />
      ))}
      
    </g>
  );
};

export default Sankey;
