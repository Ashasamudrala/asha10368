// npm dependencies
import React, { Component } from 'react';
import { saveAs } from 'file-saver';

import C3Chart from 'react-c3js';
import c3 from 'c3';
import 'c3/c3.css';

import * as d3 from 'd3';
import Sankey from './Sankey';

// Service imports
import { getService } from '../../shared/utlities/RestAPI';

// Component dependencies
import Select from '../../shared/components/Select/Select';

// Internal Dependency imports
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl, RA_API_URL, RA_API_STATUS } from '../../shared/utlities/constants';

// style imports
import './TransactionDataReport.scss';
import { applyMiddleware } from 'redux';
var moment = require('moment');

/**
 * Class component: This component is for displaying and exporting transaction report data.
 */
class TransactionDataReport extends Component {
  /**
   * Constructor for AssociateEndPoint Component
   * @class
   * @param 			{Object} 	props		Initialization props
   * @description		It initialises the state, from props
   */
  constructor(props) {
    super(props);
    const self = this;
    // initializing the local state
    this.state = {
      // Variables for storing form values
      orgName: '',
      // Variables for storing dropdown data
      organizationData: [],
      channelData: [],
      currencyData: [],
      downloadTypeOptions: [
        { key: 'null', content: 'Select' },
        { key: 'img', content: 'img' },
        { key: 'csv', content: 'csv' }
      ],
      dateRangeOptions: [],
      yearOptions: [],
      monthOptions: [
        { key: '1', content: 'Jan' },
        { key: '2', content: 'Feb' },
        { key: '3', content: 'Mar' },
        { key: '4', content: 'Apr' },
        { key: '5', content: 'May' },
        { key: '6', content: 'Jun' },
        { key: '7', content: 'Jul' },
        { key: '8', content: 'Aug' },
        { key: '9', content: 'Sep' },
        { key: '10', content: 'Oct' },
        { key: '11', content: 'Nov' },
        { key: '12', content: 'Dec' }
      ],
      startDate: new Date(),
      endDate: new Date(),
      disableInput: false,
      selectedDateRange: '',
      startMonthData: '',
      startYearData: '',
      endMonthData: '',
      endYearData: '',
      currency: '',
      channelName: '',
      downloadType: 'null',
      sankeyData: [],
      sankeyGraphData: {
        nodes: [],
        links: []
      },
      riskJson: [],
      sourceNode: [],
      destNode: [],
      mainNodes: [],

      //   Bar Graph Data
      bardata: {
        x: 'x',
        columns: [],

        axes: {
          // For getting two y-axis
          'Transaction Count': 'y',
          'Transaction Amount (USD)': 'y2'
        },

        type: 'bar',

        onclick: function(d, element) {
          // 'd':gives click element properties ,'internal.config.axis_x_categories[d.x]' : gives selected month
          self.graphRender(d, this.internal.config.axis_x_categories[d.x]);
        }
      },
      baraxis: {
        x: {
          type: 'category'
        },
        y2: {
          show: true,
          tick: {
            format: d3.format('.2s') // For formatting numbers
          },
          label: {
            text: 'Transaction Amount (USD)',
            position: 'outer-middle'
          }
        },
        y: {
          label: {
            text: 'Transaction Count',
            position: 'outer-middle'
          }
        }
      },
      bargrid: {
        x: {
          show: true
        },
        y: {
          show: true
        }
      },
      Wbar: {
        width: 50
      },
      barcolor: {
        pattern: ['#507696', '#69d3c6']
      },
      // To get 'id' of onClick Element of legend
      legend: {
        item: {
          onclick: function(id) {
            self.graphRender(id, '', '');
          }
        }
      },

      //   Risk Advice Count Graph
      riskDataCount: {
        columns: [],
        type: 'donut'
      },
      donut: {
        title: '',
        label: {
          position: 'absolute',
          format: function(value, ratio, id) {
            return value;
          }
        },
        width: 40
      },
      riskAdviceCountColor: {
        pattern: ['#57c1b4', '#db4332', '#607d8b', '#30aac7']
      },
      // Tx status Count Graph
      txDataCount: {
        columns: [],
        type: 'donut'
      },
      txAdviceCountColor: {
        pattern: ['#57c1b4', '#607d8b', '#30aac7', '#61c6e5', '#c4d8e8', '#db4332']
      },

      // Fraud Count Graph

      fraudDataCount: {
        columns: [],
        type: 'donut'
      },
      fraudAdviceCountColor: {
        pattern: ['#57c1b4', '#607d8b', '#30aac7', '#61c6e5', '#db4332']
      },

      sankeyData: [],
      showGraph: false,
      data: null,
      width: 0,
      height: 0,
      graphArg: {},
      month: '',
      graphData: []
    };
  }
  // For Sankey Graph SVG initialization
  svgRef = React.createRef();

  /**
   * componentDidMount is invoked immediately after this component is mounted, so all initialization goes here.
   * @description   This method contains the get call for categories.
   */
  componentDidMount() {
    this.getOrganizations();
    this.getTimeDateRange();
    let date = new Date();
    let result = [];
    const currentYear = {
      key: date.getFullYear(),
      content: date.getFullYear()
    };
    const lastYear = {
      key: date.getFullYear() - 1,
      content: date.getFullYear() - 1
    };
    result.push(currentYear);
    result.push(lastYear);
    this.setState({
      yearOptions: result,
      startMonthData: date.getMonth() + 1,
      startYearData: date.getFullYear(),
      endMonthData: date.getMonth() + 1,
      endYearData: date.getFullYear()
    });
  }

  /**
   * componentWillUnmount is invoked immediately before a component is unmounted and destroyed.
   * @description   This method takes back the error/success message to initial state.
   */
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    window.removeEventListener('resize', this.measureSVG);
  }

  measureSVG = () => {
    const { width, height } = this.svgRef.current.getBoundingClientRect();
    this.setState({
      width,
      height
    });
  };

  visualiseReport = async () => {
    this.state.showGraph = true;
    this.setState({ showGraph: this.state.showGraph });
  };

  /**
   * Handler method to handle user input.
   * @param 	{*}		e 	The HTML event generated by the form.
   * @description		It handles the change in dropdown values.
   */
  handleChange = e => {
    if (e.target.name === 'orgName') {
      this.setState({ orgName: e.target.value });
      if (e.target.value) {
        const data = [...this.state.organizationData];
        data.forEach(element => {
          if (e.target.value == element.key) {
            this.getChannels(element.content);
            this.getCurrency(element.content);
          }
        });
      }
    } else if (e.target.name === 'channelName') {
      this.setState({
        channelName: e.target.value
      });
    } else if (e.target.name === 'startYear') {
      this.setState({
        startYearData: e.target.value
      });
    } else if (e.target.name === 'startMonth') {
      this.setState({
        startMonthData: e.target.value
      });
    } else if (e.target.name === 'endYear') {
      this.setState({
        endYearData: e.target.value
      });
    } else if (e.target.name === 'endMonth') {
      this.setState({
        endMonthData: e.target.value
      });
    } else if (e.target.name === 'currencyName') {
      this.setState({
        currency: e.target.value
      });
    }
  };

  /**
   * @description   This method gets all the categories.
   */
  getOrganizations = async () => {
    // Variable that contains method,url declaration that goes to backend.
    const getDefaultOrganizations = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getDefaultOrgs']}`
    };
    // Catching backend response from getService method.
    const response = await getService(getDefaultOrganizations);
    if (response && response.status === RA_API_STATUS['200']) {
      // Coverts the object to array.
      const result = CA_Utils.objToArray(response.data.organizations, 'object');
      this.getChannels(result[0].content);
      this.getCurrency(result[0].content);
      // Updating the local state.
      this.setState({ organizationData: result, orgName: result[0].key });
    }
  };

  /**
   * @description   This method gets all the categories.
   */
  getTimeDateRange = async () => {
    // Variable that contains method,url declaration that goes to backend.
    const getTimeRanges = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getTimeRanges']}`
    };
    // Catching backend response from getService method.
    const response = await getService(getTimeRanges);
    if (response && response.status === RA_API_STATUS['200']) {
      // Coverts the object to array.
      const result = CA_Utils.objToArray(response.data.timePeriod, 'object');
      // Updating the local state.
      this.setState({ dateRangeOptions: result });
    }
  };

  /**
   * @param   {string}  organizationName    It carries organization name.
   * @description   This method gets all the categories.
   */
  getChannels = async organizationName => {
    // Variable that contains method,url declaration that goes to backend.
    const getChannels = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl'] + '/' + organizationName + RA_API_URL['assignchannels']}`
    };
    // Catching backend response from getService method.
    const response = await getService(getChannels);
    if (response && response.status === RA_API_STATUS['200']) {
      const result = response.data;
      const finalChannelData = [];
      result.forEach(element => {
        let obj = {
          key: element.name,
          content: element.name
        };
        finalChannelData.push(obj);
      });
      // Updating the local state.
      this.setState({ channelData: finalChannelData, channelName: finalChannelData[0].key });
    }
  };

  /**
   * @param   {string}  organizationName It carries organization name.
   * @description   This method gets all the categories.
   */
  getCurrency = async organizationName => {
    // Variable that contains method,url declaration that goes to backend.
    const getCurrency = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['transactionDataReport'] + '/' + organizationName + '/currency'}`
    };
    // Catching backend response from getService method.
    const response = await getService(getCurrency);
    if (response && response.status === RA_API_STATUS['200']) {
      const result = response.data;
      const finalCurrencyData = [];
      result.forEach(element => {
        let obj = {
          key: element,
          content: element
        };
        finalCurrencyData.push(obj);
      });
      // Updating the local state.
      this.setState({ currencyData: finalCurrencyData, currency: finalCurrencyData[0].key });
    }
  };

  handleDateChange = e => {
    let modifyStartDate = new Date();
    let modifyEndDate = new Date();
    let getSelectedText = e.target.value;
    if (getSelectedText !== '0') {
      this.setState({ disableInput: true });
    } else {
      this.setState({ disableInput: false });
    }
    this.setState({
      selectedDateRange: getSelectedText
    });
    switch (getSelectedText) {
      case '1':
        modifyStartDate = moment()
          .startOf('month')
          .toDate();
        break;
      case '2':
        modifyStartDate = moment()
          .add(-2, 'month')
          .startOf('month')
          .toDate();
        break;
      case '3':
        modifyStartDate = moment()
          .add(-5, 'month')
          .startOf('month')
          .toDate();
        break;
    }
    this.setState({
      startDate: modifyStartDate,
      endDate: modifyEndDate,
      startYearData: modifyStartDate.getFullYear() + 1,
      startMonthData: modifyStartDate.getMonth() + 1,
      endYearData: modifyEndDate.getFullYear() + 1,
      endMonthData: modifyEndDate.getMonth() + 1
    });
  };

  handleExport = e => {
    if (e.target.value === 'csv') {
      this.setState({ downloadType: e.target.value });
      this.exportReport();
    } else {
      this.setState({ downloadType: 'null' });
    }
  };

  graphRender = (d, month) => {
    const arg = d;
    this.state.month = month;
    this.state.graphArg = d;
    const { riskList, txCountList, fraudCountList } = this.state.monthData;
    let renderRiskNut = this.state.riskDataCount,
      renderTxStatus = this.state.txDataCount,
      renderfraudStatus = this.state.fraudDataCount;
    if (arg.id === 'Transaction Count' && month) {
      var monthMatch = this.state.monthData.find(item => {
        if (item.key == month) return item.key;
      });
      const tempRiskAdviceCountArray = [['Allow'], ['Deny'], ['Alert'], ['Increase Auth']];
      tempRiskAdviceCountArray[0].push(monthMatch.content.riskAdviceCount.allow.total);
      tempRiskAdviceCountArray[1].push(monthMatch.content.riskAdviceCount.deny.total);
      tempRiskAdviceCountArray[2].push(monthMatch.content.riskAdviceCount.alert.total);
      tempRiskAdviceCountArray[3].push(monthMatch.content.riskAdviceCount.increaseAuth.total);
      renderRiskNut.columns = tempRiskAdviceCountArray;

      //  To set data to Donut Data of Tx Count for Transaction Status
      const tempTxAdviceCountArray = [
        ['Success'],
        ['Unavailable'],
        ['Attempts'],
        ['Rejected'],
        ['Abandoned'],
        ['Failure']
      ];
      tempTxAdviceCountArray[0].push(monthMatch.content.txnStatusCount.success.total);
      tempTxAdviceCountArray[1].push(monthMatch.content.txnStatusCount.unavailable.total);
      tempTxAdviceCountArray[2].push(monthMatch.content.txnStatusCount.attempts.total);
      tempTxAdviceCountArray[3].push(monthMatch.content.txnStatusCount.rejected.total);
      tempTxAdviceCountArray[4].push(monthMatch.content.txnStatusCount.abandoned.total);
      tempTxAdviceCountArray[5].push(monthMatch.content.txnStatusCount.failure.total);

      renderTxStatus.columns = tempTxAdviceCountArray;

      // To set data to Donut Data of Fraud Count for Fraud Status
      const tempFraudCountArray = [
        ['Confirmed Genuine'],
        ['Assumed Genuine'],
        ['Undetermined'],
        ['Assumed Fraud'],
        ['Confirmed Fraud']
      ];
      tempFraudCountArray[0].push(monthMatch.content.fraudStatusCount.confirmedGenuine.total);
      tempFraudCountArray[1].push(monthMatch.content.fraudStatusCount.assumedGenuine.total);
      tempFraudCountArray[2].push(monthMatch.content.fraudStatusCount.undetermined.total);
      tempFraudCountArray[3].push(monthMatch.content.fraudStatusCount.assumedFraud.total);
      tempFraudCountArray[4].push(monthMatch.content.fraudStatusCount.confirmedFraud.total);
      renderfraudStatus.columns = tempFraudCountArray;

      this.setState({
        riskDataCount: renderRiskNut,
        txDataCount: renderTxStatus,
        fraudDataCount: renderfraudStatus
      });
      this.prepareSankeyGraph();
      this.setState({
        currentStep: 2,
        showGraph: true
      });
      this.callSVG();
    } else if (arg.id === 'Transaction Amount (USD)' && month) {
      var monthMatch = this.state.monthData.find(item => {
        if (item.key == month) return item.key;
      });
      if (this.state.currency === 'USD') {
        // Dispalying data Tx Amount for USD currency
        //   To set data to Donut Data of Risk Amount for Risk Advice
        const tempRiskAdviceAmtArray = [['Allow'], ['Deny'], ['Alert'], ['Increase Auth']];
        tempRiskAdviceAmtArray[0].push(monthMatch.content.riskAdviceAmt.USD.allow.total);
        tempRiskAdviceAmtArray[1].push(monthMatch.content.riskAdviceAmt.USD.deny.total);
        tempRiskAdviceAmtArray[2].push(monthMatch.content.riskAdviceAmt.USD.alert.total);
        tempRiskAdviceAmtArray[3].push(monthMatch.content.riskAdviceAmt.USD.increaseAuth.total);
        renderRiskNut.columns = tempRiskAdviceAmtArray;

        //   To set data to Donut Data of Tx Amount for Transaction Status
        const tempTxAdviceAmtArray = [
          ['Success'],
          ['Unavailable'],
          ['Attempts'],
          ['Rejected'],
          ['Abandoned'],
          ['Failure']
        ];
        tempTxAdviceAmtArray[0].push(monthMatch.content.txnStatusAmt.USD.success.total);
        tempTxAdviceAmtArray[1].push(monthMatch.content.txnStatusAmt.USD.unavailable.total);
        tempTxAdviceAmtArray[2].push(monthMatch.content.txnStatusAmt.USD.attempts.total);
        tempTxAdviceAmtArray[3].push(monthMatch.content.txnStatusAmt.USD.rejected.total);
        tempTxAdviceAmtArray[4].push(monthMatch.content.txnStatusAmt.USD.abandoned.total);
        tempTxAdviceAmtArray[5].push(monthMatch.content.txnStatusAmt.USD.failure.total);
        renderTxStatus.columns = tempTxAdviceAmtArray;

        //   To set data to Donut Data of Fraud Amount for Fraud Status
        const tempFraudAmtArray = [
          ['Confirmed Genuine'],
          ['Assumed Genuine'],
          ['Undetermined'],
          ['Assumed Fraud'],
          ['Confirmed Fraud']
        ];
        tempFraudAmtArray[0].push(monthMatch.content.fraudStatusAmt.USD.confirmedGenuine.total);
        tempFraudAmtArray[1].push(monthMatch.content.fraudStatusAmt.USD.assumedGenuine.total);
        tempFraudAmtArray[2].push(monthMatch.content.fraudStatusAmt.USD.undetermined.total);
        tempFraudAmtArray[3].push(monthMatch.content.fraudStatusAmt.USD.assumedFraud.total);
        tempFraudAmtArray[4].push(monthMatch.content.fraudStatusAmt.USD.confirmedFraud.total);
        renderfraudStatus.columns = tempFraudAmtArray;
        // For Amount Total List
        this.donutLabel();

        this.setState({
          riskDataCount: renderRiskNut,
          txDataCount: renderTxStatus,
          fraudDataCount: renderfraudStatus,
        });

        this.prepareSankeyGraph();
        this.setState({
          currentStep: 2,
          showGraph: true
        });
        this.callSVG();
      } else {
        //   To set data to Donut Data of Risk Amount for Risk Advice
        const tempRiskAdviceAmtArray = [['Allow'], ['Deny'], ['Alert'], ['Increase Auth']];
        tempRiskAdviceAmtArray[0].push(monthMatch.content.riskAdviceAmt.Base.allow.total);
        tempRiskAdviceAmtArray[1].push(monthMatch.content.riskAdviceAmt.Base.deny.total);
        tempRiskAdviceAmtArray[2].push(monthMatch.content.riskAdviceAmt.Base.alert.total);
        tempRiskAdviceAmtArray[3].push(monthMatch.content.riskAdviceAmt.Base.increaseAuth.total);
        renderRiskNut.columns = tempRiskAdviceAmtArray;

        //   To set data to Donut Data of Tx Amount for Transaction Status
        const tempTxAdviceAmtArray = [
          ['Success'],
          ['Unavailable'],
          ['Attempts'],
          ['Rejected'],
          ['Abandoned'],
          ['Failure']
        ];
        tempTxAdviceAmtArray[0].push(monthMatch.content.txnStatusAmt.Base.success.total);
        tempTxAdviceAmtArray[1].push(monthMatch.content.txnStatusAmt.Base.unavailable.total);
        tempTxAdviceAmtArray[2].push(monthMatch.content.txnStatusAmt.Base.attempts.total);
        tempTxAdviceAmtArray[3].push(monthMatch.content.txnStatusAmt.Base.rejected.total);
        tempTxAdviceAmtArray[4].push(monthMatch.content.txnStatusAmt.Base.abandoned.total);
        tempTxAdviceAmtArray[5].push(monthMatch.content.txnStatusAmt.Base.failure.total);
        renderTxStatus.columns = tempTxAdviceAmtArray;

        //   To set data to Donut Data of Fraud Amount for Fraud Status
        const tempFraudAmtArray = [
          ['Confirmed Genuine'],
          ['Assumed Genuine'],
          ['Undetermined'],
          ['Assumed Fraud'],
          ['Confirmed Fraud']
        ];
        tempFraudAmtArray[0].push(monthMatch.content.fraudStatusAmt.Base.confirmedGenuine.total);
        tempFraudAmtArray[1].push(monthMatch.content.fraudStatusAmt.Base.assumedGenuine.total);
        tempFraudAmtArray[2].push(monthMatch.content.fraudStatusAmt.Base.undetermined.total);
        tempFraudAmtArray[3].push(monthMatch.content.fraudStatusAmt.Base.assumedFraud.total);
        tempFraudAmtArray[4].push(monthMatch.content.fraudStatusAmt.Base.confirmedFraud.total);
        renderfraudStatus.columns = tempFraudAmtArray;
        // For Amount Total List
        this.donutLabel();

        this.setState({
          riskDataCount: renderRiskNut,
          txDataCount: renderTxStatus,
          fraudDataCount: renderfraudStatus
        });
      }
      this.prepareSankeyGraph();
      this.setState({
        currentStep: 2,
        showGraph: true
      });
      this.callSVG();
    } else if (arg === 'Transaction Count' && month == '') {
      // Calling default data to be dispalyed,which is Count
      this.donutGraphCall();
      this.prepareSankeyGraph();
      this.setState({
        currentStep: 2,
        showGraph: true
      });
      this.callSVG();
    } else if (arg === 'Transaction Amount (USD)' && month == '') {
      if (this.state.currency === 'USD') {
        // Dispalying data Tx Amount for USD currency
        //   To set data to Donut Data of Risk Amount for Risk Advice
        const tempRiskAdviceAmtArray = [['Allow'], ['Deny'], ['Alert'], ['Increase Auth']],
        riskList=this.state.monthData;
        riskList.forEach((obj, index) => {
          tempRiskAdviceAmtArray[0].push(obj.content.riskAdviceAmt.USD.allow.total);
          tempRiskAdviceAmtArray[1].push(obj.content.riskAdviceAmt.USD.deny.total);
          tempRiskAdviceAmtArray[2].push(obj.content.riskAdviceAmt.USD.alert.total);
          tempRiskAdviceAmtArray[3].push(obj.content.riskAdviceAmt.USD.increaseAuth.total);
        });
        renderRiskNut.columns = tempRiskAdviceAmtArray;

        //   To set data to Donut Data of Tx Amount for Transaction Status
        const tempTxAdviceAmtArray = [
          ['Success'],
          ['Unavailable'],
          ['Attempts'],
          ['Rejected'],
          ['Abandoned'],
          ['Failure']
        ],txCountList=this.state.monthData;
        txCountList.forEach((obj, index) => {
          tempTxAdviceAmtArray[0].push(obj.content.txnStatusAmt.USD.success.total);
          tempTxAdviceAmtArray[1].push(obj.content.txnStatusAmt.USD.unavailable.total);
          tempTxAdviceAmtArray[2].push(obj.content.txnStatusAmt.USD.attempts.total);
          tempTxAdviceAmtArray[3].push(obj.content.txnStatusAmt.USD.rejected.total);
          tempTxAdviceAmtArray[4].push(obj.content.txnStatusAmt.USD.abandoned.total);
          tempTxAdviceAmtArray[5].push(obj.content.txnStatusAmt.USD.failure.total);
        });
        renderTxStatus.columns = tempTxAdviceAmtArray;

        //   To set data to Donut Data of Fraud Amount for Fraud Status
        const tempFraudAmtArray = [
          ['Confirmed Genuine'],
          ['Assumed Genuine'],
          ['Undetermined'],
          ['Assumed Fraud'],
          ['Confirmed Fraud']
        ],fraudCountList=this.state.monthData;
        fraudCountList.forEach((obj, index) => {
          tempFraudAmtArray[0].push(obj.content.fraudStatusAmt.USD.confirmedGenuine.total);
          tempFraudAmtArray[1].push(obj.content.fraudStatusAmt.USD.assumedGenuine.total);
          tempFraudAmtArray[2].push(obj.content.fraudStatusAmt.USD.undetermined.total);
          tempFraudAmtArray[3].push(obj.content.fraudStatusAmt.USD.assumedFraud.total);
          tempFraudAmtArray[4].push(obj.content.fraudStatusAmt.USD.confirmedFraud.total);
        });
        renderfraudStatus.columns = tempFraudAmtArray;
        // For Amount Total List
        this.donutLabel();
        this.setState({
          riskDataCount: renderRiskNut,
          txDataCount: renderTxStatus,
          fraudDataCount: renderfraudStatus,
        });
        this.prepareSankeyGraph();
        this.setState({
          currentStep: 2,
          showGraph: true
        });
        this.callSVG();
      } else {
        //   To set data to Donut Data of Risk Amount for Risk Advice
        const tempRiskAdviceAmtArray = [['Allow'], ['Deny'], ['Alert'], ['Increase Auth']];
        riskList.forEach((obj, index) => {
          tempRiskAdviceAmtArray[0].push(obj.content.riskAdviceAmt.Base.allow.total);
          tempRiskAdviceAmtArray[1].push(obj.content.riskAdviceAmt.Base.deny.total);
          tempRiskAdviceAmtArray[2].push(obj.content.riskAdviceAmt.Base.alert.total);
          tempRiskAdviceAmtArray[3].push(obj.content.riskAdviceAmt.Base.increaseAuth.total);
        });
        renderRiskNut.columns = tempRiskAdviceAmtArray;

        //   To set data to Donut Data of Tx Amount for Transaction Status
        const tempTxAdviceAmtArray = [
          ['Success'],
          ['Unavailable'],
          ['Attempts'],
          ['Rejected'],
          ['Abandoned'],
          ['Failure']
        ];
        txCountList.forEach((obj, index) => {
          tempTxAdviceAmtArray[0].push(obj.content.txnStatusAmt.Base.success.total);
          tempTxAdviceAmtArray[1].push(obj.content.txnStatusAmt.Base.unavailable.total);
          tempTxAdviceAmtArray[2].push(obj.content.txnStatusAmt.Base.attempts.total);
          tempTxAdviceAmtArray[3].push(obj.content.txnStatusAmt.Base.rejected.total);
          tempTxAdviceAmtArray[4].push(obj.content.txnStatusAmt.Base.abandoned.total);
          tempTxAdviceAmtArray[5].push(obj.content.txnStatusAmt.Base.failure.total);
        });
        renderTxStatus.columns = tempTxAdviceAmtArray;

        //   To set data to Donut Data of Fraud Amount for Fraud Status
        const tempFraudAmtArray = [
          ['Confirmed Genuine'],
          ['Assumed Genuine'],
          ['Undetermined'],
          ['Assumed Fraud'],
          ['Confirmed Fraud']
        ];
        fraudCountList.forEach((obj, index) => {
          tempFraudAmtArray[0].push(obj.content.fraudStatusAmt.Base.confirmedGenuine.total);
          tempFraudAmtArray[1].push(obj.content.fraudStatusAmt.Base.assumedGenuine.total);
          tempFraudAmtArray[2].push(obj.content.fraudStatusAmt.Base.undetermined.total);
          tempFraudAmtArray[3].push(obj.content.fraudStatusAmt.Base.assumedFraud.total);
          tempFraudAmtArray[4].push(obj.content.fraudStatusAmt.Base.confirmedFraud.total);
        });
        renderfraudStatus.columns = tempFraudAmtArray;
        // For Amount Total List
        this.donutLabel();

        this.setState({
          riskDataCount: renderRiskNut,
          txDataCount: renderTxStatus,
          fraudDataCount: renderfraudStatus,
        });
      }
      this.prepareSankeyGraph();
      this.setState({
        currentStep: 2,
        showGraph: true
      });
      this.callSVG();
    }
  };txCountList


   /*
   For sum in center for Donut Graph
  */

  donutLabel = async =>{
    const txTotal = [];
    // const renderTxTotal = JSON.parse(JSON.stringify(this.state.donut));
    const renderTxTotal = this.state.donut;


    const txCountList= this.state.monthData;

    if(this.state.graphArg === 'Transaction Count' && this.state.month == ''){
      txCountList.forEach((obj, index) => {
        txTotal.push(obj.content.txnStatusCount.success.total);
        txTotal.push(obj.content.txnStatusCount.unavailable.total);
        txTotal.push(obj.content.txnStatusCount.attempts.total);
        txTotal.push(obj.content.txnStatusCount.rejected.total);
        txTotal.push(obj.content.txnStatusCount.abandoned.total);
        txTotal.push(obj.content.txnStatusCount.failure.total);
      });
      renderTxTotal.title ='';
      const txCountTotal = txTotal.reduce(function(sum, item) {
        return (sum += item);
      }, 0);  
      renderTxTotal.title = txCountTotal + ' Total';

    }else if(this.state.graphArg === 'Transaction Amount (USD)' && this.state.month == ''){
      if(this.state.currency === "USD"){
        txCountList.forEach((obj, index) => {
              txTotal.push(obj.content.txnStatusAmt.USD.success.total);
              txTotal.push(obj.content.txnStatusAmt.USD.unavailable.total);
              txTotal.push(obj.content.txnStatusAmt.USD.attempts.total);
              txTotal.push(obj.content.txnStatusAmt.USD.rejected.total);
              txTotal.push(obj.content.txnStatusAmt.USD.abandoned.total);
              txTotal.push(obj.content.txnStatusAmt.USD.failure.total);
           });
      }else{
        txCountList.forEach((obj, index) => {
              txTotal.push(obj.content.txnStatusAmt.Base.success.total);
              txTotal.push(obj.content.txnStatusAmt.Base.unavailable.total);
              txTotal.push(obj.content.txnStatusAmt.Base.attempts.total);
              txTotal.push(obj.content.txnStatusAmt.Base.rejected.total);
              txTotal.push(obj.content.txnStatusAmt.Base.abandoned.total);
              txTotal.push(obj.content.txnStatusAmt.Base.failure.total);
           });
      }
        renderTxTotal.title ='';
        const txCountTotal = txTotal.reduce(function(sum, item) {
            return (sum += item);
          }, 0);
      
        renderTxTotal.title = txCountTotal + ' ' + this.state.currency + ' Total';

    }else if(this.state.month && this.state.graphArg.id === 'Transaction Count'){
      if(this.state.monthData.length){
        var monthMatch = this.state.monthData.find(item => {
          if (item.key == this.state.month) return item.key;
        });

        txTotal.push(monthMatch.content.riskAdviceCount.allow.total);
        txTotal.push(monthMatch.content.riskAdviceCount.deny.total);
        txTotal.push(monthMatch.content.riskAdviceCount.alert.total);
        txTotal.push(monthMatch.content.riskAdviceCount.increaseAuth.total);
      }
      renderTxTotal.title ='';
      const txCountTotal = txTotal.reduce(function(sum, item) {
          return (sum += item);
        }, 0);
    
      renderTxTotal.title = txCountTotal +' Total';

    }else if(this.state.month && this.state.graphArg.id === 'Transaction Amount (USD)'){

    }else if(this.state.graphArg.id === undefined){
      txCountList.forEach((obj, index) => {
        txTotal.push(obj.content.txnStatusCount.success.total);
        txTotal.push(obj.content.txnStatusCount.unavailable.total);
        txTotal.push(obj.content.txnStatusCount.attempts.total);
        txTotal.push(obj.content.txnStatusCount.rejected.total);
        txTotal.push(obj.content.txnStatusCount.abandoned.total);
        txTotal.push(obj.content.txnStatusCount.failure.total);
      });
          renderTxTotal.title ='';
          const txCountTotal = txTotal.reduce(function(sum, item) {
            return (sum += item);
          }, 0);
         
          renderTxTotal.title = txCountTotal + ' Total';
    } 
      this.setState({
        donut:renderTxTotal
      })
  }

  // Calling SVG
  callSVG = async => {
    if (this.state.graphData.length > 0) {
      this.measureSVG();
      window.addEventListener('resize', this.measureSVG);
    } else {
      window.removeEventListener('resize', this.measureSVG);
    }
  };

  // on click of button Display Report

  displayReports = async e => {
    e.preventDefault();
    // Disabling success/error messages.
    // this.visualiseReport();
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    let fromDate = '';
    let toDate = '';
    if (this.state.disableInput) {
      fromDate = moment(this.state.startDate).format('L');
      toDate = moment(this.state.endDate).format('L');
    } else {
      let start_date = new Date(this.state.startYearData, this.state.startMonthData - 1, 1);
      let end_date = new Date();
      fromDate = moment(start_date).format('L');
      toDate = moment(end_date).format('L');
    }

    // Variable that contains method,url,payload declaration that goes to backend.
    const transactionDataReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['transactionDataReport']}`,
      data: {
        fromDateUI: fromDate,
        orgName: this.state.orgName,
        selectedChannel: this.state.channelName,
        toDateUI: toDate
      }
    };

    // Catching backend response from getService method.
    const response = await getService(transactionDataReport);
    if (response && response.status === RA_API_STATUS['200'] && response.data) {
      this.setState({
        monthData: CA_Utils.objToArray(response.data.jsonData.months, 'object') || {},
        defaultJson: CA_Utils.objToArray(response.data.jsonData.default, 'object') || {}
      });
      // console.log('monthData', this.state.monthData);
      // console.log('defaultJson', this.state.defaultJson);

      /**
       * Bar Graphs
       *  */

      if (this.state.monthData.length) {
        let renderData = this.state.bardata;
        const reportList = this.state.monthData;
        // For x values Bar graph
        const xlabel = ['x'];
        reportList.forEach((obj, index) => {
          xlabel.push(obj.key);
        });
        const tempArray = [];
        // For checking currency notation and setting values Bar Graph accordingly
        if (this.state.currency === 'USD') {
          tempArray.push(xlabel);
          tempArray.push(['Transaction Count']);
          tempArray.push(['Transaction Amount (USD)']);

          this.state.monthData.forEach((obj, index) => {
            tempArray[1].push(obj.content.txnCount);
            tempArray[2].push(obj.content.txnValueUSD);
          });
        } else {
          tempArray.push(xlabel);
          tempArray.push(['Transaction Count']);
          tempArray.push(['Transaction Amount Base']);

          this.state.monthData.forEach((obj, index) => {
            tempArray[1].push(obj.content.txnCount);
            tempArray[2].push(obj.content.txnValueBase);
          });
        }

        renderData.columns = tempArray;
        this.setState({ bardata: renderData });
      }

      this.donutGraphCall();
      /**
       * Sankey Graph
       *  */
      this.prepareSankeyGraph();
      this.setState({
        currentStep: 2,
        showGraph: true
      });
      this.callSVG();
      // window.scrollTo(0, 0);
    } else if (response && response.data.errorList) {
      this.props.activateErrorList(true, response.data.errorList);
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  };

  donutGraphCall = () => {
    /**
     * Donut Graphs
     */

    if (this.state.monthData.length) {
      // To set data to Donut Data of RiskCount for Risk Advice
      let renderRiskNut = this.state.riskDataCount;

      const riskList = this.state.monthData;
      const tempRiskAdviceCountArray = [['Allow'], ['Deny'], ['Alert'], ['Increase Auth']];
      riskList.forEach((obj, index) => {
        tempRiskAdviceCountArray[0].push(obj.content.riskAdviceCount.allow.total);
        tempRiskAdviceCountArray[1].push(obj.content.riskAdviceCount.deny.total);
        tempRiskAdviceCountArray[2].push(obj.content.riskAdviceCount.alert.total);
        tempRiskAdviceCountArray[3].push(obj.content.riskAdviceCount.increaseAuth.total);
      });
      renderRiskNut.columns = tempRiskAdviceCountArray;

      //  To set data to Donut Data of Tx Count for Transaction Status
      let renderTxStatus = this.state.txDataCount;
      const txCountList = this.state.monthData;
      const tempTxAdviceCountArray = [
        ['Success'],
        ['Unavailable'],
        ['Attempts'],
        ['Rejected'],
        ['Abandoned'],
        ['Failure']
      ];
      txCountList.forEach((obj, index) => {
        tempTxAdviceCountArray[0].push(obj.content.txnStatusCount.success.total);
        tempTxAdviceCountArray[1].push(obj.content.txnStatusCount.unavailable.total);
        tempTxAdviceCountArray[2].push(obj.content.txnStatusCount.attempts.total);
        tempTxAdviceCountArray[3].push(obj.content.txnStatusCount.rejected.total);
        tempTxAdviceCountArray[4].push(obj.content.txnStatusCount.abandoned.total);
        tempTxAdviceCountArray[5].push(obj.content.txnStatusCount.failure.total);
      });
      renderTxStatus.columns = tempTxAdviceCountArray;

      // To set data to Donut Data of Fraud Count for Fraud Status
      let renderfraudStatus = this.state.fraudDataCount;
      const fraudCountList = this.state.monthData;
      const tempFraudCountArray = [
        ['Confirmed Genuine'],
        ['Assumed Genuine'],
        ['Undetermined'],
        ['Assumed Fraud'],
        ['Confirmed Fraud']
      ];
      fraudCountList.forEach((obj, index) => {
        tempFraudCountArray[0].push(obj.content.fraudStatusCount.confirmedGenuine.total);
        tempFraudCountArray[1].push(obj.content.fraudStatusCount.assumedGenuine.total);
        tempFraudCountArray[2].push(obj.content.fraudStatusCount.undetermined.total);
        tempFraudCountArray[3].push(obj.content.fraudStatusCount.assumedFraud.total);
        tempFraudCountArray[4].push(obj.content.fraudStatusCount.confirmedFraud.total);
      });
      renderfraudStatus.columns = tempFraudCountArray;
  
      this.donutLabel();
  
      this.setState({
        riskDataCount: renderRiskNut,
        txDataCount: renderTxStatus,
        fraudDataCount: renderfraudStatus,
      });
    }
  };

  // Preparing nodes for sankey graph
  prepareSankeyGraph = async => {
    // console.log('defaultJson......',this.state.defaultJson);
    // console.log('monthData',this.state.monthData);

    let sankeyData = [],
      defaultVal = [],
      defaultValTx = [],
      sourceNode = [],
      destNode = [],
      mainNodes = [],
      source = [],
      dest = [],
      nodesList = [],
     raArray = ['Allow', 'Deny', 'Increase Auth', 'Alert'],
     tsArray = ['Success', 'Unavailable', 'Attempts', 'Rejected', 'Abandoned', 'Failure'],
     fsArray = ['Confirmed Fraud', 'Confirmed Genuine', 'Assumed Fraud', 'Assumed Genuine', 'Undetermined'];

    for (var i = 0; i < raArray.length; i++) {
      for (var j = 0; j < tsArray.length; j++) {
        sankeyData.push([raArray[i], tsArray[j], Number.MIN_VALUE, 'color:#ffffff; opacity: 0', '']);
      }
    }
    for (var i = 0; i < tsArray.length; i++) {
      for (var j = 0; j < fsArray.length; j++) {
        sankeyData.push([tsArray[i], fsArray[j], Number.MIN_VALUE, 'color:#ffffff; opacity: 0', '']);
      }
    }

    // onclick calls
    if (this.state.graphArg === 'Transaction Count' && this.state.month == '') {
      if (this.state.defaultJson.length > 0) {
        defaultVal = this.state.defaultJson[8].content;
        defaultValTx = this.state.defaultJson[6].content;
      }
    } else if (this.state.graphArg === 'Transaction Amount (USD)' && this.state.month == '') {
      if (this.state.currency === 'USD') {
        defaultVal = this.state.defaultJson[2].content.USD;
        defaultValTx = this.state.defaultJson[0].content.USD;
      } else {
        defaultVal = this.state.defaultJson[2].content.Base;
        defaultValTx = this.state.defaultJson[0].content.Base;
      }
    } else if (this.state.month && this.state.graphArg.id === 'Transaction Count') {
      if (this.state.monthData.length > 0) {
        var monthMatch = this.state.monthData.find(item => {
          if (item.key == this.state.month) return item.key;
        });
        defaultVal = monthMatch.content.riskAdviceCount;
        defaultValTx = monthMatch.content.txnStatusCount;
      }
    } else if (this.state.month && this.state.graphArg.id === 'Transaction Amount (USD)') {
      if (this.state.monthData.length > 0) {
        var monthMatch = this.state.monthData.find(item => {
          if (item.key == this.state.month) return item.key;
        });
        if (this.state.currency === 'USD') {
          defaultVal = monthMatch.content.riskAdviceAmt.USD;
          defaultValTx = monthMatch.content.txnStatusAmt.USD;
        } else {
          defaultVal = monthMatch.content.riskAdviceAmt.Base;
          defaultValTx = monthMatch.content.txnStatusAmt.Base;
        }
      }
    } else if (this.state.graphArg.id === undefined) {
      if (this.state.defaultJson.length > 0) {
        defaultVal = this.state.defaultJson[8].content;
        defaultValTx = this.state.defaultJson[6].content;
      }
    }
    this.setState({
      sankeyData: sankeyData,
      defJson: defaultVal,
      defTxJson: defaultValTx
    });

    this.pushSankeyGraphData(this.state.defJson, Object.keys(this.state.defJson));
    this.pushSankeyGraphData(this.state.defTxJson, Object.keys(this.state.defTxJson));
    this.state.graphData = this.removeOrphanNodes(this.state.sankeyData);

    for (var i = 0; i < this.state.graphData.length; i++) {
      sourceNode.push(this.state.graphData[i][0]);
      destNode.push(this.state.graphData[i][1]);
    }
    source = sourceNode.filter((item, i) => sourceNode.indexOf(item) === i);
    dest = destNode.filter((item, i) => destNode.indexOf(item) === i);
    mainNodes = source.concat(dest).filter((item, i) => source.concat(dest).indexOf(item) === i);
    if (mainNodes.length > 0) {
      mainNodes.forEach(ele => {
        nodesList.push({ name: ele });
      });
    }
    const linksArray = [];

    this.state.graphData.forEach(d => {
      linksArray.push({
        source: mainNodes.indexOf(d[0]),
        target: mainNodes.indexOf(d[1]),
        value: d[2]
      });
    });

    this.setState({
      sankeyGraphData: {
        nodes: nodesList,
        links: linksArray
      }
    });
    // console.log('sankeyGraphData', this.state.sankeyGraphData);
  };

  // Passing data for Sankey Graph
  pushSankeyGraphData = async (aggregateArr, sourceKeys) => {
    const { sankeyData } = this.state;
    var tooltipText = '';
    for (var i = 0; i < sourceKeys.length; i++) {
      var destKeys = Object.keys(aggregateArr[sourceKeys[i]]);
      if (destKeys.length > 1) {
        for (var j = 1; j < destKeys.length; j++) {
          tooltipText = '';
          if (aggregateArr[sourceKeys[i]][destKeys[j]] != undefined && aggregateArr[sourceKeys[i]][destKeys[j]] >= 0) {
            tooltipText =
              this.state.currency != ''
                ? 'Transaction Amount: ' +
                  aggregateArr[sourceKeys[i]][destKeys[j]].toLocaleString() +
                  ' ' +
                  this.state.currency
                : 'Transaction Count: ' + aggregateArr[sourceKeys[i]][destKeys[j]].toLocaleString();

            var sankeyObject = [
              this.converyKeyToDisplayName(sourceKeys[i]),
              this.converyKeyToDisplayName(destKeys[j]),
              Number(aggregateArr[sourceKeys[i]][destKeys[j]]),
              'color: #dde4ef',
              "<div class='sankeyTooltip'>" + tooltipText + '</div>'
            ];
            var arrIndex = this.findSankeyIndex(sankeyData, sankeyObject);

            if (arrIndex != -1) {
              sankeyData[arrIndex] = sankeyObject;
            } else {
              sankeyData.push(sankeyObject);
            }
          }
        }
      }
    }
  };

  // UTILITY - FINDS THE CORRECT SANKEY ROW AND RETURNS INDEX
  findSankeyIndex = function(masterArray, filterArray) {
    // findSankeyIndex = function(arr) {
    var objectIndex = -1;
    masterArray.every(function(curValue, index) {
      if (curValue.length == 5 && curValue.length == filterArray.length) {
        if (curValue[0] == filterArray[0] && curValue[1] == filterArray[1]) {
          objectIndex = index;
        }
      }
      return objectIndex;
    });

    return objectIndex;
  };

  // UTILITY - CONVERTS JSON STATUS KEYS TO READABLE NAMES
  converyKeyToDisplayName(key) {
    key = key.replace(/([A-Z])/g, ' $1').trim();
    return key.charAt(0).toUpperCase() + key.slice(1, key.length);
  }

  //UTILITY - CONVERTS READABLE NAMES TO JSON STATUS KEYS
  convertDisplayNameToKey(displayName) {
    return displayName.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
      if (+match === 0) return ''; // or if (/\s+/.test(match)) for white spaces
      return index == 0 ? match.toLowerCase() : match.toUpperCase();
    });
  }

  // VERIFIES IF A NODE IS ORPHAN OR NOT
  isOrphan = function(arr, nodeName, filter) {
    var isOrphan = true;
    arr.forEach(function(currentValue, index) {
      if (
        (currentValue[0].indexOf(nodeName) != -1 || currentValue[1].indexOf(nodeName) != -1) &&
        currentValue[2] !== filter
      ) {
        isOrphan = false;
      }
    });
    return isOrphan;
  };

  // UTILITY - REMOVES ORPHAN NODES - THOSE THAT DON'T HAVE ANY PATH FROM IT
  removeOrphanNodes = function(data) {
    var pathlessNodes = [],
      orphanNodes = [],
      resultSankey = [];
    var originalSankey = data;

    data.forEach(function(currentValue, index) {
      if (currentValue[2] === Number.MIN_VALUE) {
        if (pathlessNodes.indexOf(currentValue[0]) == -1) {
          pathlessNodes.push(currentValue[0]);
        }
        if (pathlessNodes.indexOf(currentValue[1]) == -1) {
          pathlessNodes.push(currentValue[1]);
        }
      }
    });
    pathlessNodes.forEach((currentValue, index) => {
      if (this.isOrphan(originalSankey, currentValue, Number.MIN_VALUE)) {
        orphanNodes.push(currentValue);
      }
    });

    originalSankey.forEach(function(currentValue) {
      if (
        orphanNodes.indexOf(currentValue[0]) == -1 &&
        orphanNodes.indexOf(currentValue[1]) == -1 &&
        currentValue[2] > Number.MIN_VALUE
      ) {
        resultSankey.push(currentValue);
      }
    });
    return resultSankey;
  };

  /**
   * @description   This method exports all the reports.
   */
  exportReport = async () => {
    // Disabling success/error messages.
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    let fromDate = '';
    let toDate = '';
    if (this.state.disableInput) {
      fromDate = moment(this.state.startDate).format('L');
      toDate = moment(this.state.endDate).format('L');
    } else {
      let start_date = new Date(this.state.startYearData, this.state.startMonthData - 1, 1);
      let end_date = new Date();
      fromDate = moment(start_date).format('L');
      toDate = moment(end_date).format('L');
    }
    // Variable that contains method,url,payload declaration that goes to backend.
    const exportExceptionUserReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['transactionDataReport'] + '/export'}`,
      data: {
        fromDateUI: fromDate,
        orgName: this.state.orgName,
        selectedChannel: this.state.channelName,
        toDateUI: toDate
      }
    };
    // Catching backend response from getService method.
    const response = await getService(exportExceptionUserReport);
    // Success case handling
    if (response && response.status === RA_API_STATUS['200'] && response.data) {
      // Setting the file type and saving as blob format.
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Transaction_data_Report.csv');
      this.setState({ downloadType: 'null' });
      // Activates the success message list.
      this.props.activateSuccessList(true, response.data);
      // Activates the error message list.
      this.props.activateErrorList(false, '');
      window.scrollTo(0, 0);
    } else if (response && response.data.errorList) {
      this.setState({ downloadType: '' });
      // Activates the error message list.
      this.props.activateErrorList(true, response.data.errorList);
      // Activates the success message list.
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  };

  render() {
    // console.log('title.....',this.state.donut.title);
    const {
      width,
      height,
      showGraph,
      downloadType,
      organizationData,
      downloadTypeOptions,
      channelData,
      currencyData,
      dateRangeOptions,
      disableInput,
      yearOptions,
      monthOptions,
      startYearData,
      startMonthData,
      endYearData,
      endMonthData,
      bardata,
      bargrid,
      baraxis,
      barcolor,
      x,
      Wbar,
      y2,
      riskDataCount,
      riskAdviceCountColor,
      txDataCount,
      txAdviceCountColor,
      donut,
      fraudDataCount,
      fraudAdviceCountColor,
      sankeyGraphData,
      legend
    } = this.state;
    return (
      <div className='main transaction-data-report-container'>
        <div className='row'>
          <div className='col-sm-10'>
            <h2 className='title'>Transaction data report</h2>
            <p className='desc'>
              This report shows the transaction information for selected organizations for specified duration
            </p>
          </div>
          <div className='col-sm-2'>
            <p className='desc'>Export</p>
            <Select
              name={'downloadType'}
              options={downloadTypeOptions}
              controlFunc={this.handleExport}
              selectedOption={downloadType}
            />
          </div>
        </div>
        <hr />
        <div className='row'>
          <div className='col-sm-2 date-cls'>
            <p>Organization</p>
          </div>
          <div className='col-sm-2 date-cls'>
            <p>Channel</p>
          </div>
          <div className='col-sm-2 date-cls'>
            <p>Time Period</p>
          </div>
          <div className='col-sm-2 date-cls'>
            <p>From</p>
          </div>
          <div className='col-sm-2 date-cls'>
            <p>To</p>
          </div>
          <div className='col-sm-1 date-cls'>
            <p>Currency</p>
          </div>
          <div className='col-sm-1 date-cls'></div>
        </div>
        <div className='row'>
          <div className='col-sm-2 date-cls'>
            <Select name={'orgName'} options={organizationData} controlFunc={this.handleChange} />
          </div>
          <div className='col-sm-2 date-cls'>
            <Select name={'channelName'} options={channelData} controlFunc={this.handleChange} />
          </div>
          <div className='col-sm-2 date-cls'>
            <Select name={'timePeriod'} options={dateRangeOptions} controlFunc={this.handleDateChange} />
          </div>
          <div className='col-sm-2 date-cls'>
            <div className='row'>
              <div className='col-sm-6 date-cls'>
                <Select
                  name={'startYear'}
                  options={yearOptions}
                  disabled={disableInput}
                  selectedOption={startYearData.toString()}
                  controlFunc={this.handleChange}
                />
              </div>
              <div className='col-sm-6 date-cls'>
                <Select
                  name={'startMonth'}
                  options={monthOptions}
                  disabled={disableInput}
                  selectedOption={startMonthData.toString()}
                  controlFunc={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div className='col-sm-2 date-cls'>
            <div className='row'>
              <div className='col-sm-6 date-cls'>
                <Select
                  name={'endYear'}
                  options={yearOptions}
                  disabled={disableInput}
                  selectedOption={endYearData.toString()}
                  controlFunc={this.handleChange}
                />
              </div>
              <div className='col-sm-6 date-cls'>
                <Select
                  name={'endMonth'}
                  options={monthOptions}
                  disabled={disableInput}
                  selectedOption={endMonthData.toString()}
                  controlFunc={this.handleChange}
                />
              </div>
            </div>
          </div>
          <div className='col-sm-1 date-cls'>
            <Select name={'currencyName'} options={currencyData} controlFunc={this.handleChange} />
          </div>
          <div className='col-sm-1 date-cls'>
            <input
              className='secondary-btn ml-3'
              type='submit'
              value='DISPLAY REPORT'
              onClick={this.displayReports}
            ></input>
          </div>
        </div>

        {showGraph ? (
          <div>
            <div className='row'>
              <div className='col-sm-1'></div>
              <div className='col-sm-10 donut_cls'>
                <p className='charts_text'>Transaction Report</p>
                <C3Chart
                  data={bardata}
                  axis={baraxis}
                  bar={Wbar}
                  color={barcolor}
                  x={x}
                  grid={bargrid}
                  y2={y2}
                  legend={legend}
                />
              </div>
              <div className='col-sm-1'></div>
            </div>
            <div className='row'>
              <div className='col-sm-1'></div>
              <div className='col-sm-3 donut_cls'>
                <p className='charts_text'>Risk Advice</p>
                <C3Chart data={riskDataCount} color={riskAdviceCountColor} donut={donut} />
              </div>
              <div className='col-sm-3 donut_cls'>
                <p className='charts_text'>Transaction Status</p>
                <C3Chart data={txDataCount} color={txAdviceCountColor} donut={donut} />
              </div>
              <div className='col-sm-3 donut_cls'>
                <p className='charts_text'>Fraud Status</p>
                <C3Chart data={fraudDataCount} color={fraudAdviceCountColor} donut={donut} />
              </div>
              <div className='col-sm-1'></div>
            </div>
            <div className='row'>
              <div className='col-sm-1'></div>
              <div className='col-sm-10 donut_cls'>
                <p className='charts_text'>Transaction Flow</p>
                <div className='TransactionDataReport'>
                  {this.state.graphData.length ? (
                    <svg width='95%' height='300' ref={this.svgRef}>
                      {sankeyGraphData && <Sankey data={sankeyGraphData} width={width} height={height} />}
                    </svg>
                  ) : (
                    <div></div>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default TransactionDataReport;
