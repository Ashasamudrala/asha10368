import React, { Component, Fragment } from 'react';
import Select from '../../shared/components/Select/Select';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import './ViewQueueStatus.scss';
class ViewQueueStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultOrganizationName: RA_STR_SELECT,
      organizationNames: [],
      selectedorganization: '',
      viewTable: false,
      queueStatusList: [],
      handledCasesList: [],
      manageQueueUrl: 'manage-queue',
      displayName: []
    }
  }
  componentDidMount = () => {
    this.getAllOrganizations();
  }
  getAllOrganizations = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        organizationNames: CA_Utils.objToArray(response.data.organizations, 'string'),
        originalOrganizations: CA_Utils.objToArray(response.data.organizations, 'object')
      });
    }
  };

  // On select of Org Name enabling Channels Dropdown
  onOrgSelect = (value, type) => {
    if (type === 'click') {
      const orgId = this.state.originalOrganizations.find(element => {
        if (element.content === value) return element.key;
      });
      this.setState({
        defaultOrganizationName: orgId.key,
        orgDetails: false
      });
      this.getTableDetails(orgId.key);
    }
  };
  getTableDetails = async (selectedOrg) => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getQueueDetails']}${selectedOrg}/queue`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      this.setState({
        viewTable: true
      })
      this.setState({
        queueStatusList: response.data.queueStatusList,
        handledCasesList: response.data.handledCasesList,
        sumTotalNewCases: response.data.sumTotalNewCases,
        sumInProgressCasesIchVoid: response.data.sumInProgressCasesIchVoid,
        sumDiarizedCasesNoQueue: response.data.sumDiarizedCasesNoQueue,
        sumInProgressCasesIch: response.data.sumInProgressCasesIch,
        span: response.data.queueStatusList.length,
        displayName: response.data.queueStatusList.map((item) => {
          return item.displayName
        })
      });
    }
  }
  getqueueInfo = (queuename) => {
    this.props.history.push({
      pathname: `${this.state.manageQueueUrl}`,
      // pathname: this.state.manageQueueUrl,
      state: {
        data:queuename ,
      }
    })


  }

  render() {
    const { organizationNames, manageQueueUrl, defaultOrganizationName, viewTable, queueStatusList, handledCasesList, sumTotalNewCases, sumInProgressCasesIchVoid, sumDiarizedCasesNoQueue, sumInProgressCasesIch, displayName, span } = this.state;
    return (
      <div className='main status-queue'>
  <h2 className='title'>{RA_STR.ViewQueueTitle}</h2>
        <p className='desc'>{RA_STR.statusDec}</p>
        <div className='col-sm-6'>
          <div className='form-group row'>
            <label className='col-sm-4 col-form-label'>{RA_STR.org}</label>
            <div className='col-sm-5'>
              <AutoSuggest
                orgOptions={organizationNames}
                getSelectedOrgDetails={this.onOrgSelect}
                enableAutosuggest={true}
                defaultOrganizationPlaceholder={defaultOrganizationName}
              />
            </div>
          </div>
        </div>
        {viewTable === true && (
          <div>
            <div className="row">
              <div className="col-sm-12">
                <h2>{RA_STR.currentBlogTableHeading}</h2>
                <table className='rule-effect-table' style={{ Width: '100%', clear: 'both' }}>
                  <thead>
                    <tr className='main-header'>
                      <th className='col-head' rowspan='2'>
                        {RA_STR.th1}
                    </th>
                      <th className='col-head' colspan='2'>
                      {RA_STR.th2}
                    </th>
                      <th className='col-head' rowspan='2'>
                      {RA_STR.th3}
                    </th>
                      <th className='col-head' rowspan='2'>
                      {RA_STR.th4}
                    </th>
                      <th className='col-head' rowspan='2'>
                        {RA_STR.th5}
                    </th>
                      <th className='col-head' rowspan='2'>
                      {RA_STR.th6}
                    </th>
                    </tr>
                    <tr className='main-header'>
                      <th className='col-head child-head' colspan='1'>
                      {RA_STR.th7}
                    </th>
                      <th className='col-head child-head' colspan='1'>
                      {RA_STR.th8}
                    </th>
                    </tr>
                  </thead>
                  {queueStatusList.map((item, value) => {
                    return (
                      <tr key={value}>
                        <td ><a  onClick={() => this.getqueueInfo(item.displayName, this)}>{item.displayName}</a></td>
                        <td>{item.sumCachedCases}</td>
                        <td>{item.sumOpenCases}</td>
                        <td>{item.sumDiarizedCasesQueue}</td>
                        <td>{item.sumInProgressCases}</td>
                        <td>{item.sumTotalCases}</td>
                        <td>{item.noOfAdmins}</td>
                      </tr>
                    )
                  })}
                </table>
              </div>
            </div>
            <div className="row table-view">
              <div className="col-sm-12">
                <table className='rule-effect-table details-table' style={{ Width: '100%', clear: 'both' }}>
                  <tbody>
                    <tr>
                      <td>{RA_STR.table2th1}</td>
                      <td>{sumTotalNewCases}</td>
                      <td>{RA_STR.table2th2}</td>
                      <td>{sumDiarizedCasesNoQueue}</td>

                    </tr>
                    <tr>
                      <td>{RA_STR.table2th3}</td>
                      <td> {sumInProgressCasesIchVoid}</td>
                      <td>{RA_STR.table2th4}</td>
                      <td> {sumInProgressCasesIch}</td>

                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <h2>{RA_STR.CaseHandleTableHeading}</h2>
                <table className='rule-effect-table' style={{ Width: '100%', clear: 'both' }}>
                  <thead>
                    <tr className='main-header'>
                      <th className='col-head' rowspan='2'>
                       {RA_STR.tbth1}
                    </th>
                      <th className='col-head' rowspan='2'>
                      {RA_STR.tbth2}
                    </th>
                      <th className='col-head' colspan={span}>
                      {RA_STR.tbth3}
                    </th>
                      <th className='col-head' rowspan='2'>
                      {RA_STR.tbth4}
                    </th>
                    </tr>
                    <tr className='main-header'>
                      {displayName.map((item, value) => {
                        return (
                          <th className='col-head child-head' colspan='1'>
                            {item}
                          </th>
                        )
                      })}
                    </tr>
                  </thead>

                  {handledCasesList.map((item, index) => {
                    return (
                      <Fragment>
                        <tr>
                          {index === 0 && (
                            <td> Last <span className="hours">{index + 1}</span>hour</td>
                          )}
                          {index != 0 && (
                            <td> Last <span className="hours">{index + 1}</span>hours</td>
                          )}
                          <td>{item.sumInboundCases}</td>
                          {item.handledCasesListOutbound.map(item => {
                            return <td>{item.sumOutboundCases}</td>
                          })}

                          <td>{item.sumTotalHandledCases}</td>
                        </tr>
                      </Fragment>
                    )
                  })}
                </table>
              </div>
            </div>
          </div>)}
      </div>

    )
  }
}

export default ViewQueueStatus;
