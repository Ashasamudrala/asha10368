import React, { Component } from 'react';

class TelephoneConfiguration extends Component{
    constructor(props){
        super(props);

        this.state = {}
        this.state.phone = [1];
        this.handleRowDel = this.handleRowDel.bind(this);
    }

    handleAddEvent = () => {
        let index = this.state.phone.length;
        let phonearray = [];
        for (let begin = 0; begin <= index; begin++) {
            phonearray.push(begin);
        }
        this.setState({ phone: phonearray })
    }

    handleRowDel(index) {
        if (this.state.phone.length === 1) {
          return
        }
        var index = this.state.phone.indexOf(index);
        this.state.phone.splice(index, 1);
        this.setState(this.state.phone);
      }
    render(){
        return(<div className="">
            <span class="ecc-h1">Configure Telephone Type</span>

            <table className="table">
                <tbody>
                    <tr>
                        <th>Priority</th>
                        <th>Type</th>
                        <th>Display Name</th>
                        <th>Mandatory</th>
                        <th ><img src="images/plus.jpg" onClick={this.handleAddEvent} /></th>
                    </tr>
                    {
                        this.state.phone.map((value, index) => {
                            return (
                                (index == 0) ?

                                    <tr>
                                        <td><img src="images/icon_up_arrow.png" /> &nbsp; &nbsp;<img src="images/icon_down_arrow.png" /></td>
                                        <td> <div className="text-center col-sm-12"><input className="form-control " type='text'  disabled defaultValue={"TELEPHONE"}/></div> </td>
                                        <td> <div className="text-center col-sm-12"> <input className="form-control" type='text'  defaultValue={"Phone Number"}/></div> </td>
                                        <td><input type="checkbox" name="telephoneMandatory" disabled /></td>
                                        <td><img src="images/cross_disabled.gif" /></td>
                                    </tr> : <tr>
                                        <td><img src="images/icon_up_arrow.png" /> &nbsp; &nbsp;<img src="images/icon_down_arrow.png" /></td>
                                        <td> <div className="text-center col-sm-12"><input className="form-control" type='text'  /></div> </td>
                                        <td> <div className="text-center col-sm-12"> <input className="form-control" type='text' /></div> </td>
                                        <td><input type="checkbox" name="telephoneMandatory" disabled/></td>
                                        <td><img src="images/cross_enabled.gif" onClick ={() => this.handleRowDel(index)}/></td>
                                    </tr>);
                        })}

                </tbody>
            </table>
            </div>
        )
    }

}

export default TelephoneConfiguration;
