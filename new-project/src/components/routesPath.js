export const dynamicNavbar={
    "/users": 
        {  
         '/manageUsers': 'Manage Users and Administrators',
         '/manageRoles': 'Manage Roles'
        },
     "/server-config":
        {  
         '/RA': 'Risk Analytics',
         '/AC': 'Administration console'
        }
}
export const mapRoutes = {  
    "/users/manageUsers":
       {  
        '/search': 'Search Administrators',
        '/create': 'Create Administrator',
        '/searchDetails' : 'Basic Search Details'
       },
    '/users/manageRoles': {
        '/createCustomRole': 'Create Custom Role',
        '/updateCustomRole': 'Update Custom Role',
        '/deleteCustomRole': 'Delete Custom Role'
    },
    '/org':{
        '/createOrg': 'Create Organization',
        '/cacheStatus': 'Check Cache Refresh Status',
        '/searchOrg': 'Search Organization'
    },
    '/server-config/AC': {
        'System Configuration': {
            '/uds-conn': 'UDS Connectivity Configuration',
            '/refresh-cache': 'Refresh Cache',
            '/cacheStatus': 'Check Cache Refresh Status',
            '/attr-encry-config': 'Attribute Encryption Configuration',
            '/local-config': 'Localization Configuration'
        },
        'UDS Configuration': {
            '/uds-config': 'UDS Configuration',
            '/default-org': 'Set Default Organization',
            '/config-account': 'Configure Account Type',
            '/email-telephone-config': 'Email/Telephone Type Configuration'
        },
        'Authentication': {
            '/basic-auth': 'Basic Authentication Policy',
            '/master-auth': 'Master Administrator Authentication Policy'
        },
        'Web Services': {
            '/auth': 'Authentication and Authorization'
        },
        'multilevel':true
    },    
    '/server-config/RA': {
        'System Configuration': {
            '/risk-conn': 'Risk Analytics Connectivity',
            '/trusted-cert-auth': 'Trusted Certificate Authorities',
            '/manage-token-sites': 'Manage Tokenization Sites',
            '/manage-token': 'Manage Tokenization',
            '/endpoint-manage': 'Endpoint Management',
            '/amds-conn': 'AMDS Connectivity'
        },
        'Instance Configuration': {
            '/instance-manage': 'Instance Management',
            '/protocol-config': 'Protocol Configuration'
        },
        'Additional Configuration': {
            '/custom-report-view': 'Custom Report Views',
            // '/customize-report-view': 'Customize Report Views',
        },
        'multilevel':true
    },    
    '/org/searchOrg/basicOrg': {
        'Basic Organization Information': {
            '/org-details': 'Organization Details',
            '/basic-auth-policy': 'Basic Authentication Policy',
            '/bulk-upload': 'Bulk Upload',
            '/view-bulk-requests': 'View Bulk Requests'
        },
        'dynamiclevel':true,
    },
    '/org/searchOrg/risk-engine': {
        'dynamic-level-header':'Risk Engine',
        'general-risk-analytics': {
            '/assign-channels': 'Assign Channels and Configure Default Account Types',
            '/miscellenousConfiguartions': 'Miscellenous Configurations',
            '/customize-report-view': 'Customize Report Views',
            '/conf-org-rel': 'Configure Organization Relation',
            '/enable-data-feed': 'Enable Data Feed Configuration'
        },
        'Ruleset Management': {
            '/create-ruleset':'Create Ruleset',
            '/assign-ruleset': 'Assign Ruleset',
            '/create-list': 'Create List',
            '/edit-list': 'Edit List'
        },
        'Rules Management': {
            '/Rules-Scoring':'Rules and Scoring Management',
            '/manage-list-data': 'Manage List Data and Category Mappings',
            '/model-config': 'Model Configuration',
            '/callout-config': 'Callout Configuration'
        },
        'Migrate to Production':{
            '/migrate-prod':'Migrate to Production'
        },
        'dynamiclevel':true
    }
 }