import React, { Component } from 'react';
import SwappableGrid from '../../shared/components/SwapGrid/swappingComponent';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import './configOrgRelation.scss';
import _ from 'underscore';
class ConfigOrgRelation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scopeAll: false,
      showPopUp: false,
      defaultLeftItems: [],
      defaultRightItems: [],
      parentOrg: ""
    }
  }
  componentWillUnmount = () => {
    this.props.activateSuccessList(false, '');
    this.props.activateErrorList(false, '');
  }
  componentDidMount = async () => {
    this.getRiskFortConfig();
  }
  getRiskFortConfig = async () => {
    const urlParams = new URLSearchParams(window.location.search);
    const orgName = urlParams.get('orgname');
    const riskFortConfig = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL.getAvailableOrgs}`,
      data: {
        "existingChildOrgNumber": 0,
        "orgName": orgName,
        "selectedOrgsList": []
      }
    }
    const getRiskFortConfigStatus = await getService(riskFortConfig);
    if (getRiskFortConfigStatus.data.orgNames) {
      this.setState({ defaultLeftItems: getRiskFortConfigStatus.data.orgNames, defaultRightItems: getRiskFortConfigStatus.data.selectedOrgs, parentOrg: getRiskFortConfigStatus.data.parentOrg })
    }
  }
  confirmSave = () => {
    const selectedRightItems = this.refs.swappableGridRef && this.refs.swappableGridRef.getRightItems();
    if (selectedRightItems.length) {
      window.scrollTo(0, 0);
      this.setState({ modalColor: true, showPopUp: true });
    }
  }
  handleClose = () => {
    this.setState({ modalColor: false, showPopUp: false });
  }
  saveConfigure = async () => {
    const selectedRightItems = this.refs.swappableGridRef.getRightItems();
    const urlParams = new URLSearchParams(window.location.search);
    const orgName = urlParams.get('orgname');
    const { defaultRightItems } = this.state;
    const updateChildOrgs = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL.updateChildOrgs}`,
      data: {
        "existingChildOrgNumber": defaultRightItems.length ? defaultRightItems.length : 0,
        "orgName": orgName,
        "selectedOrgsList": selectedRightItems
      }
    }
    const updateChildOrgsStatus = await getService(updateChildOrgs);
    if (updateChildOrgsStatus.status === 200) {
      this.props.activateSuccessList(true, updateChildOrgsStatus.data);
      this.props.activateErrorList(false, '');
      this.setState({ defaultRightItems: [], defaultLeftItems: [] });
      this.handleClose();
      this.getRiskFortConfig();
    } else {
      this.props.activateErrorList(true, updateChildOrgsStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  reset() {
    window.location.reload();
  }
  render() {
    const { defaultLeftItems, defaultRightItems, modalColor, showPopUp, parentOrg } = this.state;
    const urlParams = new URLSearchParams(window.location.search);
    const selectedRightItems = this.refs.swappableGridRef && this.refs.swappableGridRef.getRightItems();
    const orgName = urlParams.get('orgname');
    return <div className="main config-org-relation">
      <div className={`${modalColor ? 'dialog-mask' : ''}`} ></div>
      <div className="no-padding">
        <h2 className="title">{RA_STR.configOrgRelationTitle}</h2>
        <p className="desc">{RA_STR.configRelationBody}</p>
        <p>{RA_STR.configRelationText} [{parentOrg ? parentOrg : RA_STR.ruleNone}]</p>
        <div className="row">
          <SwappableGrid ref="swappableGridRef" leftItems={defaultLeftItems} rightItems={defaultRightItems} scopeAll={this.state.scopeAll} leftHeader={'Available Organizations'} rightHeader={'Child Organizations of Current Organization'} hideLeftIcons={true} checkMaxLimit={true}></SwappableGrid>
        </div>
        <div className="form-group form-submit-button row mt-5 ml-2">
          <input className="custom-secondary-btn" type="submit" value="RESET" onClick={this.reset} />
          <input className="secondary-btn ml-3" type="submit" value="SAVE" onClick={this.confirmSave}></input>
        </div>
      </div>
      {showPopUp ?
        <div id="myModal" className="modal">
          <div className="modal-content">
            <div className="dialog-header" className="successheader">
              <div className="dialog-title"><span className="label">Configure Organization Relationship</span></div>
              <div className="dialog-close" className="close" onClick={this.handleClose}> <span className="close">&times;</span></div>
            </div>
            <div className="modal-body">
              <p>{RA_STR.confirmConfig}</p>
              <div className="col-md-12 row ">
                <div className="col-md-8 no-padding"><b>{orgName}(parent)</b></div>
                <div className="col-md-4">
                  <div>{RA_STR.childOrgSelected} : {selectedRightItems && selectedRightItems.length ? _.difference(selectedRightItems, defaultRightItems).length : 0}</div>
                  <div>{RA_STR.totalChildSelected} : {defaultRightItems && defaultRightItems.length ? selectedRightItems.length : selectedRightItems && selectedRightItems.length ? selectedRightItems.length : 0}</div>
                </div>
                <span className="seperate-line"></span>
                <div className="childOrgList">
                  <table className="childOrgListClass">
                    <tbody>
                      <tr>
                        {selectedRightItems && _.difference(selectedRightItems, defaultRightItems).map((eachItem) => {
                          return <td class="borderClass">{eachItem}</td>
                        })}
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="form-group mt-4">
                  <input className="custom-secondary-btn" type="submit" value="NO" onClick={this.handleClose}></input>
                  <input className="secondary-btn ml-3" type="submit" value="YES" onClick={this.saveConfigure}></input>
                </div>
              </div>
            </div>
          </div></div>
        : ''}
    </div>;
  }
}
export default ConfigOrgRelation;
