import React, { Component } from 'react';
import DateRange from '../../shared/components/DateRange/DateRange';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import Select from '../../shared/components/Select/Select';
import { saveAs } from 'file-saver';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import './RiskActivityReport.scss';
class RiskActivityReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showErrorMessage: false,
      orgName: '',
      pageNo: 0,
      pageSize: 10,
      displayReportList: [],
      activePageNum: 1,
      totalCount: 0,
      enableOrg: true,
      decryptionNeeded: true,
      getDateItems: [],
      channelOptions: [],
      orgOptions: [],
      accountTypes: [],
      navbarData: [],
      selectedChannel: '',
      selectedAccount: '',
      userIdentify: '',
      showItems: false,
      defaultSelectedNav: '3DSECURE',
      columns: [
        { title: RA_STR.risk_detail_dateLogged, field: 'dateLoggedStr' },
        { title: RA_STR.riskUsername, field: 'userName' },
        { title: RA_STR.riskOrgName, field: 'orgName' },
        { title: RA_STR.riskTnxType, field: 'txnType' },
        { title: RA_STR.riskStatus, field: 'status' },
        { title: RA_STR.riskScore, field: 'score' },
        { title: RA_STR.riskAdviceId, field: 'adviceId' },
        { title: RA_STR.riskMatch, field: 'matchedRule' },
        { title: RA_STR.riskSecAuth, field: 'secAuthResult' },
        { title: RA_STR.riskTnxStatus, field: 'txnStatus' },
        { title: RA_STR.riskConfigName, field: 'configName' },
        { title: RA_STR.riskAction, field: 'action' },
        { title: RA_STR.riskCallerId, field: 'callerId' },
        { title: RA_STR.riskTnxID, field: 'txId' },
        { title: RA_STR.riskSessionID, field: 'sessionId' },
        { title: RA_STR.riskInstanceID, field: 'instanceId' },
        { title: RA_STR.riskCountry, field: 'country' },
        { title: RA_STR.riskClientID, field: 'clientIpAddress' },
        { title: RA_STR.riskHttp, field: 'httpDeviceId' },
        { title: RA_STR.riskFlash, field: 'flashDeviceId' },
        { title: RA_STR.riskOutgoingDevice, field: 'deviceIdOut' },
        { title: RA_STR.riskDeviceType, field: 'deviceTypeStr' },
        { title: RA_STR.riskDeviceStatus, field: 'deviceIDStatusStr' },
        { title: RA_STR.riskAllRule, field: 'resultAddOnRules' },
        { title: RA_STR.riskAccountType, field: 'accountType' },
        { title: RA_STR.riskAccountID, field: 'accountId' },
        { title: RA_STR.riskExemption, field: '' },
        { title: RA_STR.riskOverload, field: '' },
        { title: RA_STR.riskRuleQualify, field: '' }
      ],
      showTable: false
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  componentDidMount = async () => {
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      this.setState({ orgOptions: orgOptions });

    }
  }
  getSelectedOrgDetails = async (getSelectedOrgDetails, getType) => {
    if (getType === 'click' || getType === 'down') {
      this.setState({
        orgName: getSelectedOrgDetails,
        showItems: true
      }, async () => {
        const checkChannels = {
          method: 'GET',
          url: `${serverUrl}/common/${getSelectedOrgDetails}/channels`
        };
        const checkChannelsStatus = await getService(checkChannels);
        if (checkChannelsStatus && checkChannelsStatus.status === 200) {
          var arrNames = [];
          var navBarArray = [];
          arrNames.push({
            'content': 'All Channels',
            'key': 'All'
          });
          checkChannelsStatus.data && Object.keys(checkChannelsStatus.data).forEach(function (key) {
            var roleName = checkChannelsStatus.data[key]["displayName"];
            arrNames.push({
              'content': roleName,
              'key': checkChannelsStatus.data[key]["name"]
            });
            navBarArray.push({
              'content': roleName,
              'key': checkChannelsStatus.data[key]["name"]
            });
          });
          this.setState({ channelOptions: arrNames, navbarData: navBarArray });
          const fetchOrg = {
            method: 'GET',
            url: `${serverUrl}/organization/${getSelectedOrgDetails}`
          };
          const fetchOrgStatus = await getService(fetchOrg);
          var arrAccountNames = [];
          if (fetchOrgStatus.status === 200) {
            fetchOrgStatus.data.org.accountTypes && Object.keys(fetchOrgStatus.data.org.accountTypes).forEach(function (key) {
              var accountNames = fetchOrgStatus.data.org.accountTypes[key]["displayName"];
              arrAccountNames.push({
                'content': accountNames,
                'key': fetchOrgStatus.data.org.accountTypes[key]["name"]
              });
            });
            this.setState({ accountTypes: arrAccountNames });
          }
        }
      })
    }
  }
  downloadCSV = (response) => {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'RiskDetailActivityReport.csv');
    }
  }
  getActivePage = (getPageNum) => {
    let pageNumCount = getPageNum - 1;
    this.setState({
      pageNo: pageNumCount, activePageNum: getPageNum
    }, () => {
      this.displayReport();
    })
  }
  displayReport = async () => {
    let getDateItems = '';
    if (this.state.getDateItems.length === 0) {
      getDateItems = this.refs.daterange.getDates();
    } else {
      getDateItems = this.state.getDateItems;
    }
    const { pageNo, pageSize, accountTypes, decryptionNeeded, orgName, selectedChannel, userIdentify, selectedAccount } = this.state;
    const data = {
      fromDate: getDateItems[0].startDate,
      toDate: getDateItems[0].endDate,
      decryptionNeeded: decryptionNeeded,
      orgName: orgName,
      pageNo: pageNo,
      pageSize: pageSize,
      selectedChannel: selectedChannel ? selectedChannel : 'All',
      userName: userIdentify,
      accType: selectedAccount ? selectedAccount : accountTypes.length ? '-1000' : ''
    }
    const getDisplayReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['riskEvaluationDisplay']}`,
      data: data
    };
    const getDisplayReportStatus = await getService(getDisplayReport);
    if (getDisplayReportStatus.status === 200) {
      if (getDisplayReportStatus.data.noOfRecords === 0) {
        this.setState({ showErrorMessage: true });
      } else {
        this.setState({ showErrorMessage: false });
      }
      this.props.activateErrorList(false, '');
      this.setState({ displayReportList: getDisplayReportStatus.data.detailActivities, showTable: true, totalCount: getDisplayReportStatus.data.noOfRecords, getDateItems: getDateItems });
    } else {
      this.props.activateErrorList(true, getDisplayReportStatus.data.errorList);
    }
  }
  exportData = async () => {
    let getDateItems = '';
    if (this.state.getDateItems.length === 0) {
      getDateItems = this.refs.daterange.getDates();
    } else {
      getDateItems = this.state.getDateItems;
    }
    const { pageNo, pageSize, decryptionNeeded, accountTypes, orgName, selectedChannel, userIdentify, selectedAccount } = this.state;
    const data = {
      fromDate: getDateItems[0].startDate,
      toDate: getDateItems[0].endDate,
      decryptionNeeded: decryptionNeeded,
      orgName: orgName,
      pageNo: pageNo,
      pageSize: pageSize,
      selectedChannel: selectedChannel ? selectedChannel : 'All',
      userName: userIdentify,
      accType: selectedAccount ? selectedAccount : accountTypes.length ? '-1000' : ''
    }
    const getExportData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['riskEvaluationExport']}`,
      data: data
    };
    const getExportDataStatus = await getService(getExportData);
    if (getExportDataStatus.status === 200) {
      this.downloadCSV(getExportDataStatus);
    }
  }
  onchange = (e) => {
    let getValue = e.target;
    if (getValue.checked) {
      this.setState({ decryptionNeeded: true });
    } else {
      this.setState({ decryptionNeeded: false });
    }
  }
  togglePage = () => {
    window.location.reload();
  }
  handleChange = (e) => {
    if (e.target.name === "Select-Channel") {
      this.setState({ selectedChannel: e.target.value });
    } else {
      this.setState({ selectedAccount: e.target.value });
    }
  }
  handleUserIdentification = (e) => {
    this.setState({ userIdentify: e.target.value });
  }
  changeNavBar = (getType) => {
    this.setState({ defaultSelectedNav: getType, selectedChannel: getType }, () => {
      this.displayReport();
    });
  }
  render() {
    const { displayReportList, defaultSelectedNav, navbarData, showErrorMessage, columns, accountTypes, enableOrg, orgOptions, pageSize, totalCount, activePageNum, showTable, getDateItems, showItems, channelOptions, decryptionNeeded } = this.state;
    return <div className="main mt-2 risk-details">
      {showErrorMessage ? <div className="edl_error_feedback"><span>{RA_STR.riskErrorMsg}</span></div> : ''}
      <h2 className="title">{RA_STR.risk_detail_header}</h2>
      {!showTable ?
        <div><p className="desc">{RA_STR.risk_detail_body}</p>
          <hr />
          <div className="col-md-6">
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">{RA_STR.ruleOrgName}</label>
              <div className='col-sm-8'>
                <AutoSuggest orgOptions={orgOptions} getSelectedOrgDetails={this.getSelectedOrgDetails} enableAutosuggest={enableOrg} defaultOrganizationPlaceholder={RA_STR_SELECT} />
              </div>
            </div>
          </div>
          {showItems ?
            <div className="col-md-6">
              <Select
                name={'Select-Channel'}
                title={'Select Channel'}
                options={channelOptions ? channelOptions : ''}
                controlFunc={this.handleChange}
              />
            </div> : ''}
          {showItems ? <div>
            <div className="col-md-12 row">
              <div className="col-md-6">
                <div class="form-group row">
                  <label class="col-sm-4 col-form-label">{RA_STR.riskUserIdentify}</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" onChange={this.handleUserIdentification} />
                  </div>
                </div>
              </div>
              {accountTypes.length ? <div className="col-md-4">
                <select name="accountTypes" className="form-control" onChange={this.handleChange}>
                  <option value="-1000">{RA_STR.riskUsertType}</option>
                  <option value="-1000">{RA_STR.riskUsername}</option>
                  {accountTypes && accountTypes.map(opt => {
                    return (
                      <option
                        key={opt.key}
                        value={opt.key}>{opt.content}</option>
                    );
                  })}
                </select>
              </div> : ''}
            </div>
            <div className="row">
              <div className="col-sm-3">
                <label className="col-sm-8 col-form-label mt-3">{RA_STR.timePeriod}</label>
              </div>
              <DateRange ref="daterange"></DateRange>
            </div>
            <div className="row mt-5">
              <div className="col-sm-3">
                <Checkbox
                  type={'checkbox'}
                  options={[RA_STR.DecryptInfo]}
                  controlFunc={this.onchange}
                  selectedOptions={decryptionNeeded ? [RA_STR.DecryptInfo] : []} />
              </div>
              <div className="col-sm-9 no-padding">
                <div className="form-group row">
                  <input className="secondary-btn" id="searchButton" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
                </div>
              </div>
            </div ></div> : ''}
        </div> :
        <div>
          <div className="col-md-12 row">
            <div className="col-md-9 no-padding">
              <p>{RA_STR.riskInnerBody}</p>
            </div>
            <div className="float-right col-md-3">
              {totalCount ? <div><input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.togglePage}></input></div> : ""}
            </div>
          </div>
          <div className="col-md-4 no-padding">
            <div className="form-group form-inline">
              <label className="col-sm-4 col-form-label">{RA_STR.noOfRecords}</label>
              <div className="col-sm-8">
                {totalCount}
              </div>
            </div>
            <div className="form-group form-inline">
              <label className="col-sm-4 col-form-label">From:{getDateItems[0].startDate}</label>
              <div className="col-sm-8">
                {`To:${getDateItems[0].endDate}`}
              </div>
            </div>
          </div>
          <div className="tabToggle">
            <ul className="nav nav-pills form-inline">
              {navbarData && navbarData.map((eachNav) => {
                return <li className={(eachNav.key === defaultSelectedNav) ? 'custom-secondary-btn' : 'dynamiclist-anchor'} name={eachNav.key} onClick={this.changeNavBar.bind(this, eachNav.key)}>{eachNav.content}</li>
              })}
            </ul>
          </div>
        </div>
      }
      {showTable ?
        <div className="tabele-buttons">
          <DynamicTable columns={columns} data={displayReportList} enablePagination={true} pageSize={pageSize} totalCount={totalCount} activePage={this.getActivePage} activePageNumNew={activePageNum} /></div> : ''}
    </div>
  }
}

export default RiskActivityReport;
