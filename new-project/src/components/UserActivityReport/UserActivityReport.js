import React, { Component, Fragment } from 'react';
import AdminReport from '../Reports-admin/adminReport';
import { RA_API_URL, serverUrl } from '../../shared/utlities/constants';

class UserActivityReport extends Component {
  componentWillMount() {
    this.columns = [
      { title: 'Date', field: 'date' },
      { title: 'User ID', field: 'userId' },
      { title: 'Account Type', field: 'accountType' },
      { title: 'Account ID', field: 'accountId' },
      { title: 'Event Type', field: 'eventType' },
      { title: 'Organization', field: 'organization' },
      { title: 'Status', field: 'status' },
      { title: 'Transaction ID', field: 'transactionId' },
      { title: 'Reason', field: 'reason' },
      { title: 'Client IP Address', field: 'clientIPAddress' },
      { title: 'Caller ID', field: 'callerId' },
    ];
    this.displayReportText='This report displays the user activities. You can filter the result based on event type';
  }
  render() {
    return <Fragment>
      <AdminReport reportType="user-activity-report" columns={this.columns} displayreportUrl={serverUrl + RA_API_URL.userActivityReport} exportreportUrl={serverUrl + RA_API_URL.exportUserActivityReportInCSV} displayReportText={this.displayReportText}></ AdminReport>
    </Fragment>

  }
}

export default UserActivityReport;
