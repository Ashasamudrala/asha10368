import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './ManageListCategoryMappingRA.scss';

import IpRanges from '../../shared/components/IpRanges/IpRanges';
import TrustedIpRanges from '../../shared/components/TrustedIpRanges/TrustedIpRanges';

import Select from '../../shared/components/Select/Select';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';

import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';

import CA_Utils from '../../shared/utlities/commonUtils';

import _ from 'underscore';

class ManageListCategoryMappingRA extends Component {

  constructor(props) {
    super(props);
    this.state = {
      defaultRuleSet: '',
      ruleOptions: [],
      organization: '',
      config: '',
      manageListCateogory: 0,
      manage: 'listData',
      defaultlistType: '',
      listTypesMetadata: [],
      noListFound: false,

      selectOptions: {},
      optionDetails: {},
      actualOptionDetails: [],
      selectedFileOption: 'append',
      dataFile: [],
      format: '',
      selectedIPOption: 'selectedMask',
      informationSource: '',
      defaultAggregator: '',
      aggregatorData: [],
      newAggregator: '',
      aggratorIdMetadata: {},
      trustedIpArray: [],

      categoryMetadata: [],
      defaultCategory: '',
      classificationData: '',
      dataset: '',
    }
  }

  componentDidMount() {
    this.getRuleSets();
  }

  getRuleSets = async () => {
    const urlParams = new URLSearchParams(window.location.search);
    const getOrgName = urlParams.get('orgname');
    let orgName = '';
    if (getOrgName) {
      orgName = getOrgName;
    } else {
      orgName = RA_STR.ruleOrg;
    }
    // const orgName = JSON.parse(localStorage.getItem('orgName'));

    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}/${orgName}${RA_API_URL['getRuleset']}`
    };
    const response = await getService(url);
    if (response && response.status === 200) {
      if (response.data && response.data.configNamesList) {
        const ruleSets = CA_Utils.objToArray(response.data.configNamesList, 'object');
        const organization = ruleSets[0].content.split('-') ? ruleSets[0].content.split('-')[0].trim() : '';
        const config = ruleSets[0].content.split('-') ? ruleSets[0].content.split('-')[1].trim() : '';
        this.setState(
          { ruleOptions: ruleSets, defaultRuleSet: ruleSets[0].key, organization: organization, config: config },
          () => this.getList()
        );

      }
    }
  };

  getList = async () => {
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['listtype']}`
    }
    const response = await getService(url);
    if (response && response.status === 200) {
      const lists = CA_Utils.objToArray(response.data.listTypes, 'object');
      this.setState(
        { listTypesMetadata: lists, defaultlistType: lists[0].key },
        () => this.getListTypeData(lists[0].key)
      );
    }
  }

  getListTypeData = async (listType) => {
    const { organization, config } = this.state;
    if (listType === 'NEGATIVECOUNTRY') {
      const url = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL['manageListData']}${RA_API_URL['negativeCountries']}${RA_API_URL['getOrgUrl']}/${organization}${RA_API_URL['config']}/${config}`
      }
      const response = await getService(url);
      if (response && response.status === 200) {
        this.setState({ noListFound: false });
        const avaialableCountries = [];
        const selectedCountries = {};
        if (response.data) {
          if (response.data.availableCountries.length) {
            response.data.availableCountries.forEach((obj) => {
              avaialableCountries.push(obj.country);
            });
          }
          if (response.data.availableCountries.length) {
            response.data.negativeCountries.forEach((obj) => {
              selectedCountries[obj['countryCode']] = obj.country;
            });
          }
        }
        this.setState({ actualOptionDetails: response.data.availableCountries, optionDetails: avaialableCountries, selectOptions: selectedCountries })
      }
    }

    if (listType === 'TRUSTEDIP') {
      this.setState({ noListFound: false });
      this.getTrustedIPs('TRUSTEDIP');
    }

    if (listType === 'TRUSTEDAGGREGATOR') {
      this.setState({ noListFound: false });
      this.getTrustedAggregatorList();
    }

    if (listType === 'SIMPLE_LIST_LOOKUP') {
      this.getOthersList();
    }

    if (listType === 'SECURE_LIST_LOOKUP') {
      this.getSecureList();
    }

  }

  getTrustedIPs = async (listType) => {
    const { organization, config, defaultAggregator } = this.state;
    let restUrl = '';

    switch (listType) {
      case 'TRUSTEDIP':
        restUrl = `${serverUrl}${RA_API_URL['trustedip']}/${organization}${RA_API_URL['config']}/${config}?startPageIndex=1`;
        break;
      case 'TRUSTEDAGGREGATOR':
        const aggregator = _.findWhere(this.state.aggregatorData, { key: defaultAggregator });
        restUrl = `${serverUrl}${RA_API_URL['trustedaggregatorlist']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['aggregator']}/${aggregator.content}?pageStartIndex=1`;
        break;
    }

    const requestBody = {
      method: 'GET',
      url: restUrl
    }

    const response = await getService(requestBody);

    if (response && response.status === 200) {
      if (listType === 'TRUSTEDAGGREGATOR') {
        const trustedIps = response.data.ipAddressBeanList ? JSON.parse(JSON.stringify(response.data.ipAddressBeanList)) : [];
        this.setState({
          trustedIpArray: trustedIps,
          aggratorIdMetadata: response.data.aggregatorDetails
        }, () => {
          if (this.state.trustedIpArray.length) {
            this.refs.ipRangesDOM.updateIpRanges();
          }
        });
      } else if (listType === 'TRUSTEDIP') {
        if (response.data) {
          this.setState({ trustedIpArray: JSON.parse(JSON.stringify(response.data)) }, () => {
            if (this.state.trustedIpArray.length) {
              this.refs.ipRangesDOM.updateIpRanges();
            }
          });
        }
      }
    }
  }

  onSelectRuleSet = (e) => {
    const { ruleOptions } = this.state;
    const organization = ruleOptions[e.target.value].content.split('-') ? ruleOptions[e.target.value].content.split('-')[0].trim() : '';
    const config = ruleOptions[e.target.value].content.split('-') ? ruleOptions[e.target.value].content.split('-')[1].trim() : '';
    this.setState(
      { defaultRuleSet: e.target.value, organization: organization, config: config, defaultlistType: this.state.listTypesMetadata[0].key },
      () => {
        this.getListTypeData(this.state.listTypesMetadata[0].key);
        this.resetToast();
      }
    );
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  toggleManage = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    // const { manage } = this.state;
    if (e.target.value === 'listData') {
      this.setState({ defaultlistType: this.state.listTypesMetadata[0].key }, () => this.getListTypeData(this.state.defaultlistType));
      this.resetToast();
    } else if (e.target.value === 'categoryMap') {
      this.getCategories();
      this.resetToast();
    }
  }

  getCategories = async () => {
    const { organization, config } = this.state;
    const request = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['categoryList']}/${organization}${RA_API_URL['config']}/${config}`
      // url: `${serverUrl}${RA_API_URL['categoryList']}/RFAUTOORG14${RA_API_URL['config']}/DEFAULT`
    }
    let response = await getService(request);
    if (response.status === 200) {
      if (response.data.dataSet.length) {
        let categories = CA_Utils.objToArray(response.data.dataSet, 'object');
        this.setState({ categoryMetadata: categories, defaultCategory: categories[0].key, dataset: categories[0].content });
      }
    }
  }

  onSelectlistType = (e) => {
    this.setState(
      { defaultlistType: e.target.value, informationSource: '' },
      () => {
        this.getListTypeData(this.state.defaultlistType);
        this.resetToast();
      }
    );
  }

  onSelectCategoryType = (e) => {
    const element = this._findWhere(this.state.categoryMetadata, e.target.value);
    this.setState({ defaultCategory: e.target.value, dataset: element });
  }

  saveNegativeCountriesList = async () => {
    const { organization, config } = this.state;
    const tempSelection = Object.values(this.state.selectOptions);
    const tempCountries = [];
    tempSelection.forEach(country => {
      const selectedCountry = _.findWhere(this.state.actualOptionDetails, { country: country });
      tempCountries.push(selectedCountry);
    })
    const data = {
      negativeCountries: tempCountries
    }
    const url = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_URL['manageListData']}${RA_API_URL['negativeCountries']}${RA_API_URL['getOrgUrl']}/${organization}${RA_API_URL['config']}/${config}`,
      data: data
    }
    const response = await getService(url);
    if (response && response.status === 200) {
      this.toastMessage('success', response.data);
    } else {
      this.toastMessage('error', response.data.errorList);
    }
  }

  handledoubleRightChange = () => {
    let item = [];
    if (this.state.optionDetails.length !== 0) {
      for (let i = 0; i < Object.keys(this.state.optionDetails).length; i++) {
        item = Object.keys(this.state.optionDetails)[i];
        this.state.selectOptions[item] = this.state.optionDetails[item];
        delete this.state.optionDetails[item];
        i = i - 1;
      }
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions })
  }

  handledoubleLeftChange = () => {
    let item = [];
    if (this.state.selectOptions.length !== 0) {
      for (let i = 0; i < Object.keys(this.state.selectOptions).length; i++) {
        item = Object.keys(this.state.selectOptions)[i];
        this.state.optionDetails[item] = this.state.selectOptions[item];
        delete this.state.selectOptions[item]
        i = i - 1;
      }
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions })
  }

  handleLeftChange = (node) => {
    var options = [].slice.call(node.querySelectorAll('option'));
    var selected = options.filter(function (option) {
      return option.selected;
    });
    var selectedValues = selected.map(function (option) {
      return { "value": option.value, "id": option.id, "text": option.text };
    });
    for (let i = 0; i < selectedValues.length; i++) {
      delete this.state.optionDetails[selectedValues[i].value]
      this.state.selectOptions[selectedValues[i].value] = selectedValues[i].text;
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions });
  }

  handleRightChange = (node) => {
    var options = [].slice.call(node.querySelectorAll('option'));
    var selected = options.filter(function (option) {
      return option.selected;
    });
    var selectedValues = selected.map(function (option) {
      return { "value": option.value, "id": option.id, "text": option.text };
    });
    for (let i = 0; i < selectedValues.length; i++) {
      delete this.state.selectOptions[selectedValues[i].value]
      this.state.optionDetails[selectedValues[i].value] = selectedValues[i].text;
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions });
  }

  shownextelement = (ref, e) => {
    this.setState({ [e.target.name]: e.target.value });
    if (ref && e.target.value.length === 3) {
      let node = ReactDOM.findDOMNode(this.refs[ref]);
      node.focus();
    }
  }

  showifnextelement = (selectedIPOption, mask1, endIpAddress1, e) => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.value.length === 3) {
      let node = selectedIPOption === 'selectedMask' ?
        ReactDOM.findDOMNode(this.refs[mask1]) : ReactDOM.findDOMNode(this.refs[endIpAddress1]);
      node.focus();
    }
  }

  onFileChange = (e) => {
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      reader.onload = () => {
        const intArray = new Int8Array(reader.result);
        this.setState({ dataFile: intArray });
      }
      reader.readAsArrayBuffer(file);
    }
    else {
      this.setState({ dataFile: [] })
    }
  }

  uploadDataFile = async () => {
    const { defaultlistType, organization, config, manage, dataset, classificationData } = this.state;
    let reqUrl = '';
    const data = {};

    if (manage === 'listData') {
      if (defaultlistType === 'UNTRUSTEDIP') {
        reqUrl = `${serverUrl}${RA_API_URL['untrustedip']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['upload']}`;
        data.file = Object.values(this.state.dataFile);
        data.overwriteOrAppendData = (this.state.selectedFileOption === 'replace') ? 1 : 2;
      }
      else if (defaultlistType === 'SIMPLE_LIST_LOOKUP') {
        reqUrl = `${serverUrl}${RA_API_URL['othersList']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['dataset']}/${dataset}`;
        data.listData = classificationData ? this.unpack(classificationData) : Object.values(this.state.dataFile);
        data.overwriteListDataSet = (this.state.selectedFileOption === 'replace') ? true : false;
        data.appendToProductionListDataSet = (this.state.selectedFileOption === 'replace') ? false : true;
      }
      if (defaultlistType === 'SECURE_LIST_LOOKUP') {
        reqUrl = `${serverUrl}${RA_API_URL['secureList']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['dataset']}/${dataset}`;
        data.listData = classificationData ? this.unpack(classificationData) : Object.values(this.state.dataFile);
        data.overwriteListDataSet = (this.state.selectedFileOption === 'replace') ? true : false;
        data.appendToProductionListDataSet = (this.state.selectedFileOption === 'replace') ? false : true;
      }
    }

    else if (manage === 'categoryMap') {
      // const organization = 'RFAUTOORG14', config = 'DEFAULT';
      reqUrl = `${serverUrl}${RA_API_URL['categoryListUpload']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['dataset']}/${dataset}`;
      data.listData = classificationData ? this.unpack(classificationData) : Object.values(this.state.dataFile);
      data.overwriteListDataSet = (this.state.selectedFileOption === 'replace') ? true : false;
      data.appendToProductionListDataSet = (this.state.selectedFileOption === 'replace') ? false : true;
    }
    const fileUpload = {
      method: 'PUT',
      url: reqUrl,
      data: data
    }

    const response = await getService(fileUpload);
    if (response && response.status === 200) {
      this.setState({ file: [], classificationData: '' });
      document.querySelector('#input-field').value = '';
      this.toastMessage('success', response.data);
    } else {
      this.setState({ file: [] });
      document.querySelector('#input-field').value = '';
      this.toastMessage('error', response.data.errorList);
    }
  }

  IPRangeSubmit = async (action) => {
    const { organization, config, informationSource, defaultAggregator } = this.state;
    const ipObj = this.refs.IpAddressRanges.returnIpAddress();
    let REST;

    const data = {
      endIpAddr1: ipObj.endIpAddr1,
      endIpAddr2: ipObj.endIpAddr2,
      endIpAddr3: ipObj.endIpAddr3,
      endIpAddr4: ipObj.endIpAddr4,
      source: informationSource,
      ipAddr1: ipObj.ipAddr1,
      ipAddr2: ipObj.ipAddr2,
      ipAddr3: ipObj.ipAddr3,
      ipAddr4: ipObj.ipAdd4,
      mask1: ipObj.mask1,
      mask2: ipObj.mask2,
      mask3: ipObj.mask3,
      mask4: ipObj.mask4,
      maskOrEndIPSelection: ipObj.selectedIPOption === 'mask' ? 'mask' : 'endip',
    };
    const payload = [];
    payload.push(data);

    switch (action) {
      case 'untruestedAdd':
        REST = {
          method: 'POST',
          url: `${serverUrl}${RA_API_URL['untrustedip']}/${organization}${RA_API_URL['config']}/${config}`,
          data: payload
        };
        break;
      case 'untruestedDelete':
        REST = {
          method: 'DELETE',
          url: `${serverUrl}${RA_API_URL['untrustedip']}/${organization}${RA_API_URL['config']}/${config}`,
          data: payload
        };
        break;
      case 'truestedAdd':
        REST = {
          method: 'POST',
          url: `${serverUrl}${RA_API_URL['trustedip']}/${organization}${RA_API_URL['config']}/${config}`,
          data: payload
        };
        break;
      case 'aggrTruestedAdd':
        const aggregator = _.findWhere(this.state.aggregatorData, { key: defaultAggregator });
        REST = {
          method: 'POST',
          url: `${serverUrl}${RA_API_URL['trustedaggregatorlist']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['aggregator']}/${aggregator.content}`,
          data: payload
        };
        break;
    }
    const response = await getService(REST);
    if (response && response.status === 200) {
      this.toastMessage('success', response.data);
      if (action === 'truestedAdd') {
        this.getTrustedIPs('TRUSTEDIP');
      } else if (action === 'aggrTruestedAdd') {
        this.getTrustedIPs('TRUSTEDAGGREGATOR');
      }
    } else {
      this.toastMessage('error', response.data.errorList);
    }
  };

  handleAggregatorChange = (e) => {
    this.setState({ defaultAggregator: e.target.value }, () => {
      this.getTrustedIPs('TRUSTEDAGGREGATOR')
    });
  }

  getTrustedAggregatorList = async () => {
    const { organization, config } = this.state;
    const body = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['trustedaggregatorlist']}/${organization}${RA_API_URL['config']}/${config}`,
    }
    const response = await getService(body);
    if (response && response.status === 200) {
      let newAggregatorData = [{ key: '', content: '-Add New Aggregator-' }];
      let newDefaultAggregator = '';
      if (response.data && response.data.length) {
        const newArray = CA_Utils.objToArray(response.data, 'object');
        newAggregatorData = [...newAggregatorData, ...newArray];
        newDefaultAggregator = newArray[0].key;

      }
      this.setState({ aggregatorData: newAggregatorData, defaultAggregator: newDefaultAggregator },
        () => {
          if (response.data && response.data.length) {
            this.getTrustedIPs('TRUSTEDAGGREGATOR');
          }
        }
      );
    } else {
      this.setState({ aggregatorData: [{ key: '', content: '-Add New Aggregator-' }] })
    }
  }

  _findWhere(array, value) {
    // const { aggregatorData, defaultAggregator } = this.state;
    const element = _.findWhere(array, { key: value });
    return element.content;
  }

  unpack(string) {
    var bytes = [];
    for (var i = 0, n = string.length; i < n; i++) {
      var char = string.charCodeAt(i);
      bytes.push(char >>> 8, char & 0xFF);
    }
    return bytes;
  }

  addAggregator = async () => {
    const { organization, config, newAggregator } = this.state;
    const body = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['addAggregator']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['aggregator']}/${newAggregator}`,
      data: {
        newAggregator: newAggregator
      }
    }
    const response = await getService(body);
    if (response && response.status === 200) {
      this.getTrustedAggregatorList();
      this.toastMessage('success', response.data);
    } else {
      this.toastMessage('error', response.data.errorList);
    }
  }

  updateAggregatorID = async () => {
    const { organization, config, aggregatorData, defaultAggregator } = this.state;
    const aggregator = this._findWhere(aggregatorData, defaultAggregator);
    const reqUrl = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_URL['trustedaggregatorlist']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['aggregator']}/${aggregator}`
    };
    const response = await getService(reqUrl);
    this.getTrustedIPs('TRUSTEDAGGREGATOR');
  }

  accesIpRanges = async (action, getData) => {
    const { organization, config, defaultlistType, aggregatorData, defaultAggregator } = this.state;
    const method = action === 'save' ? 'PUT' : 'DELETE';
    let restUrl = '';
    const payload = getData;

    if (defaultlistType === 'TRUSTEDIP') {
      restUrl = `${serverUrl}${RA_API_URL['trustedip']}/${organization}${RA_API_URL['config']}/${config}`;
    } else if (defaultlistType === 'TRUSTEDAGGREGATOR') {
      const aggregator = _.findWhere(aggregatorData, { key: defaultAggregator });
      restUrl = `${serverUrl}${RA_API_URL['trustedaggregatorlist']}/${organization}${RA_API_URL['config']}/${config}${RA_API_URL['aggregator']}/${aggregator.content}`;
    }

    const requestBody = {
      method: method,
      url: restUrl,
      data: payload
    };

    const response = await getService(requestBody);
    if (response && response.status === 200) {
      this.getTrustedIPs(defaultlistType);
      this.toastMessage('success', response.data);
    } else {
      this.toastMessage('error', response.data.errorList);
    }
  }

  toastMessage = (action, msg) => {
    if (action === 'success') {
      this.props.activateSuccessList(true, msg);
      this.props.activateErrorList(false, '');
    } else if (action === 'error') {
      this.props.activateErrorList(true, msg);
      this.props.activateSuccessList(false, '');
    }
  }

  resetToast = () => {
    this.props.activateSuccessList(false, '');
    this.props.activateErrorList(false, '');
  }

  getOthersList = async () => {
    const { organization, config } = this.state
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['othersList']}/${organization}${RA_API_URL['config']}/${config}`
    }
    const response = await getService(url);
    if (response.status === 200) {
      console.log(response);
      if (response.data.dataSet.length) {
        this.setState({ noListFound: false });
        let categories = CA_Utils.objToArray(response.data.dataSet, 'object');
        this.setState({ categoryMetadata: categories, defaultCategory: categories[0].key, dataset: categories[0].content });
      } else {
        this.setState({ noListFound: true });
      }
    }
  }

  getSecureList = async () => {
    const { organization, config } = this.state
    const url = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['secureList']}/${organization}${RA_API_URL['config']}/${config}`
    }
    const response = await getService(url);
    if (response.status === 200) {
      console.log(response);
      if (response.data.dataSet.length) {
        this.setState({ noListFound: false });
        let categories = CA_Utils.objToArray(response.data.dataSet, 'object');
        this.setState({ categoryMetadata: categories, defaultCategory: categories[0].key, dataset: categories[0].content });
      } else {
        this.setState({ noListFound: true });
      }
    }
  }


  render() {
    const { ruleOptions, defaultRuleSet, listTypesMetadata, defaultlistType, manage, selectedFileOption, informationSource, defaultAggregator, aggregatorData, newAggregator, aggratorIdMetadata, trustedIpArray, noListFound, defaultCategory, categoryMetadata, classificationData } = this.state;

    return (
      <div className="main manage-list-category">
        <div className="no-padding">
          <h2 className="title">{RA_STR.manageList}</h2>
          <p className="desc">Select the Ruleset to manage list data or category mappings for the rules in the Ruleset.Data uploaded here can be viewed through Rule Data Report.</p>
        </div>
        <div className="col-md-8">
          <Select
            name={'ruleset'}
            title={'Select Existing Ruleset'}
            required={false}
            selectedOption={defaultRuleSet}
            options={ruleOptions ? ruleOptions : ['']}
            controlFunc={this.onSelectRuleSet} />
        </div>

        <div className="col-sm-8">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label ml-3"> {RA_STR.manage} </label>
            <div className="col-sm-7 col-form-label flex justify-between">
              <div className="custom-control custom-radio radioTopMargin validityPeriod">
                <input type="radio"
                  id="listData"
                  className="custom-control-input"
                  name="manage"
                  value='listData'
                  checked={manage === 'listData'}
                  onChange={this.toggleManage} />
                <label className="custom-control-label" htmlFor="listData"> {RA_STR.listData} </label>
              </div>
              <div className="custom-control custom-radio radioTopMargin validityPeriod">
                <input type="radio"
                  id="categoryMap"
                  className="custom-control-input"
                  name="manage"
                  value='categoryMap'
                  checked={manage === 'categoryMap'}
                  onChange={this.toggleManage} />
                <label className="custom-control-label" htmlFor="categoryMap"> {RA_STR.categoryMappings} </label>
              </div>
            </div>
          </div>
        </div>
        {manage === 'listData' ?
          <div>

            <div className="row">
              <div className="offset-md-3 col-md-4">
                <Select
                  name={'listType'}
                  title={'Select List Type'}
                  required={false}
                  selectedOption={defaultlistType}
                  options={listTypesMetadata ? listTypesMetadata : ['']}
                  controlFunc={this.onSelectlistType} />
              </div>
              <div className="col-md-4">
                <span> {RA_STR.selectList} {noListFound ? <i> {RA_STR.noListFound} </i> : <span> {defaultlistType} </span>} </span>
              </div>
            </div>

            {defaultlistType === 'NEGATIVECOUNTRY' ?
              <div>
                <div className="row">
                  <div className="col-md-2">
                    <span className="">{RA_STR.selectNegativeCountry}</span>
                  </div>
                  <div className="col-md-8">
                    <SwappingSelectBox selectOptions={this.state.selectOptions}
                      optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={false} />
                  </div>
                </div>
                <div className="form-group form-submit-button row">
                  <input className="secondary-btn" id="saveNegativeCountryList" type="submit" value="SAVE" onClick={this.saveNegativeCountriesList}></input>
                </div>
              </div>
              : ''}

            {defaultlistType === 'UNTRUSTEDIP' ?

              <div className="upload-untrusted-ip-ranges col-md-8">
                <div className="row">
                  <div className="col-md-4">
                    <span className="">{RA_STR.uploadUntrustedIPRanges}</span>
                  </div>
                  <div className="col-sm-4 col-form-label flex justify-between p-0">
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                      <input type="radio"
                        id="append"
                        className="custom-control-input"
                        name="selectedFileOption"
                        value='append'
                        checked={selectedFileOption === 'append'}
                        onChange={this.handleChange} />
                      <label className="custom-control-label" htmlFor="append"> {RA_STR.append} </label>
                    </div>
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                      <input type="radio"
                        id="replace"
                        className="custom-control-input"
                        name="selectedFileOption"
                        value='replace'
                        checked={selectedFileOption === 'replace'}
                        onChange={this.handleChange} />
                      <label className="custom-control-label" htmlFor="replace"> {RA_STR.replace} </label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="offset-md-4 col-md-7 mb-3">
                    <label className="col-form-label mr-3">{RA_STR.uploadDataFile}</label>
                    <input type="file" name="myFile" onChange={this.onFileChange} id='input-field' />
                  </div>
                  <div className="col-md-1">
                    <div className="form-group form-submit-button">
                      <input className="custom-default-btn" id="saveNegativeCountryList" type="submit" value="UPLOAD" onClick={this.uploadDataFile}></input>
                    </div>
                  </div>
                </div>

                <div className="add-delete-ip-range row">
                  <div className="col-md-4">
                    <span className="">{RA_STR.untrustedIPRange}</span>
                  </div>
                  <div className="col-md-8">
                    <IpRanges ref="IpAddressRanges" />

                    <div className="form-group">
                      <label> {RA_STR.informationSource} </label>
                      <input type="text" name="informationSource" maxLength="64" size="8" value={informationSource} onChange={this.handleChange} />
                    </div>
                  </div>
                </div>
                <div className="form-group form-submit-button row">
                  <input className="secondary-btn" type="submit" value="add range" onClick={this.IPRangeSubmit.bind(this, 'untruestedAdd')}></input>
                  <input className="custom-default-btn ml-3" type="submit" value="delete range" onClick={this.IPRangeSubmit.bind(this, 'untruestedDelete')}></input>
                </div>
              </div>
              : ''}

            {defaultlistType === 'TRUSTEDIP' ?
              <div>
                <div className="add-delete-ip-range row">
                  <div className="col-md-12">
                    <IpRanges ref="IpAddressRanges" />
                  </div>
                  <div className="form-group form-submit-button row">
                    <input type="submit" value="add range" className="secondary-btn" onClick={this.IPRangeSubmit.bind(this, 'truestedAdd')}></input>
                  </div>
                </div>
                {trustedIpArray.length > 0 ?
                  <TrustedIpRanges ref="ipRangesDOM" data={trustedIpArray} getIpData={this.accesIpRanges} />
                  : ''}
              </div>
              : ''}

            {defaultlistType === 'TRUSTEDAGGREGATOR' ?
              <div className="trusted-aggregator">
                <div className="row clearfix">
                  <div className="col-md-8">
                    <Select
                      name={'aggregator'}
                      title={RA_STR.aggregator}
                      required={false}
                      selectedOption={defaultAggregator}
                      options={aggregatorData ? aggregatorData : ['']}
                      controlFunc={this.handleAggregatorChange} />
                  </div>

                  {/* <div className="col-md-3">
              <label className="col-form-label"> {RA_STR.aggregator} </label>
            </div>
            <div className="col-md-3 p-0">
              <select name="aggregator" className="form-select form-control" value={defaultAggregator} onChange={this.handleAggregatorChange}>
                <option value=""> -Add New Aggregator- </option>
                {aggregatorData.map(data => <option key={data} value={data}>{data}</option>)}
              </select>
            </div> */}
                  {!defaultAggregator ?
                    <div className="col-md-4 p-0">
                      <input type="text" className="form-input form-control aggr-input" name="newAggregator" value={newAggregator} onChange={this.handleChange} />
                      <input type="submit" className="secondary-btn" value="create" onClick={this.addAggregator}></input>
                    </div>
                    : ''}
                </div>
                {defaultAggregator ?
                  <div>
                    <div className="mt-3">
                      <div className="mb-2">
                        <label className="col-md-3 col-form-label"> {RA_STR.aggregatorId1} </label>
                        <span> {aggratorIdMetadata.aid1} </span>
                      </div>
                      <div className="mb-2">
                        <label className="col-md-3 col-form-label"> {RA_STR.aggregatorId2} </label>
                        <span> {aggratorIdMetadata.aid2} </span>
                      </div>
                      <div className="mb-2">
                        <label className="col-md-3 col-form-label"> {RA_STR.aggregatorId3} </label>
                        <span> {aggratorIdMetadata.aid3} </span>
                      </div>
                      <input type="submit" className="secondary-btn" value={RA_STR.updateAggregatorID} onClick={this.updateAggregatorID}></input>
                    </div>

                    <div className="col-md-8 mt-4">
                      <div className="col-md-12">
                        <IpRanges ref="IpAddressRanges" />
                      </div>
                      <div className="form-group form-submit-button row">
                        <input type="submit" value="add range" className="secondary-btn" onClick={this.IPRangeSubmit.bind(this, 'aggrTruestedAdd')}></input>
                      </div>
                    </div>
                    {trustedIpArray.length > 0 ?
                      <TrustedIpRanges ref="ipRangesDOM" data={trustedIpArray} getIpData={this.accesIpRanges} />
                      : ''}
                  </div>
                  : ''}
              </div>
              : ''}

            {defaultlistType === 'SIMPLE_LIST_LOOKUP || SECURE_LIST_LOOKUP' && !noListFound ?
              <div>
                <div className="row">
                  <div className="offset-md-3 col-md-6">
                    <Select
                      name={'categoryList'}
                      title={'Select Category Mapping'}
                      required={false}
                      selectedOption={defaultCategory}
                      options={categoryMetadata ? categoryMetadata : ['']}
                      controlFunc={this.onSelectCategoryType} />
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-3">
                    <span className="">{RA_STR.uploadClassificationData}</span>
                  </div>
                  <div className="col-sm-3 col-form-label flex justify-between p-0">
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                      <input type="radio"
                        id="append"
                        className="custom-control-input"
                        name="selectedFileOption"
                        value='append'
                        checked={selectedFileOption === 'append'}
                        onChange={this.handleChange} />
                      <label className="custom-control-label" htmlFor="append"> {RA_STR.append} </label>
                    </div>
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                      <input type="radio"
                        id="replace"
                        className="custom-control-input"
                        name="selectedFileOption"
                        value='replace'
                        checked={selectedFileOption === 'replace'}
                        onChange={this.handleChange} />
                      <label className="custom-control-label" htmlFor="replace"> {RA_STR.replace} </label>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="offset-md-3 col-md-4 mb-3 p-0">
                    <label className="col-form-label mr-3">{RA_STR.uploadDataFile}</label>
                    <input type="file" name="myFile" onChange={this.onFileChange} id='input-field' />
                  </div>
                  <div className="col-md-5">
                    <span className="mr-3"> OR </span>
                    <label className="col-form-label mr-3">{RA_STR.enterData}</label>
                    <textarea className="vertical-align-top" rows="4" cols="35" name="classificationData" value={classificationData} onChange={this.handleChange}> </textarea>
                  </div>
                </div>

                <div className="form-group form-submit-button">
                  <input className="secondary-btn" id="saveNegativeCountryList" type="submit" value="UPLOAD" onClick={this.uploadDataFile}></input>
                </div>

              </div>
              : ''}
          </div>
          : ''}

        {manage == 'categoryMap' ?
          <div>
            {categoryMetadata.length === 0 ?
              <div className="offset-md-3"> <span> {RA_STR.selectCategoryMapping} <i> {RA_STR.noMappingFound} </i> </span> </div>
              : ''}
            {categoryMetadata.length > 0 ?
              <div>
                <div className="row">
                  <div className="offset-md-3 col-md-6">
                    <Select
                      name={'categoryList'}
                      title={'Select Category Mapping'}
                      required={false}
                      selectedOption={defaultCategory}
                      options={categoryMetadata ? categoryMetadata : ['']}
                      controlFunc={this.onSelectCategoryType} />
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-3">
                    <span className="">{RA_STR.uploadClassificationData}</span>
                  </div>
                  <div className="col-sm-3 col-form-label flex justify-between p-0">
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                      <input type="radio"
                        id="append"
                        className="custom-control-input"
                        name="selectedFileOption"
                        value='append'
                        checked={selectedFileOption === 'append'}
                        onChange={this.handleChange} />
                      <label className="custom-control-label" htmlFor="append"> {RA_STR.append} </label>
                    </div>
                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                      <input type="radio"
                        id="replace"
                        className="custom-control-input"
                        name="selectedFileOption"
                        value='replace'
                        checked={selectedFileOption === 'replace'}
                        onChange={this.handleChange} />
                      <label className="custom-control-label" htmlFor="replace"> {RA_STR.replace} </label>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="offset-md-3 col-md-4 mb-3 p-0">
                    <label className="col-form-label mr-3">{RA_STR.uploadDataFile}</label>
                    <input type="file" name="myFile" onChange={this.onFileChange} id='input-field' />
                  </div>
                  <div className="col-md-5">
                    <span className="mr-3"> OR </span>
                    <label className="col-form-label mr-3">{RA_STR.enterData}</label>
                    <textarea className="vertical-align-top" rows="4" cols="35" name="classificationData" value={classificationData} onChange={this.handleChange}> </textarea>
                  </div>
                </div>

                <div className="form-group form-submit-button">
                  <input className="secondary-btn" id="saveNegativeCountryList" type="submit" value="UPLOAD" onClick={this.uploadDataFile}></input>
                </div>

              </div>
              : ''}
          </div>
          : ''}
      </div>);
  }
}

export default ManageListCategoryMappingRA;
