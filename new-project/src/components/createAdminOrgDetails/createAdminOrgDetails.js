
import React, { Component } from 'react';
import './createAdminOrgDetails.scss'
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_CHECKBOX, RA_STR_RADIO, RA_STR_SELECT } from '../../shared/utlities/constants';
import ReactTooltip from 'react-tooltip';
import Select from '../../shared/components/Select/Select';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import SuccessList from '../../shared/components/SuccessList/SuccessList';
import SwappableGrid from '../../shared/components/SwapGrid/swappingComponent';
import { RA_STR } from '../../shared/utlities/messages';


class CreateAdministratorSub extends Component {
    constructor(props) {
        super(props);
        this.state = {
            createOrgDetailsFields: {},
            orgOptions: [],
            leftItems: [],
            rightItems: [],
            confirm_password: '',
            new_password: '',
            role: '',
            createAdminStatus: '',
            roleOptions: [],
            filterNames: [],
            storeSelected: [],
            allChecked: false,
            localeOptions: [],
            errorMessage: '',
            successMessage: '',
            scopeAll: false
        }
    }
    componentDidMount = async () => {
        const getOrg = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
        }

        const getRoles = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getRoles']}`
        }
        const getOrganization = await getService(getOrg);
        const getPredefinedRoles = await getService(getRoles);
        let objectKeys = Object.keys(getPredefinedRoles.data.roles);
        let objectValues = Object.values(getPredefinedRoles.data.roles);
        for (let i = 0; i < objectKeys.length; i++) {
            this.state.localeOptions.push({
                'key': objectValues[i].roleName,
                'content': objectValues[i].displayName
            })
        }
        this.setState({ roleOptions: this.state.localeOptions });
        let leftItemsObj = { leftItems: getOrganization.data.organizations, leftStaticArray: getOrganization.data.organizations };
        this.setState(leftItemsObj);
    }
    _handleChange = (e) => {
        const createOrgDetailsFields = this.state.createOrgDetailsFields;
        var feildValue;
        (e.target.type === RA_STR_CHECKBOX || e.target.type === RA_STR_RADIO) ?
            feildValue = e.target.checked
            :
            feildValue = e.target.value;
        createOrgDetailsFields[e.target.name] = feildValue;
        if (e.target.type === RA_STR_CHECKBOX) {
            this.setState({ scopeAll: !this.state.scopeAll });
        }
        this.setState({
            createOrgDetailsFields
        })

    }
    _handleCreate = async () => {
        const rightItems = this.refs.swappableGridRef.getRightItems();
        const createAdmin = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['createAdminStepTwoUrl']}`,
            data: {
                ...this.props.createAdminFields,
                ...this.state.createOrgDetailsFields,
                phoneNumbers: this.props.phoneNumbers,
                emailIds: this.props.emailIds,
                "customAttributes": this.props.customAttributes,
                "userName": this.props.userName,
                'scopeAll': this.state.scopeAll,
                'selectedOrganizations': rightItems,
                'selectedScopes': [
                    ""
                ],
            }
        };
        const createAdminStatus = await getService(createAdmin);
        if (createAdminStatus && createAdminStatus.status === 400) {
            this.setState({
                errorMessage: createAdminStatus.data.errorList,
                successMessage: ''
            });
            window.scrollTo(0, 0);
        } else if (createAdminStatus.status === 200) {
            this.setState({
                successMessage: createAdminStatus.data,
                errorMessage: ''
            });
            this.props.onBack('new', this.state.successMessage);
            window.scrollTo(0, 0);
            // this.props.callCreateParent();                   
        }
        this.setState({ createAdminStatus: createAdminStatus });
    }
    render() {
        var onBack = this.props.onBack;
        return (
            <div>
                <ErrorList validationDataList={this.state.errorMessage} />
                <SuccessList validationData={this.state.successMessage} />
                <div className="main">

                    <div className="row">
                        <p className="col-sm-3">Username</p>
                        <p className="col-sm-3 ">{this.props.userName} </p>
                    </div>
                    <div className="row">
                        <p className="col-sm-3">Organization</p>
                        <p className="col-sm-3">{this.props.organizationName}</p>
                    </div>
                    <div className="row">
                        <p className="col-sm-3">User Status</p>
                        <p className="col-sm-3">Active</p>
                    </div>

                    <h2 className="title">Create Administrator</h2>
                    <p className="desc">Set the Administrator's Role, Scope, and Password credential.<br />
                        Note: To reset the credential, the target administrator will need to go through the reset flow.</p>
                    <span className="ecc-h1">Role</span>
                    <div className="col-sm-7 create">
                        <Select
                            name={'role'}
                            title={'Role'}
                            required={true}
                            options={this.state.roleOptions}
                            placeholder={RA_STR_SELECT}
                            controlFunc={this._handleChange} />
                        <span className="ecc-h1 row">Set Password</span>
                        <span data-tip data-for='password-criteria'>
                            <SingleInput
                                title={'Password'}
                                inputType={'password'}
                                name={'newPassword'}
                                required={'true'}
                                controlFunc={this._handleChange} />
                            <SingleInput
                                title={'Confirm Password'}
                                inputType={'password'}
                                required={'true'}
                                name={'confirmPassword'}
                                controlFunc={this._handleChange} />
                        </span>
                        <ReactTooltip id='password-criteria' place="right" type="dark" >
                            <ul className="password-criteria">
                                Password must be at least 6 characters long, and must include
              <li>At least 1 numeric characters</li>
                                <li>At least 4 alphabetic characters( 0 LowerCase, 0 UpperCase)</li>
                                <li>At least 1 special characters  ( Allowed special characters are    !@#$%^&amp;*()_+ )</li></ul>
                        </ReactTooltip>
                    </div>
                    <span className="ecc-h1">Manages</span>
                    <div className="row">
                        <Checkbox
                            type={'checkbox'}
                            setName="scopeAll"
                            value={this.state.scopeAll}
                            checked={this.state.scopeAll}
                            controlFunc={this._handleChange}
                            options={["All Organizations"]}
                            selectedOptions={this.state.scopeAll ? ["All Organizations"] : []} />
                        <div className="col-sm-1">OR</div>
                        <div className="col-sm-4">{RA_STR.availableOrg}</div>
                        <div className="col-sm-4">{RA_STR.selectOrg}</div>
                    </div>
                    <div className='row'>
                        <div className='org-width' ></div>
                        <div className='col-sm-1'></div>
                        <SwappableGrid ref="swappableGridRef" scopeAll={this.state.scopeAll} leftItems={this.state.leftItems}></SwappableGrid>
                    </div>
                    <div className="form-group form-submit-button row mt-5 ml-2">
                        <input className="custom-secondary-btn" id="RESET" type="button" value="Back" onClick={onBack} />
                        <input className="secondary-btn ml-3" id="manageTokenButton" type="submit" value="CREATE" onClick={this._handleCreate}></input>
                    </div>
                </div >
            </div>
        );
    }
}

export default CreateAdministratorSub;