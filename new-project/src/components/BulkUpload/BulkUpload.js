import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import './BulkUpload.css';
import { RA_STR } from '../../shared/utlities/messages';

class BulkUpload extends Component {
    constructor(props) {
        super(props);
        const urlParams = new URLSearchParams(window.location.search);
        const orgName = urlParams.get('orgname');
        const status = urlParams.get('status');
        this.state = {
            roleOptions: [],
            operationOptions: [],
            uploadFileBinaryData: '',
            status: '',
            Description: '',
            orgName: orgName,
            orgStatus: status,
            format: ''
        }

    }
    handleFormChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }
    componentDidMount = async () => {
        const getOperations = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['operationsUrl']}`
        };
        const operationOptions = await getService(getOperations);
        let objectValues = Object.keys(operationOptions.data.operations);
        let objectKeys = Object.values(operationOptions.data.operations);
        for (let index = 0; index < objectKeys.length; index++) {
            this.state.operationOptions.push({
                'key': objectKeys[index],
                'content': objectValues[index]
            })
        }
        this.setState({ operationOptions: this.state.operationOptions, status: objectKeys[0] });
    }
    redirectToHelp = () => {
        this.props.history.push(RA_API_URL['searchUrl']);
    }
    uploadFile = async () => {
        const bulkUpload = {
            method: 'POST',
            url: `${serverUrl}/organization/${this.state.orgName}/users/bulkupload`,
            data: {
                "action": "string",
                "description": this.state.Description,
                "operation": this.state.status,
                "orgName": this.state.orgName,
                "status": this.state.orgStatus,
                "format": this.state.format,
                "uploadFile": Object.values(this.state.uploadFileBinaryData)
            }
        }
        const bulkUploadSuccessData = await getService(bulkUpload);
        if (bulkUploadSuccessData && bulkUploadSuccessData.status === 200) {
            this.setState({
                uploadFileBinaryData: [],
                format: '',
                Description:''
            });
            document.querySelector('#input-field').value = '';
            this.props.activateSuccessList(true, bulkUploadSuccessData.data);
            this.props.activateErrorList(false, '');
        } else {
            this.setState({
                uploadFileBinaryData: [],
                format: '',
                Description:''
            });
            document.querySelector('#input-field').value = '';
            this.props.activateErrorList(true, bulkUploadSuccessData.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }
    handleChange = (e) => {
        let reader = new FileReader();
        let file = e.target.files[0];
        if (file) {
            const lastDot = file.name.lastIndexOf('.');
            const fileName = file.name.substring(lastDot + 1);
            reader.onload = () => {
                const intArray = new Int8Array(reader.result);
                this.setState({
                    uploadFileBinaryData: intArray,
                    format: fileName
                })
            }
            reader.readAsArrayBuffer(file)
        }
        else {
            this.setState({
                uploadFileBinaryData: [],
                format: ''
            })
        }
    }
    render() {
        const { status ,Description} = this.state;
        return (<div className='main'>

            <h2 className="title">{RA_STR.bulkuploadtitle}</h2>
            <p className="desc">{RA_STR.bulkuploaddescription}</p>

            <b>Bulk Upload</b>

            <div className="col-sm-6">
                <Select
                    name={'status'}
                    title={'BulkUploadOperation'}
                    required={true}
                    options={this.state.operationOptions}
                    controlFunc={this.handleFormChange}
                    selectedOption={status}
                />
                <div className="form-group">
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">{RA_STR.bulkuploadfiletitle}</label>
                        <div className="col-sm-8">
                            <input type="file" name="myFile" onChange={this.handleChange} id='input-field' />
                        </div>
                    </div>
                </div>
                <SingleInput
                    title={'Description'}
                    inputType={'text'}
                    name={'Description'}
                    content={Description}
                    controlFunc={this.handleFormChange} />
            </div>

            <div className="form-group form-submit-button row">
                <input className="custom-secondary-btn" id="cancel" type="button" value="CANCEL" onClick={this.redirectToHelp}></input>
                <input className="secondary-btn" id="" type="submit" value="UPLOAD" onClick={this.uploadFile}></input>
            </div>
        </div>
        );
    }
}

export default BulkUpload;