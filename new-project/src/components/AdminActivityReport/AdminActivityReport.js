import React, { Component, Fragment } from 'react';
import AdminReport from '../Reports-admin/adminReport';
import { RA_API_URL, serverUrl } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';

class AdminActivityReport extends Component {
  componentWillMount() {

  }
  render() {
    return <Fragment>
      <AdminReport reportType="admin-activity-report" displayreportUrl={serverUrl + RA_API_URL.getAdministratorActivityReport} exportreportUrl={serverUrl + RA_API_URL.getAdminReportInCSV}
       displayReportText={RA_STR.adminReportDesc}></ AdminReport>
    </Fragment>

  }
}

export default AdminActivityReport;