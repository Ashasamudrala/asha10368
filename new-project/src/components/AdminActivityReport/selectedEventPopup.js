import React, { Fragment } from 'react';
import { RA_STR } from '../../shared/utlities/messages';


const showSelectedEventDetails = (that) => {
    let { selectedEvent, copiedClipboard } = that.state;
    return <div className="modal admin">
        <div className="modal-content">
            <div className="dialog-header" className="successheader">
                <div className="dialog-title"><span className="label">{RA_STR.reportModalTitle}</span></div>
                <div className="dialog-close" className="close" onClick={that.handleClose}> <span className="close">&times;</span></div>
            </div>
            <div className="modal-body">
                <div className="col-md-12 row">
                    <div className="col-md-6">
                        <div className="htmlForm-group">
                            <label htmlFor="htmlFormGroupExampleInput" className="apply-bold">Transaction ID</label>
                            <div>{selectedEvent.transactionID}</div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="htmlForm-group">
                            <label htmlFor="htmlFormGroupExampleInput" className="apply-bold">Event Type</label>
                            <div>{selectedEvent.eventType}</div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="htmlForm-group">
                            <label htmlFor="htmlFormGroupExampleInput" className="apply-bold">Target Organization</label>
                            <div>{selectedEvent.targetOrg}</div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="htmlForm-group">
                            <label htmlFor="htmlFormGroupExampleInput" className="apply-bold">Administrator</label>
                            <div>{selectedEvent.administratorID}</div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="htmlForm-group">
                            <label htmlFor="htmlFormGroupExampleInput" className="apply-bold">Date Time</label>
                            <div>{selectedEvent.date}</div>
                        </div>
                    </div>
                    <div className="col-sm-12 scroll-set">
                        <table className="report-scrolling infoDetailTable">
                            {selectedEvent.details["Changes for DEFAULT"] ?
                                <tbody>
                                    <tr>
                                        <th class="infoValue">{RA_STR.reportNewValue}</th>
                                        <th class="infoValue">{RA_STR.changeType}</th>
                                        <th class="infoValue">{RA_STR.reportRuleName}</th>
                                        <th class="infoValue">{RA_STR.prevValue}</th>
                                    </tr>
                                    {Object.keys(selectedEvent.details).map((eachEvent, index) => {
                                        return <Fragment key={index}>
                                            <tr>
                                                <td>
                                                    <h6>{eachEvent}</h6>
                                                </td>
                                            </tr>
                                            <tr class="form-group infoValue">
                                                <td>{selectedEvent.details[eachEvent]}</td>
                                            </tr>
                                        </Fragment>
                                    })
                                    }
                                </tbody>
                                :
                                <tbody>
                                    {Object.keys(selectedEvent.details).map((obj, index) =>
                                        <Fragment key={index}>
                                            <tr>
                                                <td className="infoKey"><h4>{RA_STR.selectedEventFieldnames[obj] ? RA_STR.selectedEventFieldnames[obj] : obj}</h4></td>
                                            </tr>
                                            <tr>
                                                <td className="infoValue">{selectedEvent.details[obj]}</td>
                                            </tr>
                                        </Fragment>
                                    )}
                                </tbody>
                            }
                        </table>
                    </div>
                    <div className="htmlForm-group htmlForm-submit-button col-sm-12 ml-30 mt-2">
                        <input className="custom-secondary-btn" type="submit" value="OK" onClick={that.handleClose}></input>
                        <input className="secondary-btn" type="submit" value="COPY TO CLIPBOARD" onClick={that.handleClipBoard}></input>
                        {copiedClipboard && <span className="tooltiptext">{RA_STR.copyToClipboard}</span>}
                    </div>
                </div>
            </div>
        </div>
    </div >

}
export default showSelectedEventDetails;