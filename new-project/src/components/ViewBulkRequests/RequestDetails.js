import React, { Component } from 'react';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import SuccessList from '../../shared/components/SuccessList/SuccessList';
import { orgData } from '../../shared/utlities/renderer';
import FileSaver from 'file-saver';

class RequestDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewRequestData: {},
            displaytasksData: [],
            successMsg: '',
            errorMsg: '',
            orgDetails: '',
            searchList: [],
            csvData: ''
        }
    }

    componentDidMount = () => {
        this.state.orgDetails = orgData('');
    }

    displayTasks = async (reqID, e) => {
        const displaytasks = {
            method: 'POST',
            url: `${serverUrl}/organization${RA_API_URL['displayTasks']}`,
            data: { 'requestId': reqID }
        };
        const displaytasksStatus = await getService(displaytasks);
        if (displaytasksStatus && displaytasksStatus.status === 400) {
            this.props.activateSuccessList(true, displaytasksStatus.data);
            this.props.activateErrorList(false, '');
        }
        else {
            this.setState({
                displaytasksData: displaytasksStatus.data.taskBeans
            })
            this.props.activateSuccessList(true, displaytasksStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    taskDetails = async (taskData, e) => {
        const viewTaskDetails = {
            method: 'POST',
            url: `${serverUrl}/organization${RA_API_URL['viewTasksDetails']}`,
            data: {
                'requestId': taskData.requestId,
                'taskId': taskData.taskId
            }
        };
        const viewTaskDetailsStatus = await getService(viewTaskDetails);
        if (viewTaskDetailsStatus && viewTaskDetailsStatus.status === 400) {
            this.props.activateSuccessList(true, viewTaskDetailsStatus.data);
            this.props.activateErrorList(false, '');
        }
        else {
            this.setState({
                viewTaskDetails: viewTaskDetailsStatus.data.taskDetails
            })
            this.props.history.push({
                pathname: `/org/searchOrg/basicOrg/task-details`,
                search: `?orgname=${this.state.orgDetails.orgName}&status=${this.state.orgDetails.status}`,
                state: {
                    taskDetails: this.state.viewTaskDetails
                }

            })
            this.props.activateSuccessList(true, viewTaskDetailsStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    returnToSearch = async (id, e) => {
        let params = {
            status: 'All',
            operation: 'All',
            orgName: this.state.orgDetails.orgName,
            requestId: id,
            pageNo: 0,
            pageSize: 10
        }
        const searchRequest = {
            method: 'POST',
            url: `${serverUrl}/organization/${params.orgName}${RA_API_URL['searchRequests']}`,
            data: params
        };
        const searchRequestStatus = await getService(searchRequest);
        if (searchRequestStatus && searchRequestStatus.status === 400) {
            this.props.activateErrorList(true, searchRequestStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
        else {
            this.setState({
                searchList: searchRequestStatus.data.listTaskBean === null ? [] : searchRequestStatus.data.listTaskBean,
            })
            this.props.history.push({
                pathname: `/org/searchOrg/basicOrg/view-bulk-requests`,
                search: `?orgname=${this.state.orgDetails.orgName}&status=${this.state.orgDetails.status}`,
                state: {
                    searchData: this.state.searchList,
                    returnSearch: true,
                    requestId: id
                }
            })
            this.props.activateSuccessList(true, searchRequestStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    refresh = async (taskData, e) => {
        const refreshDetails = {
            method: 'POST',
            url: `${serverUrl}/organization/${this.state.orgDetails.orgName}${RA_API_URL['viewRequests']}`,
            data: {
                'requestId': taskData,
                'orgName': this.state.orgDetails.orgName
            }
        };
        const refreshDetailsStatus = await getService(refreshDetails);
        if (refreshDetailsStatus && refreshDetailsStatus.status === 400) {
            this.props.activateErrorList(true, refreshDetailsStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
        else {
            this.setState({
                viewTaskDetails: refreshDetailsStatus.data,
            })
            this.props.activateSuccessList(true, refreshDetailsStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    exportFailures = async (reqId, e) => {
        const exportFile = {
            method: 'POST',
            url: `${serverUrl}/organization${RA_API_URL['exportTasks']}`,
            data: {
                'requestId': reqId,
                'status': 'Failure'
            }
        };
        const exportFileStatus = await getService(exportFile);
        if (exportFileStatus && exportFileStatus.status === 400) {
            this.props.activateErrorList(true, exportFileStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
        else {
            this.setState({
                csvData: exportFileStatus.data
            })
            var blob = new Blob([this.state.csvData], { type: "text/plain;charset=utf-8" });
            FileSaver.saveAs(blob, `FailedTasks_${reqId}.csv`);
            this.props.activateSuccessList(true, exportFileStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    render() {
        const props = this.props.location;
        const viewRequestData = props.state.searchData;
        return (
            <div>
                <SuccessList validationData={this.state.successMsg} />
                <ErrorList validationDataList={this.state.errorMsg} />
                <div className='main view-bulk-request'>
                    <h2 className="title">Bulk Requests Details</h2>
                    <p className="desc">Details of the Bulk Request</p>
                    <div className="row ml-1">
                        <div className="col-sm-5">
                            <div className="form-group dynamic-form-select">
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">Request ID</label>
                                    <div className="col-sm-6">
                                        <span>{viewRequestData.requestID}</span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">Description</label>
                                    <div className="col-sm-6">
                                        <span>{viewRequestData.description}</span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">Status</label>
                                    <div className="col-sm-6">
                                        <span>{viewRequestData.status}</span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">Submitted By:</label>
                                    <div className="col-sm-6">
                                        <span>{viewRequestData.submittedBy}</span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">Date of Submission</label>
                                    <div className="col-sm-6">
                                        <span>{viewRequestData.submittedDate}</span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">Date of Completion</label>
                                    <div className="col-sm-6">
                                        <span>{viewRequestData.completedDate}</span>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">No of successfull operations</label>
                                    <div className="col-sm-6">
                                        {viewRequestData.noOfSuccessfulOps > 0 ?
                                            <span className="request-id" onClick={this.displayTasks.bind(this, viewRequestData.requestID)}>{viewRequestData.noOfSuccessfulOps}</span>
                                            : <span>{viewRequestData.noOfSuccessfulOps}</span>
                                        }

                                    </div>
                                </div>
                                <div className="form-group row">
                                    <label className="col-sm-6 col-form-label">No of failed operations</label>
                                    <div className="col-sm-6">
                                        {viewRequestData.noOfFailedOps > 0 ?
                                            <span className="request-id" onClick={this.displayTasks.bind(this, viewRequestData.requestID)}>{viewRequestData.noOfFailedOps}</span>
                                            : <span>{viewRequestData.noOfFailedOps}</span>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-inline">
                        <div className="form-group form-submit-button">
                            <input className="secondary-btn" type="submit" value="RETURN TO SEARCH" onClick={this.returnToSearch.bind(this, viewRequestData.requestID)} />
                        </div>
                        <div className="form-group form-submit-button p-2">
                            <input className="secondary-btn" type="submit" value="EXPORT FAILURES"
                                disabled={viewRequestData.noOfFailedOps === 0 ? true : false} onClick={this.exportFailures.bind(this, viewRequestData.requestID)} />
                        </div>
                        <div className="form-group form-submit-button p-2">
                            <input className="secondary-btn" type="submit" value="REFRESH" onClick={this.refresh.bind(this, viewRequestData.requestID)} />
                        </div>
                    </div>
                    {this.state.displaytasksData.length ?
                        <div>
                            <h5>Tasks List</h5>
                            <table className="table table-borderless">
                                <thead>
                                    <tr>
                                        <th scope="col">Task ID</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.displaytasksData.map((list, index) => {
                                        return (
                                            <tr key={index}>
                                                <td className="request-id" onClick={this.taskDetails.bind(this, list)}>{list.taskId}</td>
                                                <td >{list.status}</td>
                                                <td>{list.statusdesc}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                        : ''
                    }
                </div>
            </div>
        );
    }
}

export default RequestDetails;