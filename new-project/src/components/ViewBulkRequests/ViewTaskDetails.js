import React, { Component } from 'react';
import { orgData } from '../../shared/utlities/renderer';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';

class ViewTaskDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orgDetails: '',
            viewRequestData: {},
            searchData: []
        }
    }

    componentDidMount = () => {
        this.state.orgDetails = orgData('');
    }

    returnToTask = async () => {
        const props = this.props.location;
        const viewTaskDetails = props.state.taskDetails;
        let params = {
            orgName: this.state.orgDetails.orgName,
            requestId: viewTaskDetails.requestId
        }
        const viewRequestDetails = {
            method: 'POST',
            url: `${serverUrl}/organization/${params.orgName}${RA_API_URL['viewRequests']}`,
            data: params
        };
        const viewRequestStatus = await getService(viewRequestDetails);
        if (viewRequestStatus && viewRequestStatus.status === 400) {
            this.props.activateErrorList(true, viewRequestStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
        else {
            this.setState({
                viewRequestData: viewRequestStatus.data.bulkRequestDetailBean
            })
            this.props.history.push({
                pathname: `${RA_API_URL['viewBulkRequests']}`,
                search: `?orgname=${this.state.orgDetails.orgName}&status=${this.state.orgDetails.status}`,
                state: {
                    searchData: this.state.viewRequestData
                }

            })
            this.props.activateSuccessList(true, viewRequestStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    returnToSearch = async (id, e) => {
        let params = {
            status: 'All',
            operation: 'All',
            orgName: this.state.orgDetails.orgName,
            pageNo: 0,
            pageSize: 10,
            requestId: id
        }
        const searchRequest = {
            method: 'POST',
            url: `${serverUrl}/organization/${params.orgName}${RA_API_URL['searchRequests']}`,
            data: params
        };
        const searchRequestStatus = await getService(searchRequest);
        if (searchRequestStatus && searchRequestStatus.status === 400) {
            this.props.activateErrorList(true, searchRequestStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
        else {
            this.setState({
                searchData: searchRequestStatus.data.listTaskBean === null ? [] : searchRequestStatus.data.listTaskBean,
                successMsg: searchRequestStatus.data,
                errorMsg: ''
            })
            this.props.history.push({
                pathname: `/org/searchOrg/basicOrg/view-bulk-requests`,
                search: `?orgname=${this.state.orgDetails.orgName}&status=${this.state.orgDetails.status}`,
                state: {
                    searchData: this.state.searchData,
                    requestId: id
                }

            })
            this.props.activateSuccessList(true, searchRequestStatus.data);
            this.props.activateErrorList(false, '');
        }
    }

    render() {
        const props = this.props.location;
        const viewTaskDetails = props.state.taskDetails;
        return (
            <div className="main view-bulk-request">
                <h2 className='title'>{RA_STR.bulkTaskTitle}</h2>
                <p className='desc'>{RA_STR.detailsBulkTask}</p>
                <p className='desc'><b>{RA_STR.bulkTaskTitle}</b></p>
                <div className="row ml-1">
                    <div className="col-sm-5">
                        <div className="form-group dynamic-form-select">
                            <div className="form-group row">
                                <label className="col-sm-6 col-form-label">Request ID</label>
                                <div className="col-sm-6">
                                    <span className="request-id" onClick={this.returnToTask}>{viewTaskDetails.requestId}</span>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-6 col-form-label">Task ID</label>
                                <div className="col-sm-6">
                                    <span>{viewTaskDetails.taskId}</span>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-6 col-form-label">Status</label>
                                <div className="col-sm-6">
                                    <span>{viewTaskDetails.status}</span>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-6 col-form-label">Description</label>
                                <div className="col-sm-6">
                                    <span>{viewTaskDetails.statusdesc}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-inline">
                    <div className="form-group form-submit-button">
                        <input className="secondary-btn" type="submit" value="Return to Tasks List" onClick={this.returnToTask} />
                    </div>
                    <div className="form-group form-submit-button p-2">
                        <input className="secondary-btn" type="submit" value="Return to Search" onClick={this.returnToSearch.bind(this, viewTaskDetails.requestId)} />
                    </div>
                </div>
                <h5>Params List</h5>
                <table className="table table-borderless">
                    <thead>
                        <tr>
                            <th scope="col">Param Name</th>
                            <th scope="col">Param Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        {viewTaskDetails.taskParams.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td >{list.paramname}</td>
                                    <td >{list.paramvalue}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ViewTaskDetails;