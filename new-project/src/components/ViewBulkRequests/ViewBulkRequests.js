import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { orgData } from '../../shared/utlities/renderer';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { RA_STR } from '../../shared/utlities/messages';
import './ViewBulkRequests.css';

class ViewBulkRequests extends Component {
    constructor(props) {
        super(props);
        this.state = {
            operations: [],
            listStatus: [],
            orgDetails: '',
            viewBulkForm: {},
            successMsg: '',
            errorMsg: '',
            pageNo: 0,
            currentPageNo: 1,
            pageSize: 10,
            requestId: '',
            totalCount: [],
            propSearchData: [],
            columns: [
                {
                    title: 'Request ID', field: 'requestId', render: (rowData) =>
                        <div className="request-id" onClick={this.getRequestDetails.bind(this, rowData.requestId)}>{rowData.requestId}</div>
                },
                { title: 'Operation', field: 'displayName' },
                { title: 'Status', field: 'status' },
                { title: 'Description', field: 'description' },
                { title: 'Date of Submission', field: 'dateOfSubm' },
                { title: 'Date of Completion', field: 'dateOfCompl' }
            ],
            searchData: [],
            viewRequestData: {},
            displaytasksData: [],
            viewTaskDetails: {},
            returnSearch: false
        }
    }

    componentDidMount = async () => {
        const props = this.props.location;
        this.state.requestId = props.state ? props.state.requestId : '';
        this.state.orgDetails = orgData('');
        const getStatus = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getBulkRequestStatus']}`
        };
        const getBulkRequestStatus = await getService(getStatus);
        if (getBulkRequestStatus && getBulkRequestStatus.status === 200) {
            let objectKeys = Object.keys(getBulkRequestStatus.data.listStatus);
            let objectValues = Object.values(getBulkRequestStatus.data.listStatus);
            for (let i = 0; i < objectKeys.length; i++) {
                this.state.listStatus.push({
                    'key': objectValues[i],
                    'content': objectValues[i]
                })
                this.setState({
                    listStatus: this.state.listStatus
                });
            }
        }
        const getOperations = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getBulkRequestDetails']}`
        };
        const getBulkRequestDetails = await getService(getOperations);
        if (getBulkRequestDetails && getBulkRequestDetails.status === 200) {
            this.setState({
                operations: getBulkRequestDetails.data.operations
            });
        }
    }

    handleChange = (e) => {
        const viewBulkForm = this.state.viewBulkForm;
        const { name, value } = e.target;
        var feildValue;
        feildValue = value;
        viewBulkForm[name] = feildValue;
        if (name === 'requestId') {
            this.setState({
                requestId: value
            })
        }
        this.setState({
            viewBulkForm

        });
    }

    handleSearch = async () => {
        var test = /^\d+$/.test(this.state.requestId);
        if (this.state.requestId !== '' && !test) {
            alert(RA_STR.positiVeNum);
        }
        else if(this.state.requestId.length > 10){
            alert(RA_STR.validID);
        }
        else {
            let params = {
                status: 'All',
                operation: 'All',
                ...this.state.viewBulkForm,
                orgName: this.state.orgDetails.orgName,
                requestId: this.state.requestId,
                pageNo: this.state.pageNo,
                pageSize: this.state.pageSize
            }
            const searchRequest = {
                method: 'POST',
                url: `${serverUrl}/organization/${params.orgName}${RA_API_URL['searchRequests']}`,
                data: params
            };
            const searchRequestStatus = await getService(searchRequest);
            if (searchRequestStatus && searchRequestStatus.status === 400) {
                this.setState({
                    searchData: [],
                    propSearchData: []
                })
                this.props.activateErrorList(true, searchRequestStatus.data.errorList);
                this.props.activateSuccessList(false, '');
            }
            else {
                this.setState({
                    propSearchData: [],
                    searchData: searchRequestStatus.data.listTaskBean,
                    totalCount: searchRequestStatus.data.resultCount,
                    returnSearch: true,
                })
                this.props.activateSuccessList(true, searchRequestStatus.data);
                this.props.activateErrorList(false, '');
                delete this.state.propSearchData;
            }
        }
    }

    getRequestDetails = async (getId, e) => {
        let params = {
            orgName: this.state.orgDetails.orgName,
            requestId: getId
        }
        const viewRequestDetails = {
            method: 'POST',
            url: `${serverUrl}/organization/${params.orgName}${RA_API_URL['viewRequests']}`,
            data: params
        };
        const viewRequestStatus = await getService(viewRequestDetails);
        if (viewRequestStatus && viewRequestStatus.status === 400) {
            this.props.activateErrorList(true, viewRequestStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
        else {
            this.setState({
                viewRequestData: viewRequestStatus.data.bulkRequestDetailBean
            })
            this.props.activateSuccessList(true, viewRequestStatus.data);
            this.props.activateErrorList(false, '');
            this.props.history.push({
                pathname: `/org/searchOrg/basicOrg/view-request-details`,
                search: `?orgname=${this.state.orgDetails.orgName}&status=${this.state.orgDetails.status}`,
                state: {
                    searchData: this.state.viewRequestData
                }

            })
        }
    }

    getActivePage = (getPageNo) => {
        let pageCount = getPageNo - 1;
        this.setState({
            pageNo: pageCount,
            currentPageNo: getPageNo

        }, () => {
            this.handleSearch();
        })

    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    render() {
        const props = this.props.location;
        const { listStatus, operations, totalCount } = this.state;
        const propSearchData = props.state ? props.state.searchData : [];
        const searchData = this.state.searchData ? this.state.searchData : [];
        return (
            <div className='main view-bulk-request'>
                <h2 className="title">Search Bulk Requests</h2>
                <p className="desc">Enter the ID of the request that you want to search</p>
                <div className="row ml-1">
                    <div className="col-sm-5">
                        <SingleInput
                            title={'Request ID'}
                            inputType={'text'}
                            name={'requestId'}
                            content={this.state.requestId}
                            controlFunc={this.handleChange} />
                        <Select
                            name={'status'}
                            title={'Status'}
                            options={listStatus ? listStatus : ['']}
                            controlFunc={this.handleChange} />
                        <div className="form-group dynamic-form-select">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">Operation</label>
                                <div className="col-sm-8">
                                    <select
                                        name='operation'
                                        onChange={this.handleChange}
                                        className="form-select form-control">
                                        <option value="">All</option>
                                        {Object.keys(operations).map(key =>
                                            <option value={operations[key]} key={key}>{operations[key]}</option>
                                        )}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-group form-submit-button row">
                    <input className="secondary-btn ml-2" id="" type="submit" value="SEARCH" onClick={this.handleSearch} />
                </div>
                {(propSearchData.length && this.state.returnSearch === false) ?
                    <DynamicTable columns={this.state.columns} data={propSearchData.length ? propSearchData : []} pageSize={this.state.pageSize} enablePagination={true}
                        totalCount={totalCount} pageSize={this.state.pageSize} activePageNumNew={this.state.currentPageNo}
                        activePage={this.getActivePage} />
                    : ''
                }
                {this.state.returnSearch ?
                    <DynamicTable columns={this.state.columns} data={searchData} pageSize={this.state.pageSize} enablePagination={true}
                        totalCount={totalCount} pageSize={this.state.pageSize} activePageNumNew={this.state.currentPageNo}
                        activePage={this.getActivePage} />
                    : ''
                }

            </div>
        );
    }
}

export default ViewBulkRequests;