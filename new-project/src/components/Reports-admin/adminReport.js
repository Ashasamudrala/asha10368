import React, { Component, Fragment } from 'react';
import '../MyActivityReport/MyActivityReport.scss';
import "react-datepicker/dist/react-datepicker.css";
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, serverUrl, LOADING_TEXT } from '../../shared/utlities/constants';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import { saveAs } from 'file-saver';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import DateRange from '../../shared/components/DateRange/DateRange';
import { RA_STR } from '../../shared/utlities/messages';
import { RA_STR_SELECT } from '../../shared/utlities/constants';
import Select from '../../shared/components/Select/Select';
import Loader from '../../shared/utlities/loader';
import ReactDOM from 'react-dom';
import showSelectedEventDetails from '../AdminActivityReport/selectedEventPopup';

class AdminReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userActivities: [],
            organizationEvents: [],
            successMessage: '',
            errorMessage: [],
            totalCount: 0,
            pageNo: 0,
            pageSize: 10,
            activePageNum: 1,
            showdisplayReport: false,
            getDateItems: [],
            organizationList: [],
            showLoadBar: false,
            showModalPopup: false,
            tabledata: [],
            selectedEvent: '',
            userReportObj: {
                eventType: '',
                userName: '',
                orgNames: [],
                adminPreferredTimezone: '',
                decryptionNeeded: true,
                pageNo: 0,
                pageSize: 10,
                activePageNum: 1,
            }
        }
        this.eventTypes = [];
        this.handleChange = this.handleChange.bind(this);
        this.handleSelectOrganizations = this.handleSelectOrganizations.bind(this);
    }
    showInfo = (getSelectedInfo) => {
        this.setState({ modalColor: true, showModalPopup: true, selectedEvent: getSelectedInfo });
    }
    handleClose = () => {
        this.setState({ modalColor: false, showModalPopup: false });
    }
    setColumns() {
        if (this.props.reportType === RA_STR.administratorReport) {
            this.columns = [
                { title: RA_STR.date, field: 'date' },
                { title: RA_STR.administratorId, field: 'administratorID' },
                { title: RA_STR.adminOrg, field: 'adminPreferredORG' },
                { title: RA_STR.transactionId, field: 'transactionID' },
                { title: RA_STR.eventTypeTable, field: 'eventType' },
                { title: RA_STR.status, field: 'status' },
                { title: RA_STR.reason, field: 'reason' },
                { title: RA_STR.userId, field: 'userID' },
                { title: RA_STR.targetOrg, field: 'targetOrg' },
                {
                    title: RA_STR.details, field: 'details', render: (rowData) =>
                        <div>
                            {
                                rowData.details
                                    ? <span onClick={this.showInfo.bind(this, rowData)}>
                                        <img src="images/info.png"></img>
                                    </span>
                                    : ''
                            }
                        </div>
                }
            ]
        } else {
            this.columns = this.props.columns;
        }
    }
    handleChange(e) {
        let { userReportObj } = this.state;
        userReportObj[e.target.name] = e.target.value;
        if (e.target.name === 'eventType') {
            this.displayReport()
        }
        this.setState({ userReportObj });
    }
    handleClipBoard = () => {
        const copyText = document.querySelector(".report-scrolling").outerHTML;
        const el = document.createElement('textarea');
        el.value = copyText;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        this.setState({ copiedClipboard: true }, () => {
            var timerid = setTimeout(() => {
                this.setState({ copiedClipboard: false });
                clearTimeout(timerid);
            }, 200);
        });
    }
    onchange = (e) => {
        let getValue = e.target;
        let { userReportObj } = this.state;
        if (getValue.checked) {
            userReportObj[e.target.name] = true;
        } else {
            userReportObj[e.target.name] = false;
        }
        this.setState({ userReportObj });
    }
    startDateChange = (date) => {
        this.setState({
            startDate: date
        });
    }
    componentWillMount() {
        this.getOrganizations();
        this.getEventTypes();
        this.setColumns();
        if (this.props.reportType === RA_STR.administratorReport) {
            this.setEventPopupData();
        }
    }
    setEventDropdownIntable() {
        const currentDom = ReactDOM.findDOMNode(this);
        if (currentDom) {
            let getTableNode = currentDom.querySelectorAll('th.MuiTableCell-root');
            if (getTableNode && getTableNode[4]) {
                getTableNode[4].innerHTML = 'Event Type <img src="images/dropdown.png" class="clickDownArrow" />' + this.field;
                currentDom.querySelector(".clickDownArrow") && currentDom.querySelector(".clickDownArrow").addEventListener("click", this.hideEventList);
                currentDom.querySelector(".event-type-submit") && currentDom.querySelector(".event-type-submit").addEventListener("click", this.submitEventTypes);
            }
        }
    }
    componentWillUnmount() {

    }
    getActivePage = (getPageNum) => {
        let pageNumCount = getPageNum - 1;
        let { userReportObj } = this.state;
        userReportObj.pageNo = pageNumCount;
        userReportObj.activePageNum = getPageNum;
        this.setState({ userReportObj }, () => {
            this.displayReport();
        })
    }
    reset = () => {
        window.location.reload();
    }
    handleSelectOrganizations = (e) => {
        let { name, selectedOptions } = e.target;
        let { userReportObj } = this.state;
        if (selectedOptions) {
            userReportObj[name] = Array.from(selectedOptions, (item) => item.value)
            this.setState({ userReportObj });
        }
    }
    getEventTypes = async () => {
        const response = await getService({ method: 'GET', url: serverUrl + RA_API_URL.getUserActivityEvents });
        if (response && response.status === 200) {
            let organizationEvents = [];
            organizationEvents = response.data.map(obj => { return { key: obj, content: obj } })
            organizationEvents = [{ key: '', content: 'All Events' }, ...organizationEvents];
            this.setState({ organizationEvents });
        }
    }
    newReport = () => {
        this.eventTypes = [];
        this.setState({
            showdisplayReport: false, getDateItems: [], userReportObj: {
                userName: '',
                orgNames: [],
                adminPreferredTimezone: 'GMT+05:30',
                decryptionNeeded: true,
                fromDate: new Date(),
                pageNo: 0,
                pageSize: 10,
                eventType: '',
                activePageNum: 1,
                toDate: new Date(),
            }
        });
        this.setDefaultOrgSelected();
    }
    exportReport = async (buttonEvent) => {
        let { userReportObj, updateStateObj = {}, getDateItems } = JSON.parse(JSON.stringify(this.state));
        this.dateObj = {};
        if (this.refs.daterange) {
            this.dateObj = this.refs.daterange.getDates();
            this.dateObj = this.dateObj ? this.dateObj[0] : this.dateObj;
        } else {
            this.dateObj = getDateItems;
        }
        userReportObj.fromDate = this.dateObj.startDate;
        userReportObj.toDate = this.dateObj.endDate;
        userReportObj.adminPreferredTimezone = 'GMT+05:30';
        if (this.props.reportType === RA_STR.administratorReport) {
            userReportObj.adminName = userReportObj.userName ? userReportObj.userName : '';
            userReportObj.orgList = userReportObj.orgNames.length > 0 ? userReportObj.orgNames : null;
            userReportObj.eventTypes = this.eventTypes;
            if (buttonEvent) {
                userReportObj.pageNo = null;
            }
            delete userReportObj.userName;
            delete userReportObj.orgNames;
        } else {
            userReportObj.userName = userReportObj.userName ? userReportObj.userName : null;
            userReportObj.orgNames = userReportObj.orgNames.length > 0 ? userReportObj.orgNames : null;
            userReportObj.eventType = userReportObj.eventType ? userReportObj.eventType : null;
        }
        const response = await getService({ method: 'POST', url: this.props.exportreportUrl, data: userReportObj });
        if (response && response.status === 400) {
            updateStateObj = {
                errorMessage: response.data.errorList,
                successMessage: ''
            };
            window.scrollTo(0, 0);
        } else if (response.status === 200) {
            if (response && response.data) {
                const blob = new Blob([response.data], { type: 'application/octet-stream' });
                saveAs(blob, this.reportName + userReportObj.fromDate + userReportObj.toDate + '.csv');
            }
        }

        this.setState(updateStateObj);
    }

    displayReport = async (buttonEvent) => {
        let { userReportObj, updateStateObj = {}, getDateItems, showdisplayReport } = JSON.parse(JSON.stringify(this.state));
        this.dateObj = {};
        if (this.refs.daterange) {
            this.dateObj = this.refs.daterange.getDates();
            this.dateObj = this.dateObj ? this.dateObj[0] : this.dateObj;
        } else {
            this.dateObj = getDateItems;
        }
        userReportObj.fromDate = this.dateObj.startDate;
        userReportObj.toDate = this.dateObj.endDate;
        userReportObj.adminPreferredTimezone = 'GMT+05:30';
        if (this.props.reportType === RA_STR.administratorReport) {
            userReportObj.adminName = userReportObj.userName ? userReportObj.userName : '';
            userReportObj.orgList = userReportObj.orgNames.length > 0 ? userReportObj.orgNames : null;
            userReportObj.eventTypes = this.eventTypes;
            if (buttonEvent) {
                userReportObj.pageNo = null;
            }
            delete userReportObj.userName;
            delete userReportObj.orgNames;
        } else {
            userReportObj.userName = userReportObj.userName ? userReportObj.userName : null;
            userReportObj.eventType = userReportObj.eventType ? userReportObj.eventType : null;
            userReportObj.orgNames = userReportObj.orgNames.length > 0 ? userReportObj.orgNames : null;
        }

        this.setState({ showLoadBar: true });
        const response = await getService({ method: 'POST', url: this.props.displayreportUrl, data: userReportObj });
        if (response && response.status === 400) {
            updateStateObj = {
                errorMessage: response.data.errorList,
                successMessage: '',
                showLoadBar: false
            };
            window.scrollTo(0, 0);
        } else if (response && response.status === 200) {
            updateStateObj = {
                errorMessage: response.data.errorList,
                showdisplayReport: true,
                showLoadBar: false,
                totalCount: response.data.noOfRecords || response.data.numberOfRecords
            };
            if (this.props.reportType == RA_STR.creationReport) {
                updateStateObj.tabledata = response.data.userCreation;
            } else if (this.props.reportType == RA_STR.administratorReport) {
                updateStateObj.tabledata = response.data.reportList;
            } else {
                updateStateObj.tabledata = response.data.userActivities;
            }
        }
        updateStateObj.getDateItems = this.dateObj;
        this.setState(updateStateObj, () => {
            if (this.props.reportType === RA_STR.administratorReport && response && response.status === 200 || showdisplayReport) {
                this.setEventDropdownIntable();
            }
        });
    }
    hideEventList = () => {
        const currentDom = ReactDOM.findDOMNode(this);
        if (currentDom) {
            let eventListNode = currentDom.querySelector(".event-list");
            if (eventListNode && eventListNode.style.display === RA_STR.NONE) {
                eventListNode.style.display = RA_STR.BLOCK;
            } else if (eventListNode) {
                eventListNode.style.display = RA_STR.NONE;
            }
        }
    }
    submitEventTypes = () => {
        this.eventTypes = [];
        var getselected = document.querySelectorAll(".event-select option");
        for (var i = 0; i < getselected.length; i++) {
            if (getselected[i].selected) {
                this.eventTypes.push(getselected[i].text);
            }
        }
        this.displayReport(true);
    }
    async setEventPopupData() {
        // Variable that contains method,url declaration that goes to backend.
        const getEventTypes = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getEventTypes']}`
        };
        // Catching backend response from getService method.
        const eventTypeResponse = await getService(getEventTypes);
        if (eventTypeResponse && eventTypeResponse.status === 200) {
            var field = '<div class="event-list" style="display:none"><select multiple class="event-select custom-select">'
            Object.keys(eventTypeResponse.data.eventTypes).forEach((eachType) => {
                field += '<optgroup label=' + eachType + '>';
                eventTypeResponse.data.eventTypes[eachType].forEach((eachEvent) => {
                    field += '<option>' + eachEvent + '</option>'
                })
                field += '</optgroup>'
            })
            field += '</select><div class="event-footer"><input type="submit" class="custom-secondary-btn event-type-submit"/></div></div>'
        }
        this.field = field;
    }
    setDefaultOrgSelected() {
        let profiledata;
        if (localStorage.getItem('profiledata')) {
            profiledata = JSON.parse(localStorage.getItem('profiledata'));
        }
        let { userReportObj } = this.state;
        if (profiledata.userProfile.preferredOrganization && profiledata.userProfile.preferredOrganization !== RA_STR_SELECT) {
            userReportObj.orgNames = [profiledata.userProfile.preferredOrganization];
            // this.setState({ userReportObj });
        }
    }
    async getOrganizations() {
        const getOrganizationList = await getService({ method: 'GET', url: serverUrl + RA_API_URL['getOrgUrl'] });
        if (getOrganizationList && getOrganizationList.status === 200) {
            let data = Object.keys(getOrganizationList.data.organizations);
            let organizationList = Object.values(getOrganizationList.data.organizations).map((obj, key) => { return { key: data[key], content: obj } });
            this.setState({ organizationList });
            // this.setDefaultOrgSelected();
        }
    }
    render() {
        let { userReportObj, organizationList, successMessage, errorMessage, showdisplayReport, tabledata, reportName, totalCount, getDateItems, organizationEvents, showLoadBar, showModalPopup } = this.state;
        switch (this.props.reportType) {
            case RA_STR.creationReport://User Creation Report
                reportName = RA_STR.userCreation;
                break;
            case RA_STR.ActivityReport://User Activity Report
                reportName = RA_STR.userActivity;
                break;
            default:
                reportName = RA_STR.userAdministrator;
                break;
        }
        this.reportName = reportName;
        return <div className="main mt-2 myActivity setAdminActivity">
            {successMessage ?
                <div className="error-parent">
                    <div className="edl_success" id="ssErrorMessagesTable">
                        <div className="error-list">{successMessage}</div>
                    </div>
                </div>
                : ''
            }
            {errorMessage && errorMessage.length > 0 && <ErrorList validationDataList={errorMessage} />}
            {!showLoadBar && <h2 className="title">{reportName}</h2>}
            {!showdisplayReport ? <div>
                <p className="desc" dangerouslySetInnerHTML={{ __html: (reportName !== RA_STR.userAdministrator ? RA_STR.useractivitySearch : RA_STR.useradministratorSearch) }}></p>
                <hr />
                <div className="row mb-2">
                    <div className="col-sm-3">
                        <label className="col-sm-8 col-form-label mt-3">{RA_STR.timePeriod}</label>
                    </div>
                    <DateRange ref="daterange"></DateRange>
                </div>
                <div className="row ml-1 mt-4">
                    <div className="col-sm-6 dynamic-form-select">
                        <SingleInput
                            title={reportName !== RA_STR.userAdministrator ? RA_STR.Username : RA_STR.adminName}
                            inputType={'text'}
                            name={'userName'}
                            content={userReportObj.userName}
                            controlFunc={this.handleChange}
                        />
                        <div className="dynamic-form-input form-group row">
                            <label className="col-sm-4 col-form-label adjustLabel">{RA_STR.EXCEPTION_USER_REPORT.ORGANIZATION_LABEL}</label>
                            <div className="col-sm-8 input-group input-group-unstyled">
                                <Select
                                    name='orgNames'
                                    title=''
                                    multiple='true'
                                    options={organizationList}
                                    selectedOption={userReportObj.orgNames}
                                    controlFunc={this.handleSelectOrganizations}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-sm-3">
                        <div className="custom-control custom-checkbox">
                            <label key='decryptionNeeded' className='form-label capitalize col-sm-12' >
                                <input
                                    className="form-checkbox custom-control-input"
                                    type='checkbox'
                                    id='decryptionNeeded'
                                    name='decryptionNeeded'
                                    onChange={this.onchange}
                                    value={userReportObj.decryptionNeeded}
                                    checked={userReportObj.decryptionNeeded}
                                />
                                <label className="form-label custom-control-label" htmlFor='decryptionNeeded'>{RA_STR.DecryptInfo}</label>
                            </label>
                        </div>
                    </div>
                    <div className="col-sm-9">
                        <div className="form-group row ReportButtons">
                            <input className="secondary-btn" id="searchButton" type="submit" value="RESET" onClick={this.reset}></input>
                            <input className="secondary-btn" id="searchButton" type="submit" value="EXPORT" onClick={() => this.exportReport(true)}></input>
                            <input className="secondary-btn" id="searchButton" type="submit" value="DISPLAY REPORT" onClick={() => this.displayReport(true)}></input>
                        </div>
                    </div>
                </div >
            </div>
                :
                <Fragment>
                    {showLoadBar ? <div><Loader show='true' /> {LOADING_TEXT}</div> :
                        <div>
                            <div className="col-md-12 row">
                                <div className="col-md-9 no-padding">
                                    <p>{this.props.displayReportText}</p>
                                </div>
                                <div className="float-right col-md-3">
                                    <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportReport}></input>
                                    <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.newReport}></input>
                                </div>
                            </div>
                            <div className="col-md-8 pl-0">
                                <div className="form-group row">
                                    <div className="col-sm-4 ">{RA_STR.noOfRecords}</div>
                                    <div className="col-sm-8">
                                        {totalCount}
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4">From:{getDateItems.startDate}</div>
                                    <div className="col-sm-8">
                                        To:{getDateItems.endDate}
                                    </div>
                                </div>
                                {reportName === RA_STR.userActivity && <div className="eventSelector pl-3">
                                    <Select
                                        name={'eventType'}
                                        title={'Events to display'}
                                        options={organizationEvents}
                                        selectedOption={userReportObj.eventType}
                                        controlFunc={this.handleChange}
                                    />
                                </div>}
                            </div>
                            {showModalPopup && showSelectedEventDetails(this)}
                            <div className="tabele-buttons">
                                <DynamicTable columns={this.columns} data={tabledata} enablePagination={true} pageSize={userReportObj.pageSize} totalCount={totalCount} activePage={this.getActivePage} activePageNumNew={userReportObj.activePageNum} /></div>
                        </div>
                    }
                </Fragment>
            }
        </div>
    }
}

export default AdminReport;