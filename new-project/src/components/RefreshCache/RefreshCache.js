import React, { Component } from 'react';
import { serverUrl, RA_API_URL, RA_STR_PROFILEDATA } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import './RefreshCache.css'

class RefreshCache extends Component {
  constructor(props) {
    super(props);
    this.state = {
      systemConfig: false,
    };
  }

  onOkClick = async () => {
    const { systemConfig } = this.state;
    const { token = null } = JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA));
    if (systemConfig) {
      const makeCall = await window.confirm('Are you sure you want to refresh the cache?');
      if (makeCall) {
        const refreshStatus = {
          method: 'POST',
          url: `${serverUrl}${RA_API_URL['refreshCache']}`,
          data: {
            "organziationCacheRefresh": true,
            "systemConfigCacheRefresh": systemConfig
          }
        };
        const refreshCacheStatusUpdate = await getService(refreshStatus);
        if (!!refreshCacheStatusUpdate.status && refreshCacheStatusUpdate.status === 200) {
          this.setState({ response: refreshCacheStatusUpdate.data });
        }
        // else console.log('Error ==> ', refreshCacheStatusUpdate);  
      }
    } else alert('Select atleast one checkbox.');
  }

  onCheckbox = () => {
    const { systemConfig: refresh } = this.state;
    this.setState({ systemConfig: !refresh });
  }

  render() {
    const { systemConfig, response } = this.state;
    return <div className="main">
      {response && <div className="edl_success_feedback">{response.message}</div>}
      <div><h2 className="title">Refresh Cache</h2>
        <p className="desc">Refresh the cache configuration of all the product servers. If 'Refresh Organization Configuration' is selected, the cache of all the Organizations managed by you is refreshed.</p>
        <div className="col-sm-7 div-seperator">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">
              Refresh System Configuration
                      </label>
            <div className="col-sm-8">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="customCheck1" checked={systemConfig} onChange={this.onCheckbox} />
                <label className="custom-control-label" htmlFor="customCheck1"></label>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">
              Refresh Organization Configuration
                      </label>
            <div className="col-sm-8">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="customCheck2" disabled="disabled" />
                <label className="custom-control-label" htmlFor="customCheck2"></label>
              </div>
            </div>
          </div>
          <div className="form-group form-submit-button row">
            <input className="secondary-btn" id="createRoleButton" type="submit" value="OK" onClick={this.onOkClick} />
          </div>
        </div>
      </div>
    </div>;
  }
}


export default RefreshCache;
