import React, { Component, Fragment } from 'react';
import { RA_API_URL, serverUrl, LOADING_TEXT } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import './CustomReportViews.scss';
import Loader from '../../shared/utlities/loader';

class CustomReportViews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CustomReportList: [],
      loaded: false
    };
    this.handleCheckboxSelection = this.handleCheckboxSelection.bind(this);
  }
  componentWillUnmount() {
    if (typeof this.props.activateErrorList === 'function') {
      this.props.activateErrorList(false, '');
      this.props.activateSuccessList(false, '');
    }
  }
  async getCustomReportList() {
    const CustomReportData = await getService({ method: 'GET', url: serverUrl + RA_API_URL.getCustomReport });
    if (CustomReportData && CustomReportData.status === 200) {
      if (CustomReportData.data.elements) {
        CustomReportData.data.elements.forEach(obj => {
          var allChecked = obj.elementDetails.filter(inner => inner.selected == true);
          obj.selected = (allChecked.length === obj.elementDetails.length);
        });
        this.CustomReportList = JSON.parse(JSON.stringify(CustomReportData.data.elements));
        this.setState({ CustomReportList: CustomReportData.data.elements, loaded: true })
      }
    }
  }
  updateCustomReportList = async () => {
    let CustomReportList = JSON.parse(JSON.stringify(this.state.CustomReportList));
    CustomReportList.forEach(report => {
      delete report.selected;
      report.elementDetails = report.elementDetails.filter(innerreport => {
        var findParentReport = this.CustomReportList.filter(rep => rep.categoryId === report.categoryId)[0];
        if (findParentReport) {
          var findchildReport = findParentReport.elementDetails.filter(rep => rep.elementName === innerreport.elementName)[0];
          return findchildReport && findchildReport.selected !== innerreport.selected;
        }
      })
    })
    let data = { elements: CustomReportList };
    const CustomReportData = await getService({ method: 'POST', url: serverUrl + RA_API_URL.getCustomReport, data });
    if (CustomReportData && CustomReportData.status === 200) {
      this.props.activateSuccessList(true, CustomReportData.data);
      this.props.activateErrorList(false, '');
      window.scrollTo(0, 0);
    } else if (CustomReportData && CustomReportData.status === 400) {
      this.props.activateSuccessList(false, '');
      this.props.activateErrorList(true, CustomReportData.data.errorList);
      window.scrollTo(0, 0);
    }
  }
  resetList = () => {
    let CustomReportList = JSON.parse(JSON.stringify(this.CustomReportList));
    this.setState({ CustomReportList })
  }
  componentWillMount() {
    this.getCustomReportList();

  }
  handleCheckboxSelection(e, childReport) {
    let { name, checked } = e.target;
    let { CustomReportList } = this.state;
    let parentName = childReport ? childReport.displayName : name;
    CustomReportList.forEach(obj => {
      if (obj.displayName === parentName) {
        if (!childReport) {
          obj.selected = checked;
        }
        obj.elementDetails.forEach(inner => {
          if (!childReport) {
            inner.selected = checked;
            inner.showInExport = inner.showInFASummary = (checked ? 1 : 0);
          } else if (inner.displayName === name) {
            inner.selected = checked;
            inner.showInExport = inner.showInFASummary = (checked ? 1 : 0);
            var allChecked = obj.elementDetails.filter(inner => inner.selected == true);
            obj.selected = (allChecked.length === obj.elementDetails.length);
          }
        });
      }
    });
    this.setState({ CustomReportList });
  }
  render() {
    const { CustomReportList, loaded } = this.state;
    return <Fragment>
      {loaded ?
        <div className="main customReportView">
          <h2 className="title">Custom Report Views</h2>
          <p className="desc">Select the table columns that must be made available to all organizations to display in the Analyze Transaction report.</p>
          <hr></hr>
          {CustomReportList.map((report, index) => {
            return report.categoryId !== -1 && <Fragment key={index}>
              <div className="div-seperator" >
                <div className="col-md-7 form-inline row">
                  <h6 className="apply-bold col-md-6 row">{report.displayName}</h6>
                  <div className="col-md-6 selectAll">
                    <div className="custom-control custom-checkbox">
                      <label key={report.displayName} className='form-label capitalize col-sm-12'>
                        <input
                          className="form-checkbox custom-control-input"
                          id={report.displayName}
                          name={report.displayName}
                          onChange={this.handleCheckboxSelection}
                          value={report.selected}
                          checked={report.selected}
                          type='checkbox'
                        />
                        <label className="form-label custom-control-label" htmlFor={report.displayName}>Select all</label>
                      </label>
                    </div>
                  </div>
                </div>
                <div className="custom-control custom-checkbox">
                  {report.elementDetails && report.elementDetails.map((innerobj, index) => {
                    return <label key={innerobj.displayName + index} className='form-label capitalize col-sm-3'>
                      <input
                        type='checkbox'
                        className="form-checkbox custom-control-input"
                        id={innerobj.displayName + index}
                        name={innerobj.displayName}
                        onChange={(e) => this.handleCheckboxSelection(e, report)}
                        value={innerobj.selected}
                        checked={innerobj.selected}
                      />
                      <label className="form-label custom-control-label" htmlFor={innerobj.displayName + index}>{innerobj.displayName}</label>
                    </label>
                  }
                  )}
                </div>
              </div>
              <hr></hr>
            </Fragment>
          }
          )}
          <div className="form-group row ml-2">
            <input className="secondary-btn" id="RESET" type="submit" value="SUBMIT" onClick={this.updateCustomReportList}></input>
            <input className="secondary-btn ml-2" id="EXPORT" type="submit" value="RESET" onClick={this.resetList}></input>
          </div>
        </div>
        : <div className="main">  <Loader show='true' />{LOADING_TEXT}</div>
      }
    </Fragment>
  }
}

export default CustomReportViews;
