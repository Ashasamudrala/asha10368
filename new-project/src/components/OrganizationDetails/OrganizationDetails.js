import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import { serverUrl, RA_API_URL} from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';
import { orgData } from '../../shared/utlities/renderer';
import TextArea from '../../shared/components/TextArea/TextArea';
import DeleteOrgInfo from './DeleteOrgInfo';
import './OrganizationDetails.css';

class OrganizationDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authMechanismOptions: [],
            localeOptions: [],
            mfaMethodTypes: [],
            mfaMethods: [],
            orgInfo: '',
            OrganizationInfo: true,
            expandInputs:false,
            orgData: '',
        }
        this.state.customAttributes = [
            {
                id: "1",
                name: '',
                value: ''
            }
        ];
    }
    componentDidMount = async () => {
        let OrgData = orgData('');
        this.setState({ orgData: OrgData });
        const getOrgDetails = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['searchactions']}/${OrgData.orgName}`,
        };
        const getOrgDetailsStatus = await getService(getOrgDetails);
        var arrNamesTypes = [];
        var arrNames = [];
        var authMechanismObj = [];
        if (getOrgDetailsStatus && getOrgDetailsStatus.status === 200) {
            let objectKeys = Object.keys(getOrgDetailsStatus.data.org.authMechanism);
            let objectValues = Object.values(getOrgDetailsStatus.data.org.authMechanism);
            for (let i = 0; i < objectKeys.length; i++) {
                authMechanismObj.push({
                    'key': objectKeys[i],
                    'content': objectValues[i]
                })
            }
            getOrgDetailsStatus.data && getOrgDetailsStatus.data.org.mfaMethodTypes.map((eachMethod) => {
                arrNamesTypes.push({
                    'content': eachMethod,
                    'key': eachMethod
                });
                return arrNamesTypes
            });
            getOrgDetailsStatus.data && getOrgDetailsStatus.data.org.mfaMethods.map((eachMethod) => {
                arrNames.push({
                    'content': eachMethod,
                    'key': eachMethod
                });
                return arrNames
            });
            if(getOrgDetailsStatus.data.org.customaAtributes){
                let customAttributes=getOrgDetailsStatus.data.org.customaAtributes;
                delete customAttributes.isReferGlobalEncryptionSet;
                var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
                var customArray=[];
                customAttributes && Object.keys(customAttributes).map((eachAtrribute)=>{
                 var obj={
                 id:id,
                 name:eachAtrribute,
                 value:customAttributes[eachAtrribute]};
                 customArray.push(obj);
                 return customArray
            })
            this.setState({customAttributes:customArray,expandInputs:true});
        }
            this.setState({ mfaMethodTypes: arrNamesTypes, mfaMethods: arrNames, orgInfo: getOrgDetailsStatus.data.org, authMechanismOptions: authMechanismObj });
        }
        const checkLocale = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['localeUrl']}`
        };
        const getLocale = await getService(checkLocale);
        if (getLocale.status === 200) {
            let objectKeys = Object.keys(getLocale.data.locales);
            let objectValues = Object.values(getLocale.data.locales);
            for (let i = 0; i < objectKeys.length; i++) {
                this.state.localeOptions.push({
                    'key': objectKeys[i],
                    'content': objectValues[i]
                })
            }
            this.setState({ localeOptions: this.state.localeOptions });
        }
    }
    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }
    handleRowDel(product) {
        if (this.state.customAttributes.length === 1) {
            return
        }
        var index = this.state.customAttributes.indexOf(product);
        this.state.customAttributes.splice(index, 1);
        this.setState(this.state.customAttributes);
    };

    handleAddEvent() {
        var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
        var product = {
            id: id,
            name: "",
            value: ""
        }
        this.state.customAttributes.push(product);
        this.setState(this.state.customAttributes);
    }
    handleProductTable(evt) {
        var item = {
            id: evt.target.id,
            name: evt.target.name,
            value: evt.target.value
        };
        var adminAttributes = this.state.customAttributes.slice();
        var newAdminAttributes = adminAttributes.map(function (attribute) {
            for (var key in attribute) {
                if ((key === item.name || key === item.value) && attribute.id === item.id) {
                    attribute[key] = item.value;
                }
            }
            return attribute;
        });
        this.setState({ customAttributes: newAdminAttributes });
    };
    handleChange = (field, e) => {
        const { orgInfo } = this.state;
        if(e.target.type === "checkbox"){
            orgInfo[field]=e.target.checked;
        }else{
            orgInfo[field]=e.target.value;
        }
        this.setState({ orgInfo: orgInfo });
    }
    updateOrganization = async () => {
        const { orgInfo } = this.state;
        let customaAtributes = {};
        for(let i = 0; i < this.state.customAttributes.length ; i++ ) {
            if(this.state.customAttributes[i].name && this.state.customAttributes[i].value){
                customaAtributes[this.state.customAttributes[i].name] = this.state.customAttributes[i].value
            }
        }
        const updateOrgDetails = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['searchactions']}`,
            data: {
                "orgName": orgInfo.orgName,
                "description": orgInfo.description,
                "displayName": orgInfo.displayName,
                "authMechanismType": orgInfo.authMechanismType,
                "preferredLocale": orgInfo.preferredLocale,
                "preferredDateTimeFormat": orgInfo.preferredDateTimeFormat,
                "orgIdleTimeout": orgInfo.orgIdleTimeout,
                "repoName": orgInfo.repoName,
                "isDefault": orgInfo.isDefault,
                "keyLabel": orgInfo.keyLabel,
                "storage": orgInfo.storage,
                "orgStatus": orgInfo.orgStatus,
                "referGlobalKeyLabel": orgInfo.referGlobalKeyLabel,
                "referGlobalConfig": orgInfo.referGlobalConfig,
                "referGlobal": orgInfo.referGlobal,
                "customaAtributes":customaAtributes ? customaAtributes : null,
                "mfaMethodType": orgInfo.mfaMethodType,
                "mfaChannelType": orgInfo.mfaChannelType,
                "mfaEnabled": orgInfo.mfaEnabled,
                "emailDomains": orgInfo.emailDomains
            }
        };
        const getOrgDetailsStatus = await getService(updateOrgDetails);
        if (getOrgDetailsStatus && getOrgDetailsStatus.status === 200) {
            const match = this.props.match;
            const urlParams = new URLSearchParams(window.location.search);
            const orgName = urlParams.get('orgname');
            const status = urlParams.get('status');
            const routeToRedirect = '/attribute-encryption';
            this.props.history.push({
                pathname: `${match.path}${routeToRedirect}`,
                search: `?orgname=${orgName}&status=${status}`,
                state: {
                    data: orgInfo,
                    basePath: match.path
                }
            })
        } else if (getOrgDetailsStatus.status === 400) {
            this.props.activateErrorList(true, getOrgDetailsStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    returnToSearch = () => {
        this.props.history.push({
            pathname: `/org/searchOrg`,
            state: {
                redirectToSearchOrg: true
            }
        })
    }
    handledoubleRightChange = () => {
        let item = [];
        if (this.state.optionDetails.length !== 0) {
            for (let i = 0; i < Object.keys(this.state.optionDetails).length; i++) {
                item = Object.keys(this.state.optionDetails)[i];
                this.state.selectOptions[item] = this.state.optionDetails[item];
                delete this.state.optionDetails[item];
                i = i - 1;
            }
        }
        this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions })
    }
    handledoubleLeftChange = () => {
        let item = [];
        if (this.state.selectOptions.length !== 0) {
            for (let i = 0; i < Object.keys(this.state.selectOptions).length; i++) {
                item = Object.keys(this.state.selectOptions)[i];
                this.state.optionDetails[item] = this.state.selectOptions[item];
                delete this.state.selectOptions[item]
                i = i - 1;
            }
        }
        this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions })
    }
    handleLeftChange = (node) => {
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });
        var selectedValues = selected.map(function (option) {
            return { "value": option.value, "id": option.id, "text": option.text };
        });
        for (let i = 0; i < selectedValues.length; i++) {
            delete this.state.optionDetails[selectedValues[i].value]
            this.state.selectOptions[selectedValues[i].value] = selectedValues[i].text;
        }
        this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions });
    }
    handleRightChange = (node) => {
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });
        var selectedValues = selected.map(function (option) {
            return { "value": option.value, "id": option.id, "text": option.text };
        });
        for (let i = 0; i < selectedValues.length; i++) {
            delete this.state.selectOptions[selectedValues[i].value]
            this.state.optionDetails[selectedValues[i].value] = selectedValues[i].text;
        }
        this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions });
    }
    render() {
        const { orgInfo, localeOptions, mfaMethodTypes, mfaMethods, authMechanismOptions } = this.state;
        return (<div className="main">
            {this.state.OrganizationInfo && orgInfo.orgStatus !== "Deleted" ?
                <div>
                    <h2 className="title">Organization Information</h2>
                    <p className="desc">Edit the required fields and click next to save the changes</p>
                    <div className="row">
                        <label className=" col-sm-2 control-label" >Organization Name</label>
                        <div className="col-sm-8 ">
                            <p className="form-control-static organization-label">{orgInfo.orgName}</p>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <SingleInput
                            title={'Display Name*'}
                            inputType={'text'}
                            content={orgInfo.displayName ? orgInfo.displayName : ''}
                            controlFunc={this.handleChange.bind(this, "displayName")}
                            name={'Display-name'} />
                        <SingleInput
                            title={'Description'}
                            inputType={'text'}
                            content={orgInfo.description ? orgInfo.description : ''}
                            controlFunc={this.handleChange.bind(this, "description")}
                            name={'Description'} />
                        <Select
                            name={'Administrator-Mechanism'}
                            title={'Administrator Authentication Mechanism'}
                            required={true}
                            selectedOption={orgInfo.authMechanismType}
                            options={authMechanismOptions}
                            controlFunc={this.handleChange.bind(this, "authMechanismType")}
                        />
                    </div>
                    <div className="row">
                        <label className=" col-sm-2 control-label" >Date Created</label>
                        <div className="col-sm-8 ">
                            <p className="form-control-static organization-label">{orgInfo.creationDate}</p>
                        </div>
                    </div><div className="row">
                        <label className=" col-sm-2 control-label" >Last Modified</label>
                        <div className="col-sm-8 ">
                            <p className="form-control-static organization-label">{orgInfo.lastModified}</p>
                        </div>
                    </div>
                    <div className="row">
                        <label className=" col-sm-2 control-label" >Default Organization</label>
                        <div className="col-sm-8 ">
                            <p className="form-control-static organization-label">{orgInfo.isDefault}</p>
                        </div>
                    </div>
                    <div className="row">
                        <label className=" col-sm-2 control-label" >Key Label</label>
                        <div className="col-sm-8 ">
                            <p className="form-control-static organization-label">{orgInfo.keyLabel}</p>
                        </div>
                    </div>
                    <div className="row">
                        <label className=" col-sm-2 control-label" >Storage Type</label>
                        <div className="col-sm-8 ">
                            <p className="form-control-static organization-label">{orgInfo.storage}</p>
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <span className="ecc-h1 row orgSectionLabels">Localization Configuration</span>
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">Use Global Configuration</label>
                            <div className="col-sm-8">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" defaultChecked={orgInfo.referGlobalConfig} id="customCheck1" onClick={this.handleChange.bind(this, "referGlobalConfig")} />
                                    <label className="custom-control-label" htmlFor="customCheck1"></label>
                                </div>
                            </div>
                        </div>
                        <SingleInput
                            title={'Date Time Format'}
                            inputType={'text'}
                            name={'dateTime'}
                            required={true}
                            content={orgInfo.preferredDateTimeFormat}
                            imgActivate={true}
                            controlFunc={this.handleChange.bind(this,"preferredDateTimeFormat")}
                            disabled={orgInfo.referGlobalConfig}>
                        </SingleInput>
                        <Select
                            name={'Preferred Locale'}
                            title={'Preferred Locale'}
                            options={localeOptions}
                            disabled={orgInfo.referGlobalConfig}
                            selectedOption={orgInfo.preferredLocale}
                            controlFunc={this.handleChange.bind(this, "preferredLocale")}
                        />
                        <span className="ecc-h1 row orgSectionLabels">Session Configuration</span>
                        <SingleInput
                            title={'Idle Timeout (in seconds)'}
                            inputType={'text'}
                            required={true}
                            content={orgInfo.orgIdleTimeout ? orgInfo.orgIdleTimeout : ''}
                            controlFunc={this.handleChange.bind(this, "orgIdleTimeout")}
                            name={'idle-timeout'} />
                    </div>
                    <span className="ecc-h1 row orgSectionLabels">Multi-Factor Authentication Configuration</span>
                    <div className="col-md-12 row">
                        <div className="col-sm-8">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">Enable Multi-Factor Authentication</label>
                                <div className="col-sm-8">
                                    <div className="custom-control custom-checkbox">
                                        <input type="checkbox" className="custom-control-input" defaultChecked={orgInfo.mfaEnabled} onClick={this.handleChange.bind(this, "mfaEnabled")} id="customCheck2" />
                                        <label className="custom-control-label" htmlFor="customCheck2"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {orgInfo.mfaEnabled ? <span className="col-md-4">
                            <b>Note:</b>Make sure that you mapped this organization to IDS Encryption Service.
                    </span> : ""}
                    </div>
                    <div className="col-sm-8">
                        <TextArea
                            title={`Accepted Domains for Email Factor (${orgInfo.orgName}.com)`}
                            resize={false}
                            required={orgInfo.mfaEnabled}
                            name={'email-factor'}
                            content={orgInfo.emailDomains ? orgInfo.emailDomains : ''}
                            placeholder={''} controlFunc={this.handleChange.bind(this, "emailDomains")} />
                        <Select
                            name={'2FA-method'}
                            title={'Select the 2FA method'}
                            required={orgInfo.mfaEnabled}
                            options={mfaMethodTypes ? mfaMethodTypes : ['']}
                            selectedOption={orgInfo.mfaMethodType}
                            controlFunc={this.handleChange.bind(this, "mfaMethodType")}
                        />
                        <Select
                            name={'delivery-channel'}
                            title={'Delivery channel'}
                            required={orgInfo.mfaEnabled}
                            options={mfaMethods ? mfaMethods : ['']}
                            selectedOption={orgInfo.mfaChannelType}
                            controlFunc={this.handleChange.bind(this, "mfaChannelType")}
                        />
                    </div>

                    <div className="col-sm-5">
                        <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)} 
                        onRowAdd={this.handleAddEvent.bind(this)} 
                        onRowDel={this.handleRowDel.bind(this)} 
                        products={this.state.customAttributes} 
                        expandInputs={this.state.expandInputs}
                        />
                    </div>
                    <div className="form-group form-submit-button row mt-2 ml-2">
                        <input className="custom-secondary-btn" id="custom-secondary-btn" type="submit" value="RETURN TO SEARCH" onClick={this.returnToSearch} />
                        <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.updateOrganization} />
                    </div>
                </div> : <DeleteOrgInfo orgInfo={orgInfo} getInfoDelete={this.returnToSearch}></DeleteOrgInfo>
            }
        </div>);
    }
}
export default OrganizationDetails;
