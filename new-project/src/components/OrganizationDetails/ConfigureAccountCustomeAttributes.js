import React, { Component } from 'react';
import './OrganizationDetails.css'
import { getService } from '../../shared/utlities/RestAPI';
import * as _ from 'underscore';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import './ConfigureAccount.scss';
class ConfigureAccountCustomeAttributes extends Component {
    constructor(props) {
        super(props)
        let accountType = [];
        let configInputValues = [];
        this.state = {
            accountCustomAttribute: true,
            accoutTypes: configInputValues,
            account: accountType
        }
    }

    moveConfigureEmailTelephoneType = async () => {
        let data = [];
        this.state.accoutTypes.forEach((item) => {
            data.push({ 'name': item.id, 'displayName': item.id, 'customAttributes': {}, 'scopeAll': true });
        });
        let accountData = _.uniq(data, function (v) {
            return v.name;
        })

        accountData.forEach((item) => {
            this.state.accoutTypes.forEach((data) => {
                if (item.name === data.id) {
                    item.customAttributes[data.value] = data.value
                }
            })
        });
        const configureCustomAttributes = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['addAccounttypesToOrganization']}`,
            data: accountData
        }
        const configureAccountStatus = await getService(configureCustomAttributes);

        if (this.props.location.state.updateOrg) {
            if (configureAccountStatus && configureAccountStatus.status === 200) {

                const urlParams = new URLSearchParams(window.location.search);
                const orgName = urlParams.get('orgname');
                const status = urlParams.get('status');
                const routeToRedirect = '/configure-account';
                this.props.history.push({
                    pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                    search: `?orgname=${orgName}&status=${status}`,
                    state: {
                        emailTelephone: this.props.location.state.swappingData,
                        basePath: this.props.location.state.basePath
                    }
                })
            }
        }
        else {
            if (configureAccountStatus && configureAccountStatus.status === 200) {
                const contactType = this.state.contactType;
                const routeToRedirect = '/configure-email-telephone-type';
                this.props.history.push({
                    pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                    state: {
                        emailTelephone: this.props.location.state.swappingData,
                        redirectFromOrg: true,
                        basePath: this.props.location.state.basePath
                    }
                })
            }
        }
    }

    addRow = (index, configtype) => {
        let configInputValues = { 'id': configtype };
        configInputValues['type'] = configtype + (this.state.account[index].customAttributes.length + 1);
        configInputValues['value'] = '';
        let data = [...this.state.account];
        let count = data[index].count[data[index].count.length - 1] + 1;
        data[index].count.push(count);
        this.state.account[index].customAttributes.splice(this.state.account[index].customAttributes.length + 1, 0, configInputValues);
        this.setState({ data });
    }

    removeRow = (index, row, value) => {
        if (this.state.account[index].count.length === 1) {
            return
        }
        let accoutTypes = this.state.accoutTypes;
        let indexPosition = accoutTypes.map(x => {
            return x.type;
        }).indexOf(value);

        accoutTypes.splice(indexPosition, 1);
        let item = this.state.account[index].count.indexOf(row);
        this.state.account[index].count.splice(item, 1);
        if (this.state.account[index].customAttributes !== null) {
            this.state.account[index].customAttributes.splice(item, 1);
        }
        this.setState(this.state.account);
        this.setState({ accoutTypes: accoutTypes });
    }
    InputValue(data, index, row, e) {
        let configInputValues = { 'id': data };
        configInputValues['type'] = e.target.id;
        configInputValues['value'] = e.target.value;
        let splicedaccountType = [...this.state.accoutTypes]
        let flag = false;
        let item = this.state.account[index].count.indexOf(row);

        if (this.state.account[index].customAttributes !== null) {
            if (this.state.account[index].customAttributes.length >= item) {
                this.state.account[index].customAttributes.splice(item, 1);
            }
        }
        this.state.account[index].customAttributes.splice(item, 0, configInputValues);
        if (splicedaccountType.length !== 0) {
            splicedaccountType.forEach(function (attribute, index) {
                if ((attribute.id === configInputValues.id && attribute.type === e.target.id)) {
                    attribute['value'] = configInputValues.value;
                    flag = true;
                    splicedaccountType.splice(index, 1);
                    splicedaccountType.push(attribute);
                    return;
                }
            });
        }
        if (!flag) {
            splicedaccountType.push(configInputValues)
        }
        this.setState({
            accoutTypes: [...splicedaccountType]
        });
    }

    returnToSearch = () => {
        this.props.history.push({
            pathname: `/org/searchOrg`,
            state: {
                redirectToSearchOrg: true
            }
        })
    }
    componentDidMount = async () => {
        let orgName = this.props.location.state.swappingData.orgName;
        const getConfigAttributes = {
            method: 'GET',
            url: `${serverUrl}/organization/${orgName}`
        };
        const getConfigAttributesStatus = await getService(getConfigAttributes);
        if (getConfigAttributesStatus && getConfigAttributesStatus.status === 200) {
            let accountType = [];
            let objCount = [];
            let customAttributes = [];
            let configInputValues = [];
            if (getConfigAttributesStatus.data.org.accountTypes) {
                getConfigAttributesStatus.data.org.accountTypes.forEach((data) => {
                    let obj = {}
                    customAttributes = [];
                    objCount = [];
                    obj.type = data.name
                    obj.displayName = data.displayName
                    obj.count = [0]
                    obj.customAttributes = [{ 'id': data.name, 'type': data.name + 0, 'value': '' }]
                    if (data.customAttributes !== null) {
                        objCount = [];
                        customAttributes = [];
                        Object.keys(data.customAttributes).forEach((item, index) => {
                            let obj = {};
                            obj['id'] = data.name;
                            obj['type'] = data.name + index;
                            obj['value'] = item;
                            objCount.push(index);
                            configInputValues.push(obj);
                            customAttributes.push(obj)
                        })
                    }
                    if (objCount.length !== 0) {
                        obj.count = objCount
                    }

                    if (customAttributes.length !== 0) {
                        obj.customAttributes = customAttributes;
                    }

                    accountType.push(obj);
                });
            }
            this.setState({
                accoutTypes: configInputValues,
                account: accountType
            });
        }
    }

    render() {
        let updateOrg = this.props.location.state.updateOrg || false;
        return (<div className="main configure-account">
            <div>
                <h2 className="title">{RA_STR.configureAccountCustomtitle}</h2>
                <p className="desc">{RA_STR.configureAccountCustomtab1}</p>
                <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.configureAccountCustomtab2 }}></p>

                <table className="col-md-10 table">
                    <tbody>
                        <tr>
                            <th>{RA_STR.accountType}</th>
                            <th>{RA_STR.accountDisplayName}</th>
                            <th>{RA_STR.customAttributes}</th>
                        </tr>
                        {this.state.account.map((eachAccount, index) => {
                            return (
                                <tr key={index}>
                                    <td >{eachAccount.type}</td>
                                    <td >{eachAccount.displayName}</td>

                                    <td>
                                        {this.state.account[index].count.map((eachRow, i) => {
                                            return (
                                                <div className="customeAttribute " key={i}>
                                                    <input type="text"
                                                        maxLength='64'
                                                        className='form-control'
                                                        id={eachAccount.type + i}
                                                        onChange={(e) => this.InputValue(eachAccount.type, index, eachRow, e)}
                                                        value={
                                                            i <= (this.state.account[index]['customAttributes'].length)
                                                                ? (this.state.account[index]['customAttributes'][i]
                                                                    ? this.state.account[index]['customAttributes'][i]['value']
                                                                    : '')
                                                                : ''
                                                        } />
                                                    <img src="images/plus.jpg"
                                                        onClick={() => this.addRow(index, eachAccount.type, this)}
                                                        className="customeAttribute" />
                                                    <img className="customeAttribute"
                                                        src={
                                                            this.state.account[index].count.length == 1
                                                                ? 'images/cross_disabled.gif'
                                                                : 'images/cross_enabled.gif'
                                                        }
                                                        onClick={() => this.removeRow(index, eachRow, eachAccount.type + i)} />
                                                </div>)
                                        })}
                                    </td>
                                </tr>
                            )
                        })
                        }
                    </tbody>
                </table>
                {
                    (updateOrg) ?
                        <div className="form-group form-submit-button row mt-2 ml-2">
                            <input className="custom-secondary-btn" id="custom-secondary-btn" type="button" value="RETURN TO SEARCH" onClick={this.returnToSearch}></input>
                            <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveConfigureEmailTelephoneType}></input>
                        </div> :
                        <div className="form-group form-submit-button row mt-2 ml-2">
                            <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveConfigureEmailTelephoneType}></input>
                        </div>


                }
            </div>
        </div>);
    }
}
export default ConfigureAccountCustomeAttributes;
