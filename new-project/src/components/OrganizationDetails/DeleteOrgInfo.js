import React, { Component } from 'react';
class DeleteOrgInfo extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {orgInfo}=this.props;
        return <div>
            <h2 className="title">Organization Information</h2>
            <p className="desc">The details of the deleted organization are:</p>
            <span className="ecc-h1 orgSectionLabels">Organization Details</span>
            <div className="row">
                <label className=" col-sm-2 control-label" >Organization Name:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.orgName}</p>
                </div>
            </div>
            <div className="row">
                <label className=" col-sm-2 control-label" >Display Name:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.displayName}</p>
                </div>
            </div>
            <div className="row">
                <label className=" col-sm-2 control-label" >Description:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.description}</p>
                </div>
            </div>
            <div className="row">
                <label className=" col-sm-2 control-label" >Status:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.orgStatus}</p>
                </div>
            </div>
            <div className="row">
                <label className=" col-sm-2 control-label" >Date Created:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.creationDate}</p>
                </div>
            </div>
            <div className="row">
                <label className=" col-sm-2 control-label" >Last Modified:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.lastModified}</p>
                </div>
            </div>
            <div className="row">
                <label className=" col-sm-2 control-label" >Default Organization:</label>
                <div className="col-sm-8 ">
                    <p className="form-control-static organization-label">{orgInfo.isDefault}</p>
                </div>
            </div>
            <input type="submit" value="OK" onClick={this.props.getInfoDelete}></input>
        </div>
    }
}
export default DeleteOrgInfo;