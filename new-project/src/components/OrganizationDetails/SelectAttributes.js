import React, { Component } from 'react';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import { getService } from '../../shared/utlities/RestAPI';
import './OrganizationDetails.css';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';

class SelectAttributes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectAttributesDisplay: true,
            defaultGloabalConfig: this.props.location.state.data.referGlobalEncryptionSet,
            selectOptions: {},
            optionDetails: {}
        }
    }
    componentDidMount() {
        const getEncset = this.props.location.state.data;
        this.setState({ optionDetails: getEncset.potentialEncSet, selectOptions: getEncset.chosenEncSet });
    }
    _handleChange = (e) => {
        let checkStatus = e.target;
        if (checkStatus.checked) {
            this.setState({ defaultGloabalConfig: true });
        } else {
            this.setState({ defaultGloabalConfig: false });
        }
    }
    moveToUpdateAdministrator = async () => {
        let data = {
            "chosenEncSet": this.state.selectOptions,
            "orgName": this.props.location.state.data.orgName,
            "referGlobalEncryptionSet": this.state.defaultGloabalConfig
        }
        const encryptionSet = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['encryptionset']}`,
            data: data
        }
        const encryptionSetStatus = await getService(encryptionSet);
        if (encryptionSetStatus && encryptionSetStatus.status === 200) {
            this.redirectToUpdateAdmin();
        }
    }
    redirectToUpdateAdmin = () => {
        let createPage = this.props.location.state.createPage;
        const routeToRedirect = '/updateadmin';
        const pageRedirect = true;
        if (createPage !== undefined) {
            this.props.history.push({
                pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                state: {
                    swappingData: this.props.location.state.data,
                    basePath: this.props.location.state.basePath,
                    addAdministrator: pageRedirect
                }
            })
        } else {
            const urlParams = new URLSearchParams(window.location.search);
            const orgName = urlParams.get('orgname');
            const status = urlParams.get('status');
            this.props.history.push({
                pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                search: `?orgname=${orgName}&status=${status}`,
                state: {
                    swappingData: this.props.location.state.data,
                    basePath: this.props.location.state.basePath,
                    addAdministrator: !pageRedirect
                }
            })
        }
    }
    handledoubleRightChange = () => {
        let { selectOptions, optionDetails } = this.state;
        let multiselectObject = CA_Utils.handledoubleRightChange(optionDetails, selectOptions, 'nooptiongroup');
        this.setState(multiselectObject);
    }

    handledoubleLeftChange = () => {
        let { selectOptions, optionDetails } = this.state;
        let multiselectObject = CA_Utils.handledoubleLeftChange(optionDetails, selectOptions, 'nooptiongroup');
        this.setState(multiselectObject);
    }


    handleLeftChange = (node) => {
        let multiselectObject = {};
        let { selectOptions, optionDetails } = this.state;
        multiselectObject = CA_Utils.handleLeftChange(optionDetails, selectOptions, 'nooptiongroup', node);
        this.setState(multiselectObject);
    }

    handleRightChange = (node) => {
        let multiselectObject = {};
        let { selectOptions, optionDetails } = this.state;
        multiselectObject = CA_Utils.handleRightChange(optionDetails, selectOptions, 'nooptiongroup', node);
        this.setState(multiselectObject);
    }

    returnToSearch = () => {
        this.props.history.push({
            pathname: `/org/searchOrg`,
            state: {
                redirectToSearchOrg: true
            }
        })
    }


    render() {
        let createPage = this.props.location.state.createPage;
        return (
            <div className="main">

                <div>
                    <h2 className="title">{RA_STR.selectAttributeTitle}</h2>
                    <p className="desc">{RA_STR.selectAttributeTab1}</p>
                    <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.selectAttributeTab2 }}></p>
                    <span className="ecc-h1">{RA_STR.selectAttributeHeading}</span>
                    <div className="row">
                        <div className="row col-sm-12">
                            <div className="col-sm-3">
                                <Checkbox
                                    type={'checkbox'}
                                    setName="scopeAll"
                                    controlFunc={this._handleChange}
                                    options={["Use Global Configuration"]}
                                    selectedOptions={this.state.defaultGloabalConfig ? ["Use Global Configuration"] : [""]} />
                            </div>
                            <div className="col-sm-1">OR</div>
                            <div className="col-sm-8">
                                <SwappingSelectBox selectOptions={this.state.selectOptions}
                                    optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={false} rightMenuName='Attributes Selected for encryption' leftMenuName='Available Attributes for encryption' disableSwapping={this.state.defaultGloabalConfig} />
                            </div>
                        </div>
                    </div>{
                        createPage ?
                            <div className="form-group form-submit-button row mt-2 ml-2">
                                <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveToUpdateAdministrator}></input>
                            </div> : <div className="form-group form-submit-button row mt-2 ml-2">
                                <input className="custom-secondary-btn" id="custom-secondary-btn" type="submit" value="SKIP" onClick={this.moveToUpdateAdministrator}></input>
                                <input className="custom-secondary-btn" id="custom-secondary-btn" type="submit" value="RETURN TO SEARCH" onClick={this.returnToSearch}></input>
                                <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveToUpdateAdministrator}></input>
                            </div>}
                </div>
            </div>
        )
    }

}

export default SelectAttributes