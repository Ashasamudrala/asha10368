import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import { serverUrl, RA_API_URL, RA_STR_OPTIONDETAILS, RA_STR_SELECTOPTION } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';

class ConfigureAccountType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      configureAccountType: true,
      selectOptions: [],
      optionDetails: [],
      options: [],
      swappingData: this.props.location.state.swappingData
    }
  }
  componentDidMount() {
    let leftMenuItems = [];
    let rightMenuItems = [];
    let multiselectObject = {};
    let scopes = JSON.parse(JSON.stringify(this.props.location.state.swappingData.scopes));
    let indexValue = 0;
    (scopes).forEach(function (data, index) {
      if (data.scopeType === "disabled") {
        data['disabled'] = false;
        data['privilegeId'] = index;
        data['displayName'] = data.accountName;
        leftMenuItems.push(data);
        indexValue = index;
      }
    });
    indexValue = indexValue + 1;
    (scopes).forEach(function (data, index) {
      if (data.scopeType === "enabled") {
        if (data.headerName === 'Organization-Specific Accounts') {
          data['disabled'] = false;
          data['privilegeId'] = indexValue + index;
          data['displayName'] = data.accountName;
          rightMenuItems.push(data);
        } else {
          data['disabled'] = true;
          data['privilegeId'] = indexValue + index;
          data['displayName'] = data.accountName;
          rightMenuItems.push(data);
        }
      }
    })
    scopes = CA_Utils.swapSorting(scopes);
    this.getPrivileges(leftMenuItems, RA_STR_OPTIONDETAILS);
    this.getPrivileges(rightMenuItems, RA_STR_SELECTOPTION);
    multiselectObject = CA_Utils.objectFormation(scopes);
    this.setState({ emptyList: multiselectObject['selectOptions'], optDetails: multiselectObject['data'] });
  }

  getPrivileges = (turnedOffPrivileges, optionValue) => {
    let multiselectObject = CA_Utils.objectFormation(turnedOffPrivileges);
    if (optionValue === RA_STR_SELECTOPTION) {
      this.setState({ selectOptions: multiselectObject['optionDetails'], optDetails: multiselectObject['data'] });
    }
    else {
      this.setState({ optionDetails: multiselectObject['optionDetails'], emptyList: multiselectObject['selectOptions'], options: multiselectObject['data'] });
    }
  }
  handleLeftChange = (node) => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let selectedValues = CA_Utils.getselectedValues(node);
    let multiselectObject = {};
    if (selectedValues.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handleLeftChange(optionDetails, selectOptions, 'update', node);
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  getKeys = (optionDetails) => {
    Object.keys(optionDetails[0]).filter(function (item) {
      if (optionDetails[0][item].length !== 0) {
        return item;
      }
    });
  }
  handleRightChange = (node) => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let selectedValues = CA_Utils.getselectedValues(node);
    let multiselectObject = {};
    if (selectedValues.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handleRightChange(optionDetails, selectOptions, 'update', node);
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  setSelectedOptionObject = (selectOptions) => {
    let emptyList = JSON.parse(JSON.stringify(this.state.emptyList));
    return CA_Utils.setSelectedOptionObject(selectOptions, emptyList);
  }
  setSelectedLeftObject = (optionDetails) => {
    let { options } = this.state;
    let selectedOptions = CA_Utils.setSelectedLeftObject(optionDetails, options);
    return selectedOptions;
  }
  setSelectedRightObject = (selectOptions) => {
    let { optDetails } = this.state;
    let selectedOptions = CA_Utils.setSelectedRightObject(selectOptions, optDetails);
    return selectedOptions;
  }
  handledoubleRightChange = () => {
    let multiselectObject = {};
    let { optionDetails, options, selectOptions, optDetails } = this.state;
    if (this.state.optionDetails.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handledoubleRightChange(optionDetails, selectOptions, 'disableupdate');
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
      this.setState(multiselectObject);
    }
  }
  handledoubleLeftChange = () => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let multiselectObject = {};
    if (this.state.selectOptions.length !== 0) {
      optionDetails = this.setSelectedOptionObject(optionDetails);
      selectOptions = this.setSelectedOptionObject(selectOptions);
      multiselectObject = CA_Utils.handledoubleLeftChange(optionDetails, selectOptions, 'disableupdate');
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  getSelectedScopes = () => {
    let scopes = [];
    if (this.state.selectOptions.length !== 0) {
      for (let i = 0; i < Object.keys(this.state.selectOptions[0]).length; i++) {
        let item = Object.keys(this.state.selectOptions[0]);
        for (let y = 0; y < this.state.selectOptions[0][item[i]].length; y++) {

          let data = this.state.selectOptions[0][item[i]][y];
          if (!(data.disabled === true)) {
            scopes.push(data['accountName']);
          }
        }
      }
    }
    return scopes;
  }
  moveConfigureAccountCustomeAttributes = async () => {
    let data = {
      "scopes": this.getSelectedScopes(),
      "orgName": this.props.location.state.swappingData.orgName,
    }
    this.getSelectedScopes().map((data) => {
      let accountObj = {
        'name': data,
        'displayName': data,
        'scopeAll': false,
        'customAttributes': null,
        'availableGroupedResources': null,
        'selectedGroupedResources': null,
        'customAttributesString': null
      }
      this.props.location.state.swappingData.accountTypes.push(accountObj)
    }

    );
    const encryptionSet = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['saveEnabledAccounts']}`,
      data: data
    }
    const configureAccountStatus = await getService(encryptionSet);
    if (configureAccountStatus && configureAccountStatus.status === 200) {
      if (this.props.location.state.updateOrg) {
        const urlParams = new URLSearchParams(window.location.search);
        const orgName = urlParams.get('orgname');
        const status = urlParams.get('status');
        const routeToRedirect = '/configure-custom-attribute';
        this.props.history.push({
          pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
          search: `?orgname=${orgName}&status=${status}`,
          state: {
            orgName: this.props.location.state.swappingData.orgName,
            swappingData: this.props.location.state.swappingData,
            basePath: this.props.location.state.basePath,
            updateOrg: true
          }
        })
      }
      else {
        const routeToRedirect = '/configure-custom-attribute';
        this.props.history.push({
          pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
          state: {
            orgName: this.props.location.state.orgName,
            swappingData: this.props.location.state.swappingData,
            basePath: this.props.location.state.basePath
          }
        })
      }
    }
  }

  returnToSearch = () => {
    this.props.history.push({
        pathname: `/org/searchOrg`,
        state: {
            redirectToSearchOrg: true
        }
    })
  }

  render() {
    return (<div className="main">
      <div>
        <h2 className="title">{RA_STR.configureAccountTitle}</h2>
        <p className="desc">{RA_STR.configureAccountTab1}</p>
        <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.configureAccountTab2 }}></p>
        <span className="ecc-h1 accountTag ">{RA_STR.configureAccountHeading}</span>
        <div className="row ml-1">
          <SwappingSelectBox selectOptions={this.state.selectOptions}
            optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={true} rightMenuName='Enabled' leftMenuName='Disabled' />
        </div>
        {!(this.props.location.state.updateOrg) ?
          <div className="form-group form-submit-button row mt-2 ml-2">
            <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveConfigureAccountCustomeAttributes}></input>
          </div> :
          <div className="form-group form-submit-button row mt-2 ml-2">
            <input className="custom-secondary-btn"  type="button" value="RETURN TO SEARCH" onClick={this.returnToSearch}></input>
            
            <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveConfigureAccountCustomeAttributes}></input>
          </div>}
      </div>

    </div>);
  }
}

export default ConfigureAccountType;