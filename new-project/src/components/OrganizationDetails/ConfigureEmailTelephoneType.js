import React, { Component } from 'react';
import EmailConfiguration from '../EmailConfiguration/EmailConfiguration';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
class ConfigureEmailTelphoneType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contactData: [],
            telephoneData: []
        }
    }
    componentDidMount() {
        let filteredContacts = this.props.location.state.emailTelephone.contactType.filter((eachContact) => {
            return eachContact.contactType === "E-MAIL"
        })
        filteredContacts.map((eachFilter) => {
            var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
            eachFilter.disableRow = true
            eachFilter.id = id
            eachFilter.priority = parseInt(eachFilter.priority)
        })
        filteredContacts.sort((a, b) => a.priority - b.priority);
        this.setState({
            contactData: filteredContacts
        }, () => {
            let telephoneContacts = this.props.location.state.emailTelephone.contactType.filter((eachContact) => {
                return eachContact.contactType === "TELEPHONE"
            })
            var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
            telephoneContacts.map((eachFilter) => {
                eachFilter.disableRow = true
                eachFilter.id = id
                eachFilter.priority = parseInt(eachFilter.priority)
            })
            this.setState({ telephoneData: telephoneContacts })
        })
    }
    handleRowDel = (eachContact) => {
        if (eachContact.contactType === "E-MAIL") {
            if (this.state.contactData.length === 1) {
                return
            }
            var index = this.state.contactData.indexOf(eachContact);
            this.state.contactData.map((data ) => {
                if( data.priority > index) {
                    data['priority'] =  data['priority'] - 1 
                }
            })
            this.state.contactData.splice(index, 1);
            this.setState(this.state.contactData);
        } else {
            if (this.state.telephoneData.length === 1) {
                return
            }
            var index = this.state.telephoneData.indexOf(eachContact);
            this.state.telephoneData.map((data ) => {
                if( data.priority > index) {
                    data['priority'] =  data['priority'] - 1 
                }
            })
            this.state.telephoneData.splice(index, 1);
            this.setState(this.state.telephoneData);
        }
    }
    saveConfigureData = async () => {
        const storecontactData = this.state.contactData;
        const storetelephoneData = this.state.telephoneData;
        const myprop = JSON.parse(JSON.stringify(this.state.contactData)).filter(function (props) {
            delete props.id;
            delete props.disableRow;
            props['priority'] = (props['priority']).toString()
            return true;
        });
        const mypropTelephone = JSON.parse(JSON.stringify(this.state.telephoneData)).filter(function (props) {
            delete props.id;
            delete props.disableRow;
            props['priority'] = (props['priority']).toString();
            return true;
        });
        let data = {
            "orgName": this.props.location.state.emailTelephone.orgName,
            "contactType": myprop.concat(mypropTelephone)
        }
        const saveContact = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['contacts']}`,
            data: data
        }
        const saveContactStatus = await getService(saveContact);
        if (saveContactStatus && saveContactStatus.status === 200) {
           if(this.props.location.state && this.props.location.state.redirectFromOrg){
            const routeToRedirect = '/activate-organisation';
            this.props.history.push({
                pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                state: {
                    orgName: this.props.location.state.emailTelephone.orgName 
                }
            })
            this.props.activateErrorList(false, '');
           }
           
            else {
            const urlParams = new URLSearchParams(window.location.search);
            const orgName = urlParams.get('orgname');
            const status = urlParams.get('status');
            if (saveContactStatus.data && saveContactStatus.data.message) {
            this.props.activateErrorList(false, '');
            this.props.activateSuccessList(true, saveContactStatus.data);
            }
            this.props.history.push({
                pathname: `${this.props.location.state.basePath}`,
                search: `?orgname=${orgName}&status=${status}`,
                state : { successData : saveContactStatus.data.message }
            })
        }
        } else  if(saveContactStatus && (saveContactStatus.status !== 200)){
            this.props.activateErrorList(true, saveContactStatus.data.errorList);
            this.props.activateSuccessList(false, '');
            this.setState({ contactData: storecontactData, telephoneData: storetelephoneData })
        }
    }
    handleAddEvent = (getdata) => {
        var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
        if (getdata === 'email') {
            var emailData = {
                id: id,
                contactType: "E-MAIL",
                contactName: "",
                displayName: "",
                priority: this.state.contactData.length + 1,
                mandatory: "0",
                newContact:true
            }
            this.state.contactData.push(emailData);
            this.setState(this.state.contactData);
        } else {
            var telephoneDataGroup = {
                id: id,
                contactType: "TELEPHONE",
                contactName: "",
                displayName: "",
                priority: this.state.telephoneData.length + 1,
                mandatory: "0",
                newContact:true
            }
            this.state.telephoneData.push(telephoneDataGroup);
            this.setState(this.state.telephoneData);
        }
    }
    handleContactDetails(evt) {
        var item = {
            id: evt.target.id,
            name: evt.target.name,
            value: evt.target.value
        };
        var contactData = '';
        if (evt.target.dataset.contacttype === "E-MAIL") {
            contactData = this.state.contactData.slice();
        } else if (evt.target.dataset.contacttype === "TELEPHONE") {
            contactData = this.state.telephoneData.slice();
        }
        var newContactData = contactData.map(function (emailTelephone) {
            for (var key in emailTelephone) {
                if (key === item.name && emailTelephone.id === item.id) {
                    emailTelephone[key] = item.value;
                }
            }
            return emailTelephone;
        });
        if (evt.target.dataset.contacttype === "E-MAIL") {
            this.setState({ contactData: newContactData });
        } else if (evt.target.dataset.contacttype === "TELEPHONE") {
            this.setState({ telephoneData: newContactData });
        }
    }



    getPriorityData = (getClickStatus, getProduct) => {
        let getStatusChanges = '';
        let totalLength = '';
        if ((getProduct.contactType === "E-MAIL" && getProduct.priority <= this.state.contactData.length) || (getProduct.contactType === "TELEPHONE" && getProduct.priority <= this.state.telephoneData.length)) {
            if (getProduct.contactType === "E-MAIL") {
                totalLength = this.state.contactData.length;
            } else if (getProduct.contactType === "TELEPHONE") {
                totalLength = this.state.telephoneData.length;
            }
        }
        if (getClickStatus === 'inc' && getProduct.priority !== 1) {
            getStatusChanges = getProduct.priority - 1;
        } else if (getClickStatus === 'dec' && getProduct.priority !== totalLength) {
            getStatusChanges = getProduct.priority + 1;
        }

        if (getProduct.contactType === "E-MAIL") {
            let emailContacts = getStatusChanges && this.state.contactData.map((eachContact, key) => {
                if (eachContact.id === getProduct.id) {
                    eachContact.priority = getStatusChanges;
                    if (getClickStatus === 'inc') {
                        this.state.contactData[key - 1].priority = getProduct.priority + 1;
                    } else if (getClickStatus === 'dec') {
                        this.state.contactData[key + 1].priority = getProduct.priority - 1;
                    }
                }
                return eachContact
            })
            if (emailContacts.length) {
                emailContacts && emailContacts.sort((a, b) => a.priority - b.priority);
                this.setState({ contactData: emailContacts });
            }
        } else if (getProduct.contactType === "TELEPHONE") {
            let telephoneContacts = getStatusChanges && this.state.telephoneData.map((eachContact, key) => {
                if (eachContact.id === getProduct.id) {
                    eachContact.priority = getStatusChanges;
                    if (getClickStatus === 'inc') {
                        this.state.telephoneData[key - 1].priority = getProduct.priority + 1;
                    } else if (getClickStatus === 'dec') {
                        this.state.telephoneData[key + 1].priority = getProduct.priority - 1;
                    }
                }
                return eachContact
            })
            if (telephoneContacts.length) {
                telephoneContacts && telephoneContacts.sort((a, b) => a.priority - b.priority);
                this.setState({ telephoneData: telephoneContacts });
            }
        }

    }
    returnToSearch = () => {
        this.props.history.push({
            pathname: `/org/searchOrg`,
            state: {
                redirectToSearchOrg: true
            }
        })
    }

    redirectToHelp = () => {
        const routeToRedirect = '/activate-organisation';
        this.props.history.push({
            pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
            state: {
                orgName:  this.props.location.state.emailTelephone.orgName ,
            }
        })
    }
    render() {
        return (<div className="main">
            <div>
                <h2>Configure Email/Telephone Type</h2>
                <p className="desc">Configure Email and/or Telephone Type</p>
                <p className="desc">Note: You must refresh this organization cache of product servers for email/telephone type configuration changes to take effect. </p>
                <span>Configure Email Type</span>
                <EmailConfiguration onProductTableUpdate={this.handleContactDetails.bind(this)} onRowAdd={this.handleAddEvent} onRowDel={this.handleRowDel} products={this.state.contactData} tableType={'email'} getPriorityStatus={this.getPriorityData} />
                <span>Configure Telephone Type</span>
                <EmailConfiguration onProductTableUpdate={this.handleContactDetails.bind(this)} onRowAdd={this.handleAddEvent} onRowDel={this.handleRowDel} products={this.state.telephoneData} tableType={'telephone'} getPriorityStatus={this.getPriorityData} />
              {this.props.location.state  && this.props.location.state.redirectFromOrg === true ? 
                <div className="form-group form-submit-button row mt-2 ml-2">
                    <input className="custom-secondary-btn"  type="submit" value="SKIP" onClick={this.redirectToHelp}></input>
                    <input className="secondary-btn ml-3"  type="submit" value="SAVE" onClick={this.saveConfigureData}></input>
              </div> :
               <div className="form-group form-submit-button row mt-2 ml-2">
                    <input className="custom-secondary-btn"  type="submit" value="RETURN TO SEARCH" onClick={this.returnToSearch}></input>
                    <input className="secondary-btn ml-3"  type="submit" value="SAVE" onClick={this.saveConfigureData}></input>
              </div> 
              }
            </div>

        </div>)
    }
}

export default ConfigureEmailTelphoneType;