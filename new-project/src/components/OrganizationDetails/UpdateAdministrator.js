import React, { Component } from 'react';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';

class UpdateAdministrator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UpdateAdministrator: true,
            leftMenuName: 'Available Administrators',
            rightMenuName: 'Managing Administrators',
            selectOptions: {},
            optionDetails: {}
        }
    }
    componentDidMount() {
        const getswappingData = this.props.location.state.swappingData;
        this.setState({ optionDetails: getswappingData.availableAdmins, selectOptions: getswappingData.managingAdmins });
    }
    moveConfigureAccountType = async () => {
        let data = {
            "managingAdmins": this.state.selectOptions.filter(Boolean),
            "orgName": this.props.location.state.swappingData.orgName,
        }
        const managingadmins = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['managingadmins']}`,
            data: data
        }
        const managingadminsStatus = await getService(managingadmins);
        if (managingadminsStatus && managingadminsStatus.status === 200) {
            if (this.props.location.state.addAdministrator) {
                const routeToRedirect = '/configure-account';
                this.props.history.push({
                    pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                    state: {
                        orgName: this.props.location.state.swappingData.orgName,
                        swappingData: this.props.location.state.swappingData,
                        basePath: this.props.location.state.basePath,
                    }
                })
            }
            else {
                const urlParams = new URLSearchParams(window.location.search);
                const orgName = urlParams.get('orgname');
                const status = urlParams.get('status');
                const routeToRedirect = '/configure-account-type';
                this.props.history.push({
                    pathname: `${this.props.location.state.basePath}${routeToRedirect}`,
                    search: `?orgname=${orgName}&status=${status}`,
                    state: {
                        orgName: this.props.location.state.swappingData.orgName,
                        swappingData: this.props.location.state.swappingData,
                        emailTelephone: this.props.location.state.swappingData,
                        basePath: this.props.location.state.basePath,
                        updateOrg: true
                    }
                })
            }
        }
    }
    handledoubleRightChange = () => {
        let { selectOptions, optionDetails } = this.state;
        let multiselectObject = CA_Utils.handledoubleRightChange(optionDetails, selectOptions, 'nooptiongroup');
        this.setState(multiselectObject);
    }

    handledoubleLeftChange = () => {
        let { selectOptions, optionDetails } = this.state;
        let multiselectObject = CA_Utils.handledoubleLeftChange(optionDetails, selectOptions, 'nooptiongroup');
        this.setState(multiselectObject);
    }


    handleLeftChange = (node) => {
        let multiselectObject = {};
        let { selectOptions, optionDetails } = this.state;
        multiselectObject = CA_Utils.handleLeftChange(optionDetails, selectOptions, 'nooptiongroup', node);
        this.setState(multiselectObject);
    }

    handleRightChange = (node) => {
        let multiselectObject = {};
        let { selectOptions, optionDetails } = this.state;
        multiselectObject = CA_Utils.handleRightChange(optionDetails, selectOptions, 'nooptiongroup', node);
        this.setState(multiselectObject);
    }

    
    returnToSearch = () => {
        this.props.history.push({
            pathname: `/org/searchOrg`,
            state: {
                redirectToSearchOrg: true
            }
        })
    }
    render() {
        const { leftMenuName, rightMenuName } = this.state;
        const addAdministrator = this.props.location.state.addAdministrator;
        return (<div className="main">
            {this.state.UpdateAdministrator ?
                <div>
                    <h2 className="title">{(addAdministrator) ? RA_STR.addAdminstratortitle : RA_STR.updateAdministratortitle }</h2>
                    <p className="desc">{(addAdministrator) ? RA_STR.addAdminstratortab : RA_STR.updateAdministratortab1}</p>
                    {(addAdministrator) ? <React.Fragment /> : <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.updateAdministratortab2 }}></p>}
                    <span className="ecc-h1">{RA_STR.updateAdministratorheading}</span>
                    <div className="">
                        <div className="row col-sm-12">
                            <SwappingSelectBox selectOptions={this.state.selectOptions}
                                optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={false} rightMenuName={rightMenuName} leftMenuName={leftMenuName} />
                        </div>
                    </div>
                    <div className="form-group form-submit-button row mt-2 ml-2">
                        {(addAdministrator) ?
                            <React.Fragment>
                                <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveConfigureAccountType}></input>
                            </React.Fragment> :
                            <React.Fragment>
                                <input className="custom-secondary-btn" id="custom-secondary-btn" type="button" value="RETURN TO SEARCH" onClick={this.returnToSearch}></input>
                                <input className="secondary-btn ml-3" id="custom-secondary-btn" type="submit" value="NEXT" onClick={this.moveConfigureAccountType}></input>
                            </React.Fragment>
                        }
                    </div>
                </div> : ''
            }
        </div>)
    }
}

export default UpdateAdministrator;