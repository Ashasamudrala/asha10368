import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { serverUrl , RA_API_URL  } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { Link } from 'react-router-dom';
import {RA_STR} from '../../shared/utlities/messages';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';

class EditForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            role_id : this.props.location.state.role_id,
            roleDetails : JSON.parse(JSON.stringify(this.props.location.state.role_id)),
            basePath: this.props.location.state.basePath ,
            advanceSearch : this.props.location.state.advanceSearch,
            customAttributes: [
                {
                    id: '1',
                    name: '',
                    value: ''
                },
            ],
        }
        this.handleChange=this.handleChange.bind(this);
    }


    handleAddEvent(evt) {
        var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
        var product = {
            id: id,
            name: '',
            value: ''
        }
        this.state.customAttributes.push(product);
        this.setState(this.state.customAttributes);
    }
    handleChange=(e)=>{
        let role_id=this.state.role_id;
        role_id[[e.target.name]]=e.target.value;
        this.setState({ role_id});
    }

    saveUserInfo=async(e)=>{
      const{dateCreated,dateModified,daysSinceReset,endLockTime,orgName,password,passwordHistoryCount,passwordResetTime,role,roleDisplayName,scope,startLockTime,status,strikeCount,userID, userName, userRefID} = this.props.location.state.role_id ;
      const {  basePath  , advanceSearch , roleDetails} = this.state;
      const routeToRedirect = '/basicUser/userInfo';
      let role_id = this.props.location.state.role_id;
      let searchData = this.props.location.state.searchData;
      const { firstName,lastName,middleName,emailAddress,telephoneNumber} = this.state ;
      let customAttributes = this.state.customAttributes ;
      customAttributes.forEach((eachData) => {
       delete  eachData['id'];
      });

      const saveUserInfo = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,
          data : {
            "list": [
              {
                "admin": false,
                "customAttributes": customAttributes,
                "customAttributesString": "string",
                "dateCreated": dateCreated,
                "dateModified": dateModified,
                "daysSinceReset": daysSinceReset,
                "emailAddress": emailAddress ?emailAddress: role_id.emailAddress,
                "endLockTime": endLockTime,
                "firstName": firstName ? firstName:role_id.firstName,
                "lastName": lastName ? lastName :role_id.lastName ,
                "middleName": middleName ?middleName:role_id.middleName,
                "orgName": orgName,
                "password": password,
                "passwordHistoryCount":passwordHistoryCount,
                "passwordResetTime":passwordResetTime,
                "role": role,
                "roleDisplayName": roleDisplayName,
                "scope": scope,
                "startLockTime": startLockTime,
                "status": status,
                "strikeCount": strikeCount,
                "telephoneNumber": telephoneNumber ?telephoneNumber:role_id.telephoneNumber ,
                "userID": userID,
                "userName": userName,
                "userRefID": userRefID
              }
            ]
          
      }
    }
      const updateUserInfo = await getService(saveUserInfo);
      if(updateUserInfo && updateUserInfo.status !== 200){
          this.props.history.push({
            pathname: `${basePath}${routeToRedirect}`,
            state: {
              error_data: updateUserInfo.data.errorList,
              editForm: true,
              role_id: roleDetails,
              basePath:basePath,
              searchData : searchData,
              advanceSearch : advanceSearch
            }
          });
       }else if(updateUserInfo.status === 200 && updateUserInfo.data.message){
        this.props.history.push({
            pathname: `${basePath}${routeToRedirect}`,
            state: {
              success_data: updateUserInfo.data,
              editForm: true,
              role_id: role_id,
              basePath:basePath,
              searchData : searchData,
              advanceSearch : advanceSearch
            }
          });  
       }

    }
    showAdminDetails=()=>{
        const {  basePath , advanceSearch } = this.state;
        const routeToRedirect = '/basicUser/userInfo/updateUserInformation';
        let searchData = this.props.location.state.searchData;
        this.props.history.push({
            pathname: `${basePath}${routeToRedirect}`,
            state: {
              role_id: this.props.location.state.role_id,
              searchData: searchData,
              basePath : basePath ,
              advanceSearch : advanceSearch
            }
          }
          );
    }
    handleSearch = () => {
        const {  basePath , advanceSearch} = this.state;
        const routeToRedirect = advanceSearch ? '/users/advancesearch' :'/search';
        let searchData = this.props.location.state.searchData;
        this.props.history.push({
          pathname: `${basePath}${routeToRedirect}`,
          state: {
              searchData: searchData ,
              advanceSearch : advanceSearch
          }
      });
    }
   
    handleProductTable(evt) {
        let products =[];
        let item = {
            id: evt.target.id,
            name: evt.target.name,
            value: evt.target.value
        };
         products = this.state.customAttributes.slice();
        let newProducts = products.map(function (product) {
            for (let key in product) {
                if (key === item.name && product.id === item.id) {
                    product[key] = item.value;
                }
                else if (key === item.value && product.id === item.id) {
                    product[key] = item.value;
                }
            }
            return product;
        });
        this.setState({ customAttributes: newProducts });
    }
    handleRowDel(product) {
        if (this.state.customAttributes.length === 1) {
            return
        }
        var index = this.state.customAttributes.indexOf(product);
        this.state.customAttributes.splice(index, 1);
        this.setState(this.state.customAttributes);
    }
    render() {
        const {dateCreated , dateModified , firstName,lastName,middleName,emailAddress,telephoneNumber} = this.state.role_id;
        const { role_id , basePath } = this.state ;
        const routeToRedirect = '/basicUser/userInfo';
        return (
            <div className="main">
                <div className="no-padd col-md-12">
                <div className="row">
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.Username}</label>
                            <span>{this.props.location.state.role_id.userID}</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.Organization}</label>
                            <span className="text-capitalize">{this.props.location.state.role_id.orgName}</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.Status}</label>
                            <span className="text-capitalize">{RA_STR.statusMessage}</span>
                        </div>
                    </div>
                </div>
                <ul className="nav nav-pills form-inline">
                    <li className="nav-item">
                    <Link  className='custom-secondary-btn' to={{ 
                      pathname: `${basePath}${routeToRedirect}`,
                       state: {
                       role_id: role_id,
                       basePath:basePath
                      }}} >{RA_STR.basicuserInformation}</Link>
                    </li>
                </ul>
            </div>

                <h2 className="title">{RA_STR.updateadmintitle}</h2>
                <p className="desc">{RA_STR.updateadmindesc}</p>
                <span className="ecc-h1 mt-2"> {RA_STR.updateDetailsInfo}</span>
                <div className="row">
                    <label className=" col-sm-2 control-label" >{RA_STR.Datecreated}</label>
                    <div className="col-sm-8 ">
                        <p className="form-control-static organization-label">{dateCreated}</p>
                    </div>
                </div>
                <div className="row">
                    <label className=" col-sm-2 control-label" >{RA_STR.LastModified}</label>
                    <div className="col-sm-8 ">
                        <p className="form-control-static organization-label">{dateModified}</p>
                    </div>
                </div>
                <div className="col-sm-8">
                    <SingleInput
                        title={'First Name'}
                        inputType={'text'}
                        name={'firstName'}
                        controlFunc={this.handleChange}
                        content={firstName}
                        required={true} />
                    <SingleInput
                        title={'Middle Name'}
                        inputType={'text'}
                        content={middleName}
                        controlFunc={this.handleChange}
                        name={'middleName'} />
                    <SingleInput
                        title={'Last Name'}
                        inputType={'text'}
                        name={'lastName'} 
                        content={lastName}
                        controlFunc={this.handleChange}
                        required={true} />
                </div>
                <div className="div-seperator">
                    <span className="ecc-h1">{RA_STR.EmailAddress}</span>
                    <div className="col-sm-8 create">
                        <SingleInput
                            title={'Email'}
                            required={true}
                            inputType={'text'}
                            name={'emailAddress'}
                            content={emailAddress}
                            controlFunc={this.handleChange} />
                    </div>
                </div>
                <div className="div-seperator">
                    <span className="ecc-h1">{RA_STR.TelephoneNumber}</span>
                    <div className="col-sm-8 create">
                        <SingleInput
                            title={'Phone Number'}
                            inputType={'text'}
                            required={true}
                            name={'telephoneNumber'}
                            content={telephoneNumber}
                            controlFunc={this.handleChange} />
                    </div>
                </div>
                <div className="col-sm-5">
                    <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.customAttributes} />
                </div>

                <div className="form-inline">
                    <input className="custom-secondary-btn" id="custom-secondary-btn" type="button" value="RETURN TO SEARCH" onClick={this.handleSearch}></input>
                    <div className="form-group form-submit-button p-2">
                        <input className="secondary-btn" id="Inactive" type="submit" value="UPDATE ADMINISTRATOR DETAILS" onClick={this.showAdminDetails} ></input>
                    </div>
                    <div className="form-group form-submit-button p-2">
                        <input className="secondary-btn" id="Deleted" type="submit" value="SAVE" onClick={this.saveUserInfo} ></input>
                    </div>
                </div>
            </div>);
    }

}

export default EditForm;