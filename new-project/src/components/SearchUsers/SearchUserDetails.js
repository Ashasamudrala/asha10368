import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_INACTIVE_TEMP, RA_STR_API_INACTIVE, RA_STR_API_DELETE, RA_STR_ACTIVEINFO } from '../../shared/utlities/constants';
import './SearchUsers.scss';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { RA_STR } from '../../shared/utlities/messages';
import DatePicker from '../../shared/components/DatePicker/DatePicker';

class SearchUserDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role_id: this.props.location.state !== undefined ? this.props.location.state.role_id : JSON.parse(localStorage.getItem('role_id')),
      searchData: this.props.location.state !== undefined ? this.props.location.state.searchData : JSON.parse(localStorage.getItem('searchData')),
      basePath: this.props.location.state !== undefined ? this.props.location.state.basePath : JSON.parse(localStorage.getItem('basePath')),
      advanceSearch: this.props.location.state !== undefined ? (this.props.location.state.advanceSearch || false) : (JSON.parse(localStorage.getItem('advanceSearch')) || false),
      fromDate: new Date(),
      toDate: new Date(),
      toDisplayDate: '',
      fromTime: '',
      fromSec: '',
      formDisplayDate: moment(new Date()).format('MM/DD/YYYY'),
      hours: ['HH', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
      minutes: ['MM', '00', '15', '30', '45']
    }
    this.handleFromDate = this.handleFromDate.bind(this);
    this.handleToDate = this.handleToDate.bind(this);
  }

  handleEdit = () => {
    const { role_id, searchData, basePath, advanceSearch } = this.state;
    const routeToRedirect = '/basicUser/userInfo/editUserInformation';
    this.props.history.push({
      pathname: `${basePath}${routeToRedirect}`,
      state: {
        role_id: role_id,
        searchData: searchData,
        basePath: basePath,
        advanceSearch: advanceSearch
      }
    }
    );
  }

  componentDidMount() {
    const { role_id, searchData, basePath, advanceSearch } = this.state;
    let editForm = (this.props.location.state) ? (this.props.location.state.editForm) : false;
    localStorage.setItem('role_id', JSON.stringify(role_id));
    localStorage.setItem('searchData', JSON.stringify(searchData));
    localStorage.setItem('advanceSearch', JSON.stringify(advanceSearch));
    localStorage.setItem('basePath', JSON.stringify(basePath));
    if (editForm) {
      if (this.props.location.state.success_data) {
        this.props.activateSuccessList(true, this.props.location.state.success_data);
        this.props.activateErrorList(false, '');
      }
      else {
        this.props.activateErrorList(true, this.props.location.state.error_data);
        this.props.activateSuccessList(false, '');
      }
    }

  }

  deactivateTemporarilySelectedAdmin = async () => {
    if (this.state.role_id.length === 0) {
      alert(RA_STR.admindeaactivateTempAlertMessage);
    }
    else {
      if (window.confirm(RA_STR.admindeactiviateConfirmMessage)) {
        let role_id = this.state.role_id;
        let startLockTime = this.state.formDisplayDate + " " + (this.state.fromTime || '00') + ":" + (this.state.fromSec || '00') + ":" + '00';
        let endLockTime = this.state.toDisplayDate ? (this.state.toDisplayDate || '00') + " " + (this.state.toTime || '00') + ":" + (this.state.toSec || '00') + ":" + '00' : '';
        role_id['startLockTime'] = startLockTime;
        role_id['endLockTime'] = endLockTime;
        const deactivateAdminTemporarily = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,
          data:
          {
            "list":
              [role_id]
            ,
            "updateType": RA_STR_INACTIVE_TEMP
          }
        };
        const deactivateAdminTemporarilyStatus = await getService(deactivateAdminTemporarily);
        if (deactivateAdminTemporarilyStatus && deactivateAdminTemporarilyStatus.status === 200) {
          this.handleClose();
          this.props.activateSuccessList(true, deactivateAdminTemporarilyStatus.data);
          this.props.activateErrorList(false, '');
        }
        else {
          this.handleClose();
          this.props.activateErrorList(true, deactivateAdminTemporarilyStatus.data.errorList);
          this.props.activateSuccessList(false, '');
        }
      } else {

      }
    }
  }

  deactivateSelectedAdmin = async () => {
    const { role_id } = this.state;
    if (this.state.role_id.length === 0) {
      alert(RA_STR.admindeactivateAlertMessage);
    }
    else {
      if (window.confirm(RA_STR.admindeactivateConfirmMessage)) {
        const deactivateAdmin = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,

          data:
          {
            "list":
              [role_id]
            ,
            "updateType": RA_STR_API_INACTIVE
          }
        };
        const deActivateAdminStatus = await getService(deactivateAdmin);
        if (deActivateAdminStatus && deActivateAdminStatus.status === 200) {
          let roleData = this.state.role_id;
          roleData.status = '2';
          this.setState({ role_id: roleData });
          this.props.activateSuccessList(true, deActivateAdminStatus.data);
          this.props.activateErrorList(false, '');
        }
        else {
          this.props.activateErrorList(true, deActivateAdminStatus.data.errorList);
          this.props.activateSuccessList(false, '');
        }
      }
      else {

      }
    }
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  handleClose = () => {
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
  }

  handleFromDate(d) {
    this.setState({ formDisplayDate: moment(d).format('MM/DD/YYYY') });
  }
  handleChange = () => {

  }

  getModal = () => {
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
  }

  handleSearch = () => {
    const { searchData, basePath, advanceSearch } = this.state;
    const routeToRedirect = advanceSearch ? '/users/advancesearch' : '/search';
    this.props.history.push({
      pathname: `${basePath}${routeToRedirect}`,
      state: {
        searchData: searchData
      }
    });
  }
  activateSelectedAdmin = async () => {
    if (this.state.role_id.length === 0) {
      alert(RA_STR.adminactivateAlertMessage);
    } else {
      const deactivateAdmin = {
        method: 'PUT',
        url: `${serverUrl}${RA_API_URL['searchDetails']}`,
        data:
        {
          "list":
            [this.state.role_id]
          ,
          "updateType": RA_STR_ACTIVEINFO
        }
      };
      const deActivateAdminStatus = await getService(deactivateAdmin);
      if (deActivateAdminStatus.status === 200) {
        let roleData = this.state.role_id;
        roleData.status = '1';
        this.setState(this.state.role_id);
        this.props.activateSuccessList(true, deActivateAdminStatus.data);
        this.props.activateErrorList(false, '');
      }
      else {
        this.props.activateErrorList(true, deActivateAdminStatus.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
  }
  handleToDate(d) {
    this.setState({ toDisplayDate: moment(d).format('MM/DD/YYYY') });
  }
  deleteSelectedAdmin = async () => {
    if (this.state.role_id.length === 0) {
      alert(RA_STR.admindeleteAlertMessage);
    } else {
      if (window.confirm(RA_STR.admindeleteConfirmMessage)) {
        const deleteAdmin = {
          method: 'DELETE',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,
          data:
          {
            "list":
              [this.state.role_id]
            ,
            "updateType": RA_STR_API_DELETE
          }
        };
        const deleteAdminStatus = await getService(deleteAdmin);
        if (deleteAdminStatus.status === 200) {
          this.props.activateSuccessList(true, deleteAdminStatus.data);
          this.props.activateErrorList(false, '');
        }
        else {
          this.props.activateErrorList(true, deleteAdminStatus.data.errorList);
          this.props.activateSuccessList(false, '');
        }
      }
    }
  }

  render() {
    const routeToRedirect = '/basicUser/userInfo';
    const { role_id, searchData, basePath, hours, minutes } = this.state;
    return <div className="main searchuser">
      <div className="no-padd col-md-12">
        <div className="row">
          <div className="col-sm-10">
            <div className="form-group row">
              <label className="col-sm-3 col-form-label ml-3">{RA_STR.Username}</label>
              <span>{role_id.userID}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-10">
            <div className="form-group row">
              <label className="col-sm-3 col-form-label ml-3">{RA_STR.Organization}</label>
              <span className="text-capitalize">{role_id.orgName}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-10">
            <div className="form-group row">
              <label className="col-sm-3 col-form-label ml-3">{RA_STR.Status}</label>
              <span className="text-capitalize">{(role_id.status == '1') ? RA_STR.statusActiveMessage : RA_STR.statusInactiveMessage
              }</span>
            </div>
          </div>
        </div>
        <ul className="nav nav-pills form-inline">
          <li className="nav-item">
            <Link className='custom-secondary-btn' to={{
              pathname: `${basePath}${routeToRedirect}`,
              state: {
                role_id: role_id,
                searchData: searchData,
                basePath: basePath
              }
            }} >{RA_STR.basicuserInformation}</Link>

          </li>
        </ul>
      </div>

      <span className="ecc-h1 mt-2"> {RA_STR.userInformation}</span>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.firstName}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.firstName}</p>
          </div>
        </div>
      </div>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.middleName}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.middleName}</p>
          </div>
        </div>
      </div>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.lastName}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.lastName}</p>
          </div>
        </div>
      </div>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.email}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.emailAddress}</p>
          </div>
        </div>
      </div>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.creationDate}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.dateCreated}</p>
          </div>
        </div>
      </div>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.lastModified}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.dateModified}</p>
          </div>
        </div>
      </div>
      <div className="div-seperator">
        <div className="row">
          <label className=" col-sm-2 control-label" >{RA_STR.phoneNumber}</label>
          <div className="col-sm-8 ">
            <p className="form-control-static organization-label">{role_id.telephoneNumber}</p>
          </div>
        </div>
      </div>
      <div className="form-inline">
        <div className="form-group form-submit-button">
          <input className="secondary-btn" id="Active" type="submit" value="EDIT" onClick={this.handleEdit} ></input>
        </div>
        <div className="form-group form-submit-button p-2">
          {(role_id.status == '1') ?
            <input className="secondary-btn" id="Inactive" type="submit" value="DEACTIVATE" onClick={this.deactivateSelectedAdmin} ></input>
            :
            <input className="secondary-btn" id="Active" type="submit" value="ACTIVATE" onClick={this.manipulateSearch} disabled={false} onClick={this.activateSelectedAdmin}></input>
          }
        </div>
        <div className="form-group form-submit-button p-2">
          <input className="secondary-btn" id="Deleted" type="submit" value="DEACTIVATE TEMPORARILY..." onClick={this.getModal} disabled={!(role_id.status == '1')} ></input>
          <div id="myModal" className="modal">
            <div className="modal-content">
              <div className="dialog-header" className="successheader">
                <div className="dialog-title"><span className="label">{RA_STR.model_title}</span></div>
                <div className="dialog-close" className="close" onClick={this.handleClose}> <span className="close">&times;</span></div>
              </div>
              <div className="modal-body">
                <p className="desc" >{RA_STR.modal_body}</p>
                <div className="col-sm-6 ">
                  <div className="dynamic-form-input form-group row">
                    <label className="col-sm-3 col-form-label"><b>{RA_STR.from_date}</b></label>
                    <div className='col-sm-5'>
                      <div className="input-group input-group-unstyled">
                        <input
                          className="form-input form-control"
                          autoComplete="off"
                          name='formDisplayDate'
                          type='text'
                          onChange={this.handleChange}
                          value={this.state.formDisplayDate}
                        />
                      </div>
                      <div>(MM/dd/YYYY)</div>
                    </div>
                    <div className="col-sm-2">
                      <DatePicker content={this.state.fromDate}
                        controlFunc={this.handleFromDate} />
                    </div>
                  </div>
                  <div className="form-group dynamic-form-select row mt-2">
                    <label className="col-sm-3 text-left col-form-label">{RA_STR.time}</label>
                    <div className="col-sm-3 ">
                      <select
                        name={'fromTime'}
                        onChange={this.handleChange}
                        className="form-select form-control ">
                        {hours.map(opt => {
                          return (
                            <option key={opt} value={opt}>{opt}</option>);
                        })}
                      </select>
                    </div>
                    <div className="col-sm-3 ">
                      <select
                        name={'fromSec'}
                        onChange={this.handleChange}
                        className="form-select form-control ">
                        {minutes.map(opt => {
                          return (
                            <option key={opt} value={opt}>{opt}</option>);
                        })}
                      </select>
                    </div>


                  </div>


                  <div className="dynamic-form-input form-group row mt-2">
                    <label className="col-sm-3 col-form-label"><b>{RA_STR.to_date}</b></label>
                    <div className='col-sm-5'>
                      <div className="input-group input-group-unstyled">
                        <input
                          className="form-input form-control"
                          autoComplete="off"
                          name='toDisplayDate'
                          type='text'
                          value={this.state.toDisplayDate}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div>(MM/dd/YYYY)</div>
                    </div>
                    <div className="col-sm-2">
                      <div className="col-sm-2">
                        <DatePicker content={this.state.toDate}
                          controlFunc={this.handleToDate} />
                      </div>
                    </div>
                  </div>
                  <div className="form-group dynamic-form-select row mt-2">
                    <label className="col-sm-3 text-left col-form-label">{RA_STR.time}</label>
                    <div className="col-sm-3 select">
                      <select
                        name={'toTime'}
                        onChange={this.handleChange}
                        className="form-select form-control ">
                        {hours.map(opt => {
                          return (
                            <option key={opt} value={opt}>{opt}</option>);
                        })}
                      </select>
                    </div>
                    <div className="col-sm-3">
                      <select
                        name={'toSec'}
                        onChange={this.handleChange}
                        className="form-select form-control ">
                        {minutes.map(opt => {
                          return (
                            <option key={opt} value={opt}>{opt}</option>);
                        })}
                      </select>
                    </div>

                  </div>

                </div>
                <div className="form-inline">
                  <div className="form-group form-submit-button mt-2">
                    <input className="custom-secondary-btn" id="RESET" type="button" value="CANCEL" onClick={this.handleClose} />
                  </div>
                  <div className="form-group form-submit-button ml-2 mt-2">
                    <input className="secondary-btn" id="Active" type="submit" value="SAVE" onClick={this.deactivateTemporarilySelectedAdmin} ></input>
                  </div>
                </div>
              </div>

            </div>

          </div>
        </div>
        <div className="form-group form-submit-button p-2" >
          <input className="secondary-btn" id="refreshcache" type="submit" value="RETURN TO SEARCH " onClick={this.handleSearch} ></input>
        </div>
      </div>

    </div>
  }

}

export default SearchUserDetails;