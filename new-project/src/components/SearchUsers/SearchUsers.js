                                                                                                                                       import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import AdvancedSearch from './AdvancedSearch';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, LOADING_TEXT , RA_STR_ACTIVE ,RA_STR_INACTIVE , RA_STR_INACTIVE_TEMP , RA_STR_API_INACTIVE ,RA_STR_API_DELETE} from '../../shared/utlities/constants';
import './SearchUsers.scss';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import Loader from '../../shared/utlities/loader';
import DatePicker from '../../shared/components/DatePicker/DatePicker';
import {RA_STR} from '../../shared/utlities/messages';


class SearchUsers extends Component {
  constructor(props) {
    super(props);
    this.handleFromDate = this.handleFromDate.bind(this);
    this.handleToDate = this.handleToDate.bind(this);
    this.state = {
      openLeftNav: true,
      advancedSearch: false,
      firstName: '',
      lastName: '',
      userName: '',
      organization: '',
      email: '',
      pageNo: 0,
      activePageNum: 1,
      pageSize: 10,
      currentPageNo: 1,
      totalCount: [],
      telephoneNumber: '',
      reload: true,
      showTable: false,
      currentPageNo: 1,
      totalCount: [],
      minutes : ['MM', '00', '15', '30', '45'],
      fromDate: new Date(),
      toDate: new Date(),
      dataLoading: false,
      fromSec:'', 
      textChanged : false,
      fromTime:'',
      toDisplayDate: '',
      modalColor : false ,
      formDisplayDate: moment(new Date()).format('MM/DD/YYYY'),
      apiData: [
      ],
      hours : ['HH', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
      showUserDetails: false,
      copySearchData : (this.props.location.state) ?  JSON.parse(JSON.stringify(this.props.location.state.searchData)) :'',
      columns: [
        { title: 'Username', field:RA_STR.userID, render: rowData => this.getSelectedUsername(rowData) },
        { title: 'Full Name', field:RA_STR.fullName , render : rowData => this.getFullName(rowData) },
        { title: 'Organisation', field:RA_STR.searchorgName},
        { title: 'Email', field:RA_STR.emailAddress , render: rowData => this.redirectToEmail(rowData) },
        { title: 'Phone Number', field:RA_STR.telephoneNumber },
        { title: 'Role', field:RA_STR.roleDisplayName},
        { title: 'User Status', field:RA_STR.statusInfo},
      ],
      selectedAdmins: []
    }
    this.data = this.data.bind(this);
  }

  data = (rowData) => {
    const basePath =  this.props.match.url.substring(0,this.props.match.url.lastIndexOf('/'));
    const routeToRedirect = '/basicUser/userInfo';
    const { searchData } = this.state;
    delete rowData.fullName;
    delete rowData.statusInfo;
    this.props.history.push({
      pathname: `${basePath}${routeToRedirect}`,
      state: {
        role_id: rowData,
        searchData: searchData,
        basePath:basePath ,
      }
    }
    );
  }
  getFullName = (rowData) => {
    return <div title = {rowData.fullName} onClick={() => this.data(rowData)}>{ (rowData.fullName.length > 25) ? rowData.fullName.substring(0, 25) + '....' : rowData.fullName}
    </div> 
  }
  getSelectedUsername = (rowData) => {
    return <div className="orgName" title = {rowData.userID} onClick={() => this.data(rowData)}>{ (rowData.userID.length > 25) ? rowData.userID.substring(0, 25) + '....' : rowData.userID}</div>
   }
  redirectToEmail = (rowData) => {
    return <a className="orgName" href="mailto:Got@ca.com">{rowData.emailAddress}</a>
  }
  activateAdvancedSearch = () => {
    const basePath =  this.props.match.url.substring(0,this.props.match.url.lastIndexOf('/'));
    const routeToRedirect = '/users/advancesearch';
    this.props.history.push({
      pathname: `${basePath}${routeToRedirect}`,
    }
    );
    this.setState({ advancedSearch: true });
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value ,  textChanged : true })
  }
  getActivePage = (getPageNo) => {
    let pageCount = getPageNo - 1;
    this.setState({
        pageNo : pageCount,
        currentPageNo : getPageNo,
        textChanged : false 

    },() => {
      this.getSearchAdminDetails();
    })

}
  componentDidMount() {
    if ( this.props.location.state !== undefined) {
      this.getSearchAdminDetails(true);
    }
  }
  getModal = () => {
    if (this.state.selectedAdmins.length === 0) {
      alert(RA_STR.admindeactivateTempAlertMessage);
    } else {
      this.setState({modalColor : true });
      let modal = document.getElementById("myModal");
      modal.style.display = "block";
    }
  }

  getSearchAdminDetails = async (value) => {
    let { firstName, lastName, userName, organization, email, telephoneNumber ,copySearchData , textChanged  } = this.state;
    let {pageNo ,currentPageNo} = this.state;  
    this.setState({ dataLoading: true });
    let searchData;
    if(textChanged) {
      pageNo = 0 
      currentPageNo = 1
    }
   if(this.props.location.state !== undefined )
   {
     currentPageNo = this.props.location.state.searchData !== undefined ?
     this.props.location.state.searchData.pageNo+1 : pageNo+1;
     if(this.props.location.state.searchData ){
      firstName = copySearchData.firstName ;
      lastName  = copySearchData.lastName ;
      userName  = copySearchData.userName ;
      organization = copySearchData.organization ;
      email = copySearchData.email ;
      pageNo = pageNo+1;
      telephoneNumber  = copySearchData.telephoneNumber ;
     }
     

   }
    searchData = value === true ? (this.props.location.state !== undefined ? this.props.location.state.searchData : {
      "creationDate": "",
      "email": email,
      "fetchDeletedUsers": false,
      "firstName": firstName,
      "includeAccounts": true,
      "lastModifiedDate": "",
      "lastName": lastName,
      "middleName": "",
      "organization": organization,
      "roles": [],
      "status": [],
      "telephoneNumber": telephoneNumber,
      "userName": userName,
      "pageNo":pageNo,
      "pageSize":10
    }) : {
      "creationDate": "",
        "email": email,
        "fetchDeletedUsers": false,
        "firstName": firstName,
        "includeAccounts": true,
        "lastModifiedDate": "",
        "lastName": lastName,
        "middleName": "",
        "organization": organization,
        "roles": [],
        "status": [],
        "telephoneNumber": telephoneNumber,
        "userName": userName,
        "pageNo":pageNo,
        "pageSize":10
      }
      if(this.props.location.state !== undefined )
      {
        delete this.props.location.state ;
   
      }
    const getsearchDetails = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['searchDetails']}`,
      data: searchData
    };
    const searchDetails = await getService(getsearchDetails);
    if ( searchDetails && searchDetails.status === 200) {

      searchDetails.data && searchDetails.data.list.map(function(eachvalue){
        eachvalue.fullName = eachvalue.firstName + eachvalue.middleName + eachvalue.lastName;
        if (eachvalue.status === '1') {
          eachvalue.statusInfo = RA_STR_ACTIVE ;
        } else {
          eachvalue.statusInfo = RA_STR_INACTIVE ;
        }
      })
      this.props.activateErrorList(false, '');
      this.setState({ apiData: searchDetails.data.list, showTable: true, dataLoading: false, searchData , totalCount: searchDetails.data.resultCount , currentPageNo :currentPageNo ,copySearchData,firstName, lastName, userName, organization, email, telephoneNumber ,pageNo});
    }
    else if(searchDetails && searchDetails.status !== 200) {
      this.props.activateSuccessList(false, '');
      this.props.activateErrorList(true,  searchDetails.data.errorList);
      this.setState({ apiData: [], showTable: false, dataLoading: false });
    }
  }
  getcheckboxselection = (getData) => {
    let adminsData = JSON.parse(JSON.stringify(getData));
    let tempSelectedAdmins = [];
    if (adminsData.length) {
      adminsData.map((eachData) => {
        delete eachData.fullName
        delete eachData.statusInfo
        tempSelectedAdmins.push(eachData);
      })
    } else {
      this.props.activateSuccessList(false, '');
      tempSelectedAdmins = [];
    }
    this.setState({ selectedAdmins: tempSelectedAdmins });
  }
  deactivateTemporarilySelectedAdmin = async () => {
    this.setState({modalColor : true });
    if (this.state.selectedAdmins.length === 0) {
      alert(RA_STR.admindeaactivateTempAlertMessage );
    }
    else {
      if (window.confirm(RA_STR.admindeactiviateConfirmMessage)) {
        let selectedAdmins = this.state.selectedAdmins;
        let startLockTime = this.state.formDisplayDate + " " + (this.state.fromTime || '00') + ":" + (this.state.fromSec || '00') + ":" + '00';
        let endLockTime = this.state.toDisplayDate ? (this.state.toDisplayDate || '00') + " " + (this.state.toTime || '00') + ":" + (this.state.toSec || '00') + ":" + '00' : '';
        selectedAdmins.forEach(function (value) {
          value['startLockTime'] = startLockTime;
          value['endLockTime'] = endLockTime;
        });
        const deactivateAdminTemporarily = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,
          data:
          {
            "list":
              selectedAdmins
            ,
            "updateType": RA_STR_INACTIVE_TEMP
          }
        };
        const deactivateAdminTemporarilyStatus = await getService(deactivateAdminTemporarily);
        if (deactivateAdminTemporarilyStatus.status === 200) {
          this.handleClose();
          this.getSearchAdminDetails();
          this.props.activateSuccessList(true, deactivateAdminTemporarilyStatus.data);
          this.props.activateErrorList(false, '');
        }
        else {
          this.handleClose();
          this.props.activateErrorList(true, deactivateAdminTemporarilyStatus.data.errorList);
          this.props.activateSuccessList(false, '');
        }
      } else {

      }

    }
  }

  deactivateSelectedAdmin = async (e) => {
    if (this.state.selectedAdmins.length === 0) {
      alert(RA_STR.admindeactivateAlertMessage);
    }
    else {
      if (window.confirm(RA_STR.admindeactivateConfirmMessage)) {
        const deactivateAdmin = {
          method: 'PUT',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,

          data:
          {
            "list":
              this.state.selectedAdmins
            ,

            "updateType": RA_STR_API_INACTIVE
          }
        };
        const deActivateAdminStatus = await getService(deactivateAdmin);
        if (deActivateAdminStatus.status === 200) {
          this.getSearchAdminDetails();
          this.props.activateSuccessList(true, deActivateAdminStatus.data);
          this.props.activateErrorList(false, '');
        }
        else {
          this.props.activateErrorList(true, deActivateAdminStatus.data.errorList);
          this.props.activateSuccessList(false, '');
        }
      }
      else {

      }
    }
  }
  deleteSelectedAdmin = async () => {
    if (this.state.selectedAdmins.length === 0) {
      alert(RA_STR.admindeleteAlertMessage);
    } else {
      if (window.confirm(RA_STR.admindeleteConfirmMessage)) {
        const deleteAdmin = {
          method: 'DELETE',
          url: `${serverUrl}${RA_API_URL['searchDetails']}`,
          data:
          {
            "list":
              this.state.selectedAdmins
            ,
            "updateType": RA_STR_API_DELETE
          }
        };
        const deleteAdminStatus = await getService(deleteAdmin);
        if (deleteAdminStatus.status === 200) {
          this.getSearchAdminDetails();
          this.props.activateSuccessList(true, deleteAdminStatus.data);
          this.props.activateErrorList(false, '');
        }
        else {
          this.props.activateErrorList(true, deleteAdminStatus.data.errorList);
          this.props.activateSuccessList(false, '');
        }
      }
    }
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  handleClose = () => {
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
    this.setState({modalColor : false });
  }

  handleFromDate(d) {
    this.setState({ formDisplayDate: moment(d).format('MM/DD/YYYY') });
  }

  handleToDate(d) {
    this.setState({ toDisplayDate: moment(d).format('MM/DD/YYYY') });
  }

  openDatepicker = () => this._calendar.setOpen(true);
  render() {
    const { totalCount ,minutes , hours , organization } = this.state;
    return (<div>
      {!this.state.advancedSearch ?
        <div className="main searchuser">
          {/* <div   className={`${this.state.modalColor ? 'dialog-mask':''}`} ></div> */}
          <table >
            <tbody>
              <tr>
                <td>
                  <span className="pageTitle">
                    <h2 className="title">{RA_STR.searchusertitle}</h2>
                  </span>
                </td>
              </tr>
              <tr>
                <td>
                  <p className="desc">{RA_STR.searchuserdescription}
              </p>
                  <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.searchuserdescriptiontab }}/>
                   
                </td>
                <td >
                  <a className="regular-link" onClick={this.activateAdvancedSearch}>Advanced Search</a></td></tr>
            </tbody>
          </table>
          <div className="col-sm-5 ">
            <SingleInput
              title={'First Name'}
              inputType={'text'}
              name={'firstName'}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Last Name'}
              inputType={'text'}
              name={'lastName'}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Username'}
              inputType={'text'}
              name={'userName'}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Organization'}
              inputType={'text'}
              name={'organization'}
              content = {organization}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Email'}
              inputType={'text'}
              name={'email'}
              controlFunc={this.handleChange} />
            <SingleInput
              title={'Phone Number'}
              inputType={'text'}
              name={'telephoneNumber'}
              controlFunc={this.handleChange} />
            <div className="form-group row">
              <input className="secondary-btn" id="searchButton" type="submit" value="Search" onClick={this.getSearchAdminDetails}></input>
            </div>
          </div>
          {this.state.dataLoading ? <React.Fragment><Loader show={this.state.dataLoading } />{LOADING_TEXT} </React.Fragment> : <Loader show={this.state.dataLoading } />}
          {this.state.showTable ? <div className="tabele-buttons">

                 
            <DynamicTable columns={this.state.columns} data={this.state.apiData} enablePagination={true}    totalCount={totalCount} pageSize={this.state.pageSize} activePageNumNew={this.state.currentPageNo} 
                    activePage={this.getActivePage} selection={true}  handleCheckboxSelection={this.getcheckboxselection} title={"Select Users to Modify"} />
            <div className="form-inline">
              <div className="form-group form-submit-button">
                <input className="secondary-btn" id="Active" type="submit" value="ACTIVATE" onClick={this.manipulateSearch} disabled={true}></input>
              </div>
              <div className="form-group form-submit-button p-2">
                <input className="secondary-btn" id="Inactive" type="submit" value="DEACTIVATE" onClick={this.deactivateSelectedAdmin} disabled={false}></input>
              </div>
              <div className="form-group form-submit-button p-2">
                <input className="secondary-btn" id='myBtn' type="submit" value="Deactivate Temporarily..." onClick={this.getModal}   ></input>
              </div>
              <div id="myModal" className="modal searchuser">
                <div className="modal-content">
                  <div className="dialog-header" className="successheader">
                    <div className="dialog-title"><span className="label">{RA_STR.model_title}</span></div>
                    <div className="dialog-close" className="close" onClick={this.handleClose}> <span className="close">&times;</span></div>
                  </div>
                  <div className="modal-body">
                    <p className="desc" >{RA_STR.modal_body}</p>
                    <div className="col-sm-6 ">
                      <div className="dynamic-form-input form-group row">
                        <label className="col-sm-3 col-form-label"><b> {RA_STR.from_date}</b></label>
                        <div className='col-sm-5'>
                          <div className="input-group input-group-unstyled">
                            <input
                              className="form-input form-control"
                              autoComplete="off"
                              name='fromDisplayDate'
                              type='text'
                              value={this.state.formDisplayDate}
                              onChange={this.handleChange}
                            />
                          </div>
                          <div>(MM/dd/YYYY)</div>
                        </div>
                        <div className="col-sm-2">
                             <DatePicker content={this.state.fromDate}
                            controlFunc={this.handleFromDate}/>
                        </div>
                      </div>
                      <div className="form-group dynamic-form-select row mt-2">
                        <label className="col-sm-3 text-left col-form-label">{RA_STR.time}</label>
                        <div className="col-sm-3">
                          <select
                            name={'fromTime'}
                            onChange={this.handleChange}
                            className="form-select form-control">
                            {hours.map(opt => {
                              return (
                                <option key={opt} value={opt}>{opt}</option>);
                            })}
                          </select>
                        </div>
                        <div className="col-sm-3 ">
                          <select
                            name={'fromSec'}
                            onChange={this.handleChange}
                            className="form-select form-control">
                            {minutes.map(opt => {
                              return (
                                <option key={opt} value={opt}>{opt}</option>);
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="dynamic-form-input form-group row mt-2">
                        <label className="col-sm-3 col-form-label"><b>{RA_STR.to_date}</b></label>
                        <div className='col-sm-5'>
                          <div className="input-group input-group-unstyled">
                            <input
                              className="form-input form-control"
                              autoComplete="off"
                              name='todisplayDate'
                              type='text'
                              value={this.state.toDisplayDate}
                              onChange={this.handleChange}
                            />
                          </div>
                          <div>(MM/dd/YYYY)</div>
                        </div>
                        <div className="col-sm-2">
                            <DatePicker content={this.state.toDate}
                            controlFunc={this.handleToDate}/>
                        </div>
                      </div>
                      <div className="form-group dynamic-form-select row mt-2">
                        <label className="col-sm-3 text-left col-form-label">{RA_STR.time}</label>
                        <div className="col-sm-3">
                          <select
                            name={'toTime'}
                            onChange={this.handleChange}
                            className="form-select form-control ">
                            {hours.map(opt => {
                              return (
                                <option key={opt} value={opt}>{opt}</option>);
                            })}
                          </select>
                        </div>
                        <div className="col-sm-3">
                          <select
                            name={'toSec'}
                            onChange={this.handleChange}
                            className="form-select form-control ">
                            {minutes.map(opt => {
                              return (
                                <option key={opt} value={opt}>{opt}</option>);
                            })}
                          </select>
                        </div>

                      </div>

                    </div>
                    <div className="form-inline">
                      <div className="form-group form-submit-button mt-2">
                      <input className="custom-secondary-btn" id="RESET" type="button" value="CANCEL" onClick={this.handleClose} />
                      </div>
                      <div className="form-group form-submit-button ml-2 mt-2">
                        <input className="secondary-btn" id="Active" type="submit" value="SAVE" onClick={this.deactivateTemporarilySelectedAdmin} ></input>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div className="form-group form-submit-button p-2" >
                <input className="secondary-btn" id="refreshcache" type="submit" value="DELETE" onClick={this.deleteSelectedAdmin} disabled={false}></input>
              </div>
            </div>
          </div>
            : ''}
        </div> :
        <AdvancedSearch />
      }
    </div>)
  }
}

export default SearchUsers;
