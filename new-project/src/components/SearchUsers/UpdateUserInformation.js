import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import DatePicker from "react-datepicker";
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import "react-datepicker/dist/react-datepicker.css";
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import SwappableGrid from '../../shared/components/SwapGrid/swappingComponent';
import ReactTooltip from 'react-tooltip';
import { serverUrl, RA_STR_CHECKBOX, RA_API_URL , LOADING_TEXT } from '../../shared/utlities/constants';
import './SearchUsers.scss';
import {RA_STR} from '../../shared/utlities/messages';
import { Link } from 'react-router-dom';
import * as _ from 'underscore'; 



const Input = React.forwardRef(({ onChange, value, id }, props) => {
    return (
        <React.Fragment>
            <div className="ml-5">From</div>
            <input
                value={value}
                id={id}
                className="form-control"
                onChange={onChange}
                disabled={true}
            />
            <div>(MM/dd/yyyy)</div>
        </React.Fragment>
    );
});
const DateInput = React.forwardRef(({ onChange, value, id }, props) => {
    return (
        <React.Fragment>
            <div className="ml-5">To</div>
            <input
                value={value}
                id={id}
                className="form-control"
                disabled={true}
                onChange={onChange}
            />
            <div>(MM/dd/yyyy)</div>
        </React.Fragment>
    );
});

class UpdateUserInformation extends Component {
    constructor(props) {
        super(props);
        this.handleFromDate = this.handleFromDate.bind(this);
        this.handleToDate = this.handleToDate.bind(this);
        this.state = {
            role_id: this.props.location.state.role_id,
            roleOptions: [],
            selectedRole: '',
            lock: false,
            fromDate: '',
            toDate: '',
            scopeAll: false,
            dataLoading:false,
            basePath: this.props.location.state.basePath ,
            advanceSearch : this.props.location.state.advanceSearch
        }
    }
    handleFromDate(d) {
        this.setState({ fromDate: d });
    }
    handleToDate(d) {
        this.setState({ toDate: d });
    }
    openDatepicker = () => this._calendar.setOpen(true);

    opentoDatepicker = () => this._tocalendar.setOpen(true);



    handleChange = (e) => {
        if (e.target.type === RA_STR_CHECKBOX) {
            this.setState({ [e.target.name]: e.target.checked }
            );
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    }

    componentDidMount = async () => {
        this.getRoleData();
        this.getOrganisationData();
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    _handleSave = async () => {
        let selectedItems = [];
        const { password, confirmPassword, lock, fromDate, toDate } = this.state;
        if (this.state.scopeAll === true) {
            selectedItems = Object.keys(this.state.leftItems);
        }
        else {
            selectedItems = Object.values(this.refs.swappableGridRef.getRightItems());

        }

        const { emailAddress, firstName, lastName, middleName, userID, roleDisplayName, status, scope, userName, strikeCount, daysSinceReset, passwordResetTime, dateCreated, dateModified, passwordHistoryCount, customAttributes, customAttributesString, telephoneNumber, userRefID, role, orgName } = this.state.role_id;
        const saveAdmin = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['updateAdminInfo']}`,
            data:
            {
                "lock": lock,
                "password": password ? password : '',
                "confirmPassword": confirmPassword ? confirmPassword : '',
                "startAdminLockTime": fromDate ? fromDate.toLocaleDateString() + ' ' + '00:00:00' : '',
                "endAdminLockTime": toDate ? toDate.toLocaleDateString() + ' ' + '00:00:00' : '',
                "admin": true,
                "firstName": firstName,
                "lastName": lastName,
                "emailAddress": emailAddress,
                "telephoneNumber": telephoneNumber,
                "userID": userID,
                "userRefID": userRefID,
                "orgName": orgName,
                "middleName": middleName,
                "dateCreated": dateCreated,
                "dateModified": dateModified,
                "role": role,
                "roleDisplayName": roleDisplayName,
                "startLockTime": null,
                "endLockTime": null,
                "status": status,
                "scope": this.state.scopeAll,
                "selectedOrgs": selectedItems,
                "userName": userName,
                "strikeCount": strikeCount,
                "daysSinceReset": daysSinceReset,
                "passwordResetTime": passwordResetTime,
                "passwordHistoryCount": passwordHistoryCount,
                "customAttributes": customAttributes,
                "customAttributesString": customAttributesString
            }


        }

        const updateUserInfo = await getService(saveAdmin);
        if (updateUserInfo.status !== 200) {
            this.props.activateErrorList(true, updateUserInfo.data.errorList);
            this.props.activateSuccessList(false, '');
        } else if (updateUserInfo.status === 200 && updateUserInfo.data.message) {
            this.props.activateSuccessList(true, updateUserInfo.data);
            this.props.activateErrorList(false, '');
        }

    }
    handleSearch = () => {
        const { advanceSearch } = this.state ;
        let searchData = this.props.location.state.searchData;
        const routeToRedirect = advanceSearch ? '/users/advancesearch' :'/search';
        this.props.history.push({
            pathname: `/users/manageUsers${routeToRedirect}`,
            state: {
                searchData: searchData,
                advanceSearch : this.props.location.state.advanceSearch
            }
        }
        );
    }
    getOrganisationData = async () => {
        let selectedOrganisations = {};
        let avaliableOrganisations ;
        this.setState({dataLoading : true});
        const getAvaliableOrg = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
        }
        const getAvaliableOrganization = await getService(getAvaliableOrg);

        const getOrg = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getSelectedOrgs']}?adminId=${this.props.location.state.role_id.userID}&orgName=${this.props.location.state.role_id.orgName}`
        }
        const getOrganization = await getService(getOrg);
        if (getOrganization) {
            getOrganization.data.forEach((orgdata) => {
                selectedOrganisations[orgdata] = orgdata;
            })
        }
        
        if (Object.keys(selectedOrganisations).length !== 0) {
            avaliableOrganisations   = Object.keys(getAvaliableOrganization.data.organizations).filter(function(objFromA) {
                return !Object.keys(selectedOrganisations).find(function(objFromB) {
                  return _.isEqual(objFromA,objFromB)
                })
              })
        }
        if ( Object.keys(selectedOrganisations).length !== 0) {
        let RightItemsObj = { rightItems: selectedOrganisations, rightStaticArray: selectedOrganisations };
        this.setState(RightItemsObj);
        }
        let leftItemsObj = { leftItems: avaliableOrganisations, leftStaticArray: avaliableOrganisations };
        this.setState(leftItemsObj);
    }

    handleBack = () => {
        let searchData = this.props.location.state.searchData;
        const {  role_id , basePath  , advanceSearch } = this.state;
        const routeToRedirect = '/basicUser/userInfo/editUserInformation';
        this.props.history.push({
            pathname: `${basePath}${routeToRedirect}`,
            state: {
                role_id: role_id,
                searchData: searchData,
                basePath:basePath ,
                advanceSearch : advanceSearch
            }
        }
        );
    }

    getRoleData = async () => {
        let selectedRole = this.props.location.state.role_id.role;
        const getRoles = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getRoles']}`
        }
        let roleOptions = [];
        const getPredefinedRoles = await getService(getRoles);
        let objectKeys = Object.keys(getPredefinedRoles.data.roles);
        let objectValues = Object.values(getPredefinedRoles.data.roles);
        for (let i = 0; i < objectKeys.length; i++) {
            roleOptions.push({
                'key': objectValues[i].roleName,
                'content': objectValues[i].displayName
            })
        }
        this.setState({ roleOptions: roleOptions, selectedRole: selectedRole });
    }

    render() {
        console.log(this.state);
        const { roleOptions, selectedRole, role_id , basePath  } = this.state;
        const routeToRedirect = '/basicUser/userInfo';
        return (<div className="main">
            <div className="no-padd col-md-12">
                <div className="row">
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.Username}</label>
                            <span>{this.props.location.state.role_id.userID}</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.Organization}</label>
                            <span className="text-capitalize">{this.props.location.state.role_id.orgName}</span>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.Status}</label>
                            <span className="text-capitalize">{RA_STR.statusMessage}</span>
                        </div>
                    </div>
                </div>
                <ul className="nav nav-pills form-inline">
                    <li className="nav-item">
                        <Link className='custom-secondary-btn' to={{
                            pathname: `${basePath}${routeToRedirect}`,
                            state: {
                                role_id: role_id,
                                basePath:basePath
                            }
                        }} >{RA_STR.basicuserInformation}</Link>
                    </li>
                </ul>
            </div>

            <h2 className="title">{RA_STR.updateadmintitle}</h2>
            <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.admintitle }}></p>
            <div className="div-seperator">
                <span className="ecc-h1">{RA_STR.updateadmintitle}</span>
                <div className="col-sm-8 create">
                    <Select
                        name={'selectedRole'}
                        title={'Role'}
                        selectedOption={selectedRole}
                        options={roleOptions ? roleOptions : ['']}
                        controlFunc={this.handleChange} />
                </div>

            </div>
            <div className="div-seperator">
                <span className="ecc-h1">{RA_STR.setPassword}</span>
                <div className="col-sm-8 create">
                    <span data-tip data-for='password-criteria'>
                        <SingleInput
                            title={'Password'}
                            required={true}
                            inputType={'password'}
                            name={'password'}
                            controlFunc={this.handleChange} />
                    </span>
                    <ReactTooltip id='password-criteria' place="right" effect="solid" type="dark" >
                        <ul className="password-criteria">
                            Password must be at least 6 characters long, and must include
              <li>At least 1 numeric characters</li>
                            <li>At least 4 alphabetic characters( 0 LowerCase, 0 UpperCase)</li>
                            <li>At least 1 special characters  ( Allowed special characters are    !@#$%^&amp;*()_+ )</li></ul>
                    </ReactTooltip>
                </div>
                <div className="col-sm-8 create">
                    <span data-tip data-for='password-criteria1'>
                        <SingleInput
                            title={'Confirm Password'}
                            required={true}
                            inputType={'password'}
                            name={'confirmPassword'}
                            controlFunc={this.handleChange} />
                    </span>
                    <ReactTooltip id='password-criteria1' place="right" effect="solid" type="dark" >
                        <ul className="password-criteria">
                            Password must be at least 6 characters long, and must include
              <li>At least 1 numeric characters</li>
                            <li>At least 4 alphabetic characters( 0 LowerCase, 0 UpperCase)</li>
                            <li>At least 1 special characters  ( Allowed special characters are    !@#$%^&amp;*()_+ )</li></ul>
                    </ReactTooltip>
                </div>
            </div>
            <div className="div-seperator">
                <span className="ecc-h1 ">{RA_STR.setPassword}</span>
                <div className="form-group admin-check row">
                    <label className="col-sm-3">{RA_STR.lock}</label>
                    <div className="col-sm-8">
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id="customCheck1" name="lock" onChange={this.handleChange} />
                            <label className="custom-control-label" htmlFor="customCheck1"></label>
                        </div>
                    </div>
                </div>
            </div>

            <div  className={!(this.state.lock) ? 'display-lock' : ''}>
                <div className="div-seperator">
                    <div className="form-group admin-check row">
                        <label className="col-sm-3"></label>
                        <div className="col-sm-8">
                            <DatePicker
                                selected={this.state.fromDate}
                                todayButton="today"
                                dateFormat='MM/dd/yyyy'
                                onChange={this.handleFromDate}
                                showMonthDropdown={true}
                                showYearDropdown={true}
                                scrollableYearDropdown={false}
                                dropdownMode="select"
                                customInput={<Input  />}
                                ref={(c) => this._calendar = c}
                            />
                            <img src='images/calendarButton.gif' onClick={this.openDatepicker} className="ml-2" />
                        </div>
                    </div>
                </div>
                <div className="div-seperator">
                    <div className="form-group admin-check row">
                        <label className="col-sm-3">{RA_STR.credentialLockPeriod}</label>
                        <div className="col-sm-8">
                            <DatePicker
                                selected={this.state.toDate}
                                todayButton="today"
                                dateFormat='MM/dd/yyyy'
                                onChange={this.handleToDate}
                                showMonthDropdown={true}
                                showYearDropdown={true}
                                scrollableYearDropdown={false}
                                dropdownMode="select"
                                customInput={<DateInput />}
                                ref={(calender) => this._tocalendar = calender}
                            />
                            <img src='images/calendarButton.gif' onClick={this.opentoDatepicker} className="ml-2" />
                        </div>
                    </div>
                </div>
            </div>
            <span className="ecc-h1">{RA_STR.Manages}</span>
            <div className="row">
                <Checkbox
                    type={'checkbox'}
                    setName="scopeAll"
                    value={this.state.scopeAll}
                    checked={this.state.scopeAll}
                    controlFunc={this.handleChange}
                    options={["All Organisations"]}
                />
                <div className="col-sm-1">{RA_STR.OR}</div>
                <SwappableGrid ref="swappableGridRef" scopeAll={this.state.scopeAll} rightItems = {this.state.rightItems} leftItems ={this.state.leftItems}></SwappableGrid>
            </div>
            <div className="form-group form-submit-button row mt-5 ml-2">
                <input className="secondary-btn" id="manageTokenButton" type="submit" value="Back" onClick={this.handleBack} />
                <input className="secondary-btn ml-3" id="manageTokenButton" type="submit" value="SAVE" onClick={this._handleSave}></input>
                <input className="secondary-btn ml-3" id="manageTokenButton" type="submit" value="RETURN TO SEARCH" onClick={this.handleSearch}></input>
            </div>
        </div >);
    }
}
export default UpdateUserInformation;
