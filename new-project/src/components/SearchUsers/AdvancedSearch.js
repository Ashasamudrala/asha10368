import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import { getService } from '../../shared/utlities/RestAPI';
import ReactDOM from 'react-dom';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { RA_STR } from '../../shared/utlities/messages';
import moment from 'moment';
import { serverUrl, RA_API_URL, RA_STR_ACTIVE, RA_STR_INACTIVE, RA_STR_INACTIVE_TEMP, RA_STR_API_INACTIVE, RA_STR_API_DELETE , RA_STR_ACTIVEINFO } from '../../shared/utlities/constants';
import "react-datepicker/dist/react-datepicker.css";
import './SearchUsers.scss';
import _ from 'underscore';
import DatePicker from '../../shared/components/DatePicker/DatePicker';

class AdvancedSearch extends Component {
    constructor(props) {
        super(props);
        this.handleFromDate = this.handleFromDate.bind(this);
        this.handleToDate = this.handleToDate.bind(this);
        this.data = this.data.bind(this);
        this.state = {
            currentUsers: ["Current Users"],
            deletedUsers: [],
            disableCurrentUsers: false,
            searchParams: {Active: true},
            roleOptions: [],
            apiData: [],
            pageNo: 0,
            activePageNum: 1,
            pageSize: 10,
            currentPageNo: 1,
            totalCount: [],
            minutes: ['MM', '00', '15', '30', '45'],
            fromDate: new Date(),
            toDate: new Date(),
            fromSec: '',
            fromTime: '',
            toDisplayDate: '',
            statusInfo :[] ,
            formDisplayDate: moment(new Date()).format('MM/DD/YYYY'),
            apiData: [
            ],
            roles:[],
            activeOption : ['Active'] ,
            selectedAdmins : [] ,
            hours: ['HH', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
            columns: [
                { title: 'Username', field: RA_STR.userID, render: rowData => this.getSelectedUsername(rowData) },
                { title: 'Full Name', field: RA_STR.fullName },
                { title: 'Organisation', field: RA_STR.searchorgName },
                { title: 'Email', field: RA_STR.emailAddress, render: rowData => this.redirectToEmail(rowData) },
                { title: 'Phone Number', field: RA_STR.telephoneNumber },
                { title: 'Role', field: RA_STR.roleDisplayName },
                { title: 'User Status', field: RA_STR.statusInfo },
            ],
        };
    }
    getSelectedUsername = (rowData) => {
        return <div className="orgName" onClick={() => this.data(rowData)}>{rowData.userID}</div>
    }
    redirectToEmail = (rowData) => {
        return <a className="orgName" href="mailto:Got@ca.com">{rowData.userID}</a>
    }
    data = (rowData) => {
        const basePath = '/users/manageUsers';
        const routeToRedirect = '/basicUser/userInfo';
        const { searchData } = this.state;
        delete rowData.fullName;
        delete rowData.statusInfo;
        this.props.history.push({
            pathname: `${basePath}${routeToRedirect}`,
            state: {
                role_id: rowData,
                searchData: searchData,
                basePath: basePath,
                advanceSearch:true
            }
        }
        );
    }
    saveSearchData = async (e) => {
        let roles = this.getSelectedRoleData();
        let { fetchDeletedUsers, pageNo  , showTable , statusInfo } = this.state;
        let status = [];
        let statusInformation = [];
        status = this.getStatus();
        let searchData = {
            ...this.state.searchParams,
            "fetchDeletedUsers": fetchDeletedUsers,
            "includeAccounts": true,
            "roles": roles,
            "status": fetchDeletedUsers ? null : showTable ? _.uniq(statusInfo) : status,
            "pageNo": pageNo,
            "pageSize": 10
        }
        if ( this.props.location.state !== undefined) {
            searchData = this.props.location.state.searchData
            if(pageNo !== 0 ) {
                searchData['pageNo'] = pageNo
            }
        }
        const getsearchDetails = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['searchDetails']}`,
            data: searchData
        };
        const searchDetails = await getService(getsearchDetails);
        if (searchDetails && searchDetails.status === 200) {

            searchDetails.data && searchDetails.data.list.map(function (eachvalue) {
                eachvalue.fullName = eachvalue.firstName + eachvalue.middleName + eachvalue.lastName;
                if (eachvalue.status === '1') {
                    eachvalue.statusInfo = RA_STR_ACTIVE;
                    statusInformation.push('1');
                } else {
                    eachvalue.statusInfo = RA_STR_INACTIVE;
                    statusInformation.push('2');
                }
            })
            this.props.activateErrorList(false, '');
            this.setState({ apiData: searchDetails.data.list, showTable: true, totalCount: searchDetails.data.resultCount, searchData , statusInfo : statusInformation });
        }
        else if (searchDetails && searchDetails.status !== 200) {
            this.props.activateSuccessList(false, '');
            this.props.activateErrorList(true, searchDetails.data.errorList);
            this.setState({ apiData: [], showTable: false });
        }
    }


    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    getActivePage = (getPageNo) => {
        let pageCount = getPageNo - 1;
        this.setState({
            pageNo: pageCount,
            currentPageNo: getPageNo,

        }, () => {
            this.saveSearchData();
        })
    }

    getStatus = () => {
        let searchParams = this.state.searchParams;
        let status = [];
        if (searchParams['Active'] !== undefined && searchParams['Active']) {
            status.push('1');
        }
        if (searchParams['Inactive'] !== undefined && searchParams['Inactive']) {
            status.push('2');
        }
        if (searchParams['Initial'] !== undefined && searchParams['Initial']) {
            status.push('0');
        }
        return status;
    }

    getSelectedRoleData = () => {
        let select = ReactDOM.findDOMNode(this.refs['getRole']);
        if (select) {
            let values = [].filter.call(select.options, function (o) {
                return o.selected;
            }).map(function (o) {
                return o.value;
            });
            this.setState({roles:values})
            return values;
        }
        return this.state.roles;
    }

    componentDidMount() {
        this.getRoleData();
        if ( this.props.location.state !== undefined) {
            this.saveSearchData();
            this.setState({ currentPageNo : this.props.location.state.searchData.pageNo+1 })
        }
    }
    getRoleData = async () => {
        const getRoles = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getRoles']}`
        }
        const getPredefinedRoles = await getService(getRoles); 
        if(getPredefinedRoles && getPredefinedRoles.status === 200) {
            this.setState({ roleOptions: getPredefinedRoles.data.roles });
        }
    }

    getcheckboxselection = (getData) => {
        let adminsData = JSON.parse(JSON.stringify(getData));
        let tempSelectedAdmins = [];
        if (adminsData.length) {
            adminsData.map((eachData) => {
                delete eachData.fullName
                delete eachData.statusInfo
                tempSelectedAdmins.push(eachData);
            })
        } else {
            this.props.activateSuccessList(false, '');
            tempSelectedAdmins = [];
        }
        this.setState({ selectedAdmins: tempSelectedAdmins });
    }

    handleChange = (e) => {
        let deletedUsers = [];
        let currentUsers = [];
        let searchParams = this.state.searchParams;
        let feildValue;
        let activeOption = this.state.activeOption ;
        let disableCurrentUsers;
        let fetchDeletedUsers;
        if (e.target.type === 'radio') {
            if (e.target.value === 'Deleted Users') {
                deletedUsers.push('Deleted Users');
                disableCurrentUsers = true;
                fetchDeletedUsers = true;
            }
            else {
                currentUsers.push('Current Users');
                disableCurrentUsers = false;
                fetchDeletedUsers = false;
            }
            this.setState({ deletedUsers: deletedUsers, currentUsers: currentUsers, disableCurrentUsers: disableCurrentUsers, fetchDeletedUsers: fetchDeletedUsers });
        }
        else if (e.target.type === 'checkbox') {
            feildValue = e.target.value;
            searchParams[e.target.name] = e.target.checked;
            if( e.target.name === 'Active' ) {
                if(e.target.checked) {
                    activeOption.push('Active');
                } 
                else {
                    activeOption.pop();
                }
            }
            this.setState({ searchParams: searchParams ,activeOption :activeOption  });
        }
        else {
            feildValue = e.target.value;
            searchParams[e.target.name] = feildValue;
            this.setState({ [e.target.name]: e.target.value, disableCurrentUsers: false, searchParams: searchParams })
        }
    }
    deactivateTemporarilySelectedAdmin = async () => {
        if (this.state.selectedAdmins.length === 0) {
            alert(RA_STR.admindeaactivateTempAlertMessage);
        }
        else {
            if (window.confirm(RA_STR.admindeactiviateConfirmMessage)) {
                let selectedAdmins = this.state.selectedAdmins;
                let startLockTime = this.state.formDisplayDate + " " + (this.state.fromTime || '00') + ":" + (this.state.fromSec || '00') + ":" + '00';
                let endLockTime = this.state.toDisplayDate ? (this.state.toDisplayDate || '00') + " " + (this.state.toTime || '00') + ":" + (this.state.toSec || '00') + ":" + '00' : '';
                selectedAdmins.forEach(function (value) {
                    value['startLockTime'] = startLockTime;
                    value['endLockTime'] = endLockTime;
                });
                const deactivateAdminTemporarily = {
                    method: 'PUT',
                    url: `${serverUrl}${RA_API_URL['searchDetails']}`,
                    data:
                    {
                        "list":
                            selectedAdmins
                        ,
                        "updateType": RA_STR_INACTIVE_TEMP
                    }
                };
                const deactivateAdminTemporarilyStatus = await getService(deactivateAdminTemporarily);
                if (deactivateAdminTemporarilyStatus.status === 200) {
                    this.handleClose();
                    this.saveSearchData()
                    this.props.activateSuccessList(true, deactivateAdminTemporarilyStatus.data);
                    this.props.activateErrorList(false, '');
                }
                else {
                    this.handleClose();
                    this.props.activateErrorList(true, deactivateAdminTemporarilyStatus.data.errorList);
                    this.props.activateSuccessList(false, '');
                }
            } else {

            }

        }
    }

    deactivateSelectedAdmin = async (e) => {
        if (this.state.selectedAdmins.length === 0) {
            alert(RA_STR.admindeactivateAlertMessage);
        }
        else {
            if (window.confirm(RA_STR.admindeactivateConfirmMessage)) {
                const deactivateAdmin = {
                    method: 'PUT',
                    url: `${serverUrl}${RA_API_URL['searchDetails']}`,
                    data:
                    {
                        "list":
                            this.state.selectedAdmins
                        ,
                        "updateType": RA_STR_API_INACTIVE
                    }
                };
                const deActivateAdminStatus = await getService(deactivateAdmin);
                if (deActivateAdminStatus.status === 200) {
                    this.saveSearchData();
                    this.props.activateSuccessList(true, deActivateAdminStatus.data);
                    this.props.activateErrorList(false, '');
                }
                else {
                    this.props.activateErrorList(true, deActivateAdminStatus.data.errorList);
                    this.props.activateSuccessList(false, '');
                }
            }
            else {

            }
        }
    }
    deleteSelectedAdmin = async () => {
        if (this.state.selectedAdmins.length === 0) {
            alert(RA_STR.admindeleteAlertMessage);
        } else {
            if (window.confirm(RA_STR.admindeleteConfirmMessage)) {
                const deleteAdmin = {
                    method: 'DELETE',
                    url: `${serverUrl}${RA_API_URL['searchDetails']}`,
                    data:
                    {
                        "list":
                            this.state.selectedAdmins
                        ,

                        "updateType": RA_STR_API_DELETE
                    }
                };
                const deleteAdminStatus = await getService(deleteAdmin);
                if (deleteAdminStatus.status === 200) {
                    this.props.activateSuccessList(true, deleteAdminStatus.data);
                    this.props.activateErrorList(false, '');
                }
                else {
                    this.props.activateErrorList(true, deleteAdminStatus.data.errorList);
                    this.props.activateSuccessList(false, '');
                }
            }
        }
    }

    activateSelectedAdmin = async () => {
        if (this.state.selectedAdmins.length === 0) {
            alert(RA_STR.adminactivateAlertMessage);
        } else {
            const deactivateAdmin = {
                method: 'PUT',
                url: `${serverUrl}${RA_API_URL['searchDetails']}`,
                data:
                {
                    "list":
                        this.state.selectedAdmins
                    ,
                    "updateType": RA_STR_ACTIVEINFO
                }
            };
            const deActivateAdminStatus = await getService(deactivateAdmin);
            if (deActivateAdminStatus.status === 200) {
                this.saveSearchData();
                this.props.activateSuccessList(true, deActivateAdminStatus.data);
                this.props.activateErrorList(false, '');
            }
            else {
                this.props.activateErrorList(true, deActivateAdminStatus.data.errorList);
                this.props.activateSuccessList(false, '');
            }
        }
    }

    handleClose = () => {
        var modal = document.getElementById("myModal");
        modal.style.display = "none";
    }

    handleFromDate(d) {
        this.setState({ formDisplayDate: moment(d).format('MM/DD/YYYY') });
    }

    handleToDate(d) {
        this.setState({ toDisplayDate: moment(d).format('MM/DD/YYYY') });
    }

    getModal = () => {
        if (this.state.selectedAdmins.length === 0) {
            alert(RA_STR.admindeactivateTempAlertMessage);
        } else {
            var modal = document.getElementById("myModal");
            modal.style.display = "block";
        }
    }
    openDatepicker = () => this._calendar.setOpen(true);

    render() {
        const { currentUsers, deletedUsers, disableCurrentUsers, totalCount , minutes , hours , activeOption , statusInfo , pageSize }= this.state;
        return <div className="advanced-search main searchuser">
            <span className="pageTitle">
                <h2 className="title">{RA_STR.advancesearchtitle}</h2>
            </span>
            <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.advancesearchdescriptionsub }} />

            {this.state.showTable ? <div className="tabele-buttons">
                <DynamicTable columns={this.state.columns} data={this.state.apiData} enablePagination={true} totalCount={totalCount} pageSize={this.state.pageSize} activePageNumNew={this.state.currentPageNo}
                    activePage={this.getActivePage} selection={true} handleCheckboxSelection={this.getcheckboxselection} title={"Select Users to Modify"} />
                <div className="form-inline">
                    <div className="form-group form-submit-button">
                        <input className="secondary-btn" id="Active" type="submit" value="ACTIVATE" onClick={this.manipulateSearch} disabled={!(statusInfo.includes("2"))} onClick={this.activateSelectedAdmin}></input>
                    </div>
                    <div className="form-group form-submit-button p-2">
                        <input className="secondary-btn" id="Inactive" type="submit" value="DEACTIVATE" onClick={this.deactivateSelectedAdmin} disabled={!(statusInfo.includes("1"))}></input>
                    </div>
                    <div className="form-group form-submit-button p-2">
                        <input className="secondary-btn" id='myBtn' type="submit" value="Deactivate Temporarily..." onClick={this.getModal}   disabled={!(statusInfo.includes("1"))} ></input>
                    </div>
                    <div id="myModal" className="modal">
                        <div className="modal-content">
                            <div className="dialog-header" className="successheader">
                                <div className="dialog-title"><span className="label">{RA_STR.model_title}</span></div>
                                <div className="dialog-close" className="close" onClick={this.handleClose}> <span className="close">&times;</span></div>
                            </div>
                            <div className="modal-body">
                                <p className="desc" >{RA_STR.modal_body}</p>
                                <div className="col-sm-6 ">
                                    <div className="dynamic-form-input form-group row">
                                        <label className="col-sm-3 col-form-label"><b> {RA_STR.from_date}</b></label>
                                        <div className='col-sm-5'>
                                            <div className="input-group input-group-unstyled">
                                                <input
                                                    className="form-input form-control"
                                                    autoComplete="off"
                                                    name='fromDisplayDate'
                                                    type='text'
                                                    value={this.state.formDisplayDate}
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                            <div>(MM/dd/YYYY)</div>
                                        </div>
                                        <div className="col-sm-2">
                                            <DatePicker content={this.state.fromDate}
                                                controlFunc={this.handleFromDate} />
                                        </div>
                                    </div>
                                    <div className="form-group dynamic-form-select row mt-2">
                                        <label className="col-sm-3 text-left col-form-label">{RA_STR.time}</label>
                                        <div className="col-sm-3">
                                            <select
                                                name={'fromTime'}
                                                onChange={this.handleChange}
                                                className="form-select form-control">
                                                {hours.map(opt => {
                                                    return (
                                                        <option key={opt} value={opt}>{opt}</option>);
                                                })}
                                            </select>
                                        </div>
                                        <div className="col-sm-3 ">
                                            <select
                                                name={'fromSec'}
                                                onChange={this.handleChange}
                                                className="form-select form-control">
                                                {minutes.map(opt => {
                                                    return (
                                                        <option key={opt} value={opt}>{opt}</option>);
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="dynamic-form-input form-group row mt-2">
                                        <label className="col-sm-3 col-form-label"><b>{RA_STR.to_date}</b></label>
                                        <div className='col-sm-5'>
                                            <div className="input-group input-group-unstyled">
                                                <input
                                                    className="form-input form-control"
                                                    autoComplete="off"
                                                    name='todisplayDate'
                                                    type='text'
                                                    value={this.state.toDisplayDate}
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                            <div>(MM/dd/YYYY)</div>
                                        </div>
                                        <div className="col-sm-2">
                                            <DatePicker content={this.state.toDate}
                                                controlFunc={this.handleToDate} />
                                        </div>
                                    </div>
                                    <div className="form-group dynamic-form-select row mt-2">
                                        <label className="col-sm-3 text-left col-form-label">{RA_STR.time}</label>
                                        <div className="col-sm-3">
                                            <select
                                                name={'toTime'}
                                                onChange={this.handleChange}
                                                className="form-select form-control ">
                                                {hours.map(opt => {
                                                    return (
                                                        <option key={opt} value={opt}>{opt}</option>);
                                                })}
                                            </select>
                                        </div>
                                        <div className="col-sm-3">
                                            <select
                                                name={'toSec'}
                                                onChange={this.handleChange}
                                                className="form-select form-control ">
                                                {minutes.map(opt => {
                                                    return (
                                                        <option key={opt} value={opt}>{opt}</option>);
                                                })}
                                            </select>
                                        </div>

                                    </div>

                                </div>
                                <div className="form-inline">
                                    <div className="form-group form-submit-button mt-2">
                                        <input className="custom-secondary-btn" id="RESET" type="button" value="CANCEL" onClick={this.handleClose} />
                                    </div>
                                    <div className="form-group form-submit-button ml-2 mt-2">
                                        <input className="secondary-btn" id="Active" type="submit" value="SAVE" onClick={this.deactivateTemporarilySelectedAdmin} ></input>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="form-group form-submit-button p-2" >
                        <input className="secondary-btn" id="refreshcache" type="submit" value="DELETE" onClick={this.deleteSelectedAdmin} disabled={false}></input>
                    </div>
                </div>
            </div>
                :
                <React.Fragment>
                    <div className="col-sm-6">
                        <span className="ecc-h1 row">{RA_STR.advancesearchstatuslabel}</span>
                        <SingleInput
                            title={'userName'}
                            inputType={'text'}
                            name={'userName'}
                            controlFunc={this.handleChange} />
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">{RA_STR.advancesearchlabel}</label>
                            <div className="col-sm-8">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                    <label className="custom-control-label" htmlFor="customCheck1"></label>
                                </div>
                            </div>
                        </div>
                        <SingleInput
                            title={'First Name'}
                            inputType={'text'}
                            name={'firstName'}
                            controlFunc={this.handleChange} />
                        <SingleInput
                            title={'Last Name'}
                            inputType={'text'}
                            name={'lastName'}
                            controlFunc={this.handleChange} />
                        <SingleInput
                            title={'Organization'}
                            inputType={'text'}
                            name={'organization'}
                            controlFunc={this.handleChange} />
                        <SingleInput
                            title={'Email'}
                            inputType={'text'}
                            name={'email'}
                            controlFunc={this.handleChange} />
                        <SingleInput
                            title={'Phone Number'}
                            inputType={'text'}
                            name={'telephoneNumber'}
                            controlFunc={this.handleChange} />
                        <div className="div-seperator">
                            <span className="ecc-h1 row">{RA_STR.advancesearchstatuslabel}</span>
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">
                                    <RadioGroup
                                        title={'Current Users'}
                                        setName={'users'}
                                        type={'radio'}
                                        options={['Current Users']}
                                        selectedOptions={currentUsers}
                                        controlFunc={this.handleChange} />
                                </label>
                                <div className="col-sm-8 form-inline">
                                    <Checkbox
                                        title={'Active'}
                                        setName={'Active'}
                                        type={'checkbox'}
                                        options={['Active']}
                                        selectedOptions={activeOption}
                                        disable={disableCurrentUsers}
                                        controlFunc={this.handleChange} />
                                    <Checkbox
                                        title={'Inactive'}
                                        setName={'Inactive'}
                                        type={'checkbox'}
                                        options={['Inactive']}
                                        disable={disableCurrentUsers}
                                        controlFunc={this.handleChange} />
                                    <Checkbox
                                        title={'Initial'}
                                        setName={'Initial'}
                                        type={'checkbox'}
                                        options={['Initial']}
                                        disable={disableCurrentUsers}
                                        controlFunc={this.handleChange} />
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">
                                    <RadioGroup
                                        title={'Deleted Users'}
                                        setName={'delete-users'}
                                        type={'radio'}
                                        options={['Deleted Users']}
                                        selectedOptions={deletedUsers}
                                        controlFunc={this.handleChange} />
                                </label>
                            </div>
                            <div className="div-seperator ">
                                <span className="ecc-h1 row">{RA_STR.advancesearchrowlabel}</span>
                                <div className="form-group row role">
                                    <label className="col-sm-4 col-form-label">{RA_STR.advancesearchrolelabel}</label>
                                    <div className="col-sm-6">
                                        <select className="custom-select" multiple ref="getRole">{
                                            this.state.roleOptions.map((value, index) => (
                                                <option value={value.roleName} key={index} id={''}>{value.displayName}</option>
                                            ))
                                        }
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <input className="secondary-btn" id="searchButton" type="submit" value="Search" onClick={this.saveSearchData}></input>
                        </div>
                    </div>
                </React.Fragment>
            }
        </div>
    }
}
export default AdvancedSearch;