import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import MaterialTable from "material-table";
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import {RA_STR} from '../../shared/utlities/messages';

class CacheStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusMap: [],
      requestIds: [],
      status: '',
      requestId: '',
      showTable: false,
      cacheColumns: [
        { title: 'Request ID', field: RA_STR.cacherequestId },
        { title: 'Administrator Name', field: RA_STR.cacheadminName },
        { title: 'Organization', field: RA_STR.cacheorgName},
        { title: 'Timestamp', field: RA_STR.cachetimeStamp },
        { title: 'Event Type', field: RA_STR.cacheeventType }
      ],
      serverColumns: [
        { title: 'Resource', field:  RA_STR.cacheresourceName},
        { title: 'Server Instance ID', field: RA_STR.cacheinstanceId},
        { title: 'Server Instance Name', field: RA_STR.cacheinstanceName},
        { title: 'Host Name', field: RA_STR.cachehostName },
        { title: 'Status', field: RA_STR.cachestatus}
      ],
      apiData: [
      ],
      cacheDeatils: []

    };
  }
  componentDidMount = async () => {
    const getRequestIds = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getLastRequestIds']}`
    };
    const getCacheStatus = await getService(getRequestIds);
    if (getCacheStatus && getCacheStatus.status === 200) {
      this.setState({ requestIds: getCacheStatus.data.requestIds, requestId: getCacheStatus.data.requestIds[0], statusMap: getCacheStatus.data.statusMap, status: (Object.keys(getCacheStatus.data.statusMap))[0] });
    }
  }
  getCacheStatus = async () => {
    const getRequestIds = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchCacheDetails']}${this.state.requestId}?status=${this.state.status}`
    };
    const getCacheStatus = await getService(getRequestIds);
    if (getCacheStatus && getCacheStatus.status === 200) {
      let cacheData = [];
      cacheData.push(getCacheStatus.data.cacheRefreshDetails);
      this.props.activateErrorList(false, '');
      this.setState({ showTable: true, apiData: getCacheStatus.data.serverInstances, cacheDeatils: cacheData });
    }
    else {
      this.props.activateErrorList(true, getCacheStatus.data.errorList);
      this.setState({ showTable: false });
    }
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
  }
  render() {
    return <div className="main"><h2 className="title">{RA_STR.cachestatustitle}</h2>
      <p className="desc">{RA_STR.cachestatusdesc}</p>
      <div className="col-sm-6 div-seperator">
        <div className="form-group dynamic-form-select">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.requestidlabel}</label>
            <div className="col-sm-8">
              <select
                name='requestId'
                value={this.state.requestId}
                onChange={this.handleChange}
                className="form-select form-control">
                {this.state.requestIds.map((value, index) =>
                  <option value={value} key={index}>{value}</option>
                )}
              </select>
            </div>
          </div>
        </div>

        <div className="form-group dynamic-form-select">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">{RA_STR.cachestatuslabel}</label>
            <div className="col-sm-8">
              <select
                name='status'
                value={this.state.status}
                onChange={this.handleChange}
                className="form-select form-control">
                {Object.keys(this.state.statusMap).map(key =>
                  <option value={key} key={key}>{this.state.statusMap[key]}</option>
                )}
              </select>
            </div>
          </div>
        </div>
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" onClick={this.getCacheStatus} value="Search"></input>
      </div>
      {
        this.state.showTable ?
          <div style={{ maxWidth: '100%', clear: 'both' }}>
            <span className="ecc-h1 mt-3">{RA_STR.refreshdetailslabel}</span>
            <MaterialTable
              columns={this.state.cacheColumns}
              data={this.state.cacheDeatils}
              options={{
                selection: false,
                paging: false,
                search: false,
                sorting: false,
              }}
            />
            <span className="ecc-h1 mt-3">{RA_STR.serverinstancelabel} </span>
            <MaterialTable
              columns={this.state.serverColumns}
              data={this.state.apiData}
              options={{
                selection: false,
                paging: false,
                search: false,
                sorting: false,
              }}
            />
          </div>
          :
          <React.Fragment />}
    </div>;
  }
}

export default CacheStatus;
