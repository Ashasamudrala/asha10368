import React, { Component } from 'react';
import { Route, Link, Switch, Redirect } from 'react-router-dom';
import { dynamicNavbar } from '../../shared/utlities/privilegeRoutes';
import SearchUser from '../SearchUsers/SearchUsers';
import AdvanceSearchUser from '../SearchUsers/AdvancedSearch';
import CreateCustomRole from '../CreateCustomRole/CreateCustomRole';
import UpdateCustomRole from '../updateCustomRole/updateCustomRole';
import DeleteCustomRole from '../DeleteCustomRole/DeleteCustomRole';
import CreateOrg from '../CreateOrganization/CreateOrganization';
import SearchOrg from '../searchOrg/searchOrg';
import CacheStatus from '../CacheStatus/CacheStatus';
import RiskAnalytics from '../RiskAnalyticsConn/RiskAnalyticsConn';
import TrustedCertificate from '../TrustedCertificate/TrustedCertificate';
import ManageTokenSite from '../ManageTokenSites/ManageTokenSites';
import ManageToken from '../ManageToken/ManageToken';
import EndpointManagement from '../EndpointManagement/EndpointManagement';
import AMDSConnectivity from '../AMDSConnectivity/AMDSConnectivity';
import ProtocolConfig from '../ProtocolConfig/ProtocolConfig';
import CustomReportViews from '../CustomReportViews/CustomReportViews';
// import CustomReportizeViews from '../CustomizeReportViews/CutomizeReportViews';
import UDSConnectivity from '../UDSConnectivity/UDSConnectivity';
import RefreshCache from '../RefreshCache/RefreshCache';
import AttributeEncryptionConfig from '../AttributeEncryptionConfig/AttributeEncryptionConfig';
import LocalizationConfig from '../LocalizationConfig/LocalizationConfig';
import ConfigureAccount from '../ConfigureAccount/ConfigureAccount';
import SetDefaultOrg from '../SetDefaultOrg/SetDefaultOrg';
import AuthenticationAuthorization from '../AuthenticationAndAuthorization/AuthenticationAndAuthorization';
import BasicAuthneticationPriority from '../BasicAuthenticationPolicy/BasicAuthenticationPolicy';
import MasterAdministratorAuthenticationPolicy from '../MasterAdministratorAuthenticationPolicy/MasterAdministratorAuthenticationPolicy'
import UserDataServiceConfiguration from '../UserDataServiceConfiguration/UserDataServiceConfiguration';
import EmailConfiguration from '../EmailConfiguration/EmailConfiguration';
import UdsContactDetails from '../UdsContactDetails/UdsContactDetails';
import MiscellenousConfig from '../miscellenousConfig/miscellenousConfig';
import ConfigOrgRelation from '../configOrgRelation/configOrgRelation';
import EnableDataFeedConfig from '../enableDataFeedConfig/enableDataFeedConfig';
import MigrateToProduction from '../migrateToProduction/migrateToProduction';
import RuleScoringManagement from '../RulesScoringMgmt/RulesScoringMgmt';
import AssignChannels from '../AssignChannelsConfig/AssignChannelsConfig';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import SuccessList from '../../shared/components/SuccessList/SuccessList';
import MyProfile from '../myProfile/myProfile';
import Create from '../createAdminDetails/createAdminDetails';
import SearchDetails from '../SearchUsers/SearchUserDetails';
import EditForm from '../SearchUsers/EditForm';
import UpdateUserInformation from '../SearchUsers/UpdateUserInformation';
import CreateRule from '../CreateRule/CreateRule';
import AssignRule from '../AssignRule/AssignRule';
import OrganizationDetails from '../OrganizationDetails/OrganizationDetails';
import CreateListorganization from '../CreateListOrganization/createListorg';
import EditList from '../EditList/EditList';
import BasicAuthenticationPolicy from '../BasicAuthenticationPolicy/BasicAuthenticationPolicy';
import BulkUpload from '../BulkUpload/BulkUpload';
import ViewBulkRequests from '../ViewBulkRequests/ViewBulkRequests';
import CalloutConfig from '../CalloutConfig/CalloutConfig';
import SSConfigMiscellaneousConfig from '../SSConfigMiscellaneousConfig/SSConfigMiscellaneousConfig';
import AssociateEndPoint from '../AssociateEndPoint/AssociateEndPoint';
import ManageDataFeed from '../ManageDataFeed/ManageDataFeed';
import MapDataFeed from '../MapDataFeed/MapDataFeed';
import AmConnConfig from '../AmConnConfig/AmConnConfig';
import RequestDetails from '../ViewBulkRequests/RequestDetails';
import DynamicListRender from '../../shared/components/DynamicListRender/DynamicListRender';
import { RA_STR_BASIC_AUTH_PATH } from '../../shared/utlities/constants';
import ViewTaskDetails from '../ViewBulkRequests/ViewTaskDetails';
import CustomizeReportViews from '../CustomizeReportViews/CutomizeReportViews';
import ReportsSummary from '../ReportsSummary/ReportsSummary';
import RiskEvaluationDetailActivityReport from '../RiskActivityReport/RiskActivityReport';
import RiskAdviceSummaryReport from '../RiskSummaryReport/RiskSummaryReport';
import ExceptionUserReport from '../ExceptionUserReport/ExceptionUserReport';
import RuleConfigurationsReport from '../RuleConfigReport/RuleConfigReport';
import RulesDataReport from '../RulesDataReport/RulesDataReport';
import DeviceSummaryReport from '../DeviceSummaryReport/DeviceSummaryReport';
import MyActivityReport from '../MyActivityReport/MyActivityReport';
import AdminActivityReport from '../AdminActivityReport/AdminActivityReport';
import UserActivityReport from '../UserActivityReport/UserActivityReport';
import UserCreationReport from '../UserCreationReport/UserCreationReport';
import OrganizationReport from '../OrganizationReport/OrganizationReport';
import EntitlementReport from '../EntitlementReport/EntitlementReport';
import ManageInboundCalls from '../ManageInboundCalls/ManageInboundCalls';
import WorkOnCases from '../WorkOnCases/WorkOnCases';
import AnalyzeTransactions from '../AnalyzeTransactions/AnalyzeTransactions';
import SearchCases from '../SearchCases/SearchCases';
import QueueStatus from '../ViewQueueStatus/ViewQueueStatus';
import ManageQueues from '../ManageQueue/ManageQueue';
import CaseActivityReport from '../CaseActivityReport/CaseActivityReport';
import AverageCaseLifeReport from '../AverageCaseLifeReport/AverageCaseLifeReport';
import FraudStatisticsReport from '../FraudStaticsReport/FraudStaticsReport';
import RuleEffectivenessReport from '../RuleEffectivnessReport/RuleEffectivnessReport';
import FalsePositivesReport from '../FalsePositivesReport/FalsePositivesReport';
import CaseStatus from '../CaseStatus/CaseStatus';
import FraudStatus from '../FraudStatus/FraudStatus';
import FraudReport from '../FraudReport/FraudReport';
import ActivateOrganisation from '../CreateOrganization/ActivateOrganisation';
import SelectedAttributes from '../OrganizationDetails/SelectAttributes';
import UpdateAdmin from '../OrganizationDetails/UpdateAdministrator';
import ConfigureEmailTelephone from '../OrganizationDetails/ConfigureEmailTelephoneType';
import ConfigureAccountType from '../OrganizationDetails/ConfigureAccountType';
import ConfigureAccountUpdateType from '../OrganizationDetails/ConfigureAccountType';
import ConfigureAccountCustomeAttributes from '../OrganizationDetails/ConfigureAccountCustomeAttributes';
import ConfigureEmailTelphoneType from '../OrganizationDetails/ConfigureEmailTelephoneType';
import BulkUploadOrganisation from '../BulkUploadOrganisation/BulkUploadOrganisation';
import BulkUploadAdministrator from '../BulkUploadAdministrator/BulkUploadAdministrator';
import BulkUploadActivityReport from '../BulkUploadActivityReport/BulkUploadActivityReport';
import BulkUploadActivityViewReport from '../BulkUploadActivityReport/BulkUploadActivityViewReport';
import ModelConfiguration from '../ModelConfiguration/ModelConfiguration';
import InstanceManagementReport from '../InstanceManagementReport/InstanceManagementReport';
// import InstanceManagement from '../InstanceManagement/instancemanagement';'
import TransactionDataReport from '../TransactionDataReport/TransactionDataReport';
import AmdsProfileAssociation from '../AmdsProfileAssociation/AmdsProfileAssociation';
import EvaluateionCallout from '../CalloutConfig/EvaluateionCallout';
import ServerCalloutConfig from '../ServerCalloutConfig/ServerCalloutConfig';
import ManageListCategoryMappingRA from "../ManageListCategoryMappingRA/ManageListCategoryMappingRA";
import ConfigureFieldsRelay from '../ConfigureFieldsRelay/ConfigureFieldsRelay';
import CaseStatusReport from '../CaseStatus/CaseStatusReport';
import CaseStatusReportView from '../CaseStatus/CaseStatusView';
import InstanceManagement from '../InstanceManagement/InstanceManagement';
import InstanceManagemenTitle from '../InstanceManagement/InstanceManagmentTitle';
import SessionPopup from '../SessionTimeout/sessionPopup'
import './SideBar.css';
import IdleTimer from 'react-idle-timer'
import { local } from 'd3';


class SideBar extends Component {
  constructor(props) {
    super(props);
    let sessionTimout = 0;
    if (localStorage.getItem('orgIdleTimeout')) {
      sessionTimout = parseInt(localStorage.getItem('orgIdleTimeout'));
    }
    this.state = {
      openLeftNav: true,
      enableErrorList: false,
      enableSuccessList: false,
      validationData: [],
      successValidationData: [],
      windowLocation: window.location.pathname.split('/')[window.location.pathname.split('/').length - 1] || '',
      showSessionPopup: false,
      sessionTimeout: sessionTimout
    }
  }

  componentWillMount() {
    const { history } = this.props;
    this.unsubscribeFromHistory = history.listen(this.handleLocationChange);
    this.handleLocationChange(history.location);
  }

  componentWillUnmount() {
    if (this.unsubscribeFromHistory) this.unsubscribeFromHistory();
  }

  handleLocationChange = (location) => {
    window.scrollTo(0, 0);
  }
  extendSession = () => {
    localStorage.setItem('orgIdleTimeout', 120);
    this.setState({ showSessionPopup: false, sessionTimeout: 120 });
  }
  toogleLeftNav = () => {
    this.setState({ openLeftNav: !this.state.openLeftNav });
  }
  showErrorList = (getStatus, getData) => {
    if (getStatus) {
      this.setState({ enableErrorList: true, validationData: getData });
      window.scrollTo(0, 0);
    } else {
      this.setState({ enableErrorList: false });
    }
  }
  showSuccessList = (getStatus, getData) => {
    if (getStatus) {
      this.setState({ enableSuccessList: true, successValidationData: getData });
      window.scrollTo(0, 0);
    } else {
      this.setState({ enableSuccessList: false });
    }
  }
  onAction = (e) => { }

  onActive = (e) => {
  }

  onIdle = (e) => {
    this.setState({ showSessionPopup: true })
  }
  render() {
    const match = this.props.match;
    const { showSessionPopup, windowLocation, sessionTimeout } = this.state;
    const windowLocationBasePath = process.env.REACT_APP_ROUTER_BASE || '';
    const urlParams = new URLSearchParams(window.location.search);
    const orgName = urlParams.get('orgname');
    const status = urlParams.get('status');
    const orgList = {
      'status': status, 'orgName': orgName
    }
    const messageProps = {
      activateErrorList: this.showErrorList,
      activateSuccessList: this.showSuccessList
    }
    var strSlash = match.url.lastIndexOf("/");
    var stringRes = match.url.substring(strSlash, match.url.length);
    const getCurrentpath = (windowLocationBasePath) ? window.location.pathname.replace(windowLocationBasePath, '') : window.location.pathname;
    var strSlashDynamic = getCurrentpath.lastIndexOf("/");
    var stringResDynamic = getCurrentpath.substring(0, strSlashDynamic);
    var getLefNavInfo = '';
    if (this.props.selectedRouteInfo && this.props.selectedRouteInfo.hideSecondNav) {
      getLefNavInfo = this.props.selectedRouteInfo.subRoutes;
    } else if (this.props.selectedRouteInfo) {
      getLefNavInfo = this.props.selectedRouteInfo.subRoutes.filter(function (entry) { return entry.route === stringRes });
    }

    let getLefNavInfoNav = dynamicNavbar.filter(function (entry) { return entry.route === stringResDynamic });
    if (getLefNavInfoNav.length === 0) {
      getLefNavInfoNav = dynamicNavbar.filter(function (entry) { return entry.route === getCurrentpath });
    }
    const activationClass = stringResDynamic;
    if (getLefNavInfoNav.length === 0) {
      var strSlashSecond = stringResDynamic.lastIndexOf("/");
      var stringResSecond = stringResDynamic.substring(0, strSlashSecond);
      getLefNavInfoNav = dynamicNavbar.filter(function (entry) { return entry.route === stringResSecond });
      stringResDynamic = stringResSecond;
    }
    const profiledata = JSON.parse(localStorage.getItem('profiledata'));
    const previlages = profiledata ? profiledata.userProfile.privileges : '';
    return <div className="sidebar-parent">
      {this.state.openLeftNav ?
        <div className="side-toogle-menu">
          <div id="reportTaskPane">
            <ul className="side-menu-items">
              {this.props.selectedRouteInfo && !this.props.selectedRouteInfo.hideSecondNav ? getLefNavInfo && getLefNavInfo.map((keyOuter) => {
                {
                  return !keyOuter.multilevel ? keyOuter.subRoutes && keyOuter.subRoutes.map((innerRoute) => {
                    if (previlages.includes(innerRoute.privilegeID)) {
                      return <Link to={`${match.url}${innerRoute.route}`} key={innerRoute.route}>
                        <li className={`${match.url}${innerRoute.route}` === getCurrentpath ? 'activated-route' : ''}>{innerRoute.description}</li></Link>
                    }
                  }) :
                    keyOuter.subRoutes.map((multilevelData) => {
                      let count = 0;
                      return multilevelData.subRoutesInfo.map((innerData) => {
                        if (previlages.includes(innerData.privilegeID)) {
                          count = count + 1;
                          return [multilevelData.BaseHeader && count === 1 ? <h6 className="side_menu_l4_header" key={multilevelData.BaseHeader}>{multilevelData.BaseHeader}</h6> : '',
                          <Link to={`${match.url}${innerData.route}`} key={innerData.route}>
                            <li className={`${match.url}${innerData.route}` === getCurrentpath ? 'activated-route' : ''}>{innerData.description}</li></Link>

                          ]
                        }
                      })
                    })
                }
              }) : getLefNavInfo && getLefNavInfo.map((displayeaach) => {
                if (displayeaach.subRoutesInfo && displayeaach.subRoutesInfo.length) {
                  let count = 0;
                  return displayeaach.subRoutesInfo.map((innerData) => {
                    if (previlages.includes(innerData.privilegeID)) {
                      count = count + 1;
                      return [displayeaach.BaseHeader && count === 1 ? <h6 className="side_menu_l4_header" key={displayeaach.BaseHeader}>{displayeaach.BaseHeader}</h6> : '',
                      <Link to={`${match.url}${innerData.route}`} key={innerData.route}>
                        <li className={`${match.url}${innerData.route}` === getCurrentpath ? 'activated-route' : ''}>{innerData.description}</li></Link>

                      ]
                    }
                  })
                } else {
                  if (previlages.includes(displayeaach.privilegeID) || displayeaach.defaultOrg) {
                    return <Link to={`${match.url}${displayeaach.route}`} key={displayeaach.route}>
                      <li className={`${match.url}${displayeaach.route}` === getCurrentpath ? 'activated-route' : ''}>{displayeaach.description}</li></Link>
                  }
                }
              })}
            </ul>
            {getLefNavInfoNav[0] ?
              <ul className="dynamic-adding-list">
                {getLefNavInfoNav[0] && getLefNavInfoNav[0].dynamicLevel ?
                  <h6 className="side_menu_l4_header baseHeader" key={getLefNavInfoNav[0].BaseHeader} title='Basic Organization Information'>{getLefNavInfoNav[0].description}</h6>
                  : ''}
                {getLefNavInfoNav && getLefNavInfoNav[0].dynamicLevel && getLefNavInfoNav[0].multilevel ?
                  getLefNavInfoNav[0].subRoutes.map(multilevelData => {
                    let count = 0;
                    return multilevelData.subRoutesInfo.map((innerData) => {
                      if (previlages.includes(innerData.privilegeID)) {
                        count = count + 1;
                        return [multilevelData.BaseHeader && count === 1 ? <h6 className="side_menu_l4_header" key={multilevelData.BaseHeader}>{multilevelData.BaseHeader}</h6> : '',
                        <Link to={`${stringResDynamic}${innerData.route}?orgname=${orgName}&status=${status}`} key={innerData.route}>
                          <li className={`${stringResDynamic}${innerData.route}` === getCurrentpath ? 'activated-route' : ''}>{innerData.description}</li></Link>
                        ]
                      }
                    })
                  })
                  :
                  getLefNavInfoNav[0].dynamicLevel && getLefNavInfoNav[0].subRoutes.map(extraList => {
                    if (previlages.includes(extraList.privilegeID)) {
                      if (orgName) {
                        return <Link to={`${stringResDynamic}${extraList.route}?orgname=${orgName}&status=${status}`} key={extraList.route}>
                          <li className={(`${stringResDynamic}${extraList.route}` === getCurrentpath) || (`${stringResDynamic}${extraList.route}` === activationClass) ? 'activated-route' : ''}>{extraList.description}</li></Link>
                      } else {
                        return <Link to={`${stringResDynamic}${extraList.route}`} key={extraList.route}>
                          <li className={(`${stringResDynamic}${extraList.route}` === getCurrentpath) || (`${stringResDynamic}${extraList.route}` === activationClass) ? 'activated-route' : ''}>{extraList.description}</li></Link>
                      }
                    }
                  })
                }
              </ul> : ''
            }
          </div>
        </div> : ''}
      <div className={this.state.openLeftNav ? 'main-content' : 'menu-hide main-content'}>
        <span className="left-nav-buttons" onClick={this.toogleLeftNav}>
          {this.state.openLeftNav ? <img src="images/closeLeftPanel.png"></img> : <img src="images/openLeftPanel.png"></img>}
        </span>
        {this.state.enableErrorList ? <ErrorList validationDataList={this.state.validationData}></ErrorList> : ''}
        {this.state.enableSuccessList ? <SuccessList validationData={this.state.successValidationData}></SuccessList> : ''}
        <div>
          {orgList['orgName'] !== null && orgList['status'] !== null ?
            <DynamicListRender data={orgList} />
            : ''
          }
        </div>
        {sessionTimeout > 0 && <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={1000 * sessionTimeout} />}
        <Switch>
          <Route path={`/myProfile`} render={(props) => <MyProfile {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Redirect path='/users/manageUsers' to='manageUsers/search' exact />
          <Redirect path='/users/manageRoles' to='manageRoles/createCustomRole' exact />
          <Redirect path='/org/ra' to='ra/createOrg' exact />

          {profiledata.userProfile.role === 'MA' ? <Redirect path='/server-config/ra' to='ra/risk-conn' exact /> : <Redirect path='/server-config/ra' to='ra/service-miss-config' exact />}
          {profiledata.userProfile.role === 'MA' ? <Redirect path='/server-config/ac' to='ac/am-conn' exact /> : <Redirect path='/server-config/ac' to='ac/refresh-cache' exact />}
          {profiledata.userProfile.role === 'MA' ? <Redirect path='/reports/ra' to='ra/instance-mgmt-report' exact /> : <Redirect path='/reports/ra' to='ra/risk-conn' exact />}
          <Redirect path='/reports/ac' to='ac/activity-report' exact />
          <Route path={`/users/manageUsers/search`} render={(props) => <SearchUser {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageUsers/basicUser/userInfo`} render={(props) => <SearchDetails {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageUsers/basicUser/userInfo/editUserInformation`} render={(props) => <EditForm {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageUsers/basicUser/userInfo/updateUserInformation`} render={(props) => <UpdateUserInformation {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageUsers/users/advancesearch`} render={(props) => <AdvanceSearchUser  {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageUsers/bulkuploadadministrator`} render={(props) => <BulkUploadAdministrator {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageUsers/create`} render={(props) => <Create {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageRoles/createCustomRole`} render={(props) => <CreateCustomRole {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageRoles/updateCustomRole`} render={(props) => <UpdateCustomRole {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/users/manageRoles/deleteCustomRole`} render={(props) => <DeleteCustomRole {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/createOrg`} render={(props) => <CreateOrg {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/bulkuploadorganisation`} render={(props) => <BulkUploadOrganisation {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg`} render={(props) => <SearchOrg {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/create-ruleset`} render={(props) => <CreateRule {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/ss-create-ruleset`} render={(props) => <CreateRule {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/assign-ruleset`} render={(props) => <AssignRule {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/cacheStatus`} render={(props) => <CacheStatus {...props} activateErrorList={this.showErrorList} />} />
          <Route path={`/server-config/ra/risk-conn`} render={(props) => <RiskAnalytics {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/manage-data-feed`} render={(props) => <ManageDataFeed {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/service-miss-config`} component={SSConfigMiscellaneousConfig} />
          <Route path={`/server-config/ra/ss-callout-config`} render={(props) => <ServerCalloutConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/create-list`} render={(props) => <CreateListorganization {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/manage-data-feed`} component={ManageDataFeed} />}
          <Route path={`/server-config/ra/associate-end-pont`} render={(props) => <AssociateEndPoint {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/trusted-cert-auth`} render={(props) => <TrustedCertificate {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/manage-token-sites`} render={(props) => <ManageTokenSite {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/manage-token`} render={(props) => <ManageToken {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/endpoint-manage`} render={(props) => <EndpointManagement {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/protocol-config`} render={(props) => <ProtocolConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/amds-conn`} render={(props) => <AMDSConnectivity {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/custom-report-view`} render={(props) => <CustomReportViews {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/map-data-feed`} render={(props) => <MapDataFeed {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/manage-list-data`} render={(props) => <ManageListCategoryMappingRA {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/uds-conn`} render={(props) => <UDSConnectivity {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/refresh-cache`} component={RefreshCache} />
          <Route path={`/server-config/ra/ra-config`} render={(props) => <ConfigureFieldsRelay {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/cacheStatus`} render={(props) => <CacheStatus {...props} activateErrorList={this.showErrorList} />} />
          <Route path={`/server-config/ac/attr-encry-config`} render={(props) => <AttributeEncryptionConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ac/local-config`} render={(props) => <LocalizationConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ac/default-org`} render={(props) => <SetDefaultOrg {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/config-account`} render={(props) => <ConfigureAccount {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ac/auth`} render={(props) => <AuthenticationAuthorization {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ac/basic-auth`} render={(props) => <BasicAuthneticationPriority {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ac/master-auth`} render={(props) => <MasterAdministratorAuthenticationPolicy {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ac/email-telephone-config`} render={(props) => <UdsContactDetails {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/uds-config`} render={(props) => <UserDataServiceConfiguration {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/am-conn`} component={AmConnConfig} />
          <Route path={`/server-config/ra/instance-manage`} render={(props) => <InstanceManagement {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/instance-manage-title`} render={(props) => <InstanceManagemenTitle {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ac/amds-profile`} render={(props) => <AmdsProfileAssociation {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/createOrg/activate-organisation`} component={ActivateOrganisation} />
          <Route path={`/org/searchOrg/basicOrg/org-details`} render={(props) => <OrganizationDetails {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/basicOrg/basic-auth-policy`} render={(props) => <BasicAuthenticationPolicy {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/basicOrg/bulk-upload`} render={(props) => <BulkUpload {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/basicOrg/view-bulk-requests`} render={(props) => <ViewBulkRequests {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/basicOrg/view-request-details`} render={(props) => <RequestDetails {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/basicOrg/task-details`} render={(props) => <ViewTaskDetails {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ra/instance-mgmt-report`} render={(props) => <InstanceManagementReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />

          /* Create Organization Details Sub Routes*/
          <Route path={`/org/createOrg/attribute-encryption`} component={SelectedAttributes} />
          <Route path={`/org/createOrg/updateadmin`} component={UpdateAdmin} />
          <Route path={`/org/createOrg/configure-account`} component={ConfigureAccountType} />
          <Route path={`/org/createOrg/configure-custom-attribute`} component={ConfigureAccountCustomeAttributes} />
          <Route path={`/org/createOrg/configure-email-telephone-type`} render={(props) => <ConfigureEmailTelphoneType {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/createOrg/activate-organisation`} component={ActivateOrganisation} />

          /*  Organization Details Sub Routes*/
          <Route path={`/org/searchOrg/basicOrg/org-details/attribute-encryption`} component={SelectedAttributes} />
          <Route path={`/org/searchOrg/basicOrg/org-details/updateadmin`} component={UpdateAdmin} />
          <Route path={`/org/searchOrg/basicOrg/org-details/configure-account-type`} render={(props) => <ConfigureAccountUpdateType {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg/basicOrg/org-details/configure-account`} render={(props) => <ConfigureEmailTelephone {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg/basicOrg/org-details/configure-custom-attribute`} component={ConfigureAccountCustomeAttributes} />}/>

          /*  Call out Sub Routes*/
          <Route path={`/org/searchOrg/risk-engine/callout-config/evaluate-callout`} render={(props) => <EvaluateionCallout {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/ss-callout-config/evaluate-callout`} render={(props) => <EvaluateionCallout {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />

          <Route path={`/org/searchOrg/risk-engine/miscellenousConfiguartions`} render={(props) => <MiscellenousConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/create-list`} render={(props) => <CreateListorganization {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg/risk-engine/edit-list`} render={(props) => <EditList {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg/risk-engine/conf-org-rel`} render={(props) => <ConfigOrgRelation {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/enable-data-feed`} render={(props) => <EnableDataFeedConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/migrate-prod`} render={(props) => <MigrateToProduction {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/server-config/ra/migrate-prod`} render={(props) => <MigrateToProduction {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg/risk-engine/Rules-Scoring`} render={(props) => <RuleScoringManagement {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/org/searchOrg/risk-engine/model-config`} render={(props) => <ModelConfiguration {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/manage-list-data`} render={(props) => <ManageListCategoryMappingRA {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />

          <Route path={`/org/searchOrg/risk-engine/callout-config`} render={(props) => <CalloutConfig {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/assign-channels`} render={(props) => <AssignChannels {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/customize-report-view`} render={(props) => <CustomizeReportViews {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/server-config/ra/Rules-Scoring`} render={(props) => <RuleScoringManagement {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/reports/ra/transaction-report`} render={(props) => <TransactionDataReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/org/searchOrg/risk-engine/assign-channels`} render={(props) => <AssignChannels {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ra/risk-conn`} component={ReportsSummary} />
          <Route path={`/reports/ra/risk-activity-report`} render={(props) => <RiskEvaluationDetailActivityReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/reports/ra/risk-summary-report`} render={(props) => <RiskAdviceSummaryReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ra/exception-user-report`} render={(props) => <ExceptionUserReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ra/rule-config-report`} render={(props) => <RuleConfigurationsReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ac/activity-report`} render={(props) => <MyActivityReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ra/rules-data-report`} render={(props) => <RulesDataReport {...props} {...messageProps} />} exact />
          <Route path={`/reports/ra/device-summary-report`} render={(props) => <DeviceSummaryReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ac/admin-activity-report`} render={(props) => <AdminActivityReport {...props}  {...messageProps} />} exact />
          <Route path={`/reports/ac/bulk-upload-activity`} component={BulkUploadActivityReport} />
          <Route path={`/reports/ac/report-view`} component={BulkUploadActivityViewReport} />
          <Route path={`/reports/ac/user-activity-report`} component={UserActivityReport} />
          <Route path={`/reports/ac/user-creation-report`} component={UserCreationReport} />
          <Route path={`/reports/ac/org-report`} render={(props) => <OrganizationReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ac/entitlement-report`} render={(props) => <EntitlementReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/manage-inbound`} render={(props) => <ManageInboundCalls {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/reports/ac/entitlement-report`} render={(props) => <EntitlementReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/work-cases`} component={WorkOnCases} />
          <Route path={`/case-manage/analyze-transaction`} render={(props) => <AnalyzeTransactions {...props}  {...messageProps} />} exact />
          <Route path={`/case-manage/search-case`} render={(props) => <SearchCases {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/queue-status`} component={QueueStatus} />
          <Route path={`/case-manage/manage-queue`} render={(props) => <ManageQueues {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/case-activity-report`} render={(props) => <CaseActivityReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/average-report`} render={(props) => <AverageCaseLifeReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/fraud-report`} render={(props) => <FraudStatisticsReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/rule-report`} render={(props) => <RuleEffectivenessReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/false-report`} render={(props) => <FalsePositivesReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/reviewer-report`} render={(props) => <CaseStatus {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/reviewer-report/reviewer-efficiency-report`} render={(props) => <CaseStatusReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/reviewer-report/reviewer-efficiency-report-view`} render={(props) => <CaseStatusReportView {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} exact />
          <Route path={`/case-manage/reviewer-report-fraud`} render={(props) => <FraudStatus {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
          <Route path={`/case-manage/rule-report-fraud`} render={(props) => <FraudReport {...props} activateErrorList={this.showErrorList} activateSuccessList={this.showSuccessList} />} />
        </Switch>
        {showSessionPopup && <SessionPopup extendSession={this.extendSession}></SessionPopup>}
      </div>
    </div>
  }
}
export default SideBar;
