import React, { Component } from 'react';
import {
  serverUrl, RA_API_URL, RA_API_STATUS, RA_STR_SELECT, RA_STR_CHECKBOX
} from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import { getService } from '../../shared/utlities/RestAPI';
import Select from '../../shared/components/Select/Select';
import '../ReportsSummary/ReportsSummary.css';
import '../ManageDataFeed/ManageDataFeed.css';
import './MapDataFeed.scss';
import ReactDOM from 'react-dom';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import _ from 'underscore';

class MapDataFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pluginList: [],
      protocolList: [],
      getDataFeedList: [],
      getConfigList: [],
      mapDataForm: {},
      showProtocol: '',
      selectedPlugin: '',
      orgList: [],
      apiData: [],
      selected: [],
      groupNames: [],
      groupForm: [],
      enableDatafeed: false,
      showConfig: false,
      columns: [
        {
          title: 'ORGANIZATION', field: 'name', render: (rowData) =>
            <span>{rowData.displayName}</span>
        },
        {
          title: 'CLIENTID', field: 'CLIENTID', render: rowData =>
            <div className="dynamic-form-input">
              <input type='text' name="clientId" className=" form-control" defaultValue={rowData.clientId}
                onChange={this.rowChange.bind(this, rowData)} />
            </div>
        },
        {
          title: 'VENDORID', field: 'VENDORID', render: rowData =>
            <div className="dynamic-form-input">
              <input type='text' name='vendorId' className="form-control" defaultValue={rowData.vendorId}
                onChange={this.rowChange.bind(this, rowData)} />
            </div>
        },
        {
          title: 'USERID', field: 'USERID', render: rowData =>
            <div className="dynamic-form-input">
              <input type='text' name="userId" className="form-control" defaultValue={rowData.userId}
                onChange={this.rowChange.bind(this, rowData)} />
            </div>
        },
        {
          title: 'GETQNAME', field: 'GETQNAME', render: rowData =>
            <div className="dynamic-form-input">
              <input type='text' name='getQueueId' className="form-control" defaultValue={rowData.getQueueId}
                onChange={this.rowChange.bind(this, rowData)} />
            </div>
        },
        {
          title: 'PUTQNAME', field: 'PUTQNAME', render: rowData =>
            <div className="dynamic-form-input">
              <input type='text' name="putQueueId" className="form-control" defaultValue={rowData.putQueueId}
                onChange={this.rowChange.bind(this, rowData)} />
            </div>
        },
        {
          title: 'CONFIGNAME', field: 'CONFIGNAME', render: rowData =>
            <div className="dynamic-form-select">
              <select className="form-select form-control custom-select-sm"
                name="dfEpConfigId"
                defaultValue={rowData.dfEpConfigId}
                onChange={this.rowChange.bind(this, rowData)}>
                <option value="">{RA_STR_SELECT}</option>
                {this.state.getConfigList.map(opt => {
                  return (
                    <option
                      key={opt.key}
                      value={opt.key}>{opt.content}</option>
                  );
                })}
              </select>
            </div>
        },
      ],
    }
  }


  showErrorList = () => {
    window.scrollTo(0, 0);
  }

  getPlugin = async () => {
    const getPluginData = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getPlugins']}`,
    };
    const getPluginStatus = await getService(getPluginData);
    if (getPluginStatus && getPluginStatus.status === RA_API_STATUS['200']) {
      let objectKeys = Object.keys(getPluginStatus.data.plugins);
      let objectValues = Object.values(getPluginStatus.data.plugins);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.pluginList.push({
          'key': objectValues[i],
          'content': objectValues[i]
        })
        this.setState({
          pluginList: this.state.pluginList
        });
      }
    }
  }

  getProtocol = async () => {
    const getProtocolData = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getProtocols']}`,
    };
    const getProtocolStatus = await getService(getProtocolData);
    if (getProtocolStatus && getProtocolStatus.status === RA_API_STATUS['200']) {
      let objectKeys = Object.keys(getProtocolStatus.data.protocols);
      let objectValues = Object.values(getProtocolStatus.data.protocols);
      for (let i = 0; i < objectKeys.length; i++) {
        this.state.protocolList.push({
          'key': objectValues[i],
          'content': objectValues[i]
        })
        this.setState({
          protocolList: this.state.protocolList
        });
      }
    }
  }

  componentDidMount() {
    this.getPlugin();
  }

  rowChange = (val, e) => {
    const rowList = val;
    const { name, value } = e.target;
    rowList[name] = value;
    let splicedaccountType = [...this.state.groupForm]
    splicedaccountType.push(rowList)
    this.setState({
      groupForm: _.uniq(splicedaccountType)
    });
  }

  handleChange = async (val, e) => {
    const mapDataForm = this.state.mapDataForm;
    const { name, value } = e.target;
    var fieldValue;
    fieldValue = value;
    mapDataForm[name] = fieldValue;
    this.setState({
      mapDataForm
    })
    if (val === 'plugin' && value !== '') {
      this.getProtocol();
      this.setState({
        showProtocol: true,
        selectedPlugin: value
      })
    }
    else if (val === 'protocol' && value !== '') {
      const getDataFeed = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL['getDatafeedEndpoints']}/${this.state.selectedPlugin}/${value}`,
      };
      const getDataFeedStatus = await getService(getDataFeed);
      if (getDataFeedStatus && getDataFeedStatus.status === RA_API_STATUS['200']) {
        let objectKeys = Object.keys(getDataFeedStatus.data.endpoints);
        let objectValues = Object.values(getDataFeedStatus.data.endpoints);
        for (let i = 0; i < objectKeys.length; i++) {
          this.state.getDataFeedList.push({
            'key': objectValues[i],
            'content': objectValues[i]
          })
          this.setState({
            getDataFeedList: this.state.getDataFeedList
          });
        }
      }
    }
    else if (val === 'dfendpoint' && value !== '') {
      this.setState({
        getConfigList: []
      })
      const getOrg = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL['getAvailableOrganizations']}/${this.state.selectedPlugin}/${value}`,
      };
      const getOrgStatus = await getService(getOrg);
      if (getOrgStatus && getOrgStatus.status === RA_API_STATUS['200']) {
        this.setState({
          orgList: getOrgStatus.data.availableOrgs
        });
      }
      const getConfig = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL['getEndpointConfiguration']}/${value}`,
      };
      const getConfigStatus = await getService(getConfig);
      if (getConfigStatus && getConfigStatus.status === RA_API_STATUS['200']) {
        let objectKeys = Object.keys(getConfigStatus.data.configs);
        for (let i = 0; i < objectKeys.length; i++) {
          this.state.getConfigList.push({
            'key': objectKeys[i],
            'content': objectKeys[i]
          })
          this.setState({
            getConfigList: this.state.getConfigList,
          });
        }
      }
    }
    else if (val === 'selectOptions') {
      var select = ReactDOM.findDOMNode(this.refs['selectOrg']);
      var values = [].filter.call(select.options, function (o) {
        return o.selected;
      }).map(function (o) {
        return o.value;
      });
      this.setState({
        selected: values
      })
    }
    else if (val === 'enableDatafeed' && e.target.type === RA_STR_CHECKBOX) {
      let enableObj = this.state.apiData || this.state.groupForm;
      if (e.target.checked === true) {
        var filtered = _.filter(enableObj, function (item) {
          return item.selected === true
        });
        this.setState({
          apiData: filtered
        })
      }
      else {
        this.showConfig();
      }
    }
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  getcheckboxselection = (getData) => {
    getData.map(item => {
      item.selected = item.tableData.checked;
      if(item.selected === true){
        item.id = item.id;
      }
      else{
        item.id = null;
      }
    })
    this.setState({
      groupForm: getData || this.state.groupForm,

    })
  }

  handleReset = () => {
    this.setState({
      pluginList: [],
      protocolList: [],
      getDataFeedList: [],
      mapDataForm: {},
      showProtocol: '',
      selectedPlugin: '',
      orgList: [],
      selected: [],
      getConfigList: [],
      showConfig: false
    })
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    this.getPlugin();
  }

  showConfig = async () => {
    this.setState({
      apiData: [],
      groupForm: []
    })
    let params = {
      ...this.state.mapDataForm,
      selectedOrgs: [...this.state.selected]
    }
    const showConfig = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['showConfigurations']}`,
      data: params
    };
    const showConfigStatus = await getService(showConfig);
    if (showConfigStatus && showConfigStatus.status === RA_API_STATUS['200']) {
      this.props.activateSuccessList(true, showConfigStatus.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
      this.setState({
        apiData: showConfigStatus.data.orgsDfConfigurations,
        showConfig: true
      })
    }
    else {
      this.props.activateErrorList(true, showConfigStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  }

  handleSubmit = async () => {
    let params = {
      category: "ECMDF",
      actionId: 3,
      showTable: true,
      sortOrder: "ASC",
      selectedOrgnames: [...this.state.selected],
      groupNames: [...this.state.groupForm],
      ...this.state.mapDataForm,
    }

    const saveDataFeed = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['mapconfiguration']}`,
      data: params
    };
    const saveDataFeedStatus = await getService(saveDataFeed);
    if (saveDataFeedStatus && saveDataFeedStatus.status === RA_API_STATUS['200']) {
      this.props.activateSuccessList(true, saveDataFeedStatus.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
    }
    else {
      this.props.activateErrorList(true, saveDataFeedStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  }

  render() {
    const { pluginList, protocolList, showProtocol, getDataFeedList,
      orgList, apiData, selected, showConfig } = this.state;
    return (
      <div className='main map-data-feed'>
        <h2 className='title'>{RA_STR.mapDataTitle}</h2>
        <p className='desc'>{RA_STR.mapDataText}</p>
        <div className='col-sm-8'>
          <Select
            name={'plugin'}
            title={'Plugin'}
            options={pluginList ? pluginList : ['']}
            placeholder={RA_STR_SELECT}
            controlFunc={this.handleChange.bind(this, 'plugin')} />
          {showProtocol ?
            <Select
              name={'protocol'}
              title={'Protocol'}
              options={protocolList ? protocolList : ['']}
              placeholder={RA_STR_SELECT}
              controlFunc={this.handleChange.bind(this, 'protocol')} />
            : ''
          }
          {getDataFeedList.length ?
            <Select
              name={'dfendpoint'}
              title={'Data Feed Endpoint'}
              options={getDataFeedList ? getDataFeedList : ['']}
              placeholder={RA_STR_SELECT}
              controlFunc={this.handleChange.bind(this, 'dfendpoint')} />
            : ''
          }
        </div>
        {orgList.length ?
          <div className="margin">
            <div className='end-config'>{RA_STR.mapDataClientId}</div>
            <p className='desc'>{RA_STR.mapDataClientText}</p>
            <div className='col-sm-12 no-padding margin'>
              <div className='map-org'>
                <select className="custom-select" name="selectOptions" multiple="multiple"
                  id="selectOrg" ref="selectOrg" onChange={this.handleChange.bind(this, 'selectOptions')}>
                  {orgList.map(opt => {
                    return (
                      <option className={opt.configured ? 'configured' : '' }
                        key={opt.id}
                        value={opt.displayName}>{opt.displayName}</option>
                    );
                  })}
                </select>
                {selected.length ?
                  <p className="desc text-right">{selected.length} Seleted</p>
                  : ''
                }
              </div>
              <input className="secondary-btn pull-left show-config" type="button" value="SHOW CONFIGURATION"
                onClick={this.showConfig} />
            </div>
            {showConfig ?
              <div>
                <div className="row margin">
                  <div className="custom-control custom-checkbox ml-3">
                    <input type="checkbox" className="custom-control-input"
                      id="enableDatafeed" name="enableDatafeed"
                      defaultValue={this.state.enableDatafeed}
                      onChange={this.handleChange.bind(this, 'enableDatafeed')} />
                    <label className="custom-control-label" htmlFor="enableDatafeed"></label>
                  </div>
                  <label className="col-form-label">{RA_STR.mapDataDisplayOrg}</label>
                  <DynamicTable columns={this.state.columns} data={apiData} enablePagination={false} selection={true}
                    handleCheckboxSelection={this.getcheckboxselection} />
                  <p className='margin'><b>Note:</b>{RA_STR.mapDataNote}</p>
                </div>
              </div>
              : ''}
            <div className="form-group form-submit-button mt-5 ml-2 text-left">
              <input className="custom-secondary-btn margin" type="button" value="CANCEL"
                onClick={this.handleReset} />
              {showConfig ?
                <input className="secondary-btn ml-3" type="submit"
                  value="SAVE" onClick={this.handleSubmit} />
                : ''
              }
            </div>
          </div>
          : ''
        }
      </div>
    )
  }
}

export default MapDataFeed;
