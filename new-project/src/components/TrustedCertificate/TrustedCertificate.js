import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { RA_STR } from '../../shared/utlities/messages';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_API_STATUS } from '../../shared/utlities/constants';

class DocumentInput extends React.Component {
  render() {
    return (
      <input
        type='file'
        className='dynaic-file-input'
        name={`document-${this.props.index}-document`}
        onChange={this.props.changeData}
        multiple
      />
    );
  }
}
class TrustedCertificate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileInputs: [],
      fileCaseInputs: [],
      actionDetails: [],
      selectedFile: [],
      selectedCaseFile:[],
      selectedOtherFile:[],
      selectedVal: [],
      loopdocuments: [1, 2],
      loopdoc: [1, 2],
      action: 0,
      otherName:'',
      caseMgmtName:'',
      riskName:'',
      formHide:true
    };
    this.add = this.add.bind(this);
    this.addCase = this.addCase.bind(this);
    
  }

  componentWillMount() {
    let loopdocuments = [];
    let loopdoc=[];
    for (let i = 0; i < 2; i++) {
      loopdocuments = loopdocuments.concat(i);
      loopdoc = loopdoc.concat(i);
    }
    this.setState({ 
      fileInputs: loopdocuments, 
      fileCaseInputs: loopdoc });
  }

  add() {
    let documents = this.state.loopdocuments.length;
    let loopdocuments = this.state.loopdocuments;
    loopdocuments.push(documents + 1);
    this.setState({ loopdocuments: loopdocuments });
  }
  addCase(){
    let doc = this.state.loopdoc.length;
    let loopdoc = this.state.loopdoc;
    loopdoc.push(doc + 1);
    this.setState({ loopdoc: loopdoc });
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  handleFileChange = (e) => {
    let uploadedFiles = this.state.selectedFile;
    uploadedFiles.push(e.target.files);
    
    this.setState({
      selectedFile: uploadedFiles
    });
  };

  handleCaseFileChange = (e) => {
    let uploadedCaseFiles = this.state.selectedCaseFile;
    uploadedCaseFiles.push(e.target.files);
    this.setState({
      selectedCaseFile: uploadedCaseFiles
    });
  }

  handleOtherFileChange = (e) => {
    this.setState({
      selectedOtherFile :  Array.from(e.target.files)
    })
  }
 

  onSave = async id => {
    const { selectedFile,selectedCaseFile,selectedOtherFile } = this.state;
    const fd = new FormData();

    /*  Risk Analytics */
    selectedFile.forEach((files, i) => {
      fd.append('files', files[0]);
    });    
    
    /*Case Management */
    const caseFormData = new FormData();
    selectedCaseFile.forEach((files,i) => {
      caseFormData.append('files', files[0]);
    });

    /*Other Trusted */
    const otherData = new FormData();
    otherData.append('files', selectedOtherFile[0])

    const { otherName, caseMgmtName, riskName } = this.state;
    if (id === 1) {
      const trustedCert = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['trustedCertificates']}?actionId=${id}&storeName=${riskName}`,
        data: fd
      };
      const successData = await getService(trustedCert);
      if (successData && successData.status === 200) {
        this.setState({
          formHide:false
        })
        this.props.activateSuccessList(true, successData.data);
        this.props.activateErrorList(false, '');
      } else {
        this.setState({
          formHide:true
        })
        this.props.activateErrorList(true, successData.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    } else if (id === 2) {
      const trustedCert = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['trustedCertificates']}?actionId=${id}&storeName=${caseMgmtName}`,
        data:caseFormData

      };
      const successData = await getService(trustedCert);
      if (successData && successData.status === 200) {
        this.setState({
          formHide:false
        })
        this.props.activateSuccessList(true, successData.data);
        this.props.activateErrorList(false, '');
      } else {
        this.setState({
          formHide:true
        })
        this.props.activateErrorList(true, successData.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    } else {
      const trustedCert = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['trustedCertificates']}?actionId=${id}&storeName=${otherName}`,
        data: otherData
      };
      const successData = await getService(trustedCert);
      if (successData && successData.status === 200) {
        this.setState({
          formHide:false
        })
        this.props.activateSuccessList(true, successData.data);
        this.props.activateErrorList(false, '');
      } else {
        this.setState({
          formHide:true
        })
        this.props.activateErrorList(true, successData.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
  };

  componentWillUnmount(){
    this.props.activateSuccessList(false, '');
    this.props.activateErrorList(false, '');
  }

  render() {
    const { otherName, riskName, caseMgmtName } = this.state;
    const fileInputs = this.state.loopdocuments.map((Element, index) => {
      return <DocumentInput key={index} index={index} changeData={this.handleFileChange} index={index} />;
    });
    const fileCaseInputs = this.state.loopdoc.map((Element, index) => {
      return <DocumentInput key={index} index={index} changeData={this.handleCaseFileChange} index={index} />;
    });
    return (
      <div>
        {this.state.formHide ? (
              <div className='main'>
              {/* Risk Analytics  */}
              <h2 className='title'>{RA_STR.trustedTitle}</h2>
              <p className='desc'>{RA_STR.trustedUpload}</p>
              <div className='col-sm-5'>
                <SingleInput
                  title={RA_STR.trustedName}
                  inputType={'text'}
                  name={'riskName'}
                  content={riskName}
                  controlFunc={this.handleInputChange}
                />
                <div className='form-group row'>
                  <label className='col-sm-4 col-form-label'>{RA_STR.root}</label>
                  <div className='col-md-8'>
                    {fileInputs}
                    <input type='button' className='custom-secondary-btn' onClick={this.add} value='Add More' />
                  </div>
                </div>
                <div className='form-group form-submit-button row'>
                  <input
                    className='secondary-btn'
                    id='riskButton'
                    type='submit'
                    value='SAVE'
                    onClick={() => this.onSave(1)}
                  ></input>
                </div>
              </div>
      
              {/* Case Management Server  */}
              <h2 className='title'>{RA_STR.caseMgmnt}</h2>
              <p className='desc'>{RA_STR.uploadMul}</p>
              <div className='col-sm-5'>
                <SingleInput
                  title={RA_STR.trustedName}
                  inputType={'text'}
                  name={'caseMgmtName'}
                  content={caseMgmtName}
                  controlFunc={this.handleInputChange}
                />
                <div className='form-group row'>
                  <label className='col-sm-4 col-form-label'>{RA_STR.root}</label>
                  <div className='col-md-8'>
                    {fileCaseInputs}
                    <input type='button' className='custom-secondary-btn' onClick={this.addCase} value='Add More' />
                  </div>
                </div>
                <div className='form-group form-submit-button row'>
                  <input
                    className='secondary-btn'
                    id='caseButton'
                    type='submit'
                    value='SAVE'
                    onClick={() => this.onSave(2)}
                  ></input>
                </div>
              </div>
      
              {/* Other Trusted */}
              <h2 className='title'>{RA_STR.trustedAuth}</h2>
              <p className='desc'>{RA_STR.addCertificate}</p>
              <div className='col-sm-5'>
                <SingleInput
                  title={RA_STR.trustedName}
                  inputType={'text'}
                  name={'otherName'}
                  controlFunc={this.handleInputChange}
                  content={otherName}
                />
                <div className='form-group row'>
                  <label className='col-sm-4 col-form-label'>{RA_STR.root}</label>
                  <div className='col-md-8'>
                    <input
                      type='file'
                      id='input-field'
                      className='dynaic-file-input'
                      name={`trustCert`}
                      onChange={this.handleOtherFileChange}
                    />
                  </div>
                </div>
                <div className='form-group form-submit-button row'>
                  <input
                    className='secondary-btn'
                    id='caseButton'
                    type='submit'
                    value='SAVE'
                    onClick={() => this.onSave(3)}
                  ></input>
                </div>
              </div>
            </div>
        ) : (
          <div></div>
          )}
      </div>
      
    );
  }
}

export default TrustedCertificate;
