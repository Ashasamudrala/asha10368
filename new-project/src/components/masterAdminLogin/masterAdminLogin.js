import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl,RA_API_URL } from '../../shared/utlities/constants';
import './masterAdminLogin.css';
import Footer from '../Footer';
const adminUrl = "/auth/login";
class masterAdminLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginError: false,
      orgError: false,
      usernameError: false,
      passwordError: false,
      orgName: 'masteradmin',
      username: '',
      password: '',
      userAuth: false,
    };
  }

  handleCredentials = (e, msg) => {
    if (msg === "username") {
      this.setState({ username: e.target.value });
      if (e.target.value.length) {
        this.setState({ usernameError: false });
      } else {
        this.setState({ usernameError: true });
      }
    } else if (msg === "password") {
      this.setState({ password: e.target.value });
      if (e.target.value.length) {
        this.setState({ passwordError: false });
      } else {
        this.setState({ passwordError: true });
      }
    }
  }
  handlekeyDown = (e) => {
    if (e.key === 'Enter') {
      this.onRedirectNext();
    }
  }
  keyPress = (e) => {
    if (e.keyCode === 13 && this.state.username && this.state.password) {
      this.onRedirectNext();
    }
  }
  onRedirectNext = async () => {
    const match = this.props.match;
    if (this.state.username && this.state.password && match.path === "/masteradminconsole") {
      const checkAdmin = {
        method: 'POST',
        url: `${serverUrl}${adminUrl}`,
        data: {
          "orgName": this.state.orgName,
          "userName": this.state.username,
          "userPassword": this.state.password
        }
      };
      const checkAdminStatus = await getService(checkAdmin);
      if (checkAdminStatus.status === 200) {
        localStorage.setItem('profiledata', JSON.stringify(checkAdminStatus.data));
        localStorage.setItem('userName', JSON.stringify(checkAdminStatus.data.userProfile.userName));
        window.location.href = (process.env.REACT_APP_ROUTER_BASE || '') + RA_API_URL['usersUrl'];
      } else{
        if (this.refs) {
          this.refs.userPassword.value = '';
        }
        this.setState({ userAuth: true });
      }
    } else {
      if (this.state.username !== '' && this.state.password !== '' && match.path !== "/masteradminconsole") {
        this.setState({ userAuth: true });
        setTimeout(() => {
          this.setState({ userAuth: false });
        }, 3000);
        return;
      }
      if (this.state.username === '') {
        this.setState({ usernameError: true });
      }
      if (this.state.password === '') {
        this.setState({ passwordError: true });
      }
      return
    }
  }
  keyPress = (e) => {
    if (e.keyCode === 13 && this.state.username && this.state.password) {
      this.onRedirectNext();
    }
  }
  render() {
    const { usernameError, passwordError, userAuth } = this.state;
    return (
      <div className="login-parent">
        <div className="eupopup-parent">
        </div>
        <div className="wrapper">
          <div className="">
            <div className="clearfloat edl-login-box">
              <div id="brand-holder">
                <div>
                  <img src="images/ca-logo-new.png" alt="caLogo" height="50"></img>
                  <p className="ecc-h1">Administration Console</p>
                </div>
              </div>
              <div id="form-holder">
                <div className="form-header" id="signin-header">SIGN IN</div>
                <form name="bamForm" method="post" >
                </form>
                <div id="loginbox">
                  <div>
                    <div className="body-font">Username</div>
                    <div className="edl_login_input">
                      <input name="userName"
                        title={`${!this.state.username.length ? 'Please fill out this field.' : ''}`}
                        className={`loginTextBox ${usernameError ? ' error' : ''}`}
                        autoComplete="off" type="text"
                        onKeyDown={this.handlekeyDown}
                        onChange={e => this.handleCredentials(e, "username")}
                        autoFocus />
                      <div className={`${usernameError ? 'error-msg' : 'no-msg'}`}>Please enter username.</div>
                    </div>
                    <div className="body-font">Password</div>
                    <div className="edl_login_input">
                      <input name="userPassword" 
                        title={`${!this.state.password.length ? 'Please fill out this field.' : ''}`}
                        ref="userPassword" className={`loginTextBox ${passwordError ? ' error' : ''}`}
                        autoComplete="off" type="password"
                        onKeyDown={this.handlekeyDown}
                        onChange={e => this.handleCredentials(e, "password")} />
                      <div className={`${passwordError ? 'error-msg' : 'no-msg'}`}>Please enter password.</div>
                    </div>
                    <div className={`${userAuth ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">Authentication failed.</div>
                  </div>
                  <input className="button edl_btn_main left_align margin-top-8 user-sign-in" onClick={this.onRedirectNext} type="submit" value="SIGN IN" />
                </div>
                <br className="clearfloat" />
                <br />
                <div className="eupopup"></div>

              </div>
            </div>
          </div>
          <div className="push">
          </div>

        </div>
        <Footer/>
      </div>
    );
  }
}

export default masterAdminLogin;
