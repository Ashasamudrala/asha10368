// npm dependencies
import React, { Component } from 'react';
import { saveAs } from 'file-saver';

// Service imports
import { getService } from '../../shared/utlities/RestAPI';

// Component dependencies
import Select from '../../shared/components/Select/Select';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import InlineDateRange from '../../shared/InlineDateRange/InlineDateRange';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';

// Internal Dependency imports
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl, RA_API_URL, RA_API_STATUS } from '../../shared/utlities/constants';

// Style imports
import './ExceptionUserReport.scss'

/**
 * Class component: This component is for displaying and exporting exception user reports.
 */
class ExceptionUserReport extends Component {
  /**
    * Constructor for exception User report Component
    * @class
    * @param 				{Object} 	props		Initialization props
    * @description		It initialises the state, from props
    */
  constructor(props) {
    super(props);
    // initializing the local state
    this.state = {

      // Variables for storing form values
      userId: '',
      orgName: '',
      decryptionNeeded: false,
      // Variables for storing dropdown data
      organizationData: [],
      reportData: [],
      columns: [
        { title: 'Start Date', field: 'startDate' },
        { title: 'End Date', field: 'endDate' },
        { title: 'Username', field: 'userName' },
        { title: 'Reason', field: 'reason' },
        { title: 'Organization', field: 'orgName' }
      ],
      dates: {},
      currentStep: 1,
    };
  }

  /**
   * componentDidMount is invoked immediately after this component is mounted, so all initialization goes here.
   * @description   This method contains the get call for categories.
   */
  componentDidMount() {
    this.getOrganizations();
  };

  /**
   * componentWillUnmount is invoked immediately before a component is unmounted and destroyed.
   * @description   This method takes back the error/success message to initial state.
   */
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  /**
  * Handler method to handle user input.
  * @param 	{*}		e 	The HTML event generated by the form.
  * @description				It handles the change in dropdown values.
  */
  handleChange = async (e) => {
    if (e.target.name === "orgName") {
      this.setState({ orgName: e.target.value });
    } else if (e.target.name === 'decryptionNeeded') {
      this.setState({ decryptionNeeded: e.target.checked });
    } else {
      this.setState({ [e.target.name]: e.target.value })
    }
  }

  /**
   * @description   This method gets all the categories.
   */
  getOrganizations = async () => {
    // Variable that contains method,url declaration that goes to backend.
    const getDefaultOrganizations = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getDefaultOrgs']}`
    };
    // Catching backend response from getService method.
    const response = await getService(getDefaultOrganizations);
    if (response && response.status === RA_API_STATUS['200']) {
      // Converts the object to array.
      const result = CA_Utils.objToArray(response.data.organizations, 'object');
      // Updating the local state.
      this.setState({ organizationData: result });
    }
  }

  /**
  * @description   This method displays all the reports.
  */
  displayReports = async () => {
    // Disabling success/error messages.
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    const date = this.refs.daterange.getDates();
    this.setState({ dates: date[0] });
    // Variable that contains method,url,payload declaration that goes to backend.
    const exceptionUserReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['exceptionUserReport'] + 'display'}`,
      data: {
        "adminPreferredTimezone": "GMT",
        "orgName": this.state.orgName,
        "userId": this.state.userId,
        "fromDate": date[0].startDate,
        "toDate": date[0].endDate,
        "decryptionNeeded": this.state.decryptionNeeded
      }
    }
    // Catching backend response from getService method.
    const response = await getService(exceptionUserReport);
    // Success case handling
    if (response && response.status === RA_API_STATUS['200'] && response.data) {
      this.setState({ currentStep: 2, reportData: response.data.reportList });
      // Activates the success message list.
      this.props.activateSuccessList(true, response.data);
      // Activates the error message list.
      this.props.activateErrorList(false, '');
      window.scrollTo(0, 0);
    } else if (response && response.status === RA_API_STATUS['400'] && response.data.errorList) {
      // Activates the error message list.
      this.props.activateErrorList(true, response.data.errorList);
      // Activates the success message list.
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  }

  /**
   * @description   This method exports all the reports.
   */
  exportReport = async () => {
    // Disabling success/error messages.
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    // Variable that contains method,url,payload declaration that goes to backend.
    const exportExceptionUserReport = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['exceptionUserReport'] + 'export'}`,
      data: {
        "adminPreferredTimezone": "GMT",
        "orgName": this.state.orgName,
        "userId": this.state.userId,
        "fromDate": this.state.dates.startDate,
        "toDate": this.state.dates.endDate,
        "decryptionNeeded": this.state.decryptionNeeded
      }
    }
    // Catching backend response from getService method.
    const response = await getService(exportExceptionUserReport);
    // Success case handling
    if (response && response.status === RA_API_STATUS['200'] && response.data) {
      // Setting the file type and saving as blob format.
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'Exception_User_Report.csv');
      // Activates the success message list.
      this.props.activateSuccessList(true, response.data);
      // Activates the error message list.
      this.props.activateErrorList(false, '');
      window.scrollTo(0, 0);
    } else if (response && response.data.errorList) {
      // Activates the error message list.
      this.props.activateErrorList(true, response.data.errorList);
      // Activates the success message list.
      this.props.activateSuccessList(false, '');
      window.scrollTo(0, 0);
    }
  }

  /**
   * @description   This method takes the user to initial state.
   */
  newReport = () => {
    this.setState({ currentStep: 1, orgName: '', dates: {} });
  }

  render() {
    // Gets all the required variables from local state.
    const { organizationData, orgName, currentStep, dates, columns, reportData, decryptionNeeded } = this.state;
    return (
      <div className='main exception-user-report-container'>
        <h2 className='title'>{RA_STR.EXCEPTION_USER_REPORT.TITLE}</h2>
        {
          (currentStep == 1)
            ? <div>
              <p className='desc'>{RA_STR.EXCEPTION_USER_REPORT.DESCRIPTION}</p>
              <hr />
              <div className='col-sm-6'>
                <Select
                  name={'orgName'}
                  title={'Organization Name'}
                  options={organizationData}
                  controlFunc={this.handleChange}
                  placeholder={'Select'} />
                {
                  (orgName)
                    ? <div className='row'>
                      <div className="col-sm-12">
                        <SingleInput
                          title={'Enter User Identification'}
                          inputType={'text'}
                          name={'userId'}
                          controlFunc={this.handleChange}
                        />
                      </div>
                    </div>
                    : null
                }
              </div>
              <div className='col-sm-12'>
                {
                  (orgName)
                    ? <div className='row'>
                      <div className="col-sm-2">
                        <label>{RA_STR.EXCEPTION_USER_REPORT.TIME_PERIOD_LABEL}</label>
                      </div>
                      <div className="col-sm-10">
                        <InlineDateRange ref="daterange" />
                      </div>
                    </div>
                    : null
                }
              </div>
              <div className='col-sm-6'>
                {
                  (orgName)
                    ? <div className='row'>
                      <div className="col-sm-6">
                        <Checkbox
                          type={'checkbox'}
                          setName={'decryptionNeeded'}
                          checked={decryptionNeeded}
                          controlFunc={this.handleChange}
                          options={["Decrypt Sensitive Information"]} />
                      </div>
                      <div className="col-sm-6">
                        <input className="secondary-btn ml-3" type="submit" value="DISPLAY REPORT" onClick={this.displayReports}></input>
                      </div>
                    </div>
                    : null
                }
              </div>
            </div>
            : <div>
              <span className='desc'>{RA_STR.EXCEPTION_USER_REPORT.DESCRIPTION1}</span>
              <span className="export-btn">
                <input className="secondary-btn ml-3" type="submit" value="EXPORT" onClick={this.exportReport} />
              </span>
              <span>
                <input className="secondary-btn ml-3" type="submit" value="NEW REPORT" onClick={this.newReport} />
              </span>
              <div className="form-group row ">
                <label className="col-sm-2 ">{RA_STR.EXCEPTION_USER_REPORT.ORGANIZATION_LABEL}</label>
                <label className="col-sm-8"> {orgName}</label>
              </div>
              <div className="form-group row">
                <label className="col-sm-2 ">{RA_STR.EXCEPTION_USER_REPORT.FROM_LABEL} {dates.startDate}</label>
                <label className="col-sm-8">{RA_STR.EXCEPTION_USER_REPORT.TO_LABEL} {dates.endDate}</label>
              </div>
              <DynamicTable columns={columns} data={reportData} />
            </div>
        }

      </div>
    )
  }
}

// Default export
export default ExceptionUserReport;