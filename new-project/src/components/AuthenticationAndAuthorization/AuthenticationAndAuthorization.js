import React, { Component } from 'react';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import { RA_STR_SELECTOPTION, RA_STR_OPTIONDETAILS, RA_API_URL,serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
class AuthenticationAndAuthorization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectAttributesDisplay: true,
      selectOptions: [],
      optionDetails: [],
    }
  }
  componentDidMount = async () => {
    this.getWebServices();
  }
  getWebServices = async () => {
    let multiselectObject = {};
    const data={
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['fetchWSAuthPrivileges']}`,
    }
    const fetchWSAuthPrivilegesStatus = await getService(data);
    if (fetchWSAuthPrivilegesStatus.status === 200) {
      let authPrviliges = fetchWSAuthPrivilegesStatus.data;
      Object.keys(authPrviliges.availableResources).forEach(function (key) {
        authPrviliges.availableResources[key].headerName = authPrviliges.availableResources[key].parentKey;
        authPrviliges.availableResources[key].privilegeId = authPrviliges.availableResources[key].resourceId;
        delete authPrviliges.availableResources[key].parentKey;
        delete authPrviliges.availableResources[key].resourceId;
      });
      Object.keys(authPrviliges.selectedResources).forEach(function (key) {
        authPrviliges.selectedResources[key].headerName = authPrviliges.selectedResources[key].parentKey;
        authPrviliges.selectedResources[key].privilegeId = authPrviliges.selectedResources[key].resourceId;
        delete authPrviliges.selectedResources[key].parentKey;
        delete authPrviliges.selectedResources[key].resourceId;
      });
      let obj = []
      obj = [...authPrviliges.availableResources, ...authPrviliges.selectedResources];
      this.getPrivileges(authPrviliges.availableResources, RA_STR_OPTIONDETAILS);
      this.getPrivileges(authPrviliges.selectedResources, RA_STR_SELECTOPTION);
      obj = CA_Utils.swapSorting(obj);
      multiselectObject = CA_Utils.objectFormation(obj);
      this.setState({ emptyList: multiselectObject['selectOptions'], optDetails: multiselectObject['data'] });
    }
  }
  getPrivileges = (turnedOffPrivileges, optionValue, rolesMenu) => {
    let multiselectObject = CA_Utils.objectFormation(turnedOffPrivileges);
    if (optionValue === RA_STR_SELECTOPTION) {
      this.setState({ selectOptions: multiselectObject['optionDetails'], optDetails: multiselectObject['data'] });
    }
    else {
      this.setState({ optionDetails: multiselectObject['optionDetails'], emptyList: multiselectObject['selectOptions'], roleBased: rolesMenu, options: multiselectObject['data'] });
    }
  }
  handleLeftChange = (node) => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let selectedValues = CA_Utils.getselectedValues(node);
    let multiselectObject = {};
    if (selectedValues.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handleLeftChange(optionDetails, selectOptions, 'update', node);
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  handleRightChange = (node) => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let selectedValues = CA_Utils.getselectedValues(node);
    let multiselectObject = {};
    if (selectedValues.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handleRightChange(optionDetails, selectOptions, 'update', node);
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  setSelectedOptionObject = (selectOptions) => {
    let emptyList = JSON.parse(JSON.stringify(this.state.emptyList));
    return CA_Utils.setSelectedOptionObject(selectOptions, emptyList);
  }
  setSelectedLeftObject = (optionDetails) => {
    let { options } = this.state;
    let selectedOptions = CA_Utils.setSelectedLeftObject(optionDetails, options);
    return selectedOptions;
  }
  setSelectedRightObject = (selectOptions) => {
    let { optDetails } = this.state;
    let selectedOptions = CA_Utils.setSelectedRightObject(selectOptions, optDetails);
    return selectedOptions;
  }
  handledoubleRightChange = () => {
    let multiselectObject = {};
    let { optionDetails, options, selectOptions, optDetails } = this.state;
    if (this.state.optionDetails.length !== 0) {
      selectOptions = this.setSelectedOptionObject(selectOptions);
      optionDetails = this.setSelectedOptionObject(optionDetails);
      multiselectObject = CA_Utils.handledoubleRightChange(optionDetails, selectOptions, 'update');
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
      this.setState(multiselectObject);
    }
  }
  handledoubleLeftChange = () => {
    let { optionDetails, selectOptions, optDetails, options } = this.state;
    let multiselectObject = {};
    if (this.state.selectOptions.length !== 0) {
      optionDetails = this.setSelectedOptionObject(optionDetails);
      selectOptions = this.setSelectedOptionObject(selectOptions);
      multiselectObject = CA_Utils.handledoubleLeftChange(optionDetails, selectOptions, 'update');
      let optionObj = this.setSelectedLeftObject(multiselectObject['optionDetails'], options);
      multiselectObject['optionDetails'] = optionObj['optionDetails'];
      multiselectObject['options'] = optionObj['options'];
      optionObj = this.setSelectedRightObject(multiselectObject['selectOptions'], optDetails);
      multiselectObject['selectOptions'] = optionObj['selectOptions'];
      multiselectObject['optDetails'] = optionObj['optDetails'];
      this.setState(multiselectObject);
    }
  }
  saveWebService = async () => {
    let webServicesUpdate = [];
    const { selectOptions } = this.state;
    Object.keys(selectOptions[0]).map((eachKey) => {
      selectOptions[0][eachKey].map((geteach) => {
        geteach.parentKey = geteach.headerName;
        geteach.resourceId = geteach.privilegeId;
        delete geteach.headerName;
        delete geteach.privilegeId;
        webServicesUpdate.push(geteach);
      })
    });
    const dataPayload = {
      "selectedResources": webServicesUpdate
    }
    const data={
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['saveWSAuthPrivileges']}`,
      data:dataPayload
    }
    const fetchWSAuthPrivilegesStatus = await getService(data);
    if (fetchWSAuthPrivilegesStatus.status === 200) {
      this.props.activateSuccessList(true, fetchWSAuthPrivilegesStatus.data);
      this.props.activateErrorList(false, '');
      this.getWebServices();
    } else {
      this.props.activateErrorList(true, fetchWSAuthPrivilegesStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
 
  render() {
    return <div className="main">
      <h2 className="title">{RA_STR.webServiceTitle}</h2>
      <p className="desc">{RA_STR.webServiceText}</p>
      <p className="desc" dangerouslySetInnerHTML={{ __html: RA_STR.webServiceBody }}></p>
      <div className="col-md-12 form-group row">
        <label className="col-md-3 col-form-label vertical-center">Web Services</label>
        <div className="col-md-9">
          <SwappingSelectBox selectOptions={this.state.selectOptions}
            optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={true} />
        </div>
      </div>
      <div className="form-group form-submit-button row">
        <input className="secondary-btn" id="encryButton" type="submit" value="Save" onClick={this.saveWebService}></input>
      </div>
    </div>;
  }
}
export default AuthenticationAndAuthorization;
