import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_API_STATUS } from '../../shared/utlities/constants';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';


class EndpointManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryData: [],
      endPointsData: [],
      endPointApiData: [],
      certsData: [],
      selectedEndPointMetaData: {},
      queueMetaData: [],
      protocolMetaData: [{ 'key': 'tcp', 'content': 'TCP' }, { 'key': 'ssl', 'content': 'SSL' }],
      category: '',
      endPoint: '',
      queueName: '',
      protocol: 'tcp',
      host: '',
      port: '',
      readTimeout: 10000,
      saveValueQueue: 0,
      seqId: 0,
      maxConns: 5,
      minConns: 3,
      certId: 0,
      connTimeout: 30000,
      isCategorySelected: false,
      isAddClicked: false,
      isEndPointSelected: false,
      testButtonEnable: false
    };
  }
  componentDidMount() {
    this.getCategories();
    this.getCerts();
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  handleCategoryChange = e => {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    if (e.target.value) {
      if (e.target.value == 'BROKER' || e.target.value == 'DATAFEED_ENDPOINT') {
        this.setState({
          testButtonEnable: true
        })
      } else {
        this.setState({
          testButtonEnable: false
        })
      }
      this.setState({
        category: e.target.value,
        isCategorySelected: true,
        isAddClicked: false,
        selectedEndPointMetaData: {},
        isEndPointSelected: false
      });
      this.getEndPoints(e.target.value);
    } else {
      this.setState({
        category: '',
        isCategorySelected: false,
        isEndPointSelected: false,
        isAddClicked: false
      });
    }
  };

  handleEndPointChange = e => {
    if (e.target.value) {
      if (this.state.isAddClicked) {
        this.setState({
          endPoint: e.target.value,
          isCategorySelected: true
        })
      } else {
        this.getQueues(this.state.category);
        const selectedEndPointData = this.state.endPointApiData.find(element => element.endpointName == e.target.value);
        this.setState({
          endPoint: e.target.value,
          isCategorySelected: true,
          isEndPointSelected: true,
          isAddClicked: false,
          protocol: selectedEndPointData.protocol,
          host: selectedEndPointData.hostname,
          port: selectedEndPointData.port,
          minConns: selectedEndPointData.minConns,
          maxConns: selectedEndPointData.maxConns,
          connTimeout: selectedEndPointData.connTimeout,
          readTimeout: selectedEndPointData.readTimeout,
          selectedEndPointMetaData: selectedEndPointData,
          seqId: selectedEndPointData.seqId
        });
      }
    } else {
      this.setState({
        endPoint: '',
        isEndPointSelected: false
      })
    }
  };

  handleFormChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleCancel = () => {
    window.location.reload();
  }

  getCategories = async () => {
    const getEndPointCategories = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getCategories']}`
    };
    const response = await getService(getEndPointCategories);
    if (response.status == 200) {
      let fetchedCategoryData = [];
      response.data.forEach(element => {
        fetchedCategoryData.push({
          key: element,
          content: element
        });
      });
      this.setState({
        categoryData: fetchedCategoryData,
      });
    }
  };

  getQueues = async (endPoint) => {
    const queuesURL = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getEndPointsURL1']}${endPoint}${RA_API_URL['getQueuesURL']}`
    };
    const response = await getService(queuesURL);
    if (response && response.status === 200) {
      this.setState({ queueName: response.data['Queue Name'] })
    }
  };

  getCerts = async () => {
    const getCertsURL = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getCerts']}`
    };
    const response = await getService(getCertsURL);
    this.setState({ certsData: CA_Utils.objToArray(response.data, 'object') });
  };

  getEndPoints = async (endPoint) => {
    const endPointsURL = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getEndPointsURL1']}${endPoint}${RA_API_URL['getEndPointsURL2']}`
    };
    const response = await getService(endPointsURL);
    let endPointsMetaData = [];
    response.data.forEach(element => {
      endPointsMetaData.push({
        key: element.endpointName,
        content: element.endpointName
      });
    });
    this.setState({ endPointsData: endPointsMetaData, endPointApiData: response.data });
  };

  addEndPoint = (e) => {
    e.preventDefault();
    this.getQueues(this.state.category);
    this.getCerts();
    this.setState({ isAddClicked: true, port: '61616', endPoint: '', host: '' });
  }

  testEndPoint = async () => {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    if (this.state.minConns == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.MIN_CONN_ALERT);
    } else if (this.state.maxConns == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.MAX_CONN_ALERT);
    } else if (this.state.connTimeout == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.CONN_TIMEOUT_ALERT);
    } else if (this.state.readTimeout == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.READ_TIMEOUT_ALERT);
    } else {
      const testURL = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['testEndPoint']}`,
        data: {
          category: this.state.category,
          certId: this.state.certId,
          connTimeout: this.state.connTimeout,
          endpointName: this.state.endPoint,
          hostname: this.state.host,
          maxConns: this.state.maxConns,
          minConns: this.state.minConns,
          port: this.state.port,
          protocol: this.state.protocol,
          queue: this.state.queueName,
          readTimeout: this.state.readTimeout,
          saveValueQueue: this.state.saveValueQueue,
          seqId: this.state.seqId
        }
      };
      const response = await getService(testURL);
      if (response && response.status === RA_API_STATUS['200'] && response.data) {
        this.props.activateSuccessList(true, response.data);
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, response.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
  };

  saveEndPoint = async () => {
    this.props.activateErrorList(false, '');
      this.props.activateSuccessList(false, '');
    if (this.state.minConns == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.MIN_CONN_ALERT);
    } else if (this.state.maxConns == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.MAX_CONN_ALERT);
    } else if (this.state.connTimeout == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.CONN_TIMEOUT_ALERT);
    } else if (this.state.readTimeout == '') {
      alert(RA_STR.ENDPOINT_MANAGEMENT.READ_TIMEOUT_ALERT);
    } else {
      const saveURL = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['saveEndPointDetails']}`,
        data: {
          category: this.state.category,
          certId: this.state.certId,
          connTimeout: this.state.connTimeout,
          endpointName: this.state.endPoint,
          hostname: this.state.host,
          maxConns: +this.state.maxConns,
          minConns: +this.state.minConns,
          port: this.state.port,
          protocol: this.state.protocol,
          queue: this.state.queueName,
          readTimeout: this.state.readTimeout,
          saveValueQueue: this.state.saveValueQueue,
          seqId: this.state.seqId
        }
      };
      const response = await getService(saveURL);
      if (response && response.status === RA_API_STATUS['200'] && response.data) {
        this.props.activateSuccessList(true, response.data);
        this.props.activateErrorList(false, '');
        this.setState({
          certId: this.state.certId,
          connTimeout: this.state.connTimeout,
          endpointName: '',
          hostname: this.state.host,
          maxConns: +this.state.maxConns,
          minConns: +this.state.minConns,
          port: this.state.port,
          protocol: this.state.protocol,
          queue: '',
          readTimeout: this.state.readTimeout,
          saveValueQueue: this.state.saveValueQueue,
          seqId: this.state.seqId
        })
      } else {
        this.props.activateErrorList(true, response.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
  };

  render() {
    const { category } = this.state;
    return (
      <div className='main'>
        <h2 className='title'>{RA_STR.ENDPOINT_MANAGEMENT.TITLE}</h2>
        <p className='desc'>{RA_STR.ENDPOINT_MANAGEMENT.DESCRIPTION}</p>
        <div className='col-sm-6'>
          <Select
            name={'category'}
            title={'Category'}
            options={this.state.categoryData}
            placeholder={'Select'}
            controlFunc={this.handleCategoryChange}
          />
          {
            (category && !this.state.isAddClicked)
              ? <Select
                name={'endPoint'}
                title={'Endpoint Name'}
                options={this.state.endPointsData}
                placeholder={'Select'}
                controlFunc={this.handleEndPointChange}
              />
              : null
          }
          {
            (category && this.state.isAddClicked)
              ? <SingleInput
                title={'Endpoint Name'}
                required={true}
                inputType={'text'}
                controlFunc={this.handleEndPointChange}
                name={'endPoint'}
              />
              : null
          }
          {
            (this.state.isCategorySelected && !this.state.isAddClicked && !this.state.isEndPointSelected)
              ? <input className="secondary-btn ml-3" type="submit" value="ADD" onClick={this.addEndPoint} />
              : null
          }
          {
            (category && this.state.isAddClicked && !this.state.isEndPointSelected)
              ? <div>
                {
                  (this.state.category == 'BROKER')
                    ? <SingleInput
                      title={'Queue Name'}
                      required={false}
                      inputType={'text'}
                      content={this.state.queueName}
                      controlFunc={this.handleFormChange}
                      name={'queueName'}
                      readOnly
                    />
                    : null
                }
                <Select
                  name={'protocol'}
                  title={'Protocol'}
                  options={this.state.protocolMetaData}
                  controlFunc={this.handleFormChange}
                />
                <SingleInput
                  title={'Host'}
                  required={true}
                  inputType={'text'}
                  controlFunc={this.handleFormChange}
                  name={'host'}
                />
                <SingleInput
                  title={'Port'}
                  required={true}
                  inputType={'number'}
                  content={this.state.port}
                  controlFunc={this.handleFormChange}
                  name={'port'}
                />

                {
                  (this.state.category == 'BROKER')
                    ? null
                    : <div>
                      <div className='summary'>{RA_STR.ENDPOINT_MANAGEMENT.CONNECTION_POOL_LABEL}</div>
                      <div className='row'>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6">{RA_STR.ENDPOINT_MANAGEMENT.MIN_CONNECTIONS_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="minConns"
                                defaultValue={this.state.minConns || 3} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6 col-form-label ml-6">{RA_STR.ENDPOINT_MANAGEMENT.MAX_CONNECTIONS_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="maxConns"
                                defaultValue={this.state.maxConns || 5} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6">{RA_STR.ENDPOINT_MANAGEMENT.CONN_TIMEOUT_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="connTimeout"
                                defaultValue={this.state.connTimeout || 30000} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6 col-form-label ml-6">{RA_STR.ENDPOINT_MANAGEMENT.READ_TIMEOUT_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="readTimeout"
                                defaultValue={this.state.readTimeout || 10000} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                }
                {
                  (this.state.protocol == 'ssl')
                    ? <div><br />
                      <div className='summary'>{RA_STR.ENDPOINT_MANAGEMENT.SSL_CONFIG_LABEL}</div><br />
                      <Select
                        name={'certId'}
                        title={'Server Root Certificate'}
                        options={this.state.certsData || []}
                        placeholder={'Select'}
                        controlFunc={this.handleFormChange}
                      />
                      <Checkbox
                        type={'checkbox'}
                        options={['Two Way SSL']}
                        controlFunc={this.onchange}
                        selectedOptions={['Two Way SSL']}
                        readOnly
                      />
                    </div>
                    : null
                }
              </div>
              : null
          }
          {
            (this.state.isEndPointSelected)
              ? <div>
                {
                  (this.state.category == 'BROKER')
                    ? <SingleInput
                      title={'Queue Name'}
                      required={false}
                      inputType={'text'}
                      content={this.state.queueName}
                      controlFunc={this.handleFormChange}
                      name={'queueName'}
                      readOnly
                    />
                    : null
                }
                <Select
                  name={'protocol'}
                  title={'Protocol'}
                  options={this.state.protocolMetaData}
                  selectedOption={this.state.protocol}
                  controlFunc={this.handleFormChange}
                />
                <SingleInput
                  title={'Host'}
                  required={true}
                  inputType={'text'}
                  controlFunc={this.handleFormChange}
                  content={this.state.host}
                  name={'host'}
                />
                <SingleInput
                  title={'Port'}
                  required={true}
                  inputType={'number'}
                  controlFunc={this.handleFormChange}
                  content={this.state.port}
                  name={'port'}
                />
                {
                  (this.state.category == 'BROKER')
                    ? null
                    : <div>
                      <div className='summary'>{RA_STR.ENDPOINT_MANAGEMENT.CONNECTION_POOL_LABEL}</div>
                      <div className='row'>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6">{RA_STR.ENDPOINT_MANAGEMENT.MIN_CONNECTIONS_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="minConns"
                                defaultValue={this.state.minConns} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6 col-form-label ml-6">{RA_STR.ENDPOINT_MANAGEMENT.MAX_CONNECTIONS_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="maxConns"
                                defaultValue={this.state.maxConns} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6">{RA_STR.ENDPOINT_MANAGEMENT.CONN_TIMEOUT_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="connTimeout"
                                defaultValue={this.state.connTimeout} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="col-sm-6 col-form-label ml-6">{RA_STR.ENDPOINT_MANAGEMENT.READ_TIMEOUT_LABEL}</label>
                            <div className="col-sm-6">
                              <input type="text" className="form-control"
                                name="readTimeout"
                                defaultValue={this.state.readTimeout} onChange={this.handleFormChange} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                }
                {
                  (this.state.protocol == 'ssl')
                    ? <div><br />
                      <div className='summary'>{RA_STR.ENDPOINT_MANAGEMENT.SSL_CONFIG_LABEL} </div><br />
                      <Select
                        name={'certId'}
                        title={'Server Root Certificate'}
                        options={this.state.certsData || []}
                        placeholder={'Select'}
                        controlFunc={this.handleFormChange}
                      />
                      <Checkbox
                        type={'checkbox'}
                        options={['Two Way SSL']}
                        controlFunc={this.onchange}
                        selectedOptions={['Two Way SSL']}
                        readOnly
                      />
                    </div>
                    : null
                }
              </div>
              : null
          }
          {
            this.state.isEndPointSelected || this.state.isAddClicked
              ? <div className='col-sm-12 form-submit-button'>
                <div className='row'>
                  {
                    (this.state.testButtonEnable)
                      ? <div className='col-sm-3'>
                        <input className='secondary-btn' type='submit' value='TEST' onClick={this.testEndPoint} />
                      </div>
                      : null
                  }
                  <div className='col-sm-3'>
                    <input className='secondary-btn' type='submit' value='SAVE' onClick={this.saveEndPoint} />
                  </div>
                  <div className='col-sm-3'>
                    <input className="custom-secondary-btn" type="button" value="CANCEL" onClick={this.handleCancel} />
                  </div>
                </div>
              </div>
              : null
          }
        </div>
      </div>
    );
  }
}

export default EndpointManagement;
