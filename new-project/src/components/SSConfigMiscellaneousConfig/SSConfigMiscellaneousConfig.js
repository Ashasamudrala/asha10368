import React, { Component, Fragment } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_PROFILEDATA, RA_API_SSCONFIG_MISSCONFIG } from '../../shared/utlities/constants';
import './SSConfigMiscellaneousConfig.css';

class SSConfigMiscellaneousConfig extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            allPoperties: [],
            properties: [],
            channelId: 0,
        }
    }

    componentDidMount() {
        this.getChannelData();
    }

    getChannelData = async () => {
        
            const beneficiary = {
                method: 'GET',
                url: `${serverUrl}${RA_API_URL.getOrgUrl}s${RA_API_SSCONFIG_MISSCONFIG.riskfortConfig}${RA_API_SSCONFIG_MISSCONFIG.channelId}`,
            };
            const beneficiaryUpdate = await getService(beneficiary);
            if (beneficiaryUpdate.status === 200) {
                const { data: { allPoperties, properties } = {} } = beneficiaryUpdate;
                this.setState({ allPoperties, properties });
            }
        

    }

    getRowdata = (property, key) => {
        const {
            displayName = null, isMultiDropDown = false,
            supportedValues = null, displayNameKey = null
        } = property || {};
        let { value = null } = property || {};
        if (supportedValues !== null && value !== null) {
            if (isMultiDropDown) value = value.split(',').map(index => supportedValues[index])
            else value = supportedValues[value]
        };
        return (
            <tr className={`reportRow ${key % 2 !== 0 ? 'line' : ''}`} key={key}>
                <td className='row-width-25'>{displayName === null ? displayNameKey : displayName}</td>
                <td className='row-width-5'></td>
                <td className='row-width-60'>
                    {Array.isArray(value) ? (
                        <ul className="list-scrollpane p-1">
                            {value.map((oneLi, i) => <li className="listItem" key={i}>{oneLi}</li>)}
                        </ul>
                    ) : value}
                </td>
            </tr>
        );
    }

    render() {
        const { properties = null } = this.state;
        return (
            <div className="p-4 SSConfig-misc">
                <h2 className="title">Miscellaneous Configurations</h2>
                <p className="desc">This screen displays configuration of various case management parameters, some rule parameters and any channel specific parameters.</p>
                <table className="config-table">
                    <thead>
                        <tr>
                            <th className='row-width-3' />
                            <th className='row-width-25' />
                            <th className='row-width-5' />
                            <th className='row-width-60' />
                        </tr>
                    </thead>
                    <tbody>
                        {
                            Array.isArray(properties) && properties.map((property, i) => this.getRowdata(property, i))
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default SSConfigMiscellaneousConfig;