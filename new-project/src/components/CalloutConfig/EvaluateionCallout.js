import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import {
    serverUrl, RA_API_URL, RA_API_STATUS, RA_STR_SELECT
} from '../../shared/utlities/constants';
import { orgData } from '../../shared/utlities/renderer';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import './CalloutConfig.scss';
import _ from 'underscore';

class EvaluateionCallout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authentication: ['YES', 'NO'],
            serverAuthentication: ['NO'],
            clientAuthentication: 'NO',
            availableConfigNames: [],
            proposedCalloutConfig: {},
            orgDetails: '',
            serverRootCertBytes: [],
            rfClientCertBytes: [],
            rfClientPrivateKeyBytes: [],
            serverAuthSSL: 'NO',
            clientAuthSSL: 'NO',
            operation: this.props.location.state.operation,
            calloutForm: {},
            clientDisable: true,
            serverDisable: true,
            windowLocation: window.location.pathname.split('/')[window.location.pathname.split('/').length - 1] || ''
        }
    }

    showErrorList = () => {
        window.scrollTo(0, 0);
    }

    componentDidMount = async () => {
        this.state.orgDetails = orgData('');
        const getRuleSet = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getDefaultOrgs']}/${this.props.location.state.orgName}${RA_API_URL['availableConfigs']}`,
        };
        const getRuleSetStatus = await getService(getRuleSet);
        if (getRuleSetStatus && getRuleSetStatus.status === RA_API_STATUS['200']) {
            let objectKeys = Object.keys(getRuleSetStatus.data.availableConfigNames);
            let objectValues = Object.values(getRuleSetStatus.data.availableConfigNames);
            for (let i = 0; i < objectKeys.length; i++) {
                this.state.availableConfigNames.push({
                    'key': objectValues[i],
                    'content': objectValues[i]
                })
                this.setState({
                    availableConfigNames: this.state.availableConfigNames
                });
            }
        }
    }

    getCalloutData = async (val) => {
        this.setState({
            proposedCalloutConfig: {}
        })
        const calloutData = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getDefaultOrgs']}/${this.props.location.state.orgName}${RA_API_URL['callout']}/${val.replace(/%20/g, "")}${RA_API_URL['details']}/${this.state.operation}`,
        };
        const calloutDataStatus = await getService(calloutData);
        if (calloutDataStatus && calloutDataStatus.status === RA_API_STATUS['200']) {
            this.setState({
                proposedCalloutConfig: calloutDataStatus.data.proposedCalloutConfig
            })
        }
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    handleChange = (e) => {
        const calloutForm = this.state.calloutForm;
        const { name, value } = e.target;
        var fieldValue;
        if (name === 'toUpdateConfigName') {
            this.getCalloutData(e.target.value);
        }
        else if (name === 'clientAuthSSL') {
            this.setState({
                clientAuthSSL: value
            })
            if (value === 'YES') {
                this.setState({
                    clientDisable: false
                })
            }
            else {
                this.setState({
                    clientDisable: true
                })
            }
        }
        else if (name === 'serverRootCertBase64') {
            let reader = new FileReader();
            let file = e.target.files[0];
            if (file) {
                const lastDot = file.name.lastIndexOf('.');
                const fileName = file.name.substring(lastDot + 1);
                reader.onload = () => {
                    const intArray = new Int8Array(reader.result);
                    this.setState({
                        serverRootCertBytes: intArray,
                        // format: fileName
                    })
                }
                reader.readAsArrayBuffer(file)
            }
        }
        else if (name === 'clientCertBase64') {
            let reader = new FileReader();
            let file = e.target.files[0];
            if (file) {
                const lastDot = file.name.lastIndexOf('.');
                const fileName = file.name.substring(lastDot + 1);
                reader.onload = () => {
                    const intArray = new Int8Array(reader.result);
                    this.setState({
                        rfClientCertBytes: intArray,
                        // format: fileName
                    })
                }
                reader.readAsArrayBuffer(file)
            }
        }
        else if (name === 'clientPrivateKeyBase64') {
            let reader = new FileReader();
            let file = e.target.files[0];
            if (file) {
                const lastDot = file.name.lastIndexOf('.');
                const fileName = file.name.substring(lastDot + 1);
                reader.onload = () => {
                    const intArray = new Int8Array(reader.result);
                    this.setState({
                        rfClientPrivateKeyBytes: intArray,
                        // format: fileName
                    })
                }
                reader.readAsArrayBuffer(file)
            }
        }
        fieldValue = value;
        calloutForm[name] = fieldValue;
        this.setState({
            [e.target.name]: e.target.value,
            calloutForm
        })

    }

    handleChangeServer = (e) => {
        this.setState({
            serverAuthentication: [e.target.value],
            serverAuthSSL: e.target.value,
        });
        if (e.target.value === 'YES') {
            this.setState({
                serverDisable: false
            })
        }
        else {
            this.setState({
                serverDisable: true
            })
        }
    }

    handleSubmit = async () => {
        let params = {
            ...this.state.proposedCalloutConfig,
            ...this.state.calloutForm,
            selectedOperation: this.state.operation,
            orgName: this.state.orgDetails.orgName,
            createOwnOrReferGlobal: "createOwn",
            serverAuthSSL: this.state.serverAuthSSL,
            clientAuthSSL: this.state.clientAuthSSL,
            serverRootCertBytes: _.values(this.state.serverRootCertBytes),
            rfClientCertBytes: _.values(this.state.rfClientCertBytes),
            rfClientPrivateKeyBytes: _.values(this.state.rfClientPrivateKeyBytes)
        }
        const saveCallout = {
            method: 'POST',
            url: `${serverUrl}${RA_API_URL['getDefaultOrgs']}/${this.props.location.state.orgName}${RA_API_URL['callout']}`,
            data: params
        };
        const saveCalloutStatus = await getService(saveCallout);
        if (saveCalloutStatus && saveCalloutStatus.status === RA_API_STATUS['200']) {
            this.props.activateSuccessList(true, saveCalloutStatus.data);
            this.props.activateErrorList(false, '');
            this.showErrorList();
        }
        else {
            this.props.activateErrorList(true, saveCalloutStatus.data.errorList);
            this.props.activateSuccessList(false, '');
            this.showErrorList();
        }
    }

    render() {
        const { availableConfigNames, proposedCalloutConfig } = this.state;
        return (
            <div className="main">
                <h2 className="title">{RA_STR.evaluateCalloutTitle}</h2>
                {this.props.location.state.operation === 1 ?
                    <p className="desc">{RA_STR.evaluateCalloutDesc}</p>
                    :
                    <p className="desc">{RA_STR.scrollingCalloutDesc}</p>
                }
                <div className="col-sm-8">
                    <Select
                        name={'toUpdateConfigName'}
                        title={'Select Existing Ruleset'}
                        options={availableConfigNames ? availableConfigNames : ['']}
                        placeholder={RA_STR_SELECT}
                        controlFunc={this.handleChange} />
                </div>
                {_.keys(proposedCalloutConfig).length > 0 ?
                    <div>
                        < table className="table callout-table">
                            <thead className="thead-light">
                                <tr>
                                    <th scope="col">{RA_STR.calloutElements}</th>
                                    <th scope="col">{RA_STR.statusMessage}</th>
                                    <th scope="col">{RA_STR.proposed}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{RA_STR.serverAuthSSL}</td>
                                    <td>{RA_STR.no}</td>
                                    <td>
                                        <RadioGroup
                                            title={''}
                                            setName={'serverAuthSSL'}
                                            controlFunc={this.handleChangeServer}
                                            type={'radio'}
                                            options={this.state.authentication}
                                            selectedOptions={proposedCalloutConfig.serverAuthSSL === null ? this.state.serverAuthentication : [proposedCalloutConfig.serverAuthSSL]}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.clientAuthSSL}</td>
                                    <td>{RA_STR.no}</td>
                                    <td>
                                        <div className="custom-control custom-radio">
                                            <span className="form-label capitalize col-md-6">
                                                <input type="radio" className="custom-control-input "
                                                    id="clientAuthSSL"
                                                    name="clientAuthSSL"
                                                    onChange={this.handleChange}
                                                    defaultChecked={proposedCalloutConfig.clientAuthSSL}
                                                    value='YES'
                                                />
                                                <label className="form-label custom-control-label" htmlFor="clientAuthSSL">YES</label>
                                            </span>
                                            <span className="form-label capitalize col-md-6">
                                                <input type="radio" className="custom-control-input "
                                                    id="clientAuthSSLY"
                                                    name="clientAuthSSL"
                                                    value="NO"
                                                    defaultChecked={proposedCalloutConfig.clientAuthSSL === null ? "NO" : proposedCalloutConfig.clientAuthSSL}
                                                    onChange={this.handleChange}
                                                />
                                                <label className="custom-control-label" htmlFor="clientAuthSSLY">NO</label>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.calloutUrl}</td>
                                    <td>{RA_STR.NA}</td>
                                    <td className="callout-label">
                                        <SingleInput
                                            title={''}
                                            inputType={'text'}
                                            name={'calloutUrl'}
                                            content={proposedCalloutConfig.calloutUrl}
                                            controlFunc={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.connectTimeout}</td>
                                    <td>{RA_STR.NA}</td>
                                    <td className="callout-label">
                                        <SingleInput
                                            title={''}
                                            inputType={'text'}
                                            name={'connectionTimeout'}
                                            content={proposedCalloutConfig.connectionTimeout}
                                            controlFunc={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.readTimeout}</td>
                                    <td>{RA_STR.NA}</td>
                                    <td className="callout-label">
                                        <SingleInput
                                            title={''}
                                            inputType={'text'}
                                            name={'readTimeout'}
                                            content={proposedCalloutConfig.readTimeout}
                                            controlFunc={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.serverCertificate}</td>
                                    <td>{RA_STR.NA}</td>
                                    <td className="row">
                                        <div className='col-sm-8'>
                                            <input type="file" className="form-control callout-file" name='serverRootCertBase64'
                                                onChange={this.handleChange}
                                                disabled={this.state.serverDisable} />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.raPrivateKeyText}</td>
                                    <td>{RA_STR.NA}</td>
                                    <td>
                                        <div className="col-sm-12 row">
                                            <span>{RA_STR.certificate}</span>
                                            <input type="file" className="form-control callout-file col-sm-8" name="clientCertBase64"
                                                onChange={this.handleChange}
                                                disabled={this.state.clientDisable} />
                                        </div>
                                        <p>{RA_STR.encodedText}</p>
                                        <div className="col-sm-12 row">
                                            <span>{RA_STR.privateKey}</span>
                                            <input type="file" className="form-control callout-file col-sm-8" name="clientPrivateKeyBase64"
                                                onChange={this.handleChange}
                                                disabled={this.state.clientDisable} />
                                        </div>
                                        <p>{RA_STR.pemEncoded}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{RA_STR.calloutDesc}</td>
                                    <td>{RA_STR.NA}</td>
                                    <td>
                                        <textarea className="form-control" name="calloutDesc" rows='3' cols='26'
                                            defaultValue={proposedCalloutConfig.calloutDesc} onChange={this.handleChange} ></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="form-group form-submit-button row mt-5">
                            <input className="secondary-btn ml-2" type="button" value="SAVE" onClick={this.handleSubmit} />
                        </div>
                    </div>
                    : ''
                }
            </div>
        );
    }
}

export default EvaluateionCallout;