import React, { Component } from 'react';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import { RA_STR } from '../../shared/utlities/messages';
import { orgData } from '../../shared/utlities/renderer';
import { RA_STR_CALLOUT_CONFIG } from '../../shared/utlities/constants';
import './CalloutConfig.scss';

class CalloutConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      evaluation: ['Evaluation Callout', 'Scoring Callout'],
      evaluationSelection: ['Evaluation Callout'],
      orgDetails: '',
      operation: 1,
      windowLocation: window.location.pathname.split('/')[window.location.pathname.split('/').length - 1] || ''
    }
  }

  componentDidMount = () => {
    this.state.orgDetails = orgData('');
  }

  handleNext = () => {
    let orgName = (this.state.windowLocation === RA_STR_CALLOUT_CONFIG) ? this.state.orgDetails.orgName : "SYSTEM";
    if (this.state.windowLocation === RA_STR_CALLOUT_CONFIG) {
      this.props.history.push({
        pathname: `/org/searchOrg/risk-engine/callout-config/evaluate-callout`,
        search: `?orgname=${this.state.orgDetails.orgName}&status=${this.state.orgDetails.status}`,
        state: {
          operation: this.state.operation,
          orgName: orgName
        }

      })
    }
    else{
      this.props.history.push({
        pathname: `/server-config/ra/ss-callout-config/evaluate-callout`,
        search: ``,
        state: {
          operation: this.state.operation,
          orgName: orgName
        }

      })
    }
  }

  handleSelection = (e) => {
    if (e.target.value === 'Evaluation Callout') {
      this.setState({
        operation: 1
      })
    }
    else if (e.target.value === 'Scoring Callout') {
      this.setState({
        operation: 2
      })
    }
    this.setState({ evaluationSelection: [e.target.value] });
  }
  render() {
    return <div className="main">
      <h2 className="title">{RA_STR.calloutConfig}</h2>
      <p className="desc">{RA_STR.requiredCallout}</p>
      <div className="col-sm-7 ">
        <div className="row">
          <label className="col-sm-4 col-form-label">Callout</label>
          <div className="col-sm-8 mt-1">
            <RadioGroup
              title={'Evaluation Callout'}
              setName={'Evaluation'}
              controlFunc={this.handleSelection}
              type={'radio'}
              options={this.state.evaluation}
              selectedOptions={this.state.evaluationSelection}
            />
          </div>
        </div>
      </div>
      <div className="form-group form-submit-button row mt-5">
        <input className="secondary-btn ml-2" type="button" value="NEXT" onClick={this.handleNext} />
      </div>
    </div>
  }
}

export default CalloutConfig;
