import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class MonthCheckScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { THRESHOLD: '01' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleInputChanges = (fieldId, e) => {
		const { fields } = this.state;
		fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
		this.setState({ fields: fields });
	}
	handleValidation = () => {
		const { fields } = this.state;
		let obj = {};
		let ruleGroup = [];
		fields['OPERATOR']=this.props.passDataOperator;
		fields['TAGNAME1']=this.props.passDataElement;
		obj['ruleKey'] = 'monthcheckScreen';
		obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields['THRESHOLD']}`;
		obj['ruleData'] = fields;
		ruleGroup.push(obj);
		return ruleGroup
	}
	render() {
		const {fields}=this.state;
		return <div id="MonthCheckScreen" >
			{RA_STR.allMonths}
			<select className="form-control" id="monthNames_MonthCheckScreen" name="monthNames_MonthCheckScreen" onChange={this.handleInputChanges.bind(this, 'THRESHOLD')} value={fields['THRESHOLD']}>
				<option selected value="01" >01</option>
				<option value="02" >02</option>
				<option value="03" >03</option>
				<option value="04" >04</option>
				<option value="05" >05</option>
				<option value="06" >06</option>
				<option value="07" >07</option>
				<option value="08" >08</option>
				<option value="09" >09</option>
				<option value="10" >10</option>
				<option value="11" >11</option>
				<option value="12" >12</option>
			</select>
		</div>;
	}
}
export default MonthCheckScreen;
