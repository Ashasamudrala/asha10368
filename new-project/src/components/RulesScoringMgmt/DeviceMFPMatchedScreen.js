import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class DeviceMFPMatchedScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { REVERSE_LOOKUP_THRESHOLD: '0' }
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields['PASS_THRESHHOLD'] || fields['PASS_THRESHHOLD'] > 100) {
      formIsValid = false;
      alert(RA_STR.ruleScreenValidate100);
      return
    }
    if (formIsValid) {
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='deviceMfpScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} GREATER_OR_EQUAL TO ${fields['PASS_THRESHHOLD']} AND REVERSE LOOKUP THRESHOLD GREATER_OR_EQUAL TO ${fields['REVERSE_LOOKUP_THRESHOLD']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const {fields}=this.state;
    return <div id="DeviceMFPMatchedScreen" >
      <table>
        <tr>
          <td>
            {RA_STR.ruleMatchThreshold}
          </td>
          <td>
            <input className="form-control" id='passThreshold_DeviceMFPMatchedScreen' type='text' maxLength='3' size='3' onChange={this.handleInputChanges.bind(this,'PASS_THRESHHOLD')} value={fields['PASS_THRESHHOLD']}/>
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleReverseThreshold}
          </td>
          <td>
            <input className="form-control" id='revLookupPassThreshold_DeviceMFPMatchedScreen' name='revLookupPassThreshold_DeviceMFPMatchedScreen' type='text' maxLength='3' size='3' disabled={true} value={fields['REVERSE_LOOKUP_THRESHOLD']}/>
          </td>
          <td>
          </td>
        </tr>
      </table>
      {/* <div id="UserKnownScreen">
        {RA_STR.ruleNoConfigMsg}
      </div>
      <div id="ExceptionUserScreen">
        {RA_STR.ruleNoConfigMsg}
      </div> */}
    </div>
  }
}

export default DeviceMFPMatchedScreen;
