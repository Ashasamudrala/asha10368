import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class DateCheckScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {}
    }
  }
  componentDidMount = () => {
    const {fields}=this.state;
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
    }
	}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleRuleValidation = (e) => {
    var val = e.target.value;
    if (val.length > 8 || val.length < 8) {
      alert(RA_STR.ruleScreenYearMonthDate);
      e.target.value = "";
      return
    } else {
      if(val){
        let splitDigits = this.splitToNumbers(val);
        let splitMonthYear = this.splitToNumberTwo(splitDigits[1].toString());
        if(splitMonthYear[0] > 12){
          alert(RA_STR.ruleScreenvalidateMonth);
          e.target.value = "";
        }else if(splitMonthYear[1] > 31){
          alert(RA_STR.ruleScreenValidateDays);
          e.target.value = "";
        }
      }
    }
  }
  splitToNumbers = (str) => {
    return str.match(str.length % 4 ? /^\d|\d{4}/g : /\d{4}/g).map(Number)
  }
  splitToNumberTwo=(str)=>{
    return str.match(str.length % 2 ? /^\d|\d{2}/g : /\d{2}/g).map(Number)
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["THRESHOLD"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenYearMonthDate);
      return
    }
    if (formIsValid) {
      fields['TAGNAME1']=this.props.passDataElement;
      fields['OPERATOR']=this.props.passDataOperator;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='dateCheckScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields["THRESHOLD"]}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const{fields}=this.state;
    return 	<table id="DateCheckScreen" >
    <tr><td>{RA_STR.ruleDateEnter}</td><td><input name="txt_DateCheckScreen" className="form-control"  id="txt_DateCheckScreen" type="text" onChange={this.handleInputChanges.bind(this, 'THRESHOLD')} onBlur={this.handleRuleValidation} value={fields['THRESHOLD']}/></td></tr>
	</table>;
  }
}

export default DateCheckScreen;
