import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
class InDerivedListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { match: 'EXACT' },
      dataSetListItems: []
    }
  }
  componentDidMount = async () => {
    const { fields } = this.state;
    const listSecureDataElements = `/organization/${this.props.ruleOrgName}/rulesandscore/rulebuildersettings`;
    const DataSetUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/orglistoflistsforelement/${this.props.configname}`;
    const secureListUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/listOfsecurelists/${this.props.configname}`;
    const dataSetMap = {
      method: 'GET',
      url: `${serverUrl}${DataSetUrl}`
    };
    const dataSetMapStatus = await getService(dataSetMap);
    const listSimpleLists = CA_Utils.getListData(dataSetMapStatus.data);
    if (dataSetMapStatus.status === 200) {
      const listSecureListMap = {
        method: 'GET',
        url: `${serverUrl}${listSecureDataElements}`
      };
      const listSecureListMapStatus = await getService(listSecureListMap);
      if (listSecureListMapStatus.status === 200) {
        const secureListMap = {
          method: 'GET',
          url: `${serverUrl}${secureListUrl}`
        };
        const secureListMapStatus = await getService(secureListMap);
        if (secureListMapStatus.status === 200) {
          const secureListData = CA_Utils.getListData(secureListMapStatus.data);
          var arrRespParts = listSecureListMapStatus.data.split(";");
          var listSecureDataElementsMap = [];
          for (var nSettingCounter = 0; nSettingCounter < arrRespParts.length; nSettingCounter++) {
            let arrSettingList = arrRespParts[nSettingCounter].split("=");
            if (arrSettingList === null || arrSettingList.length === 0)
              continue;
            if ("SECURE_DATA_ELEMENTS" === arrSettingList[0]) {
              listSecureDataElementsMap.push(arrSettingList[1].split(","));
            }
          }
          var lists = [];
          var bIsElementSecureElement = listSecureDataElementsMap.includes(this.props.ruleOrgName);
          if (!bIsElementSecureElement) {
            for (var nElementCounter = 0; nElementCounter < listSimpleLists.length; nElementCounter += 2) {
              if (listSimpleLists[nElementCounter] != this.props.passDataElement && listSimpleLists[nElementCounter] != "@UNSPECIFIED@")
                continue;
              else {
                for (var nListCounter = 0; nListCounter < listSimpleLists[nElementCounter + 1].length; nListCounter++) {
                  if (listSimpleLists[nElementCounter + 1][nListCounter][0] === "LOCAL") {
                    let obj = {};
                    obj['ruleKey'] = listSimpleLists[nElementCounter + 1][nListCounter][2];
                    obj['ruleData'] = listSimpleLists[nElementCounter + 1][nListCounter][3]
                    lists.push(obj);
                  }
                }
              }
            }
          } else {
            for (var nElementCounter = 0; nElementCounter < secureListData.length; nElementCounter += 2) {
              if (secureListData[nElementCounter] !== this.props.ruleOrgName)
                continue;
              else {
                for (var nListCounter = 0; nListCounter < secureListData[nElementCounter + 1].length; nListCounter++) {
                  if (secureListData[nElementCounter + 1][nListCounter][0] === "LOCAL") {
                    let obj = {};
                    obj['ruleKey'] = secureListData[nElementCounter + 1][nListCounter][2];
                    obj['ruleData'] = secureListData[nElementCounter + 1][nListCounter][3];
                    lists.push(obj);
                  }
                }
              }
            }
          }
          let getAllFieldData = fields;
          getAllFieldData['dataSet'] = lists[0].ruleKey;
          this.setState({ dataSetListItems: lists, fields: getAllFieldData });
        }
      }
      if (this.props.ruleFields) {
        this.setState({ fields: this.props.ruleFields });
      }
    }
  }
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["dataSet"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenDataName);
      return
    }
    if (!fields["mapSet"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenMapName);
      return
    }
    if (formIsValid) {
      fields['TAGNAME']=this.props.passDataElement;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='derivedListScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} WITH DATASET = ${fields['dataSet']} & MAPPINGSET = ${fields['mapSet']} & MATCHTYPE = ${fields['match']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const { dataSetListItems,fields } = this.state;
    return <div id="InDerivedListScreen">
      <div id="incategorylist_combo" >{RA_STR.ruleIdentityData}
        <select id="incategorylist_listOfLists" name="incategorylist_listOfLists" className="form-control" onChange={this.handleInputChanges.bind(this, 'dataSet')} value={fields['dataSet']}>
          {dataSetListItems && dataSetListItems.map((eachList) => {
            return <option value={eachList.ruleKey}>{eachList.ruleData}</option>
          })}
        </select></div>
      <div id="incategorylist_text"  >{RA_STR.ruleIdentityMapping} <input type="text" size="25" id="dataSetId_InDerivedListScreen" name="dataSetId_InDerivedListScreen" className="form-control" onChange={this.handleInputChanges.bind(this, 'mapSet')} value={fields['mapSet']}/></div>
      <div>
        {RA_STR.ruleMatchType}
        <select id="matchtype_InDerivedListScreen" className="form-control" onChange={this.handleInputChanges.bind(this, 'match')} value={fields['match']}>
          <option id="exact" value="EXACT">{RA_STR.ruleExactMatch}</option>
          <option id="partial" value="PARTIAL">{RA_STR.rulePartialMatch}</option>
        </select>
      </div>
    </div>
  }
}
export default InDerivedListScreen;
