import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class CustomInDerivedListScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { MATCHTYPE: 'EXACT MATCH' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleChange = (field, e) => {
		let fields = this.state.fields;
		fields[field] = e.target.value;
		this.setState({ fields: fields });
	}
	handleValidation = () => {
		let fields = this.state.fields;
		let formIsValid = true;

		if (!fields["TAGNAME"]) {
			formIsValid = false;
			alert(RA_STR.ruleScreenDataName);
		}
		if (!fields["LISTDATASET"]) {
			formIsValid = false;
			alert(RA_STR.ruleScreenDataName);
		}
		if (!fields["MAPPINGDATASET"]) {
			formIsValid = false;
			alert(RA_STR.ruleScreenMapName);
		}
		if (formIsValid) {
			let obj = {};
			let ruleGroup = [];
			obj['ruleKey'] = 'customInderivedScreen';
			obj['ruleExpression'] = `${fields['TAGNAME']} IN_CATEGORY WITH DATASET = ${fields["LISTDATASET"]} & MAPPINGSET = ${fields['MAPPINGDATASET']} & MATCHTYPE = ${fields['MATCHTYPE']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
		}
		return formIsValid;
	}
	render() {
		const { fields } = this.state;
		return <div id="CustomInDerivedListScreen">
			<div>{RA_STR.ruleTagName}<input className="form-control" type="text" size="25" id="tagname_CustomInDerivedListScreen" name="tagname_InDerivedListScreen" onChange={this.handleChange.bind(this, "TAGNAME")} value={fields['TAGNAME']} /></div>
			<div>{RA_STR.ruleIdentityData}<input className="form-control" type="text" size="25" id="dataSetId_CustomInDerivedListScreen" name="dataSetId_CustomInDerivedListScreen" onChange={this.handleChange.bind(this, "LISTDATASET")} value={fields['LISTDATASET']} /></div>
			<div>{RA_STR.ruleIdentityMapping}<input className="form-control" type="text" size="25" name="mappingSetId_CustomInDerivedListScreen" id="mappingSetId_CustomInDerivedListScreen" onChange={this.handleChange.bind(this, "MAPPINGDATASET")} value={fields['MAPPINGDATASET']} /></div>
			{RA_STR.ruleMatchType}
			<select className="form-control" id="matchtype_CustomInDerivedListScreen" onChange={this.handleChange.bind(this, "MATCHTYPE")} value={fields['MATCHTYPE']}>
				<option id="exact" value="EXACT">{RA_STR.ruleExactMatch}</option>
				<option id="partial" value="PARTIAL">{RA_STR.rulePartialMatch}</option>
			</select>
		</div>
	}
}

export default CustomInDerivedListScreen;
