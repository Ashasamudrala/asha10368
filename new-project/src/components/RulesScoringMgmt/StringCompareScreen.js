import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
class StringCompareScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {}
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["THRESHOLD"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenInput);
      return
    }
    if(formIsValid){
      fields['OPERATOR']=this.props.passDataOperator;
      fields['TAGNAME1']=this.props.passDataElement;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='StringCompareScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields["THRESHOLD"]}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    }
  }
  render() {
    const {fields}=this.state;
    return <div id="StringCompareScreen">
      <div id="tabHeader_stringCompareScreen">
        {RA_STR.ruleStrCompare}
        <input className="form-control" type="text" size="30" id="identifier_stringCompareScreen" onChange={this.handleInputChanges.bind(this,'THRESHOLD')} value={fields.THRESHOLD}/>
      </div>
    </div>
  }
}

export default StringCompareScreen;
