import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import AdditionalFilter from './AdditionalFilter';
class ActionCumulativeAmountCheckScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields : { HISTTRANSCTIONSTATUS: 'All', THRESHOLDACTION: 'PINCHANGE',COMBINATION_STR_EXT:'NULL' }
    }
  }
  componentDidMount = () => {
    if (this.props.ruleFields) {
      this.setState({ fields: this.props.ruleFields });
    }
  }
  handleChange = (field, e) => {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields['THRESHOLD']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenCumulative);
      return
    }
    if (!fields['THRESHOLDTRANSACTION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenThresholdCount);
      return
    }
    if (!fields['DURATION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenLookback);
      return
    }
    if (!fields['MAXTRANSACTION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenMaxTnx);
      return
    }
    if (formIsValid) {
      let obj = {};
      let ruleGroup = [];
      let getFilterItem = this.refs.additionalFilter.getFilterData();
      fields['COMBINATION_STR_EXT']=getFilterItem;
      obj['ruleKey'] = 'actionCumulative';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields['THRESHOLD']} ${fields['THRESHOLDTRANSACTION']} ${fields['DURATION']} ${fields['MAXTRANSACTION']} ${fields['HISTTRANSCTIONSTATUS']} ${fields['THRESHOLDACTION']} ${fields['COMBINATION_STR_EXT']}`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj)
      return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const { fields } = this.state;
    return <div id='ActionCumulativeAmountCheckScreen'>
      <table>
        <tr>
          <td>
            {RA_STR.ruleCummulativeAmount}
          </td>
          <td>
            <input className="form-control" id="actionCumulativeAmountCheckScreen_amount" type='text' maxlength="100" onChange={this.handleChange.bind(this, "THRESHOLD")} value={fields['THRESHOLD']} />
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxCount}
          </td>
          <td>
            <input className="form-control" id="actionCumulativeAmountCheckScreen_txnCount" type='text' maxlength="10" onChange={this.handleChange.bind(this, "THRESHOLDTRANSACTION")} value={fields['THRESHOLDTRANSACTION']} />
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleLookBack}
          </td>
          <td>
            <input className="form-control" id="actionCumulativeAmountCheckScreen_duration" type='text' maxlength="10" onChange={this.handleChange.bind(this, "DURATION")} value={fields['DURATION']} />
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleMaxTnxCount}
          </td>
          <td>
            <input className="form-control" id="actionCumulativeAmountCheckScreen_maxTransactionLookBack" type='text' maxlength="10" onChange={this.handleChange.bind(this, "MAXTRANSACTION")} value={fields['MAXTRANSACTION']} />
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxStatus}
          </td>
          <td>
            <select id="actionCumulativeAmountCheckScreen_histTransactionStatus" className="form-control" onChange={this.handleChange.bind(this, "HISTTRANSCTIONSTATUS")} value={fields['HISTTRANSCTIONSTATUS']}>
              <option value='ALL'>{RA_STR.ruleTnxStatusAll}</option>
              <option value='APPROVED'>{RA_STR.ruleTnxStatusApproved}</option>
              <option value='DENIED'>{RA_STR.ruleTnxStatusDenied}</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleExclcurrtxn}
          </td>
          <td>
            <select id="actionCumulativeAmountCheckScreen_thresholdAction" className="form-control" onChange={this.handleChange.bind(this, "THRESHOLDACTION")} value={fields['THRESHOLDACTION']}>
              <option value='PINCHANGE'>{RA_STR.rulePinChange}</option>
              <option value='FINANCIALINQUIRY'>{RA_STR.ruleFinancialEnquiry}</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleAddFilter}
          </td>
          <td>
          <AdditionalFilter ruleOrgName={this.props.ruleOrgName} defaultFilter={fields['COMBINATION_STR_EXT']} ref="additionalFilter"></AdditionalFilter>
          </td>
        </tr>
      </table>
    </div>;
  }
}

export default ActionCumulativeAmountCheckScreen;
