import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class ZoneHoppingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {}
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["MAX_SPEED"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenMaxSpeed);
      return
    }
    if (!fields["MAX_USER"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenUsers);
      return
    }
    if (!fields["UNCERTAINITY_OFFSET"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenTolerance);
      return
    }
    if (formIsValid) {
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='ZONEHOPPING';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} MAX SPEED = ${fields["MAX_SPEED"]} MILES/HOUR & MAX USER COUNT = ${fields["MAX_USER"]} & UNCERTAINTY = ${fields["UNCERTAINITY_OFFSET"]} MILES`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid
    }
  }
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  render() {
    const {fields}=this.state;
    return <div id="ZoneHoppingScreen">
      <table>
        <tr>
          <td>{RA_STR.ruleSpeedTravel}</td>
          <td>
            <input className="form-control" id="maxSpeed_ZoneHoppingScreen" onChange={this.handleInputChanges.bind(this, 'MAX_SPEED')} type='text' size="5" maxlength="5" value={fields['MAX_SPEED']} />
          </td>
          <td>
            {RA_STR.ruleMilePH}
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleMaxUSers}
          </td>
          <td>
            <input className="form-control" id="numUsers_ZoneHoppingScreen" onChange={this.handleInputChanges.bind(this, 'MAX_USER')} type='text' size="5" maxlength="2" value={fields['MAX_USER']} />
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleDistanceTolerance}
          </td>
          <td>
            <input className="form-control" id="maxDist_ZoneHoppingScreen" onChange={this.handleInputChanges.bind(this, 'UNCERTAINITY_OFFSET')} type='text' size="5" maxlength="5" value={fields['UNCERTAINITY_OFFSET']} />
          </td>
          <td >
            {RA_STR.ruleMiles}
          </td>
        </tr>
      </table>
    </div>;
  }
}
export default ZoneHoppingScreen;
