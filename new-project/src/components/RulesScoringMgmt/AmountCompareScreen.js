import React, { Component } from 'react';
import CA_Utils from '../../shared/utlities/commonUtils';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
import ReactDOM from 'react-dom';
class AmountCompareScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {currency:"ALL"},
      allCurrenciesData: [],
      amountCurrencyData: [],
      allCurrenciesReverseMap:[]
    }
  }
  componentDidMount = async () => {
    const orgBaseUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/getorgbasecurrlist`;
    const orgBaseUrlMap = {
      method: 'GET',
      url: `${serverUrl}${orgBaseUrl}`
    };
    const orgBaseUrlMapStatus = await getService(orgBaseUrlMap);
    if(orgBaseUrlMapStatus.status === 200){
      const secureListUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/currencylist`;
      const currencyMap = {
        method: 'GET',
        url: `${serverUrl}${secureListUrl}`
      };
      const dataSetMapStatus = await getService(currencyMap);
      if (dataSetMapStatus.status === 200) {
        var opList = dataSetMapStatus.data;
        var arr = opList.split(";");
        let allCurrencies = [];
        let allCurrenciesReverseMap = [];
        for (var arri = 0; arri < arr.length; ++arri) {
          if (arr[arri].trim() == "")
            continue;
          var arr2 = arr[arri].split(":");
          var currType = arr2[0];
          var currCode = arr2[1];
          allCurrencies[currCode] = currType;
          allCurrenciesReverseMap[currType] = currCode;
        }
        this.setState({ allCurrenciesData: allCurrencies,allCurrenciesReverseMap:allCurrenciesReverseMap });
        if (this.props.ruleFields) {
          let propsData=this.props.ruleFields;
          propsData.currency='';
          propsData.amountCmp='';
          this.setState({ fields: propsData });
        }
      }
    }
  }
  handleRuleValidation = (e) => {
    var val = e.target.value;
    if (CA_Utils.checkForInteger(val, 1, "") == false) {
      e.target.value = "";
      alert(RA_STR.ruleValidationMsg);
      return false;
    }
    return true;
  }
  handleInputChanges = (field, e) => {
    let fields = this.state.fields;
    fields[field] = e.target.selectedOptions ? e.target.selectedOptions[0].text : e.target.value
    this.setState({ fields: fields });
  }
  handleAmountAdd = () => {
    let { fields } = this.state;
    if (fields['amountCmp']) {
      let currencyFilter = fields['amountCurrencyData'] && fields['amountCurrencyData'].filter((eachCurrency) => {
        return eachCurrency.currencyType === 'USD'
      })
      let amountCurrencyData=[];
      if ((currencyFilter && currencyFilter.length === 0 && fields['currency'] !== 'USD')||(currencyFilter === undefined && fields['currency'] !== 'USD')) {
        alert("Set a threshold for the base currency 'USD'.");
      } else {
        let obj = {};
        obj['currencyValue'] = fields['amountCmp'];
        obj['currencyType'] = fields['currency'];
        amountCurrencyData.push(obj);
        if(fields['amountCurrencyData']){
          fields['amountCurrencyData'].push(obj);
        }else{
          fields['amountCurrencyData'] = amountCurrencyData;
        }
        this.setState({fields: fields });
      }
    }
  }
  handleAmountRemove = () => {
    const{fields}=this.state;
    let getDataElements = this.getSelectedDropdown('amounts_AmountCompareScreen');
    fields['amountCurrencyData'].map((eachCurrency,key)=>{
      getDataElements.map((eachData)=>{
        let splitText=eachData.split(" ");
        if(splitText[1] === eachCurrency.currencyType){
          fields['amountCurrencyData'].splice(key,1);
        }
      })
    })
    this.setState({fields:fields});
  }
  getSelectedDropdown = (getSelection) => {
    let node = ReactDOM.findDOMNode(this.refs[getSelection]);
    var options = node && node.querySelectorAll('option');
    let groupSelected = [];
    for (var i = 0; i < options.length; i++) {
        if (options[i].selected) {
            groupSelected.push(options[i].value);
        }
    }
    return groupSelected
}
  handleValidation = () => {
    let { fields } = this.state;
    let formIsValid = true;
    if (!fields['amountCmp'] || !fields['amountCurrencyData']) {
      alert(RA_STR.ruleAmountList);
      formIsValid = false;
    }
    if (formIsValid) {
      let groupCurrency=[];
      fields['amountCurrencyData'] && fields['amountCurrencyData'].map((eacCurrenct)=>{
        groupCurrency.push(`${eacCurrenct.currencyValue}_${eacCurrenct.currencyType}`);
      })
      fields['OPERATOR']=this.props.passDataOperator;
      fields['allCurrenciesReverse']=this.state.allCurrenciesReverseMap;
      let obj = {};
      let ruleGroup = [];
      obj['ruleKey'] = 'amountCompare';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} [${groupCurrency.join(',')}]`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj);
      return ruleGroup
    }
  }
  render() {
    const { fields, allCurrenciesData, amountCurrencyData } = this.state;
    return <div id='AmountCompareScreen' >
      <table>
        <tr>
          <td><input id='text_AmountCompareScreen' className="form-control" name='text_AmountCompareScreen' type='text' maxLength='10' size='10' onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'amountCmp')} /></td>
          <td><select id="select_AmountCompareScreen" className="form-control" onChange={this.handleInputChanges.bind(this, 'currency')}>
            {allCurrenciesData && allCurrenciesData.map((eachCurrency, key) => {
              return <option value={key}>{eachCurrency}</option>
            })
            }
          </select></td>
          <td><input className="amountAdd" type="button" value="Add" onClick={this.handleAmountAdd} /></td>
        </tr>
        <tr>
          <td><select size="10" multiple="multiple" id="amounts_AmountCompareScreen" className="amounts_AmountCompareScreen" ref="amounts_AmountCompareScreen">
            {fields['amountCurrencyData'] && fields['amountCurrencyData'].map((eachAmount) => {
              return <option>{eachAmount.currencyValue} {eachAmount.currencyType}</option>
            })}
          </select></td>
          <td><input className="amountRemove" type="button" value="Remove" onClick={this.handleAmountRemove} /></td>
        </tr>
      </table>
    </div>;
  }
}

export default AmountCompareScreen;
