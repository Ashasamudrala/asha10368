import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class DevUserMaturityScreen extends Component {
  render() {
    return <div id='DevUserMaturityScreen'>
      <table>
        <tr>
          <td>
            {RA_STR.ruleSuccess}
				</td>
          <td>
            <input id='velocity_DevUserMaturityScreen' name='velocity_DevUserMaturityScreen' onblur='currentActiveObject.checkValue(this);' type='text' maxLength='3' size='3' value='' />&nbsp;&nbsp;
				</td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleSuccessTxn}
				</td>
          <td>
            <input id='time_DevUserMaturityScreen' name='time_DevUserMaturityScreen' onblur='currentActiveObject.checkValue(this);' type='text' maxLength='3' size='3' value="" />&nbsp;
				</td>
          <td>
            {RA_STR.ruleMoreDays}
				</td>
        </tr>
      </table>
    </div>;
  }
}

export default DevUserMaturityScreen;
