import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class HourOfDayCheckScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { THRESHOLD: '00' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleInputChanges = (fieldId, e) => {
		const { fields } = this.state;
		fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
		this.setState({ fields: fields });
	}
	handleValidation = () => {
		const { fields } = this.state;
		let obj = {};
		let ruleGroup = [];
		fields['OPERATOR']=this.props.passDataOperator;
		fields['TAGNAME1']=this.props.passDataElement;
		obj['ruleKey']='HourOfDayCheckScreen';
		obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields['THRESHOLD']}`;
		obj['ruleData'] = fields;
		ruleGroup.push(obj)
		return ruleGroup
	}
	render() {
		const {fields}=this.state;
		return <table id="HourOfDayCheckScreen" >
			<tr><td>{RA_STR.ruleHourDay}</td><td>
				<select id="hourOfDay_HourOfDayCheckScreen" className="form-control" name="hourOfDay_HourOfDayCheckScreen" onChange={this.handleInputChanges.bind(this,'THRESHOLD')} value={fields['THRESHOLD']}>
					<option selected value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
				</select>
			</td></tr>
		</table>;
	}
}

export default HourOfDayCheckScreen;
