import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import AdditionalFilter from './AdditionalFilter';
import DistinctScreen from './DistinctScreen';
class HistoryUserVelocity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        VELOCITY_DURATION_UNIT: 'MINUTES',
        HISTTRANSCTIONSTATUS: 'ALL',
        EXCL_CURR_TXN: 'NO',
        COMBINATION_STR_EXT: 'NULL',
        DISTINCT_ATTRIBS: 'NULL'
      }
    }
  }
  componentDidMount = () => {
    if (this.props.ruleFields) {
      this.setState({ fields: this.props.ruleFields });
    }
  }
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    let formIsValid = true;
    const { fields } = this.state;
    if (!fields["VELOCITY_TRANSACTION_COUNT"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenThresholdCount);
      return
    }
    if (!fields["VELOCITY_DURATION"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenLookBack);
      return
    }
    if (formIsValid) {
      let obj = {};
      let ruleGroup = [];
      let getFilterItem = this.refs.additionalFilter.getFilterData();
      fields['COMBINATION_STR_EXT'] = getFilterItem;
      let getDistinctItem = this.refs.distinctFilter.getFilterData();
      fields['DISTINCT_ATTRIBS'] = getDistinctItem;
      fields['TAGNAME']=this.props.passDataElement;
      obj['ruleKey'] = 'historyVelocityScreen';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} FOR [${this.props.passDataElement}] GREATER THAN OR EQUAL TO ${fields['VELOCITY_TRANSACTION_COUNT']} IN LAST ${fields['VELOCITY_DURATION']} ${fields['VELOCITY_DURATION_UNIT']} of status ${fields['HISTTRANSCTIONSTATUS']} [Additional History filter : ${fields['COMBINATION_STR_EXT']}] [Exclude Curr Txn :${fields['EXCL_CURR_TXN']}][Distinct : ${fields['DISTINCT_ATTRIBS']}]`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj)
      return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const { fields } = this.state;
    return <div id='HistoryUserVelocity'>
      <table>
        <tr>
          <td>
            {RA_STR.ruleGreaterEqual}
          </td>
          <td>
            <input id='velocity_VelocityScreen' className="form-control" name='velocity_VelocityScreen'
              type='text' maxLength='100' onChange={this.handleInputChanges.bind(this, 'VELOCITY_TRANSACTION_COUNT')} value={fields['VELOCITY_TRANSACTION_COUNT']} />
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleLast}
          </td>
          <td>
            <input className="form-control" id='time_VelocityScreen' name='time_VelocityScreen'
              type='text' maxLength='100' onChange={this.handleInputChanges.bind(this, 'VELOCITY_DURATION')} value={fields['VELOCITY_DURATION']} />
          </td>
          <td>
            <select id="timeUnit_VelocityScreen" className="form-control" onChange={this.handleInputChanges.bind(this, 'VELOCITY_DURATION_UNIT')} value={fields['VELOCITY_DURATION_UNIT']}>
              <option value='MINUTES'>{RA_STR.ruleMins}</option>
              <option value='HOURS'>{RA_STR.ruleHours}</option>
            </select>
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleTnxStatus}
          </td>
          <td>
            <select className="form-control" id="histTransactionStatus_VelocityScreen" onChange={this.handleInputChanges.bind(this, 'HISTTRANSCTIONSTATUS')} value={fields['HISTTRANSCTIONSTATUS']}>
              <option value='ALL'>{RA_STR.ruleTnxStatusAll}</option>
              <option value='APPROVED'>{RA_STR.ruleTnxStatusApproved}</option>
              <option value='DENIED'>{RA_STR.ruleTnxStatusDenied}</option>
            </select>
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleExclcurrtxn}
          </td>
          <td>
            <select className="form-control" id="exclcurrtxn_VelocityScreen" onChange={this.handleInputChanges.bind(this, 'EXCL_CURR_TXN')} value={fields['EXCL_CURR_TXN']}>
              <option value='NO'>{RA_STR.ruleNo}</option>
              <option value='YES'>{RA_STR.ruleYes}</option>
            </select>
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleAddFilter}
          </td>
          <td>
            <AdditionalFilter ruleOrgName={this.props.ruleOrgName} defaultFilter={fields['COMBINATION_STR_EXT']} ref="additionalFilter"></AdditionalFilter>
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleDistinct}
          </td>
          <td>
            <DistinctScreen ruleOrgName={this.props.ruleOrgName} ruleSelectedChannel={this.props.ruleSelectedChannels} defaultFilter={fields['DISTINCT_ATTRIBS']} ref="distinctFilter"></DistinctScreen>
          </td>
        </tr>
      </table>
    </div>;
  }
}

export default HistoryUserVelocity;
