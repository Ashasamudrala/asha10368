import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class GeneralEqualToScreen extends Component {
  constructor(props) {
		super(props);
		this.state = {
			fields: {}
		}
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleChange = (field, e) => {
		let fields = this.state.fields;
		fields[field] = e.target.value;
		this.setState({ fields: fields });
  }
  handleValidation=()=>{
    const{fields}=this.state;
    let formIsValid=true;
    if(!fields['THRESHOLD']){
     alert(RA_STR.ruleScreenEmpty);
     formIsValid=false;
    }else if(fields['THRESHOLD'] > 1000){
     alert(RA_STR.ruleValidation);
     formIsValid=false;
    }
    if(formIsValid){
      fields['OPERATOR']=this.props.passDataOperator;
      fields['TAGNAME1']=this.props.passDataElement;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='GeneralEqualToScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields['THRESHOLD']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    }else{
      return formIsValid
    }
   
  }
  render() {
    const {fields}=this.state;
    return 	<div id="GeneralEqualToScreen">
		{RA_STR.ruleEnterValue}
		<input className="form-control" name="txt_GeneralEqualToScreen"  id="txt_GeneralEqualToScreen" type="text"  onChange={this.handleChange.bind(this,"THRESHOLD")} value={fields['THRESHOLD']}/></div>;
  }
}

export default GeneralEqualToScreen;
