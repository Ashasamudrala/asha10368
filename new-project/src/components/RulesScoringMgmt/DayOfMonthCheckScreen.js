import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
class DayOfMonthCheckScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { THRESHOLD: '01' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleInputChanges = (fieldId, e) => {
		const { fields } = this.state;
		fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
		this.setState({ fields: fields });
	}
	handleValidation = () => {
		const { fields } = this.state;
		fields['OPERATOR']=this.props.passDataOperator;
		fields['TAGNAME1']=this.props.passDataElement;
		let obj = {};
		let ruleGroup = [];
		obj['ruleKey']='dayMonthScreen';
		obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields['THRESHOLD']}`;
		obj['ruleData'] = fields;
		ruleGroup.push(obj)
		return ruleGroup
	}
	render() {
		const { fields } = this.state;
		return <div id="DayOfMonthCheckScreen" >
			{RA_STR.ruleDayMonth}
			<select className="form-control" id="dayOfMonthNames_DayOfMonthCheckScreen" name="dayOfMonthNames_DayOfMonthCheckScreen" onChange={this.handleInputChanges.bind(this, 'THRESHOLD')} value={fields['THRESHOLD']}>
				<option selected id="1_DayOfMonthCheckScreen" value="01">01</option>
				<option id="2_DayOfMonthCheckScreen" value="02">02</option>
				<option id="3_DayOfMonthCheckScreen" value="03">03</option>
				<option id="4_DayOfMonthCheckScreen" value="04">04</option>
				<option id="5_DayOfMonthCheckScreen" value="05">05</option>
				<option id="6_DayOfMonthCheckScreen" value="06">06</option>
				<option id="7_DayOfMonthCheckScreen" value="07">07</option>
				<option id="8_DayOfMonthCheckScreen" value="08">08</option>
				<option id="9_DayOfMonthCheckScreen" value="09">09</option>
				<option id="10_DayOfMonthCheckScreen" value="10">10</option>
				<option id="11_DayOfMonthCheckScreen" value="11">11</option>
				<option id="12_DayOfMonthCheckScreen" value="12">12</option>
				<option id="13_DayOfMonthCheckScreen" value="13">13</option>
				<option id="14_DayOfMonthCheckScreen" value="14">14</option>
				<option id="15_DayOfMonthCheckScreen" value="15">15</option>
				<option id="16_DayOfMonthCheckScreen" value="16">16</option>
				<option id="17_DayOfMonthCheckScreen" value="17">17</option>
				<option id="18_DayOfMonthCheckScreen" value="18">18</option>
				<option id="19_DayOfMonthCheckScreen" value="19">19</option>
				<option id="20_DayOfMonthCheckScreen" value="20">20</option>
				<option id="21_DayOfMonthCheckScreen" value="21">21</option>
				<option id="22_DayOfMonthCheckScreen" value="22">22</option>
				<option id="23_DayOfMonthCheckScreen" value="23">23</option>
				<option id="24_DayOfMonthCheckScreen" value="24">24</option>
				<option id="25_DayOfMonthCheckScreen" value="25">25</option>
				<option id="26_DayOfMonthCheckScreen" value="26">26</option>
				<option id="27_DayOfMonthCheckScreen" value="27">27</option>
				<option id="28_DayOfMonthCheckScreen" value="28">28</option>
				<option id="29_DayOfMonthCheckScreen" value="29">29</option>
				<option id="30_DayOfMonthCheckScreen" value="30">30</option>
				<option id="31_DayOfMonthCheckScreen" value="31">31</option>
			</select>
		</div>;
	}
}

export default DayOfMonthCheckScreen;
