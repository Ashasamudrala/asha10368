import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
class DistinctScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields:{},
            historyFilter: []
        }
    }
    componentDidMount = async () => {
        const filterUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/additionalHistoryFilter`;
        const filterMap = {
            method: 'GET',
            url: `${serverUrl}${filterUrl}`
        };
        const filterStatus = await getService(filterMap);
        var arrRespParts;
        var res =filterStatus.data;
        var arrRespParts = res.split("#$#");
		if (arrRespParts != undefined && arrRespParts.length >= 2 && "" != arrRespParts[2])
			arrHistoryDistinctAttribs =  arrRespParts[2].split("|");
		else
            var arrHistoryDistinctAttribs = new Array();
            let historyDistinctAttribs=[];
		for(var index=0; index<arrHistoryDistinctAttribs.length; index++){
		let	sarrTemp1 = arrHistoryDistinctAttribs[index].split(":");
		let	sarrTemp2 = sarrTemp1[1].split(";");
			historyDistinctAttribs[sarrTemp1[0]] = sarrTemp2;
		}
        this.setState({ historyFilter: historyDistinctAttribs[this.props.ruleSelectedChannel[0]] });
    }
    handleChange = (field, e) => {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields: fields });
    }
    getFilterData = () => {
        const{fields}=this.state;
        return fields['distinctScreen']!=='NULL' ? fields['distinctScreen'] :'NULL';
    }
    render() {
        const { historyFilter,fields } = this.state;
        return <select id="distinct_VelocityScreen" className="form-control" value={this.props.defaultFilter !=="NULL" ? this.props.defaultFilter : fields['distinctScreen']!=='NULL' ? fields['distinctScreen'] :'NULL'} onChange={this.handleChange.bind(this, "distinctScreen")}>
            <option value='NULL'>{RA_STR.ruleNone}</option>
            {historyFilter.map((eachFilter) => {
                return <option value={eachFilter}>{eachFilter}</option>
            })}
        </select>
    }
}
export default DistinctScreen;