import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
import ReactDOM from 'react-dom';
import './RulesScreens.scss';
class ActionVelocityScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { VELOCITY_DURATION_UNIT: 'MINUTES' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields },()=>{
				let node = ReactDOM.findDOMNode(this.refs['actionsSelect_ActionVelocityScreen']);
				var options = node && node.querySelectorAll('option');
				for (var i = 0; i < options.length; i++) {
					if (this.props.ruleFields.actionVelocity.includes(options[i].value)) {
						options[i].selected=true;
					}
				}
			});
		}
	}
	handleRuleValidation = (e) => {
		var val = e.target.value;
		if (CA_Utils.checkForInteger(val, 1, 65535) == false) {
			e.target.value = "";
			alert(RA_STR.ruleDurationLimit);
			return false;
		}
		return true;
	}
	handleValidation = () => {
		let fields = this.state.fields;
		let formIsValid = true;
		let node = ReactDOM.findDOMNode(this.refs['actionsSelect_ActionVelocityScreen']);
		var options = node && node.querySelectorAll('option');
		let groupSelected = [];
		for (var i = 0; i < options.length; i++) {
			if (options[i].selected) {
				groupSelected.push(options[i].text);
			}
		}
		if (groupSelected.length === 0) {
			formIsValid = false;
			alert(RA_STR.ruleScreenAction);
			return
		}
		if (!fields["VELOCITY_TRANSACTION_COUNT"]) {
			formIsValid = false;
			alert(RA_STR.ruleScreenActionCount);
			return
		}
		if (!fields["VELOCITY_DURATION"]) {
			formIsValid = false;
			alert(RA_STR.ruleScreenInterval);
			return
		}
		fields['ACTION']=groupSelected;
		if (formIsValid) {
			let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='actionVelocityScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} FOR [${groupSelected.join('|')}] GREATER_THAN OR EQUAL TO ${fields['VELOCITY_TRANSACTION_COUNT']} IN ${fields['VELOCITY_DURATION']} ${fields['VELOCITY_DURATION_UNIT']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
		} else {
			return formIsValid;
		}
	}
	handleInputChanges = (fieldId, e) => {
		const { fields } = this.state;
		if(fieldId === "ACTION"){
			fields[fieldId] =  e.target.value;
			let node = ReactDOM.findDOMNode(this.refs['actionsSelect_ActionVelocityScreen']);
			var options = node && node.querySelectorAll('option');
			for (var i = 0; i < options.length; i++) {
				if (options[i].selected) {
					options[i].selected=true;
				}
			}
		}else{
			fields[fieldId] =  e.target.value;
		}
		this.setState({ fields: fields });
	}
	render() {
		const {fields}=this.state;
		return <div id='ActionVelocityScreen'>
			<table>
				<tr>
					<td>{RA_STR.ruleGreater}</td>
					<td>
						<input id='velocity_VelocityScreen' className="form-control" name='velocity_VelocityScreen' type='text' onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'VELOCITY_TRANSACTION_COUNT')} value={fields['VELOCITY_TRANSACTION_COUNT']}/>
					</td>
				</tr>
				<br></br>
				<tr>
					<td>{RA_STR.ruleLast}</td>
					<td>
						<input id='time_VelocityScreen' className="form-control" name='time_VelocityScreen' type='text' onBlur={() => this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'VELOCITY_DURATION')} value={fields['VELOCITY_DURATION']}/>
					</td>
					<td>
						<select id="timeUnit_VelocityScreen" className="form-control" ref='timeUnit_VelocityScreen' onChange={this.handleInputChanges.bind(this, 'VELOCITY_DURATION_UNIT')} value={fields['VELOCITY_DURATION_UNIT']}>
							<option value='MINUTES'>{RA_STR.ruleMins} </option>
							<option value='HOURS'>{RA_STR.ruleHours}</option>
							<option value='DAYS'>{RA_STR.ruleDays}</option>
						</select>
					</td>
				</tr>
				<br></br>
				<tr>
					<td>{RA_STR.ruleSetActions}</td>
					<td>
						<select multiple="multiple" id="actionsSelect_ActionVelocityScreen" name="actionsSelect_actionVelocityScreen" ref="actionsSelect_ActionVelocityScreen" onChange={this.handleInputChanges.bind(this, 'ACTION')}>
							{
								this.props.actionData && this.props.actionData.map((opt) => {
									return (
										<option
											key={opt.txTypeId}
											value={opt.txType}>{opt.txType}</option>
									);
								})
							}
						</select>
					</td>
				</tr>
				<br></br>
				<tr>
					<td colspan="2" dangerouslySetInnerHTML={{ __html: RA_STR.ruleKick }}></td>
				</tr>
			</table>
		</div>;
	}
}

export default ActionVelocityScreen;
