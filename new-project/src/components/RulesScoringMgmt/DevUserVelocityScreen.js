import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class DevUserVelocityScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { TIME_INTERVAL_UNIT: 'DAYS' }
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleRuleValidation = (e) => {
    var val = e.target.value;
    if (val === '') {
      alert(RA_STR.ruleValidationMsg);
    }
    return true;
  }
  handleValidation = () => {
    let fields = this.state.fields;
    let formIsValid = true;
    if (!fields["USER_COUNT"]) {
      formIsValid = false;
      alert(RA_STR.ruleVelocity);
      return
    }
    if (!fields["TIME_INTERVAL"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenInterval);
      return
    }
    if (formIsValid) {
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='devuserVelocityScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator}  GREATER_THAN OR EQUAL TO ${fields['USER_COUNT']} IN ${fields['TIME_INTERVAL']} ${fields['TIME_INTERVAL_UNIT']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid;
    }
  }
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  render() {
    const{fields}=this.state;
    return <div id='DevUserVelocityScreen'>
      <table>
        <tr>
          <td>
            {RA_STR.ruleGreaterEqual}
          </td>
          <td>
            <input className="form-control" id='velocity_DevUserVelocityScreen' name='velocity_DevUserVelocityScreen' type='text' maxLength='3' size='3' onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'USER_COUNT')} value={fields['USER_COUNT']}/>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleLast}
          </td>
          <td>
            <input className="form-control" id='time_DevUserVelocityScreen' name='time_DevUserVelocityScreen' type='text' maxLength='3' size='3' onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'TIME_INTERVAL')} value={fields['TIME_INTERVAL']}/>
          </td>
          <td>
            <select id="timeUnit_DevUserVelocityScreen" className="form-control" onChange={this.handleInputChanges.bind(this, 'TIME_INTERVAL_UNIT')} value={fields['TIME_INTERVAL_UNIT']}>
              <option value='MINUTES'>{RA_STR.ruleMins} </option>
              <option value='HOURS'>{RA_STR.ruleHours}</option>
              <option value='DAYS'>{RA_STR.ruleDays}</option>
            </select>
          </td>
        </tr>
      </table>
      <table>
        <tr>
          <td >
            <p dangerouslySetInnerHTML={{ __html:RA_STR.ruleUserVelocity}}></p>
          </td>
        </tr>
      </table>
    </div>
  }
}

export default DevUserVelocityScreen;
