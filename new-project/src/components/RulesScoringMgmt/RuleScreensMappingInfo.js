const CA_RULE_SCREEN_Utils = {
    STRING_COMPARE_SCREEN: {
        finalize: (params) => {
            var operMap = { "MATCHES": "CONTAINS" };
            params["OPERATOR"] = operMap[params["OPERATOR"]];
            params["THRESHOLD"] = encodeURIComponent(params["THRESHOLD"]);
            return params;
        }
    },
    IN_LIST_SCREEN: {
        finalize: (params) => {
            var tempValue = params["SIMPLEINLISTVALUE"];
            if (undefined === tempValue || 'undefined' === tempValue) {
                tempValue = "";
            }
            params["SIMPLEINLISTVALUE"] = encodeURIComponent(tempValue);
            return params;
        }
    },
    AMOUNT_COMPARE_SCREEN: {
        finalize: (params) => {
            var operMap = { "EQUAL_TO" : "EQ", "NOT_EQUAL_TO" : "NEQ", "GREATER_OR_EQUAL" : "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL" : "LTE", "LESS_THAN" : "LT", "NOT_EQUAL_TO" : "NEQ" };
            params["OPERATOR"] = operMap[params["OPERATOR"]];				
            
            var threshold = params["THRESHOLD"];
            var currData =  threshold.split("[")[1].split("]")[0].split(",");
            threshold = "";
            for(var t= 0; t < currData.length; ++t) {
                if(currData[t].trim() == "")
                    continue;
                var d = currData[t].split("_");
                var s = d[0] + ":" + params['allCurrenciesReverse'][d[1]];
                threshold += s + "|";
            }
            params["THRESHOLD"] = threshold;	
            return params;
        }
    },
    AMOUNT_CB_COMPARE_SCREEN:{
        finalize: (params) => {
            var operMap = { "EQUAL_TO_CB" : "EQ_CB", "NOT_EQUAL_TO_CB" : "NEQ_CB", "GREATER_OR_EQUAL_CB" : "GTE_CB", "GREATER_THAN_CB": "GT_CB", "LESS_OR_EQUAL_CB" : "LTE_CB", "LESS_THAN_CB" : "LT_CB"};
            params["OPERATOR"] = operMap[params["OPERATOR"]];				
            
            var threshold = params["THRESHOLD"];
            var currData =  threshold.split("[")[1].split("]")[0].split(",");
            threshold = "";
            for(var t= 0; t < currData.length; ++t) {
                if(currData[t].trim() == "")
                    continue;
                var d = currData[t];
                threshold += d;
            }
            params["THRESHOLD"] = threshold;	
            return params;
        }   
    }

}
export default CA_RULE_SCREEN_Utils;
