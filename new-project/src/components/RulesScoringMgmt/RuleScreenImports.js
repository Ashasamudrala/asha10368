import VelocityScreen from './VelocityScreen';
import InDerivedListScreen from './InDerivedListScreen';
import IPVelocityScreen from './IPVelocityScreen';
import AmountCompareScreen from './AmountCompareScreen';
import StringCompareScreen from './StringCompareScreen';
import ZoneHoppingScreen from './ZoneHoppingScreen';
import CountryInListScreen from './CountryInListScreen';
import IPAddressNegativeListScreen from './IPAddressNegativeListScreen';
import CustomScreen1 from './CustomScreen1';
import CustomScreen2 from './CustomScreen2';
import CustomInDerivedListScreen from './CustomInDerivedListScreen';
import GeneralEqualToScreen from './GeneralEqualToScreen';
import DeviceMFPMatchedScreen from './DeviceMFPMatchedScreen';
import TxTypeCheckScreen from './TxTypeCheckScreen';
import ActionVelocityScreen from './ActionVelocityScreen';
import CurrentTimeCheckScreen from './CurrentTimeCheckScreen';
import MonthCheckScreen from './MonthCheckScreen';
import YearCheckScreen from './YearCheckScreen';
import DayOfMonthCheckScreen from './DayOfMonthCheckScreen';
import HourOfDayCheckScreen from './HourOfDayCheckScreen';
import DateCheckScreen from './DateCheckScreen';
import DateTimeCheckScreen from './DateTimeCheckScreen';
import AmountBinPrefixCompareScreen from './AmountBinPrefixCompareScreen';
import CumulativeAmountCheckScreen from './CumulativeAmountCheckScreen';
import ActionCumulativeAmountCheckScreen from './ActionCumulativeAmountCheckScreen';
import GroupedCumulativeAmountCheckScreen from './GroupedCumulativeAmountCheckScreen';
import HistoryUserVelocity from './HistoryUserVelocity';
import HistoryZoneHoppingScreen from './HistoryZoneHoppingScreen';
import DevUserVelocityScreen from './DevUserVelocityScreen';
import DevUserMaturityScreen from './DevUserMaturityScreen';
import GdpRangeScreen from './gdpRangeScreen';
import UserProfileScreen from './UserProfileScreen';
import ListScreen from './InListScreen';
const ruleScreens = {
    VELOCITY_SCREEN: VelocityScreen,
    IP_VELOCITY_SCREEN:IPVelocityScreen,
    IN_LIST_SCREEN:ListScreen,
    IN_DERIVED_LIST_SCREEN:InDerivedListScreen,
    AMOUNT_COMPARE_SCREEN:AmountCompareScreen,
    STRING_COMPARE_SCREEN:StringCompareScreen,
    ZONE_HOPPING_SCREEN:ZoneHoppingScreen,
    COUNTRY_IN_LIST_SCREEN:CountryInListScreen,
    IN_NEGATIVE_LIST_SCREEN:IPAddressNegativeListScreen,
    CUSTOM_SCREEN_1:CustomScreen1,
    CUSTOM_SCREEN_2:CustomScreen2,
    CUSTOM_IN_DERIVED_LIST_SCREEN:CustomInDerivedListScreen,
    NUMERIC_COMPARISON_SCREEN:GeneralEqualToScreen,
    DEVICEMFPMATCHED_SCREEN:DeviceMFPMatchedScreen,
    TXTYPECHECK_SCREEN:TxTypeCheckScreen,
    ACTIONVELOCITY_SCREEN:ActionVelocityScreen,
    CURRENTTIME_CHECK_SCREEN:CurrentTimeCheckScreen,
    MONTH_CHECK_SCREEN:MonthCheckScreen,
    YEAR_CHECK_SCREEN:YearCheckScreen,
    DAYOFMONTH_CHECK_SCREEN:DayOfMonthCheckScreen,
    HOUROFDAY_CHECK_SCREEN:HourOfDayCheckScreen,
    DATE_CHECK_SCREEN:DateCheckScreen,
    DATE_TIME_CHECK_SCREEN:DateTimeCheckScreen,
    AMOUNT_CB_COMPARE_SCREEN:AmountBinPrefixCompareScreen,
    CUMULATIVE_AMOUNT_SCREEN:CumulativeAmountCheckScreen,
    ACTION_CUMULATIVE_AMOUNT_SCREEN:ActionCumulativeAmountCheckScreen,
    GROUPED_CUMULATIVE_AMOUNT_CHECK_SCREEN:GroupedCumulativeAmountCheckScreen,
    HISTORY_USER_VELOCITY_SCREEN:HistoryUserVelocity,
    HISTORY_ZONE_HOPPING_SCREEN:HistoryZoneHoppingScreen,
    DEV_USER_VELOCITY_SCREEN:DevUserVelocityScreen,
    DEV_USER_MATURITY_SCREEN:DevUserMaturityScreen,
    RANGE_SCREEN:GdpRangeScreen,
    USER_PROFILE_SCREEN:UserProfileScreen
}
export default ruleScreens