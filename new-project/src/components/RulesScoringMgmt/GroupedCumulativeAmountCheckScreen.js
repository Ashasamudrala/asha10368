import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import AdditionalFilter from './AdditionalFilter';
class GroupedCumulativeAmountCheckScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields : { HISTTRANSCTIONSTATUS: 'All',COMBINATION_STR_EXT:'NULL' }
    }
  }
  componentDidMount = () => {
    if (this.props.ruleFields) {
      this.setState({ fields: this.props.ruleFields });
    }
  }
  handleChange = (field, e) => {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields['THRESHOLD']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenCumulative);
      return
    }
    if (!fields['GROUPINGDURATION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenGrpPeriod);
      return
    }
    if (!fields['MINTHRESHOLDBREACHCOUNT']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenBreach);
      return
    }
    if (!fields['DURATION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenLookBackMin);
      return
    }
    if (!fields['MAXTRANSACTION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenMaxTnx);
      return
    }
    if (!fields['STARTTIME']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenStartTime);
      return
    }
    if (!fields['ENDTIME']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenEndTime);
      return
    }
    if (formIsValid) {
      let obj = {};
      let ruleGroup = [];
      let getFilterItem = this.refs.additionalFilter.getFilterData();
      fields['COMBINATION_STR_EXT']=getFilterItem;
      obj['ruleKey'] = 'groupedCumulative';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields['THRESHOLD']} ${fields['GROUPINGDURATION']} ${fields['MINTHRESHOLDBREACHCOUNT']} ${fields['DURATION']} ${fields['MAXTRANSACTION']} ${fields['STARTTIME']} ${fields['ENDTIME']} ${fields['HISTTRANSCTIONSTATUS']} ${fields['additionalFilter']}`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj)
      return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const {fields}=this.state;
    return <div id='GroupedCumulativeAmountCheckScreen'>
      <table>
        <tr>
          <td>
            {RA_STR.ruleCummulativeAmount}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_amount" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "THRESHOLD")} value={fields['THRESHOLD']}/>
          </td>

        </tr>
        <tr>
          <td>
            {RA_STR.ruleGroupPeriod}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_groupingDuration" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "GROUPINGDURATION")} value={fields['GROUPINGDURATION']}/>
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleThresholdBreach}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_minThresholdBreach" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "MINTHRESHOLDBREACHCOUNT")} value={fields['MINTHRESHOLDBREACHCOUNT']}/>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleLookBack}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_duration" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "DURATION")} value={fields['DURATION']}/>
          </td>

        </tr>
        <tr>
          <td>
            {RA_STR.ruleMaxTnxCount}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_maxTransactionLookBack" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "MAXTRANSACTION")} value={fields['MAXTRANSACTION']}/>
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxStartTime}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_startTime" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "STARTTIME")} value={fields['STARTTIME']}/>
          </td>

        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxEndTime}
          </td>
          <td>
            <input id="groupedCumulativeAmountCheckScreen_endTime" className="form-control" type='text' maxlength="100" onChange={this.handleChange.bind(this, "ENDTIME")} value={fields['ENDTIME']}/>
          </td>

        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxStatus}
          </td>
          <td>
            <select className="form-control" id="histTransactionStatus_VelocityScreen" onChange={this.handleChange.bind(this, "HISTTRANSCTIONSTATUS")} value={fields['HISTTRANSCTIONSTATUS']}>
              <option value='ALL'>{RA_STR.ruleTnxStatusAll}</option>
              <option value='APPROVED'>{RA_STR.ruleTnxStatusApproved}</option>
              <option value='DENIED'>{RA_STR.ruleTnxStatusDenied}</option>
            </select>
          </td>
        </tr>
        <tr>
        <td>
            {RA_STR.ruleAddFilter}
          </td>
          <td>
          <AdditionalFilter ruleOrgName={this.props.ruleOrgName} defaultFilter={fields['COMBINATION_STR_EXT']} ref="additionalFilter"></AdditionalFilter>
          </td>
        </tr>
      </table>
    </div >;
  }
}

export default GroupedCumulativeAmountCheckScreen;
