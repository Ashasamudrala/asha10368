import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
class CustomScreen2 extends Component {
	constructor(props) {
    super(props);
    this.state = {
      fields: {MATCHTYPE:'EXACT'}
    }
  }
  componentDidMount = () => {
	if (this.props.ruleFields) {
		this.setState({ fields: this.props.ruleFields });
	}
}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
	}
	handleValidation = () => {
    let fields = this.state.fields;
    let formIsValid = true;
    if (!fields['TAGNAME']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenTagName);
      return
    } else if (!fields['LISTDATASET']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenUniqueIdentity);
      return
    } 
    if (formIsValid) {
			let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='customscreen2';
			obj['ruleExpression'] = `${fields['TAGNAME']} IN_LIST ${fields['LISTDATASET']} WITH MATCHTYPE = ${fields['MATCHTYPE']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
	  const {fields}=this.state;
    return <div id="CustomScreen2">
		<div id="tabHeader_customScreen2" >
			<table>
				<tr>
					<td>{RA_STR.ruleTagName}
					</td>
					<td><input className="form-control" type="text" size="30" id="tagname_customScreen2" onChange={this.handleInputChanges.bind(this, 'TAGNAME')} value={fields['TAGNAME']}/>
					</td>
				</tr>
				<br></br>
				<tr>
					<td>{RA_STR.ruleIdentityListName}</td>
					<td><input className="form-control" type="text" size="30" id="identifier_customScreen2" onChange={this.handleInputChanges.bind(this, 'LISTDATASET')} value={fields['LISTDATASET']}/>
					</td>
				</tr>
				<br></br>
				<tr>
					<td>{RA_STR.ruleMatchType}
					</td>
					<td>
						<select className="form-control" id="matchtype_customScreen2" onChange={this.handleInputChanges.bind(this, 'MATCHTYPE')} value={fields['MATCHTYPE']}>
							<option id="exact" value="EXACT">{RA_STR.ruleExactMatch}</option>
							<option id="partial" value="PARTIAL">{RA_STR.rulePartialMatch}</option>
						</select>
					</td>
				</tr>
			</table>
		</div>
	</div>;
  }
}

export default CustomScreen2;
