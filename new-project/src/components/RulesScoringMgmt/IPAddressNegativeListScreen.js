import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class IPAddressNegativeListScreen extends Component {
  constructor(props) {
		super(props);
		this.state = {
			fields: { AR_NEGATIVE: 0,ACTIVE:0,SUSPECT:0,PRIVATE:0,INACTIVE:0,UNKNOWN:0}
		}
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleChange = (field, e) => {
		let fields = this.state.fields;
		fields[field] = e.target.checked ? 1 : 0;
		this.setState({ fields: fields });
  }
  handleValidation=()=>{
    const{fields}=this.state;
    let obj = {};
    let ruleGroup = [];
    obj['ruleKey']='ipAddressNegative';
    obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} __SYS__UNTRUSTEDIP_62470 WITH NEGATIVE IP TYPES AS ACTIVE = ${fields['ACTIVE']} & NEGATIVE = ${fields['AR_NEGATIVE']} & PRIVATE = ${fields['PRIVATE']} & SUSPECT = ${fields['SUSPECT']} & INACTIVE = ${fields['INACTIVE']} & UNKNOWN = ${fields['UNKNOWN']}`;
    obj['ruleData'] = fields;
    ruleGroup.push(obj)
    return ruleGroup
  }
  render() {
    const {fields}=this.state;
    return <div id="IPAddressNegativeListScreen">
      <div id="negativeIPTypesTable_IPAddressNegativeListScreen">
        <table class="reportTable edl_table">
          <thead>
            <tr>
              <th>{RA_STR.ruleNegativeIp}</th>
              <th>{RA_STR.ruleActive}</th>
              <th>{RA_STR.ruleSuspectIp}</th>
              <th>{RA_STR.rulePrivate}</th>
              <th>{RA_STR.ruleInactive}</th>
              <th>{RA_STR.ruleUnknown}</th>
            </tr>
          </thead>
          <tbody>
            <tr className="edl_checkbox">
              <td >
                <label className="edl_chb">
                  <input type="checkbox" id="proposedNegativeType" value="NEGATIVE" onChange={this.handleChange.bind(this,"AR_NEGATIVE")} checked={fields['AR_NEGATIVE'] ? true : false}/>
                  <span></span>
                </label>
              </td>
              <td>
                <label className="edl_chb">
                  <input type="checkbox" id="proposedActiveType" value="ACTIVE" onChange={this.handleChange.bind(this,"ACTIVE")} checked={fields['ACTIVE'] ? true : false}/>
                  <span></span>
                </label>
              </td>
              <td >
                <label className="edl_chb">
                  <input type="checkbox" id="proposedSuspectType" value="SUSPECT" onChange={this.handleChange.bind(this,"SUSPECT")} checked={fields['SUSPECT'] ? true : false}/>
                  <span></span>
                </label>
              </td>
              <td>
                <label className="edl_chb">
                  <input type="checkbox" id="proposedPrivateType" value="PRIVATE" onChange={this.handleChange.bind(this,"PRIVATE")} checked={fields['PRIVATE'] ? true : false}/>
                  <span></span>
                </label>
              </td>
              <td>
                <label className="edl_chb">
                  <input type="checkbox" id="proposedInactiveType" value="INACTIVE" onChange={this.handleChange.bind(this,"INACTIVE")} checked={fields['INACTIVE'] ? true : false}/>
                  <span></span>
                </label>
              </td>
              <td>
                <label className="edl_chb">
                  <input type="checkbox" id="proposedUnknownType" value="UNKNOWN" onChange={this.handleChange.bind(this,"UNKNOWN")} checked={fields['UNKNOWN'] ? true : false}/>
                  <span></span>
                </label>
              </td>
            </tr>
          </tbody>
        </table>
      </div></div>;
  }
}

export default IPAddressNegativeListScreen;
