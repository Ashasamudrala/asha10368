import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class DateTimeCheckScreen extends Component {
  render() {
    return 	<div id="DateTimeCheckScreen">
		<table>
			<tr>
				<td>
					{RA_STR.ruleDateEnter}
				</td>
				<td>
					<input name="txt_DateValueCheckScreen" className="form-control"  id="txt_DateValueCheckScreen" type="text"  onblur=""/>
				</td>
			</tr>
			<tr>
				<td>
					{RA_STR.ruleEnterTime}
				</td>
				<td>
					<input name="txt_TimeValueCheckScreen" className="form-control"  id="txt_TimeValueCheckScreen" type="text"  onblur=""/>
				</td>
			</tr>
		</table>
	</div>;
  }
}

export default DateTimeCheckScreen;
