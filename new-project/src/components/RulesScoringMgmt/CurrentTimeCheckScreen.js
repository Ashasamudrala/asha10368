import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class CurrentTimeCheckScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {}
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleRuleValidation = (e) => {
    var val = e.target.value;
    if (val.length > 4 || val.length < 4) {
      alert(RA_STR.ruleScreenValidate4);
      e.target.value = "";
      return
    } else {
      if(val){
        let splitDigits = this.splitToNumbers(val);
        if(splitDigits[0] > 24){
          alert(RA_STR.ruleScreenHour);
          e.target.value = "";
        }else if(splitDigits[1] > 60){
          alert(RA_STR.ruleScreenMinute);
          e.target.value = "";
        }
      }
    }
  }
  splitToNumbers = (str) => {
    return str.match(str.length % 2 ? /^\d|\d{2}/g : /\d{2}/g).map(Number)
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["THRESHOLD"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenValidate4);
      return
    }
    if (formIsValid) {
      fields['OPERATOR']=this.props.passDataOperator;
      fields['TAGNAME1']=this.props.passDataElement;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='velocityScreen';
			obj['ruleExpression'] =`${this.props.passDataElement} ${this.props.passDataOperator} ${fields["THRESHOLD"]}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const {fields}=this.state;
    return <table id="CurrentTimeCheckScreen">
      <tr>
        <td>{RA_STR.ruleEnterTime}</td>
        <td><input name="txt_CurrentTimeCheckScreen" className="form-control" id="txt_CurrentTimeCheckScreen" type="text" onChange={this.handleInputChanges.bind(this, 'THRESHOLD')} onBlur={this.handleRuleValidation} value={fields['THRESHOLD']}/></td>
      </tr>
    </table>;
  }
}

export default CurrentTimeCheckScreen;
