import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
class AdditionalFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields:{},
            historyFilter: []
        }
    }
    componentDidMount = async () => {
        const filterUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/additionalHistoryFilter`;
        const filterMap = {
            method: 'GET',
            url: `${serverUrl}${filterUrl}`
        };
        const filterStatus = await getService(filterMap);
        if(filterStatus.status === 200 && filterStatus.data){
        var arrRespParts, historyFilterList, arrRBFlags;
        var historyFilterData = [];
        // var rulebuilderConfiguration = [];
        var res =filterStatus.data;
        var arrRespParts = res.split("#$#");
        if (arrRespParts != undefined && arrRespParts.length >= 0 && "" != arrRespParts[0]) {
            arrRBFlags = arrRespParts[0].split("|");
        } else
            arrRBFlags = new Array();
        // for (var index = 0; index < arrRBFlags.length; index++) {
        //     var sarrTemp1 = arrRBFlags[index].split(":");
        //     if (sarrTemp1.length >= 2)
        //         rulebuilderConfiguration[sarrTemp1[0]] = sarrTemp1[1];
        // }
        if (arrRespParts != undefined && arrRespParts.length >= 1 && "" != arrRespParts[1])
            historyFilterList = arrRespParts[1].split("|");
        else
            historyFilterList = new Array();
        for (var index = 0; index < historyFilterList.length; index++) {
            var filter = historyFilterList[index].split(":");
            if (filter.length >= 2) {
                var filterObject = new Object();
                filterObject.name = filter[0];
                filterObject.value = filter[1];
                historyFilterData[index] = filterObject;
            }
        }
        this.setState({ historyFilter: historyFilterData });
    }
    }
    handleChange = (field, e) => {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields: fields });
    }
    getFilterData = () => {
        const{fields}=this.state;
        return fields['HISTORY_OF']!=='NULL' ? fields['HISTORY_OF'] :'NULL';
    }
    render() {
        const { historyFilter,fields } = this.state;
        return <select id="combinationStrExt_VelocityScreen" className="form-control" value={this.props.defaultFilter !=="NULL" ? this.props.defaultFilter : fields['HISTORY_OF']!=='NULL' ? fields['HISTORY_OF'] :'NULL'} onChange={this.handleChange.bind(this, "HISTORY_OF")}>
            <option value='NULL'>{RA_STR.ruleNone}</option>
            {historyFilter.map((eachFilter) => {
                return <option value={eachFilter.value}>{eachFilter.name}</option>
            })}
        </select>
    }
}
export default AdditionalFilter;