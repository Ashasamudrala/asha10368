import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
class CustomScreen1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {}
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    let fields = this.state.fields;
    let formIsValid = true;
    if (!fields['TAGNAME1']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenElementName);
      return
    } else if (!fields['THRESHOLD']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenElementValue);
      return
    } else if (isNaN(fields['THRESHOLD'])) {
      formIsValid = false;
      alert(RA_STR.ruleScreenCombo);
      return
    }
    if (formIsValid) {
      fields['OPERATOR']=this.props.this.props.passDataOperator;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='customscreen1';
			obj['ruleExpression'] = `${fields['TAGNAME1']} GREATER_THAN ${fields['THRESHOLD']}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
    } else {
       return false
    }
  }
  render() {
    const {fields}=this.state;
    return <div id="CustomScreen1">
      <div></div>
      <table >
        <tr>
          <td>
            {RA_STR.ruleElemName}
          </td>
          <td>
            <input className="form-control" type="text"  size="30" name="tagname_customScreen1" id="tagname_customScreen1" onChange={this.handleInputChanges.bind(this, 'TAGNAME1')} value={fields['TAGNAME1']}/>
          </td>
        </tr>
        <br></br>
        <tr >
          <td>
            {RA_STR.ruleElemValue}
          </td>
          <td>
            <input className="form-control" name="value_customScreen1" id="value_customScreen1" type="text" size="30" onChange={this.handleInputChanges.bind(this, 'THRESHOLD')} value={fields['THRESHOLD']}/>
          </td>
        </tr>
      </table>
    </div>;
  }
}

export default CustomScreen1;
