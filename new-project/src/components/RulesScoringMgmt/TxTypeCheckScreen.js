import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import ReactDOM from 'react-dom';
class TxTypeCheckScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { dataset: '' },
			ruleSelectAction: {},
		}
	}
	componentDidMount = async () => {
		const { fields } = this.state;
		const txTypeDisplay = `/organization/${this.props.ruleOrgName}/rulesandscore/ruletxtypecheckdisplay`;
		const txTypeDisplayMap = {
			method: 'GET',
			url: `${serverUrl}${txTypeDisplay}`
		};
		const txTypeDisplayMapStatus = await getService(txTypeDisplayMap);
		let allTxnTypeCheckMap = {};
		if (txTypeDisplayMapStatus.status === 200) {
			var opList = txTypeDisplayMapStatus.data;
			var arr = opList.split(";");
			for (var arri = 0; arri < arr.length; ++arri) {
				if (arr[arri].trim() == "")
					continue;
				var arr2 = arr[arri].split(":");
				var key = arr2[0];
				var value = arr2[1];
				allTxnTypeCheckMap[key] = value;
			}
			let getAllFieldData = fields;
			getAllFieldData['dataSet'] = allTxnTypeCheckMap[0];
			this.setState({ ruleSelectAction: allTxnTypeCheckMap, fields: getAllFieldData });
		}
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields },()=>{
				let node = ReactDOM.findDOMNode(this.refs['tnxTypeChekScreen']);
				var options = node && node.querySelectorAll('option');
				for (var i = 0; i < options.length; i++) {
					if (this.props.ruleFields.actions.includes(options[i].value)) {
						options[i].selected=true;
					}
				}
			});
		}
	}
	handleInputChanges = (fieldId, e) => {
		const { fields } = this.state;
		if(fieldId === "PREVTXNTYPES"){
			fields[fieldId] =  e.target.value;
			let node = ReactDOM.findDOMNode(this.refs['tnxTypeChekScreen']);
			var options = node && node.querySelectorAll('option');
			for (var i = 0; i < options.length; i++) {
				if (options[i].selected) {
					options[i].selected=true;
				}
			}
		}else{
			fields[fieldId] =  e.target.value;
		}
		
		this.setState({ fields: fields });
	}
	handleRuleValidation = (e) => {
		var val = e.target.value;
		if (CA_Utils.checkForInteger(val, 1, 65535) == false) {
			e.target.value = "";
			alert(RA_STR.ruleDurationLimit);
			return false;
		}
		return true;
	}
	handleValidation = () => {
		let fields = this.state.fields;
		let formIsValid = true;
		let node = ReactDOM.findDOMNode(this.refs['tnxTypeChekScreen']);
		var options = node && node.querySelectorAll('option');
		let groupSelected = [];
		for (var i = 0; i < options.length; i++) {
			if (options[i].selected) {
				groupSelected.push(options[i].text);
			}
		}
		if (groupSelected.length === 0) {
			formIsValid = false;
			alert(RA_STR.ruleScreenAction);
			return
		}
		if (!fields["DURATION"]) {
			formIsValid = false;
			alert(RA_STR.ruleDurationLimit);
			return
		}
		fields['PREVTXNTYPES']=groupSelected;
		if (formIsValid) {
			let obj = {};
			let ruleGroup = [];
			obj['ruleKey'] = 'txnTypeScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} FOR [${groupSelected.join('|')}] ${fields['DURATION']} IN HOURS`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
		}
		return formIsValid;
	}
	render() {
		const { ruleSelectAction, fields } = this.state;
		return <div id="TxTypeCheckScreen">
			<table>
				<tr>
					{/* <td>{RA_STR.ruleEnterDuration}</td> */}
					<td><input className="form-control" type="text" size="3" onblur={this.handleRuleValidation} onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'DURATION')} name="duration_TxtypeCheckScreen" id="duration_TxtypeCheckScreen" value={fields['DURATION']} /></td>
					<td>HOURS</td>
				</tr>
				<tr>
					{/* <td>{RA_STR.ruleSelectAction}</td> */}
					<td>
						<select multiple="multiple" className="form-control" onChange={this.handleInputChanges.bind(this,'PREVTXNTYPES')} name="previousTxnTypes_TxTypeInListScreen" id="previousTxnTypes_TxTypeInListScreen" ref="tnxTypeChekScreen">
							{Object.keys(ruleSelectAction).map((eachAction) => {
								return <option value={ruleSelectAction[eachAction]}>{ruleSelectAction[eachAction]}</option>
							})
							}
						</select>
					</td>
				</tr>
			</table></div>;
	}
}

export default TxTypeCheckScreen;
