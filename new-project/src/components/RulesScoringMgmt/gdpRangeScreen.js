import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class GdpRangeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {}
        }
    }
    componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
    handleRuleValidation = (e) => {
        var val = e.target.value;
        if (e.target.name === "LOWERLIMIT" && val > 1000) {
            alert(RA_STR.ruleScreenLimitLower);
            e.target.value = '';
            return
        } else if (e.target.name === "UPPERLIMIT" && val > 1000) {
            alert(RA_STR.ruleScreenUpperLimit);
            e.target.value = '';
            return
        }
    }
    handleValidation = () => {
        let fields = this.state.fields;
        let formIsValid = true;
        if (!fields["LOWERLIMIT"]) {
            formIsValid = false;
            alert(RA_STR.ruleScreenLimitLower);
            return
        } else if (!fields["UPPERLIMIT"]) {
            formIsValid = false;
            alert(RA_STR.ruleScreenUpperLimit);
            return
        }else if (parseInt(fields["UPPERLIMIT"]) < parseInt(fields["LOWERLIMIT"])) {
            formIsValid = false;
            alert(RA_STR.ruleScreenEqual);
            return
        }
        if (formIsValid) {
            fields['TAGNAME']=this.props.passDataElement;
            let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='gdpRange';
			obj['ruleExpression'] =`${this.props.passDataElement} ${this.props.passDataOperator} ${fields["LOWERLIMIT"]} and ${fields["UPPERLIMIT"]}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
        } else {
            return formIsValid
        }
    }
    handleInputChanges = (fieldId, e) => {
        const { fields } = this.state;
        fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
        this.setState({ fields: fields });
    }
    render() {
        const{fields}=this.state;
        return <div id='gdpRangeScreen'>
            <table>
                <tr>
                    <td>
                        {RA_STR.ruleLowerLimit}
                    </td>
                    <td>
                        <input className="form-control" name="LOWERLIMIT" id="gdpRangeScreen_lowerLimit" type='text' maxlength="5" size='5' onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'LOWERLIMIT')} value={fields['LOWERLIMIT']}/>
                    </td>

                </tr>
                <tr>
                    <td>
                        {RA_STR.ruleUpperLimit}
                    </td>
                    <td>
                        <input className="form-control" name="upperLimit" id="gdpRangeScreen_upperLimit" type='text' maxlength="5" size='5' onBlur={this.handleRuleValidation} onChange={this.handleInputChanges.bind(this, 'upperLimit')} value={fields['upperLimit']}/>
                    </td>
                </tr>
            </table>

        </div>;
    }
}

export default GdpRangeScreen;
