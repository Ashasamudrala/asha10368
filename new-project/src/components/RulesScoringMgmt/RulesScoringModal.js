import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import TextArea from '../../shared/components/TextArea/TextArea';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import ReactDOM from 'react-dom';
import { RA_STR } from '../../shared/utlities/messages';
import Loader from '../../shared/utlities/loader';
import { serverUrl, RA_STR_SELECT } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import _ from 'underscore';
import ReactTooltip from 'react-tooltip';
import ruleScreens from './RuleScreenImports';
class RulesScoringModal extends Component {
    constructor(props) {
        super(props);
        this.count = 0;
        this.storeRuleMneomnic = '';
        this.state = {
            fields: {},
            exemptionOptions: [],
            expressionList: [],
            filterMnemonic: [],
            channelActionSelection: ['All Channels', 'Enable Cross Channel'],
            Actions: ["All Actions"],
            disableChannels: false,
            disableActions: false,
            ruleChannelsOptions: [],
            ruleActionsOptions: [],
            storeActualRuleActionsOptions: [],
            channelMapOptions: [],
            channelTxnMap: [],
            storeMnemonic: [],
            selectedChannel: [],
            channelElementOperatorMap: [],
            disableFields: false,
            showNewFragment: false,
            dataLoadingModal: false,
            dynamicRuleScreen: false,
            enableUpdate: false,
            channelIds: [],
            ActionIds: [],
            leftExpressionList: [],
            dateElements: [],
            actualDataElements: [],
            actualUpdatedDataElements: [],
            eleoperatorregionMapData: [],
            operatorsData: [],
            getUpdateInfo: [],
            elementOperatorUpdateScreenMap: [],
            elementOperatorTypeMap: [],
            regionId: '',
            acualRegionId: '',
            regionInnerText: '',
            dataElementsInfo: [],
            ruletypeReGetgionId: {},
            modalGridActivate:true
        }

    }

    componentDidMount = async () => {
        this.setState({ ruleChannelsOptions: this.props.ruleChannelsOptionsModal, ruleActionsOptions: this.props.ruleActionsOptionsModal, storeActualRuleActionsOptions: this.props.ruleActionsOptionsModal });
        const rulesetUrl = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/ruleimbunes`;
        const rulechannelMap = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/channeltxntypemap`;
        const dateElementsMap = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/dataelements`;
        const channeltxntypelemapurl = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/channeltxntypelemap`;
        const eleoperatorregionurl = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/eleoperatorregion`;
        const getExemption = {
            method: 'GET',
            url: `${serverUrl}${rulesetUrl}`
        };
        const getExemptionStatus = await getService(getExemption);
        if (getExemptionStatus.status === 200) {
            this.setState({ exemptionOptions: getExemptionStatus.data.ruleImBuneList });
            const getChannelMap = {
                method: 'GET',
                url: `${serverUrl}${rulechannelMap}`
            };
            const getChannelMapStatus = await getService(getChannelMap);
            if (getChannelMapStatus.status === 200) {
                this.setState({ channelMapOptions: getChannelMapStatus.data.channelTxnTypeList });
                const getDataElements = {
                    method: 'GET',
                    url: `${serverUrl}${dateElementsMap}`
                };
                const getDataElementsStatus = await getService(getDataElements);
                if (getDataElementsStatus.status === 200) {
                    let allowedDataElements = [];
                    allowedDataElements.push({ name: 'ACTION', displayName: 'ACTION' });
                    allowedDataElements.push({ name: 'USERNAME', displayName: 'USERNAME' });
                    getDataElementsStatus.data.systemElems.map((eachSysElem) => {
                        allowedDataElements.push(eachSysElem);
                    })
                    const groupDatatElements = [
                        {
                            title: 'Transaction Elements',
                            value: allowedDataElements
                        },
                        {
                            title: 'Device Elements',
                            value: getDataElementsStatus.data.deviceElems
                        },
                        {
                            title: 'GeoLocation Elements',
                            value: getDataElementsStatus.data.geoLocElems
                        },
                        {
                            title: 'Model Elements',
                            value: getDataElementsStatus.data.modelElems
                        },
                        {
                            title: 'Custom Elements',
                            value: getDataElementsStatus.data.customElems
                        }
                    ]
                    this.setState({ dateElements: groupDatatElements, actualDataElements: JSON.parse(JSON.stringify(groupDatatElements)) }, async () => {
                        const channeltxntypelemap = {
                            method: 'GET',
                            url: `${serverUrl}${channeltxntypelemapurl}`
                        };
                        const channeltxntypelemapStatus = await getService(channeltxntypelemap);
                        if (channeltxntypelemapStatus.status === 200) {
                            this.setState({ channelTxnMap: channeltxntypelemapStatus.data.channelTxnTypeElements }, async () => {
                                const eleoperatorregionMap = {
                                    method: 'GET',
                                    url: `${serverUrl}${eleoperatorregionurl}`
                                };
                                const eleoperatorregionMapStatus = await getService(eleoperatorregionMap);
                                if (eleoperatorregionMapStatus.status === 200) {
                                     this.setState({modalGridActivate:false});
                                    var arr1 = eleoperatorregionMapStatus.data.operatorRegions && eleoperatorregionMapStatus.data.operatorRegions.split("|");
                                    let elementOperatorAssociation = [];
                                    let channelElementOperatorAssociation = [];
                                    let elementOperatorToRegionId = [];
                                    let elementOperatorToRuletype = [];
                                    let ruletypeToRegionId = {};
                                    for (var arr1i = 0; arr1i < arr1.length; ++arr1i) {
                                        if (arr1[arr1i].trim() == "")
                                            continue;
                                        var arr2 = arr1[arr1i].split("=");
                                        var chId = arr2[0].trim();
                                        var info = arr2[1].trim();
                                        if (chId == "" || info == "")
                                            continue;
                                        var arr3 = info.split("#");
                                        for (var arr3j = 0; arr3j < arr3.length; ++arr3j) {
                                            if (arr3[arr3j].trim() == "")
                                                continue;
                                            var arr4 = arr3[arr3j].split(";");
                                            var infoObj = {};
                                            for (var t = 0; t < arr4.length; ++t) {
                                                if (arr4[t].trim() == "")
                                                    continue;
                                                var arr5 = arr4[t].split(":");
                                                infoObj[arr5[0]] = arr5[1];
                                            }
                                            var elemIds = infoObj["names"];
                                            var operId = infoObj["operid"];
                                            var regionId = infoObj["regionid"];
                                            var ruletype = infoObj["ruletype"];
                                            var obj = {};
                                            obj['regionId'] = regionId;
                                            obj['operatorId'] = operId;
                                            obj['ruleType'] = ruletype;
                                            if (null == channelElementOperatorAssociation[chId]) {
                                                channelElementOperatorAssociation[chId] = Array();
                                            }
                                            if (null == (channelElementOperatorAssociation[chId])[elemIds]) {
                                                (channelElementOperatorAssociation[chId])[elemIds] = Array();
                                            }
                                            (channelElementOperatorAssociation[chId])[elemIds].push(obj);

                                            if (elementOperatorAssociation[elemIds] == null)
                                                elementOperatorAssociation[elemIds] = Array();
                                            if (elementOperatorAssociation[elemIds].length === 0) {
                                                elementOperatorAssociation[elemIds].push(obj);
                                            } else {
                                                let checkOperId = elementOperatorAssociation[elemIds] && elementOperatorAssociation[elemIds].findIndex(eachValue => eachValue.operatorId === operId);
                                                if (checkOperId === -1) {
                                                    elementOperatorAssociation[elemIds].push(obj);
                                                }
                                            }

                                            elementOperatorToRegionId[elemIds + ":"
                                                + operId] = regionId;
                                            elementOperatorToRuletype[elemIds + ":"
                                                + operId] = ruletype;
                                            ruletypeToRegionId[ruletype] = regionId;
                                        }
                                    }
                                    this.setState({ ruletypeReGetgionId: ruletypeToRegionId, eleoperatorregionMapData: elementOperatorAssociation, elementOperatorTypeMap: elementOperatorToRuletype, channelElementOperatorMap: channelElementOperatorAssociation, elementOperatorUpdateScreenMap: elementOperatorToRegionId }, () => {
                                        let storeMnemonic = [];
                                        this.props.addRuleData.map((eacAddRuleData) => {
                                            return storeMnemonic.push(eacAddRuleData.mnemonic)
                                        })
                                        this.setState({ storeMnemonic: storeMnemonic });
                                        ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'none';

                                        let filterMnemonic = this.props.selectedMnemonic && this.props.addRuleData && this.props.addRuleData.filter((eachRule) => {
                                            if (eachRule && eachRule.mnemonic) {
                                                return eachRule.mnemonic && eachRule.mnemonic === this.props.selectedMnemonic
                                            }
                                        })
                                        const { expressionList, elementOperatorUpdateScreenMap } = this.state;
                                        if (filterMnemonic) {
                                            let pushChannelActions = [];
                                            if (filterMnemonic[0].channelIdStr === "-1") {
                                                pushChannelActions.push("All Channels");
                                            }
                                            if (filterMnemonic[0].txnIdStr === "-1") {
                                                pushChannelActions.push("All Actions");
                                            }
                                            let nCountOfSelectedActions = this.getSelectedDropdown('ruleActionsListDisplay');
                                            var nCountOfExpectedActions = JSON.parse("[" + filterMnemonic[0].txnIdStr + "]").length;
                                            if (nCountOfSelectedActions !== nCountOfExpectedActions && !(nCountOfExpectedActions === 1 && nCountOfExpectedActions[0] === -1)) {
                                                pushChannelActions.push("Enable Cross Channel");
                                            }
                                            this.setState({ filterMnemonic: filterMnemonic[0], disableFields: this.props.selectedMnemonic ? true : false, expressionList: expressionList, selectedChannel: pushChannelActions });

                                            this.setState({ channelIds: JSON.parse("[" + filterMnemonic[0].channelIdStr + "]") }, () => {
                                                let getGroupSelected = '';
                                                if (filterMnemonic[0].channelIdStr === "-1") {
                                                    getGroupSelected = this.selectedChannelsIds();
                                                }
                                                this.AllChannelSelection(filterMnemonic[0].channelIdStr === "-1" ? getGroupSelected : JSON.parse("[" + filterMnemonic[0].channelIdStr + "]"), '', JSON.parse("[" + filterMnemonic[0].txnIdStr + "]"));
                                                let node = ReactDOM.findDOMNode(this.refs['selectedChannels']);
                                                var options = node && node.querySelectorAll('option');
                                                for (var i = 0; i < options.length; i++) {
                                                    if (JSON.parse("[" + filterMnemonic[0].channelIdStr + "]").includes(parseInt(options[i].value))) {
                                                        options[i].selected = true;
                                                    }
                                                }
                                            })

                                            /*To load rule screens for update functionality */
                                            if (filterMnemonic[0]['factoryRule'] === true) {
                                                let getParams = this.extractParams(filterMnemonic[0].paramValueStr, filterMnemonic[0].mnemonic, filterMnemonic[0].ruleType);
                                                var deviceMFPRules = Array(
                                                    "USER_DEVICE_ASSOCIATED_AND_DEVICE_MFP_MATCHED",
                                                    "USER_DEVICE_NOT_ASSOCIATED_AND_DEVICE_MFP_MATCHED",
                                                    "USER_DEVICE_ASSOCIATED_AND_DEVICE_MFP_NOT_MATCHED",
                                                    "USER_DEVICE_NOT_ASSOCIATED_AND_DEVICE_MFP_NOT_MATCHED");
                                                let isDeviceMFPRule = deviceMFPRules.includes(filterMnemonic[0].mnemonic);
                                                let dataElements = isDeviceMFPRule ? Array()
                                                    : filterMnemonic[0].expression.split(" ")[0].split("+");
                                                let operator = isDeviceMFPRule ? "" : filterMnemonic[0].expression.split(" ")[1];
                                                var key1 = dataElements + ":" + operator;
                                                var obj = {};
                                                obj['ruleType'] = getParams.RULETYPE;
                                                obj['tnxElement'] = dataElements.join(',');
                                                obj['operator'] = elementOperatorUpdateScreenMap[key1];
                                                obj['actualOperator'] = `${elementOperatorUpdateScreenMap[key1]}@${operator}`;
                                                obj['expressionItem'] = filterMnemonic[0].expression;
                                                obj['color'] = true;
                                                obj['name'] = "expr";
                                                obj['deleteType'] = "delete";
                                                obj['fields'] = getParams;
                                                expressionList.push(obj);
                                                this.setState({ dataElementsInfo: dataElements, regionInnerText: operator, acualRegionId: `${elementOperatorUpdateScreenMap[key1]}@${operator}`, regionId: elementOperatorUpdateScreenMap[key1], getUpdateInfo: getParams, dynamicRuleScreen: true }, () => {
                                                    let node = ReactDOM.findDOMNode(this.refs['dataElementsDropdown']);
                                                    var options = node && node.querySelectorAll('option');
                                                    for (var i = 0; i < options.length; i++) {
                                                        options[i].selected = false;
                                                        if (options[i].value === dataElements.join(',')) {
                                                            options[i].selected = true;
                                                        }
                                                    }
                                                    this.handleDataElements();
                                                })
                                            } else {
                                                this.getFragmentObjects(filterMnemonic[0]['expression']);
                                            }
                                           
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        }

    }
    getCombinationRuleExpression=(target)=>{
        const {expressionList}=this.state;
        var finalArray = [];
        var expression="";
        if(expressionList.length > 1){
            for(var i = 0; i < expressionList.length; ++i) {
                var curObj = expressionList[i];
                if(curObj["name"] === "oper"){
                    if(finalArray.length > 0){
                        var prevObj = expressionList[i-1];
                        if(prevObj["name"] === "obr" && curObj["name"] === "oper"){
                        }else{
                            finalArray.push(curObj);
                        }
                    }
                }else if(curObj["name"] === "not"){
                    finalArray.push(curObj);
                }else if(curObj["name"] === "obr"){
                    finalArray.push(curObj);
                }else if(curObj["name"] === "cbr"){
                    if(finalArray.length > 0){
                        var prevObj = finalArray[finalArray.length-1];
                        if(prevObj["name"] === "obr" || prevObj["name"] === "oper" || prevObj["name"] === "not"){
                            finalArray.pop();
                        } else {
                            finalArray.push(curObj);
                        }
                    }
                }else if(curObj["ruletype"] === target){
                    if(finalArray.length > 0){
                        var prevObj = expressionList[i-1];
                        if(prevObj["name"] === "oper" || prevObj["name"] === "not"){
                            finalArray.pop();
                        }else if(prevObj["name"] === "obr"){
                            var nextObj = expressionList[i+1];
                            if(nextObj["name"] === "cbr"){
                                finalArray.pop();
                            }
                        }
                    }
                }else{
                    finalArray.push(curObj);
                }
            }
            
            //remove any lingering operators
            if(finalArray.length > 0){
                var index = finalArray.length-1;
                if(finalArray[index]["name"] === "oper" || finalArray[index]["name"] === "not"){
                    finalArray.pop();
                }
            }
            
            for(var i = 0; i < finalArray.length; ++i) {
                var tempObj = finalArray[i]["name"];
                if(tempObj === "oper"){
                    expression += finalArray[i]["operator"]+" ";
                }else if(tempObj === "not"){
                    expression += "NOT ";
                }else if(tempObj === "obr"){
                    expression += "( ";
                }else if(tempObj === "cbr"){
                    expression += ") ";
                }else{
                    if(finalArray[i]["operator"] === "IN_LIST" || finalArray[i]["operator"] === "IN_CATEGORY") {
                        var tempExp = finalArray[i]["statement"];
                        var indexName = (finalArray[i]["operator"] === "IN_LIST")?"WITH":"MATCHTYPE";
                        var index = tempExp.indexOf(indexName);
                        if(index > 0){
                            
                            //to check the matching type
                            var matchingType = tempExp.substring(index, tempExp.length).trim();
                            
                            //get the matching type array
                            var matchArray = [];
                            matchArray = matchingType.split("=");
                            
                            //add the expression which is required
                            var expToAdd = tempExp.substring(0, index).trim();
                            var expLength = expToAdd.length;
                            if(expToAdd.charAt(expLength-1) == '&'){
                                expToAdd = expToAdd.substring(0,expLength-1).trim();
                            }
                            
                            if(finalArray[i]["operator"] === "IN_LIST"){
                                var listName = finalArray[i]["params"]["LISTDATASET"];
                                if(listName != 'undefined'){
                                       expToAdd = expToAdd.replace(listName,"!L!"+listName);
                                }   
                            }
                            
                            expression += expToAdd + " ";
                        }else{
                            expression += tempExp + " ";
                        }
                    }else{
                        expression += finalArray[i]["statement"]+" ";
                    }
                }
            }
        }
        return (expression.length>0)?expression.split("&").join("__AMP__"):'NULL';
    }
    handleFinalRuleScreen = (getScreenType, params, getType = '') => {
        switch (getScreenType) {
            case `STRING_COMPARE_SCREEN`: {
                if (getType === '') {
                    var operMap = { "MATCHES": "CONTAINS" };
                    params["OPERATOR"] = operMap[params["OPERATOR"]];
                    params["THRESHOLD"] = encodeURIComponent(params["THRESHOLD"]);
                }
                return params;
            }
            case `IN_LIST_SCREEN`: {
                if (getType === '') {
                    var tempValue = params["SIMPLEINLISTVALUE"];
                    if (undefined === tempValue || 'undefined' === tempValue) {
                        tempValue = "";
                    }
                    params["SIMPLEINLISTVALUE"] = encodeURIComponent(tempValue);
                    return params;
                }
            }
            case `AMOUNT_COMPARE_SCREEN`: {
                if (getType === 'update') {
                    // var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                    // params["OPERATOR"] = operMap[params["OPERATOR"]];

                    // var threshold = params["THRESHOLD"];
                    // var data = threshold.split("|");
                    // threshold = "";
                    // for (var t = 0; t < data.length; ++t) {
                    //     var d = data[t];
                    //     if (d.trim() == "")
                    //         continue;
                    //     var kp = d.split(":");
                    //     if (threshold == "")
                    //         threshold = kp[0] + "_" + allCurrencies[kp[1]];
                    //     else
                    //         threshold += "," + kp[0] + "_" + allCurrencies[kp[1]];
                    // }
                    // params["THRESHOLD"] = "[" + threshold + "]";
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                    params["OPERATOR"] = operMap[params["OPERATOR"]];
                    var threshold = params["THRESHOLD"];
                    var currData = threshold.split("[")[1].split("]")[0].split(",");
                    threshold = "";
                    for (var t = 0; t < currData.length; ++t) {
                        if (currData[t].trim() == "")
                            continue;
                        var d = currData[t].split("_");
                        var s = d[0] + ":" + params['allCurrenciesReverse'][d[1]];
                        threshold += s + "|";
                    }
                    params["THRESHOLD"] = threshold;
                }
                return params;
            }
            case `AMOUNT_CB_COMPARE_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ_CB": "EQUAL_TO_CB", "NEQ_CB": "NOT_EQUAL_TO_CB", "GTE_CB": "GREATER_OR_EQUAL_CB", "GT_CB": "GREATER_THAN_CB", "LTE_CB": "LESS_OR_EQUAL_CB", "LT_CB": "LESS_THAN_CB" };
                    params["OPERATOR"] = operMap[params["OPERATOR"]];
                    var threshold = params["THRESHOLD"];
                    params["THRESHOLD"] = "[" + threshold.trim() + "]";
                } else {
                    var operMap = { "EQUAL_TO_CB": "EQ_CB", "NOT_EQUAL_TO_CB": "NEQ_CB", "GREATER_OR_EQUAL_CB": "GTE_CB", "GREATER_THAN_CB": "GT_CB", "LESS_OR_EQUAL_CB": "LTE_CB", "LESS_THAN_CB": "LT_CB" };
                    params["OPERATOR"] = operMap[params["OPERATOR"]];
                    var threshold = params["THRESHOLD"];
                    var currData = threshold.split("[")[1].split("]")[0].split(",");
                    threshold = "";
                    for (var t = 0; t < currData.length; ++t) {
                        if (currData[t].trim() == "")
                            continue;
                        var d = currData[t];
                        threshold += d;
                    }
                    params["THRESHOLD"] = threshold;
                    return params;
                }
            }
            case `CUMULATIVE_AMOUNT_SCREEN`: {
                if (getType === '') {
                    params["COMBINATIONSTRING"] = this.getCombinationRuleExpression("CUMULAMOUNTCHECK");
                    var expression = this.state.channelIds;
                    params["SELECTEDCHANNELS"] = (expression.length > 0) ? expression.join(",") : "NULL";
                    params["SELECTEDTXNTYPES"] = this.state.ActionIds;
                    return params;
                }
            }
            case `ACTION_CUMULATIVE_AMOUNT_SCREEN`: {
                if (getType === '') {
                    params["COMBINATIONSTRING"] = this.getCombinationRuleExpression("ACTIONCUMULAMOUNTCHECK");
                    var expression = this.state.channelIds;
                    params["SELECTEDCHANNELS"] = (expression.length > 0) ? expression.join(",") : "NULL";
                    params["SELECTEDTXNTYPES"] = this.state.ActionIds;
                    return params;
                }
            }
            case `GROUPED_CUMULATIVE_AMOUNT_CHECK_SCREEN`: {
                if (getType === '') {
                params["COMBINATIONSTRING"] = this.getCombinationRuleExpression("GROUPEDCUMULAMOUNTCHECK");
                var expression = this.state.channelIds;
                params["SELECTEDCHANNELS"] = (expression.length > 0) ? expression.join(",") : "NULL";
                params["SELECTEDTXNTYPES"] = this.state.ActionIds;
                return params;
                }
            }
            case `HISTORY_USER_VELOCITY_SCREEN`: {
                if (getType === '') {
                params["COMBINATIONSTRING"] = this.getCombinationRuleExpression("HISTORYUSERVELOCITY");
                var expression = this.state.channelIds;
                params["SELECTEDCHANNELS"] = (expression.length > 0) ? expression.join(",") : "NULL";
                params["SELECTEDTXNTYPES"] = this.state.ActionIds;
                return params;
                }
            }
            case `HistoryZoneHoppingScreen`: {
                if (getType === '') {
                params["COMBINATIONSTRING"] = this.getCombinationRuleExpression("HISTORYZONEHOPPING");
                var expression = this.state.channelIds;
                params["SELECTEDCHANNELS"] = (expression.length > 0) ? expression.join(",") : "NULL";
                params["SELECTEDTXNTYPES"] = this.state.ActionIds;
                return params;
                }
            }
            case `CUSTOM_SCREEN_1`: {
                if(getType === 'update'){
                    var operMap = { "EQ" : "EQUAL_TO" , "NEQ" : "NOT_EQUAL_TO", "GTE" : "GREATER_OR_EQUAL" , "GT" : "GREATER_THAN", "LTE" : "LESS_OR_EQUAL" , "LT" : "LESS_THAN", "NEQ" : "NOT_EQUAL_TO"};
                    params["OPERATOR"] = operMap[params["OPERATOR"]];	
                }else{
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                    params["OPERATOR"] = operMap[params["OPERATOR"]];
                }
                return params;
            }
            case `ACTIONVELOCITY_SCREEN`: {
                // var actions = params['ACTION'];
                // params['ERROR'] = '';
                // if(undefined == actions || 'undefined' == actions){
                //     params['ERROR']="RF.RB.error.actionChannelMismatch";
                // } else {
                //     var actionsArray = actions.split("|");
                //     var txnTypes = $("txTypesBuilder").options;
                //     var isActionsValid = false;
                //     for(var count=0; count<actionsArray.length; count++){
                //         isActionsValid = false;
                //         for(var i = 0; i < txnTypes.length; ++i) {
                //             if(txTypeNames[txnTypes[i].value] == actionsArray[count]){
                //                 isActionsValid = true;
                //                 break;
                //             }
                //         }
                //         if(false == isActionsValid){
                //             params['ERROR']="RF.RB.error.actionChannelMismatch";
                //             break;
                //         }
                //     }
                // }
                // return params;
            }
            case `TXTYPECHECK_SCREEN`: {
                if(getType === 'update'){
                    var operMap = { "1" : "REGULAR", "2" : "ATTEMPTS", "3":"AE_WITH_PWD","4":"AE_WITHOUT_PWD","5":"FORGOT_PWD", "6":"SEC_CH", "7":"FORGOT_PWD_MULTI_CH", "8":"FORGOT_PWD_SINGLE_CH","9":"ABRIDGED_ADS","10":"SEC_CH_ABRIDGED","11":"UNKNOWN"};
                    var strArr  = params["PREVTXNTYPES"].split("|");
                    var finalString = "";
                    for(var i = 0; i < strArr.length; ++i) {
                        finalString += operMap[strArr[i]];
                        if(strArr.length != i+1){
                            finalString += "|";
                        }
                    }
                    params["PREVTXNTYPES"] = finalString;
                }else{
                    if (params["PREVTXNTYPES"] !== null && params["PREVTXNTYPES"] !== undefined && params["PREVTXNTYPES"] !== 'undefined') {
                        var strArr = params["PREVTXNTYPES"].split("|");
                        var operMap = { "REGULAR": "1", "ATTEMPTS": "2", "AE_WITH_PWD": "3", "AE_WITHOUT_PWD": "4", "FORGOT_PWD": "5", "SEC_CH": "6", "FORGOT_PWD_MULTI_CH": "7", "FORGOT_PWD_SINGLE_CH": "8", "ABRIDGED_ADS": "9", "SEC_CH_ABRIDGED": "10", "UNKNOWN": "11" };
                        var finalString = "";
                        for (var i = 0; i < strArr.length; ++i) {
                            finalString += operMap[strArr[i]];
                            if (strArr.length != i + 1) {
                                finalString += "|";
                            }
    
                        }
                        params["PREVTXNTYPES"] = finalString;
                    }
                }
        
                return params;
            }
            case `MonthCheckScreen`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `DATE_TIME_CHECK_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `DATE_CHECK_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `DAYOFMONTH_CHECK_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `HOUROFDAY_CHECK_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `CURRENTTIME_CHECK_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `YEAR_CHECK_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
            case `NUMERIC_COMPARISON_SCREEN`: {
                if (getType === 'update') {
                    var operMap = { "EQ": "EQUAL_TO", "NEQ": "NOT_EQUAL_TO", "GTE": "GREATER_OR_EQUAL", "GT": "GREATER_THAN", "LTE": "LESS_OR_EQUAL", "LT": "LESS_THAN", "NEQ": "NOT_EQUAL_TO" };
                } else {
                    var operMap = { "EQUAL_TO": "EQ", "NOT_EQUAL_TO": "NEQ", "GREATER_OR_EQUAL": "GTE", "GREATER_THAN": "GT", "LESS_OR_EQUAL": "LTE", "LESS_THAN": "LT", "NOT_EQUAL_TO": "NEQ" };
                }
                params["OPERATOR"] = operMap[params["OPERATOR"]];
                return params;
            }
        }
    }
    getFragmentObjects = (stmt) => {
        const { elementOperatorUpdateScreenMap, ruletypeReGetgionId } = this.state;
        var fragmentedObjects = [];
        stmt = stmt.trim();
        if (stmt === "")
            return fragmentedObjects;
        if (stmt.indexOf(" AND ") >= 0) {
            var arr = stmt.split(" AND ");
            for (var i = 0; i < arr.length; ++i) {
                fragmentedObjects = fragmentedObjects
                    .concat(this.getFragmentObjects(arr[i]));
                if (i < arr.length - 1) {
                    var obj = new Operator();
                    obj.operator = "AND";
                    fragmentedObjects.push(obj);
                }
            }
            return fragmentedObjects;
        } else if (stmt.indexOf(" OR ") >= 0) {
            var arr = stmt.split(" OR ");
            for (var i = 0; i < arr.length; ++i) {
                fragmentedObjects = fragmentedObjects
                    .concat(this.getFragmentObjects(arr[i]));
                if (i < arr.length - 1) {
                    var obj = new Operator();
                    obj.operator = "OR";
                    fragmentedObjects.push(obj);
                }
            }
            return fragmentedObjects;
        } else if (stmt.startsWith("NOT ")) {
            stmt = stmt.substring(4);
            var obj = new Not();
            fragmentedObjects.push(obj);
            fragmentedObjects = fragmentedObjects
                .concat(this.getFragmentObjects(stmt.trim()));
            return fragmentedObjects;
        } else if (stmt.indexOf(" NOT ") >= 0) {
            var arr = stmt.split(" NOT ");
            for (var i = 0; i < arr.length; ++i) {
                fragmentedObjects = fragmentedObjects
                    .concat(this.getFragmentObjects(arr[i]));
                if (i < arr.length - 1) {
                    var obj = new Not();
                    fragmentedObjects.push(obj);
                }
            }
            return fragmentedObjects;
        } else if (stmt.indexOf("(") >= 0) {
            var arr = stmt.split("(");
            for (var i = 0; i < arr.length; ++i) {
                fragmentedObjects = fragmentedObjects
                    .concat(this.getFragmentObjects(arr[i]));
                if (i < arr.length - 1) {
                    var obj = new Obr();
                    fragmentedObjects.push(obj);
                }
            }
            return fragmentedObjects;
        } else if (stmt.indexOf(")") >= 0) {
            var arr = stmt.split(")");
            for (var i = 0; i < arr.length; ++i) {
                fragmentedObjects = fragmentedObjects
                    .concat(this.getFragmentObjects(arr[i]));
                if (i < arr.length - 1) {
                    var obj = new Cbr();
                    fragmentedObjects.push(obj);
                }
            }
            return fragmentedObjects;
        }
        var ruleData = this.getRuleDataFromMnemonic(stmt);
        if (!ruleData)
            return fragmentedObjects;
        if (ruleData["isSystemGeneratedExpression"] === false) {
            var obj = new Saved();
            obj.mnemonic = stmt;
            fragmentedObjects.push(obj);
            return fragmentedObjects;
        } else {
            var obj = {};
            let dataElements = ruleData.expression.split(" ")[0].split("+");
            let operator = ruleData.expression.split(" ")[1];
            let getRuleTypeFromList = ruletypeReGetgionId[ruleData['ruleType']];
            let params = {};
            params['OPERATOR'] = operator;
            let getFinalData = this.handleFinalRuleScreen(getRuleTypeFromList, params, 'update');
            var key1 = dataElements + ":" + getFinalData['OPERATOR'];
            let getParams = this.extractParams(ruleData.paramValueStr, ruleData.mnemonic, ruleData.ruleType);
            getParams['OPERATOR'] = getFinalData['OPERATOR'];
            obj['ruleType'] = ruleData.ruleType;
            obj['tnxElement'] = ruleData.expression.split(" ")[0].split("+");
            obj['operator'] = elementOperatorUpdateScreenMap[key1];
            obj['actualOperator'] = `${elementOperatorUpdateScreenMap[key1]}@${getFinalData['OPERATOR']}`;
            obj['expressionItem'] = ruleData.expression;
            obj['color'] = true;
            obj['name'] = "expr";
            obj['deleteType'] = "delete";
            obj['fields'] = getParams;
            fragmentedObjects.push(obj);
            this.setState({ expressionList: fragmentedObjects, regionInnerText: getFinalData['OPERATOR'], dataElementsInfo: dataElements, acualRegionId: `${elementOperatorUpdateScreenMap[key1]}@${getFinalData['OPERATOR']}`, regionId: elementOperatorUpdateScreenMap[key1], getUpdateInfo: getParams, dynamicRuleScreen: true }, () => {
                let node = ReactDOM.findDOMNode(this.refs['dataElementsDropdown']);
                var options = node && node.querySelectorAll('option');
                for (var i = 0; i < options.length; i++) {
                    options[i].selected = false;
                    if (options[i].value === dataElements.join(',')) {
                        options[i].selected = true;
                    }
                }
                this.handleDataElements();
            })
        }
        function Saved() { this.name = "saved"; this.mnemonic = ""; }
        function Operator() { this.name = "oper"; this.operator = ""; }
        function Not() { this.name = "not"; this.value = "NOT"; }
        function Obr() { this.name = "obr"; this.value = "("; }
        function Cbr() { this.name = "cbr"; this.value = ")"; }
    }
    getRuleDataFromMnemonic = (getStmt) => {
        let noSoretedData = this.props.noSortAddRuleData;
        for (var i = 0; i < noSoretedData.length; ++i)
            if (noSoretedData[i]["mnemonic"] === getStmt)
                return noSoretedData[i];
        return {};
    }
    extractParams = (paramValueStr, mnemonic, ruletype) => {
        var paramValues = {};
        if (paramValueStr != "") {
            var arrp = paramValueStr.split("|!|");
            for (var i = 0; i < arrp.length; ++i) {
                if (arrp[i].trim() == "")
                    continue;
                var arrp2 = arrp[i].split("@");
                var k = arrp2[0].trim();
                var v = arrp2[1].trim();
                paramValues[k] = v;
            }
        }
        paramValues["RULEMNEMONIC"] = mnemonic;
        paramValues["RULETYPE"] = ruletype;
        return paramValues;
    }
    handleDataElements = () => {
        const { eleoperatorregionMapData, channelElementOperatorMap, channelIds, regionId, disableFields } = this.state;
        this.resetOperatorSelection();
        this.setState({ operatorsData: [], regionId: disableFields ? regionId : '' }, () => {
            let getDataElements = this.getSelectedDropdown('dataElementsDropdown');
            let key = getDataElements.join(',');
            var tempOperators = null;
            if (channelIds.length > 0) {
                for (var channel = 0; channel < channelIds.length; channel++) {
                    if (null == channelElementOperatorMap[channelIds[channel]]) {
                        tempOperators = channelElementOperatorMap["_"][key];
                    } else if (null == channelElementOperatorMap[channelIds[channel]][key]) {
                        tempOperators = channelElementOperatorMap["_"][key];
                    } else {
                        tempOperators = channelElementOperatorMap[channelIds[channel]][key];
                    }
                    if (typeof (tempOperators) != "undefined")
                        break;
                }
            }
            var operators = eleoperatorregionMapData[getDataElements.join(',')];
            if (null != tempOperators && typeof (tempOperators) != "undefined") {
                operators = tempOperators;
            }
            this.setState({ operatorsData: operators, dataElementsInfo: getDataElements });
        });
    }
    resetOperatorSelection = () => {
        let node = ReactDOM.findDOMNode(this.refs['operatorData']);
        var options = node && node.querySelectorAll('option');
        for (var i = 0; i < options.length; i++) {
            options[i].selected = false;
        }
    }
    getSelectedDropdown = (getSelection) => {
        let node = ReactDOM.findDOMNode(this.refs[getSelection]);
        var options = node && node.querySelectorAll('option');
        let groupSelected = [];
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                groupSelected.push(options[i].value);
            }
        }
        return groupSelected
    }
    AllChannelSelection = (groupSelected, getCheckBoxType = '', getTnxStr = '') => {
        const { channelMapOptions, storeActualRuleActionsOptions, selectedChannel } = this.state;
        let MatchedChannel = channelMapOptions && channelMapOptions.filter((eachChannel) => {
            if (eachChannel && eachChannel.channelId) {
                return groupSelected.includes(eachChannel.channelId)
            }
        })
        let result = MatchedChannel.length ? MatchedChannel[0].txypeids : [];
        for (let i = 1; i < MatchedChannel.length; i++) {
            let inter = '';
            if (selectedChannel.includes('Enable Cross Channel') && selectedChannel.includes('All Channels')) {
                inter = _.union(result, MatchedChannel[i].txypeids);
            } else {
                inter = _.intersection(result, MatchedChannel[i].txypeids);
            }
            if (inter.length) {
                result = inter;
            } else {
                result = [];
                break;
            }
        }
        var filterChannels = storeActualRuleActionsOptions && storeActualRuleActionsOptions.filter((eachRule) => {
            if (eachRule && eachRule.txTypeId) {
                return result && result.includes(eachRule.txTypeId)
            }
        })
        if (selectedChannel.includes('Enable Cross Channel') && selectedChannel.includes('All Channels')) {
            this.setState({ channelIds: groupSelected });
        }
        const { disableFields } = this.state;
        this.setState({ ruleActionsOptions: JSON.parse(JSON.stringify(filterChannels)) }, async () => {
            if (getCheckBoxType === "" || getCheckBoxType === 'selectedChannels') {
                let actionNode = ReactDOM.findDOMNode(this.refs['ruleActionsListDisplay']);
                var actionOptions = actionNode && actionNode.querySelectorAll('option');
                for (var i = 0; i < actionOptions.length; i++) {
                    if (disableFields && getTnxStr && getTnxStr.includes(parseInt(actionOptions[i].value)) && selectedChannel.includes('Enable Cross Channel') && selectedChannel.includes('All Channels')) {
                        actionOptions[i].selected = true;
                    } else if (disableFields && !getTnxStr.includes(parseInt(actionOptions[i].value)) && selectedChannel.includes('Enable Cross Channel') && selectedChannel.includes('All Channels')) {
                        actionOptions[i].selected = false;
                    } else {
                        actionOptions[i].selected = true;
                    }

                }
            }
            await this.handleActionIds();
            this.dyanmicDataElementsChanges();
        });
    }
    handleChannels = async (e) => {
        const { fields } = this.state;
        if (e.target.name === "selectedChannels") {
            let node = ReactDOM.findDOMNode(this.refs['selectedChannels']);
            var options = node && node.querySelectorAll('option');
            let groupSelected = [];
            for (var i = 0; i < options.length; i++) {
                if (options[i].selected) {
                    groupSelected.push(parseInt(options[i].value));
                }
            }
            this.setState({ channelIds: groupSelected });
            this.AllChannelSelection(groupSelected, e.target.name);
        } else {
            this.AllChannelSelection(this.state.channelIds, e.target.name);
        }
        this.setState({ fields: fields });
    }
    dyanmicDataElementsChanges = () => {
        var allowedDataElements = [];
        const { ActionIds, channelIds, selectedChannel, actualDataElements } = this.state;
        for (var i = 0; i < channelIds.length; i++) {
            for (var j = 0; j < ActionIds.length; j++) {
                if (i === 0 && j === 0) {
                    allowedDataElements = this.filterChannelsData(channelIds[i], ActionIds[j]);
                }
                else {
                    if (selectedChannel.includes('Enable Cross Channel')) {
                        allowedDataElements = _.union(
                            allowedDataElements, this.filterChannelsData(channelIds[i], ActionIds[j]));
                    }
                    else {
                        allowedDataElements = _.intersection(
                            allowedDataElements, this.filterChannelsData(channelIds[i], ActionIds[j]));
                    }
                }
            }
        }
        let copyActualDataElements = JSON.parse(JSON.stringify(actualDataElements));
        allowedDataElements && allowedDataElements.map((eachElement) => {
            const foundObjTxn = _.filter(copyActualDataElements[0].value, function (val) {
                return val.displayName && val.displayName.indexOf(eachElement) !== -1;
            });
            if (foundObjTxn.length === 0) {
                copyActualDataElements[0].value.push({
                    name: eachElement,
                    displayName: eachElement
                })
            }
        })
        let sortedElements = [];
        copyActualDataElements.map((eachDataElement) => {
            sortedElements.push({
                title: eachDataElement.title,
                value: _.sortBy(eachDataElement.value, 'displayName')
            })
        })
        this.setState({ dateElements: sortedElements, actualUpdatedDataElements: JSON.parse(JSON.stringify(sortedElements)) });
    }
    filterChannelsData = (getChannelId, getActionId) => {
        const { channelTxnMap } = this.state;
        let filteredData = [];
        channelTxnMap.forEach(obj => {
            if (obj.channelId === getChannelId) {
                _.filter(obj.data, function (val) {
                    if (val.txnTypeId && val.txnTypeId === getActionId) {
                        filteredData.push(val.elementName)
                    }
                });
            }
        })
        return filteredData
    }
    handleActionIds = () => {
        let groupSelected = [];
        let actionNode = ReactDOM.findDOMNode(this.refs['ruleActionsListDisplay']);
        var actionOptions = actionNode && actionNode.querySelectorAll('option');
        for (var i = 0; i < actionOptions.length; i++) {
            if (actionOptions[i].selected) {
                groupSelected.push(parseInt(actionOptions[i].value));
            }
        }
        this.setState({ ActionIds: groupSelected });

    }
    handleSelectedRules = () => {
        let node = ReactDOM.findDOMNode(this.refs['mnemonicDropdown']);
        if (node !== null) {
            var options = node.querySelectorAll('option');
            const { expressionList, leftExpressionList } = this.state;
            for (var i = 0; i < options.length; i++) {
                if (options[i].selected) {
                    var obj = {};
                    obj['expressionItem'] = options[i].innerText;
                    obj['color'] = false;
                    obj['deleteType'] = 'delete';
                    obj['name'] = 'saved';
                    obj['fields'] = [];
                    let getIndex = this.getExpressionIndex();
                    if (getIndex !== -1) {
                        expressionList[getIndex] = obj;
                    } else {
                        expressionList.push(obj);
                    }
                    leftExpressionList.push(options[i].innerText);
                }
            }
            this.setState({ expressionList: expressionList, operatorsData: [], dynamicRuleScreen: false, regionId: '' }, () => {
                this.DynamicRuleScreen();
                this.resetOperatorSelection();
            });
        }
    }
    handleSavedRules = (e) => {
        const { expressionList, disableFields } = this.state;
        const selectedArray = [];
        var obj = {};
        obj['expressionItem'] = e.target.innerText;
        obj['color'] = false;
        obj['fields'] = [];
        if (e.target.innerText === 'AND' || e.target.innerText === 'OR') {
            obj['name'] = 'oper';
        } else if (e.target.innerText === 'NOT') {
            obj['name'] = 'not';
        } else if (e.target.innerText === 'SKIPPED') {
            obj['name'] = 'skipped';
        } else if (e.target.innerText === '(') {
            obj['name'] = 'obr';
        } else if (e.target.innerText === ')') {
            obj['name'] = 'cbr';
        }
        if (disableFields && e.target.innerText === 'SKIPPED') {
            if (expressionList.length && expressionList[0].expressionItem === 'SKIPPED') {
                expressionList.push(obj);
                this.setState({ expressionList: expressionList });
            } else {
                selectedArray.push(obj);
                this.setState({ expressionList: selectedArray });
            }

        } else {
            let getIndex = this.getExpressionIndex();
            if (getIndex !== -1) {
                expressionList[getIndex] = obj;
            } else {
                expressionList.push(obj);
            }
            this.setState({ expressionList: expressionList, operatorsData: [], dynamicRuleScreen: false, regionId: '' }, () => {
                ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'none';
                ReactDOM.findDOMNode(this.refs["enableAdd"]).style.display = 'block';
                this.DynamicRuleScreen();
            });
        }
    }
    handleExpressionPosition = (getType, e) => {
        const { expressionList } = this.state;
        let getIndex = this.getExpressionIndex();
        if (getType === 'left' && getIndex !== -1) {
            if (getIndex !== 0) {
                let getSibilingExpression = expressionList[getIndex - 1];
                expressionList[getIndex - 1] = expressionList[getIndex];
                expressionList[getIndex] = getSibilingExpression;
            }
        } else if (getType === 'right' && getIndex !== -1) {
            if (getIndex !== (expressionList.length - 1)) {
                let getSibilingExpression = expressionList[getIndex + 1];
                expressionList[getIndex + 1] = expressionList[getIndex];
                expressionList[getIndex] = getSibilingExpression;
            }
        }
        this.setState({ expressionList: expressionList });
    }

    getExpressionIndex = () => {
        const { expressionList } = this.state;
        var activatedExpression = expressionList.findIndex(function (v) {
            return v.color;
        })
        return activatedExpression
    }
    clearSavedRules = () => {
        let dialogResult = window.confirm(RA_STR.clearFragments);
        if (dialogResult) {
            this.setState({ expressionList: [], leftExpressionList: [] });
        }
    }
    handleChange = (fieldId, e) => {
        const { fields } = this.state;
        fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
        this.setState({ fields: fields });
    }
    selectedChannelsIds = () => {
        let node = ReactDOM.findDOMNode(this.refs['selectedChannels']);
        var options = node && node.querySelectorAll('option');
        let groupSelected = [];
        for (var i = 0; i < options.length; i++) {
            groupSelected.push(parseInt(options[i].value));
        }
        return groupSelected
    }
    onchange = (e) => {
        const { selectedChannel, storeActualRuleActionsOptions } = this.state;
        if (e.target.checked) {
            selectedChannel.push(e.target.value);
        } else {
            let filtered = selectedChannel.indexOf(e.target.value);
            selectedChannel.splice(filtered, 1);
        }
        if ((selectedChannel.includes('All Channels')) || (selectedChannel.includes('Enable Cross Channel') && selectedChannel.includes('All Channels'))) {
            this.setState({ disableChannels: true });
            let getGroupSelected = this.selectedChannelsIds();
            this.AllChannelSelection(getGroupSelected);
        } else {
            this.setState({ disableChannels: false });
        }
        this.setState({ selectedChannel: selectedChannel }, () => {
            if (selectedChannel.includes('All Actions') && !selectedChannel.includes('All Channels') || (selectedChannel.includes('All Actions') && selectedChannel.includes('All Channels'))) {
                document.querySelector('#Enable_Cross_Channel').disabled = 'disabled';
            } else {
                document.querySelector('#Enable_Cross_Channel').disabled = '';
            }
            if (selectedChannel.includes('All Actions')) {
                let actionNode = ReactDOM.findDOMNode(this.refs['ruleActionsListDisplay']);
                var actionOptions = actionNode && actionNode.querySelectorAll('option');
                for (var i = 0; i < actionOptions.length; i++) {
                    actionOptions[i].selected = false;
                }
                this.setState({ disableActions: true });
            } else {
                this.setState({ disableActions: false });
            }
        });
    }
    getRuletype = (str, oper) => {
        const { elementOperatorTypeMap } = this.state;
        var ruletype = elementOperatorTypeMap[":" + oper];
        var ruletype2 = elementOperatorTypeMap[str + ":" + oper];
        if (ruletype2 !== null && ruletype2 !== "" && oper !== "IN_LIST") ruletype = ruletype2;
        return ruletype;
    }
    getUniqueSequenceId = async () => {
        const sequenceUrl = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/getuniquesequenceid`;
        const getSequenceId = {
            method: 'GET',
            url: `${serverUrl}${sequenceUrl}`
        };
        const getSequenceIdStatus = await getService(getSequenceId);
        let sequenceGenerated = '';
        if (getSequenceIdStatus.status === 200) {
            sequenceGenerated = getSequenceIdStatus.data;
        }
        return sequenceGenerated
    }
    createRuleScoring = async (getType) => {
        let formIsValid = true;
        const { fields, regionInnerText, dataElementsInfo, selectedChannel, leftExpressionList, ActionIds, channelIds, disableFields, filterMnemonic, dataLoadingModal, expressionList } = this.state;
        if ((getType === 'create' && !fields.ruleDisplayName) || ((getType === 'update' && fields.hasOwnProperty('ruleDisplayName') && fields.ruleDisplayName === ''))) {
            formIsValid = false;
            alert(RA_STR.ruleNameValidate);
        } else if (fields.ruleMnemonic && fields.ruleMnemonic.indexOf(' ') !== -1) {
            formIsValid = false;
            alert(RA_STR.checkRuleWhiteSpace);
        } else if ((getType === 'create' && !fields.ruleMnemonic) || ((getType === 'update' && fields.hasOwnProperty('ruleMnemonic') && fields.ruleMnemonic === ''))) {
            formIsValid = false;
            alert(RA_STR.ruleMnemonicValidate);
        } else if ((getType === 'create' && !fields.ruleDesc) || ((getType === 'update' && fields.hasOwnProperty('ruleDesc') && fields.ruleDesc === ''))) {
            formIsValid = false;
            alert(RA_STR.ruleDescValidate);
        } else if ((getType === 'create' && !channelIds.length)) {
            formIsValid = false;
            alert(RA_STR.ruleSelectValidate);
        }
        if (getType === 'update' && formIsValid) {
            let dialogResult = window.confirm(RA_STR.ruleUpdateConfirm);
            if (!dialogResult) {
                formIsValid = false;
            }
        }
        if (this.syntaxCheckForFinalRuleStatement() === false) {
            formIsValid = false;
            return
        }
        if (this.count === 0) {
            var getGeneratedString = this.generateArraysForServerCall();
            var confUnParsed = '';
            if (dataElementsInfo.length && regionInnerText) {
                confUnParsed = `${getGeneratedString[0].ruleConfUnParsedStr}`;
                this.storeRuleMneomnic = `EXP@#@${getGeneratedString[0].ruleMnemonic}`
            } else {
                confUnParsed = `EXP@#@${leftExpressionList.join('')}`;
            }
        }
        if (formIsValid) {
            if (getType === 'create' && dataLoadingModal === false) {
                this.setState({ dataLoadingModal: true });
            }
            let node = ReactDOM.findDOMNode(this.refs['rule-expression-list']);
            var allNodes = node.querySelectorAll('.rule-expresiion');
            let groupText = '';
            allNodes && allNodes.forEach((eachNode) => {
                groupText += ' ' + eachNode.innerText;
            });

            const createUrl = `/organization/${this.props.orgConfigDetails.orgName}/rulesandscore/rule`;
            const getRuleMnemonic = fields.ruleMnemonic ? fields.ruleMnemonic : disableFields ? filterMnemonic.mnemonic : '';
            let data = {
                "ruleMnemonic": this.count === 0 ? getGeneratedString[0].ruleMnemonic : getRuleMnemonic,
                "ruleType": this.count === 0 ? getGeneratedString[0].ruleType : "COMBINATION_RULE",
                "ruleConfUnParsedStr": this.count === 0 ? `${confUnParsed}` : this.storeRuleMneomnic,
                "ruleDataUnParsedStr": "",
                "channelIds": selectedChannel.includes('All Channels') ? ["-1"] : channelIds,
                "txnTypes": selectedChannel.includes('All Actions') ? ["-1"] : ActionIds,
                "ruleDisplayName": this.count === 0 ? getGeneratedString[0].ruleMnemonic : fields.ruleDisplayName ? fields.ruleDisplayName : disableFields ? filterMnemonic.display : '',
                "ruleDesc": this.count === 0 ? getGeneratedString[0].ruleMnemonic : fields.ruleDesc ? fields.ruleDesc : disableFields ? filterMnemonic.description : '',
                "expressionType": this.count === 0 ? 2 : 1,
                "currentExpression": this.count === 0 ? null : groupText.trim(),
                "orgName": this.props.orgConfigDetails.orgName.trim(),
                "configName": this.props.orgConfigDetails.configName.trim()
            }
            const createRule = {
                method: disableFields ? 'PUT' : 'POST',
                url: `${serverUrl}${createUrl}`,
                data: data
            }
            const createRuleStatus = await getService(createRule);
            if (createRuleStatus.status === 200) {
                if (getType === 'create') {
                    if (this.count === 0) {
                        this.count += 1;
                        this.createRuleScoring('create');
                    } else {
                        this.setState({ dataLoadingModal: false });
                        alert(`Rule with mnemonic ${getRuleMnemonic} created successfully. Click OK and wait while the page is redirected.`);
                        this.props.refreshGridData();
                    }
                } else {
                    if (this.count === 0) {
                        this.count += 1;
                        this.createRuleScoring('update');
                    } else {
                        this.setState({ dataLoadingModal: false });
                        alert(`Rule with mnemonic ${getRuleMnemonic} updated successfully. Click OK and wait while the page is redirected.`);
                        this.props.refreshGridData();
                    }
                }
            } else {
                alert(createRuleStatus.data && createRuleStatus.data.errorList[0].errorMessage);
            }
        }
    }
    generateArraysForServerCall = () => {
        const { expressionList, ruletypeReGetgionId } = this.state;
        let DELIM_PARAM_VALUE = '@#@';
        let DELIM_PARAM_PAIRS = '###';
        // let finalRuleStatement = '';
        let paramStrArr = '';
        let groupExpressionInfo = [];
        expressionList.map((eachList) => {
            // if (eachList["name"] === "saved")
            //     finalRuleStatement += " " + eachList["expressionItem"];
            // else if (eachList["name"] === "oper")
            //     finalRuleStatement += " " + eachList["expressionItem"];
            // else if (eachList["name"] === "not")
            //     finalRuleStatement += " " + "NOT";
            // else if (eachList["name"] === "skipped")
            //     finalRuleStatement += " " + "SKIPPED";
            // else if (eachList["name"] === "obr")
            //     finalRuleStatement += " " + "(";
            // else if (eachList["name"] === "cbr")
            //     finalRuleStatement += " " + ")";
            if (eachList["name"] === "expr") {
                let getFinalData = '';
                let getRuleTypeFromList = ruletypeReGetgionId[eachList.fields['RULETYPE']];
                getFinalData = this.handleFinalRuleScreen(getRuleTypeFromList, eachList.fields);
                if (!getFinalData) {
                    getFinalData = eachList.fields;
                }
                var keys = Object.keys(getFinalData);
                var last = keys[keys.length - 1];
                for (var k in getFinalData) {
                    if (last !== k.trim()) {
                        paramStrArr += k.trim() + DELIM_PARAM_VALUE
                            + getFinalData[k].trim() + DELIM_PARAM_PAIRS;
                    } else {
                        paramStrArr += k.trim() + DELIM_PARAM_VALUE
                            + getFinalData[k].trim()
                    }
                }
                let ruleObj = {};
                ruleObj['ruleMnemonic'] = eachList.fields['RULEMNEMONIC'];
                ruleObj['ruleType'] = eachList.fields['RULETYPE'];
                ruleObj['ruleConfUnParsedStr'] = paramStrArr;
                groupExpressionInfo.push(ruleObj);
            }

        })
        return groupExpressionInfo
    }
    syntaxCheckForFinalRuleStatement = () => {
        var bracket = 0;
        const { expressionList } = this.state;
        let statement = [];
        function checkInd(ind) {
            return (ind != -1) && (ind < statement.length);
        }
        function expectEXPR(statement, ind) {
            return ((ind < statement.length) && (statement[ind]["name"] === "expr"));
        }
        function expectSAVED(statement, ind) {
            return ((ind < statement.length) && (statement[ind]["name"] === "saved"));
        }
        function expectOPER(statement, ind) {
            return ((ind < statement.length) && (statement[ind]["name"] === "oper"));
        }
        function expectNOT(statement, ind) {
            return ((ind < statement.length) && (statement[ind]["name"] === "not"));
        }
        function expectOBR(statement, ind) {
            if ((ind < statement.length) && (statement[ind]["name"] === "obr")) {
                ++bracket;
                return true;
            }
            return false;
        }
        function expectCBR(statement, ind) {
            if ((ind < statement.length) && (statement[ind]["name"] === "cbr")) {
                --bracket;
                return true;
            }
            return false;
        }
        function expectRule(statement, ind, must) {
            if (checkInd(ind) == false) {
                if (must == false)
                    return ind;
                return ind - 1;
            }

            if (expectEXPR(statement, ind) == true) {
                if (checkInd(ind + 1) == false)
                    return ind + 1;
                else if (expectOPER(statement, ind + 1) == true) {
                    return expectRule(statement, ind + 2, true);
                }
                return ind + 1;
            }

            if (expectSAVED(statement, ind) == true) {
                if (checkInd(ind + 1) == false)
                    return ind + 1;
                else if (expectOPER(statement, ind + 1) == true) {
                    return expectRule(statement, ind + 2, true);
                }
                return ind + 1;
            }

            if (expectOBR(statement, ind) == true) {
                var ind2 = expectRule(statement, ind + 1, true);
                if (checkInd(ind2) == false)
                    return ind2;
                if (expectCBR(statement, ind2) == true) {
                    if (checkInd(ind2 + 1) == false)
                        return ind2 + 1;
                    if (expectOPER(statement, ind2 + 1) == true)
                        return expectRule(statement, ind2 + 2, true);
                    else
                        return ind2 + 1;
                }
                return ind2;
            }

            if (expectNOT(statement, ind) == true)
                return expectRule(statement, ind + 1, true);
            return ind;
        }

        expressionList.map((eachExpression, key) => {
            statement.push({
                "id": key,
                "name": eachExpression.name
            });
        })
        var v = expectRule(statement, 0, true);
        if (bracket != 0) {
            var tok1 = expressionList[v - 1].expressionItem;
            alert(`Missing bracket after ${tok1}`);
            return false;
        } else if (v < statement.length) {
            var tok1 = expressionList[v].expressionItem;
            if (v == statement.length - 1) {
                alert(`Error at fragment [ ${tok1} ] - Unexpected end of rule expression.`);
            } else {
                var tok2 = expressionList[v + 1].expressionItem;
                alert(`Error after fragment [ ${tok1} ] - Unexpected fragment [ ${tok2} ]`);
            }
            return false;
        }
        return true;
    }
    handleClose = () => {
        this.props.modalClose();
    }
    _handleKeyPress = (e) => {
        const { actualDataElements, dateElements, channelIds, actualUpdatedDataElements } = this.state;
        let dataToModify = '';
        if (channelIds.length) {
            dataToModify = dateElements;
        } else {
            dataToModify = actualDataElements;
        }
        let filteredData = [];
        if (e.target.value) {
            dataToModify.forEach(obj => {
                const foundObj = _.filter(obj.value, function (val) {
                    return val.displayName && val.displayName.indexOf(e.target.value.toUpperCase()) !== -1;
                });
                if (foundObj) {
                    filteredData.push({
                        title: obj.title,
                        value: foundObj
                    })
                }
            })
        } else {
            filteredData = channelIds.length ? JSON.parse(JSON.stringify(actualUpdatedDataElements)) : actualDataElements;
        }
        this.setState({ dateElements: filteredData });
    }
    OperatorChanges = (e) => {
        let splitText = e.target.value.split("@");
        this.setState({ regionId: splitText[0], acualRegionId: e.target.value, regionInnerText: e.target.selectedOptions[0].text, dynamicRuleScreen: true });
    }
    selectedPanel = (getRegionId) => {
        const { ruleActionsOptions, dataElementsInfo, regionInnerText, expressionList, channelIds, disableFields, getUpdateInfo } = this.state;
        const childElementsInfo = dataElementsInfo.join('');
        const checkRuleMatch = expressionList.filter((eachRule) => {
            return eachRule.operator && eachRule.operator === getRegionId && eachRule.color
        })
        if (getRegionId === "USERKNOWN_SCREEN") {
            this.setState({ regionId: '' });
        }
        if (checkRuleMatch.length || disableFields) {
            ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'block';
            ReactDOM.findDOMNode(this.refs["dataElementsDropdown"]).classList.add('disabled');
            ReactDOM.findDOMNode(this.refs["operatorData"]).classList.add('disabled');
            ReactDOM.findDOMNode(this.refs['data-element-input']).classList.add('disabled');
            ReactDOM.findDOMNode(this.refs["enableAdd"]).style.display = 'none';
        } else {
            ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'none';
            ReactDOM.findDOMNode(this.refs["enableAdd"]).style.display = 'block';
            ReactDOM.findDOMNode(this.refs["dataElementsDropdown"]).classList.remove('disabled');
            ReactDOM.findDOMNode(this.refs["operatorData"]).classList.remove('disabled');
        }
        var ruleProps = {
            ruleOrgName: this.props.orgConfigDetails.orgName,
            configname: this.props.orgConfigDetails.configName.trim(),
            passDataElement: childElementsInfo,
            passDataOperator: regionInnerText,
            actionData: ruleActionsOptions,
            ruleSelectedChannels: channelIds,
            ruleFields: checkRuleMatch.length ? checkRuleMatch[0].fields : disableFields ? expressionList[0].fields : ''
        };
        if (ruleScreens[getRegionId]) {
            ReactDOM.findDOMNode(this.refs["hideDynamicRuleScreen"]).style.display = 'block';
            const Handler = ruleScreens[getRegionId];
            return <Handler ref="rules-screen" {...ruleProps} ></Handler>
        } else {
            return false
        }
    }
    buildFragmentGroup = async () => {
        const { dataElementsInfo, regionId, acualRegionId, leftExpressionList, regionInnerText } = this.state;
        const childElementsInfo = dataElementsInfo.join('');
        let getValidationValues = this.refs['rules-screen'] && this.refs['rules-screen'].handleValidation();
        if (getValidationValues[0].ruleData) {
            let getRuleType = this.getRuletype(dataElementsInfo.join(''), regionInnerText);
            let getSequenceID = await this.getUniqueSequenceId();
            let confUnParsed = `${"__SYS__" + getRuleType + "_" + getSequenceID}`;
            getValidationValues[0].ruleData['RULETYPE'] = getRuleType;
            getValidationValues[0].ruleData['RULEMNEMONIC'] = confUnParsed;
        }
        const { expressionList } = this.state;
        if (getValidationValues) {
            let obj = {};
            obj['ruleType'] = getValidationValues[0].ruleKey;
            obj['tnxElement'] = childElementsInfo;
            obj['operator'] = regionId;
            obj['actualOperator'] = acualRegionId;
            obj['expressionItem'] = getValidationValues[0].ruleExpression;
            obj['color'] = false;
            obj['deleteType'] = 'delete';
            obj['name'] = 'expr';
            obj['fields'] = getValidationValues[0].ruleData;
            expressionList.push(obj);
            leftExpressionList.push(childElementsInfo);
            this.setState({ expressionList: expressionList, operatorsData: [], dynamicRuleScreen: false, regionId: '' }, () => {
                this.DynamicRuleScreen();
                this.resetOperatorSelection();
            });
        }
    }
    DynamicRuleScreen = () => {
        ReactDOM.findDOMNode(this.refs["hideDynamicRuleScreen"]).style.display = 'none';
        let node = ReactDOM.findDOMNode(this.refs['dataElementsDropdown']);
        var options = node && node.querySelectorAll('option');
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                options[i].selected = false;
            }
        }
    }
    updateFragmentGroup = () => {
        const { expressionList, dataElementsInfo, regionId, disableFields } = this.state;
        let updateValidationValues = this.refs['rules-screen'] && this.refs['rules-screen'].handleValidation();
        const getMatchedItem = expressionList.findIndex((eachItem) => {
            return updateValidationValues && eachItem.ruleType === updateValidationValues[0].ruleKey && eachItem.tnxElement === dataElementsInfo.join('') && eachItem.operator === regionId
        })
        if (disableFields) {
            expressionList[0].expressionItem = updateValidationValues[0].ruleExpression;
            expressionList[0].fields = updateValidationValues[0].ruleData;
        } else {
            expressionList[getMatchedItem].expressionItem = updateValidationValues[0].ruleExpression;
            expressionList[getMatchedItem].fields = updateValidationValues[0].ruleData;
        }
        expressionList.map((eachList, key) => {
            return eachList.color = false
        })
        this.setState({ expressionList: expressionList, operatorsData: [], dynamicRuleScreen: false }, () => {
            ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'none';
            ReactDOM.findDOMNode(this.refs["enableAdd"]).style.display = 'block';
            ReactDOM.findDOMNode(this.refs["dataElementsDropdown"]).classList.remove('disabled');
            ReactDOM.findDOMNode(this.refs["operatorData"]).classList.remove('disabled');
            ReactDOM.findDOMNode(this.refs['data-element-input']).classList.remove('disabled');
            this.DynamicRuleScreen();
        });
    }
    deleteExpression = () => {
        const { expressionList } = this.state;
        let getIndex = this.getExpressionIndex();
        if (getIndex !== -1) {
            expressionList.splice(getIndex, 1)
        }
        this.setState({ expressionList: expressionList, operatorsData: [], dynamicRuleScreen: false, regionId: '' }, () => {
            ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'none';
            ReactDOM.findDOMNode(this.refs["enableAdd"]).style.display = 'block';
            ReactDOM.findDOMNode(this.refs["dataElementsDropdown"]).classList.remove('disabled');
            ReactDOM.findDOMNode(this.refs["operatorData"]).classList.remove('disabled');
            this.DynamicRuleScreen();
            this.resetOperatorSelection();
        });
    }
    selectedExpresionListItem = (getkey, getCompleteRuleData) => {
        const { expressionList } = this.state;
        expressionList.map((eachList, key) => {
            if (key !== getkey) {
                return eachList.color = false
            }
        })
        if (expressionList[getkey].color) {
            expressionList[getkey].color = false;
        } else {
            expressionList[getkey].color = true;
        }
        this.setState({ expressionList: expressionList }, () => {
            const activateList = expressionList.filter((eachList) => {
                if (eachList.deleteType = "delete" && eachList.color && eachList.ruleType !== false) {
                    return eachList
                }
            })
            if (!activateList.length) {
                ReactDOM.findDOMNode(this.refs["enableUpdate"]).style.display = 'none';
                ReactDOM.findDOMNode(this.refs["enableAdd"]).style.display = 'block';
                this.setState({ operatorsData: [], dynamicRuleScreen: false, regionId: '' }, () => {
                    this.DynamicRuleScreen();
                    this.resetOperatorSelection();
                });
            }
        });
        if (getCompleteRuleData) {
            let node = ReactDOM.findDOMNode(this.refs['dataElementsDropdown']);
            var options = node && node.querySelectorAll('option');
            for (var i = 0; i < options.length; i++) {
                options[i].selected = false;
                if (options[i].value === getCompleteRuleData.tnxElement) {
                    options[i].selected = true;
                }
            }
            this.handleDataElements();
            this.setState({ regionId: getCompleteRuleData.operator, acualRegionId: getCompleteRuleData.actualOperator, dynamicRuleScreen: true }, () => {
                this.selectedPanel(getCompleteRuleData.operator);
            });
        }
    }
    render() {
        const { exemptionOptions,modalGridActivate,acualRegionId, disableActions, dynamicRuleScreen, eleoperatorregionMapData, dataElementsInfo, regionId, operatorsData, dateElements, ruleChannelsOptions, dataLoadingModal, leftExpressionList, ruleActionsOptions, storeMnemonic, channelActionSelection, expressionList, filterMnemonic, disableFields, selectedChannel, disableChannels, fields } = this.state;
        return <div>
            {modalGridActivate ? <React.Fragment><Loader show={modalGridActivate} /><span className="loader-text-create"></span> </React.Fragment> :
            <div className="parent-modal container">
            <p className="desc">{RA_STR.ruleModalDesc}</p>
            <div className="col-md-7">
                <SingleInput
                    title={'Name'}
                    inputType={'text'}
                    name={'Name'}
                    controlFunc={this.handleChange.bind(this, 'ruleDisplayName')}
                    content={fields.hasOwnProperty('ruleDisplayName') ? fields.ruleDisplayName : filterMnemonic.display}
                />
                <SingleInput
                    title={'Mnemonic'}
                    inputType={'text'}
                    name={'Mnemonic'}
                    controlFunc={this.handleChange.bind(this, 'ruleMnemonic')}
                    disabled={disableFields}
                    content={filterMnemonic.mnemonic}
                />
                <TextArea
                    title={'Description'}
                    resize={false}
                    content={''}
                    name={'Description'}
                    content={fields.hasOwnProperty('ruleDesc') ? fields.ruleDesc : filterMnemonic.display}
                    placeholder={''}
                    controlFunc={this.handleChange.bind(this, 'ruleDesc')} />
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label">{RA_STR.ruleExemption}</label>
                    <div className="col-sm-8">
                        <select className="form-control"
                            name="exemption"
                            onChange={this.handleChange.bind(this, 'ruleExemption')}>
                            <option value={RA_STR_SELECT}>{RA_STR_SELECT}</option>
                            {exemptionOptions && exemptionOptions.map(opt => {
                                return (
                                    <option
                                        key={opt.exemptionid}
                                        value={opt.exemptionid}>{opt.displayname}</option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </div>
            <hr></hr>
            <p className="desc">{RA_STR.ruleChannelsActions}</p>
            <p>{RA_STR.ruleChannelsActionsBody}</p>
            <div className="col-md-12 row">
                <div className="col-md-4">
                    <div className="floatleft">
                        {RA_STR.ruleChannels}
                        <br></br>
                        <select name="selectedChannels" ref="selectedChannels" multiple="multiple" className={disableChannels || (disableFields && selectedChannel.includes('Enable Cross Channel') && selectedChannel.includes('All Channels')) ? "disabled selectedRulesetList" : "selectedRulesetList"} onChange={this.handleChannels}>
                            {ruleChannelsOptions && ruleChannelsOptions.map(opt => {
                                return (
                                    <option
                                        key={opt.channelId}
                                        value={opt.channelId}>{opt.channelName}</option>
                                );
                            })}
                        </select>
                        <Checkbox
                            type={'checkbox'}
                            options={channelActionSelection}
                            controlFunc={this.onchange}
                            selectedOptions={selectedChannel} />
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="floatleft">
                        {RA_STR.ruleActions}
                        <br></br>
                        <select className={disableActions ? "disabled selectedActions" : "selectedActions"} name="selectedActions" multiple="multiple" ref="ruleActionsListDisplay" onChange={this.handleChannels}>
                            {ruleActionsOptions && ruleActionsOptions.map(opt => {
                                return (
                                    <option
                                        key={opt.txTypeId}
                                        value={opt.txTypeId}>{opt.txType}</option>
                                );
                            })}
                        </select>
                        <Checkbox
                            type={'checkbox'}
                            options={this.state.Actions}
                            controlFunc={this.onchange}
                            disable={selectedChannel.includes('Enable Cross Channel') ? true : false}
                            selectedOptions={selectedChannel} />
                    </div>
                </div>
            </div>
            <hr></hr>
            <p className="desc">{RA_STR.ruleNewFragment}</p>
            <div className="col-md-12 row">
                <div className="col-md-4 no-padding">
                    <div className="floatleft">
                        {RA_STR.ruleSelectionElement}
                        <br></br>
                        <div className="fake-input">
                            <input type="text" placeholder="Please enter to filter" className="form-control search-input data-element-search" onKeyUp={this._handleKeyPress} ref='data-element-input' />
                            <img src="images/search.png" width="20" height="20" alt="no-img" onClick={(e) => this._handleKeyPress(e, 'search')} />
                        </div>
                        <div >
                            <select className={disableFields ? "disabled multiSelectOptionList" : "multiSelectOptionList"} multiple ref="dataElementsDropdown" onChange={this.handleDataElements}>
                                {eleoperatorregionMapData && dateElements && dateElements.map(opt => {
                                    return (
                                        <optgroup label={opt.title}>
                                            {opt.value && opt.value.map((eachInner) => {
                                                return <option key={eachInner.displayName} value={eachInner.name}>{eachInner.displayName}</option>
                                            })}
                                        </optgroup>
                                    );
                                })}
                            </select>
                        </div>
                    </div>
                    <div className="data-element-info">
                        <a data-tip data-for='clickme' className="float-right" data-event='click'> <img src="images/info.png" height="23" width="23"></img></a>
                        <ReactTooltip id='clickme' place='right' effect='solid' clickable={true}>
                            {dataElementsInfo.length ? <div>{dataElementsInfo.map((eachData) => { return <span className="tooltip-info">{eachData}</span> })}</div> : RA_STR.ruleDataDesc}
                        </ReactTooltip>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="floatleft">
                        {RA_STR.ruleOperator}
                        <select className={disableFields ? "disabled form-control" : "form-control"} onChange={this.OperatorChanges} ref="operatorData" value={acualRegionId}>
                            <option>--Select--</option>
                            {operatorsData && operatorsData.map((eachOpt, key) => {
                                return (
                                    <option
                                        key={eachOpt.ruleType}
                                        value={`${eachOpt.regionId}@${eachOpt.operatorId}`}>{eachOpt.operatorId}</option>
                                );
                            })}
                        </select>
                        <br></br>
                    </div>
                </div>
                <div className='col-md-5 dynamic-rule-screen' ref='hideDynamicRuleScreen'>
                    {regionId && dynamicRuleScreen ? this.selectedPanel(regionId) : RA_STR.ruleSelectionMsg}
                </div>
            </div>
            <div className="form-group form-submit-button data-element-add">
                <input className="secondary-btn" id="createRoleButton" type="submit" value="UPDATE" onClick={this.updateFragmentGroup} ref="enableUpdate"></input>
                <input className={`secondary-btn  ${!regionId ? ' disabled' : ''}`} id="createRoleButton" type="submit" value="ADD" onClick={this.buildFragmentGroup} ref="enableAdd"></input>
            </div>
            <div className="col-md-12 row">
                <div className="col-md-3 no-padding">
                    <div className="floatleft">
                        {RA_STR.ruleSaved}
                        <br></br>
                        <select name="selectedRulesetList" size="10" className={disableFields ? "disabled selectedSavedRules" : "selectedSavedRules"} ref="mnemonicDropdown">
                            {storeMnemonic && storeMnemonic.map(opt => {
                                return (
                                    <option
                                        key={opt}
                                        value={opt}>{opt}</option>
                                );
                            })}
                        </select>
                    </div>
                </div>
                <div className="col-md-2">
                    <div className="arrow_btn tertiary-btn" id="submit" onClick={this.handleSelectedRules}> &gt; </div>
                </div>
                <div className="col-md-7">
                    {RA_STR.ruleDeveloped}
                    <div className="row">
                        <span className={`arrow_btn tertiary-btn-2 ${disableFields ? ' disabled' : ''}`} id="submit" onClick={this.handleSavedRules}> {RA_STR.ruleAnd}</span>
                        <span className={`arrow_btn tertiary-btn-2 ${disableFields ? ' disabled' : ''}`} id="submit" onClick={this.handleSavedRules}> {RA_STR.ruleOr}</span>
                        <span className={`arrow_btn tertiary-btn-2 ${disableFields ? ' disabled' : ''}`} id="submit" onClick={this.handleSavedRules}> {RA_STR.ruleNot}</span>
                        <span className="arrow_btn tertiary-btn-2" id="submit" onClick={this.handleSavedRules}>{RA_STR.ruleSkipped}</span>
                        <span className={`arrow_btn tertiary-btn-2 ${disableFields ? ' disabled' : ''}`} onClick={this.handleSavedRules}> (</span>
                        <span className={`arrow_btn tertiary-btn-2 ${disableFields ? ' disabled' : ''}`} onClick={this.handleSavedRules}> )</span>
                        <div className="float-right display-right row">
                            <span className={`arrow_btn tertiary-btn-2`} id="submit" onClick={this.handleExpressionPosition.bind(this, 'left')}>&lt;</span>
                            <span className="arrow_btn tertiary-btn-2" id="submit" onClick={this.handleExpressionPosition.bind(this, 'right')}>&gt;</span>
                        </div>
                        <div className="form-control" id="exampleFormControlTextarea1" rows="5" ref="rule-expression-list">
                            {expressionList.map((eachChannel, key) => {
                                return <div className={eachChannel.color ? 'rule-expresiion rule-developed' : 'rule-expresiion'} onClick={this.selectedExpresionListItem.bind(this, key, expressionList[key])} data-showing-type={eachChannel.deleteType}>{eachChannel.expressionItem}{eachChannel.color}</div>
                            })}
                        </div>
                    </div>
                    <div className="float-right expression">
                        {parseInt(this.props.completeRuleData.expresionLimit) - leftExpressionList.length} {RA_STR.ruleExpressionLeft}
                    </div>
                    <br></br>
                    <div className="float-left row">
                        <div className="form-group form-submit-button">
                            <input className={`secondary-btn ${disableFields ? ' disabled' : ''}`} type="submit" value="DELETE" onClick={this.deleteExpression}></input>
                        </div>
                        <div className="form-group form-submit-button clear-button">
                            <input className={`secondary-btn ${disableFields ? ' disabled' : ''}`} type="submit" value="CLEAR" onClick={this.clearSavedRules}></input>
                        </div>
                    </div>
                </div>
            </div>
            <hr></hr>
            {!dataLoadingModal ? <div className="float-left row">
                <div className="form-group form-submit-button">
                    <input className="custom-default-btn" id="createRoleButton" type="submit" value="CANCEL" onClick={this.handleClose}></input>
                </div>
                <div className="form-group form-submit-button clear-button">
                    {disableFields ?
                        <input className="secondary-btn" type="submit" value="UPDATE" onClick={() => this.createRuleScoring('update')}></input> :
                        <input className="secondary-btn" id="createRoleButton" type="submit" value="CREATE" disabled={!expressionList.length ? true : false} onClick={() => this.createRuleScoring('create')}></input>
                    }
                </div>

            </div> : ''}
            {dataLoadingModal ? <React.Fragment><Loader show={dataLoadingModal} /><span className="loader-text-create">{RA_STR.ruleCreateMessage}</span> </React.Fragment> : ''}
        </div >}</div>
    }
}
export default RulesScoringModal;