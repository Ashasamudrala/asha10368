import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class YearCheckScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {}
    }
  }
  componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleRuleValidation = (e) => {
    var val = e.target.value;
    if (val.length > 4 || val.length < 4) {
      alert(RA_STR.ruleScreenYear);
      e.target.value = "";
      return
    }
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["THRESHOLD"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenYear);
      return
    }
    if(formIsValid){
      fields['OPERATOR']=this.props.passDataOperator;
      fields['TAGNAME1']=this.props.passDataElement;
      let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='yearCheck';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} ${fields["THRESHOLD"]}`;
			obj['ruleData'] = fields;
      ruleGroup.push(obj);
      return ruleGroup
    }
  }
  render() {
    const {fields}=this.state;
    return 	<div id="YearCheckScreen" >
		{RA_STR.ruleYearCheck}
		<input name="txt_YearCheckScreen" className="form-control"  id="txt_YearCheckScreen" type="text"  onChange={this.handleInputChanges.bind(this, 'THRESHOLD')} value={fields['THRESHOLD']} onBlur={this.handleRuleValidation}/>
	</div>;
  }
}

export default YearCheckScreen;
