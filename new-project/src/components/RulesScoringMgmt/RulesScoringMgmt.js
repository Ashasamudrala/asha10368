import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import Modal from 'react-bootstrap/Modal';
import RulesScoringModal from './RulesScoringModal';
import CA_Utils from '../../shared/utlities/commonUtils';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl, RA_API_URL, LOADING_TEXT } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import MaterialTable from "material-table";
import _ from 'underscore';
import Loader from '../../shared/utlities/loader';
import './RulesScoringMgmt.scss';
class RulesScoringMgmt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMnemonic: '',
      defaultScore: '',
      ruleOrgName: '',
      ruleOptions: [],
      orgName: '',
      show: false,
      dataLoading: false,
      hideDefault: true,
      priorityData: [],
      scoringDetails: [],
      storeActiveList: [],
      storeActualReference: [],
      ruleActionsOptions: [],
      ruleChannelsOptions: [],
      storeCompleteData: [],
      hideTable: true,
      configName: '',
      columns: [
        {
          title: '', field: 'display',
          render: (rowData) =>
            <div className="custom-name-render">
                <span className={rowData.priority/10} onClick={rowData.mnemonic !== "SCORINGHTTPCALLOUT" && rowData.mnemonic !== "EVALHTTPCALLOUT" ? this.setShow.bind(this, rowData.mnemonic) :''}>{rowData.display}</span>
            </div>
        },
        {
          title: RA_STR.ruleEnable, field: 'enable', render: (rowData) =>
            <div class="custom-rule-message">
              {this.renderActiveList(rowData.mnemonic, 'score') ? <input id={`customCheckbox${rowData.mnemonic}`} type="checkbox" checked={this.renderActiveList(rowData.mnemonic, 'enable')} disabled={true} /> :
                "This rule"}
            </div>
        },
        { title: RA_STR.riskScore, field: 'score', render: (rowData) => this.renderActiveList(rowData.mnemonic, 'score') ? this.renderActiveList(rowData.mnemonic, 'score') : 'has no' },
        { title: RA_STR.rulePriority, field: 'index', render: (rowData) => this.renderActiveList(rowData.mnemonic, 'priority') ? this.renderActiveList(rowData.mnemonic, 'priority') / 10 : 'active configuration' },
        {
          title: RA_STR.ruleEnable, field: 'enable',
          render: (rowData) =>
            <div className="checkbox">
              <input id={`customCheckbox2${rowData.mnemonic}`} type="checkbox" checked={rowData.enable} onClick={this.handleSelection.bind(this, rowData.mnemonic)} />
              <label htmlFor={`customCheckbox2${rowData.mnemonic}`}></label>
            </div>
        },
        {
          title: RA_STR.riskScore, field: 'score', render: (rowData) =>
            <div className="risk-score-change">
              <input className="form-control" type="text" disabled={!rowData.enable} value={rowData.score} onChange={this.handleSelection.bind(this, rowData.mnemonic)} onBlur={this.handleSelection.bind(this, rowData.mnemonic)} />
            </div>
        },
        {
          title: RA_STR.ruleAdvice, field: 'score', render: (rowData) =>
            <span className="custom-value">
              {this.renderCustomAdvice(rowData.score)}
            </span>
        },
        {
          title: RA_STR.rulePriority, field: 'index', render: rowData =>
            <div className="dynamic-form-select">
              <select className="form-select form-control custom-select-sm" value={rowData.index} onChange={this.handleSelection.bind(this, rowData.mnemonic)}>{
                this.state.priorityData.map((eachvalue, key) => {
                  return <option key={key} value={eachvalue}>{eachvalue}</option>
                })
              }
              </select>
            </div>
        },
      ],
      data: []
    }
  }
  componentDidMount = async () => {
    this.formGrid();
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  componentDidUpdate = () => {
    const getGridHeader = document.querySelector('.MuiTableRow-head');
    const mainHeader = document.querySelector('.main-header');
    if (getGridHeader && !mainHeader) {
      getGridHeader.insertAdjacentHTML('beforebegin', '<tr class="main-header"><th colspan="2">RULENAME</th><th colspan="3">ACTIVE</th><th colspan="4">PROPOSED</th></tr>');
    }
  }
  formGrid = async () => {
    const urlParams = new URLSearchParams(window.location.search);
    const getOrgName = urlParams.get('orgname');
    let orgName = '';
    if (getOrgName) {
      orgName = getOrgName;
    } else {
      orgName = RA_STR.ruleOrg;
    }
    this.setState({})
    const RulesetUrl = `/organization/${orgName}/rulesandscore`;
    const getRulesetData = {
      method: 'GET',
      url: `${serverUrl}${RulesetUrl}`
    };
    const getRulesetDataStatus = await getService(getRulesetData);
    if (getRulesetDataStatus && getRulesetDataStatus.status === 200) {
      this.setState({ ruleOptions: CA_Utils.objToArray(getRulesetDataStatus.data.ruleList, 'object') });
      const ruleChannels = `/organization/${orgName}/rulesandscore/channels`;
      const ruleActions = `/organization/${orgName}/rulesandscore/rfactions`;
      const getChannels = {
        method: 'GET',
        url: `${serverUrl}${ruleChannels}`
      };
      const getChannelsStatus = await getService(getChannels);
      if (getChannelsStatus.status === 200) {
        this.setState({ ruleChannelsOptions: getChannelsStatus.data.chanelList });
        const getActions = {
          method: 'GET',
          url: `${serverUrl}${ruleActions}`
        };
        const getActionsStatus = await getService(getActions);
        if (getActionsStatus.status === 200) {
          this.setState({ ruleActionsOptions: getActionsStatus.data.txTypeNames });
        }
      }
    }
  }
  renderRulesetTable = async (getConfig) => {
    const { ruleOrgName } = this.state;
    const RulescoreUrl = `/organization/${ruleOrgName}/rulesandscore`;
    const getRulesetGridData = {
      method: 'POST',
      url: `${serverUrl}${RulescoreUrl}`,
      data: {
        configName: getConfig.trim()
      }
    };
    const getRulesetGridDataStatus = await getService(getRulesetGridData);
    if (getRulesetGridDataStatus.status === 200) {
      const storeIndex = getRulesetGridDataStatus.data.proposedRules && getRulesetGridDataStatus.data.proposedRules.map((eachindex) => {
        return eachindex.index
      })
      storeIndex.sort((a, b) => a - b);
      const getScoringDetails = {
        method: 'GET',
        url: `${serverUrl}${RulescoreUrl}${RA_API_URL.scoredetails}`,

      };
      const getScoringDetailsStatus = await getService(getScoringDetails);
      if (getScoringDetailsStatus.status === 200) {
        let tableRowMap=getRulesetGridDataStatus.data.proposedRules.filter((eachRule)=>{
           return eachRule.rule
        })
        let tableRowMapSortData=_.sortBy(tableRowMap, 'priority');
        this.setState({ storeCompleteData: getRulesetGridDataStatus.data, storeActiveList: getRulesetGridDataStatus.data.activeRules, storeActualReference: JSON.parse(JSON.stringify(getRulesetGridDataStatus.data.proposedRules)), data:tableRowMapSortData, priorityData: storeIndex, scoringDetails: getScoringDetailsStatus.data.scoreList, hideTable: false, dataLoading: false });
      }
    }
  }
  renderCustomAdvice = (getScore) => {
    var result = this.state.scoringDetails.filter(function (v, i) {
      return ((getScore >= v["startScore"] && getScore <= v["endScore"]));
    })
    if (result[0]) {
      return result[0].advice
    }
  }
  setShow = (getMnemonic) => {
    if (!getMnemonic && this.state.show) {
      let dialogResult = window.confirm(RA_STR.modalCloseMsg);
      if (dialogResult) {
        this.setState({ show: !this.state.show });
      }
    } else {
      this.setState({ show: !this.state.show, showMnemonic: getMnemonic });
    }
  }
  handleRuleset = (e) => {
    this.setState({ dataLoading: true, hideTable: true });
    let getValue = e.target.selectedOptions[0].innerHTML;
    let splitValues = getValue.split('-');
    if (splitValues[1]) {
      this.setState({ configName: splitValues[1].trim(), ruleOrgName: splitValues[0].trim() }, () => {
        this.renderRulesetTable(splitValues[1]);
      });
    }
  }
  renderActiveList = (getMnemonic, getAttribute) => {
    var getName = this.state.storeActiveList.filter((eachValue) => {
      return eachValue.mnemonic === getMnemonic
    })
    return getName[0] ? getName[0][getAttribute] : ''
  }
  handleSelection = (getkey, e) => {
    let storeActualData = this.state.storeActualReference;
    let getactualdata = this.state.data;
    let mnemonicIndex = getactualdata && getactualdata.findIndex(eachValue => eachValue.mnemonic === getkey);
    if (e.target.type === "checkbox") {
      if (e.target.checked) {
        getactualdata[mnemonicIndex].enable = true;
      } else {
        getactualdata[mnemonicIndex].enable = false;
      }
    } else if (e.target.type === "text") {
      if ((e.target.value > 100 && e.type === "blur") || (e.target.value === "" && e.type === "blur")) {
        alert(RA_STR.ruleValidation);
        getactualdata[mnemonicIndex].score = storeActualData[mnemonicIndex].score;
      } else {
        getactualdata[mnemonicIndex].score = e.target.value;
      }
    } else if (e.target.type === "select-one") {
      getactualdata.forEach((eachData, key) => {
        if ((key >= parseInt(e.target.value) - 1) && (key < mnemonicIndex)) {
          eachData.index += 1;
        } else if (key === mnemonicIndex) {
          eachData.index = parseInt(e.target.value);
        }
      })
    }
    this.setState({ data: getactualdata });
  }
  saveRuleScoring = async () => {
    const { storeCompleteData } = this.state;
    if (!storeCompleteData.proposedDefaultScore) {
      alert(RA_STR.ruleValidation);
      this.refs.scoreMessage.focus();
    } else {
      const { ruleOrgName, configName } = this.state;
      const RulescoreUrl = `/organization/${ruleOrgName}/rulesandscore/rulesscoringinfo`;
      let attributesArray = [];
      this.state.data.forEach((eachData) => {
        var ruleData = {
          ruleName: eachData.mnemonic,
          scoringPriority: eachData.index * 10,
          scoringEnable: eachData.enable,
          score: eachData.score
        }
        attributesArray.push(ruleData);
      })
      const saveRuleScoringDetails = {
        method: 'POST',
        url: `${serverUrl}${RulescoreUrl}`,
        data: {
          "ruleScoringInfoList": attributesArray,
          "orgName": ruleOrgName.trim(),
          "configName": configName.trim(),
          "defaultScore": storeCompleteData.proposedDefaultScore
        }
      };
      const saveRuleScoringDetailsStatus = await getService(saveRuleScoringDetails);
      if (saveRuleScoringDetailsStatus.status === 200) {
        this.props.activateSuccessList(true, saveRuleScoringDetailsStatus.data);
      }
    }
  }
  handleDefaultScore = (e) => {
    const { storeCompleteData } = this.state;
    storeCompleteData.proposedDefaultScore = e.target.value
    this.setState({ storeCompleteData: storeCompleteData });
  }
  handleCloseModal = () => {
    this.setState({ show: false });
  }
  formChannelsGroup = (getChannelIds) => {
    const { ruleChannelsOptions } = this.state;
    let MatchedChannel = ruleChannelsOptions && ruleChannelsOptions.filter((eachChannel) => {
      if (eachChannel && eachChannel.channelId) {
        return getChannelIds.includes(eachChannel.channelId)
      }
    })
    return MatchedChannel.map((eachChannel) => {
      return <div>{eachChannel.channelDispalyName}</div>
    })
  }
  formActionsGroup = (getChannelIds) => {
    const { ruleActionsOptions } = this.state;
    let MatchedChannel = ruleActionsOptions && ruleActionsOptions.filter((eachChannel) => {
      if (eachChannel && eachChannel.txTypeId) {
        return getChannelIds.includes(eachChannel.txTypeId)
      }
    })
    return MatchedChannel.map((eachChannel) => {
      return <div>{eachChannel.displayName}</div>
    })
  }
  deleteRule = async (getruleMnemonic) => {
    const { configName, ruleOrgName } = this.state;
    const deleteRuleUrl = `/organization/${ruleOrgName}/rulesandscore/rule`;
    const deleteRule = {
      method: 'DELETE',
      url: `${serverUrl}${deleteRuleUrl}`,
      data: {
        "configName": configName,
        "ruleMnemonic": getruleMnemonic
      }
    };
    const deleteRuleStatus = await getService(deleteRule);
    if (deleteRuleStatus.status === 200) {
      this.props.activateSuccessList(true, deleteRuleStatus.data);
      this.refreshAPIData();
    }
  }
  refreshAPIData = () => {
    this.setState({
      hideTable: true, dataLoading: true
    }, () => {
      this.handleCloseModal();
      this.renderRulesetTable(this.state.configName)
    })
  }
  render() {
    const { ruleOptions, storeCompleteData, show, columns, data, hideTable, showMnemonic, ruleOrgName, configName, ruleChannelsOptions, ruleActionsOptions } = this.state;
    return <div className="main rules-mgmt-parent">
      <div className="no-padding">
        <h2 className="title">{RA_STR.ruleHeader}</h2>
        <p className="desc">{RA_STR.ruleBody}</p>
      </div>
      <div className="col-md-7">
        <Select
          name={'ruleset'}
          title={'Select the Ruleset'}
          required={true}
          options={ruleOptions ? ruleOptions : ['']}
          controlFunc={this.handleRuleset}
          placeholder={'Select'} />
      </div>
      {this.state.dataLoading ? <React.Fragment><Loader show={this.state.dataLoading} /><span className="loader-text">{LOADING_TEXT}</span> </React.Fragment> : <Loader show={this.state.dataLoading} />}
      {!hideTable ?
        <div>
          <span className="rule-load">{storeCompleteData.ruleLimit}</span>
          <div className="float-right">
            <input type="submit" className="custom-secondary-btn m-3" onClick={() => this.setShow()} value="ADD A NEW RULE"></input>
          </div>
          <div style={{ maxWidth: '100%', clear: 'both' }}>
            <MaterialTable
              columns={columns}
              data={data}
              options={{
                selection: false,
                paging: false,
                search: false,
                sorting: false,
              }}
              detailPanel={[
                {
                  openIcon: () => {
                    return <span class="table-left-icon">[-]</span>
                  },
                  icon: () => {
                    return <span class="table-left-icon">[+]</span>
                  },
                  render: rowData => {
                    return (
                      <div className="internal-grid">
                        <div className="internamMnemonic"><b>Mnemonic {rowData.mnemonic}</b></div>
                        <div>{!this.renderActiveList(rowData.mnemonic, 'score') ? <div className="config-rule"><b>{RA_STR.notAvailableConfig}</b></div> : ''}</div>
                        <table className="table">
                          <thead>
                            <tr>
                              <th><b>{RA_STR.ruleDetail}</b></th>
                              <th><b>{RA_STR.ruleActive}</b></th>
                              <th><b>{RA_STR.ruleProposed}</b></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><b>{RA_STR.ruleDesc}</b></td>
                              <td>{this.renderActiveList(rowData.mnemonic, 'description')}</td>
                              <td>{rowData.description}</td>
                            </tr>
                            <tr>
                              <td><b>{RA_STR.ruleExpress}</b></td>
                              <td>{this.renderActiveList(rowData.mnemonic, 'expression')}</td>
                              <td>{rowData.expression}</td>
                            </tr>
                            <tr>
                              <td><b>{RA_STR.ruleChannels}</b></td>
                              <td>{this.renderActiveList(rowData.mnemonic, 'channelIdStr') === "-1" ? 'All' : this.formChannelsGroup(this.renderActiveList(rowData.mnemonic, 'channelIdStr'))}</td>
                              <td>{rowData.channelIdStr === "-1" ? 'All' : this.formChannelsGroup(rowData.channelIdStr)}</td>
                            </tr>
                            <tr>
                              <td><b>{RA_STR.ruleActions}</b></td>
                              <td>{this.renderActiveList(rowData.mnemonic, 'txnIdStr') === "-1" ? 'All' : this.formActionsGroup(this.renderActiveList(rowData.mnemonic, 'txnIdStr'))}</td>
                              <td>{rowData.txnIdStr === "-1" ? 'All' : this.formActionsGroup(rowData.txnIdStr)}</td>
                            </tr>
                          </tbody>
                        </table>
                        {!this.renderActiveList(rowData.mnemonic, 'score') ? <div className="delete-rule float-right" onClick={this.deleteRule.bind(this, rowData.mnemonic)}>Delete this rule</div> : ''}
                      </div>
                    )
                  },
                }
              ]}
            />
          </div>
          <div className="float-right">
            <input type="submit" className="custom-secondary-btn m-3" onClick={() => this.setShow()} value="ADD A NEW RULE"></input>
          </div>
          <div className="clear-floats">
            <span>{RA_STR.defaultScore}</span>
            <table className="edl_table">
              <thead>
                <tr>
                  <th colSpan="2">{RA_STR.ruleActive}</th>
                  <th colSpan="2">{RA_STR.ruleProposed}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>{RA_STR.ruleScore}</th>
                  <th>{RA_STR.ruleAdvice}</th>
                  <th>{RA_STR.ruleScore}</th>
                  <th>{RA_STR.ruleAdvice}</th>
                </tr>
                <tr>
                  <td>{storeCompleteData.activeDefalutScore}</td>
                  <td>{this.renderCustomAdvice(storeCompleteData.activeDefalutScore)}</td>
                  <td><input type="text" ref="scoreMessage" className="form-control" value={storeCompleteData.proposedDefaultScore} onChange={this.handleDefaultScore}></input></td>
                  <td>{this.renderCustomAdvice(storeCompleteData.proposedDefaultScore)}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="form-group form-submit-button">
            <input className="secondary-btn" type="submit" value="SAVE" onClick={this.saveRuleScoring} ></input>
          </div>
        </div>
        : ''
      }

      <Modal
        show={show}
        onHide={() => this.setShow()}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            <h2 class="title">{RA_STR.ruleModalTitle}</h2>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <RulesScoringModal ruleChannelsOptionsModal={ruleChannelsOptions} ruleActionsOptionsModal={ruleActionsOptions} modalClose={this.handleCloseModal} noSortAddRuleData={_.sortBy(this.state.storeActualReference,'priority')} addRuleData={data} selectedMnemonic={showMnemonic} orgConfigDetails={{ "orgName": ruleOrgName, "configName": configName }} refreshGridData={this.refreshAPIData} completeRuleData={storeCompleteData}></RulesScoringModal>
        </Modal.Body>
      </Modal>
    </div>;
  }
}

export default RulesScoringMgmt;
