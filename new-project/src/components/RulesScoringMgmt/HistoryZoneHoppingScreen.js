import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import AdditionalFilter from './AdditionalFilter';
class HistoryZoneHoppingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { HISTTRANSCTIONSTATUS: 'ALL', COMBINATION_STR_EXT: 'NULL' }
    }
  }
  componentDidMount = () => {
    if (this.props.ruleFields) {
      this.setState({ fields: this.props.ruleFields });
    }
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["MAX_SPEED"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenMaxSpeed);
      return
    }
    if (!fields["MAX_USER"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenUsers);
      return
    }
    if (!fields["UNCERTAINITY_OFFSET"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenTolerance);
      return
    }
    if (formIsValid) {
      let obj = {};
      let ruleGroup = [];
      let getFilterItem = this.refs.additionalFilter.getFilterData();
      fields['COMBINATION_STR_EXT']=getFilterItem;
      obj['ruleKey'] = 'historyZoneScreen';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} MAX SPEED = ${fields["MAX_SPEED"]} MILES/HOUR & MAX USER COUNT = ${fields["UNCERTAINITY_OFFSET"]} & UNCERTAINTY = ${fields["MAX_USER"]} MILES of status ${fields["HISTTRANSCTIONSTATUS"]} [Additional History filter:${fields['additionalFilter']}]`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj)
      return ruleGroup
    } else {
      return formIsValid
    }
  }
  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  render() {
    const { fields } = this.state;
    return <div id="HistoryZoneHoppingScreen">
      <table>
        <tr>
          <td>
            {RA_STR.ruleSpeedTravel}
          </td>
          <td>
            <input className="form-control" id="maxSpeed_HistoryZoneHoppingScreen" type='text' size="5" maxlength="5" onChange={this.handleInputChanges.bind(this, 'MAX_SPEED')} value={fields['MAX_SPEED']} />
          </td>
          <td>{RA_STR.ruleMilePH}</td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleMaxUSers}
          </td>
          <td>
            <input className="form-control" id="numUsers_HistoryZoneHoppingScreen" type='text' size="5" maxlength="2" onChange={this.handleInputChanges.bind(this, 'MAX_USER')} value={fields['MAX_USER']} />
          </td>
        </tr>
        <br></br>
        <tr>
          <td>
            {RA_STR.ruleDistanceTolerance}
          </td>
          <td>
            <input className="form-control" id="maxDist_HistoryZoneHoppingScreen" type='text' size="5" maxlength="5" onChange={this.handleInputChanges.bind(this, 'UNCERTAINITY_OFFSET')} value={fields['UNCERTAINITY_OFFSET']} />&nbsp;
          </td>
          <td>{RA_STR.ruleMiles}</td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxStatus}
          </td>
          <td>
            <select className="form-control" id="histTransactionStatus_VelocityScreen" onChange={this.handleInputChanges.bind(this, 'HISTTRANSCTIONSTATUS')} value={fields['HISTTRANSCTIONSTATUS']}>
              <option value='ALL'>{RA_STR.ruleTnxStatusAll}</option>
              <option value='APPROVED'>{RA_STR.ruleTnxStatusApproved}</option>
              <option value='DENIED'>{RA_STR.ruleTnxStatusDenied}</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>{RA_STR.ruleAddFilter}</td>
          <td>
            <AdditionalFilter ruleOrgName={this.props.ruleOrgName} defaultFilter={fields['COMBINATION_STR_EXT']} ref="additionalFilter"></AdditionalFilter>
          </td>
        </tr>
      </table>
    </div>;
  }
}

export default HistoryZoneHoppingScreen;
