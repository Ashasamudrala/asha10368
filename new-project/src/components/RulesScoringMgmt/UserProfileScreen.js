import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';

class UserProfileScreen extends Component {
  render() {
    return <div id="UserProfileScreen">
    <table>
      <tr>
        <td>
          {RA_STR.ruleThresholdCount}
        </td>
        <td>
           <input type="text" name="selectThresholdCount_userprofilescreen" id="selectThresholdCount_userprofilescreen" size="3" value="5" />
        </td>
      </tr>
    </table>
  </div>;
  }
}

export default UserProfileScreen;
