import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
class IPVelocityScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { DURATION_UNIT: 'MINUTES', org: 'ORG' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleRuleValidation = (e) => {
		var val = e.target.value;
		if (CA_Utils.checkForInteger(val, 1, '') == false) {
			e.target.value = "";
			alert(RA_STR.ruleValidationMsg);
			return false;
		}
	}
	handleChange = (field, e) => {
		let fields = this.state.fields;
		fields[field] = e.target.value;
		this.setState({ fields: fields });
	}
	handleValidation = () => {
		let fields = this.state.fields;
		let formIsValid = true;

		if (!fields["FREQUENCY"]) {
			formIsValid = false;
			alert(RA_STR.ruleVelocity);
		}
		if (!fields["DURATION"]) {
			formIsValid = false;
			alert(RA_STR.ruleTime);
		}
		if (formIsValid) {
			let obj = {};
			let ruleGroup = [];
			obj['ruleKey']='IPVelocityScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} GREATER THAN ${fields["FREQUENCY"]} IN LAST ${fields["DURATION"]} ${fields["DURATION_UNIT"]} ${(this.props.passDataElement === "DEVICEID" || this.props.passDataElement === "DEVICEALIAS") ? `[SCOPE: ${fields["org"]}]` : ''}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
		}
		return formIsValid;
	}
  render() {
	  const {fields}=this.state;
    return <div id='IPVelocityScreen'> 
		<table>
			<tr>
				<td>{RA_STR.ruleGreater}</td>
				<td>
					<input id='velocity_IPVelocityScreen' className="form-control" name='velocity_IPVelocityScreen'  type='text' maxLength='3' size='3' onChange={this.handleChange.bind(this, "FREQUENCY")} value={fields['FREQUENCY']} onBlur={this.handleRuleValidation}/>
				</td>
				<td>{RA_STR.ruleLast}</td>
				<td>
					<input id='time_IPVelocityScreen' className="form-control" name='time_IPVelocityScreen' type='text' maxLength='3' size='3' onChange={this.handleChange.bind(this, "DURATION")} value={fields['DURATION']} onBlur={this.handleRuleValidation}/>
				</td>
				<td>
					<select id="timeUnit_IPVelocityScreen" className="form-control" onChange={this.handleChange.bind(this, "DURATION_UNIT")} value={fields['DURATION_UNIT']}>
						<option value='MINUTES'>{RA_STR.ruleMins} </option>
						<option value='HOURS'>{RA_STR.ruleHours}</option>
					</select>
				</td>
			</tr>
		</table>
	</div>;
  }
}

export default IPVelocityScreen;
