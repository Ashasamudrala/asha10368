import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
class VelocityScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: { VELOCITY_DURATION_UNIT: 'MINUTES', DATA_SCOPE: 'ORG' }
		}
	}
	componentDidMount = () => {
		if (this.props.ruleFields) {
			this.setState({ fields: this.props.ruleFields });
		}
	}
	handleChange = (field, e) => {
		let fields = this.state.fields;
		fields[field] = e.target.value;
		this.setState({ fields: fields });
	}
	handleRuleValidation = (e) => {
		var val = e.target.value;
		if (CA_Utils.checkForInteger(val, 1, '') == false) {
			e.target.value = "";
			alert(RA_STR.ruleValidationMsg);
			return false;
		}
		return true;
	}
	handleValidation = () => {
		let fields = this.state.fields;
		let formIsValid = true;

		if (!fields["VELOCITY_TRANSACTION_COUNT"]) {
			formIsValid = false;
			alert(RA_STR.ruleVelocity);
		}
		if (!fields["VELOCITY_DURATION"]) {
			formIsValid = false;
			alert(RA_STR.ruleTime);
		}
		if (formIsValid) {
			let obj = {};
			let ruleGroup = [];
			obj['ruleKey'] = 'velocityScreen';
			obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} GREATER THAN ${fields["VELOCITY_TRANSACTION_COUNT"]} IN LAST ${fields["VELOCITY_DURATION"]} ${fields["VELOCITY_DURATION_UNIT"]} ${(this.props.passDataElement === "DEVICEID" || this.props.passDataElement === "DEVICEALIAS") ? `[SCOPE: ${fields["DATA_SCOPE"]}]` : ''}`;
			obj['ruleData'] = fields;
			ruleGroup.push(obj)
			return ruleGroup
		}
		return formIsValid;
	}
	render() {
		const { fields } = this.state;
		const selectedOperator = this.props.passDataElement;
		return <div id='VelocityScreen'>
			<table>
				<tr>
					<td>{RA_STR.ruleGreater}</td>
					<td>
						<input id='velocity_VelocityScreen' className="form-control" name='velocity_VelocityScreen' type='text' maxLength='3' size='3' value={fields['VELOCITY_TRANSACTION_COUNT']} onChange={this.handleChange.bind(this, "VELOCITY_TRANSACTION_COUNT")} onBlur={this.handleRuleValidation} />
					</td>
					<td>{RA_STR.ruleLast}</td>
					<td>
						<input id='time_VelocityScreen' className="form-control" name='time_VelocityScreen' type='text' maxLength='3' size='3' value={fields['VELOCITY_DURATION']} onChange={this.handleChange.bind(this, "VELOCITY_DURATION")} onBlur={this.handleRuleValidation} />
					</td>
					<td>
						<select id="timeUnit_VelocityScreen" className="form-control" value={fields['VELOCITY_DURATION_UNIT']} onChange={this.handleChange.bind(this, "VELOCITY_DURATION_UNIT")}>
							<option value='MINUTES'>{RA_STR.ruleMins} </option>
							<option value='HOURS'>{RA_STR.ruleHours}</option>
						</select>
					</td>
				</tr>
				<br></br>
				<tr>
					{(selectedOperator === "DEVICEID" || selectedOperator === "DEVICEALIAS") ?
						[<td><div id="VelocityScreen_ScopeApplicable1">
							{RA_STR.ruleScope}
						</div>
						</td>,
						<td colspan="3"><div id="VelocityScreen_ScopeApplicable2">
							<select id="velocity_datascope" name="velocity_datascope" className="form-control">
								<option value="DATA_SCOPE">{RA_STR.ruleOrgNameScreen}</option>
							</select>
						</div>
						</td>] : ''
					}
				</tr>
			</table>
		</div>
	}
}

export default VelocityScreen;
