import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import AdditionalFilter from './AdditionalFilter';
class CumulativeAmountCheckScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields : { tnxStatus: 'All', EXCL_CURR_TXN: 'NO',HISTORY_OF:'NULL' }
    }
  }
  componentDidMount = () => {
    if (this.props.ruleFields) {
      this.setState({ fields: this.props.ruleFields });
    }
  }
  handleChange = (field, e) => {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields['THRESHOLD']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenCumulative);
      return
    }
    if (!fields['THRESHOLDTRANSACTION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenThresholdCount);
      return
    }
    if (!fields['DURATION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenLookback);
      return
    }
    if (!fields['MAXTRANSACTION']) {
      formIsValid = false;
      alert(RA_STR.ruleScreenMaxTnx);
      return
    }
    if (formIsValid) {
      let obj = {};
      let ruleGroup = [];
      let getFilterItem = this.refs.additionalFilter.getFilterData();
      fields['HISTORY_OF']=getFilterItem;
      fields['TAGNAME']=this.props.passDataElement;
      obj['ruleKey'] = 'actionCumulative';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} WITH amount = ${fields['THRESHOLD']} thresholdTransaction = ${fields['THRESHOLDTRANSACTION'] } duration = ${fields['DURATION']} maxTransaction = ${fields['MAXTRANSACTION']} of status ${fields['HISTTRANSCTIONSTATUS']} [Additional History filter: ${fields['EXCL_CURR_TXN']}] [Exclude Curr Txn: ${fields['additionalFilter']}]`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj)
      return ruleGroup
    } else {
      return formIsValid
    }
  }
  render() {
    const{fields}=this.state;
    return <div id='CumulativeAmountCheckScreen'>
      <table>
        <tr>
          <td>
            {RA_STR.ruleCummulativeAmount}
          </td>
          <td>
            <input className="form-control" id="cumulativeAmountCheckScreen_amount" type='text' maxlength="100" onChange={this.handleChange.bind(this, "THRESHOLD")} value={fields['THRESHOLD']}/>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxCount}
          </td>
          <td>
            <input className="form-control" id="cumulativeAmountCheckScreen_txnCount" type='text' maxlength="100" onChange={this.handleChange.bind(this, "THRESHOLDTRANSACTION")} value={fields['THRESHOLDTRANSACTION']}/>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleLookBack}
          </td>
          <td>
            <input className="form-control" id="cumulativeAmountCheckScreen_duration" type='text' maxlength="100" onChange={this.handleChange.bind(this, "DURATION")} value={fields['DURATION']}/>
          </td>
          <td>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleMaxTnxCount}
          </td>
          <td>
            <input className="form-control" id="cumulativeAmountCheckScreen_maxTransactionLookBack" type='text' maxlength="100" onChange={this.handleChange.bind(this, "MAXTRANSACTION")} value={fields['MAXTRANSACTION']}/>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleTnxStatus}
          </td>
          <td>
            <select className="form-control" id="histTransactionStatus_VelocityScreen" onChange={this.handleChange.bind(this, "HISTTRANSCTIONSTATUS")} value={fields['HISTTRANSCTIONSTATUS']}>
              <option value='ALL'>{RA_STR.ruleTnxStatusAll}</option>
              <option value='APPROVED'>{RA_STR.ruleTnxStatusApproved}</option>
              <option value='DENIED'>{RA_STR.ruleTnxStatusDenied}</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            {RA_STR.ruleExclcurrtxn}
          </td>
          <td>
            <select className="form-control" id="exclcurrtxn_VelocityScreen" onChange={this.handleChange.bind(this, "EXCL_CURR_TXN")} value={fields['EXCL_CURR_TXN']}>
              <option value='NO'>{RA_STR.ruleNo}</option>
              <option value='YES'>{RA_STR.ruleYes}</option>
            </select>
          </td>
        </tr>
        <tr>
        <td>
            {RA_STR.ruleAddFilter}
          </td>
          <td>
          <AdditionalFilter ruleOrgName={this.props.ruleOrgName} defaultFilter={fields['HISTORY_OF']} ref="additionalFilter"></AdditionalFilter>
          </td>
        </tr>
      </table>
    </div>;
  }
}

export default CumulativeAmountCheckScreen;
