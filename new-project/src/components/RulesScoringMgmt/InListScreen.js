import React, { Component } from 'react';
import { RA_STR } from '../../shared/utlities/messages';
import { serverUrl } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import CA_Utils from '../../shared/utlities/commonUtils';
import './RulesScreens.scss';

class ListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { match: 'EXACT' },
      showList: false,
      dataSetListItems: []
    }
  }
  componentDidMount = async () => {
    const { fields } = this.state;
    const listSecureDataElements = `/organization/${this.props.ruleOrgName}/rulesandscore/rulebuildersettings`;
    const DataSetUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/orglistoflistsforelement/${this.props.configname}`;
    const secureListUrl = `/organization/${this.props.ruleOrgName}/rulesandscore/listOfsecurelists/${this.props.configname}`;
    const dataSetMap = {
      method: 'GET',
      url: `${serverUrl}${DataSetUrl}`
    };
    const dataSetMapStatus = await getService(dataSetMap);
    const listSimpleLists = CA_Utils.getListData(dataSetMapStatus.data);
    if (dataSetMapStatus.status === 200) {
      const listSecureListMap = {
        method: 'GET',
        url: `${serverUrl}${listSecureDataElements}`
      };
      const listSecureListMapStatus = await getService(listSecureListMap);
      if (listSecureListMapStatus.status === 200) {
        const secureListMap = {
          method: 'GET',
          url: `${serverUrl}${secureListUrl}`
        };
        const secureListMapStatus = await getService(secureListMap);
        if (secureListMapStatus.status === 200) {
          const secureListData = CA_Utils.getListData(secureListMapStatus.data);
          var arrRespParts = listSecureListMapStatus.data.split(";");
          var listSecureDataElementsMap = [];
          for (var nSettingCounter = 0; nSettingCounter < arrRespParts.length; nSettingCounter++) {
            let arrSettingList = arrRespParts[nSettingCounter].split("=");
            if (arrSettingList === null || arrSettingList.length === 0)
              continue;
            if ("SECURE_DATA_ELEMENTS" === arrSettingList[0]) {
              listSecureDataElementsMap.push(arrSettingList[1].split(","));
            }
          }
          var lists = [];
          var bIsElementSecureElement = listSecureDataElementsMap.includes(this.props.ruleOrgName);
          if (!bIsElementSecureElement) {
            for (var nElementCounter = 0; nElementCounter < listSimpleLists.length; nElementCounter += 2) {
              if (listSimpleLists[nElementCounter] != this.props.passDataElement && listSimpleLists[nElementCounter] != "@UNSPECIFIED@")
                continue;
              else {
                for (var nListCounter = 0; nListCounter < listSimpleLists[nElementCounter + 1].length; nListCounter++) {
                  if (listSimpleLists[nElementCounter + 1][nListCounter][0] === "LOCAL") {
                    let obj = {};
                    obj['ruleKey'] = listSimpleLists[nElementCounter + 1][nListCounter][2];
                    obj['ruleData'] = listSimpleLists[nElementCounter + 1][nListCounter][3]
                    lists.push(obj);
                  }
                }
              }
            }
          } else {
            for (var nElementCounter = 0; nElementCounter < secureListData.length; nElementCounter += 2) {
              if (secureListData[nElementCounter] !== this.props.ruleOrgName)
                continue;
              else {
                for (var nListCounter = 0; nListCounter < secureListData[nElementCounter + 1].length; nListCounter++) {
                  if (secureListData[nElementCounter + 1][nListCounter][0] === "LOCAL") {
                    let obj = {};
                    obj['ruleKey'] = secureListData[nElementCounter + 1][nListCounter][2];
                    obj['ruleData'] = secureListData[nElementCounter + 1][nListCounter][3];
                    lists.push(obj);
                  }
                }
              }
            }
          }
          let getAllFieldData = fields;
          getAllFieldData['dataSet'] =lists[0] && lists[0].ruleKey;
          this.setState({ dataSetListItems: lists, fields: getAllFieldData });
        }
      }
      if (this.props.ruleFields) {
        this.setState({ fields: this.props.ruleFields });
      }
    }
  }

  handleInputChanges = (fieldId, e) => {
    const { fields } = this.state;
    fields[fieldId] = e.target.selectedOptions ? e.target.selectedOptions[0].value : e.target.value;
    this.setState({ fields: fields });
  }
  handleValidation = () => {
    const { fields } = this.state;
    let formIsValid = true;
    if (!fields["dataSet"]) {
      formIsValid = false;
      alert(RA_STR.ruleScreenDataSet);
      return
    }
    if (formIsValid) {
      let obj = {};
      let ruleGroup = [];
      obj['ruleKey'] = 'listScreen';
      obj['ruleExpression'] = `${this.props.passDataElement} ${this.props.passDataOperator} WITH DATASET = ${fields['dataSet']} WITH MATCHTYPE = ${fields['match']}`;
      obj['ruleData'] = fields;
      ruleGroup.push(obj)
      return ruleGroup
    } else {
      return formIsValid
    }
  }
  showListMap = () => {
    this.setState({ showList: !this.state.showList })
  }
  render() {
    const { dataSetListItems, fields, showList } = this.state;
    const groupedElements = ["ACTION", "CURR_CODE", "DATE", "LOCAL_DATE", "DAYOFMONTH", "DAYOFWEEK", "MONTH", "YEAR", "HOUROFDAY", "LOGINID_CREATION_DATETIME", "REQUEST_INITIATED_DATETIME"]
    return <table id="InDerivedListScreen">
      <tr id="incategorylist_combo" >
        <td>{RA_STR.ruleIdentityData}</td>
        <td>
          <select id="incategorylist_listOfLists" name="incategorylist_listOfLists" className="form-control" onChange={this.handleInputChanges.bind(this, 'dataSet')} value={fields['dataSet']}>
            {dataSetListItems && dataSetListItems.map((eachList) => {
              return <option value={eachList.ruleKey}>{eachList.ruleData}</option>
            })}
          </select>
        </td>
        <td>
          {this.props.passDataElement !== "USERNAME" && dataSetListItems.length ? <span className="simpleListLink_InListScreenC" onClick={this.showListMap.bind(this)}>{showList ? 'Hide List' : 'Show List'}</span> : ''}
        </td>
      </tr>
      {groupedElements.includes(this.props.passDataElement) ? <tr>
        <td><span>{RA_STR.ruleMatchType}</span></td>
        <td><b className="ml-4">{RA_STR.ruleScreenExact}</b></td>
      </tr> : <tr>
          <td>{RA_STR.ruleMatchType}</td>
          <td>
            <select id="matchtype_InDerivedListScreen" className="form-control" onChange={this.handleInputChanges.bind(this, 'match')} value={fields['match']}>
              <option id="exact" value="EXACT">{RA_STR.ruleExactMatch}</option>
              <option id="partial" value="PARTIAL">{RA_STR.rulePartialMatch}</option>
            </select>
          </td>
        </tr>}
      <br></br>
      {showList ? <tr id="divSimpleListArea_InListScreen">
        <td><b>{RA_STR.ruleScreenList}</b></td>
        <td>
          <textarea id="simpleListArea_InListScreen" name="simpleListArea" cols="1" className="form-control" onChange={this.handleInputChanges.bind(this, 'listData')} value={fields['listData']}></textarea>
          <img id="help" onclick={this.showPopUp} src="images/rfexclamation.png" width="16" height="16" className="float-right"></img></td>
      </tr> : ''}
    </table>
  }
}
export default ListScreen;
