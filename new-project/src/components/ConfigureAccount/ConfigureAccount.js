import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import CA_Utils from '../../shared/utlities/commonUtils';
import './ConfigureAccount.css';

import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, DELETE_ACC_TYPE } from '../../shared/utlities/constants';

class ConfigureAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkBoxOptions: ["Apply to all Organizations"],
      products: [
        {
          id: '1',
          name: '',
          value: ''
        }
      ],
      name: '',
      displayName: '',
      defaultAccountType: '',
      accountType: [],
      selectOptions: {},
      optionDetails: {},
      applyToAllOrg: false,
      disableCheckbox: false,
      defaultGloabalConfig: false
    }
  }

  handleRowDel(product) {
    if (this.state.products.length === 1) {
      return
    }
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      name: '',
      value: ''
    }
    this.state.products.push(product);
    this.setState(this.state.products);
  };

  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };

    var products = this.state.products.slice();
    var newProducts = products.map(function (product) {
      for (var key in product) {
        if ((key == item.name || key == item.value) && product.id === item.id) {
          product[key] = item.value;
        }
      }
      return product;
    });
    this.setState({ products: newProducts });
  };

  handledoubleRightChange = () => {
    let item = [];
    if (this.state.optionDetails.length !== 0) {
      for (let i = 0; i < Object.keys(this.state.optionDetails).length; i++) {
        item = Object.keys(this.state.optionDetails)[i];
        this.state.selectOptions[item] = this.state.optionDetails[item];
        delete this.state.optionDetails[item];
        i = i - 1;
      }
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions })
  }

  handledoubleLeftChange = () => {
    let item = [];
    if (this.state.selectOptions.length !== 0) {
      for (let i = 0; i < Object.keys(this.state.selectOptions).length; i++) {
        item = Object.keys(this.state.selectOptions)[i];
        this.state.optionDetails[item] = this.state.selectOptions[item];
        delete this.state.selectOptions[item]
        i = i - 1;
      }
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions })
  }

  handleLeftChange = (node) => {
    var options = [].slice.call(node.querySelectorAll('option'));
    var selected = options.filter(function (option) {
      return option.selected;
    });
    var selectedValues = selected.map(function (option) {
      return { "value": option.value, "id": option.id, "text": option.text };
    });
    for (let i = 0; i < selectedValues.length; i++) {
      delete this.state.optionDetails[selectedValues[i].value]
      this.state.selectOptions[selectedValues[i].value] = selectedValues[i].text;
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions });
  }

  handleRightChange = (node) => {
    var options = [].slice.call(node.querySelectorAll('option'));
    var selected = options.filter(function (option) {
      return option.selected;
    });
    var selectedValues = selected.map(function (option) {
      return { "value": option.value, "id": option.id, "text": option.text };
    });
    for (let i = 0; i < selectedValues.length; i++) {
      delete this.state.selectOptions[selectedValues[i].value]
      this.state.optionDetails[selectedValues[i].value] = selectedValues[i].text;
    }
    this.setState({ optionDetails: this.state.optionDetails, selectOptions: this.state.selectOptions });
  }

  handleChange = async (e) => {
    const acTypeName = e.target.value;
    this.setState({ defaultAccountType: acTypeName });
    if (acTypeName !== '') {
      this.getAccountTypeCofig(acTypeName);
    } else if (acTypeName === '') {
      this.setState({ name: '', displayName: '', applyToAllOrg: false, disableCheckbox: false, products: [{ id: '1', name: '', value: '' }], selectOptions: {} });
      this.getAllOrganizations();
    }
  };

  handleInputChange = (value, event) => {
    value === 'name' ? this.setState({ name: event.target.value }) : this.setState({ displayName: event.target.value });
  }

  onChangeCheckbox = (e) => {
    this.setState({ applyToAllOrg: e.target.checked });
  }

  getAccountTypeCofig = async (acTypeName) => {
    const udsAcTypes = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['udsAcTypes']}/${acTypeName}`
    };
    const acTypeConfig = await getService(udsAcTypes);
    if (acTypeConfig.status === 200) {
      const accountConfig = acTypeConfig.data.configAccountTypeDetails;
      if (accountConfig.scopeAll) {
        this.getAllOrganizations();
        this.setState({ name: accountConfig.name, displayName: accountConfig.displayName, applyToAllOrg: accountConfig.scopeAll, selectOptions: {} });
      } else {
        this.setState({ name: accountConfig.name, displayName: accountConfig.displayName, applyToAllOrg: accountConfig.scopeAll, optionDetails: CA_Utils.objToArray(accountConfig.availableGroupedResources, 'string'), selectOptions: accountConfig.selectedGroupedResources });
      }
      this.setState({ products: CA_Utils.convertCustAttr(accountConfig.customAttributes) });
    }
  }

  updateConfig = async (type) => {
    const data = {
      name: this.state.name,
      displayName: this.state.displayName,
      scopeAll: this.state.applyToAllOrg,
      customAttributes: CA_Utils.customAttributes(this.state.products),
      selectedGroupedResources: CA_Utils.objValToVal(this.state.selectOptions)
    }
    if (type === 'update') {
      const udpateConfigUrl = {
        method: this.state.defaultAccountType === '' ? 'POST' : 'PUT',
        url: `${serverUrl}${RA_API_URL['udsAcTypes']}`,
        data: data
      };
      const updateConfigServiceResponse = await getService(udpateConfigUrl);
      if (updateConfigServiceResponse && updateConfigServiceResponse.status === 200) {
        this.getConfigTypes();
        this.getAccountTypeCofig(this.state.name);
        this.setState({ defaultAccountType: this.state.name.toUpperCase() });
        this.props.activateSuccessList(true, updateConfigServiceResponse.data);
        this.props.activateErrorList(false, '');
      } else {
        this.props.activateErrorList(true, updateConfigServiceResponse.data.errorList);
        this.props.activateSuccessList(false, '');
      }
    }
    else if (type === 'delete') {
      const deleteConfirm = window.confirm(DELETE_ACC_TYPE);
      if (deleteConfirm) {
        const deleteConfigUrl = {
          method: 'DELETE',
          url: `${serverUrl}${RA_API_URL['udsAcTypes']}/${this.state.name}`
        };
        const updateConfigServiceResponse = await getService(deleteConfigUrl);
        if (updateConfigServiceResponse && updateConfigServiceResponse.status === 200) {
          this.setState({ name: '', displayName: '', defaultAccountType: '', applyToAllOrg: false, disableCheckbox: false, products: [{ id: '1', name: '', value: '' }], selectOptions: {} });
          this.getConfigTypes();
          this.getAllOrganizations();
        }
      } else {
        return;
      }
    }
  };

  componentDidMount = async () => {
    this.getConfigTypes();
    this.getAllOrganizations();
  };

  getConfigTypes = async () => {
    const typesUrl = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['udsAcTypes']}`
    };
    const AccountTypes = await getService(typesUrl);
    if (AccountTypes && AccountTypes.status === 200) {
      this.setState({ accountType: CA_Utils.objToArray(AccountTypes.data.configTypes, 'object') })
    }
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }

  getAllOrganizations = async () => {
    const OrgUrl = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const availableOrgs = await getService(OrgUrl);
    if (availableOrgs && availableOrgs.status === 200) {
      this.setState({ optionDetails: CA_Utils.objToArray(availableOrgs.data.organizations, 'string') })
    }
  };

  render() {
    const { defaultAccountType, accountType, name, displayName, products, applyToAllOrg, disableCheckbox } = this.state;
    return <div className="main">
      <h2 className="title">Configure Account Type</h2>
      <p className="desc">Add or Update Account Type.</p>
      <p className="desc"><b>Note:</b> You must refresh the system cache of product servers for the add/update/delete account type & its custom attribute changes to take effect. If organizations are assigned/un-assigned, organization cache refresh is also needed for the organization changes to take effect.</p>
      <div className="div-seperator col-md-5">
        <Select
          name={'accountType'}
          title={'accountType '}
          required={true}
          selectedOption={defaultAccountType}
          placeholder={'-Add Account Type-'}
          options={accountType}
          controlFunc={this.handleChange} />
        <SingleInput
          title={'Name'}
          inputType={'text'}
          required={true}
          name={'Name'}
          content={name}
          controlFunc={this.handleInputChange.bind(this, 'name')}
          disabled={disableCheckbox} />
        <SingleInput
          title={'Display Name'}
          inputType={'text'}
          required={true}
          name={'Display Name'}
          content={displayName}
          controlFunc={this.handleInputChange.bind(this, 'displayName')} />
        <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.products} filterText={this.state.filterText} />
      </div>
      <div className="div-seperator">
        <span className="ecc-h1">Assign to Organizations</span>
        <div className="col-md-12 form-group row no-padding">
          <div className="custom-control custom-checkbox">
            <input type="checkbox" className="custom-control-input" id="applyToAll" checked={applyToAllOrg} disabled={defaultAccountType} onChange={this.onChangeCheckbox.bind(this)} />
            <label className="custom-control-label" htmlFor="applyToAll" > Apply to all Organizations </label>
          </div>
          <span className="seperatText">OR</span>
          <div className="col-md-9">
            <SwappingSelectBox selectOptions={this.state.selectOptions}
              optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={false} rightMenuName='Selected' leftMenuName='Available' disableSwapping={applyToAllOrg || disableCheckbox} />
          </div>
        </div>
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="updateButton" type="submit" value={defaultAccountType ? "UPDATE" : "CREATE"} onClick={this.updateConfig.bind(this, 'update')}></input>
          {defaultAccountType ?
            <input className="custom-secondary-btn ml-3" id="deleteButton" type="submit" value="DELETE" onClick={this.updateConfig.bind(this, 'delete')}></input>
            : ''}
        </div>
      </div>
    </div>;
  }
}
export default ConfigureAccount;
