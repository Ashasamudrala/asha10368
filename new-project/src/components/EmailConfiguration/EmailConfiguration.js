import React from 'react';
import './EmailConfiguration.css';
class EmailConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expandInputs: true
        }
    }
    render() {
        let onProductTableUpdate = this.props.onProductTableUpdate;
        let rowDel = this.props.onRowDel;
        let completeProducts = this.props.products;
        let priorityStatus = this.props.getPriorityStatus;
        let product = this.props.products && this.props.products.map(function (product) {
            return (<DynamicAddInputRow onProductTableUpdate={onProductTableUpdate} product={product} onDelEvent={rowDel.bind(this)} data={completeProducts} incPriority={priorityStatus} key={product.id} />)
        });
        return (
            <div className="row email-telephone-config">
                {this.state.expandInputs ?
                    <div className="col-sm-12">
                        <table className="dynamic-table table">
                            <thead>
                                <tr>
                                    <th>Priority</th>
                                    <th>Type</th>
                                    <th>Display Name</th>
                                    <th>Mandatory</th>
                                    <th onClick={() => this.props.onRowAdd(this.props.tableType)}>
                                        <img alt="" src="images/plus.jpg" className="pull-right plus-icon" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {product}
                            </tbody>
                        </table></div> : ''}
            </div>
        );
    }
}
class DynamicAddInputRow extends React.Component {
    constructor(props) {
        super(props);
    }
    onDelEvent() {
        this.props.onDelEvent(this.props.product);
    }
    render() {
        return (
            <tr className={this.props.product.disableRow ? "disableStatus" : "eachRow"}>
                <td className="del-cell">
                    {this.props.product.disableRow ?
                        <span>
                            <img className="icon_up_arrow" src="images/icon_up_arrow.png" alt="icon_up_arrow" />
                            <img className="arrow-down" src="images/icon_down_arrow.png" alt="icon_down_arrow" />
                        </span>
                        :
                        <span>
                            <img className="icon_up_arrow" src="images/icon_up_arrow.png" alt="icon_up_arrow" onClick={() => this.props.incPriority('inc', this.props.product)} />
                            <img className="arrow-down" src="images/icon_down_arrow.png" alt="icon_down_arrow" onClick={() => this.props.incPriority('dec', this.props.product)} />
                        </span>
                    }
                </td>
                <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
                    type: "contactName",
                    value: this.props.product.contactName,
                    id: this.props.product.id,
                    disabled: this.props.product.disableRow,
                    contactType: this.props.product.contactType,
                    maxlength: '64'
                }} />
                <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
                    type: "displayName",
                    value: this.props.product.displayName,
                    id: this.props.product.id,
                    contactType: this.props.product.contactType,
                    maxlength: '32'
                }} />
                <EditableCheckboxCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
                    type: "mandatory",
                    value: this.props.product.mandatory,
                    id: this.props.product.id,
                    disabled: this.props.product.disableRow,
                    contactType: this.props.product.contactType
                }} />
                <td className="del-cell">
                    {this.props.product.disableRow ?
                        <img alt="" src="images/cross_disabled.gif" className="disable-del-btn"></img> :
                        <img alt="" src="images/cross_enabled.gif" onClick={this.onDelEvent.bind(this)} className="disable-del-btn"></img>
                    }
                </td>
            </tr>
        );
    }
}
class EditableCell extends React.Component {
    constructor(props) {
        super();
        this.state = {
            inputvalue: props.cellData.value,
        }
        this.handlechange = this.handlechange.bind(this)
    }
    handlechange(e) {
        this.setState({ inputvalue: e.target.value })
        this.props.onProductTableUpdate(e)
    }
    render() {
        return (
            <td>
                <input className="form-control"
                    type='text'
                    maxlength={this.props.cellData.maxlength}
                    data-contacttype={this.props.cellData.contactType}
                    name={this.props.cellData.type}
                    disabled={this.props.cellData.disabled}
                    id={this.props.cellData.id}
                    value={this.state.inputvalue}
                    onChange={this.handlechange} />
            </td>
        );
    }
}
class EditableCheckboxCell extends React.Component {
    constructor(props) {
        super();
        this.state = {
            inputvalue: props.cellData.value,
        }
        this.handlechange = this.handlechange.bind(this)
    }
    handlechange(e) {
        let checkStatus = "0";
        if (e.target.checked) {
            checkStatus = "1";
            e.target.value = "1";
        } else {
            e.target.value = "0";
        }
        this.setState({ inputvalue: checkStatus });
        this.props.onProductTableUpdate(e)
    }
    render() {
        return (
            <td>
                <input
                    type='checkbox'
                    name={this.props.cellData.type}
                    data-contacttype={this.props.cellData.contactType}
                    id={this.props.cellData.id}
                    value={this.state.inputvalue}
                    checked={this.props.cellData.value === "1" ? true : false}
                    onChange={this.handlechange}
                    disabled={this.props.cellData.disabled || false}
                />
            </td>
        );
    }
}
export default EmailConfiguration;
