import React, { Component, Fragment } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import './createlistorg.css';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_API_URL, SYSTEM, LOADING_TEXT, serverUrl } from '../../shared/utlities/constants';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import { RA_STR } from '../../shared/utlities/messages';
import Loader from '../../shared/utlities/loader';

const defaultlist = {
    listName: '',
    orgName: '',
    permission: '',
    elementName: '@UNSPECIFIED@',
    extListStatus: '0',
    preferredStatus: '0',
    priorityMode: 'O',
    listScopeHandler: '0',
    listScope: '',
    listUsage: '0',
    listTask: '0',
    orgSharingInfoList: [],
    listType: 'SIMPLELIST',
    hierarchicalListName: '',
    selectionForListOfOrgs: ''
}
class CreateListorganization extends Component {
    constructor(props) {
        super(props);
        this.tempSharingList = [];
        this.state = {
            elementNamesList: [],
            ruleList: [],
            rulesetList: [],
            Privileges: [{ key: 'Read Only', content: 'Read Only' }],
            shareableList: [],
            hierarchicalLists: [],
            showSelectTask: false,
            successMessage: false,
            loaded: false,
            errorMessage: [],
            usageList: [
                { key: "0", content: "General Purpose" },
                { key: "1", content: "Blacklist" },
                { key: "2", content: "Whitelist" }
            ],
            organizationCreateList: true,
            createListOrg: {
                listName: '',
                orgName: '',
                permission: '',
                elementName: '@UNSPECIFIED@',
                extListStatus: '0',
                preferredStatus: '0',
                priorityMode: 'O',
                listScopeHandler: '0',
                listScope: '',
                listUsage: '0',
                listTask: '1',
                orgSharingInfoList: [],
                listType: 'SIMPLELIST',
                hierarchicalListName: "",
                priorityMode: 'O',
                selectionForListOfOrgs: ''
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.addSharingPrivilege = this.addSharingPrivilege.bind(this);
        this.createOrganizationList = this.createOrganizationList.bind(this);
    }
    componentWillUnmount() {
        if (typeof this.props.activateErrorList === 'function') {
            this.props.activateErrorList(false, '');
            this.props.activateSuccessList(false, '');
        }
    }
    removeFromSharingInfo(index) {
        let { createListOrg, shareableList } = this.state;
        createListOrg.orgSharingInfoList.splice(index, 1);
        if (this.tempSharingList) {
            shareableList.push(this.tempSharingList[index]);
            this.tempSharingList.splice(index, 1);
        }
        this.setState({ createListOrg, shareableList });
    }
    setHotlistDisabled(createListOrg) {
        if (this.orgName === SYSTEM) {
            return true;
        } else if (createListOrg.listUsage === '1' && createListOrg.elementName === 'DEVICEID') {
            return false;
        } else {
            return true;
        }
    }
    addSharingPrivilege() {
        let { createListOrg, shareableList } = this.state;
        if (createListOrg.selectionForListOfOrgs !== '') {
            var data = {
                shareType: "ORG",
                sharedOrgName: createListOrg.selectionForListOfOrgs,
                permission: "READONLY"
            }
            if (data.sharedOrgName === '1KFAMILY (ORG Family)') {
                data.shareType = "ORG Family";
            }
            createListOrg.orgSharingInfoList.push(data);
            let getObj = shareableList.filter((obj) => {
                return obj.content == createListOrg.selectionForListOfOrgs;
            })[0];
            if (getObj) {
                let index = shareableList.indexOf(getObj);
                shareableList.splice(index, 1);
                createListOrg.selectionForListOfOrgs = shareableList[0].content;
                this.tempSharingList.push(getObj);
            }
            this.setState({ createListOrg, shareableList });
        }
    }
    handleChange(e) {
        let { createListOrg, shareableList } = this.state;
        createListOrg[e.target.name] = e.target.value;
        if (e.target.name == 'listScopeHandler') {
            createListOrg.orgSharingInfoList = [];
            this.tempSharingList.forEach(shareObj => {
                shareableList.push(shareObj);
            });
            this.tempSharingList = [];
        } else if (e.target.name == 'hierarchicalListName') {
            if (createListOrg.hierarchicalListName && createListOrg.hierarchicalListName != '') {
                let getParentObj = this.state.hierarchicalLists.filter(obj => obj.listName == createListOrg.hierarchicalListName)[0];
                if (getParentObj) {
                    createListOrg.elementName = getParentObj.elementName;
                    createListOrg.listUsage = getParentObj.listUsage.toString();
                    createListOrg.extListStatus = getParentObj.extlistEnabled.toString();
                    createListOrg.preferredStatus = getParentObj.preferred.toString();
                }
            } else {
                createListOrg.elementName = '';
                createListOrg.listUsage = '0';
                createListOrg.extListStatus = '0';
                createListOrg.preferredStatus = '0';
            }
        }
        this.setState({ createListOrg, shareableList })
    }
    async getElementsList() {
        const orgUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.getOrgRules
        const getElementsList = await getService({ method: 'GET', url: orgUrl });
        if (getElementsList && getElementsList.status === 200) {
            let elementNamesList;
            if (getElementsList.data.elementNames) {
                elementNamesList = getElementsList.data.elementNames.map((element) => {
                    return {
                        'key': element,
                        'content': element
                    }
                })
            }
            this.setState({ elementNamesList })
        }
    }
    async getRulelistOptions() {
        const listUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.GetListOfRules
        const getRuleList = await getService({ method: 'GET', url: listUrl });
        if (getRuleList && getRuleList.status === 200) {
            let ruleList = [];
            if (getRuleList.data) {
                ruleList = getRuleList.data.map((data) => {
                    data.key = data.listName;
                    data.content = data.listName;
                    return data;
                })
            }
            this.setState({ ruleList })
        }
    }
    async getRuleSetList() {
        const listUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.getRulesetWithConfigNames;
        const getRuleList = await getService({ method: 'GET', url: listUrl });
        if (getRuleList && getRuleList.status === 200) {
            let rulesetList = [];
            if (getRuleList.data.configNamesList) {
                rulesetList = getRuleList.data.configNamesList.map((data) => {
                    return {
                        key: data,
                        content: data
                    };
                })
            }
            this.setState({ rulesetList })
        }
    }
    testForIllegalChars(name) {
        let notContain = true;
        if (name && name.length > 32) {
            notContain = false;
        } else if (name) {
            name = name.split('');
            let format = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;
            name.forEach(str => {
                if (format.test(str) || str.trim() === '') {
                    notContain = false;
                }
            });
        }
        return notContain;
    }
    async createOrganizationList() {
        const listUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.getOrgRules;
        let { createListOrg } = this.state;
        if (createListOrg.listName !== '' && this.testForIllegalChars(createListOrg.listName)) {
            createListOrg.selectLinkMode = createListOrg.priorityMode;
            const createList = await getService({ method: 'POST', url: listUrl, data: createListOrg });
            let updateStateObj = {};
            if (createList && createList.status === 400) {
                this.props.activateSuccessList(false, '');
                this.props.activateErrorList(true, createList.data.errorList);
                window.scrollTo(0, 0);
            } else if (createList.status === 200) {
                let setTodefault = { ...defaultlist }
                updateStateObj = {
                    createListOrg: JSON.parse(JSON.stringify(setTodefault)),
                };
                this.props.activateSuccessList(true, createList.data);
                this.props.activateErrorList(false, '');
                updateStateObj.createListOrg.listTask = this.listTask;
                window.scrollTo(0, 0);
            }
            this.setState(updateStateObj);
            if (this.orgName !== SYSTEM) {
                this.getShareableOrg();
            }
        } else {
            let alertText = '';
            if (createListOrg.listName === '') {
                alertText = RA_STR.ValidListName;
            } else if (createListOrg.listName && createListOrg.listName.length > 32) {
                alertText = RA_STR.maxSizeofListname;
            } else {
                alertText = RA_STR.IllegalChars;
            }
            alert(alertText);
        }
    }
    async getShareableOrg() {
        const sharelistUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.sharableOrg;
        const getShareableList = await getService({ method: 'GET', url: sharelistUrl });
        if (getShareableList && getShareableList.status === 200) {
            let shareableList = [];
            if (getShareableList.data.siblingOrgs) {
                shareableList = getShareableList.data.siblingOrgs.map((data, index) => {
                    return {
                        key: data,
                        content: data
                    };
                })
            }
            let { createListOrg } = this.state;
            if (shareableList.length > 0) {
                createListOrg.selectionForListOfOrgs = shareableList[0].key;
            }
            this.setState({ shareableList, createListOrg });
        }
    }
    async getParentOrganizations() {
        const sharelistUrl = serverUrl + RA_API_URL.getOrganizationUrl + this.orgName + RA_API_URL.parentOrg;
        const getParentOrgList = await getService({ method: 'GET', url: sharelistUrl });
        if (getParentOrgList && getParentOrgList.status === 200) {
            let hierarchicalLists = [];
            if (getParentOrgList.data && getParentOrgList.data.hierarchicalLists) {
                var showSelectTask = getParentOrgList.data.hierarchyOrg && getParentOrgList.data.rulesetExecMode != 0;
                hierarchicalLists = getParentOrgList.data.hierarchicalLists.map((data, index) => {
                    data.key = data.listName;
                    data.content = data.listName;
                    return data;
                })
            }
            let { createListOrg } = this.state;
            if (showSelectTask) {
                createListOrg.listTask = '1';
                this.listTask = '1';
            } else {
                createListOrg.listTask = '0';
                this.listTask = '0';
            }
            this.setState({ createListOrg, showSelectTask, loaded: true, hierarchicalLists, organizationCreateList: this.orgName !== SYSTEM })
        }
    }
    componentWillMount() {
        this.orgName = SYSTEM;
        let orgname = new URLSearchParams(window.location.search).get('orgname');
        if (orgname) {
            this.orgName = orgname;
        }
        this.getElementsList();
        // this.getRulelistOptions();
        this.getRuleSetList();
        this.getShareableOrg();
        this.getParentOrganizations();
    }
    render() {
        let { createListOrg, showSelectTask, loaded, errorMessage, organizationCreateList } = this.state;
        return <Fragment>
            {loaded ?
                <div className="main createListOrg create-list">
                    {this.state.successMessage ?
                        <div className="error-parent">
                            <div className="edl_success" id="ssErrorMessagesTable">
                                <div className="error-list">List created</div>
                            </div>
                        </div>
                        : ''
                    }
                    {errorMessage.length > 0 && <ErrorList validationDataList={errorMessage} />}
                    <h2 className="title">Create List</h2>
                    <p className="desc">Create a data list that can be shared with its peer organizations. This list can then be used in rule creation. While creating lists for child organizations, you can either create a list for the child organization or use the parent organization's list. {organizationCreateList ? '' : 'SYSTEM'}</p>
                    <div className="col-sm-6">
                        {showSelectTask && organizationCreateList && < div className="form-group row">
                            <label className="col-sm-4 col-form-label">Select Task</label>
                            <div className="col-sm-8 form-inline">
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="self"
                                        name="listTask"
                                        checked={createListOrg.listTask === '0'}
                                        onChange={this.handleChange}
                                        value='0' />
                                    <label className="custom-control-label" htmlFor="self">Create List</label>
                                </div>
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="parent"
                                        name="listTask"
                                        value='1'
                                        checked={createListOrg.listTask === '1'}
                                        onChange={this.handleChange} />
                                    <label className="custom-control-label" htmlFor="parent">Use Parent List</label>
                                </div>
                            </div>
                        </div>}
                        <div className="row">
                            <label className="col-sm-4 col-form-label">List Type</label>
                            <div className="col-sm-8 mt-1">
                                ListData
                    </div>
                        </div>
                        <div className="mt-3">
                            <SingleInput
                                title={'List Name'}
                                inputType={'text'}
                                name={'listName'}
                                content={createListOrg.listName}
                                controlFunc={this.handleChange} />
                            {createListOrg.listTask === "1" && organizationCreateList && <Select
                                name={"hierarchicalListName"}
                                title={"Link to Parent Organization's List"}
                                options={this.state.hierarchicalLists}
                                placeholder={'--select--'}
                                selectedOption={createListOrg.hierarchicalListName}
                                controlFunc={this.handleChange} />}
                        </div>
                        {createListOrg.listTask === "1" && organizationCreateList && <div className="form-group row">
                            <label className="col-sm-4 col-form-label">Linking Mode</label>
                            <div className="col-sm-8 form-inline">
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="linkingself"
                                        name="priorityMode"
                                        checked={createListOrg.priorityMode === 'O'}
                                        onChange={this.handleChange}
                                        value='O' />
                                    <label className="custom-control-label" htmlFor="linkingself">Override</label>
                                </div>
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="linkingparent"
                                        name="priorityMode"
                                        value='A'
                                        checked={createListOrg.priorityMode === 'A'}
                                        onChange={this.handleChange} />
                                    <label className="custom-control-label" htmlFor="linkingparent">Append</label>
                                </div>
                            </div>
                        </div>}
                    </div>
                    <div className="row mb-2 mt-4 form-group" >
                        <label className="col-sm-2">Scope</label>
                        {organizationCreateList && <div className="col-sm-8 ml-2 scope">
                            <div className="form-inline">
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="listscopeself"
                                        name="listScopeHandler"
                                        checked={createListOrg.listScopeHandler === '0'}
                                        onChange={this.handleChange}
                                        value='0' />
                                    <label className="custom-control-label" htmlFor="listscopeself">ORG</label>
                                </div>
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="listscopeselfparent"
                                        name="listScopeHandler"
                                        value='1'
                                        checked={createListOrg.listScopeHandler === '1'}
                                        onChange={this.handleChange} />
                                    <label className="custom-control-label" htmlFor="listscopeselfparent">Ruleset</label>
                                </div>
                                <Select
                                    name={'listScope'}
                                    title={''}
                                    options={this.state.rulesetList}
                                    placeholder={''}
                                    controlFunc={this.handleChange}
                                    disabled={createListOrg.listScopeHandler === '0'} />

                            </div>
                        </div>}
                        {!organizationCreateList && <div className="col-sm-6  select-scope dynamic-form-select">
                            <div className="ruleset ml-2">
                                <div> Ruleset :  </div>
                                <div className="ruleset-select ml-1">
                                    <Select
                                        name={'listScope'}
                                        title={''}
                                        options={this.state.rulesetList}
                                        placeholder={''}
                                        selectedOption={createListOrg.listScope}
                                        controlFunc={this.handleChange} />
                                </div>
                            </div>
                        </div>
                        }
                    </div>
                    <div className="col-sm-6">
                        <Select
                            name={'elementName'}
                            title={'Element Name'}
                            options={this.state.elementNamesList}
                            placeholder={'(Unspecified)'}
                            selectedOption={createListOrg.elementName}
                            controlFunc={this.handleChange}
                            disabled={createListOrg.listTask === "1"} />
                        <Select
                            name={'listUsage'}
                            title={'List Usage'}
                            options={this.state.usageList}
                            placeholder={''}
                            disabled={createListOrg.elementName === '@UNSPECIFIED@' && !organizationCreateList || createListOrg.elementName === '' && !organizationCreateList || createListOrg.listTask === "1"}
                            selectedOption={createListOrg.listUsage}
                            controlFunc={this.handleChange}
                        />
                        {createListOrg.listTask === "0" && <div className="dynamic-form-input form-group row">
                            <label className="col-sm-4 col-form-label">Elements with Hotlist option</label>
                            <div className="col-sm-8 mt-1"> DEVICEID </div>
                        </div>}
                        <div className="form-group row">
                            <label className="col-sm-4 col-form-label">Hot List</label>
                            <div className="col-sm-8 form-inline">
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="hotself"
                                        name="extListStatus"
                                        checked={createListOrg.extListStatus === "1"}
                                        onChange={this.handleChange}
                                        value='1'
                                        disabled={this.setHotlistDisabled(createListOrg)} />
                                    <label className="custom-control-label" htmlFor="hotself">Yes</label>
                                </div>
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="hotparent"
                                        name="extListStatus"
                                        value='0'
                                        checked={createListOrg.extListStatus === "0"}
                                        onChange={this.handleChange}
                                        disabled={this.setHotlistDisabled(createListOrg)} />
                                    <label className="custom-control-label" htmlFor="hotparent">No</label>
                                </div>
                            </div>
                        </div>
                        {organizationCreateList && <div className="form-group row">
                            <label className="col-sm-4 col-form-label">Preferred List</label>
                            <div className="col-sm-8 form-inline">
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="prefself"
                                        name="preferredStatus"
                                        checked={createListOrg.preferredStatus === "1"}
                                        onChange={this.handleChange}
                                        value='1'
                                        disabled={this.setHotlistDisabled(createListOrg)} />
                                    <label className="custom-control-label" htmlFor="prefself">Yes</label>
                                </div>
                                <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                    <input type="radio" className="custom-control-input "
                                        id="prefparent"
                                        name="preferredStatus"
                                        value='0'
                                        checked={createListOrg.preferredStatus === "0"}
                                        onChange={this.handleChange}
                                        disabled={this.setHotlistDisabled(createListOrg)} />
                                    <label className="custom-control-label" htmlFor="prefparent">No</label>
                                </div>
                            </div>
                        </div>}
                    </div>
                    {!showSelectTask && organizationCreateList && <div>
                        <div className="row sharingPriviliges">
                            <label className="col-sm-2 ">Sharing & Privileges</label>
                            <div className="col-sm-10">
                                <div className="row form-group">
                                    <div className="col-sm-3 dynamic-form-select">
                                        <Select
                                            name={'selectionForListOfOrgs'}
                                            title={''}
                                            options={this.state.shareableList}
                                            placeholder={''}
                                            selectedOption={this.state.createListOrg.selectionForListOfOrgs}
                                            controlFunc={this.handleChange}
                                            disabled={createListOrg.listScopeHandler === "1"} />
                                    </div>
                                    <div className="col-sm-3 dynamic-form-select">
                                        <Select
                                            name={'sharingPermissions'}
                                            title={''}
                                            options={this.state.Privileges}
                                            placeholder={''}
                                            controlFunc={this.handleChange}
                                            disabled={createListOrg.listScopeHandler === "1"} />
                                    </div>
                                    <div className="col-sm-2">
                                        <input className="custom-secondary-btn" id="RESET" type="button" value="ADD" disabled={createListOrg.listScopeHandler === "1"} onClick={this.addSharingPrivilege} ></input>
                                    </div>
                                </div>

                                {createListOrg.orgSharingInfoList.length > 0 && <table className="edl_table">
                                    <thead>
                                        <tr>
                                            <th>Organization</th>
                                            <th>Permissions</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody id="sharedOrgTable">
                                        {createListOrg.orgSharingInfoList.map((org, index) =>
                                            <tr key={index}>
                                                <td>{org.sharedOrgName}</td>
                                                <td>{org.permission}</td>
                                                <td><a id="shared_1KFMLYORG0932" title="Remove Sharing" onClick={() => this.removeFromSharingInfo(index)}>x</a> </td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                                }
                            </div>
                        </div>
                        <div className="sharedOrgDiv_Text grey-txt  mt-2 mb-3" >
                            <i>List is not shared.</i>
                        </div>
                    </div>}
                    <div className="form-group">
                        <input className="secondary-btn" id="searchButton" type="submit" value="CREATE" onClick={this.createOrganizationList}></input>
                    </div>
                </div>
                : <div className="main">  <Loader show='true' />{LOADING_TEXT}</div>
            }
        </Fragment>
    }
}

export default CreateListorganization;