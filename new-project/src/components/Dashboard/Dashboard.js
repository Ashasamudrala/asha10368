import React, { Component } from 'react';
import './Dashboard.css';
import DynamicNavbar from '../DynamicNavbar/DynamicNavbar';
import Footer from '../Footer';
import { connect } from 'react-redux';
import { RA_STR } from '../../shared/utlities/messages';
import { Route, Link, Switch } from 'react-router-dom';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openLogout: false,
    }
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }
  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }
  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }
  setWrapperRef(node) {
    this.wrapperRef = node;
  }
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target) && this.state.openLogout) {
      this.setState({ openLogout: false });
    }
  }
  openLogoutDropdown = () => {
    this.setState({ openLogout: !this.state.openLogout });
  }
  Logout = () => {
    const getProfiledataUsername = JSON.parse(localStorage.getItem('profiledata'));
    localStorage.removeItem('profiledata');
    if (getProfiledataUsername && getProfiledataUsername.userProfile.userName === "MASTERADMIN") {
      this.props.history.push('/masteradminconsole');
    } else {
      this.props.history.push('/bamlogin');
    }
  }
  activateMyProfile = () => {
    this.props.history.push('/myProfile');
  }
  render() {
    const match = this.props.match.url;
    const getProfiledata = JSON.parse(localStorage.getItem('profiledata'));
    const userName = JSON.parse(localStorage.getItem('userName'));
    const profiledata = getProfiledata ? getProfiledata.userProfile : '';
    return <div className="wrapper">
      <nav className="navbar navbar-expand-lg">
        <a className="navbar-brand"><img src="images/ca-logo-new.png" height="40" width="130"></img></a>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item  ecc-h1">
              CA Administration Console
            </li>
          </ul>
          <div className="user-logout" onClick={this.openLogoutDropdown} ref={this.setWrapperRef}>
            <span className="navbar-text tertiary-link">
              {userName}
            </span>
            {this.state.openLogout ? <img src="images/icon_up_arrow.png"></img> : <img src="images/icon_down_arrow.png"></img>}
            {this.state.openLogout ?
              <div className="user-dropdown-menu">
                <div className="text-left">{RA_STR.logintime}</div>
                <div className="text-left">{profiledata ? profiledata.lastLoginTime : 'Last Login Time'}</div>
                <div className="text-center logout-buttons">
                  <button className="button profile-button" onClick={this.activateMyProfile}>My Profile</button>
                </div>
                <div className="text-center logout-buttons">
                  <button className="button sign-out" onClick={this.Logout}>Sign out</button>
                </div>
              </div> : ''}
          </div>
        </div>
      </nav>
      <div className="second-nav">
        <nav className="second-nav-items">
          <Link to='/users'><span className={`${match === '/users' ? 'second-nav-text active' : 'second-nav-text'}`}>Users and Administrators</span></Link>
          <Link to='/org'><span className={`${match === '/org' ? 'second-nav-text active' : 'second-nav-text'}`}>Organizations</span></Link>
          {(profiledata.role !== 'OA') && (profiledata.role !== 'UA') && (profiledata.role !== 'AM') && (profiledata.role !== 'FA') && (profiledata.role !== 'CSR')?
          <Link to='/server-config'><span className={`${match === '/server-config' ? 'second-nav-text active' : 'second-nav-text'}`}>Services and Server Configurations</span></Link>:
          ''}
          <Link to='/reports'><span className={`${match === '/reports' ? 'second-nav-text active' : 'second-nav-text'}`}>Reports</span></Link>
          {profiledata.role !== 'MA' &&  profiledata.role !== 'AM'? <Link to='/case-manage'><span className={`${match === '/case-manage' ? 'second-nav-text active' : 'second-nav-text'}`}>Case Management</span></Link> : ''}
        </nav>
      </div>
      <Switch>
        <Route path={`/users`} component={DynamicNavbar} />
        <Route path={`/org`} component={DynamicNavbar} />
        <Route path={`/server-config`} component={DynamicNavbar} />
        <Route path={`/myProfile`} component={DynamicNavbar} />
        <Route path={`/reports`} component={DynamicNavbar} />
        <Route path={`/case-manage`} component={DynamicNavbar} />
      </Switch>
      <Footer/>
    </div>;
  }
}

const mapStateToProps = state => ({
  posts: state.posts.items
})
export default connect(mapStateToProps)(Dashboard);

