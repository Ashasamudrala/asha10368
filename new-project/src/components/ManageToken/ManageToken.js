import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
import { serverUrl, RA_API_URL, RA_STR_SELECT, RA_STR_CHECKBOX, RA_API_STATUS } from '../../shared/utlities/constants';
import AutoSuggest from '../../shared/components/Autosuggest/Autosuggest';
import { getService } from '../../shared/utlities/RestAPI';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { RA_STR } from '../../shared/utlities/messages';
import CA_Utils from '../../shared/utlities/commonUtils';
import './ManageToken.css';

class ManageToken extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgOptions: [],
      siteData: [],
      sites: '',
      orgDetails: false,
      defaultOrganizationForUserCreation: '',
      tokenEnable: false,
      name: '',
      password: ''
    };

    this.baseState = {
      defaultOrganizationForUserCreation: '',
      sites: '',
      orgDetails: true,
      tokenEnable: false,
      name: '',
      password: ''
    };
  }

  componentDidMount() {
    this.getOrgDetails();
  }

  getOrgDetails = async () => {
    let orgOptions = [];
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['searchactions']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions.status === 200) {
      orgOptions = Object.values(getOrgOptions.data.organizations);
      orgOptions.unshift(RA_STR_SELECT);
      this.setState({
        orgOptions: orgOptions,
        originalOrganizations: CA_Utils.objToArray(getOrgOptions.data.organizations, 'object')
      });
    }
  };

  getSelectedOrgDetails = (getSelectedOrgDetail, type) => {
    if (type === 'click' && getSelectedOrgDetail !== null) {
      this.getSite(getSelectedOrgDetail);

      const orgId = this.state.originalOrganizations.find(element => {
        if (element.content === getSelectedOrgDetail) return element.key;
      });

      if (orgId !== null) {
        this.setState({
          defaultOrganizationForUserCreation: orgId.key
        });
      }
      localStorage.setItem('defaultOrganizationForUserCreation', getSelectedOrgDetail);
      return getSelectedOrgDetail;
    }
  };

  getSite = async getSelectedOrgDetail => {
    const siteList = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['manageToken']}/${getSelectedOrgDetail}`
    };
    const siteOptions = await getService(siteList);
    if (siteOptions.status === 200) {
      const result = CA_Utils.objToArray(siteOptions.data, 'object');
      this.setState({
        siteData: result,
        sites: result[0].key,
        orgDetails: true
      });
    }
    localStorage.setItem('sites', this.state.sites);
  };

  handleChange = e => {
    if (e.target.name === 'Site') {
      this.setState({
        sites: e.target.value
      });
    }
    localStorage.setItem('sites', e.target.value);
  };

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onChangeCheckbox = e => {
    if (e.target.type === RA_STR_CHECKBOX) {
      this.setState({ tokenEnable: e.target.checked });
      localStorage.setItem('tokenEnable', e.target.checked);
    } else {
      this.setState({ tokenEnable: e.target.value });
    }
    localStorage.setItem('tokenEnable', e.target.value);
  };

  onSubmit = async () => {
    const tokenStatus = {
      method: 'PUT',
      url: `${serverUrl}${RA_API_URL['manageToken']}`,
      data: {
        enabled: this.state.tokenEnable,
        orgName: this.state.defaultOrganizationForUserCreation,
        password: this.state.password ? this.state.password : null,
        siteID: this.state.sites,
        userName: this.state.name ? this.state.name : null
      }
    };
    const tokenPost = await getService(tokenStatus);
    if (tokenPost && tokenPost.status === RA_API_STATUS['200']) {
      this.props.activateSuccessList(true, tokenPost.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
    } else {
      this.props.activateErrorList(true, tokenPost.data.errorList);
      this.props.activateSuccessList(false, '');
      this.showErrorList();
    }
  };

  onReset = () => {
    this.setState(this.baseState);
  };

  showErrorList = () => {
    window.scrollTo(0, 0);
  };

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
    this.setState = {
      name: '',
      password: '',
      sites: '',
      tokenEnable: false,
      getSelectedOrgDetail: []
    };
    // localStorage.removeItem('name');
    // localStorage.removeItem('password');
    localStorage.removeItem('sites');
    localStorage.removeItem('tokenEnable');
    localStorage.removeItem('defaultOrganizationForUserCreation');
  }

  render() {
    const { sites, siteData, tokenEnable, name, password } = this.state;
    return (
      <div className='main manage-token-container'>
        <h2 className='title'>{RA_STR.tokenTitle}</h2>
        <p className='desc'>{RA_STR.tokenTitle}</p>
        <div className='col-sm-6'>
          <div className='form-group row'>
            <label className='col-sm-4 col-form-label'>{RA_STR.org}</label>
            <div className='col-sm-8'>
              <AutoSuggest
                name={'org'}
                orgOptions={this.state.orgOptions}
                getSelectedOrgDetails={this.getSelectedOrgDetails}
                enableAutosuggest={true}
                defaultOrganizationPlaceholder={this.state.defaultOrganizationForUserCreation}
              />
            </div>
          </div>
        </div>
        {this.state.orgDetails ? (
          <div className='col-sm-6'>
            <Select
              name={'Site'}
              title={RA_STR.site}
              required={false}
              options={siteData ? siteData : ['']}
              controlFunc={this.handleChange}
              selectedOption={sites}
            />
          </div>
        ) : (
          <div></div>
        )}
        {this.state.orgDetails ? (
          <div className='col-sm-6'>
            <div className='form-group row'>
              <label className='col-sm-4 col-form-label'>{RA_STR.enable}</label>
              <div className='col-sm-8 checkbox'>
                <div className='custom-control custom-checkbox'>
                  <input
                    type='checkbox'
                    className='custom-control-input'
                    id='tokenEnable'
                    name='enableToken'
                    checked={tokenEnable}
                    onChange={this.onChangeCheckbox}
                  />
                  <label className='custom-control-label' htmlFor='tokenEnable'></label>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}
        {this.state.orgDetails ? (
          <div className='col-sm-6'>
            <SingleInput
              title={RA_STR.adminName}
              inputType={'text'}
              name={'name'}
              required={true}
              controlFunc={this.handleInputChange}
              content={name}
            />
          </div>
        ) : (
          <div></div>
        )}
        {this.state.orgDetails ? (
          <div className='col-sm-6'>
            <SingleInput
              title={RA_STR.password}
              inputType={'password'}
              name={'password'}
              required={false}
              controlFunc={this.handleInputChange}
              content={password}
            />
          </div>
        ) : (
          <div></div>
        )}

        {this.state.orgDetails ? (
          <div className='col-sm-6'>
            <div className='form-group form-submit-button row mt-5'>
              <input className='custom-secondary-btn ml-3' type='reset' value='RESET' onClick={this.onReset}></input>
              <input className='secondary-btn' type='submit' value='SUBMIT' onClick={this.onSubmit}></input>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    );
  }
}

export default ManageToken;
