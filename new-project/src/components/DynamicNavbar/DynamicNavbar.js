import React, { Component } from 'react';
import './DynamicNavbar.css';
import { Route, Link, Switch, Redirect } from 'react-router-dom';
import SideBar from '../SideBar/SideBar';
import { dynamicNavbar } from '../../shared/utlities/privilegeRoutes';
class DynamicNavbar extends Component {
  constructor(props) {
    super(props);
    this.count = 0;
    this.state = {
      openLeftNav: true,
      displaydiv: false
    }
  }
  componentDidMount() {
    const profiledata = JSON.parse(localStorage.getItem('profiledata'));
    const loginTokenExist = profiledata ? profiledata.token : '';
    if (!loginTokenExist) {
      if (profiledata && profiledata.userProfile.userName === "MASTERADMIN") {
        this.props.history.push('/masteradminconsole');
      } else {
        this.props.history.push('/bamlogin');
      }
    }
  }
  render() {
    const match = this.props.match;
    const results = dynamicNavbar.filter(function (entry) {
      return entry.route === match.url
     });
     
    const profiledata = JSON.parse(localStorage.getItem('profiledata'));
    const userRoleInfo = profiledata ? profiledata.userProfile : '';
    var index = window.location.pathname.lastIndexOf("/");
    if (index !== -1) {
      var newStr = window.location.pathname.substring(0, index);
    }
    return <div className="dynamicNavbar-parent">
      <div className={(results[0] && results[0].hideSecondNav) || (results[0] && results[0].route === '/users' && userRoleInfo.role !== 'MA') || (results[0] && results[0].route === '/reports' && userRoleInfo.role === 'FA') || (results.length === 0) ? 'third-nav hide-div' : 'third-nav'}>
        {results[0]  && results[0].subRoutes.map((eachRoute, key) => {
          this.count = this.count + 1;
          return <Link to={`${match.url}${eachRoute.route}`} key={key}>
            <span className={`${match.url}${eachRoute.route}` === newStr ? 'third-nav-text third-nav-active' : 'third-nav-text'}>{eachRoute.description}</span></Link>
        })}
      </div>
      {this.state.openLeftNav ?
        <Switch>
          <Redirect path='/users' exact to='users/manageUsers' />
          <Redirect path='/org' exact to='org/searchOrg' />
          <Redirect path='/server-config' exact to='server-config/RA' />
          {profiledata.userProfile.role === 'FA' ? <Redirect path='/reports' exact to='reports/AC' /> : <Redirect path='/reports' exact to='reports/RA' />}
          <Redirect path='/case-manage' exact to='case-manage/manage-inbound' />
          <Route path={`/users/manageUsers`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/users/manageRoles`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/org`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/myProfile`} component={SideBar} />
          <Route path={`/server-config/AC`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/server-config/RA`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/reports/RA`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/reports/AC`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
          <Route path={`/case-manage`} render={(props) => <SideBar {...props} selectedRouteInfo={results[0]} />} />
        </Switch>
        : ''}
    </div>
  }
}
export default DynamicNavbar;
