import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { serverUrl, RA_API_RISKANALYTICS_CONNECTIVITY } from '../../shared/utlities/constants';
import './RiskAnalyticsConn.css';
import Select from '../../shared/components/Select/Select';
import { getService } from '../../shared/utlities/RestAPI';
import { RA_STR } from '../../shared/utlities/messages';
class RiskAnalyticsConn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actionId: 0,
      hostName_SvrMgmt: '',
      portNum_SvrMgmt: '',
      transport_SvrMgmt: '',
      trustStore_SvrMgmt: '',
      p12_SvrMgmt: '',
      passwordP12_SvrMgmt: '',
      hostName_q_SvrMgmt: '',
      portNum_q_SvrMgmt: '',
      transport_q_SvrMgmt: '',
      trustStore_q_SvrMgmt: '',
      p12_q_SvrMgmt: '',
      passwordP12_q_SvrMgmt: '',
      hostName: '',
      portNum: '',
      transport: '',
      trustStore: '',
      p12: '',
      passwordP12: '',
      hostName_q: '',
      backupHostName_q: '',
      portNum_q: '',
      transport_q: '',
      trustStore_q: '',
      p12_q: '',
      passwordP12_q: '',
      roleOptions: [{ 'key': 'TCP', 'content': 'TCP' }, { 'key': 'SSL', 'content': 'SSL' }],
      riskAnalyticsConnData: '',

    };
  }

  componentDidMount() {
    this.getRiskAnalyticsData();
  }

  handleInputChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  }

  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }


  getRiskAnalyticsData = async () => {
    const getRiskAnalytics = {
      method: 'GET',
      url: `${serverUrl}${RA_API_RISKANALYTICS_CONNECTIVITY['riskAnalyticsConnectivity']}`,
    }

    const getRiskAnalyticsData = await getService(getRiskAnalytics);
    if (getRiskAnalyticsData && getRiskAnalyticsData.status === 200) {
      const riskAnalyticsConnData = getRiskAnalyticsData.data;
      this.setState({
        hostName_SvrMgmt: riskAnalyticsConnData.hostName_SvrMgmt,
        portNum_SvrMgmt: riskAnalyticsConnData.portNum_SvrMgmt,
        transport_SvrMgmt: riskAnalyticsConnData.transport_SvrMgmt,
        hostName_q_SvrMgmt: riskAnalyticsConnData.hostName_q_SvrMgmt,
        portNum_q_SvrMgmt: riskAnalyticsConnData.portNum_q_SvrMgmt,
        transport_q_SvrMgmt: riskAnalyticsConnData.transport_q_SvrMgmt,
        hostName: riskAnalyticsConnData.hostName,
        portNum: riskAnalyticsConnData.portNum,
        transport: riskAnalyticsConnData.transport,
        hostName_q: riskAnalyticsConnData.hostName_q,
        portNum_q: riskAnalyticsConnData.portNum_q,
        transport_q: riskAnalyticsConnData.transport_q
      });
      this.initialData = this.state;
    }
  }
  onSaveClick = async (channelId) => {
    const data = {};
    switch (channelId) {
      case 1:
        {
          const { hostName, portNum, transport, passwordP12, trustStore, p12 } = this.state;
          data.actionId = channelId;
          data.p12 = Object.values(p12);
          data.presentP12 = false;
          data.hostName = hostName;
          data.portNum = portNum;
          data.transport = transport;
          data.trustStore = Object.values(trustStore);
          data.passwordP12 = passwordP12;
          data.presentTrustStore_q = false;
        }; break;
      case 2:
        {
          const { hostName_q, backupHostName_q, portNum_q, transport_q, passwordP12_q, trustStore_q, p12_q } = this.state;
          data.actionId = channelId;
          data.p12_q = Object.values(p12_q);
          data.presentP12_q = false;
          data.hostName_q = hostName_q;
          data.backupHostName_q = backupHostName_q;
          data.portNum_q = portNum_q;
          data.transport_q = transport_q;
          data.trustStore_q = Object.values(trustStore_q);
          data.passwordP12_q = passwordP12_q;
        }; break;
      case 3:
        {
          const { hostName_SvrMgmt, portNum_SvrMgmt, transport_SvrMgmt, passwordP12_SvrMgmt, trustStore_SvrMgmt, p12_SvrMgmt } = this.state;
          data.actionId = channelId;
          data.trustStore_SvrMgmt = Object.values(trustStore_SvrMgmt);
          data.presentTrustStore_SvrMgmt = false;
          data.p12_SvrMgmt = Object.values(p12_SvrMgmt);
          data.presentP12_SvrMgmt = false;
          data.hostName_SvrMgmt = hostName_SvrMgmt;
          data.portNum_SvrMgmt = portNum_SvrMgmt;
          data.transport_SvrMgmt = transport_SvrMgmt;
          data.passwordP12_SvrMgmt = passwordP12_SvrMgmt;
          data.presentTrustStore_q_SvrMgmt = false;
        }; break;
      case 4:
        {
          const { hostName_q_SvrMgmt, portNum_q_SvrMgmt, transport_q_SvrMgmt, passwordP12_q_SvrMgmt, trustStore_q_SvrMgmt, p12_q_SvrMgmt } = this.state;
          data.actionId = channelId;
          data.p12_q_SvrMgmt = Object.values(p12_q_SvrMgmt);
          data.presentP12_q_SvrMgmt = false;
          data.hostName_q_SvrMgmt = hostName_q_SvrMgmt;
          data.portNum_q_SvrMgmt = portNum_q_SvrMgmt;
          data.transport_q_SvrMgmt = transport_q_SvrMgmt;
          data.trustStore_q_SvrMgmt = Object.values(trustStore_q_SvrMgmt);
          data.passwordP12_q_SvrMgmt = passwordP12_q_SvrMgmt;
          data.presentTrustStore = false;
        }; break;
      default: break;
    }
    const postRiskAnalyticsData = {
      method: 'POST',
      url: `${serverUrl}${RA_API_RISKANALYTICS_CONNECTIVITY['riskAnalyticsConnectivity']}`,
      data
    };
    const postRiskAnalyticsDataUpdate = await getService(postRiskAnalyticsData);
    if (postRiskAnalyticsDataUpdate && postRiskAnalyticsDataUpdate.status === 200) {
      this.setState({ trustStore_SvrMgmt: [], p12_SvrMgmt: [], trustStore_q_SvrMgmt: [], trustStore: [], p12: [], trustStore_q: [], p12_q: [] });
      document.querySelectorAll('#input-field').forEach(file => file.value = "");
      this.props.activateSuccessList(true, postRiskAnalyticsDataUpdate.data);
      this.props.activateErrorList(false, '');
    } else if (postRiskAnalyticsDataUpdate && postRiskAnalyticsDataUpdate.status === 400) {
      this.setState({ trustStore_SvrMgmt: [], p12_SvrMgmt: [], trustStore_q_SvrMgmt: [], trustStore: [], p12: [], trustStore_q: [], p12_q: [] });
      document.querySelectorAll('#input-field').forEach(file => file.value = "");
      this.props.activateSuccessList(false, '');
      this.props.activateErrorList(true, postRiskAnalyticsDataUpdate.data.errorList);
      this.setState(
        this.initialData
      );
    }
  }
  handleChange = (e) => {
    let reader = new FileReader();
    const { target: { files, name } = {} } = e || {};
    let file = files[0];
    if (file) {
      reader.onload = () => {
        const intArray = new Int8Array(reader.result);
        this.setState({
          [name]: intArray
        })
      }
      reader.readAsArrayBuffer(file)
    }
    else {
      this.setState({
        [name]: []
      })
    }
  }
  render() {
    const { hostName_SvrMgmt, portNum_SvrMgmt, transport_SvrMgmt, passwordP12_SvrMgmt,
      hostName_q_SvrMgmt, portNum_q_SvrMgmt, transport_q_SvrMgmt, passwordP12_q_SvrMgmt,
      hostName, portNum, transport, passwordP12,
      hostName_q, backupHostName_q, portNum_q, transport_q, passwordP12_q,
      roleOptions } = this.state;
    return <div className="main riskAnalytics">
      <h2 className="title">{RA_STR.riskAnalyticsTSMC}</h2>
      <p className="desc">{RA_STR.riskAnalyticsTSMCDesc}</p>
      <div className="col-sm-6">
        <SingleInput
          title="Server"
          inputType="text"
          name="hostName_SvrMgmt"
          controlFunc={this.handleInputChange}
          content={hostName_SvrMgmt} />
        <SingleInput
          title="Server Management Port"
          inputType="text"
          name="portNum_SvrMgmt"
          controlFunc={this.handleInputChange}
          content={portNum_SvrMgmt} />
        <Select
          name="transport_SvrMgmt"
          title="Transport"
          required
          options={roleOptions}
          selectedOption={transport_SvrMgmt}
          controlFunc={this.handleInputChange} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.serverCARC}</label>
          <div className="col-md-8">
            <input
              type="file" name="trustStore_SvrMgmt" onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.clientCertificatePKCS}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="p12_SvrMgmt"
              onChange={this.handleChange}
              id='input-field'
            />
          </div></div>
        <SingleInput
          title="Client PKCS#12 Password"
          inputType="password"
          name="passwordP12_SvrMgmt"
          content={passwordP12_SvrMgmt}
          controlFunc={this.handleInputChange} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE" onClick={() => this.onSaveClick(3)}></input>
        </div>
      </div>
      <h2 className="title">{RA_STR.caseManagementSMC}</h2>
      <p className="desc">{RA_STR.caseManagementSMCDesc}</p>
      <div className="col-sm-6">
        <SingleInput
          title="Server"
          inputType="text"
          name="hostName_q_SvrMgmt"
          controlFunc={this.handleInputChange}
          content={hostName_q_SvrMgmt} />
        <SingleInput
          title="Server Management Port"
          inputType="text"
          name="portNum_q_SvrMgmt"
          controlFunc={this.handleInputChange}
          content={portNum_q_SvrMgmt} />
        <Select
          name="transport_q_SvrMgmt"
          title="Transport"
          required
          options={roleOptions}
          selectedOption={transport_q_SvrMgmt}
          controlFunc={this.handleInputChange} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.serverCARC}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="trustStore_q_SvrMgmt"
              onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.clientCertificatePKCS}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="p12_q_SvrMgmt"
              onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <SingleInput
          title="Client PKCS#12 Password"
          inputType="password"
          name='passwordP12_q_SvrMgmt'
          content={passwordP12_q_SvrMgmt}
          controlFunc={this.handleInputChange} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE" onClick={() => this.onSaveClick(4)}></input>
        </div>
      </div>
      <h2 className="title">{RA_STR.riskAnalyticsAC}</h2>
      <p className="desc">{RA_STR.riskAnalyticsACDesc}</p>
      <div className="col-sm-6">
        <SingleInput
          title="Server"
          inputType="text"
          name="hostName"
          controlFunc={this.handleInputChange}
          content={hostName} />
        <SingleInput
          title="Server Management Port"
          inputType="text"
          name="portNum"
          controlFunc={this.handleInputChange}
          content={portNum} />
        <Select
          name="transport"
          title="Transport"
          required
          options={roleOptions}
          selectedOption={transport}
          controlFunc={this.handleInputChange} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.serverCARC}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="trustStore"
              onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.clientCertificatePKCS}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="p12"
              onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <SingleInput
          title="Client PKCS#12 Password"
          inputType="password"
          name='passwordP12'
          content={passwordP12}
          controlFunc={this.handleInputChange} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE" onClick={() => this.onSaveClick(1)}></input>
        </div>
      </div>
      <h2 className="title">{RA_STR.caseManagementSC}</h2>
      <p className="desc">{RA_STR.caseManagementSCDesc}</p>
      <div className="col-sm-6">
        <SingleInput
          title="Server"
          inputType="text"
          name="hostName_q"
          controlFunc={this.handleInputChange}
          content={hostName_q} />
        <SingleInput
          title="Host"
          inputType="text"
          name="backupHostName_q"
          content={backupHostName_q}
          controlFunc={this.handleInputChange} />
        <SingleInput
          title="Backup Host"
          inputType="text"
          name="portNum_q"
          controlFunc={this.handleInputChange}
          content={portNum_q} />
        <Select
          name="transport_q"
          title="Transport"
          required
          options={roleOptions}
          selectedOption={transport_q}
          controlFunc={this.handleInputChange} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.serverCARC}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="trustStore_q"
              onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">{RA_STR.clientCertificatePKCS}</label>
          <div className="col-md-8">
            <input
              type="file"
              name="p12_q"
              onChange={this.handleChange} id='input-field'
            />
          </div></div>
        <SingleInput
          title="Client PKCS#12 Password"
          inputType="password"
          name='passwordP12_q'
          content={passwordP12_q}
          controlFunc={this.handleInputChange} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE" onClick={() => this.onSaveClick(2)}></input>
        </div>
      </div>
    </div>
  }
}

export default RiskAnalyticsConn;
