import React, { Component } from 'react';
import { httpCall } from '../../shared/utlities/RestAPI';
import DateRange from '../../shared/components/DateRange/DateRange';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import { RA_STR } from '../../shared/utlities/messages';
import DynamicTable from '../../shared/components/dynamicTable/dynamicTable';
import { saveAs } from 'file-saver';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import { getService } from '../../shared/utlities/RestAPI';
import ReactDOM from 'react-dom';
import './MyActivityReport.scss';

class MyActivityReport extends Component {
  constructor() {
    super();
    this.state = {
      pageNo: 0,
      pageSize: 10,
      displayReportList: [],
      activePageNum: 1,
      totalCount: 0,
      modalColor: false,
      decryptionNeeded: true,
      showClipBoardMessage: false,
      selectedEvent: '',
      selectedEventDetails: '',
      modifiedEventDeatils: '',
      getDateItems: [],
      eventTypes: [],
      roleOptions: [],
      columns: [
        { title: RA_STR.date, field: 'date' },
        { title: RA_STR.administratorId, field: 'administratorID' },
        { title: RA_STR.adminOrg, field: 'adminPreferredORG' },
        { title: RA_STR.transactionId, field: 'transactionID' },
        { title: RA_STR.eventTypeTable, field: 'eventType' },
        { title: RA_STR.status, field: 'status' },
        { title: RA_STR.reason, field: 'reason' },
        { title: RA_STR.userId, field: 'userID' },
        { title: RA_STR.targetOrg, field: 'targetOrg' },
        { title: RA_STR.details, field: 'details', render: (rowData) => <div>{rowData.details ? <span onClick={this.showInfo.bind(this, rowData)}><img src="images/info.png"></img></span> : ''}</div> }
      ],
      showTable: false
    }
  }
  downloadCSV = (response) => {
    if (response && response.data) {
      const blob = new Blob([response.data], { type: 'application/octet-stream' });
      saveAs(blob, 'My_Activity_report.csv');
    }
  }
  getActivePage = (getPageNum) => {
    let pageNumCount = getPageNum - 1;
    this.setState({
      pageNo: pageNumCount, activePageNum: getPageNum
    }, () => {
      this.displayReport();
    })
  }
  displayReport = async () => {
    let getDateItems = '';
    if (this.state.getDateItems.length === 0) {
      getDateItems = this.refs.daterange.getDates();
    } else {
      getDateItems = this.state.getDateItems;
    }
    const { pageNo, pageSize, decryptionNeeded } = this.state;
    let profiledata = JSON.parse(localStorage.getItem('profiledata'));
    let timeZone = profiledata ? profiledata.userProfile.timeZone : '';
    const data = {
      fromDate: getDateItems[0].startDate,
      adminPreferredTimezone: timeZone,
      toDate: getDateItems[0].endDate,
      decryptionNeeded: decryptionNeeded,
      pageNo: pageNo,
      pageSize: pageSize,
      eventTypes: this.state.eventTypes ? this.state.eventTypes : []
    }
    const getDisplayReport = await httpCall('POST', RA_API_URL['getMyActivityReport'], data);
    const currentDom = ReactDOM.findDOMNode(this);
    if (getDisplayReport.status === 200) {
      this.props.activateErrorList(false, '');
      const getEventTypes = {
        method: 'GET',
        url: `${serverUrl}${RA_API_URL['eventTypes']}`
      };
      const getEventTypestatus = await getService(getEventTypes);
      if (getEventTypestatus.status === 200) {
        var field = '<div class="event-list" style="display:none"><select multiple class="event-select custom-select">'
        Object.keys(getEventTypestatus.data.eventTypes).forEach((eachType) => {
          field += '<optgroup label=' + eachType + '>';
          getEventTypestatus.data.eventTypes[eachType].forEach((eachEvent) => {
            field += '<option>' + eachEvent + '</option>'
          })
          field += '</optgroup>'
        })
        field += '</select><div class="event-footer"><input type="submit" class="custom-secondary-btn event-type-submit"/></div></div>'
      }
      this.setState({ displayReportList: getDisplayReport.data.reportList, totalCount: getDisplayReport.data.numberOfRecords, showTable: true, getDateItems: getDateItems }, () => {
        if (currentDom) {
          let getTableNode = currentDom.querySelectorAll('th.MuiTableCell-root');
          if (getTableNode && getTableNode[4]) {
            getTableNode[4].innerHTML = 'Event Type <img src="images/dropdown.png" class="clickDownArrow" />' + field;
            currentDom.querySelector(".clickDownArrow").addEventListener("click", this.hideEventList);
            currentDom.querySelector(".event-type-submit").addEventListener("click", this.submitEventTypes);
          }
        }
      });
    } else if (currentDom) {
      this.props.activateErrorList(true, getDisplayReport.data.errorList);
    }
  }
  hideEventList = () => {
    const currentDom = ReactDOM.findDOMNode(this);
    if (currentDom.querySelector(".event-list").style.display === "none") {
      currentDom.querySelector(".event-list").style.display = "block";
    } else {
      currentDom.querySelector(".event-list").style.display = "none";
    }
  }
  submitEventTypes = () => {
    var selectedElements = [];
    var getselected = document.querySelectorAll(".event-select option");
    for (var i = 0; i < getselected.length; i++) {
      if (getselected[i].selected) {
        selectedElements.push(getselected[i].text);
      }
    }
    this.setState({ eventTypes: selectedElements, pageNo: 0, activePageNum: 1 }, () => {
      this.displayReport();
    })
  }
  exportData = async () => {
    let getDateItems = '';
    if (this.state.getDateItems.length === 0) {
      getDateItems = this.refs.daterange.getDates();
    } else {
      getDateItems = this.state.getDateItems;
    }
    const { decryptionNeeded } = this.state;
    let profiledata = JSON.parse(localStorage.getItem('profiledata'));
    let timeZone = profiledata ? profiledata.userProfile.timeZone : '';
    const data = {
      decryptionNeeded: decryptionNeeded,
      fromDate: getDateItems[0].startDate,
      adminPreferredTimezone: timeZone,
      toDate: getDateItems[0].endDate
    }
    const getExportData = await httpCall('POST', RA_API_URL['exportMyActivityReportInCSV'], data);
    if (getExportData.status === 200) {
      this.downloadCSV(getExportData);
    }
  }
  reset = () => {
    window.location.reload();
  }
  onchange = (e) => {
    let getValue = e.target;
    if (getValue.checked) {
      this.setState({ decryptionNeeded: true });
    } else {
      this.setState({ decryptionNeeded: false });
    }
  }
  togglePage = () => {
    this.setState({ showTable: false });
  }
  showInfo = (getSelectedInfo) => {
    this.setState({ modalColor: true });
    let modal = document.getElementById("myModal");
    modal.style.display = "block";
    this.setState({ selectedEvent: getSelectedInfo, selectedEventDetails: getSelectedInfo.details["Changes for DEFAULT"], modifiedEventDeatils: getSelectedInfo.details["Changes for DEFAULT"] });
    window.scrollTo(0, 0);
  }
  handleClose = () => {
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
    this.setState({ modalColor: false });
  }
  handleClipBoard = () => {
    const { selectedEvent } = this.state;
    const copyAudit = document.querySelector('.auditInfoDetail').outerHTML;
    const copyText = document.querySelector(`${selectedEvent.details && selectedEvent.details["Changes for DEFAULT"] ? '.report-table-scrolling' : '.report-scrolling-data'}`).outerHTML;
    const el = document.createElement('textarea');
    el.value = copyAudit + copyText;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.setState({ showClipBoardMessage: true });
    setTimeout(() => {
      this.setState({ showClipBoardMessage: false });
    }, 2000);
  }
  selectedChangeType = (e) => {
    const { selectedEventDetails } = this.state;
    let filteredValues = '';
    filteredValues = selectedEventDetails.filter((eachValue) => {
      return eachValue.changeType === e.target.value
    })
    this.setState({ modifiedEventDeatils: e.target.value === 'default' ? selectedEventDetails : filteredValues });
  }
  render() {
    const { displayReportList, columns, pageSize, totalCount, activePageNum, showTable, getDateItems, selectedEvent, modifiedEventDeatils, showClipBoardMessage } = this.state;
    return <div className="main mt-2 myActivity">
      <div className={`${this.state.modalColor ? 'dialog-mask' : ''}`} ></div>
      <h2 className="title">{RA_STR.activityReport}</h2>
      {!showTable ?
        <div><p className="desc">{RA_STR.reportMessage}</p>
          <hr />
          <div className="row">
            <div className="col-sm-3">
              <label className="col-sm-8 col-form-label mt-3">{RA_STR.timePeriod}</label>
            </div>
            <DateRange ref="daterange"></DateRange>
          </div>
          <div className="row mt-5">
            <div className="col-sm-3">
              <Checkbox
                type={'checkbox'}
                options={[RA_STR.DecryptInfo]}
                controlFunc={this.onchange}
                selectedOptions={[RA_STR.DecryptInfo]} />
            </div>
            <div className="col-sm-9 no-padding">
              <div className="form-group row ReportButtons">
                <input className="secondary-btn" id="RESET" type="submit" value="RESET" onClick={this.reset}></input>
                <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
                <input className="secondary-btn" id="searchButton" type="submit" value="DISPLAY REPORT" onClick={this.displayReport}></input>
              </div>
            </div>
          </div ></div> :
        <div>
          <div className="col-md-12 row">
            <div className="col-md-9 no-padding">
              <p>{RA_STR.activityInnerBody}</p>
            </div>
            <div className="float-right col-md-3">
              <input className="secondary-btn" id="EXPORT" type="submit" value="EXPORT" onClick={this.exportData}></input>
              <input className="secondary-btn ml-3" id="searchButton" type="submit" value="NEW REPORT" onClick={this.togglePage}></input>
            </div>
          </div>
          <div className="col-md-8">
            <div className="form-group form-inline">
              <label className="col-sm-4 col-form-label">{RA_STR.noOfRecords}</label>
              <div className="col-sm-8">
                {totalCount}
              </div>
            </div>
            <div className="form-group form-inline">
              <label className="col-sm-4 col-form-label">From:{getDateItems[0].startDate}</label>
              <div className="col-sm-8">
                {`To:${getDateItems[0].endDate}`}
              </div>
            </div>
          </div>
        </div>
      }
      {showTable ?
        <div className="tabele-buttons setAdminActivity">
          <DynamicTable columns={columns} data={displayReportList} enablePagination={true} pageSize={pageSize} totalCount={totalCount} activePage={this.getActivePage} activePageNumNew={activePageNum} /></div> : ''}
      <div id="myModal" className="modal">
        <div className="modal-content">
          <div className="dialog-header" className="successheader">
            <div className="dialog-title"><span className="label">{RA_STR.reportModalTitle}</span></div>
            <div className="dialog-close" className="close" onClick={this.handleClose}> <span className="close">&times;</span></div>
          </div>
          <div className="modal-body">
            <div className="col-md-12 row">
              <table>
                <tbody className="auditInfoDetail">
                  <tr>
                    <td class="infoDetailLabel"><h6>{RA_STR.tnxId}</h6>{selectedEvent.transactionID}</td>
                    <td class="infoDetailLabel"><h6>{RA_STR.eventType}</h6>{selectedEvent.eventType}</td>
                  </tr>
                  <tr>
                    <td class="infoDetailLabel"><h6>{RA_STR.targetOrg}</h6>{selectedEvent.targetOrg}</td>
                    <td class="infoDetailLabel"><h6>{RA_STR.adminActivity}</h6>{selectedEvent.administratorID}</td>
                  </tr>
                  <tr>
                    <td class="infoDetailLabel"><h6>{RA_STR.dateTime}</h6>{selectedEvent.date}</td>
                  </tr>
                </tbody>
              </table>
              {selectedEvent.details && selectedEvent.details["Changes for DEFAULT"] ? <div className="col-md-8 no-padding">
                <div class="form-group">
                  <label for="formGroupExampleInput" className="apply-bold">{RA_STR.changeType}</label>
                  <div>
                    <select className="form-control" onChange={this.selectedChangeType.bind(this)}>
                      <option value="default" selected="selected">{RA_STR.reportAll}</option>
                      <option value="Priority">{RA_STR.reportPriority}</option>
                      <option value="Risk Score">{RA_STR.riskScore}</option>
                      <option value="Default Score">{RA_STR.defaultScore}</option>
                      <option value="Enabled">{RA_STR.enableDisable}</option>
                    </select>
                  </div>
                </div>
              </div> : ''}
              <table className={`col-md-12 no-padding  ${selectedEvent.details && selectedEvent.details["Changes for DEFAULT"] ? 'report-table-scrolling' : 'report-scrolling-data'}`}>
                {selectedEvent.details && selectedEvent.details["Changes for DEFAULT"] ? <div class="jsonTableHeading">Ruleset:{Object.keys(selectedEvent.details)}</div> : ''}
                <tbody>
                  {selectedEvent.details && selectedEvent.details["Changes for DEFAULT"] ? <tr><th class="infoValue">{RA_STR.reportNewValue}</th><th class="infoValue">{RA_STR.changeType}</th><th class="infoValue">{RA_STR.reportRuleName}</th><th class="infoValue">{RA_STR.prevValue}</th></tr> : ''}
                  {selectedEvent.details && !selectedEvent.details["Changes for DEFAULT"] && Object.keys(selectedEvent.details).map((eachEvent) => {
                    return [<tr><td><h6>{eachEvent}</h6></td></tr>, <tr class="form-group infoValue"><td>{selectedEvent.details[eachEvent]}</td></tr>]
                  })}
                  {modifiedEventDeatils && modifiedEventDeatils.map((eachSelected) => {
                    return <tr><td>{eachSelected.newValue}</td><td>{eachSelected.changeType}</td><td>{eachSelected.ruleName}</td><td>{eachSelected.previousValue}</td></tr>
                  })}
                </tbody>
              </table>
              <div className="form-group form-submit-button">
                <input className="custom-secondary-btn" type="submit" value="OK" onClick={this.handleClose}></input>
                <input className="secondary-btn" type="submit" value="COPY TO CLIPBOARD" onClick={this.handleClipBoard}></input>
                {showClipBoardMessage ? <span className="tooltiptext">{RA_STR.copyToClipBoard}</span> : ''}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }
}
export default MyActivityReport;
