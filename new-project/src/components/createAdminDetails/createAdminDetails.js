import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL, RA_STR_SELECT } from '../../shared/utlities/constants';
import CreateAdminSub from '../createAdminOrgDetails/createAdminOrgDetails';
import './createAdminDetails.css';
import _ from 'underscore';

class createAdminDetails extends Component {
  constructor(props) {
    super(props);
    this.dynamicInputs = React.createRef();
    this.state = {
      orgOptions: [],
      nextPage: true,
      errorMsg: '',
      successMsg: false,
      createAdminFields: {},
      contactType: [],
      emailIds: [],
      phoneNumbers: [],
      phoneList: '',
      propAttributes: [],
      customAttributes: [
        {
          id: 1,
          name: '',
          value: ''
        },
      ],
      userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null
    }
  }

  handleChange = (e) => {
    const createAdminFields = this.state.createAdminFields;
    var feildValue;
    feildValue = e.target.value;
    createAdminFields[e.target.name] = feildValue;
    if (e.target.name === 'orgName') {
      this.getConfiguredEmails(e.target.value)
    }
    this.setState({
      createAdminFields

    });
  }
  emailChange = (val, e) => {
    const rowList = val;
    const { name, value } = e.target;
    this.setState({
      emailIds: [],
      phoneNumbers: []
    })
    if (name === 'email') {
      rowList[name] = value;
      let splicedaccountType = [...this.state.emailIds]
      splicedaccountType.push(rowList)
      this.setState({
        emailIds: _.uniq(splicedaccountType)
      });
    }
    else if (name === 'phone') {
      rowList[name] = value;
      let splicedaccountType = [...this.state.phoneNumbers]
      splicedaccountType.push(rowList)
      this.setState({
        phoneNumbers: _.uniq(splicedaccountType)
      });
    }
  }
  handleRowDel(product) {
    if (this.state.customAttributes.length === 1) {
      return
    }
    var index = this.state.customAttributes.indexOf(product);
    this.state.customAttributes.splice(index, 1);
    this.setState(this.state.customAttributes);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      name: '',
      value: ''
    }
    this.state.customAttributes.push(product);
    this.setState({
      customAttributes: this.state.customAttributes
    });
  }

  showErrorList = () => {
    window.scrollTo(0, 0);
  }

  getOrg = async() => {
    const preferedOrg = this.state.userData.userProfile;
    const checkOrg = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}`
    };
    const getOrgOptions = await getService(checkOrg);
    if (getOrgOptions && getOrgOptions.status === 200) {
      this.getConfiguredEmails(preferedOrg.preferredOrganization);
      let { createAdminFields } = this.state;
      if (getOrgOptions.data.organizations) {
        let getFirstOrg = Object.keys(getOrgOptions.data.organizations)[0];
        if (getFirstOrg) {
          createAdminFields.orgName = this.state.userData.userProfile.preferredOrganization;
        }
      }
      this.setState({
        orgOptions: getOrgOptions.data.organizations, createAdminFields
      });
    }
    localStorage.removeItem('createAdmin');
  }
  componentDidMount = () => {
    this.getOrg();
  }
  getConfiguredEmails = async (val) => {
    this.setState({
      contactType: []
    })
    const getEmails = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['getOrgUrl']}/${val}`
    };
    const getEmailsStatus = await getService(getEmails);
    if (getEmailsStatus && getEmailsStatus.status === 200) {
      this.setState({
        contactType: getEmailsStatus.data.org.contactType
      })
    }
  }

  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var adminAttributes = this.state.customAttributes.slice();
    var newadminAttributes = adminAttributes.map(function (attribute) {
      for (var key in attribute) {
        if ((key === item.name || key === item.value) && attribute.id == item.id) {
          attribute[key] = item.value;
        }
      }
      return attribute;
    });
    this.setState({ customAttributes: newadminAttributes });
  };
  onBack = (type, msg) => {
    this.getOrg();
    this.setState({
      nextPage: !this.state.nextPage,
      createAdminFields: (type == 'new' ? {} : JSON.parse(localStorage.getItem('createAdmin'))),
      successMsg: (type == 'new'),
      createAdminFields: this.state.createAdminFields,
      contactType: (type == 'new' ? [] : JSON.parse(localStorage.getItem('createAdmin')).contactType)
    });
    if (msg !== '' && type == 'new') {
      this.getOrg();
      this.props.activateSuccessList(true, msg);
      this.props.activateErrorList(false, '');
    }
  }
  componentWillUnmount() {
    this.props.activateErrorList(false, '');
    this.props.activateSuccessList(false, '');
  }
  handleSubmit = async () => {
    let custParams = [];
    this.state.customAttributes.map(function (el) {
      if (el.name === '' && el.value === '') {
        custParams = []
      }
      else {
        custParams.push(el);
      }
      return el.name;
    });
    this.setState({
      propAttributes: custParams,
      contactType: [
        this.state.emailIds,
        this.state.phoneNumbers
      ]
    })
    const params = {
      ...this.state.createAdminFields,
      customAttributes: custParams,
      phoneNumbers: this.state.phoneNumbers,
      emailIds: this.state.emailIds
    }
    const localObj = {
      ...params,
      contactType: this.state.contactType
    }
    const createAdminOne = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['createAdminStepOneUrl']}`,
      data: params
    };
    const createAdminStatus = await getService(createAdminOne);
    if (createAdminStatus && createAdminStatus.status === 400) {
      this.props.activateErrorList(true, createAdminStatus.data.errorList);
      this.props.activateSuccessList(false, '');
      this.getOrg();
      this.showErrorList();
    }
    else if (createAdminStatus && createAdminStatus.status === 200) {
      this.setState({
        nextPage: !this.state.nextPage,
      });
      this.props.activateSuccessList(true, createAdminStatus.data);
      this.props.activateErrorList(false, '');
      this.showErrorList();
      localStorage.setItem('createAdmin', JSON.stringify(localObj));
    }
    else {
      this.setState({
        errorMsg: ''
      })
    }
  }
  render() {
    const fields = this.state.createAdminFields;
    const { contactType } = this.state;
    return (
      <div>
        {
          (this.state.nextPage)
            ? <div className="main">
              <h2 className="title">Create Administrator</h2>
              <p className="desc">Enter the details for the administrator that you want to create.</p>
              <span className="ecc-h1">Administrator Details</span>
              <div className="col-sm-5 create">
                <SingleInput
                  title={'Username'}
                  inputType={'text'}
                  required={true}
                  content={fields.userName ? fields.userName : ""}
                  name={'userName'}
                  controlFunc={this.handleChange} />
                <div className="form-group dynamic-form-select">
                  <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Organization *</label>
                    <div className="col-sm-8">
                      <select
                        name='orgName'
                        onChange={this.handleChange}
                        value={fields.orgName ? fields.orgName : ''}
                        className="form-select form-control">
                        <option value={RA_STR_SELECT}>{RA_STR_SELECT}</option>
                        {Object.keys(this.state.orgOptions).map(key =>
                          <option value={key} key={key}>{this.state.orgOptions[key]}</option>
                        )}
                      </select>
                    </div>
                  </div>
                </div>
                <SingleInput
                  title={'First Name'}
                  required={true}
                  inputType={'text'}
                  name={'firstName'}
                  content={fields.firstName ? fields.firstName : ""}
                  controlFunc={this.handleChange} />
                <SingleInput
                  title={'Middle Name'}
                  inputType={'text'}
                  name={'middleName'}
                  content={fields.middleName ? fields.middleName : ''}
                  controlFunc={this.handleChange} />
                <SingleInput
                  title={'Last Name'}
                  required={true}
                  inputType={'text'}
                  name={'lastName'}
                  content={fields.lastName ? fields.lastName : ''}
                  controlFunc={this.handleChange} />
              </div>

              <div className="div-seperator">
                <span className="ecc-h1">Email Address(es)</span>
                <div className="col-sm-5 create">
                  <SingleInput
                    title={'Email'}
                    required={true}
                    inputType={'text'}
                    name={'email'}
                    content={fields.email ? fields.email : ''}
                    controlFunc={this.handleChange} />
                </div>
                {contactType.length > 0 ?
                  contactType.map((email, i) => {
                    if (email.contactType === "E-MAIL") {
                      return <div className="col-sm-5 create" key={i}>
                        <SingleInput
                          title={email.displayName}
                          required={contactType.mandatory === 1 ? true : false}
                          inputType={'text'}
                          name={'email'}
                          content={email.email ? email.email : ''}
                          controlFunc={this.emailChange.bind(this, email)} />
                      </div>
                    }
                  })
                  : ''
                }
              </div>
              <div className="div-seperator">
                <span className="ecc-h1">Telephone Number(s)</span>
                <div className="col-sm-5 create">
                  <SingleInput
                    title={'Phone Number'}
                    inputType={'text'}
                    required={true}
                    name={'phone'}
                    content={fields.phone ? fields.phone : ''}
                    controlFunc={this.handleChange} />
                </div>
                {contactType.length > 0 ?
                  contactType.map((phone, i) => {
                    if (phone.contactType === "TELEPHONE") {
                      return <div className="col-sm-5 create" key={i}>
                        <SingleInput
                          title={phone.displayName}
                          required={this.state.contactType.mandatory === 1 ? true : false}
                          inputType={'text'}
                          name={'phone'}
                          content={phone.phone ? phone.phone : ''}
                          controlFunc={this.emailChange.bind(this, phone)} />
                      </div>
                    }
                  })
                  : ''
                }
              </div>
              <div className="col-sm-5">
                <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)}
                  onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)}
                  products={fields.customAttributes ? fields.customAttributes : this.state.customAttributes} />
              </div>
              <div className="form-group form-submit-button">
                <input className="secondary-btn" id="createButton" type="submit" value="Next" onClick={this.handleSubmit}></input>
              </div>
            </div>
            :
            <CreateAdminSub onBack={this.onBack}
              organizationName={this.state.createAdminFields.orgName}
              userName={this.state.createAdminFields.userName}
              createAdminFields={this.state.createAdminFields}
              customAttributes={this.state.propAttributes}
              emailIds={this.state.emailIds}
              phoneNumbers={this.state.phoneNumbers}
            />
        }
      </div>
    );
  }
}

export default createAdminDetails;
