import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import {
    serverUrl, RA_API_URL, RA_STR_BASIC_AUTH_PATH, RA_STR_CHECKBOX, RA_STR_BASIC_AUTH_NEXT,
    RA_STR_RADIO, RA_API_STATUS, RA_STR_ALLOW_SPEC_CHAR, RA_STR_MASTER_AUTH
} from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import { orgData } from '../../shared/utlities/renderer';
import ReactDOM from 'react-dom';

class BasicAuthenticationPolicy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            validPeriod: true,
            allowMultiByte: '',
            allowAll: '',
            checkAllowAll: '',
            orgDetails: '',
            orgParam: '',
            checkPwd: '',
            authForm: {},
            neverSelected: '',
            userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null,
            windowLocation: window.location.pathname.split('/')[window.location.pathname.split('/').length - 1] || '',
            allowData: '',
            valueShowing: 0,
            emptyData: ''
        }
    }

    componentDidMount = async () => {
        let orgName = '';
        this.state.orgDetails = orgData('');
        if (this.state.windowLocation === RA_STR_MASTER_AUTH) {
            orgName = (this.state.windowLocation === RA_STR_MASTER_AUTH) ? "MASTERADMIN" : "";
            this.setState({
                orgParam: (this.state.windowLocation === RA_STR_MASTER_AUTH) ? "MASTERADMIN" : ""
            })
        }
        else {
            orgName = (this.state.windowLocation === RA_STR_BASIC_AUTH_PATH) ? this.state.orgDetails.orgName : "--globalorganization--";
            this.setState({
                orgParam: (this.state.windowLocation === RA_STR_BASIC_AUTH_PATH) ? this.state.orgDetails.orgName : "--globalorganization--"
            })
        }
        const getAuthPolicy = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['basicAuthUrl']}?orgName=${orgName}`
        };
        const getAuthStatus = await getService(getAuthPolicy);
        if (getAuthStatus && getAuthStatus.status === RA_API_STATUS['200']) {
            this.setState({
                authForm: getAuthStatus.data.basicAuthPolicy,
                allowAll: getAuthStatus.data.basicAuthPolicy.allowAll === 'Y' ? true : false,
                checkAllowAll: getAuthStatus.data.basicAuthPolicy.allowAll,
                neverSelected: getAuthStatus.data.basicAuthPolicy.neverSelected,
                checkPwd: getAuthStatus.data.basicAuthPolicy.neverSelected === false ? true : false
            })
            if (getAuthStatus.data.basicAuthPolicy.allowAll === 'Y') {
                this.setState({
                    allowMultiByte: true,
                    allowData: true,
                    valueShowing: 1
                })
            }
            else {
                this.setState({
                    allowMultiByte: false,
                })
            }
            if (this.state.neverSelected === false) {
                this.setState({
                    validPeriod: false
                })
            }
            else {
                this.setState({
                    validPeriod: true
                })
            }
        }
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    handleChange = (e) => {
        const authForm = this.state.authForm;
        const { name, value } = e.target;
        var fieldValue;
        if (e.target.type === RA_STR_CHECKBOX) {
            this.setState({
                allowAll: !this.state.allowAll,
                allowMultiByte: !this.state.allowMultiByte,
                allowData: false
            }, () => {
                this.state.checkAllowAll = (this.state.allowAll === true) ? 'Y' : 'N';
            })
        }
        if (e.target.name === '') {
            if (e.target.checked === false && this.state.valueShowing === 1) {
            } else {
                this.setState({ valueShowing: 2 })
            }
        }
        else if (e.target.type === RA_STR_RADIO) {
            this.setState({
                validPeriod: !this.state.validPeriod
            })
        }
        if (e.target.id === 'neverExpires') {
            this.setState({
                neverSelected: true,
                validPeriod: true
            })
        }
        else if (e.target.id === 'neverSelected') {
            this.setState({
                neverSelected: false,
                validPeriod: false
            })
        }
        else if (name === RA_STR_ALLOW_SPEC_CHAR) {
            fieldValue = value;
        }
        else if (name === 'adminPasswordExpireTime' && value === "") {
            fieldValue = null;
        }
        else if (value !== '') {
            fieldValue = parseInt(value);
        }
        else if (value === '') {
            fieldValue = 0
        }
        else {
            fieldValue = value
        }
        authForm[name] = fieldValue;
        this.setState({
            authForm
        })
    }

    keyPress = (e) => {
        this.handleChange(e);
    }
    handleSubmit = async () => {
        let params = {
            ...this.state.authForm,
            neverSelected: this.state.neverSelected,
            globalConfig: (this.state.windowLocation === RA_STR_BASIC_AUTH_NEXT) ? true : false,
            allowAll: this.state.checkAllowAll,
            orgName: this.state.orgParam,
        }
        const updateAuthPolicy = {
            method: 'PUT',
            url: `${serverUrl}${RA_API_URL['basicAuthUrl']}`,
            data: params
        };
        const updateAuthStatus = await getService(updateAuthPolicy);
        if (updateAuthStatus && updateAuthStatus.status === RA_API_STATUS['200']) {
            if (updateAuthStatus.data.basicAuthPolicy.allowAll === 'Y') {
                this.setState({
                    valueShowing: 1
                })
            }
            if (this.refs['parent'] !== null && updateAuthStatus.data.basicAuthPolicy.allowAll === 'Y') {
                let node = ReactDOM.findDOMNode(this.refs['parent']);
                if (node) {
                    var options = [].slice.call(node.querySelectorAll('input'));
                    for (var i = 0; i < options.length; i++) {
                        options[i].value = '';
                    }
                }
            }
            this.props.activateSuccessList(true, updateAuthStatus.data);
            this.props.activateErrorList(false, '');
        }
        else {
            this.props.activateErrorList(true, updateAuthStatus.data.errorList);
            this.props.activateSuccessList(false, '');
        }
    }
    render() {
        const authForm = this.state.authForm ? this.state.authForm : {};
        const { allowData, valueShowing, emptyData } = this.state;
        return (<div className='main'>
            {(this.state.windowLocation === RA_STR_BASIC_AUTH_PATH || this.state.windowLocation === RA_STR_BASIC_AUTH_NEXT) ?
                <div>
                    <h2 className="title">{RA_STR.basicAuthTitle}</h2>
                    <p className="desc">{RA_STR.basicAuthText}<br /><strong>Note:</strong> {RA_STR.basicAuthRefreshText}
                    </p>
                </div>
                :
                <div>
                    <h2 className="title">{RA_STR.masterBasicAuthTitle}</h2>
                    <p className="desc">{RA_STR.masterBasicAuthText}<br /></p>
                </div>
            }
            <span className="ecc-h1">{RA_STR.basicPwdPolicy}</span>
            <form>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.basicMinPwd}*</label>
                            <div className="col-sm-2">
                                <input type="number" className="form-control"
                                    name="minPasswordLength"
                                    defaultValue={authForm.minPasswordLength}
                                    onChange={this.handleChange}
                                    onKeyDown={this.keyPress} />
                            </div>
                            <div className="col-sm-3 col-form-label">  Enter a value between 6 and 32.</div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.basicMaxPwd}*</label>
                            <div className="col-sm-2">
                                <input type="number" className="form-control"
                                    name="maxPasswordLength"
                                    defaultValue={authForm.maxPasswordLength}
                                    onChange={this.handleChange}
                                    onKeyDown={this.keyPress} />
                            </div>
                            <div className="col-sm-3 col-form-label">  Enter a value between 6 and 32.</div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.basicMaxFailures}*</label>
                            <div className="col-sm-2">
                                <input type="number" className="form-control"
                                    name="strikeCount"
                                    defaultValue={authForm.strikeCount}
                                    onChange={this.handleChange}
                                    onKeyDown={this.keyPress} />
                            </div>
                            <div className="col-sm-3 col-form-label">  Enter a value between 3 and 10.</div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.basicMinNumChar}*</label>
                            <div className="col-sm-2">
                                <input type="number" className="form-control"
                                    name="minNumericChars"
                                    defaultValue={authForm.minNumericChars}
                                    onChange={this.handleChange}
                                    onKeyDown={this.keyPress} />
                            </div>
                            <div className="col-sm-3 col-form-label">  Enter a value between 0 and 32.</div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.basicPwdHisCount}*</label>
                            <div className="col-sm-2">
                                <input type="number" className="form-control"
                                    name="passwordHistoryCount"
                                    defaultValue={authForm.passwordHistoryCount}
                                    onChange={this.handleChange}
                                    onKeyDown={this.keyPress} />
                            </div>
                            <div className="col-sm-3 col-form-label">  Enter a value between 0 and 10.</div>
                        </div>
                    </div>
                </div>

                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.validPeriod}*</label>
                            <div className="col-sm-3">
                                <div className="row">
                                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                        {this.state.checkPwd ?
                                            <input type="radio" className="custom-control-input "
                                                id="neverSelected"
                                                name="neverSelected"
                                                defaultValue={authForm.neverSelected}
                                                defaultChecked={this.state.checkPwd}
                                                onChange={this.handleChange}
                                                onKeyDown={this.keyPress} />
                                            : <input type="radio" className="custom-control-input "
                                                id="neverSelected"
                                                name="neverSelected"
                                                defaultValue={authForm.neverSelected}
                                                onChange={this.handleChange}
                                                onKeyDown={this.keyPress} />
                                        }
                                        <label className="custom-control-label" htmlFor="neverSelected"></label>
                                    </div>
                                    <input type="number" className="form-control col-sm-4 validityPeriod"
                                        name="passwordExpiryDays"
                                        defaultValue={authForm.passwordExpiryDays}
                                        disabled={this.state.validPeriod}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                    <div className="col-sm-3 col-form-label">{RA_STR.days}</div>
                                </div>
                                <div className="row">
                                    <div className="custom-control custom-radio radioTopMargin validityPeriod">
                                        <input type="radio" className="custom-control-input "
                                            id="neverExpires"
                                            name="neverSelected"
                                            defaultValue={authForm.neverSelected}
                                            onChange={this.handleChange}
                                            defaultChecked={authForm.neverSelected}
                                            onKeyDown={this.keyPress} />
                                        <label className="custom-control-label" htmlFor="neverExpires">{RA_STR.neverExpires}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.allowMultiChar}</label>
                            <div className="col-sm-2">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input"
                                        id="allowAll" name="allowAll"
                                        defaultValue={this.state.allowAll}
                                        checked={this.state.allowAll}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                    <label className="custom-control-label" htmlFor="allowAll"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div ref='parent'>
                    <div className='row'>
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label ml-3">{RA_STR.minAlphaChar}*</label>
                                <div className="col-sm-2">
                                    <input type="number" className="form-control"
                                        name="minAlphaChars"
                                        defaultValue={allowData ? '' : (valueShowing === 1) ? '' : authForm.minAlphaChars}
                                        disabled={allowData || this.state.allowMultiByte}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                </div>
                                <div className="col-sm-3 col-form-label">  Enter a value between 0 and 32.</div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label ml-3">{RA_STR.minLowerCase}</label>
                                <div className="col-sm-2">
                                    <input type="number" className="form-control"
                                        name="minLowerCase"
                                        defaultValue={allowData ? '' : (valueShowing === 1) ? '' : authForm.minLowerCase}
                                        disabled={allowData || this.state.allowMultiByte}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label ml-3">{RA_STR.minUpperCase}</label>
                                <div className="col-sm-2">
                                    <input type="number" className="form-control"
                                        name="minUpperCase"
                                        defaultValue={allowData ? '' : (valueShowing === 1) ? '' : authForm.minUpperCase}
                                        disabled={allowData || this.state.allowMultiByte}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col-sm-10">
                            <div className="form-group row">
                                <label className="col-sm-3 col-form-label ml-3">{RA_STR.minSpecChar}*</label>
                                <div className="col-sm-2">
                                    <input type="number" className="form-control"
                                        name="minSplChars"
                                        defaultValue={allowData ? '' : (valueShowing === 1) ? '' : authForm.minSplChars}
                                        disabled={allowData || this.state.allowMultiByte}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                </div>
                                <div className="col-sm-3 col-form-label">   Enter a value between 0 and 32.</div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col-sm-11">
                            <div className="form-group row no-gutters">
                                <label className="col-sm-3 col-form-label">{RA_STR.allowSpecChar}</label>
                                <div className="col-sm-2">
                                    <input type="text" className="form-control"
                                        name="allowedSplChars"
                                        defaultValue={allowData ? '' : (valueShowing === 1) ? '' : authForm.allowedSplChars}
                                        disabled={allowData || this.state.allowMultiByte}
                                        onChange={this.handleChange}
                                        onKeyDown={this.keyPress} />
                                </div>
                                <span className="col-sm-4 col-form-label ml-1" title="!&quot;#$%&amp;'()*+,-./:;<=>?@[\]^_`{|}~">&nbsp;Allowed special characters...</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row'>
                    <div className="col-sm-10">
                        <div className="form-group row">
                            <label className="col-sm-3 col-form-label ml-3">{RA_STR.tempPwdDur}</label>
                            <div className="col-sm-2">
                                <input type="text" className="form-control"
                                    name="adminPasswordExpireTime"
                                    defaultValue={authForm.adminPasswordExpireTime}
                                    onChange={this.handleChange}
                                    onKeyDown={this.keyPress} />
                            </div>
                            <div className="col-sm-3 col-form-label">  Enter a value between 1 and 168 hours.</div>
                        </div>
                    </div>
                </div>


                <div className="form-group form-submit-button row">
                    <input className="secondary-btn ml-2" type="button" value="SAVE" onClick={this.handleSubmit} />
                </div>
            </form>
        </div>);
    }
}

export default BasicAuthenticationPolicy;