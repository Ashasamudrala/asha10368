import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl, RA_API_URL } from '../../shared/utlities/constants';
import './Login.css';
import Footer from '../Footer';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgName: '',
      loginError: false,
      orgError: false,
      orgMessage: '',
      hideDiv: false
    };
  }
  handleChange = e => {
    this.setState({ orgName: e.target.value, loginError: false, orgError: false });
  };
  handlekeyDown = e => {
    if (e.key === 'Enter') {
      if (e.target.name === 'orgName') {
        this.onNextClick();
      } else if (e.target.name === 'userPassword' || e.target.name === 'userName') {
        this.onRedirectNext();
      }
    }
  };
  onNextClick = async () => {
    const { orgName } = this.state;
    if (orgName === '' && orgName.trimLeft().length === 0) {
      this.setState({ orgName: '', loginError: true, orgError: false });
    } else if (orgName !== '') {
      const checkOrg = {
        method: 'POST',
        url: `${serverUrl}${RA_API_URL['authOrgUrl']}`,
        data: { orgName: orgName }
      };
      const checkOrgStatus = await getService(checkOrg);
      if (checkOrgStatus && checkOrgStatus.status === 400) {
        this.setState({ loginError: false, orgError: true, orgMessage: checkOrgStatus.data.errorList[0].errorMessage });
      } else if (checkOrgStatus && checkOrgStatus.status === 200) {
        localStorage.setItem('orgName', JSON.stringify(this.state.orgName));
        this.props.history.push(RA_API_URL['bamloginUrl']);
      }
    }
  };

  componentDidMount() {
    this.cookieExists();
  }
  cookieExists = () => {
    let name = 'COOKIE_NAME' + "=";
    let cookieExists = false;
    if (document.cookie) {
      let decodedCookie = decodeURIComponent(document.cookie);
      let ca = decodedCookie.split(';');
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          cookieExists = true;
        }
      }
    }
    this.setState({ hideDiv: cookieExists })
  }

  hideCurrentDiv = () => {
    var d = new Date();
    var expiresInDays = 30 * 24 * 60 * 60 * 1000;
    d.setTime(d.getTime() + expiresInDays);
    var expires = "expires=" + d.toGMTString();
    document.cookie = 'COOKIE_NAME' + "=" + 'cvalue' + "; " + expires + ";path=/";
    this.setState({ hideDiv: true });
  };
  keyPress = e => {
    if (e.keyCode === 13) {
      this.onNextClick();
    }
  };
  render() {
    const { orgName = '', loginError, orgError } = this.state;
    return (
      <div className="login-parent">
        <div className="eupopup-parent">
          {!this.state.hideDiv ? (
            <div className="eupopup-container">
              <p className="eupopup-body">
                Access to this system may result in sensitive data such as cookies, cached data, or downloaded reports
                being stored on your computer.
              </p>
              <center>
                <input type="submit" onClick={this.hideCurrentDiv} className="secondary-btn" value="Click here" />
                <span className="hide-message">if you do not want to see this message again</span>
              </center>
            </div>
          ) : (
              ''
            )}
        </div>
        <div className="wrapper">
          <div className="">
            <div className="clearfloat edl-login-box">
              <div id="brand-holder">
                <div>
                  <img src="images/ca-logo-new.png" alt="caLogo" height="50"></img>
                  <p className="ecc-h1">Administration Console</p>
                </div>
              </div>
              <div id="form-holder">
                <div className="form-header" id="signin-header">
                  SIGN IN
                </div>
                <form name="bamForm" method="post"></form>

                <div id="loginbox">
                  <div>
                    <p className="body-font margin-top-8">Organization Name</p>
                    <div className="edl_login_input">
                      <input
                        name="orgName"
                        title={!orgName ? "Please fill out this field." : ''}
                        className={`loginTextBox ${loginError ? ' error' : ''}`}
                        onKeyDown={this.keyPress}
                        onChange={this.handleChange}
                        value={orgName}
                        onKeyDown={this.handlekeyDown}
                        autoFocus
                      />
                      <div className={`${loginError ? 'error-msg' : 'no-msg'}`}>
                        Please enter your organization name.
                      </div>
                    </div>
                    <div>
                      <div className={`${orgError ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">
                        The Organization Name is not found. Please check if the Organization Name is correct or contact
                        your administrator.
                      </div>
                    </div>
                  </div>
                  <input
                    className="button edl_btn_main left_align margin-top-8 user-sign-in"
                    onClick={this.onNextClick}
                    type="submit"
                    value="NEXT"
                  />
                </div>
                <br className="clearfloat" />
                <br />
                <div className="eupopup"></div>
              </div>
            </div>
          </div>
          <div className="push"></div>
        </div>
        <Footer />
      </div>
    );
  }
}
export default Login;
