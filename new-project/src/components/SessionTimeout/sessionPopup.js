import React, { Component } from 'react';
import { logoutUrl } from '../../shared/utlities/constants';
import { RA_STR } from '../../shared/utlities/messages';
import './sessionPopup.scss';
import '../configOrgRelation/configOrgRelation.scss';


class SessionPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalSeconds: 120,
            displayText: '2 Minutes 0 Seconds'
        }
    }
    componentDidMount() {
        window.scrollTo({ top: 0, behavior: "smooth" });
        this.startTimer();
    }
    componentWillUnmount() {

    }
    extendSession = () => {
        this.props.extendSession();
    }
    startTimer = () => {
        let that = this;
        var counter = setInterval(timer, 1000);
        function timer() {
            let { totalSeconds, displayText } = that.state;
            if (totalSeconds != 0) {
                totalSeconds = totalSeconds - 1;
                let mins = 1, seconds = 0;
                if (totalSeconds < 120 && totalSeconds > 60) {
                    mins = 1;
                    seconds = totalSeconds - 60;
                } else if (totalSeconds < 60) {
                    mins = 0;
                    seconds = totalSeconds;
                }
                displayText = `${mins} ${RA_STR.Minutes} ${seconds} ${RA_STR.Seconds}`;
                that.setState({ totalSeconds, displayText });
            } else {
                clearInterval(counter);
                localStorage.removeItem('profiledata');
                window.location.href = (process.env.REACT_APP_ROUTER_BASE || '') + logoutUrl;
            }
        }
    }
    render() {
        let { displayText } = this.state;
        return <div className="sessionPopup config-org-relation">
            <div className="dialog-mask"  ></div>
            <div className="modal admin">
                <div className="modal-content">
                    <div className="dialog-header">
                        {RA_STR.idleTimeout}
                    </div>
                    <div className="sessionInfo internalMargin">
                        <p>{RA_STR.clearinActivity}</p>
                        <p>{RA_STR.maintainSec}</p>
                        <p>{RA_STR.loggedOutIn}{displayText}.</p>
                    </div>
                    <div className="modal-body row">
                        <div className="col-md-9"></div>
                        <div className="col-md-3">
                            <div className="mt-2">
                                <input className="secondary-btn float-right" type="submit" value="Extend Session" onClick={this.extendSession} ></input>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }

}

export default SessionPopup;
