import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import { getService } from '../../shared/utlities/RestAPI';
import {
    serverUrl, RA_STR_CHECKBOX, RA_API_CREATE_RULE_SET, RA_STR_CREATE_RULE_MSG,
    RA_STR_SELECT, RA_STR_CREATE_RULE_VALIDATION_MSG, AuthToken, RA_STR_CREATE_RULE_PATH,
    RA_STR_SS_CREATE_RULE_ORG, RA_API_URL
} from '../../shared/utlities/constants';
import  {RA_STR} from '../../shared/utlities/messages';
import './CreateRule.css';
import { orgData } from '../../shared/utlities/renderer';
import ErrorList from '../../shared/components/ErrorList/ErrorList';
import SuccessList from '../../shared/components/SuccessList/SuccessList';

class CreateRule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orgDetails: '',
            checkboxStatus: false,
            createRuleForm: {},
            newConfigName:'',
            userData: JSON.parse(localStorage.getItem('profiledata')) ? JSON.parse(localStorage.getItem('profiledata')) : null,
            configNamesList: [],
            windowLocation: window.location.pathname.split('/')[window.location.pathname.split('/').length - 1] || ''
        };

    }

    showErrorList = () => {
        window.scrollTo(0, 0);
    }

    componentDidMount = async () => {
        this.state.orgDetails = orgData('');
        let orgName = (this.state.windowLocation === RA_STR_CREATE_RULE_PATH) ? this.state.orgDetails.orgName : RA_STR_SS_CREATE_RULE_ORG;
        const getConfigNames = {
            method: 'GET',
            url: `${serverUrl}${RA_API_URL['getOrgUrl']}/${orgName}${RA_API_URL['ruleSet']}`
        };
        const getConfigStatus = await getService(getConfigNames);
        if (getConfigStatus && getConfigStatus.status === 200) {
            this.setState({
                configNamesList: getConfigStatus.data.configNamesList
            });
        }
    }

    componentWillUnmount() {
        this.props.activateErrorList(false, '');
        this.props.activateSuccessList(false, '');
    }

    handleChange = (e) => {
        if(e.target.name=="newConfigName"){
            this.setState({newConfigName:e.target.value.toUpperCase()})
        }
        const createRuleForm = this.state.createRuleForm;
        const { name, value } = e.target;
        var feildValue;
        (e.target.type === RA_STR_CHECKBOX) ?
            feildValue = e.target.checked
            :
            feildValue = value;
        if (e.target.type === RA_STR_CHECKBOX) {
            this.setState({ checkboxStatus: !this.state.checkboxStatus })
        }
        createRuleForm[name] = feildValue;
        this.setState({
            createRuleForm
        });
    }

    handleSubmit = async () => {
        var congfigName = this.state.createRuleForm['toUseConfigName'];
        var pass = this.state.createRuleForm['newConfigName'];
        var reg = /[`~!@#$%^&*()_|+\-=?;:'",.<> \{\}\[\]\\\/]{0-64}/;
        var test = reg.test(pass);
        if (Object.keys(this.state.createRuleForm).length === 0 || pass === undefined || pass === '') {
            alert(RA_STR_CREATE_RULE_MSG);
        }
        else if (this.state.createRuleForm['copy'] === true && congfigName === undefined) {
            alert('Choose an existing Ruleset to copy from.');
        }
        else if (test) {
            alert(RA_STR_CREATE_RULE_VALIDATION_MSG);
        }
        else {
            let params = {
                ...this.state.createRuleForm,
                createOwnOrReferGlobal: "createOwn"
            }
            let orgName = (this.state.windowLocation === RA_STR_CREATE_RULE_PATH) ? this.state.orgDetails.orgName : RA_STR_SS_CREATE_RULE_ORG;
            const createRuleSet = {
                method: 'POST',
                url: `${serverUrl}${RA_API_URL['getOrgUrl']}/${orgName}${RA_API_URL['ruleSet']}`,
                data: params
            };
            const createRuleSetStatus = await getService(createRuleSet);
            if (createRuleSetStatus && createRuleSetStatus.status === 400) {
                this.props.activateErrorList(true, createRuleSetStatus.data.errorList);
                this.props.activateSuccessList(false, '');
                this.showErrorList();
            }
            else {
                this.props.activateSuccessList(true, createRuleSetStatus.data);
                this.props.activateErrorList(false, '');
                this.showErrorList();
            }
        }
    }

    render() {
        return (
            <div className="main">
                <h2 className="title">{RA_STR.createRuleTitle}</h2>
                <p className="desc">{RA_STR.createRuleText}</p>

                <div className="col-sm-8">
                    <SingleInput
                        title={'Name'}
                        inputType={'text'}
                        name={'newConfigName'}
                        content={this.state.newConfigName||''}
                        controlFunc={this.handleChange}
                    />

                    <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label ml-6">{RA_STR.createRuleCopyText}</label>
                                <div className="col-sm-8">
                                    <div className="selectconfig">
                                        <div className="custom-control custom-checkbox">
                                            <input type="checkbox" name="copy"
                                                onChange={this.handleChange} className="rulesetCheckbox custom-control-input"
                                                defaultValue={this.state.checkboxStatus}
                                                checked={this.state.checkboxStatus}
                                                 id="checkRuleSet" />
                                            <label className="custom-control-label" htmlFor="checkRuleSet"></label>
                                        </div>
                                        <select className="form-control"
                                            name="toUseConfigName"
                                            disabled={!this.state.checkboxStatus}
                                            onChange={this.handleChange}>
                                            <option value="">{RA_STR_SELECT}</option>
                                            {this.state.configNamesList ? this.state.configNamesList.map((opt, i) => {
                                                return (
                                                    <option key={i}
                                                        value={opt}>{opt}</option>
                                                );
                                            }) : ''}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="form-group form-submit-button row">
                        <input className="secondary-btn ml-2" id="manageTokenButton"
                            type="submit" onClick={this.handleSubmit} value="CREATE"></input>
                    </div>

                </div>
            </div>);
    }
}

export default CreateRule;