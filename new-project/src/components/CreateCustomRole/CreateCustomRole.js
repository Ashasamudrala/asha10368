import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl , RA_API_URL , RA_STR_PROFILEDATA } from '../../shared/utlities/constants';
import './CreateCustomRole.css';
import CA_Utils from '../../shared/utlities/commonUtils';
class CreateCustomRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: [],
      roleName: '',
      roleDisplayName: '',
      roleDescription: '',
      roleBased: '',
      roles: [],
      loginData: JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA)) ?JSON.parse(localStorage.getItem(RA_STR_PROFILEDATA)) : '' ,
      selectOptions: [],
      optionDetails: [],
      options: [],
      roleIds: [],
      successdata: '',
      errordata:''
    }
  }
  showErrorList = () => {   
    window.scrollTo(0, 0);
  }
  componentWillMount = async () => {
    const getCustomRole = {
      method: 'GET',
      url: `${serverUrl}${RA_API_URL['predefinedRoles']}`
    };
    const getRoles = await getService(getCustomRole);
    if (getRoles.status === 200) {
      if(getRoles.data.roles){
      getRoles.data.roles.forEach((roles ,index) => {   
        if(index === 0) {
        } else {
          this.state.roleOptions.push(roles);
        }
     })
    }
      let privileges = JSON.parse(JSON.stringify(getRoles.data.roles[1].listOfPrivileges));
      this.getPrivileges(privileges,getRoles.data.roles[1]);
      let role = JSON.parse(JSON.stringify(getRoles.data.roles))
      role.splice(0,1);
      this.setState({ roleOptions: this.state.roleOptions, roles: role });
    }
  }


  // method helps for forming multiselect object method present in helperfunction
  getPrivileges = (listOfPrivileges , rolesMenu) => { 
   let multiselectObject =  CA_Utils.objectFormation(listOfPrivileges);
   multiselectObject['roleBased'] = rolesMenu ;
   multiselectObject['roleIds'] = rolesMenu ;
    this.setState(multiselectObject);
  }

  handleRolesChange = (e) => {
    let rolesMenu = JSON.parse(e.target.value);
    let privileges = JSON.parse(JSON.stringify(this.state.roles[e.target.selectedIndex].listOfPrivileges));
    this.getPrivileges(privileges,rolesMenu);
  }
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }
  componentWillUnmount(){
    this.props.activateErrorList(false,'');
    this.props.activateSuccessList(false,'');
  }
  handleCreateCustomRole = async () => {
    let turnedOffPrivileges = [] ;
    if(this.state.selectOptions.length !== 0) {
    for (let i = 0; i < Object.keys(this.state.selectOptions[0]).length; i++) {
      let item = Object.keys(this.state.selectOptions[0]);
      for (let y = 0; y < this.state.selectOptions[0][item[i]].length; y++) {
        let data = this.state.selectOptions[0][item[i]][y];
        turnedOffPrivileges.push({'privilegeId' : data['privilegeId']});
      }
    }
  }

    const createCustomRole1 = {
      method: 'POST',
      url: `${serverUrl}${RA_API_URL['createCustomRole']}`,
      data: {
        "displayName": this.state.roleDisplayName ? this.state.roleDisplayName :'' ,
        "orderId": this.state.roleIds['orderId'],
        "parentRole": this.state.roleIds['roleName'],
        "roleDescription": this.state.roleDescription ? this.state.roleDescription :'' ,
        "roleName": this.state.roleName ? this.state.roleName : '',
        "turnedOffPrivileges": turnedOffPrivileges
      }
    };

    const createCustomRoleStatus = await getService(createCustomRole1);

      if (createCustomRoleStatus.status === 200) {  
        let privileges = JSON.parse(JSON.stringify(this.state.roles[0].listOfPrivileges));
        this.getPrivileges(privileges,this.state.roles[0]); 
        this.props.activateSuccessList(true,createCustomRoleStatus.data);
        this.props.activateErrorList(false,'');  
        this.setState({
         roleName: '', roleDisplayName: '', roleDescription: ''
      });
      
    } else {
      this.props.activateErrorList(true, createCustomRoleStatus.data.errorList);
      this.props.activateSuccessList(false, '');
    }
  }
  handledoubleRightChange = () => {
    let multiselectObject = {};
    let { optionDetails , selectOptions  } = this.state ;
    if(optionDetails.length !== 0){
      multiselectObject = CA_Utils.handledoubleRightChange(optionDetails ,selectOptions ,'create' );
      this.setState(multiselectObject);
    }
  }


  handledoubleLeftChange = () => {
    let multiselectObject = {};
    let { optionDetails , selectOptions  } = this.state ;
    if(this.state.selectOptions.length !== 0) {
      multiselectObject = CA_Utils.handledoubleLeftChange(optionDetails ,selectOptions ,'create');
      this.setState(multiselectObject);
    }
  }


  handleLeftChange = (node) => {
    let { optionDetails , selectOptions  } = this.state ;
    let multiselectObject = {};
      multiselectObject =  CA_Utils.handleLeftChange(optionDetails ,selectOptions ,'create',node );
      this.setState(multiselectObject);
  }



  handleRightChange = (node) => {
    let { optionDetails , selectOptions  } = this.state ;
    let multiselectObject = {};
      multiselectObject =  CA_Utils.handleRightChange(optionDetails ,selectOptions ,'create',node );
      this.setState(multiselectObject);
  }

  
  componentWillUnmount(){
    this.props.activateErrorList(false,'');
    this.props.activateSuccessList(false,'');
  }
  render() {
    const { roleOptions , roleName , roleDisplayName , roleDescription} = this.state;
    return <div className="main">
      <h2 className="title">Create Custom Role</h2>
      <p className="desc">Create a custom administrator role. Thisrole must be derived from one of the predefined roles and can have asubset of the base role's privileges.</p>
      <div className="col-sm-5">
        <span className="ecc-h1 row">Role Details</span>
        <SingleInput
          title={'Role Name'}
          inputType={'text'}
          required={true}
          name={'roleName'}
          controlFunc={this.handleChange}
          content = {roleName}
        />
        <SingleInput
          title={'Role Display Name'}
          required={true}
          inputType={'text'}
          name={'roleDisplayName'}
          controlFunc={this.handleChange}
          content = {roleDisplayName} />
        <SingleInput
          title={'Role Description'}
          inputType={'text'}
          name={'roleDescription'} 
          controlFunc={this.handleChange}
          content = {roleDescription}
        />
        <div className="form-group dynamic-form-select">
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Role Based On:*</label>
            <div className="col-sm-8">
              <select
                name='roleBased'
                onChange={this.handleRolesChange}
                value={JSON.stringify(this.state.roleBased)}
                className="form-select form-control">
                {roleOptions.map(opt => {
                  return (
                    <option
                      key={opt.displayName}
                      value={JSON.stringify(opt)}>{opt.displayName}</option>
                  );
                })}
              </select>
            </div>
          </div>
        </div>
        <span className="ecc-h1 row">Set Privileges</span>
      </div>
      <SwappingSelectBox selectOptions={this.state.selectOptions}
        optionDetails={this.state.optionDetails} handledoubleRightChange={this.handledoubleRightChange} handledoubleLeftChange={this.handledoubleLeftChange} handleRightChange={this.handleRightChange} handleLeftChange={this.handleLeftChange} optDetails={true} rightMenuName = 'Unavailable Privileges' leftMenuName='available Privileges'/>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton"
          type="submit" value="Create" onClick=
          {this.handleCreateCustomRole}></input>
      </div>
    </div>;
  }
}

export default CreateCustomRole;
