import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import Dashboard from './components/Dashboard/Dashboard';
import Login from './components/Login/Login';
import BamLogin from './components/BamLogin/BamLogin';
import masterAdminLogin from './components/masterAdminLogin/masterAdminLogin';
import './App.css';
import './shared/utlities/color-variables.css';
import './shared/utlities/commonUtils';
import store from './redux/store';
import AdminLogout from './components/AdminLogout/AdminLogout';

class App extends Component {
  componentWillMount() {
    document.title = 'CA Administration Console';
  }

  render() {
    return (
      <Provider store={store}>
        <BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE || ''}>
          <Switch>
            <Redirect path="/" exact to="/adminconsole" />
            <Route path="/adminconsole" exact component={Login} />
            <Route path="/masteradminconsole" exact component={masterAdminLogin} />
            <Route path="/bamlogin" component={BamLogin} />
            <Route path="/users" component={Dashboard} />
            <Route path="/org" component={Dashboard} />
            <Route path="/server-config" component={Dashboard} />
            <Route path="/reports" component={Dashboard} />
            <Route path="/case-manage" component={Dashboard} />
            <Route path="/myprofile" component={Dashboard} />
            <Route path="/adminlogout" component={AdminLogout} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}
export default App;
