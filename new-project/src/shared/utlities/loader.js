import React, { Component } from 'react';
class Loader extends Component {
 constructor(props) {
 super(props);
 }


 render() {
 let showLoading = this.props.show;
 return (
 <React.Fragment>
 {
 (showLoading) ?
 <img src="images/loading.gif" width="20" height="20" alt="no-img" /> :<React.Fragment/>
 }
 </React.Fragment>
 );
 }
}
export default Loader;
