import * as _ from 'underscore';

const CA_Utils = {
  covertObjectToArray: (inputObject) => {
    if (!inputObject) {
      return [];
    }
    return _.map(inputObject, function (v, k) {
      return { value: k, content: v };
    });
  },

  objToArray: (obj, type) => {
    let array = [];
    if (type === 'object' && obj !== null) {
      Object.keys(obj).forEach(function (key) {
        array.push({
          'content': obj[key],
          'key': key
        });
      });
    } else if (type === 'string' && obj !== null) {
      Object.keys(obj).forEach(function (key) {
        array.push(obj[key])
      });
    }
    return array;
  },

  arrayToObj: (array) => {
    if (array.length) {
      const constructedObj = {};
      array.forEach(element => {
        constructedObj[element] = element;
      })
      return constructedObj;
    }
  },

  // convert customeAttributes Array<any> to obj
  customAttributes: (array) => {
    const customAttrObj = {}
    array.forEach(element => {
      if (element.name || element.value)
        customAttrObj[element.name] = element.value;
    })
    return customAttrObj;
  },

  // convert customAttributes obj to Array<any>
  convertCustAttr: (obj) => {
    const attributesArray = [];
    for (let property in obj) {
      var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
      var product = {
        id: id,
        name: property,
        value: obj[property]
      }
      attributesArray.push(product);
    }
    return attributesArray;
  },

  objValToVal: (obj) => {
    const finalObj = {}
    for (let property in obj) {
      finalObj[obj[property]] = obj[property];
    }
    return finalObj;
  },
  // swappping object formation 
  objectFormation: (listOfPrivileges) => {
    let privileges = JSON.parse(JSON.stringify(listOfPrivileges));
    let options = [];
    let avaliableprivileges = [];
    let unavaliableprivileges = [];
    let optionDetails = [];
    let selectOptions = [];
    let multiselectObject = [];

    for (let index = 0; index < privileges.length; index++) {
      options.push(privileges[index].headerName);
    }
    options.sort();
    privileges.sort(function (a, b) {
      var x = a.headerName.toLowerCase();
      var y = b.headerName.toLowerCase();
      if (x < y) { return -1; }
      if (x > y) { return 1; }
      return 0;
    });
    let data = CA_Utils.getUnique(JSON.parse(JSON.stringify(options)));
    for (let i = 0; i < data.length; i++) {
      let startIndex;
      let endIndex;
      let tempListObject = {};
      let tempPrivilegeListObject = {};
      for (let y = 0; y < privileges.length; y++) {
        if (privileges[y].headerName === data[i]) {
          if (startIndex === undefined) {
            startIndex = y
          }
        }
        else {
          if (endIndex === undefined && startIndex !== undefined) {
            endIndex = y;
            break;
          }
        }
      }
      if (startIndex !== undefined) {
        tempListObject[data[i]] = privileges.slice(startIndex, endIndex).sort(function (a, b) {
          var x = a.displayName.toLowerCase();
          var y = b.displayName.toLowerCase();
          if (x < y) { return -1; }
          if (x > y) { return 1; }
          return 0;
        });
      }
      else {
        tempListObject[data[i]] = []
      }
      tempPrivilegeListObject[data[i]] = [];
      avaliableprivileges.push(tempListObject);
      unavaliableprivileges.push(tempPrivilegeListObject);
    }
    for (let k = 0; k < avaliableprivileges.length; k++) {
      let optionDetailsObject = {};
      let selectDetailsObject = {}
      for (let m = 0; m < avaliableprivileges.length; m++) {
        optionDetailsObject[data[m]] = avaliableprivileges[m][data[m]];
        selectDetailsObject[data[m]] = unavaliableprivileges[m][data[m]];
      }
      optionDetails = [];
      selectOptions = [];
      optionDetails.push(optionDetailsObject);
      selectOptions.push(selectDetailsObject);
    }
    multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions, data: data };
    return multiselectObject;
  },
  // removing duplicate elemets from array 
  getUnique: (array) => {
    var uniqueArray = [];
    for (var value of array) {
      if (uniqueArray.indexOf(value) === -1) {
        uniqueArray.push(value);
      }
    }
    return uniqueArray;
  },
  // export function for handle double right change for create related (swapping)
  handledoubleRightChange: (optionDetails, selectOptions, relatedSwapping) => {
    if (relatedSwapping === 'create') {
      let multiselectObject = [];
      Object.keys(optionDetails[0]).forEach(function (item) {
        for (let y = 0; y < optionDetails[0][item].length; y++) {
          let data = optionDetails[0][item][y];
          selectOptions[0][item].push(data);
          optionDetails[0][item].splice(y, 1);
          y = y - 1;
        }
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    } else if (relatedSwapping === 'update') {
      let multiselectObject = [];
      Object.keys(optionDetails[0]).forEach(function (item) {
        for (let y = 0; y < optionDetails[0][item].length; y++) {
          let data = optionDetails[0][item][y];
          selectOptions[0][item].push(data);
          optionDetails[0][item].splice(y, 1);
          y = y - 1;
        }
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    } 
    else if(relatedSwapping === 'disableupdate'){
      let multiselectObject = [];
      Object.keys(optionDetails[0]).forEach(function (item) {
        for (let y = 0; y < optionDetails[0][item].length; y++) {
          if( optionDetails[0][item][y].disabled === false){
          let data = optionDetails[0][item][y];
          selectOptions[0][item].push(data);
          optionDetails[0][item].splice(y, 1);
          y = y - 1;
        }
      }
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    }
    else {
      let multiselectObject = {};
      let optionDetailsObj = optionDetails;
      if (optionDetails.length !== 0) {
        Object.keys(optionDetails).forEach(function (item) {
          selectOptions[item] = optionDetails[item];
        });

        Object.keys(optionDetails).forEach(function (data) {
          if (optionDetails.hasOwnProperty(data)) {
            delete optionDetailsObj[data];
          }
        });
      }
      multiselectObject = { optionDetails: optionDetailsObj, selectOptions: selectOptions };
      return multiselectObject;
    }
  },
  // export function for handle left double change for create related (swapping)
  handledoubleLeftChange: (optionDetails, selectOptions, relatedSwapping) => {
    if (relatedSwapping === 'create') {
      let multiselectObject = [];
      Object.keys(selectOptions[0]).forEach(function (item) {
        for (let y = 0; y < selectOptions[0][item].length; y++) {
          let data = selectOptions[0][item][y];
          optionDetails[0][item].push(data);
          selectOptions[0][item].splice(y, 1);
          y = y - 1;
        }
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    }
    else if (relatedSwapping === 'update') {
      let multiselectObject = [];
      Object.keys(selectOptions[0]).forEach(function (item) {
        for (let y = 0; y < selectOptions[0][item].length; y++) {
          let data = selectOptions[0][item][y];
          optionDetails[0][item].push(data);
          selectOptions[0][item].splice(y, 1);
          y = y - 1;
        }
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    }
    else if(relatedSwapping === 'disableupdate'){
      let multiselectObject = [];
      Object.keys(selectOptions[0]).forEach(function (item) {
        for (let y = 0; y < selectOptions[0][item].length; y++) {
          if(!(selectOptions[0][item][y].disabled === true)){
          let data = selectOptions[0][item][y];
          optionDetails[0][item].push(data);
          selectOptions[0][item].splice(y, 1);
          y = y - 1;
        }
      }
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    }
    else {
      let multiselectObject = {};
      let selectedOptions = selectOptions;
      if (selectOptions.length !== 0) {
        Object.keys(selectOptions).forEach(function (item) {
          optionDetails[item] = selectOptions[item];
        });

        Object.keys(selectOptions).forEach(function (data) {
          if (optionDetails.hasOwnProperty(data)) {
            delete selectedOptions[data];
          }
        });
      }
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectedOptions };
      return multiselectObject;
    }

  },
  //// export function for handle right change for create related (swapping)
  handleRightChange: (optionDetails, selectOptions, relatedSwapping, node) => {
    let multiselectObject = [];
    if (relatedSwapping !== 'nooptiongroup') {
      let selectedValues = CA_Utils.getselectedValues(node);
      if (selectedValues.length !== 0) {
        if (relatedSwapping === 'create') {
          selectedValues.forEach(function (selectedValues) {
            for (let j = 0; j < selectOptions[0][selectedValues.title].length; j++) {
              if (JSON.stringify(selectOptions[0][selectedValues.title][j]) === JSON.stringify(selectedValues.value)) {
                selectOptions[0][selectedValues.title].splice(j, 1);
                optionDetails[0][selectedValues.title].push(selectedValues.value);
                break;
              }

            }
          })
        }
        else {
          selectedValues.forEach(function (selectedValues) {
            for (let j = 0; j < selectOptions[0][selectedValues.title].length; j++) {
              if (selectOptions[0][selectedValues.title][j].privilegeId === selectedValues.value['privilegeId']) {
                selectOptions[0][selectedValues.title].splice(j, 1);
                optionDetails[0][selectedValues.title].push(selectedValues.value);
                break;
              }

            }
          })
        }
      }
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      CA_Utils.uncheckSelectedValues(node);
      return multiselectObject;
    } else {
      let selectedValues = CA_Utils.getselectedMenuValues(node);
      Object.values(selectedValues).forEach(function (item) {
        delete selectOptions[item.value]
        optionDetails[item.value] = item.text;;
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;

    }
  },
  //// export function for handle left change for create related (swapping)


  handleLeftChange: (optionDetails, selectOptions, relatedSwapping, node) => {
    let multiselectObject = [];
    if (relatedSwapping !== 'nooptiongroup') {
      let selectedValues = CA_Utils.getselectedValues(node);
      if (selectedValues.length !== 0) {
        if (relatedSwapping === 'create') {
          selectedValues.forEach(function (selectedValues) {
            for (let j = 0; j < optionDetails[0][selectedValues.title].length; j++) {
              if (JSON.stringify(optionDetails[0][selectedValues.title][j]) === JSON.stringify(selectedValues.value)) {
                optionDetails[0][selectedValues.title].splice(j, 1);
                selectOptions[0][selectedValues.title].push(selectedValues.value);
                break;
              }

            }
          })
        }
        else {
          selectedValues.forEach(function (selectedValues) {
            for (let j = 0; j < optionDetails[0][selectedValues.title].length; j++) {
              if (optionDetails[0][selectedValues.title][j].privilegeId === selectedValues.value['privilegeId']) {
                optionDetails[0][selectedValues.title].splice(j, 1);
                selectOptions[0][selectedValues.title].push(selectedValues.value);
                break;
              }

            }
          })
        }
      }
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      CA_Utils.uncheckSelectedValues(node);
      return multiselectObject;
    }
    else {
      let selectedValues = CA_Utils.getselectedMenuValues(node);
      Object.values(selectedValues).forEach(function (item) {
        delete optionDetails[item.value];
        selectOptions[item.value] = item.text;
      });
      multiselectObject = { optionDetails: optionDetails, selectOptions: selectOptions };
      return multiselectObject;
    }
  },
  setSelectedOptionObject : (selectOptions, emptyList) => {
    if (selectOptions.length !== 0) {
      Object.keys(selectOptions[0]).forEach(function (item) {
        Object.values(selectOptions[0][item]).forEach(function (data) {
          emptyList[0][item].push(data);
        });
      });
    }
    return emptyList;
  },
  // for making the object 
  setSelectedRightObject : (selectOptions, optDetails) => {
    let optionDetails = [];
    Object.keys(selectOptions[0]).forEach(function (item, index) {
      if (selectOptions[0][item].length === 0) {
        let flag = false;
        optDetails.forEach(function (data) {
          if (data === item) {
            flag = true;
            optionDetails.push(item);
            return;
          }
        })
        if (!flag) {
          delete selectOptions[0][item];
          index = index - 1;
        }
      } else {
        optionDetails.push(item);
      }
    })
    return { selectOptions: selectOptions, optDetails: optionDetails };
  },
  setSelectedLeftObject : (optionDetail, options) => {
    let optionDetails = [];
    Object.keys(optionDetail[0]).forEach(function (item, index) {
      if (optionDetail[0][item].length === 0) {
        let flag = false;
        options.forEach(function (data) {
          if (data === item) {
            flag = true;
            optionDetails.push(item);
            return;
          }
        })
        if (!flag) {
          delete optionDetail[0][item];
          index = index - 1;
        }
      } else {
        optionDetails.push(item);
      }
    })
    return { optionDetails: optionDetail, options: optionDetails };
  },
  // getting  the selected values from dom reference 
  getselectedValues : (node) => {
    let options = [].slice.call(node.querySelectorAll('option'));
    let selected = options.filter(function (option) {
      return option.selected;
    });
    let selectedValues = selected.map(function (option) {
      return { "value": JSON.parse(option.value), "id": option.id, 'title': option.title };
    });
    return selectedValues;
  },
  // getting  the selected values from dom reference for without headers
  getselectedMenuValues : (node) => {
    var options = [].slice.call(node.querySelectorAll('option'));
    var selected = options.filter(function (option) {
      return option.selected;
    });
    var selectedValues = selected.map(function (option) {
      return { "value": option.value, "id": option.id, "text": option.text };
    });
    return selectedValues;
  },
  // for making selected options deselect
  uncheckSelectedValues : (node) => {
    let options = [].slice.call(node.querySelectorAll('option'));
    options.forEach(function (option) {
      if (option.selected) {
        option.selected = false;
      }
    });
  },
   //oject gormation of update type  swapping
  swapSorting : (data)=>{
    data.sort(function (a, b) {
      var x = a.headerName.toLowerCase();
      var y = b.headerName.toLowerCase();
      if (x < y) { return -1; }
      if (x > y) { return 1; }
      return 0;
    });
    return data;
  },
  checkForInteger:(val, low, high)=>{
    if(val === "" || isNaN(val)) return false;
    if(val.indexOf(".") != -1 ) return false;
    val=Number(val);
    var i = parseInt(val);
    if(i != val) return false;
    if(low !== "" && !isNaN(low) && low > i) return false;
    if(high !== "" && !isNaN(high) && i > high) return false;
    return true;
  },
  getListData : (getData) => {
    var arrRespParts, arrElementList, nElementCounter, nListTokenCounter, sElementName, listForListsForAnElement;
    var listOfLists = [];
    if (getData != null && getData.length > 1)
    getData = getData.substring(1);
    arrRespParts = getData.split("|");
    for (nElementCounter = 0; nElementCounter < arrRespParts.length; nElementCounter++) {
      arrElementList = arrRespParts[nElementCounter].split(":");
      if (arrElementList == null || arrElementList.length == 0)
        continue;
      sElementName = arrElementList[0];
      listForListsForAnElement = new Array();
      for (nListTokenCounter = 1; (nListTokenCounter + 4) <= arrElementList.length; nListTokenCounter += 4) {
        listForListsForAnElement.push(new Array(arrElementList[nListTokenCounter], arrElementList[nListTokenCounter + 1], arrElementList[nListTokenCounter + 2], arrElementList[nListTokenCounter + 3]));
      }
      listOfLists.push(sElementName, listForListsForAnElement);
    }
    return listOfLists
  }
};

export default CA_Utils;
