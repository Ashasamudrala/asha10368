import axios from 'axios';
import { serverUrl, LoginToken, logoutUrl } from '../../shared/utlities/constants';


export async function getService(getOptions) {

    let profiledata = JSON.parse(localStorage.getItem('profiledata'));
    let LoginToken = profiledata ? profiledata.token : '';
    let AuthToken = '';
    if (LoginToken) {
        AuthToken = { 'Authorization': 'Bearer ' + LoginToken, 'Content-Type': 'application/json' };
    } else {
        AuthToken = { 'Content-Type': 'application/json' };
    }
    getOptions.headers = Object.assign({}, AuthToken, getOptions.headers || {});
    // console.log(getOptions.headers);
    try {
        let promise = await axios(getOptions);
        return promise;
    } catch (e) {
        if (e.response && e.response.status === 401 && LoginToken) {
            window.location.href = (process.env.REACT_APP_ROUTER_BASE || '') + logoutUrl;
        } else {
            return e.response;
        }
    }
}

export async function httpCall(method, url, data) {
    const httpOptions = {
        method: method,
        url: `${serverUrl}${url}`,
    }
    if (data) {
        httpOptions.data = data;
    }
    let response = await getService(httpOptions);
    return response;
}
