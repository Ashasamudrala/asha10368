import {RA_STR} from '../utlities/messages';
export const dynamicNavbar = [
    {
        route: "/users",
        description: RA_STR.admin,
        privilegeId: '',
        subRoutes:
            [{
                route: '/manageUsers',
                description: RA_STR.manageUsers,
                privilegeID: 'AAC.ViewAdmin',
                visbility: true,
                subRoutes: [
                    {
                        route: '/create',
                        description: RA_STR.createAdmin,
                        privilegeID: 'AAC.CreateAdmin'
                    },
                    {
                        route: '/bulkuploadadministrator',
                        description: 'BUlk Upload Administrators',
                        privilegeID: 'AAC.BulkCreateAdmin'
                    },
                    {
                        route: '/search',
                        description: RA_STR.searchAdmin,
                        privilegeID: 'AAC.ViewAdmin'
                    }
                ]
            }, {
                route: '/manageRoles',
                description: RA_STR.manageRoles,
                privilegeID: 'AAC.CreateCustomRole',
                visbility: true,
                subRoutes: [{
                    route: '/createCustomRole',
                    description: RA_STR.createRole,
                    privilegeID: 'AAC.CreateCustomRole'
                },
                {
                    route: '/updateCustomRole',
                    description: RA_STR.updateRole,
                    privilegeID: 'AAC.UpdateCustomRole'
                },
                {
                    route: '/deleteCustomRole',
                    description: RA_STR.deleteRole,
                    privilegeID: 'AAC.DeleteCustomRole'
                }
                ]
            }]
    },
   
    {
        route: '/org',
        description: RA_STR.organizations,
        privilegeId: '',
        hideSecondNav: true,
        subRoutes:
            [{
                route: '/createOrg',
                description: RA_STR.createOrg,
                privilegeID: 'AAC.CreateOrganization'
            },
            {
                route: '/bulkuploadorganisation',
                description: RA_STR.bulkUploadOrg,
                privilegeID: 'AAC.BulkOrgCreate'
            },
            {
                route: '/cacheStatus',
                description: RA_STR.checkCache,
                privilegeID: 'AAC.OrgCacheRefreshStatus'
            },
            {
                route: '/searchOrg',
                description: RA_STR.searchOrg,
                defaultOrg:true,
                privilegeID: 'AAC.UpdateOrganization'
            }
            ]
    },
    {
        route: '/server-config',
        description: RA_STR.srv_Config,
        privilegeId: '',
        subRoutes:
            [{
                route: '/ra',
                description:RA_STR.riskAnalytic,
                privilegeID: '',
                multilevel: true,
                subRoutes: [
                    {
                        BaseHeader: RA_STR.sysConfig,
                        subRoutesInfo: [
                            {
                                route: '/risk-conn',
                                description: RA_STR.riskConn,
                                privilegeID: 'RF.Connectivity'
                            },
                            {
                                route: '/trusted-cert-auth',
                                description: RA_STR.trustedCert,
                                privilegeID: 'RF.SetupProtocol'
                            },
                            {
                                route: '/manage-token-sites',
                                description: RA_STR.manageTokenSite,
                                privilegeID: 'RF.SetupTokenization'
                            },
                            {
                                route: '/manage-token',
                                description: RA_STR.manageToken,
                                privilegeID: 'RF.SetupOrgTokenization'
                            },
                            {
                                route: '/endpoint-manage',
                                description: RA_STR.endpointManage,
                                privilegeID: 'RF.EndpointMgmt'
                            }, {
                                route: '/amds-conn',
                                description: RA_STR.amdsConn,
                                privilegeID: 'RF.ViewAMDSConnectivity'
                            }
                        ]

                    },
                    {
                        BaseHeader: RA_STR.instanceConfig,
                        subRoutesInfo: [
                            {
                                route: '/instance-manage',
                                description: RA_STR.instanceManage,
                                privilegeID: 'RF.InstanceManage'
                            },
                            {
                                route: '/protocol-config',
                                description: RA_STR.protocolConfig,
                                privilegeID: 'RF.SetupProtocol'
                            }
                        ]
                    },
                    {
                        BaseHeader: RA_STR.additionalConfig,
                        subRoutesInfo: [
                            {
                                route: '/custom-report-view',
                                description: RA_STR.customReportViews,
                                privilegeID: 'RF.SelectCustomElements'
                            }
                        ]

                    },
                    {
                        BaseHeader: RA_STR.generalConfig,
                        subRoutesInfo: [
                            {
                                route: '/service-miss-config',
                                description: RA_STR.missConfig,
                                privilegeID: 'RF.ConfigManage'
                            },
                            {
                                route: '/ra-config',
                                description: RA_STR.configRelay,
                                privilegeID: 'RF.ExternalFields_config'
                            }
                        ]
                    },
                    {
                        BaseHeader: RA_STR.ruleMgmt,
                        subRoutesInfo: [
                            {
                                route: '/ss-create-ruleset',
                                description:RA_STR.createRule,
                                privilegeID: 'RF.ConfigurationCreate'
                            },
                            {
                                route: '/create-list',
                                description: RA_STR.createList,
                                privilegeID: 'RF.CreateList'
                            }
                        ]
                    },
                    {
                        BaseHeader: RA_STR.rulesMgmt,
                        subRoutesInfo: [
                            {
                                route: '/Rules-Scoring',
                                description: RA_STR.rulesScoringMgmt,
                                privilegeID: 'RF.ConfigurationRules'
                            }, {
                                route: '/manage-list-data',
                                description: RA_STR.manageListMapping,
                                privilegeID: 'RF.UploadListData'
                            }, {
                                route: '/associate-end-pont',
                                description: RA_STR.associateEndpoint,
                                privilegeID: 'RF.OrgEndpointAssociation'
                            }, {
                                route: '/ss-callout-config',
                                description: RA_STR.calloutConfig,
                                privilegeID: 'RF.ConfigurationCallout'
                            }
                        ]
                    },
                    {
                        BaseHeader: RA_STR.dataFeed,
                        subRoutesInfo: [
                            {
                                route: '/manage-data-feed',
                                description: RA_STR.manageEndPointConfig,
                                privilegeID: 'RF.DfEndpointConfig'
                            }, {
                                route: '/map-data-feed',
                                description: RA_STR.manageEndpointOrg,
                                privilegeID: 'RF.Datafeed_config'
                            }
                        ]
                    },
                    {
                        BaseHeader: RA_STR.migradeToProduction,
                        subRoutesInfo: [
                            {
                                route: '/migrate-prod',
                                description: RA_STR.migradeToProduction,
                                privilegeID: 'RF.ConfigurationDataMigrate'
                            }
                        ]
                    }
                ]
            },
            {
                route: '/ac',
                description:RA_STR.adminReports,
                privilegeID: '',
                multilevel: true,
                subRoutes: [
                    {
                        BaseHeader: RA_STR.sysConfig,
                        subRoutesInfo: [
                            {
                                route: '/am-conn',
                                description: RA_STR.amConnConfig,
                                privilegeID: 'AAC.UpdateGlobalAMConfig'
                            },
                            {
                                route: '/uds-conn',
                                description: RA_STR.udsConnConfig,
                                privilegeID: 'AAC.UpdateGlobalUDSConfig'
                            },
                            {
                                route: '/refresh-cache',
                                description:RA_STR.refreshCache,
                                privilegeID: 'AAC.GlobalCacheRefresh'
                            },
                            {
                                route: '/cacheStatus',
                                description: RA_STR.checkCache,
                                privilegeID: 'AAC.GlobalCacheRefreshStatus'
                            },
                            {
                                route: '/attr-encry-config',
                                description:RA_STR.attrEncConfig,
                                privilegeID: 'AAC.ConfigureGlobalChosenEncrpytionSet'
                            },
                            {
                                route: '/local-config',
                                description: RA_STR.localConfig,
                                privilegeID: 'AAC.ConfigureLocaleConfig'
                            }
                        ]

                    },
                    {
                        BaseHeader: 'UDS Configuration',
                        subRoutesInfo: [
                            {
                                route: '/uds-config',
                                description: RA_STR.udsConfig,
                                privilegeID: 'AAC.UpdateGlobalUDSConfig'
                            },
                            {
                                route: '/default-org',
                                description:RA_STR.defaultOrg,
                                privilegeID: 'AAC.SetDefaultOrganization'
                            },
                            {
                                route: '/config-account',
                                description: RA_STR.configAccount,
                                privilegeID: 'AAC.AddAccountType'
                            },
                            {
                                route: '/email-telephone-config',
                                description: RA_STR.emailTelephone,
                                privilegeID: 'AAC.AddGlobalContactType'
                            }
                        ]

                    },
                    {
                        BaseHeader: RA_STR.authentication,
                        subRoutesInfo: [
                            {
                                route: '/basic-auth',
                                description: RA_STR.basicAuthPolicy,
                                privilegeID: 'AAC.UpdateGlobalBasicAuthPolicy'
                            },
                            {
                                route: '/master-auth',
                                description: RA_STR.masterAdminAuth,
                                privilegeID: 'AAC.UpdateMABasicAuthPolicy'
                            },
                            {
                                route: '/amds-profile',
                                description: RA_STR.amdsProfile,
                                privilegeID: 'AAC.AssociateAMDSProfile'
                            }
                        ]

                    },
                    {
                        BaseHeader: RA_STR.webServices,
                        subRoutesInfo: [
                            {
                                route: '/auth',
                                description: RA_STR.authAuth,
                                privilegeID: 'AAC.GlobalWSAnAConfig'
                            }
                        ]

                    }
                ]
            }
            ]
    }, {
        route: '/org/searchOrg/basicOrg',
        description: RA_STR.basicOrgInfo,
        privilegeId: '',
        dynamicLevel: true,
        subRoutes:
            [{
                route: '/org-details',
                description: RA_STR.orgDetails,
                privilegeID: 'AAC.UpdateOrganization'
            },
            {
                route: '/basic-auth-policy',
                description: RA_STR.basicAuthPolicy,
                privilegeID: 'AAC.UpdateOrgBasicAuthPolicy'
            },
            {
                route: '/bulk-upload',
                description: RA_STR.bulkUpload,
                privilegeID: 'AAC.BulkUpload'
            },
            {
                route: '/view-bulk-requests',
                description: RA_STR.viewBulkReq,
                privilegeID: 'AAC.ViewBulkRequests'
            }

            ]
    },
    {
        route: '/users/manageUsers/basicUser',
        description: 'Basic user Information',
        privilegeId: '',
        dynamicLevel: true,
        subRoutes:
            [{
                route: '/userInfo',
                description: 'User Details',
                privilegeID: 'AAC.ViewAdmin'
            }
            ]
    },
    
    {
        route: '/org/searchOrg/risk-engine',
        description: RA_STR.riskEngine,
        privilegeId: '',
        dynamicLevel: true,
        multilevel: true,
        subRoutes: [
            {
                BaseHeader:RA_STR.generalRiskAnalytics,
                subRoutesInfo: [
                    {
                        route: '/assign-channels',
                        description:RA_STR.assignChannels,
                        privilegeID: 'RF.ChannelConfigManage'
                    },
                    {
                        route: '/miscellenousConfiguartions',
                        description:RA_STR.missConfig,
                        privilegeID: 'RF.ConfigManage'
                    },
                    {
                        route: '/conf-org-rel',
                        description: RA_STR.configOrgRel,
                        privilegeID: 'RF.PARENTCHILDCONFIG'
                    },
                    {
                        route: '/customize-report-view',
                        description: RA_STR.customizeReportViews,
                        privilegeID: 'RF.OrgCustomElements'
                    },
                    {
                        route: '/enable-data-feed',
                        description: RA_STR.enableDataFeedConfig,
                        privilegeID: 'RF.Datafeed_enable'
                    }
                ]
            },
            {
                BaseHeader: RA_STR.ruleMgmt,
                subRoutesInfo: [
                    {
                        route: '/create-ruleset',
                        description: RA_STR.createRule,
                        privilegeID: 'RF.ConfigurationCreate'
                    },
                    {
                        route: '/assign-ruleset',
                        description: RA_STR.assignRuleset,
                        privilegeID: 'RF.ConfigurationActivate'
                    },
                    {
                        route: '/create-list',
                        description: RA_STR.createList,
                        privilegeID: 'RF.CreateList'
                    },
                    {
                        route: '/edit-list',
                        description: RA_STR.editList,
                        privilegeID: 'RF.EditList'
                    }
                ]
            },
            {
                BaseHeader: RA_STR.rulesMgmt,
                subRoutesInfo: [
                    {
                        route: '/Rules-Scoring',
                        description: RA_STR.rulesScoringMgmt,
                        privilegeID: 'RF.ConfigurationRules'
                    }, {
                        route: '/manage-list-data',
                        description: RA_STR.manageListMapping,
                        privilegeID: 'RF.UploadListData'
                    },{
                        route: '/model-config',
                        description: RA_STR.modelConfig,
                        privilegeID: 'RF.ConfigurationModelConfig'
                    }, {
                        route: '/callout-config',
                        description: RA_STR.calloutConfig,
                        privilegeID: 'RF.ConfigurationCallout'
                    }
                ]
            },
            {
                BaseHeader:RA_STR.migradeToProduction,
                subRoutesInfo: [
                    {
                        route: '/migrate-prod',
                        description: RA_STR.migradeToProduction,
                        privilegeID: 'RF.ConfigurationDataMigrate'
                    }
                ]
            }
        ]
    },
    {
        route: '/reports',
        description: RA_STR.reports,
        privilegeId: '',
        subRoutes:
            [{
                route: '/ra',
                description: RA_STR.riskAnalytic,
                privilegeID: '',
                multilevel: true,
                subRoutes: [
                    {
                        BaseHeader: '',
                        subRoutesInfo: [
                            {
                                route: '/risk-conn',
                                description:RA_STR.reportSummary,
                                privilegeID: 'RF.ReportSummary'
                            }
                        ]

                    },
                    {
                        BaseHeader: '',
                        subRoutesInfo: [
                            {
                                route: '/instance-mgmt-report',
                                description: RA_STR.instanceManageReport,
                                privilegeID: 'RF.ViewSvrMgmtReport'
                            }
                        ]

                    },
                    {
                        BaseHeader: RA_STR.riskActivityReport,
                        subRoutesInfo: [
                            {
                                route: '/risk-activity-report',
                                description: RA_STR.riskDetailReport,
                                privilegeID: 'RF.ViewDetailActivityReport'
                            },
                            {
                                route: '/risk-summary-report',
                                description: RA_STR.riskAdviceReport,
                                privilegeID: 'Rf.ViewAdviceSummaryReport'
                            }
                        ]

                    },
                    {
                        BaseHeader:RA_STR.otherReports,
                        subRoutesInfo: [
                            {
                                route: '/exception-user-report',
                                description: RA_STR.exceptionReport,
                                privilegeID: 'RF.ViewExceptionUserReport'
                            }, {
                                route: '/transaction-report',
                                description: RA_STR.transactionDataReport,
                                privilegeID: 'RF.ViewTransactionDataReport'
                            },
                            {
                                route: '/rule-config-report',
                                description: RA_STR.ruleConfigReport,
                                privilegeID: 'RF.ViewAddonRuleReport'
                            },
                            {
                                route: '/rules-data-report',
                                description: RA_STR.rulesDataReport,
                                privilegeID: 'RF.ViewListAddonReport'
                            }, {
                                route: '/device-summary-report',
                                description: RA_STR.deviceDateReport,
                                privilegeID: 'RF.ViewDeviceSummaryReport'
                            }
                        ]
                    }
                ]
            }, {
                route: '/ac',
                description: RA_STR.adminReport,
                privilegeId: '',
                hideSecondNav: true,
                subRoutes:
                    [{
                        route: '/activity-report',
                        description: RA_STR.activityReport,
                        privilegeID: 'AAC.ViewMyActivityReport'
                    },
                    {
                        route: '/admin-activity-report',
                        description: RA_STR.adminActivityReport,
                        privilegeID: 'AAC.ViewActivityReport'
                    },
                    {
                        route: '/user-activity-report',
                        description: RA_STR.userActivityReport,
                        privilegeID: 'AAC.ViewUserActivityReport'
                    },
                    {
                        route: '/user-creation-report',
                        description: RA_STR.userCreationReport,
                        privilegeID: 'AAC.ViewUserCreationReport'
                    },
                    {
                        route: '/org-report',
                        description: RA_STR.orgReport,
                        privilegeID: 'AAC.ViewOrgActivityReport'
                    },
                    {
                        route: '/bulk-upload-activity',
                        description: RA_STR.bulkUploadActivity,
                        privilegeID: 'AAC.ViewOrgActivityReport'
                    },
                    {
                        route: '/entitlement-report',
                        description: RA_STR.entitlementReport,
                        privilegeID: 'AAC.ViewEntitlementReport'
                    }
                    ]
            }

            ]
    },
    {
        route: '/case-manage',
        description: RA_STR.caseMgmt,
        privilegeId: '',
        hideSecondNav: true,
        subRoutes: [
            {
                BaseHeader: RA_STR.caseMgmt,
                subRoutesInfo: [
                    {
                        route: '/manage-inbound',
                        description:RA_STR.manageInboundCalls,
                        privilegeID: 'Rf.ManageInboundCall'
                    },
                    {
                        route: '/work-cases',
                        description: RA_STR.workOnCases,
                        privilegeID: 'RF.UpdateCase'
                    },
                    {
                        route: '/analyze-transaction',
                        description: RA_STR.analyzeTransaction,
                        privilegeID: 'RF.ViewTransactionSummary'
                    },
                    {
                        route: '/manage-token',
                        description: RA_STR.manageToken,
                        privilegeID: 'RF.SetupOrgTokenization'
                    },
                    {
                        route: '/search-case',
                        description: RA_STR.searchCase,
                        privilegeID: 'RF.SearchCase'
                    }
                ]

            },
            {
                BaseHeader: RA_STR.queueMgmt,
                subRoutesInfo: [
                    {
                        route: '/queue-status',
                        description: RA_STR.viewQueueStatus,
                        privilegeID: 'Rf.ViewQueueStatus'
                    },
                    {
                        route: '/manage-queue',
                        description: RA_STR.manageQueue,
                        privilegeID: 'Rf.ManageQueues'
                    }
                ]

            },
            {
                BaseHeader: RA_STR.report,
                subRoutesInfo: [
                    {
                        route: '/case-activity-report',
                        description:RA_STR.caseActivity,
                        privilegeID: 'RF.ViewCaseMgmtActivityReport'
                    }, {
                        route: '/average-report',
                        description: RA_STR.averageLifeReport,
                        privilegeID: 'RF.ViewCaseMgmtAgingReport'
                    }, {
                        route: '/fraud-report',
                        description: RA_STR.fraudStaticReport,
                        privilegeID: 'Rf.ViewCaseMgmtReport'
                    }, {
                        route: '/rule-report',
                        description: RA_STR.rulesEffectiveReport,
                        privilegeID: 'Rf.ViewTxnsByMatchedRuleReport'
                    },
                    {
                        route: '/false-report',
                        description: RA_STR.falsePositiveReport,
                        privilegeID: 'Rf.ViewFalsePositiveReport'
                    }, {
                        route: '/reviewer-report',
                        description: RA_STR.reviewerCase,
                        privilegeID: 'RF.ViewReviewerEffCaseReport'
                    }, {
                        route: '/reviewer-report-fraud',
                        description: RA_STR.reviewerFraud,
                        privilegeID: 'RF.ViewReviewerEffFraudReport'
                    }, {
                        route: '/rule-report-fraud',
                        description: RA_STR.ruleEffectReport,
                        privilegeID: 'RF.ViewRuleEffectFraudReport'
                    }
                ]
            }
        ]
    }
]

