
export const serverUrl = process.env.REACT_APP_SERVER_URL;
// export const serverUrl =  'http://192.168.236.99:8180/arcotadmin/api';


// string constants
export const RA_STR_MASTERADMIN = 'MASTERADMIN';
export const RA_STR_PROFILEDATA = 'profiledata';
export const RA_STR_CHECKBOX = 'checkbox';
export const RA_STR_RADIO = 'radio';
export const RA_STR_SELECT = '--Select--';
export const RA_STR_MA = 'MA';
export const RA_STR_OPTIONDETAILS = 'optionDetails';
export const RA_STR_SELECTOPTION = 'selectOption';
export const RA_STR_USERNAME = 'username';
export const RA_STR_PASSWORD = 'password';
export const RA_STR_NEWPASSWORD = 'newpassword';
export const RA_STR_CONFIRMPASSWORD = 'confirmpassword';
export const RA_STR_INITIAL = 'INITIAL';

export const RA_STR_BASIC_AUTH_PATH = 'basic-auth-policy';
export const RA_STR_BASIC_AUTH_NEXT = 'basic-auth';
export const RA_STR_CALLOUT_CONFIG = 'callout-config';
export const RA_STR_MASTER_AUTH = 'master-auth';
export const RA_STR_ALLOW_SPEC_CHAR = 'allowedSplChars';
export const RA_STR_CREATE_RULE_PATH = 'create-ruleset';
export const RA_STR_SS_CREATE_RULE_ORG = 'SYSTEM';
export const RA_STR_CREATE_RULE_MSG = 'Ruleset Name should not be empty.';
export const RA_STR_CREATE_RULE_VALIDATION_MSG =
  'Validation error in Config Name. 1. Should not be empty. 2. Should not contain Whitespace. 3. Should not exceeds length 64. 4. Should not contain non-printable characters. 5. Should not contain special character ;#%&. 6. Should not have a malicious statement like <.*script.*languages*=s*javascript.*>.';
export const RA_STR_SELFORG = 'self';
export const RA_ORG_OBJECT = '';
export const LOADING_TEXT = 'Loading ... please wait.';
export const SYSTEM = 'SYSTEM';
export const RA_STR_ACTIVEINFO = 'ACTIVE';
export const RA_STR_ACTIVE = 'Active';
export const RA_STR_INACTIVE = 'Inactive';
export const RA_STR_INACTIVE_TEMP = 'INACTIVE_TEMP';
export const RA_STR_API_INACTIVE = "INACTIVE";
export const RA_STR_API_DELETE = 'DELETE';
export const RA_STR_API_ACTIVE = 'ACTIVE';
export const RA_SESSION_EXPIRED = 'Your current session has expired. Please ';
export const DELETE_ACC_TYPE = 'Are you sure you want to delete the account type?';
export const RA_API_STATUS = {
  '200': 200,
  '300': 300,
  '400': 400,
  '500': 500
};

// Api URls
export const RA_API_URL = {
  getEventTypes: '/common/eventtypes',
  postsearch: '/organization/search',
  searchactions: '/organization',
  mfamethods: '/organization/mfamethods',
  mfamethodstypes: '/organization/mfamethodstypes',
  encryptionset: '/organization/encryptionset',
  managingadmins: '/organization/managingadmins',
  contacts: '/organization/contacts',
  predefinedRoles: '/predefinedroles',
  customRoleDelete: '/customroles',
  localeUrl: '/locale',
  timezoneUrl: '/timezone',
  masterProfileUrl: '/profile/master',
  globalProfileUrl: '/profile',
  getCustomRoles: '/predefinedroles',
  createCustomRole: '/customroles',
  getCustomRoles: '/customroles',
  upateCustomRole: '/customroles',
  getAllPredefinedRoles: '/predefinedroles',
  usersUrl: '/users',
  bamloginUrl: '/bamlogin',
  adminUrl: '/auth/login',
  changepasswordUrl: '/auth/password',
  authOrgUrl: '/auth/organization',
  basicAuthUrl: '/organization/basicauthpolicy',
  getOrgUrl: '/organization',
  createAdminStepOneUrl: '/admin/create/validate/stepone',
  createAdminStepTwoUrl: '/admin',
  getRoles: '/manageableroles',
  ruleSet: '/ruleset',
  assignment: '/assignment',
  getBulkRequestStatus: '/organization/bulkupload/status',
  getBulkRequestDetails: '/organization/bulkupload/operations',
  searchRequests: '/bulkupload/searchrequests',
  viewRequests: '/bulkupload/viewrequestdetails',
  displayTasks: '/bulkupload/displaytasks',
  viewTasksDetails: '/bulkupload/viewtaskdetails',
  exportTasks: '/bulkupload/exporttasks',
  refreshCache: '/cacherefreshconfig',
  getLastRequestIds: '/cacherefreshstatus/requestids',
  searchCacheDetails: '/cacherefreshstatus/search/',
  getUdsConnectivityDetails: '/udsconfig/connectivity',
  udsConnectivityConfig: '/udsconfig/connectivity',
  udsAcTypes: '/udsconfig/accounttypes',
  customizereport: '/customizereport',
  operationsUrl: '/organization/bulkupload/operations',
  searchUrl: '/org/searchOrg',
  downloadOrganizationCSV: '/organization/downloadorganizationcsv',
  exportOrganizations: '/organization/exportorganizations',
  datafeed: '/datafeed',
  queryParams: '?actionid=-1&category=ECMDF&sortorder=ASC',
  getAuthMechanisms: '/organization/authmechanisms',
  getRepositoryTypes: '/organization/repositorytypes',
  getMfaMethods: '/organization/mfamethods',
  getMfaTypes: '/organization/mfatypes',
  createOrg: '/organization',
  status: '/organization/status',
  getOrganizationUrl: '/organizations/',
  getOrgRules: '/rulelist',
  GetListOfRules: '/rulelist/all',
  sharableOrg: '/rulelist/shareableorganizations',
  parentOrg: '/rulelist/parentorgList',
  getDatafeedEndpoints: '/datafeed/endpoints',
  getEndpointConfiguration: '/datafeed/endpointconfig',
  insertDataFeedConfig: '/datafeed/configuration',
  getEndpointConfigurationOnAdd: '/datafeed/endpointnewconfig',
  viewBulkRequests: '/org/searchOrg/basicOrg/view-request-details',
  getRulelistDetails: '/details',
  getRulesetWithConfigNames: '/rulelist/confignames',
  getLocaleConfigs: '/localization',
  getPlugins: '/datafeed/plugins',
  getProtocols: '/datafeed/protocols',
  getAvailableOrganizations: '/datafeed/availableorgs',
  showConfigurations: '/datafeed/showconfiguration',
  mapconfiguration: '/datafeed/datafeedmapconfiguration',
  downloadAdministratorCSV: '/organization/downloadadministratorcsv',
  exportAdministrations: '/organization/exportadministrators',
  getEndpointCategories: '/organization/endpoint/categories',
  associateEndPoint: '/organization/endpoint',
  udsConfig: '/udsconfig',
  getDefaultOrgs: '/organization',
  setDefaultOrg: '/udsconfig/defaultorg',
  getCategories: '/endpointmanagement/categories',
  getEndPointsURL1: '/endpointmanagement/category/',
  getEndPointsURL2: '/endpoints',
  getQueuesURL: '/queues',
  getCerts: '/endpointmanagement/certs',
  testEndPoint: '/endpointmanagement/testconnection',
  saveQueueDetails: '/endpointmanagement/savequeue',
  saveEndPointDetails: '/endpointmanagement/saveendpoint',
  attributeEncryption: '/ssconfig/administrator/attrencconfig',
  amdsconnectivity: '/amdsconnectivity',
  amdsconnectivity: '/amdsconnectivity',
  getTimePeriods: '/common/timeperioddaterange',
  getMyActivityReport: '/reports/administrator/myactivity',
  exportMyActivityReportInCSV: '/reports/administrator/myactivity/export',
  userActivityReport: '/reports/administrator/useractivity/display',
  rfuseractivitydisplay: '/reports/administrator/useractivity/display',
  rfuseractivitydisplay1: '/reports/administrator/useractivity/display',
  searchDetails: '/users',
  getSelectedOrgs: '/selectedorgs',
  updateAdminInfo: '/admin',
  fetchWSAuthPrivileges: '/adminConsole/wsAuthConfig',
  saveWSAuthPrivileges: '/adminConsole/wsAuthConfig',
  assignchannels: '/assignchannels',
  exportUserActivityReportInCSV: '/reports/administrator/useractivity/export',
  userCreationReport: '/reports/administrator/usercreation/display',
  exportUserCreationReportInCSV: '/reports/administrator/usercreation/export',
  getInstances: '/protocolconfig/instances',
  getInstanceDetails: '/protocolconfig/instancedetails',
  protocolDetails: '/protocolconfig/details',
  displayReport: '/case/reports/activity',
  exportReportData: '/case/reports/activity/export',
  reportDataDrillDown: '/case/reports/activity/drilldown',
  caseFraudstatistics: '/case/reports/fraudstatistics',
  caseFraudstatisticsExport: '/case/reports/fraudstatistics/export',
  common: '/common',
  channels: '/channels',
  getAvailableOrgs: '/userorgs',
  updateChildOrgs: '/selectedorgs',
  manageToken: '/tokenization/siteMapList',
  getRuleset: 'getRulesetData',
  scoredetails: '/scoredetails',
  caseAvgLifeDisplayTable: '/case/reports/averagecaselife/display',
  exportAvgReport: '/arcotadmin1/api/case/reports/averagecaselife/export',
  siteMaps: '/ssconfig/riskanalytics/managetokenizationsite/siteMap',
  saveSiteMaps: '/ssconfig/riskanalytics/managetokenizationsite',
  saveEnabledAccounts: '/organization/enabledaccounts',
  addAccounttypesToOrganization: '/organization/accounttypes',
  getBulkReportViews: '/reports/administrator/bulkupload',
  getReportViews: '/reports/administrator/bulkupload/view',
  getdownloadReport: '/reports/administrator/bulkupload/download/',
  manageToken: '/tokenization/siteMapList',
  trustedCertificates: '/clientstore/trustedca',
  saveEnabledAccounts: '/organization/enabledaccounts',
  addAccounttypesToOrganization: '/organization/accounttypes',
  getBulkReportViews: '/reports/administrator/bulkupload',
  getReportViews: '/reports/administrator/bulkupload/view',
  getdownloadReport: '/reports/administrator/bulkupload/download/',
  caseAvgLifeDisplayTable: '/case/reports/averagecaselife/display',
  getRuleset: '/ruleset',
  organizationReport: '/reports/administrator/organization',
  entitlementReport: '/reports/administrator/entitlement',
  siteMaps: '/ssconfig/riskanalytics/managetokenizationsite/siteMap',
  saveSiteMaps: '/ssconfig/riskanalytics/managetokenizationsite',
  displayInstanceMangmentReport: '/ssconfig/reports/instances/display',
  ExportdisplayInstanceMangmentReport: '/ssconfig/reports/instances/export',
  exceptionUserReport: '/reports/riskanalytics/exceptionuser/',
  getCustomReport: '/customreport',
  showRuleReport: '/riskanalytics/ruleconfigreport',
  exportRuleReport: '/riskanalytics/ruleconfigreport/export',
  fraudReport: '/case/reports/ruleeffectiveness/fraud',
  getAmdProfileInfo: '/ssconfig/administrator/amds/profileassociation',
  siteMaps: '/ssconfig/riskanalytics/managetokenizationsite/siteMap',
  saveSiteMaps: '/ssconfig/riskanalytics/managetokenizationsite',
  searchCases: '/case/',
  searchHistory: '/history',
  export: '/export',
  getEventTypes: '/common/eventtypes',
  ruleEffectiveness: '/case/reports/ruleeffectiveness',
  ruleEffectExport: '/case/reports/ruleeffectiveness/export',
  riskSummaryDisplay: '/reports/riskanalytics/riskadvicesummary/display',
  riskSummaryExport: '/reports/riskanalytics/riskadvicesummary/export',
  displayInstanceMangmentReport: '/ssconfig/reports/instances/display',
  ExportdisplayInstanceMangmentReport: '/ssconfig/reports/instances/export',
  exceptionUserReport: '/reports/riskanalytics/exceptionuser/',
  getCustomReport: '/customreport',
  getConfigurations: '/ssconfig/ra/configfields',
  getTransactionFields: '/ssconfig/ra/configfields/transactionfields',
  createConfiguration: '/ssconfig/ra/configfields',
  exportRuleReport: '/riskanalytics/ruleconfigreport/export',
  saveUpdateExtFieldConfig: '/ssconfig/ra/configfields/saveextfieldconfig',

  // manage-list-data-category

  listtype: '/listtype',
  manageListData: '/managelistdata',
  negativeCountries: '/negativecountries',
  config: '/config',
  untrustedip: '/managelistdata/untrustedip/organization',
  trustedip: '/managelistdata/trustedip/organization',
  trustedaggregatorlist: '/managelistdata/trustedaggregatorlist/organization',
  aggregator: '/aggregator',
  addAggregator: '/managelistdata/trustedaggregatorlist/newAggregator/organization',
  upload: '/upload',
  categoryList: '/managelistdata/categorymappings/organization',
  categoryListUpload: '/managelistdata/categorymapping/organization',
  dataset: '/dataset',

  getReviewerEfficiencyCaseStatusReport: '/case/reports/reviewerefficiency/casestatus',
  getReviewerEfficiencyCaseStatusDetailedReport: '/case/reports/reviewerefficiency/casestatus/details',
  exportReviewerEfficiencyCaseStatusDetailedReport: '/case/reports/reviewerefficiency/casestatus/details/export',
  exportReviewerEfficiencyCaseStatusReport: '/case/reports/reviewerefficiency/casestatus/export',
  availableConfigs: '/callout/availableConfigs',
  callout: '/callout',
  details: '/details',
  transactionDataReport: '/reports/riskanalytics/transactiondata',
  getTimeRanges: '/reports/riskanalytics/transactiondata/timedaterange',
  saveAmdsprflData: '/ssconfig/administrator/amds/profileassociation',
  saveAmdsprflData: '/ssconfig/administrator/amds/profileassociation',
  deviceChannels: '/reports/devicesummary/channeelList',
  deviceDisplay: '/reports/devicesummary/display',
  getCaseStatuses: '/case/status',
  getRulesData: '/reports/riskanalytics/rulesdata',
  getRulesetNames: '/reports/riskanalytics/rulesdata/rulesetnames/',
  getDataSetList: '/reports/riskanalytics/rulesdata/listdataset',
  getUserActivityEvents: '/reports/administrator/useractivity/events',
  exportRuledataReport: '/reports/riskanalytics/rulesdata/export',
  displayRuledataReport: '/reports/riskanalytics/rulesdata',
  saveAmdsprflData: '/ssconfig/administrator/amds/profileassociation',
  getReviewerEfficiencyFraudStatusReport: '/case/reports/reviewerefficiency/fraudstatus',
  exportReviewerEfficiencyFraudStatusReport: '/case/reports/reviewerefficiency/fraudstatus/export',
  getReviewerEfficiencyFraudStatusDetailedReport: '/case/reports/reviewerefficiency/fraudstatus/details',
  exportReviewerEfficiencyFraudStatusDetailedReport: '/case/reports/reviewerefficiency/fraudstatus/details/export',
  getAdministratorActivityReport: '/reports/administrator/activity',
  getAdminReportInCSV: '/reports/administrator/activity/export',
  rfInstances: '/instancemanagement/instances',
  rfInstanceRefresh: '/instancemanagement/refreshriskfort',
  rfInstanceShutdown: '/instancemanagement/shutdownriskfort',
  caseManagementInstanceRefresh: '/instancemanagement/refreshcasemanagement',
  casemanagementInstanceShutdown: '/instancemanagement/shutdowncasemanagement',
  updateRiskfortInstance: '/instancemanagement/updateriskfort',
  updateCasemanagementInstance: '/instancemanagement/updatecasemanagement',
  brokerList: '/instancemanagement/brokerlist',
  getAdminReportInCSV: '/reports/administrator/activity/export',
  fraudReport: '/case/reports/ruleeffectiveness/fraud',
  eventTypes: '/common/eventtypes',
  searchByCaseId: '/case/search',
  searchByCriteria: '/case/searchbycriteria',
  getQueueDetails: '/organization/',
  rfInstances: '/instancemanagement/instances',
  getAdviceData: '/casemanagement/analyzetransactions/#organization/riskadvicedata',
  getSecondaryAuthData: '/casemanagement/analyzetransactions/#organization/secauthdata',
  getFraudStatus: '/casemanagement/analyzetransactions/#organization/fraudstatusdata',
  getChannels: '/casemanagement/analyzetransactions/#organization/channels',
  getRules: '/casemanagement/analyzetransactions/#organization/rules',
  getAccountTypes: '/organization/#organization/assignchannels/accountTypes',
  setSearchCriteria: '/casemanagement/analyzetransactions/#organization/search/channel/#channelId',
  getRuleimbunes: '/organization/#organization/rulesandscore/ruleimbunes',
  postTransactions: '/casemanagement/analyzetransactions/sumbit',
  analyzeTransDetails: '/casemanagement/analyzetransactions/details',
  showRelatedTrans: '/casemanagement/analyzetransactions/relatedtransactions',
  exportAnalyzeTrans: '/casemanagement/analyzetransactions/export',
  deviceExport: '/reports/devicesummary/export',
  getUserAction: '/case/manage/inboundcalls/useraction',
  getInboundCase: '/case/manage/inboundcalls',
  getMoreCaseNotes: '/case/manage/inboundcalls/morecasenotes',
  transactionsummary: '/case/manage/inboundcalls/transactionsummary',
  saveCase: '/case/save',
  getChannelTransactions: '/case/manage/inboundcalls/transactions',
  getCaseByCaseId: '/case/manage/inboundcalls/#caseID',
  falsePositive: '/reports/casemanagement/falsePositive/display',
  falsePositiveReport: '/reports/casemanagement/falsePositive/export',
  riskEvaluationDisplay: '/reports/riskAnlytics/detailactivity/display',
  riskEvaluationExport: '/reports/riskAnlytics/detailactivity/export',
  checkDataSetList: '/reports/riskanalytics/rulesdata/checkdataset',
  markAsInvestigation: '/casemanagement/analyzetransactions/raiseAlert'
};

export const RA_API_SSCONFIG_MISSCONFIG = {
  riskfortConfig: '/SYSTEM/riskfortconfig',
  channelId: '?channelId=0'
};
export const RA_API_CREATE_CUSTOM_ROLE = {
  'getCustomRoles': '/predefinedroles',
  'createCustomRole': '/customroles'
}

export const RA_API_UPDATECUSTOMROLE = {
  'getCustomRoles': '/customroles',
  'upateCustomRole': '/customroles',
  'getAllPredefinedRoles': '/predefinedroles'
}
export const RA_API_LOGIN = {
  'usersUrl': '/users',
  'bamloginUrl': '/bamlogin',
  'adminUrl': '/auth/login',
  'changepasswordUrl': '/auth/password',
  'orgUrl': '/auth/organization'
}


export const RA_API_CREATE_ADMIN = {
  'getOrgUrl': '/organization',
  'createAdminStepOneUrl': '/admin/create/validate/stepone',
  'createAdminStepTwoUrl': '/admin',
  'getRoles': '/manageableroles'
}

export const RA_API_VIEW_BULK_REQUEST = {
  'getBulkRequestStatus': '/organization/bulkupload/status',
  'getBulkRequestDetails': '/organization/bulkupload/operations',
  'searchRequests': '/bulkupload/searchrequests',
  'viewRequests': '/bulkupload/viewrequestdetails',
  'displayTasks': '/bulkupload/displaytasks',
  'viewTasksDetails': '/bulkupload/viewtaskdetails',
  'exportTasks': '/bulkupload/exporttasks',
  'refreshCache': '/cacherefreshconfig',
  'getLastRequestIds': '/cacherefreshstatus/requestids',
  'searchCacheDetails': '/cacherefreshstatus/search/',
  'getUdsConnectivityDetails': '/udsconfig/connectivity',
  'getUdsContactDetails': '/udsconfig/globalcontacttypes',
  'updateUdsContactDetails': '/udsconfig/globalcontacttypes',
  'udsConnectivityConfig': '/udsconfig/connectivity',
  'customizereport': '/customizereport',
  'operationsUrl': '/organization/bulkupload/operations',
  'searchUrl': '/org/searchOrg',
  'downloadOrganizationCSV': '/organization/downloadorganizationcsv',
  'exportOrganizations': '/organization/exportorganizations',
  'datafeed': '/datafeed',
  'queryParams': '?actionid=-1&category=ECMDF&sortorder=ASC',
  'getAuthMechanisms': '/organization/authmechanisms',
  'getRepositoryTypes': '/organization/repositorytypes',
  'getMfaMethods': '/organization/mfamethods',
  'getMfaTypes': '/organization/mfatypes',
  'createOrg': '/organization',
  'status': '/organization/status',
  'getOrganizationUrl': '/organizations/',
  'getOrgRules': '/rulelist',
  'GetListOfRules': '/rulelist/all',
  'createAdminStepTwoUrl': '/admin',
  'getRoles': '/manageableroles',
  'sharableOrg': '/rulelist/shareableorganizations',
  'parentOrg': '/rulelist/parentorgList',
  'getDatafeedEndpoints': '/datafeed/endpoints',
  'getEndpointConfiguration': '/datafeed/endpointconfig',
  'insertDataFeedConfig': '/datafeed/configuration',
  'getEndpointConfigurationOnAdd': '/datafeed/endpointnewconfig',
  'viewBulkRequests': '/org/searchOrg/basicOrg/view-request-details',

  'exportTasks': '/bulkupload/exporttasks'
}

export const RA_API_REFRESH_CACHE = {
  'refreshCache': '/cacherefreshconfig'
}
export const RA_API_CACHEREFRESH = {
  'getLastRequestIds': '/cacherefreshstatus/requestids',
  'searchCacheDetails': '/cacherefreshstatus/search/'
}
export const RA_API_UDSCONFIG = {
  'getUdsConnectivityDetails': '/udsconfig/connectivity',
  'udsConnectivityConfig': '/udsconfig/connectivity'
}

export const RA_API_BULKUPLOAD = {
  'operationsUrl': '/organization/bulkupload/operations',
  'searchUrl': '/org/searchOrg'
}

export const RA_API_RISKANALYTICS_CONNECTIVITY = {
  'riskAnalyticsConnectivity': '/riskanalyticsconnectivity'
}

export const logoutUrl = '/adminlogout';
export const masteradminUrl = '/masteradminconsole';
export const bamloginUrl = '/bamlogin';

