import React from 'react';
import PropTypes from 'prop-types';
import './MiscCheckbox.scss';

const MiscCheckbox = (props) => (
    <div className="custom-control misc-custom-checkbox">
        {props.options.map((opt, index) => {
            let optid = opt + index;
            return (
                <label key={opt} className={props.lisActivated ? 'form-label capitalize col-sm-12 edl_checkbox' : 'form-label capitalize col-sm-12'} >
                    <input
                        className="form-checkbox custom-control-input"
                        id={optid}
                        name={props.setName}
                        onChange={props.controlFunc}
                        value={opt}
                        {...props.selectedOptions && { checked: props.selectedOptions ? props.selectedOptions.indexOf(opt) > -1 : false }}
                        {...props.defaultSelectedOptions && { defaultChecked: props.defaultSelectedOptions ? props.defaultSelectedOptions.indexOf(opt) > -1 : false }}
                        type={props.type}
                        disabled={props.disable} />
                    <label className="form-label custom-control-label" htmlFor={optid}>{opt}</label>
                </label>
            );
        })}
    </div>
);

MiscCheckbox.propTypes = {
    title: PropTypes.string,
    type: PropTypes.oneOf(['checkbox', 'radio']),
    setName: PropTypes.string,
    options: PropTypes.array.isRequired,
    selectedOptions: PropTypes.array,
    controlFunc: PropTypes.func
};

export default MiscCheckbox;