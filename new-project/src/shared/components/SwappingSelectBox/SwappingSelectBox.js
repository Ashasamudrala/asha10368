import React, { Component } from 'react';
import './SwappingSelectBox.css';
import ReactDOM from 'react-dom';

class SwappingSelectBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectOptions: [],
      optionDetails: [],
      setData: this.setData.bind(this)
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let flag = 0;
    if (prevState.optionDetails !== nextProps.optionDetails) {
      flag = 1;
    }
    if (prevState.selectOptions !== nextProps.selectOptions) {
      flag = 1;
    }
    prevState.setData();
    if (flag === 1) {
      return { optionDetails: nextProps.optionDetails, selectOptions: nextProps.selectOptions };
    }
    return null;
  }


  setData() {
    let node = ReactDOM.findDOMNode(this.refs['multiGrpSel']);
    let unavaliablePrivilegeNode =  ReactDOM.findDOMNode(this.refs['multiRightGrpSel']);
    if (node !== null) { 
      var options = [].slice.call(node.querySelectorAll('option'));
      for (var i = 0; i < options.length; i++) {
        if (options[i].selected) {
          options[i].selected = false;
        }
      }
    } 
    if(unavaliablePrivilegeNode !== null ) {
      var options = [].slice.call(unavaliablePrivilegeNode.querySelectorAll('option'));
      for (var i = 0; i < options.length; i++) {
        if (options[i].selected) {
          options[i].selected = false;
        }
      }
    }
  }

  handledoubleRightChange = (e) => {
    this.props.handledoubleRightChange();
  }

  handledoubleLeftChange = (e) => {
    this.props.handledoubleLeftChange();
  }
  handleRightChange = (e) => {
    let node = ReactDOM.findDOMNode(this.refs['multiRightGrpSel']);
    this.props.handleRightChange(node);
  }
  handleLeftChange = (e) => {
    let node = ReactDOM.findDOMNode(this.refs['multiGrpSel']);
    this.props.handleLeftChange(node);
  }

  render() {
    let optionDetails = this.state.optionDetails ? ( this.state.optionDetails.length === 0 ): true ; 
    let selectOptions = this.state.selectOptions ? ( this.state.selectOptions.length === 0 ): true ; 
    return (<div  className={`form-group row col-sm-12 swapping-list-parent ${this.props.disableSwapping ? 'disabledbutton':''}`}>
      <div className="swap-select">
        <div className="text-center available-header">{this.props.leftMenuName}</div>
        <select className="custom-select available-privilages" name="selectOptions" multiple="multiple" id="multiGrpSel" ref="multiGrpSel">
          {(optionDetails) ? <React.Fragment /> :
          this.props.optDetails ?
            Object.keys(this.state.optionDetails[0]).map((item, index) => (
              <optgroup label={item} id={item} key={index}>
                {this.state.optionDetails[0][item].map((data, i) => (
                  <option title={data.headerName} value={JSON.stringify(data)} key={i} id={i} disabled={  typeof data.disabled === "undefined"? false :  data.disabled}>{data.displayName}</option>
                ))}
              </optgroup>
            )) : 
            Object.keys(this.state.optionDetails).map((value ,index) => (
              <option title={value} value={value} key={value} id={''}>{this.state.optionDetails[value]}</option>
             ))
          }}

        </select>

      </div>
      <div className="col-sm-2">
        <ul className="arrow-group">
          <li ><div className="arrow_btn tertiary-btn" onClick={this.handleLeftChange} id="submit" > > </div></li>
          <li><div className="arrow_btn tertiary-btn" onClick={this.handledoubleRightChange}> >> </div></li>
          <li><div className="arrow_btn tertiary-btn" onClick={this.handleRightChange}> &#60;</div></li>
          <li><div className="arrow_btn tertiary-btn" onClick={this.handledoubleLeftChange}> &#60;&#60;</div></li>
        </ul>
      </div>
      <div className="swap-select">
        <div className="text-center unavailable-header">{this.props.rightMenuName}</div>
        <select className="custom-select unavailable-privilages" multiple id="multiRightGrpSel" ref="multiRightGrpSel">
          {selectOptions ? <React.Fragment /> :
          this.props.optDetails ?
            Object.keys(this.state.selectOptions[0]).map((item, index) => (
              <optgroup label={item} id={item} key={index}>
                {this.state.selectOptions[0][item].map((data, i) => (
                  <option title={data.headerName} value={JSON.stringify(data)} key={i} id={i} disabled={  typeof data.disabled === "undefined"? false :  data.disabled}>{data.displayName}</option>
                ))}
              </optgroup>
            )): Object.keys(this.state.selectOptions).map((value) => (
              <option title={value} value={value}  key={value} id={''}>{this.state.selectOptions[value]}</option>
             ))

          }}
        </select>
      </div>
    </div>);
  }
}
export default SwappingSelectBox;
