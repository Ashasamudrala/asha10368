import React from 'react';
import PropTypes from 'prop-types';
import './Select.scss';

const Select = (props) => (
  <div className="form-group dynamic-form-select">
    <div className="form-group row">
      {props.title ? <label className="col-sm-4 col-form-label">{`${props.title}`}{props.required ? <span>*</span> : ""}</label> : null}
      <div className="col-sm-8">
        <select
          name={props.name}
          {...props.multiple && { multiple: true }}
          {...props.selectedOption && { value: props.selectedOption }}
          {...props.defaultOption && { defaultValue: props.defaultOption }}
          onChange={props.controlFunc}
          disabled={props.disabled}
          className="form-select form-control"
          style={{ 'background': `url(${process.env.REACT_APP_ROUTER_BASE || ''}/images/dropdown.png) no-repeat 97% 50%` }}
        >
          {(props.placeholder) ? <option value="">{props.placeholder}</option> : <React.Fragment />}
          {props.options.map(opt => {
            return (
              <option
                key={opt.key}
                value={opt.key}>{opt.displayContent ? opt.displayContent : opt.content}</option>
            );
          })}
        </select>
      </div>
    </div>
  </div>
);

Select.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOption: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.number
  ]),
  controlFunc: PropTypes.func,
  placeholder: PropTypes.string
};

export default Select;  