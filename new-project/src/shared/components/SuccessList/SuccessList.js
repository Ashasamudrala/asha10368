import React, { Component } from 'react';
import './SuccessList.css';
class SuccessList extends Component {
  render() {
    const displayList = this.props.validationData.message ? this.props.validationData.message : '';
    return (
      <div>
        {displayList !== '' ?
          <div className="success-parent">
            <div className="edl_success_feedback" id="successMessageTable">
              <div className="success-list">
                {displayList}
              </div>
            </div>
          </div>
          : ''
        }
      </div>
    )
  }
}

export default SuccessList;
