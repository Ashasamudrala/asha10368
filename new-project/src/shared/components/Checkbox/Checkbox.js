import React from 'react';
import PropTypes from 'prop-types';
import './Checkbox.css';

const Checkbox = (props) => (
  <div className="custom-control custom-checkbox">
    {props.options.map(opt => {
      return (
        <label key={opt} className={props.lisActivated ? 'form-label capitalize col-sm-12 edl_checkbox' : 'form-label capitalize col-sm-12'} >
          <input
            className="form-checkbox custom-control-input"
            id={opt.replace(/ /g, "_")}
            name={props.setName}
            onChange={props.controlFunc}
            value={opt}
            {...props.miscChecked && { checked: props.miscChecked }} // This prop is used for miscellaneous configurations.
            {...props.selectedOptions && { checked: props.selectedOptions ? props.selectedOptions.indexOf(opt) > -1 : false }}
            {...props.defaultSelectedOptions && { defaultChecked: props.defaultSelectedOptions ? props.defaultSelectedOptions.indexOf(opt) > -1 : false}}
            type={props.type}
            disabled={props.disable} />
          <label className="form-label custom-control-label" htmlFor={opt.replace(/ /g, "_")}>{opt}</label>
        </label>
      );
    })}
  </div>
);

Checkbox.propTypes = {
  title: PropTypes.string,
  type: PropTypes.oneOf(['checkbox', 'radio']),
  setName: PropTypes.string,
  options: PropTypes.array.isRequired,
  selectedOptions: PropTypes.array,
  controlFunc: PropTypes.func
};

export default Checkbox;