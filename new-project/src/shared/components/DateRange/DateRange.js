import React, { Component } from 'react';
import Select from '../Select/Select';
import DatePicker from "react-datepicker";
import { httpCall } from '../../utlities/RestAPI';
import { RA_API_URL } from '../../utlities/constants';
import CA_Utils from '../../utlities/commonUtils';
import { RA_STR } from '../../utlities/messages';
import "react-datepicker/dist/react-datepicker.css";
import './DateRange.css';
import ReactDOM from "react-dom";
var moment = require('moment');

class TimePreiod extends Component {
  constructor() {
    super();
    this.state = {
      dateRangeOptions: [],
      startDate: new Date(),
      endDate: new Date(),
      currentYear: new Date(),
      disableInput: false,
      getSelectedText: '',
      selectedDateRange: 'CUSTOM'
    }
    this.openCalendar = this.openCalendar.bind(this);
  }
  componentDidMount = async () => {
    const getElementsList = await httpCall('GET', RA_API_URL['getTimePeriods']);
    if (getElementsList && getElementsList.status === 200 && getElementsList.data.dateRange) {
      let dateRangeOptions = CA_Utils.objToArray(getElementsList.data.dateRange, 'object');
      dateRangeOptions.forEach(obj => {
        if (RA_STR.customDateStrings && RA_STR.customDateStrings[obj.content]) {
          obj.displayContent = RA_STR.customDateStrings[obj.content];
        }
      })
      this.setState({
        dateRangeOptions,
        getSelectedText: CA_Utils.objToArray(getElementsList.data.dateRange, 'object')[0].content
      })
    }
  }
  startDateChange = (date) => {
    this.setState({
      startDate: date
    }, () => {
      this.handleDateChange('', this.state.getSelectedText);
    });
  }
  endDateChange = (date) => {
    this.setState({
      endDate: date
    }, () => {
      this.handleDateChange('', this.state.getSelectedText);
    });
  }
  openCalendar(calendarType) {
    const currentDom = ReactDOM.findDOMNode(this);
    if (calendarType && currentDom && currentDom.querySelector(calendarType)) {
      currentDom.querySelector(calendarType).click();
    }
    this.handleDateChange('', this.state.getSelectedText);
  }
  handleDateChange = (event, getHandleDate) => {
    let modifyStartDate = this.state.startDate;
    let modifyEndDate = this.state.endDate;
    let getSelectedText = '';
    let value;
    if (event) {
      value = event.target.value;
    }
    let { dateRangeOptions } = this.state;
    if (value && dateRangeOptions) {
      let data = dateRangeOptions.filter(obj => obj.key == value)[0];
      if (data) {
        getSelectedText = data.content;
        this.setState({ getSelectedText });
      }
    } else if (getHandleDate) {
      getSelectedText = getHandleDate;
    }
    if (getSelectedText !== RA_STR.CUSTOM) {
      this.setState({ disableInput: true });
    } else {
      this.setState({ disableInput: false });
    }
    this.setState({ selectedDateRange: getSelectedText });
    switch (getSelectedText) {
      case RA_STR.today:
        modifyStartDate = new Date();
        modifyEndDate = new Date();
        break;
      case RA_STR.currentWeek:
        modifyStartDate = moment().startOf('week').toDate();
        break;
      case RA_STR.currentMonth:
        modifyStartDate = moment().startOf('month').toDate();
        break;
      case RA_STR.currentQuarter:
        modifyStartDate = moment().startOf('quarter').toDate();
        break;
      case RA_STR.lastWeek:
        modifyStartDate = moment().add(-1, 'week').startOf('week').toDate();
        modifyEndDate = moment().add(-1, 'week').endOf('week').toDate();
        break;
      case RA_STR.lastMonth:
        modifyStartDate = moment().add(-1, 'month').startOf('month').toDate();
        modifyEndDate = moment().add(-1, 'month').endOf('month').toDate();
        break;
      case RA_STR.lastQuarter:
        modifyStartDate = moment().add(-1, 'quarter').startOf('quarter').toDate();
        modifyEndDate = moment().add(-1, 'quarter').endOf('quarter').toDate();
        break;
      case RA_STR.last6Months:
        modifyStartDate = moment().subtract(6, 'months').add(1, 'days').toDate();
        modifyEndDate = new Date();
        break;
    }
    this.setState({ startDate: modifyStartDate, endDate: modifyEndDate });
  }
  getDates() {
    let groupDates = [];
    var obj = {
      startDate: moment(this.state.startDate).format('L'),
      endDate: moment(this.state.endDate).format('L'),
      getSelectedText: this.state.getSelectedText,
      dateRange: this.state.selectedDateRange,
    }
    groupDates.push(obj);
    return groupDates
  }
  render() {
    let { dateRangeOptions, startDate, endDate, disableInput } = this.state;
    return <div className="col-sm-9 Dateranger">
      <div className="row">
        <div className="col-sm-4">
          <Select
            name={'req-id'}
            title={'Date Range'}
            options={dateRangeOptions}
            controlFunc={this.handleDateChange}
          />
        </div>
        <div className="col-sm-8">
          <div>
            <label className="col-sm-3 col-form-label">From (MM/dd/yyyy)</label>
            <DatePicker
              selected={startDate}
              todayButton="today"
              onChange={this.startDateChange}
              showMonthDropdown={true}
              showYearDropdown={true}
              scrollableYearDropdown={false}
              disabled={disableInput}
              onYearChange={this.startDateChange}
              minDate={moment(startDate).add(-2, 'year').startOf('year').toDate()}
              maxDate={moment(startDate).add(+2, 'year').startOf('year').toDate()}
              dropdownMode="select"
              className="form-control startDate"
              ref={input => this.inputElement = input}
            /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind" onClick={() => this.openCalendar('.startDate')} />
          </div>
          <div>
            <label className="col-sm-3 col-form-label">To (MM/dd/yyyy)</label>
            <DatePicker
              selected={endDate}
              todayButton="today"
              onChange={this.endDateChange}
              showMonthDropdown={true}
              showYearDropdown={true}
              scrollableYearDropdown={false}
              disabled={disableInput}
              dropdownMode="select"
              onYearChange={this.endDateChange}
              minDate={moment(endDate).add(-2, 'year').startOf('year').toDate()}
              maxDate={moment(endDate).add(+2, 'year').startOf('year').toDate()}
              className="form-control endDate"
            /> <img src="images/calendarButton.gif" className="calendarDate" alt="rrewind" onClick={() => this.openCalendar('.endDate')} />
          </div>
        </div>
      </div>
    </div>
  }
}

export default TimePreiod;