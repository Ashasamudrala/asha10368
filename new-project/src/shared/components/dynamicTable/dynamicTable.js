import React, { Component } from 'react';
import MaterialTable from "material-table";

import Button from '@material-ui/core/Button';
import Pagination from "react-js-pagination";
import './dynamicTable.css';

class DynamicTable extends Component {
  constructor(props) {
    super(props);
  }

  handlePageChange = (pageNumber) => {
    this.props.activePage(pageNumber);
  }
  render() {
    const activePage=this.props.activePageNumNew;
    const columns=this.props.columns;
    const data=this.props.data;
    const selection=this.props.selection;
    const pageSize=this.props.pageSize;
    const totalCount=this.props.totalCount;
    const title=this.props.title;
    const detailPanel=this.props.detailPanel;
    return (
      [<span className="float-left">{title}</span>,<div className="float-right">{this.props.enablePagination && totalCount > 10? <div> <span className="search-number">Results {activePage * pageSize - pageSize + 1}<span className="sepertion">-</span>{activePage * pageSize - pageSize + (data ? data.length:0)} <span className="sepertion"> of </span>{totalCount}</span><Pagination
      hideDisabled
      firstPageText={<img src="images/rrewind.gif" alt="rrewind"></img>}
      lastPageText={<img src="images/fforward.gif" alt="fforward"></img>}
      prevPageText={<img src="images/rewind.gif" alt="rewind"></img>}
      nextPageText={<img src="images/forward.gif" alt="forward"></img>}
      activePage={activePage}
      itemsCountPerPage={pageSize}
      totalItemsCount={totalCount}
      pageRangeDisplayed={20}
      onChange={this.handlePageChange}
    /></div>:''}</div>,
      <div style={{ maxWidth: '100%',clear:'both'}}>
        <MaterialTable
          columns={columns}
          data={data}
          options={{
            selection:selection,
            paging: false,
            search: false,
            sorting: false,
          }}
          onSelectionChange={(rows) => this.props.handleCheckboxSelection(rows)}
          detailPanel={detailPanel}
        />
      </div>]
    )
  }
}
export default DynamicTable;
