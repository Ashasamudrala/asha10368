import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import './Autosuggest.css';
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
function getSuggestionValue(suggestion) {
  return suggestion;
}

function shouldRenderSuggestions() {
  return true;
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion}</span>
  );
}
class DynamicAutosuggest extends Component {
  constructor(props) {
    super(props);
    let value = '';
    if (props.value) {
      value = props.value;
    }
    this.state = {
      value: value,
      maxpageDisplaySize: 500,
      suggestions: [],
      noSuggestions: false,
      activePage: 1,
      message: '',
      orgOptions: [],
      enableAutosuggest: false,
      defaultOrganizationPlaceholder: '--Select--'
    };
    this.lastRequestId = null;
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    let flag = 0;
    if (prevState.orgOptions !== nextProps.orgOptions) {
      flag = 1;
    }
    if (prevState.enableAutosuggest !== nextProps.enableAutosuggest) {
      flag = 1;
    }
    if (prevState.defaultOrganizationPlaceholder !== nextProps.defaultOrganizationPlaceholder) {
      flag = 1;
    }
    if (flag === 1) {
      return { orgOptions: nextProps.orgOptions, enableAutosuggest: (nextProps.enableAutosuggest), defaultOrganizationPlaceholder: nextProps.defaultOrganizationPlaceholder };
    }
    return null;
  }
  onBlur = (event) => {
    if (event && event.target && event.target.value) {
      let trimmedValue = event.target.value.trim();
      this.props.getSelectedOrgDetails(trimmedValue, 'blur');
    }
  }
  setvalue(value) {
    this.setState({ value })
  }
  onChange = (event, { newValue, method }) => {
    this.setState({
      value: newValue
    });
    this.props.getSelectedOrgDetails(newValue, method);
  };
  getSuggestions = (value = '') => {
    if (this.state.displayLimit > this.state.totalDisplay) {
      return
    }
    const escapedValue = escapeRegexCharacters(value.trim());
    const regex = new RegExp('^' + escapedValue, 'i');
    return this.state.orgOptions.slice((this.state.activePage - 1) * this.state.maxpageDisplaySize, this.state.activePage * this.state.maxpageDisplaySize).filter(language => regex.test(language));
  }
  onSuggestionsFetchRequested = ({ value }) => {
    var suggestions = this.getSuggestions(value);
    if (suggestions.length !== 0) {
      this.setState({
        suggestions: suggestions
      });
    } else {
      const isInputBlank = value.trim() === '';
      if (!isInputBlank && suggestions.length === 0) {
        var getFilteredItems = this.state.orgOptions.filter(function (event) {
          return event.toLowerCase().indexOf(value) !== -1;
        })
        this.setState({
          suggestions: getFilteredItems
        });
      }
    }
  };
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };
  updateList = (pageNumber) => {
    if (pageNumber < 1 || pageNumber > Math.ceil(this.state.orgOptions.length / this.state.maxpageDisplaySize)) {
      return false
    } else {
      this.setState({
        activePage: pageNumber
      }, function () {
        this.setState({ suggestions: this.getSuggestions() })
      })
    }
  }
  renderSuggestionsContainer = ({ containerProps, children, query }) => (
    <div {...containerProps}>
      {children}
      {this.state.suggestions.length >= 500 ?
        <div className="autosuggest-footer">
          {
            this.state.activePage !== 1
            && <button className={this.state.activePage === 1 ? 'btn btn-link disabled' : 'btn btn-link'} onClick={() => this.updateList(this.state.activePage - 1)}>Back</button>
          }
          <button className={this.state.activePage === Math.ceil(this.state.orgOptions.length / this.state.maxpageDisplaySize) ? 'blue float-right btn btn-link disabled' : 'blue float-right btn btn-link'} onClick={() => this.updateList(this.state.activePage + 1)}>More..</button>
        </div> : ''
      }
    </div>
  );
  render() {
    const { value, suggestions, noSuggestions, defaultOrganizationPlaceholder } = this.state;
    let enableAutosuggest = !(this.state.enableAutosuggest);
    const inputProps = {
      placeholder: defaultOrganizationPlaceholder ? defaultOrganizationPlaceholder : '--Select--',
      value,
      disabled: enableAutosuggest,
      onChange: this.onChange,
    };
    if (this.props.hasOwnProperty('value')) {
      inputProps.onBlur = this.onBlur
    }
    return (
      [<Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        shouldRenderSuggestions={shouldRenderSuggestions}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
        renderSuggestionsContainer={this.renderSuggestionsContainer}
      />,
      noSuggestions &&
      <div className="no-suggestions">
        No suggestions
          </div>]
    );
  }
}
export default DynamicAutosuggest;
