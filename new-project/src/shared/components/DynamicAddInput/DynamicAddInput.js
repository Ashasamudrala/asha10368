import React from 'react';
import './DynamicAddInput.css';

class DynamicAddInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expandInputs: false
    }
  }
  componentWillReceiveProps(){
    if (this.props.expandInputs) {
      this.setState({expandInputs:true})
    }
  }
  expandInputs = () => {
    this.setState({ expandInputs: !this.state.expandInputs });
  }
  render() {
    var onProductTableUpdate = this.props.onProductTableUpdate;
    var rowDel = this.props.onRowDel;
    var completeProducts = this.props.products;
    var product = this.props.products.map(function (product) {
      return (<DynamicAddInputRow onProductTableUpdate={onProductTableUpdate} product={product} onDelEvent={rowDel.bind(this)} data={completeProducts} key={product.id} />)
    });
    return (
      <div className="row"><span className="expand-inputs" onClick={this.expandInputs}>{this.state.expandInputs ? '[-]' : '[+]'}</span>
        <span>Custom Attributes</span>
        {this.state.expandInputs ?
          <div className="no-padding">
            <table className="dynamic-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Value</th>
                  <th onClick={this.props.onRowAdd}>
                    <img alt="" src="images/plus.jpg" className="pull-right plus-icon" />
                  </th>
                </tr>
              </thead>
              <tbody>
                {product}
              </tbody>
            </table></div> : ''}
      </div>
    );
  }
}
class DynamicAddInputRow extends React.Component {


  onDelEvent() {
    this.props.onDelEvent(this.props.product);
  }
  render() {
    return (
      <tr className="eachRow">
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "name",
          value: this.props.product.name,
          id: this.props.product.id,
        }} />
        <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
          type: "value",
          value: this.props.product.value,
          id: this.props.product.id
        }} />
        <td className="del-cell">
          <img alt="" src={this.props.data.length === 1 ? "images/cross_disabled.gif" : "images/cross_enabled.gif"} onClick={this.onDelEvent.bind(this)} className="del-btn" />
        </td>
      </tr>
    );
  }
}
class EditableCell extends React.Component {
  constructor(props) {
    super();
    this.state = {
      inputvalue: props.cellData.value,
    }
    this.handlechange = this.handlechange.bind(this)
  }
  handlechange(e) {
    this.setState({ inputvalue: e.target.value })
    this.props.onProductTableUpdate(e)
  }
  render() {
    return (
      <td>
        <input className="form-control" type='text' name={this.props.cellData.type} id={this.props.cellData.id} value={this.state.inputvalue} onChange={this.handlechange} />
      </td>
    );
  }
}
export default DynamicAddInput;
