import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import '../../../components/createAdminOrgDetails/createAdminOrgDetails.scss'

class SwappableGrid extends Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
        this.rightTextInput = React.createRef();
        this.state = {
            scopeAll: false,
            prevLeftArray: [],
            prevRightArray: [],
            leftStaticArray: [],
            rightStaticArray: [],
            leftItems: [],
            rightItems: [],
            leftArrow: false,
            rightArrow: false,
            leftSearch: false,
            rightSearch: false,
            selectedLength: '',
            selectedRightLength: '',
            selectedOrganizations: []
        }
    }
    componentWillReceiveProps(nextProps) {
        let updateStateObj = {};
        let { leftItems, rightItems } = this.state;
        if (nextProps.leftItems && Object.keys(nextProps.leftItems).length > 0 && leftItems.length === 0) {
            updateStateObj.leftItems = nextProps.leftItems;
            updateStateObj.leftStaticArray = nextProps.leftItems;
        }
        if (nextProps.rightItems && Object.keys(nextProps.rightItems).length > 0 && rightItems.length === 0) {
          let rightUpdated= JSON.parse(JSON.stringify(nextProps.rightItems));
            if (Array.isArray(nextProps.rightItems)) {
                updateStateObj.rightItems =rightUpdated;
            } else {
                updateStateObj.rightItems = Object.keys(rightUpdated);
            }
            updateStateObj.rightStaticArray = nextProps.rightItems;
        }
        updateStateObj.scopeAll = nextProps.scopeAll;
        this.setState(updateStateObj);
    }
    setLeftItems(leftItemsObj) {
        this.setState(leftItemsObj)
    }
    setRightItems(RightItemsObj) {
        this.setState(RightItemsObj)
    }
    getRightItems() {
        return this.state.rightItems;
    }
    getLeftItems() {
        return this.state.leftItems;
    }
    _handleRightKeyPress = (e, searchIconPressed) => {
        let searchInput = this.rightTextInput.current.value;
        var updatedList = this.state.rightStaticArray;
        if (searchInput !== "" && searchInput && updatedList) {
            if (e.key === 'Enter' || searchIconPressed) {
                updatedList = Object.values(updatedList).filter(function (itemList) {
                    if (itemList) {
                        return itemList.toLowerCase().search(
                            searchInput.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ']').toLowerCase()) !== -1;
                    }
                });
                this.setState({ prevRightArray: this.state.rightItems, rightItems: updatedList, rightSearch: true });
            }
        } else {
            this.setState({ prevRightArray: this.state.rightItems, rightItems: this.state.rightStaticArray, rightSearch: true });
        }
    }
    _handleKeyPress = (e, searchIconPressed) => {
        let searchInput = this.textInput.current.value;
        var updatedList = this.state.leftStaticArray;
        this.state.leftStaticArray = JSON.parse(JSON.stringify(this.state.leftStaticArray));
        if (searchInput !== "" && searchInput) {
            if (e.key === 'Enter' || searchIconPressed) {
                updatedList = Object.values(updatedList).filter(function (itemList) {
                    if (itemList) {
                        return itemList.toLowerCase().search(
                            searchInput.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ']').toLowerCase()) !== -1;
                    }
                });
                this.setState({ prevLeftArray: this.state.leftStaticArray, leftItems: updatedList, leftSearch: true });
            }
        } else {
            this.setState({ prevLeftArray: this.state.leftItems, leftItems: this.state.leftStaticArray, leftSearch: true });
        }
    }
    _handleRightDoubleChange = () => {
        this.textInput.current.value = '';
        let leftItems = [];
        let node = ReactDOM.findDOMNode(this.refs['multiGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        if(options.length > 100 && this.props.checkMaxLimit){
         alert('You cannot select more than 100 organizations.');
         return false;
        }
        console.log('options',options);
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                options[i].selected = false;
            }
        }

        if (this.state.leftSearch === true) {
            for (let i = 0; i < this.state.prevLeftArray.length; i++) {
                let flag = 0;
                for (let j = 0; j < this.state.leftItems.length; j++) {
                    if (this.state.prevLeftArray[i].item === this.state.leftItems[j].item) {
                        flag = 1;
                    }
                }
                if (flag === 0) {
                    leftItems.push(this.state.prevLeftArray[i])
                }
            }
        }
        for (let i = 0; i < Object.keys(this.state.leftItems).length; i++) {
            this.state.rightItems.push(Object.values(this.state.leftItems)[i]);
            delete this.state.prevLeftArray[Object.values(this.state.leftItems)[i]];
        } {
            (this.state.leftSearch) ? this.setState({ rightItems: this.state.rightItems, leftItems: this.state.prevLeftArray, leftSearch: false, rightStaticArray: this.state.rightItems, leftStaticArray: this.state.prevLeftArray, rightArrow: (this.state.leftArrow), leftArrow: false }) : this.setState({ rightItems: this.state.rightItems, leftItems: [], rightStaticArray: this.state.rightItems, leftSearch: false, leftStaticArray: leftItems, leftArrow: false, rightArrow: (this.state.leftArrow) })
        }
        this._selectRightList();
    }
    _handleLeftDoubleChange = () => {
        this.rightTextInput.current.value = '';
        let rightItems = [];
        let node = ReactDOM.findDOMNode(this.refs['multiRightGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                options[i].selected = false;
            }
        }
        let { leftItems } = this.state;
        for (let i = 0; i < this.state.rightItems.length; i++) {
            leftItems[this.state.rightItems[i]] = this.state.rightItems[i];
        }
        this.setState({ leftItems });

        if (this.state.rightSearch === true) {
            for (let i = 0; i < this.state.prevRightArray.length; i++) {
                let flag = 0;
                for (let j = 0; j < this.state.rightItems.length; j++) {
                    if (this.state.prevRightArray[i].item === this.state.rightItems[j].item) {
                        flag = 1;
                    }
                }
                if (flag === 0) {
                    rightItems.push(this.state.prevRightArray[i])
                }
            }
        }
        {
            //(this.state.rightSearch) ? this.setState({ leftItems: this.state.leftItems, selectedOrganizations: selectedOrganizations, leftStaticArray: this.state.leftItems, rightSearch: false, rightStaticArray: selectedOrganizations, leftArrow: (this.state.rightArrow), rightArrow: false }) : this.setState({ leftItems: this.state.leftItems, selectedOrganizations: [], leftStaticArray: this.state.leftItems, rightSearch: false, rightStaticArray: selectedOrganizations, leftArrow: (this.state.rightArrow), rightArrow: false })
            (this.state.rightSearch) ? this.setState({ leftItems: this.state.leftItems, rightItems: rightItems, leftStaticArray: this.state.leftItems, rightSearch: false, rightStaticArray: rightItems, leftArrow: (this.state.rightArrow), rightArrow: false }) : this.setState({ leftItems: this.state.leftItems, rightItems: [], leftStaticArray: this.state.leftItems, rightSearch: false, rightStaticArray: rightItems, leftArrow: (this.state.rightArrow), rightArrow: false })
        }
        this._selectLeftList();

    }

    _selectLeftList = () => {
        let node = ReactDOM.findDOMNode(this.refs['multiGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });

        if (selected.length >= 1) {
            this.setState({ leftArrow: true })
        }
        this.setState({ selectedLength: selected.length })
    }
    _selectRightList = () => {
        let node = ReactDOM.findDOMNode(this.refs['multiRightGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });
        if (selected.length >= 1) {
            this.setState({ rightArrow: true })
        }
        this.setState({ selectedRightLength: selected.length })
    }
    _handleLeftChange = (e) => {
        this.rightTextInput.current.value = '';
        let node = ReactDOM.findDOMNode(this.refs['multiRightGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });

        var selectedValues = selected.map(function (option) {
            return { "value": option.value, "id": option.id, "text": option.text, "index": option.index };
        });
        if (this.state.rightSearch === true) {
            let { leftItems, prevRightArray, rightItems } = this.state;
            for (let i = 0; i < selectedValues.length; i++) {
                let value = selectedValues[i].text;
                rightItems = rightItems.filter(obj => {
                    return obj !== selectedValues[i].text
                })
                for (let j = 0; j < this.state.prevRightArray.length; j++) {
                    if (this.state.prevRightArray[j] == value) {
                        delete this.state.prevRightArray[value];
                        prevRightArray = prevRightArray.filter(obj => {
                            return obj != value;
                        })
                    }
                }
                leftItems[value] = value;
            }
            for (var i = 0; i < options.length; i++) {
                if (options[i].selected) {
                    options[i].selected = false;
                }
            }
            this.setState({ rightItems: prevRightArray, leftItems: leftItems, selectedOrganizations: prevRightArray, prevRightArray: prevRightArray, rightStaticArray: prevRightArray, leftStaticArray: leftItems, rightArrow: false });
            return;
        }
        let { rightItems, leftItems } = this.state;
        for (let i = 0; i < selectedValues.length; i++) {
            let value = selectedValues[i].text;
            rightItems = rightItems.filter(obj => {
                return obj !== selectedValues[i].text
            })
            leftItems[selectedValues[i].text] = selectedValues[i].text;
        }
        this.setState({ leftItems, rightItems });
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                options[i].selected = false;
            }
        }
        this.setState({ selectedOrganizations: this.state.selectedOrganizations, leftItems: this.state.leftItems, rightStaticArray: this.state.selectedOrganizations, leftStaticArray: this.state.leftItems, rightArrow: false });
    }
    _handleRightChange = (e) => {
        this.textInput.current.value = '';
        let node = ReactDOM.findDOMNode(this.refs['multiGrpSel']);
        var options = [].slice.call(node.querySelectorAll('option'));
        var selected = options.filter(function (option) {
            return option.selected;
        });

        var selectedValues = selected.map(function (option) {
            return { "value": option.value, "id": option.id, "text": option.text };
        });
        if (this.state.leftSearch === true) {
            let { rightItems } = this.state;
            for (let i = 0; i < selectedValues.length; i++) {
                let value = selectedValues[i].text
                for (let j = 0; j < Object.values(this.state.prevLeftArray).length; j++) {
                    if (Object.values(this.state.leftStaticArray)[j] === value) {
                        delete this.state.prevLeftArray[value];
                    }
                }
                rightItems.push(value);
            }
            for (var i = 0; i < options.length; i++) {
                if (options[i].selected) {
                    options[i].selected = false;
                }
            }
            this.setState({ selectedOrganizations: this.state.selectedOrganizations, leftItems: this.state.prevLeftArray, prevLeftArray: this.state.prevLeftArray, leftStaticArray: this.state.prevLeftArray, rightStaticArray: rightItems, leftArrow: false, rightItems });
            return;
        }
        let { rightItems, leftItems } = this.state;
        for (let i = 0; i < selectedValues.length; i++) {
            let value = selectedValues[i].value
            delete leftItems[selectedValues[i].value]
            rightItems.push(selectedValues[i].text);
        }
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                options[i].selected = false;
            }
        }
        this.setState({ rightItems, leftItems, leftStaticArray: this.state.leftStaticArray, rightStaticArray: rightItems, leftArrow: false });

    }
    render() {
        let staticContent = this.state.selectedLength + ' Selected';
        let staticRightContent = this.state.selectedRightLength + ' Selected';
        let staticNonContent = 'None Selected';
        return (
            <React.Fragment>
                <div className={"col-sm-3  " + (this.state.scopeAll ? 'disabled' : '')}>
                    <p>{this.props.leftHeader}</p>
                    <div className="fake-input">
                        <input type="text" placeholder="press enter to filter" className="form-control search-input" onKeyUp={this._handleKeyPress} ref={this.textInput} />
                        <img src="images/search.png" width="20" height="20" alt="no-img" onClick={(e) => this._handleKeyPress(e, 'search')} />
                    </div>
                    <div className={"selectMenu" + ((this.state.scopeAll ? 'disabled' : ''))}>

                        <select className="multiSelectOptionList" multiple ref="multiGrpSel" onClick={this._selectLeftList} onKeyUp={this._selectLeftList}>
                            {Object.keys(this.state.leftItems).map((value) => (
                                <option title={value} value={value} key={value} id={''}>{this.state.leftItems[value]}</option>
                            ))}
                        </select>
                    </div>
                    <span className="select-info">{!(this.state.leftArrow) ? (staticNonContent) : (staticContent)}</span>
                </div>
                <div className={"col-sm-1 " + (this.state.scopeAll ? 'disabled' : '')}>
                    <ul className="admin-arrow-group ">
                        <li >{(Object.keys(this.state.leftItems).length >= 1) ? <div onClick={this._handleRightDoubleChange} id="submit" > <img src='images/right-arrow-1.png' className="img-style " width="20" height="20" alt="no-img" /> </div> : <img src='images/right-arrow-1.png' className="img-style toggleBtnDis" width="20" height="20" alt="no-img" />} </li>
                        <li> {(this.state.leftArrow) ? <div onClick={this._handleRightChange}><img src='images/right-arrow.png' className="img-style " width="20" height="20" alt="no-img" /></div> : <img src='images/right-arrow.png' className="img-style toggleBtnDis" width="20" height="20" alt="no-img" />} </li>
                        {!this.props.hideLeftIcons ?
                        <span>
                        <li> {(this.state.rightArrow) ? <div onClick={this._handleLeftChange}><img src='images/left-arrow.png' className="img-style " width="20" height="20" alt="no-img" /> </div> : <img src='images/left-arrow.png' className="img-style toggleBtnDis" width="20" height="20" alt="no-img" />} </li>
                        <li> {(this.state.rightItems.length >= 1) ? <div onClick={this._handleLeftDoubleChange}><img src='images/left-arrow-1-grey.png' className="img-style " alt="no-img" width="20" height="20" /></div> : <img src='images/left-arrow-1-grey.png' className="img-style toggleBtnDis" width="20" height="20" alt="no-img" />}</li></span>:''
                        }
                    </ul>
                </div>
                <div className={"col-sm-4 " + (this.state.scopeAll ? 'disabled' : '')}>
                    <p>{this.props.rightHeader}</p>
                    <div className="fake-input">
                        <input type="text" placeholder="press enter to filter" className="form-control search-input" onKeyUp={this._handleRightKeyPress} ref={this.rightTextInput} />
                        <img src="images/search.png" width="20" height="20" alt="no-img" onClick={(e) => this._handleRightKeyPress(e, 'search')} />
                    </div>
                    <div className="selectMenu">
                        <select className="multiSelectOptionList" multiple id="multiRightGrpSel" ref="multiRightGrpSel" onClick={this._selectRightList} onKeyUp={this._selectRightList}>
                            {Object.keys(this.state.rightItems).map((value) => (
                                <option title={value} value={value} key={value} id={''}>{this.state.rightItems[value]}</option>
                            ))}
                        </select>
                    </div>
                    <span className="select-info">{!(this.state.rightArrow) ? (staticNonContent) : (staticRightContent)}</span>
                </div>
            </React.Fragment>
        )
    }
}

export default SwappableGrid;
