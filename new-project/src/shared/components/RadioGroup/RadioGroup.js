import React from 'react';
import PropTypes from 'prop-types';
import './RadioGroup.css';



const RadioGroup = (props) => (
  <div className="custom-control custom-radio">
    {props.options.map(opt => {
      const getLength = props.options.length;
      const getEachLength = 12 / getLength;
      return (
        <span key={opt} className={`radio capitalize col-md-${getEachLength}`}>
          <input
            className={`form-radio custom-control-input`}
            id={opt}
            name={props.setName}
            onChange={props.controlFunc}
            value={opt}
            checked={props.selectedOptions.indexOf(opt) > -1}
            type={props.type}
            disabled={props.disable} />
          <label className="" htmlFor={opt}>{opt}</label>
        </span>
      );
    })}
  </div>
);

RadioGroup.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['checkbox', 'radio']).isRequired,
  setName: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOptions: PropTypes.array,
  controlFunc: PropTypes.func
};

export default RadioGroup;