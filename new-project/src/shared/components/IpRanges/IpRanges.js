import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './IpRanges.scss';
import ReactDOM from 'react-dom';
import { RA_STR } from '../../utlities/messages';

class IpRanges extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIPOption: 'mask',
            ipAddr1: '',
            ipAddr2: '',
            ipAddr3: '',
            ipAdd4: '',
            mask1: '',
            mask2: '',
            mask3: '',
            mask4: '',
            endIpAddr1: '',
            endIpAddr2: '',
            endIpAddr3: '',
            endIpAddr4: '',
            maskOrEndIPSelection: ''
        }
    };

    handleRadioChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    shownextelement = (ref, e) => {
        this.setState({ [e.target.name]: e.target.value });
        if (ref && e.target.value.length === 3) {
            let node = ReactDOM.findDOMNode(this.refs[ref]);
            node.focus();
        }
    }

    returnIpAddress = () => {
        return this.state;
    }

    showifnextelement = (selectedIPOption, mask1, endIpAddress1, e) => {
        this.setState({ [e.target.name]: e.target.value });
        if (e.target.value.length === 3) {
            let node = selectedIPOption === 'mask' ?
                ReactDOM.findDOMNode(this.refs[mask1]) : ReactDOM.findDOMNode(this.refs[endIpAddress1]);
            node.focus();
        }
    }

    render() {
        const { ipAddr1, ipAddr2, ipAddr3, ipAdd4, mask1, mask2, mask3, mask4, endIpAddr1, endIpAddr2, endIpAddr3, endIpAddr4, selectedIPOption } = this.state;
        return (
            <div className="ip-range">
                <div className="form-group">
                    <label className="mr-20"> {RA_STR.ipAddress} </label>
                    <input type="text" ref="ipAddr1" name="ipAddr1" maxLength="3" size="3" value={ipAddr1} onChange={this.shownextelement.bind(this, 'ipAddr2')} /> <b>.</b>
                    <input type="text" ref="ipAddr2" name="ipAddr2" maxLength="3" size="3" value={ipAddr2} onChange={this.shownextelement.bind(this, 'ipAddr3')} /> <b>.</b>
                    <input type="text" ref="ipAddr3" name="ipAddr3" maxLength="3" size="3" value={ipAddr3} onChange={this.shownextelement.bind(this, 'ipAdd4')} /> <b>.</b>
                    <input type="text" ref="ipAdd4" name="ipAdd4" maxLength="3" size="3" value={ipAdd4} onChange={this.showifnextelement.bind(this, selectedIPOption, 'mask1', 'endIpAddr1')} />
                </div>

                <div className="form-group">
                    <input type="radio" name="selectedIPOption" value='mask' checked={selectedIPOption === 'mask'} onChange={this.handleRadioChange} />
                    <label> {RA_STR.subnetMask} </label>
                    <input type="text" ref="mask1" name="mask1" maxLength="3" size="3" value={mask1} disabled={selectedIPOption !== 'mask'} onChange={this.shownextelement.bind(this, 'mask2')} /> <b>.</b>
                    <input type="text" ref="mask2" name="mask2" maxLength="3" size="3" value={mask2} disabled={selectedIPOption !== 'mask'} onChange={this.shownextelement.bind(this, 'mask3')} /> <b>.</b>
                    <input type="text" ref="mask3" name="mask3" maxLength="3" size="3" value={mask3} disabled={selectedIPOption !== 'mask'} onChange={this.shownextelement.bind(this, 'mask4')} /> <b>.</b>
                    <input type="text" ref="mask4" name="mask4" maxLength="3" size="3" value={mask4} disabled={selectedIPOption !== 'mask'} onChange={this.shownextelement.bind(this, '')} />
                </div>

                <div className="form-group">
                    <input type="radio" name="selectedIPOption" value='endip' checked={selectedIPOption === 'endip'} onChange={this.handleRadioChange} />
                    <label> {RA_STR.endIPAddress}  </label>
                    <input type="text" ref="endIpAddr1" name="endIpAddr1" maxLength="3" size="3" value={endIpAddr1} disabled={selectedIPOption !== 'endip'} onChange={this.shownextelement.bind(this, 'endIpAddr2')} /> <b>.</b>
                    <input type="text" ref="endIpAddr2" name="endIpAddr2" maxLength="3" size="3" value={endIpAddr2} disabled={selectedIPOption !== 'endip'} onChange={this.shownextelement.bind(this, 'endIpAddr3')} /> <b>.</b>
                    <input type="text" ref="endIpAddr3" name="endIpAddr3" maxLength="3" size="3" value={endIpAddr3} disabled={selectedIPOption !== 'endip'} onChange={this.shownextelement.bind(this, 'endIpAddr4')} /> <b>.</b>
                    <input type="text" ref="endIpAddr4" name="endIpAddr4" maxLength="3" size="3" value={endIpAddr4} disabled={selectedIPOption !== 'endip'} onChange={this.shownextelement.bind(this, '')} />
                </div>
            </div>);
    }
}

export default IpRanges;