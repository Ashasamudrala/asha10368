import React, { Component } from 'react';
import './TrustedIpRanges.scss';
import { RA_STR } from '../../utlities/messages';

import _ from 'underscore';

class TrustedIpRanges extends Component {
    constructor(props) {
        super(props);
        this.state = {
            parentIpArray: [],
            constructedArray: []
        }
    };

    updateIpRanges() {
        let newArray = [];
        if (this.props && this.props.data) {
            newArray = this.props.data;
            this.setState({ parentIpArray: newArray });
        }
    }

    handleChange = (getKey, e) => {
        const { parentIpArray } = this.state;
        const name = e.target.name.split('_')[0];
        parentIpArray[getKey][name] = e.target.value;
        this.setState({ parentIpArray: parentIpArray });
    }

    checkSelection = (trustedIPSeqID, e) => {
        let { parentIpArray, constructedArray } = this.state;
        if (e.target.checked) {
            constructedArray.push(_.findWhere(parentIpArray, { trustedIPSeqID: trustedIPSeqID }))
            this.setState({ constructedArray: constructedArray });
        } else {
            constructedArray = _.without(constructedArray, _.findWhere(constructedArray, { trustedIPSeqID: trustedIPSeqID }));
            this.setState({ constructedArray: constructedArray });
        }
    }

    getChangedIp = (action) => {
        const changedIps = this.state.constructedArray;
        changedIps.forEach(obj => {
            if (this.checkEmpty(obj.mask1, obj.mask2, obj.mask3, obj.mask4)) {
                obj.maskOrEndIPSelection = "endip"
            } else {
                obj.maskOrEndIPSelection = "mask"
            }
        })
        this.setState({ constructedArray: changedIps }, () => { this.props.getIpData(action, this.state.constructedArray) });
    }

    checkEmpty(a, b, c, d) {
        return (a === '' && b === '' && c === '' && d === '') ? true : false
    }

    render() {
        const { parentIpArray } = this.state;
        return (
            <div className="col-sm-10 trusted-ip-ranges">
                <p> <b> {RA_STR.trustedIPList} </b> </p>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th> {RA_STR.select} </th>
                            <th> {RA_STR.ipAddress} </th>
                            <th> {RA_STR.subnetMask} </th>
                            <th> {RA_STR.endIPAddress} </th>
                        </tr>
                    </thead>
                    <tbody>
                        {parentIpArray.map((data, key) => {
                            return (
                                <tr>
                                    <td>
                                        <input type="checkbox" name={data.trustedIPSeqID} onChange={this.checkSelection.bind(this, data.trustedIPSeqID)} />
                                    </td>
                                    <td>
                                        <input type="text" name={`ipAddr1_${key}`} maxLength="3" size="3" value={data.ipAddr1} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`ipAddr2_${key}`} maxLength="3" size="3" value={data.ipAddr2} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`ipAddr3_${key}`} maxLength="3" size="3" value={data.ipAddr3} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`ipAddr4_${key}`} maxLength="3" size="3" value={data.ipAddr4} onChange={this.handleChange.bind(this, key)} />
                                    </td>
                                    <td>
                                        <input type="text" name={`mask1_${key}`} maxLength="3" size="3" value={data.mask1} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`mask2_${key}`} maxLength="3" size="3" value={data.mask2} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`mask3_${key}`} maxLength="3" size="3" value={data.mask3} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`mask4_${key}`} maxLength="3" size="3" value={data.mask4} onChange={this.handleChange.bind(this, key)} />
                                    </td>
                                    <td>
                                        <input type="text" name={`endIpAddr1_${key}`} maxLength="3" size="3" value={data.endIpAddr1} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`endIpAddr2_${key}`} maxLength="3" size="3" value={data.endIpAddr2} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`endIpAddr3_${key}`} maxLength="3" size="3" value={data.endIpAddr3} onChange={this.handleChange.bind(this, key)} /> <b>.</b>
                                        <input type="text" name={`endIpAddr4_${key}`} maxLength="3" size="3" value={data.endIpAddr4} onChange={this.handleChange.bind(this, key)} />
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                {/* <input type="submit" onClick={this.getChangedIp} /> */}
                <div className="form-group form-submit-button row">
                    <input className="secondary-btn" type="submit" value="save" onClick={this.getChangedIp.bind(this, 'save')}></input>
                    <input className="custom-default-btn ml-3" type="submit" value="delete" onClick={this.getChangedIp.bind(this, 'delete')}></input>
                </div>
            </div>
        )
    }
}

export default TrustedIpRanges;