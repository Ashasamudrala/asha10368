import React, { Component } from 'react';
import './ErrorList.css';
class ErrorList extends Component {
  render() {
    const displayList = this.props.validationDataList ? this.props.validationDataList : '';
    return (
      <div>
        {displayList !== '' ?
          <div className="error-parent">
            <div className="edl_error_feedback" id="ErrorMessagesTable">
              <div className="error-list">
                {displayList.length && displayList.map((eachlist, key) => {
                  return <div key={key}>{eachlist.errorMessage}</div>
                })}
              </div>
            </div>
          </div>
          : ''
        }
      </div>
    )
  }
}
export default ErrorList;
