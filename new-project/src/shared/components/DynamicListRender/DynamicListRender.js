import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { orgData } from '../../utlities/renderer';
import './DynamicListRender.css';
class DynamicListRender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgName: '',
      status: ''
    }
  }
  componentDidMount() {
    let urlParams = new URLSearchParams(window.location.search);
    let orgName = urlParams.get('orgname');
    let status = urlParams.get('status');
    let groupElements = { "orgName": orgName, "status": status };
    orgData(groupElements);
    this.setState({ orgName: orgName, status: status });
  }
  render() {
    const match = window.location.pathname;
    let strSlash = match.lastIndexOf("/");
    let stringRes = match.substring(0, strSlash);
    let splittedSlash = stringRes.lastIndexOf("/");
    let stringSplitRes = stringRes.substring(splittedSlash, stringRes.length);
    let getPageLevelUrl= stringRes.substring(0,splittedSlash);
    let splittedSlashPageLevel = getPageLevelUrl.lastIndexOf("/");
    let splittedSlashPageLevelRes = getPageLevelUrl.substring(splittedSlashPageLevel, getPageLevelUrl.length);
    const profiledata = JSON.parse(localStorage.getItem('profiledata'));
    const previlages = profiledata ? profiledata.userProfile : '';
    const { orgName, status } = this.state;
    return <div className="main">
      <div className="no-padd col-md-12">
        <div className="row">
          <div className="col-sm-10">
            <div className="form-group row">
              <label className="col-sm-3 col-form-label ml-3">Organization Name</label>
              <span>{orgName}</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-10">
            <div className="form-group row">
              <label className="col-sm-3 col-form-label ml-3">Status</label>
              <span className="text-capitalize">{status}</span>
            </div>
          </div>
        </div>
        <ul className="nav nav-pills form-inline">
          <li className="nav-item">
            <Link className={(`/basicOrg` === stringSplitRes) || (`/basicOrg` === splittedSlashPageLevelRes) ? 'custom-secondary-btn' : 'dynamiclist-anchor'} to={`/org/searchOrg/basicOrg/org-details?orgname=${orgName}&status=${status}`} title="Basic Organization Information" >Basic Organization Information</Link>
          </li>
          {previlages.role !== 'MA' ?
            <li className="nav-item">
              <Link className={(`/risk-engine` === stringSplitRes) || (`/risk-engine` === splittedSlashPageLevelRes) ? 'custom-secondary-btn' : 'dynamiclist-anchor'} to={`/org/searchOrg/risk-engine/assign-channels?orgname=${orgName}&status=${status}`} title="Risk Engine" >Risk Engine</Link>
            </li> : ''}
        </ul>
      </div>
    </div>
  }
}
function mapStateToProps(state) {
  return {
    details: state.fetchOrgDetails.data,
  };
}
export default connect(mapStateToProps, null)(DynamicListRender);
