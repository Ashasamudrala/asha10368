import React from 'react';
import PropTypes from 'prop-types';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
const ExampleCustomInput = React.forwardRef(({ onClick }, props) => (
    <div>
      <img src="images/calendarButton.gif" alt="rrewind" onClick={onClick} />
    </div>
  ));

const DatePickerComponent = (props) => (
    <DatePicker
    selected={props.content}
    todayButton="today"
    onChange={props.controlFunc}
    dateFormat='MM/dd/yyyy'
    showMonthDropdown={true}
    showYearDropdown={true}
    scrollableYearDropdown={false}
    dropdownMode="select"
    customInput={<ExampleCustomInput />}
  />
  );

  DatePickerComponent.propTypes = {
    controlFunc: PropTypes.func,
  };

  export default DatePickerComponent ;
