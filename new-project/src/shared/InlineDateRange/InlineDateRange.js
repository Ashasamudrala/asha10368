import React, { Component } from 'react';
import Select from '../components/Select/Select';
import DatePicker from "react-datepicker";
import { httpCall } from '../utlities/RestAPI';
import { RA_API_URL } from '../utlities/constants';
import CA_Utils from '../utlities/commonUtils';
import { RA_STR } from '../utlities/messages';
import "react-datepicker/dist/react-datepicker.css";
import './InlineDateRange.scss';
var moment = require('moment');

class InlineDateRange extends Component {
  constructor() {
    super();
    this.state = {
      dateRangeOptions: [],
      startDate: new Date(),
      endDate: new Date(),
      disableInput: false,
      selectedDateRange: 'Custom',
      getSelectedText: ''
    }
  }
  componentDidMount = async () => {
    const getElementsList = await httpCall('GET', RA_API_URL['getTimePeriods']);
    if (getElementsList && getElementsList.status === 200 && getElementsList.data.dateRange) {
      let dateRangeOptions = CA_Utils.objToArray(getElementsList.data.dateRange, 'object');
      dateRangeOptions.forEach(obj => {
        if (RA_STR.customDateStrings && RA_STR.customDateStrings[obj.content]) {
          obj.displayContent = RA_STR.customDateStrings[obj.content];
        }
      })
      this.setState({
        dateRangeOptions,
        getSelectedText: CA_Utils.objToArray(getElementsList.data.dateRange, 'object')[0].content
      })
    }
  }
  startDateChange = (date) => {
    this.setState({
      startDate: date
    });
  }
  endDateChange = (date) => {
    this.setState({
      endDate: date
    });
  }
  handleDateChange = (event) => {
    let modifyStartDate = this.state.startDate;
    let modifyEndDate = this.state.endDate;
    let getSelectedText = '';
    let value;
    if (event) {
      value = event.target.value;
    }
    let { dateRangeOptions } = this.state;
    if (value && dateRangeOptions) {
      let data = dateRangeOptions.filter(obj => obj.key == value)[0];
      if (data) {
        getSelectedText = data.content;
        this.setState({ getSelectedText });
      }
    }
    if (getSelectedText !== RA_STR.CUSTOM) {
      this.setState({ disableInput: true });
    } else {
      this.setState({ disableInput: false });
    }
    this.setState({ selectedDateRange: getSelectedText });
    switch (getSelectedText) {
      case RA_STR.today:
        modifyStartDate = new Date();
        modifyEndDate = new Date();
        break;
      case RA_STR.currentWeek:
        modifyStartDate = moment().startOf('week').toDate();
        modifyEndDate = new Date();
        break;
      case RA_STR.currentMonth:
        modifyStartDate = moment().startOf('month').toDate();
        modifyEndDate = new Date();
        break;
      case RA_STR.currentQuarter:
        modifyStartDate = moment().startOf('quarter').toDate();
        modifyEndDate = new Date();
        break;
      case RA_STR.lastWeek:
        modifyStartDate = moment().add(-1, 'week').startOf('week').toDate();
        modifyEndDate = moment().add(-1, 'week').endOf('week').toDate();
        break;
      case RA_STR.lastMonth:
        modifyStartDate = moment().add(-1, 'month').startOf('month').toDate();
        modifyEndDate = moment().add(-1, 'month').endOf('month').toDate();
        break;
      case RA_STR.lastQuarter:
        modifyStartDate = moment().add(-1, 'quarter').startOf('quarter').toDate();
        modifyEndDate = moment().add(-1, 'quarter').endOf('quarter').toDate();
        break;
      case RA_STR.last6Months:
        modifyStartDate = moment().add(-6, 'month').startOf('month').toDate();
        modifyEndDate = new Date();
        break;
    }
    this.setState({ startDate: modifyStartDate, endDate: modifyEndDate });
  }
  getDates() {
    let groupDates = [];
    var obj = {
      startDate: moment(this.state.startDate).format('L'),
      endDate: moment(this.state.endDate).format('L'),
      dateRange: this.state.selectedDateRange,
    }
    groupDates.push(obj);
    return groupDates
  }
  render() {
    let { startDate, endDate, disableInput, dateRangeOptions } = this.state;
    return <div className="inline-date col-sm-12">
      <div className="row">
        <div className="col-sm-4">
          <Select
            name={'req-id'}
            title={'Date Range'}
            options={dateRangeOptions}
            controlFunc={this.handleDateChange}
          />
        </div>
        <div className="col-sm-8 displayinline">
          <div className="mrleft-15">
            <label className="col-form-label mright-5">From (MM/dd/yyyy)</label>
            <DatePicker
              selected={startDate}
              todayButton="today"
              onChange={this.startDateChange}
              showMonthDropdown={true}
              showYearDropdown={true}
              scrollableYearDropdown={false}
              disabled={disableInput}
              dropdownMode="select"
              className="form-control"
            />
          </div>
          <div>
            <label className="col-form-label mright-5">To (MM/dd/yyyy)</label>
            <DatePicker
              selected={endDate}
              todayButton="today"
              onChange={this.endDateChange}
              showMonthDropdown={true}
              showYearDropdown={true}
              scrollableYearDropdown={false}
              disabled={disableInput}
              dropdownMode="select"
              className="form-control"
            />
          </div>
        </div>
      </div>
    </div>
  }
}

export default InlineDateRange;
