import { combineReducers } from 'redux'
import * as busyIndicator from './widgets/busyIndicator'
import * as dialogs from './infrastructure/dialogs'
import * as modal from './widgets/modal'
import * as pendingRequest from './infrastructure/pendingRequest'
import * as notificationPopup from './infrastructure/notificationPopup'
import * as users from './features/users'
import * as platform from './features/platform'
import * as framework from './features/framework'
import * as httpCache from './infrastructure/httpCache'
import * as settings from './features/settings'
import * as userContext from './features/userContext'
import * as addPlatform from './features/addPlatform'
import * as viewPlatform from './features/viewPlatform'
import * as viewRepositories from './features/viewRepositories'
import * as editPlatform from './features/editPlatform'
import * as addFramework from './features/addFramework'
import * as loginAndSignUp from './features/loginAndSignUp'
import * as loginUserDetails from './authAndPermissions/loginUserDetails.slice'
import * as auth from './widgets/auth'
import * as repositories from './features/repositories'
import * as continuousIntegration from './features/continuousIntegration'
import * as addRepository from './features/addRepository'
import * as editFramework from './features/editFramework'
import * as viewContinuousIntegration from './features/viewContinuousIntegration'
import * as editRepository from './features/editRepository'
import * as Team from './features/team'
import * as inviteUsers from './features/inviteUser'
import * as addContinuousDelivery from './features/addContinuousDelivery'
import * as addContinuousIntegration from './features/addContinuousIntegration'
import * as continuousDelivery from './features/continuousDelivery'
import * as viewContinuousDelivery from './features/viewContinuousDelivery'
import * as viewFramework from './features/viewframework'
import * as editContinuousIntegration from './features/editContinuousIntegration'
import * as editContinuousDelivery from './features/editContinuousDelivery'
import * as activeMember from './features/activeMembers'
import * as userProfile from './features/profile'
import * as PendingInvites from './features/pendingInvites'
import * as apps from './features/apps'
import * as database from './features/database'

export default combineReducers({
	[busyIndicator.name]: busyIndicator.reducer,
	[loginAndSignUp.name]: loginAndSignUp.reducer,
	[dialogs.name]: dialogs.reducer,
	[loginUserDetails.name]: loginUserDetails.reducer,
	[apps.name]: apps.reducer,
	[modal.name]: modal.reducer,
	[pendingRequest.name]: pendingRequest.reducer,
	[notificationPopup.name]: notificationPopup.reducer,
	[httpCache.name]: httpCache.reducer,
	[users.name]: users.reducer,
	[platform.name]: platform.reducer,
	[framework.name]: framework.reducer,
	[repositories.name]: repositories.reducer,
	[continuousIntegration.name]: continuousIntegration.reducer,
	[settings.name]: settings.reducer,
	[userContext.name]: userContext.reducer,
	[addPlatform.name]: addPlatform.reducer,
	[addRepository.name]: addRepository.reducer,
	[auth.name]: auth.reducer,
	[Team.name]: Team.reducer,
	[viewPlatform.name]: viewPlatform.reducer,
	[viewRepositories.name]: viewRepositories.reducer,
	[viewFramework.name]: viewFramework.reducer,
	[viewContinuousDelivery.name]: viewContinuousDelivery.reducer,
	[editPlatform.name]: editPlatform.reducer,
	[addFramework.name]: addFramework.reducer,
	[editFramework.name]: editFramework.reducer,
	[viewContinuousIntegration.name]: viewContinuousIntegration.reducer,
	[editRepository.name]: editRepository.reducer,
	[inviteUsers.name]: inviteUsers.reducer,
	[addContinuousDelivery.name]: addContinuousDelivery.reducer,
	[addContinuousIntegration.name]: addContinuousIntegration.reducer,
	[continuousDelivery.name]: continuousDelivery.reducer,
	[activeMember.name]: activeMember.reducer,
	[PendingInvites.name]: PendingInvites.reducer,
	[editContinuousIntegration.name]: editContinuousIntegration.reducer,
	[editContinuousDelivery.name]: editContinuousDelivery.reducer,
	[userProfile.name]: userProfile.reducer,
	[database.name]: database.reducer,
})
