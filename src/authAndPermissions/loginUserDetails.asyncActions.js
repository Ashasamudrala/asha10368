import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../infrastructure/doAsync'
import { LOGIN_AUTH, LOGIN_REFRESH } from '../utilities/apiEndpoints'

export const logInUser = createAsyncThunk(
	'login/getAll',
	async ({ username, password } = {}, thunkArgs) => {
		var userDetails = {
			username: username,
			password: password,
		}
		var userLoginDetails = []
		for (var property in userDetails) {
			var encodedKey = encodeURIComponent(property)
			var encodedValue = userDetails[property]
			userLoginDetails.push(encodedKey + '=' + encodedValue)
		}
		userLoginDetails = userLoginDetails.join('&')
		return await doAsync({
			url: LOGIN_AUTH,
			httpMethod: 'post',
			errorMessage: 'Please enter valid credentials',
			httpConfig: {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
				},
				body: userLoginDetails,
			},
			...thunkArgs,
		})
	}
)

export const logInUserOnRefresh = createAsyncThunk(
	'login/refresh',
	async (_, thunkArgs) => {
		return await doAsync({
			url: LOGIN_REFRESH,
			httpMethod: 'get',
			errorMessage: '',
			...thunkArgs,
		})
	}
)

export const logOutUser = createAsyncThunk(
	'auth/logout',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: `auth/logout`,
			httpMethod: 'post',
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
