import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './loginUserDetails.asyncActions'

const initialState = {
	loginInfo: null,
}
const slice = createSlice({
	name: 'loginUserDetails',
	initialState,

	reducers: {},
	extraReducers: {
		// asynchronous action
		[asyncActions.logInUserOnRefresh.fulfilled]: (state, action) => {
			if (action.payload) {
				state.loginInfo = action.payload
			}
		},
		[asyncActions.logInUser.fulfilled]: (state, action) => {
			if (action.payload) {
				sessionStorage.setItem('accessToken', action.payload.accessToken)
				state.loginInfo = action.payload
			}
		},
		[asyncActions.logOutUser.fulfilled]: (state, action) => {
			if (action.payload) {
				sessionStorage.removeItem('accessToken')
				state.loginInfo = null
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
