import { isNil } from 'lodash'
import slice from './loginUserDetails.slice'

export const selectSlice = (state) => state[slice.name]

export const selectGetLogInUserInfo = (state) => selectSlice(state).loginInfo

export const selectGetLoginUserId = (state) => {
	const userInfo = selectGetLogInUserInfo(state)
	if (!isNil(userInfo)) {
		return userInfo.id
	}
	return null
}

export const selectGetLoginUserDisplayName = (state) => {
	const userInfo = selectGetLogInUserInfo(state)
	if (!isNil(userInfo)) {
		return userInfo.displayName
	}
	return ''
}

export const selectGetLoginUserTitle = (state) => {
	const userInfo = selectGetLogInUserInfo(state)
	if (!isNil(userInfo)) {
		return userInfo.title
	}
	return ''
}
