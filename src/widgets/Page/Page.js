import React from 'react'
import { Container } from '@material-ui/core'
import './page.scss'

export default function Page({ children }) {
	return (
		<div>
			<Container>{children}</Container>
		</div>
	)
}
