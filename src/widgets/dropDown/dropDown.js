import React, { useState, useEffect } from 'react'
import { isArray } from 'lodash'
import PropTypes from 'prop-types'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import './dropDown.scss'
/*
It is reusable dropDown component and gets data as props
this component gets the required fields of select to display
 */
export default function DropDown(props) {
	const { name, onChange, className, options, selected, update, title } = props
	const [selectedValue, setSelectedValue] = useState(selected)

	useEffect(() => {
		setSelectedValue(props.selected)
	}, [props.selected])

	const getOptions = () => {
		if (isArray(options)) {
			return options
		}
		return []
	}

	const handleOnChange = (e) => {
		if (update) {
			onChange(e)
		} else {
			setSelectedValue(e.target.value)
			onChange(e)
		}
	}

	const renderOption = (option) => {
		return (
			<MenuItem key={option.id} value={option.id}>
				{option.hasIcon ? (
					<div>
						{option.icon}
						<span className={option.labelClass || ''}>{option.name}</span>
					</div>
				) : (
					option.name
				)}
			</MenuItem>
		)
	}

	return (
		<FormControl>
			<div className='select-class'>
				{title && <label className={`form-label`}>{`${title}`}</label>}
				<Select
					name={name}
					onChange={handleOnChange}
					className={`menu-item ${className}`}
					value={selectedValue}
				>
					{getOptions().map(renderOption)}
				</Select>
			</div>
		</FormControl>
	)
}
DropDown.propTypes = {
	name: PropTypes.string.isRequired,
	title: PropTypes.string,
	update: PropTypes.bool,
	options: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			hasIcon: PropTypes.bool,
			icon: PropTypes.any,
			labelClass: PropTypes.string,
		})
	).isRequired,
	onChange: PropTypes.func,
	className: PropTypes.string,
	selected: PropTypes.string,
}
