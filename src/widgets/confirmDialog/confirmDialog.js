import React from 'react'
import Button from '../button'
import './confirmDialog.scss'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { useTranslation } from 'react-i18next'
import {
	BUTTON_YES,
	BUTTON_NO,
	CONFIRMATION,
	PLATFORM_TRANSLATIONS,
} from '../../utilities/constants'

export default function ConfirmDialog(props) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	return (
		<div>
			<Dialog open={props.open} onClose={props.onClose} className='dialog-box'>
				<DialogTitle>
					<CloseOutlinedIcon className='close-icon' onClick={props.onClose} />
					<span className='dialog-heading'>{t(CONFIRMATION)}</span>
				</DialogTitle>
				<DialogContent>
					<DialogContentText>
						<span className='content'>{props.message}</span>
					</DialogContentText>
				</DialogContent>
				<DialogActions className='actions-btn'>
					<Button
						handleButtonOnClick={() => {
							props.onConfirm()
						}}
						buttonClass='save-btn'
					>
						{t(BUTTON_YES)}
					</Button>
					<Button handleButtonOnClick={props.onClose} buttonClass='cancel-btn'>
						{t(BUTTON_NO)}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}
