import React from 'react'
import Button from '@material-ui/core/Button'
import './button.scss'

export default function IsyButton(props) {
	const handleButtonOnClick = (e) => {
		props.handleButtonOnClick(e)
	}
	return (
		<Button
			onClick={handleButtonOnClick}
			disabled={props.disabled || false}
			className={`isy-button ${props.buttonClass}`}
		>
			{props.children}
		</Button>
	)
}
