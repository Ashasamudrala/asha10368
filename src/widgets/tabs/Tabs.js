import React from 'react'
import TabHeader from './tabHeader'
import TabContent from './tabContent'
import './tabs.scss'
import * as components from './tabRenderExport'

/**
 * this function is to render the generic tab component
 */
export default function Tabs(props) {
	const { tabProp } = props
	const [activeTab, setActiveTab] = React.useState(0)

	/**
	 * Updating the activeTab state.
	 * @param {number} value - tabs index number.
	 */
	const clickTab = (value) => {
		setActiveTab(value)
	}
	return (
		<div className={'tabs-component-root'}>
			<TabHeader clickTab={clickTab} tabs={tabProp} />
			{tabProp.map((tab, i) => {
				return (
					activeTab === i && (
						<TabContent
							key={i}
							tabData={tab}
							tabDetails={components[tab.name]}
						/>
					)
				)
			})}
		</div>
	)
}
