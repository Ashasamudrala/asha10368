import React from 'react'
import Box from '@material-ui/core/Box'

/**
 * @param {object} props - getting props from parent
 * This function is to render the content of the tab which is in active
 */
export default function TabContent(props) {
	return <Box {...props}>{props.tabDetails}</Box>
}
