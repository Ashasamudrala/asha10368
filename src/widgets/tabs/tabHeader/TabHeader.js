import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './tabHeader.scss'

/**
 * @param {any} props - getting props from parent
 */
export default function TabHeader(props) {
	const { clickTab, tabs } = props
	const [activeTab, setActiveTab] = useState(0)

	/**
	 * Represents of the clicking tabs.
	 * @param {object} event
	 * @param {number} newValue clicking of the tab position.
	 */
	const handleTabChange = (id) => {
		setActiveTab(id)
		clickTab(id)
	}
	return (
		<div className={'tabs-root'} value={activeTab}>
			{tabs.map(({ label, id }) => (
				<div
					key={id}
					className={`tab ${id === activeTab && 'active-tab'} tab-header-root`}
					onClick={() => {
						handleTabChange(id)
					}}
				>
					{label}
				</div>
			))}
		</div>
	)
}

TabHeader.propTypes = {
	// activeTab: PropTypes.number.isRequired,
	clickTab: PropTypes.func.isRequired,
	tabs: PropTypes.arrayOf(Object).isRequired,
}
