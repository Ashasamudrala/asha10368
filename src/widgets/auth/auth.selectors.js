import slice from './auth.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllPermission = (state) =>
	selectSlice(state).allowedPermission
