import { createSlice } from '@reduxjs/toolkit'
const initialState = {
	allowedPermission: [],
}

const slice = createSlice({
	name: 'allowedPermission',
	initialState,
	reducers: {
		updateAllowedPermission(state, action) {
			state.allowedPermission = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
