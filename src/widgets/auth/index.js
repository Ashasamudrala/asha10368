import Can from './Can'
import * as selectors from './auth.selectors'
import slice from './auth.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

// we prefix all selectors with the the "select" prefix
export const { selectAllPermission } = selectors

// we export the component most likely to be desired by default
export default Can
