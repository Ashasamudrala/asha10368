import { actions } from './auth.slice'
import { useDispatch } from 'react-redux'

const { updateAllowedPermission } = actions

export default function AuthProvider(props) {
	const dispatch = useDispatch()
	dispatch(updateAllowedPermission(props.userPermissions))
	return null
}
