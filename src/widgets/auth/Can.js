import { useSelector } from 'react-redux'
import { selectGetLogInUserInfo } from '../../authAndPermissions/loginUserDetails.selectors'

export default function Can(props) {
	const permissions = useSelector(selectGetLogInUserInfo)
	const check = (action) => {
		if (!permissions) {
			return false
		} else if (
			(permissions.permissions && permissions.permissions.includes(action)) ||
			permissions.permissions === 'All'
		) {
			return true
		}
		return false
	}
	return check(props.action) ? props.yes() : props.no()
}
