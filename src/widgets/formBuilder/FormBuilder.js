import React from 'react'
import MetaData from '../../widgets/metadata/Metadata'
import Versions from '../../widgets/versions'
import Divider from '@material-ui/core/Divider'
import Input from '../input/Input'
import TextArea from '../textarea/Textarea'
import Accordion from '../accordian/Accordian'
import AddVersion from '../../features/addFramework/AddVersion'
import DropDown from '../dropDown'

import _ from 'lodash'
import { useTranslation } from 'react-i18next'
import { FORMBUILDER_TRANSLATIONS } from '../../utilities/constants'

export default function FormBuilder(props) {
	const { t } = useTranslation(FORMBUILDER_TRANSLATIONS)
	const {
		formBuilderInput,
		errors,
		onChange,
		formBuilderOutput,
		metaDataInput,
		metaDataOutput,
		supportedVersions,
		handleSupportedVersion,
		removeSupportedVersion,
	} = props
	// this is used for showing the inputdata
	const renderInputWidget = (inputData) => {
		return (
			<div className={`${inputData.className ? inputData.className : ''}`}>
				<Input
					name={inputData.name}
					inputType={inputData.type}
					placeholder={t(inputData.placeholder)}
					onChange={onChange}
					title={t(inputData.label)}
					required={inputData.required}
					autoFocus={inputData.autoFocus}
					disabled={inputData.disabled}
					content={
						formBuilderOutput && _.get(formBuilderOutput, inputData.name)
					}
					withOutLabel={inputData.withOutLabel}
					readOnly={inputData.readOnly}
					showInputText={inputData.showInputText}
					maxLength={inputData.maxLength}
					className={`${
						errors && _.get(errors, inputData.name)
							? inputData.inputErrorClass
							: ''
					}`}
				/>
				<span className={inputData.errorClass}>
					{t(errors && _.get(errors, inputData.name))}
				</span>
			</div>
		)
	}

	const renderSelectWidget = (selectData) => {
		return (
			<div className={`${selectData.className ? selectData.className : ''}`}>
				<DropDown
					name={selectData.name}
					onChange={onChange}
					className={`${selectData.inputBaseClass}
					${errors && _.get(errors, selectData.name) ? selectData.inputErrorClass : ''}`}
					options={selectData.options}
					selected={
						formBuilderOutput && _.get(formBuilderOutput, selectData.name)
					}
					placeholder={selectData.placeholder}
					title={selectData.label}
				/>
				<span className={selectData.errorClass}>
					{errors && _.get(errors, selectData.name)}
				</span>
			</div>
		)
	}
	// this is used for showing the textarea
	const renderTextAreaWidget = (textAreaData) => {
		return (
			<div
				className={`${textAreaData.className ? textAreaData.className : ''}`}
			>
				<TextArea
					name={textAreaData.name}
					onChange={onChange}
					placeholder={t(textAreaData.placeholder)}
					title={t(textAreaData.label)}
					required={textAreaData.required}
					className={`${
						errors && errors[textAreaData.name]
							? textAreaData.inputErrorClass
							: ''
					}`}
					content={formBuilderOutput && formBuilderOutput[textAreaData.name]}
				/>
				<span className={textAreaData.errorClass}>
					{t(errors && _.get(errors, textAreaData.name))}
				</span>
			</div>
		)
	}
	// this is used for showing the versions
	const renderVersionWidget = (versionData) => {
		return (
			<Versions
				{...versionData}
				errors={errors}
				value={formBuilderOutput && formBuilderOutput[versionData.name]}
				handleVersions={onChange}
			/>
		)
	}
	// this is used for showing metadata
	const renderMetadataWidget = (formField) => {
		return (
			<div className={`${formField.className ? formField.className : ''}`}>
				<MetaData
					{...formField}
					metaDataInput={
						formField.metadataInput ? formField.metadataInput : metaDataInput
					}
					metaDataOutput={
						formField.metadataOutput ? formField.metadataOutput : metaDataOutput
					}
					metadata={
						formBuilderOutput && _.get(formBuilderOutput, formField.name)
					}
					handleMetadata={onChange}
				/>
			</div>
		)
	}

	// this is used for rendering form
	const renderFormDetails = (formData, className) => {
		return formData.map((formField, index) => (
			<div className={`${className || ''}`} key={index}>
				{['text', 'number', 'password', 'email'].includes(formField.type) &&
					renderInputWidget(formField)}
				{formField.type === 'select' && renderSelectWidget(formField)}
				{formField.type === 'textarea' && renderTextAreaWidget(formField)}
				{formField.type === 'version' && renderVersionWidget(formField)}
				{formField.type === 'metadata' && renderMetadataWidget(formField)}
			</div>
		))
	}
	const renderVersionsModuleWidget = (formField) => {
		return (
			<AddVersion
				handleVersiondata={onChange}
				onClick={props.onClick}
				framework={formBuilderOutput}
				errors={errors}
				supportedVersions={supportedVersions}
				handleSupportedVersion={handleSupportedVersion}
				removeSupportedVersion={removeSupportedVersion}
			/>
		)
	}
	// render accordian information
	const renderAcccordian = () => {
		return formBuilderInput.section.map((accordianData, index) => {
			return (
				<div key={index}>
					<Accordion
						header={
							<div className={accordianData.subHeader.className}>
								{t(accordianData.subHeader.subHeading)}
							</div>
						}
					>
						{accordianData.inputFields &&
							renderFormDetails(
								accordianData.inputFields,
								accordianData.className
							)}
						{_.has(accordianData, 'frameworkVersion') &&
							renderVersionsModuleWidget(t(accordianData.frameworkVersion))}
					</Accordion>
					{accordianData.divider && <Divider />}
				</div>
			)
		})
	}
	// render information with out accordian
	const renderWithOutAccordian = () => {
		return formBuilderInput.section.map((sectionData, index) => {
			return (
				<div
					className={
						sectionData.inlineFieldsClassName &&
						sectionData.inlineFieldsClassName
					}
					key={index}
				>
					{_.has(sectionData, 'subHeader') && (
						<div className={sectionData.subHeader.className}>
							{t(sectionData.subHeader.subHeading)}
						</div>
					)}

					{sectionData.inputFields &&
						renderFormDetails(sectionData.inputFields, sectionData.className)}
					{sectionData.divider && <Divider />}
				</div>
			)
		})
	}
	// render form details based on json
	return (
		<>
			{_.has(formBuilderInput, 'formHeader') && (
				<div className={formBuilderInput.formHeader.className}>
					{t(formBuilderInput.formHeader.formHeading)}
				</div>
			)}
			{_.has(formBuilderInput, 'formHeader') &&
				formBuilderInput.formHeader.divider && (
					<div className={formBuilderInput.formHeader.divider.className}>
						<Divider />
					</div>
				)}
			{_.has(formBuilderInput, 'showAccordian') &&
			formBuilderInput.showAccordian
				? renderAcccordian()
				: renderWithOutAccordian()}
		</>
	)
}
