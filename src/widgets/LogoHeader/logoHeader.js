import React from 'react'
import { useTranslation } from 'react-i18next'
import {
	ISYMPHONY,
	STUDIO,
	PLATFORM_TRANSLATIONS,
} from '../../utilities/constants'
import './logoHeader.scss'

function LogoHeader() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	return (
		<div className='logo-text'>
			<span className='logo-left'>{t(ISYMPHONY)}</span>
			<span className='logo-right'>{t(STUDIO)}</span>
		</div>
	)
}

export default LogoHeader
