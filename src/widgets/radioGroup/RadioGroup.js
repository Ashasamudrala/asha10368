import React from 'react'
import PropTypes from 'prop-types'
import { Radio } from '@material-ui/core'
import { isString } from 'lodash'
import './radioGroup.scss'

export function IsyRadioGroup(props) {
	const { label, value, options, labelClass } = props

	const getLabelClass = () => {
		if (isString(labelClass)) {
			return labelClass
		}
		return ''
	}

	const handleOnChange = (newValue) => {
		if (value !== newValue) {
			props.onChange(newValue)
		}
	}

	const renderRadio = (item) => {
		return (
			<div className='isy-radio-button' key={item.value}>
				<Radio
					className='isy-radio-class'
					checked={value === item.value}
					value={item.value}
					onChange={(e) => handleOnChange(e.target.value)}
				/>
				<span
					className='isy-radio-label'
					onClick={() => handleOnChange(item.value)}
				>
					{item.label}
				</span>
			</div>
		)
	}
	return (
		<div className='isy-radio-group'>
			<p className={'isy-radio-group-label ' + getLabelClass()}>{label}</p>
			{options.map(renderRadio)}
		</div>
	)
}
IsyRadioGroup.propTypes = {
	label: PropTypes.string,
	labelClass: PropTypes.string,
	value: PropTypes.string.isRequired,
	options: PropTypes.arrayOf(
		PropTypes.shape({
			value: PropTypes.string.isRequired,
			label: PropTypes.string.isRequired,
		})
	).isRequired,
	onChange: PropTypes.func,
}
