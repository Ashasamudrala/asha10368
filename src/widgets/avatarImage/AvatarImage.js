import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import PropTypes from 'prop-types'

/*
It is reusable avataer component and gets data as props
This component gets the required fields of image or profile pic to display
 */
export default function AvatarImage(props) {
	const { imageClassName, imageData, fileUrl, defaultNameChar } = props
	let imageUrl
	if (fileUrl) {
		imageUrl = fileUrl
	} else {
		imageUrl = `data:image/png;base64,${imageData}`
	}
	return (
		<Avatar className={imageClassName} src={imageUrl}>
			{defaultNameChar}
		</Avatar>
	)
}
AvatarImage.propTypes = {
	imageClassName: PropTypes.string,
	imageData: PropTypes.string,
	defaultNameChar: PropTypes.string,
}
