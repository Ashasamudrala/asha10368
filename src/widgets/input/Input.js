import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {
	FormControl,
	IconButton,
	InputLabel,
	InputAdornment,
} from '@material-ui/core'
import InputText from '@material-ui/core/Input'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import './input.scss'

/*
It is reusable input component and gets data as props
this component gets the required fields of input to display
 */
export default function Input(props) {
	const [showPassword, setShowPassword] = React.useState(false)

	// Updating true or false after click on password field inputEndornment icon.
	const handleClickShowPassword = () => {
		setShowPassword(!showPassword)
	}
	return (
		<Fragment>
			{!props.withOutLabel ? (
				<div className='dynamic-form-input input-field'>
					{props.title && (
						<label className={`form-label`}>
							{`${props.title}`}
							{props.required ? (
								<span className='form-label-required'>*</span>
							) : (
								''
							)}
						</label>
					)}
					<input
						className={`form-control ${props.className ? props.className : ''}`}
						autoComplete='off'
						name={props.name}
						label={props.title}
						value={props.content}
						data-id={props.dataId}
						disabled={props.disabled}
						type={props.inputType}
						onChange={props.onChange}
						placeholder={props.placeholder}
						maxLength={props.maxLength}
						autoFocus={props.autoFocus}
					/>
				</div>
			) : (
				<FormControl className={props.className}>
					<InputLabel>{props.title}</InputLabel>
					<InputText
						type={
							props.showInputText ? 'text' : showPassword ? 'text' : 'password'
						}
						name={props.name}
						value={props.content}
						onChange={props.onChange}
						disabled={props.disabled}
						endAdornment={
							props.name === 'password' && props.content.length > 0 ? (
								<InputAdornment>
									<IconButton onClick={handleClickShowPassword}>
										{showPassword ? <Visibility /> : <VisibilityOff />}
									</IconButton>
								</InputAdornment>
							) : null
						}
					/>
				</FormControl>
			)}
		</Fragment>
	)
}

Input.propTypes = {
	inputType: PropTypes.oneOf(['text', 'number', 'password', 'email'])
		.isRequired,
	name: PropTypes.string.isRequired,
	controlFunc: PropTypes.func,
	placeholder: PropTypes.string,
	disabled: PropTypes.bool,
	required: PropTypes.bool,
	className: PropTypes.string,
}
