import React from 'react'
import { Popover as MUIPopover } from '@material-ui/core'
import { isArray } from 'lodash'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

export default function Popover(props) {
	const { actions, anchorEl } = props

	const getActions = () => {
		if (isArray(actions)) {
			return actions
		}
		return []
	}

	const handleSelect = (event, key) => {
		props.onSelect(event, key)
	}

	const renderAction = (action) => {
		return (
			<MenuItem
				key={action.key}
				onClick={(event) => handleSelect(event, action.key)}
			>
				{action.hasIcon ? (
					<div>
						{action.icon}
						<span className={action.labelClass}>{action.name}</span>
					</div>
				) : (
					action.name
				)}
			</MenuItem>
		)
	}

	return (
		<MUIPopover
			open={true}
			anchorEl={anchorEl}
			onClose={(e) => props.onClose(e)}
			anchorOrigin={{
				vertical: 'bottom',
				horizontal: 'right',
			}}
			transformOrigin={{
				vertical: 'top',
				horizontal: 'left',
			}}
		>
			<Menu
				className={props.className || ''}
				anchorEl={anchorEl}
				keepMounted
				open={true}
				onClose={(e) => props.onClose(e)}
			>
				{getActions().map(renderAction)}
			</Menu>
		</MUIPopover>
	)
}
