import React from 'react'
import AutoCompleteSearch from '../autoCompleteSearch/autoCompleteSearch'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import RedoOutlinedIcon from '@material-ui/icons/RedoOutlined'
import UndoOutlinedIcon from '@material-ui/icons/UndoOutlined'
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined'
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined'
import IsyButton from '../../widgets/button/button'
import PublishWithChanges from '../../icons/PublishWithChanges'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import { useTranslation } from 'react-i18next'
import { TOOLBAR_TRANSLATIONS } from '../../utilities/constants'

export default function Toolbar(props) {
	const { t } = useTranslation(TOOLBAR_TRANSLATIONS)
	const {
		toolbarInput,
		onChange,
		searchSuggestions,
		options,
		cancelIcon,
		handleCancel,
		handleSearch,
		handleSuggestions,
	} = props

	const renderSearchWidget = (input) => {
		return (
			<AutoCompleteSearch
				searching={input}
				searchIcon={input.searchIcon}
				showSuggestions={options}
				suggestions={searchSuggestions}
				handleChange={onChange}
				handleSearch={handleSearch}
				handleCancel={handleCancel}
				handleSuggestions={handleSuggestions}
				cancelIcon={cancelIcon}
				iconPosition={input.iconPosition}
				searchPlaceholder={t(input.placeholder)}
			/>
		)
	}
	const handleChange = (e, input) => {
		onChange(e, input.name, input.actionType)
	}
	const renderIconWidget = (input) => {
		const IconComponents = {
			VisibilityOutlinedIcon,
			UndoOutlinedIcon,
			RedoOutlinedIcon,
			SaveOutlinedIcon,
			PublishWithChanges,
		}
		const Icon = IconComponents[input.iconName]
		return (
			<>
				<Tooltip title={t(input.tooltip)}>
					<IconButton
						aria-label={t(input.tooltip)}
						disabled={input.disable}
						onClick={(e) => onChange(e, input.name)}
					>
						{React.createElement(Icon)}
					</IconButton>
				</Tooltip>
			</>
		)
	}
	const renderButtonWidget = (input) => {
		return (
			<IsyButton
				buttonClass={input.className}
				handleButtonOnClick={(e) => handleChange(e, input)}
				properties={input}
				disabled={input.disable}
			>
				<AddOutlinedIcon />
				{t(input.buttonName)}
			</IsyButton>
		)
	}
	const renderToolbarWidget = (type, input) => {
		switch (type) {
			case 'search':
				return renderSearchWidget(input)
			case 'iconWithToolTip':
				return renderIconWidget(input)
			case 'button':
				return renderButtonWidget(input)
			case 'flexGrow':
				return <div className={input.className}></div>
			default:
		}
	}

	return (
		<div className='row-fileds'>
			{toolbarInput.map((input, index) => {
				return renderToolbarWidget(input.type, input)
			})}
		</div>
	)
}
