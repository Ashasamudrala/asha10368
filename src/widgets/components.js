import Dashboard from '../features/dashboard/Dashboard'
import DecisionEngine from '../features/decisionEngine/DecisionEngine'
import Database from '../features/database/Database'
import Settings from '../features/settings/Settings'
import Applications from '../features/applications/Applications'
import Profile from '../features/profile/Profile'
import Core from '../features/core/Core'
import Devops from '../features/devops/Devops'
import Accelerators from '../features/accelerators/Accelerators'
import Platform from '../features/platform/Platform'

const components = {
	Dashboard: Dashboard,
	Database: Database,
	DecisionEngine: DecisionEngine,
	Accelerators: Accelerators,
	Applications: Applications,
	Core: Core,
	Devops: Devops,
	Profile: Profile,
	Settings: Settings,
	Platform: Platform,
}
export default components
