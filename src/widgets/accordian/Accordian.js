import React from 'react'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

// Ithis is used for showing the compoenet as accordian
export default function AccordionWidget({ children, header }) {
	return (
		<div className='accordion'>
			<Accordion defaultExpanded={true}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					{header}
				</AccordionSummary>
				{children}
			</Accordion>
		</div>
	)
}
