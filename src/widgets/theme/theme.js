import React from 'react'
export default function Theme({ children, themeName }) {
	return <div className={themeName}>{children}</div>
}
