import React, { useRef, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import Input from '../input/Input'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import IsyButton from '../button/button'
import {
	ADD,
	ONCHANGE,
	PLATFORM_TRANSLATIONS,
	DELETE,
} from '../../utilities/constants'
import { useTranslation } from 'react-i18next'

/**
 *
 * @param {*} props from formbuilder component
 */
export default function Metadata(props) {
	const {
		metaDataInput,
		metadata,
		handleMetadata,
		name,
		metaDataOutput,
		header,
		headerClassName,
	} = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	//	this function is used whenever change in metadata
	const handleInputChange = (event) => {
		handleMetadata(event, name, t(ONCHANGE))
	}

	//	this function is used for deleting the rows
	const deleteRow = (id) => {
		handleMetadata(id, name, t(DELETE))
	}

	// this function is used for adding the rows
	const metaDataInputFocus = useRef(null)
	const metadataKeys = Object.keys(metaDataOutput)
	const setFocus = useRef(false)

	const setFocusAndScroll = () => {
		if (setFocus.current === true) {
			if (metaDataInputFocus && metaDataInputFocus.current) {
				let selector = null
				if (metadataKeys[0] === 'key') {
					selector = `[name=key]`
				} else if (metadataKeys[0] === 'name') {
					selector = `[name=name]`
				}

				metaDataInputFocus.current.querySelector(selector).focus()
				metaDataInputFocus.current.querySelector(selector).scrollIntoView(true)
			}
		}
	}

	useEffect(setFocusAndScroll, [metadata])

	const addRow = () => {
		handleMetadata({ ...metaDataOutput }, name, t(ADD))
		setFocus.current = true
	}

	// this is used for adding the button
	const metadataButton = () => {
		return header ? renderMetadataButtonHeader() : renderMetadataButton()
	}

	const renderMetadataButtonHeader = () => {
		return (
			<div className='container'>
				<div className={headerClassName}>{header}</div>
				<div className='button'>
					<IsyButton
						buttonClass='add-metadata-btn'
						handleButtonOnClick={addRow}
					>
						<AddOutlinedIcon />
						{t(metaDataInput.buttonName)}
					</IsyButton>
				</div>
			</div>
		)
	}
	const renderMetadataButton = () => {
		return (
			<div className='button'>
				<IsyButton buttonClass='add-metadata-btn' handleButtonOnClick={addRow}>
					<AddOutlinedIcon />
					{metaDataInput.buttonName}
				</IsyButton>
			</div>
		)
	}
	// this is header for the dynamic component
	const renderMetadataHeader = () => {
		return metadata.length > 0 ? (
			<Grid container spacing={4} className='metdata-header'>
				{metaDataInput.rowHeaderNames.map((headingDetails, index) => {
					return (
						<Grid item xs={headingDetails.gridSpace} key={index}>
							{headingDetails.headerName}
						</Grid>
					)
				})}
			</Grid>
		) : null
	}
	// render metadata grid component
	const renderMetadataGrid = () => {
		return metadata.map((keyValue, idx) => {
			return (
				<div
					className={'dynamic-form-input input'}
					key={idx}
					ref={idx === 0 ? metaDataInputFocus : null}
				>
					<Grid container spacing={4}>
						{Object.keys(keyValue).map((inputName, index) => {
							return (
								<Grid item xs={5} key={index}>
									<Input
										name={inputName}
										inputType={metaDataInput.inputFields[index].type}
										onChange={handleInputChange}
										placeholder={metaDataInput.inputFields[index].placeholder}
										content={keyValue[inputName]}
										dataId={idx}
										maxLength={metaDataInput.inputFields[index].maxLength}
									/>
								</Grid>
							)
						})}
						<Grid item xs={2}>
							<div className='delete-icon'>
								<img
									src='/images/Delete.svg'
									alt='delete-icon'
									onClick={() => {
										deleteRow(idx)
									}}
								/>
							</div>
						</Grid>
					</Grid>
				</div>
			)
		})
	}

	return (
		<div className='dynamic-metadata-input'>
			{metadataButton()}
			{renderMetadataHeader()}
			{renderMetadataGrid()}
		</div>
	)
}
