import React, { useState } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import PropTypes from 'prop-types'
import MenuIcon from '@material-ui/icons/Menu'
import DashboardIcon from '@material-ui/icons/Dashboard'
import DnsIcon from '@material-ui/icons/Dns'
import MediationIcon from '../../icons/mediation'
import ExtensionIcon from '@material-ui/icons/Extension'
import { Container } from '@material-ui/core'
import NotificationPopup from '../../../src/infrastructure/notificationPopup'
import {
	ProSidebar,
	Menu,
	MenuItem,
	SubMenu,
	SidebarContent,
	SidebarHeader,
} from 'react-pro-sidebar'
import 'react-pro-sidebar/dist/css/styles.css'
import './SideBar.scss'
import RoutePaths from '../../config/routes.json'
import { Switch, Route } from 'react-router-dom'
import components from '../components'

/**
 * This component is to create the sideBar.
 */
export default function SideBar() {
	const [collapsed, setCollapsed] = useState(true)
	/**
	 * handle to toggle the side drawer.
	 */
	const handleCollapsed = () => {
		setCollapsed(!collapsed)
	}

	const renderIcon = (icon) => {
		const IconComponents = {
			MenuIcon,
			DashboardIcon,
			DnsIcon,
			MediationIcon,
			ExtensionIcon,
		}
		const Icon = IconComponents[icon]
		if (typeof Icon !== 'undefined') {
			return React.createElement(Icon)
		}
	}

	/**
	 * This function is to handle sideBar content.
	 */
	function sidebarContent() {
		return (
			<SidebarContent>
				<Menu>
					{RoutePaths.sideNavigation.map((item) => {
						if (item.children && item.children.length > 0) {
							return (
								<SubMenu
									title={item.title}
									key={item.id}
									icon={renderIcon(item.icon)}
								>
									{item.children.map((child) => (
										<LinkContainer to={child.path} key={child.id}>
											<MenuItem>{child.title}</MenuItem>
										</LinkContainer>
									))}
								</SubMenu>
							)
						} else {
							return (
								<LinkContainer
									to={item.path}
									key={item.id}
									icon={renderIcon(item.icon)}
								>
									<MenuItem>{item.title}</MenuItem>
								</LinkContainer>
							)
						}
					})}
				</Menu>
			</SidebarContent>
		)
	}
	return (
		<div className='side-nav-menu'>
			<ProSidebar collapsed={collapsed}>
				<SidebarHeader>
					<span
						className='toggle-button'
						role='button'
						onClick={handleCollapsed}
					>
						<MenuIcon />
					</span>
				</SidebarHeader>
				{sidebarContent()}
			</ProSidebar>
			<div className='main'>
				<Switch>
					{RoutePaths.sideNavigation.map((route) => {
						const RouteComponent = components[route.componentName]
						/**
						 * If there is any child component, then the if part will execute
						 */
						if (route.children && route.children.length > 0) {
							return route.children.map((child) => {
								const ChildComponent = components[child.componentName]
								return (
									/**
									 * As per screens and what we discussed with UX team,
									 * we will have braedcrumb only for child components.
									 */
									<PageRoute path={child.path} key={child.id}>
										<NotificationPopup />
										<ChildComponent />
									</PageRoute>
								)
							})
						} else {
							/**
							 * If there is only parent component, then the else part will execute
							 */
							return (
								<PageRoute path={route.path} key={route.id}>
									<div className='route-without-breadcrum'>
										<NotificationPopup />
									</div>
									<RouteComponent />
								</PageRoute>
							)
						}
					})}
				</Switch>
			</div>
		</div>
	)
}

function PageRoute({ children, ...rest }) {
	return (
		<Route {...rest}>
			<Container fixed={false}>{children}</Container>
		</Route>
	)
}

SideBar.propTypes = {
	collapsed: PropTypes.bool,
	handleCollapsed: PropTypes.func,
}
