import React from 'react'
import PropTypes from 'prop-types'
import { isFunction, isBoolean, isString } from 'lodash'

export class IsyInput extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			value: props.value,
		}
		this.handleOnChange = this.handleOnChange.bind(this)
		this.handleOnBlur = this.handleOnBlur.bind(this)
	}

	static getDerivedStateFromProps(props, state) {
		if (props.value !== state.value) {
			return { value: state.value }
		}
		return null
	}

	getInputValue() {
		if (isFunction(this.props.onChange)) {
			return this.props.value
		}
		return this.state.value
	}

	getIsDisabled() {
		if (isBoolean(this.props.disabled)) {
			return this.props.disabled
		}
		return false
	}

	getPlaceholder() {
		if (isString(this.props.placeholder)) {
			return this.props.placeholder
		}
		return ''
	}

	handleOnChange(evt) {
		if (isFunction(this.props.onChange)) {
			this.props.onChange(evt)
		} else {
			this.setState({
				value: evt.target.value,
			})
		}
	}

	handleOnBlur(evt) {
		if (isFunction(this.props.onBlur)) {
			this.props.onBlur(evt.target.value)
		}
	}

	render() {
		return (
			<input
				type={this.props.type || 'text'}
				className={'isy_input'}
				value={this.getInputValue()}
				placeholder={this.getPlaceholder()}
				disabled={this.getIsDisabled()}
				onChange={this.handleOnChange}
				onBlur={this.handleOnBlur}
			/>
		)
	}
}

IsyInput.propTypes = {
	value: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	disabled: PropTypes.bool,
	onChange: PropTypes.func,
	onBlur: PropTypes.func,
}
