import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Profile from '../../features/profile/Profile'
import ViewProfile from '../../features/profile/viewProfile'
import EditProfile from '../../features/profile/editProfile'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import { fetchUserDetails } from '../../features/profile/profile.asyncActions'
import { selectGetLoginUserId } from '../../authAndPermissions/loginUserDetails.selectors'
import { selectProfileDetails } from '../../features/profile/profile.selectors'
import AvatarImage from '../avatarImage'
import { logOutUser } from '../../authAndPermissions/loginUserDetails.asyncActions'
import { useHistory } from 'react-router-dom'
import { LinkContainer } from 'react-router-bootstrap'

import {
	AppBar,
	Button,
	Toolbar,
	Link,
	MenuItem,
	Menu,
	IconButton,
} from '@material-ui/core'
import {
	TOP_NAVBAR_TRANSLATIONS,
	TOP_NAV_HEADING_FIRST,
	TOP_NAV_HEADING_MID,
	TOP_NAV_HEADING_LAST,
	permission,
} from '../../utilities/constants'
import TopNav from '../../config/routes.json'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import Can from '../../widgets/auth/Can'
import './topNavigation.scss'

export default function TopNavigation() {
	const [anchorEl, setAnchorEl] = useState(null)
	const userDetails = useSelector(selectProfileDetails)
	const loginUserId = useSelector(selectGetLoginUserId)
	const editUserDetails = userDetails
	const [open, setOpen] = useState(false)
	const [openView, setOpenView] = useState(false)
	const [openEdit, setOpenEdit] = useState(false)
	const dispatch = useDispatch()
	const history = useHistory()

	const firstName =
		userDetails && userDetails.firstName && userDetails.firstName.charAt(0)
	const LastName =
		userDetails && userDetails.lastName && userDetails.lastName.charAt(0)

	useEffect(() => {
		dispatch(fetchUserDetails({ userId: loginUserId }))
	}, [dispatch, loginUserId])

	const handleClickOpen = (e) => {
		setOpen(true)
	}

	const handleCancel = () => {
		setOpen(false)
		setOpenView(false)
		setOpenEdit(false)
	}

	const handleAction = (type) => {
		if (type === 'ViewProfile') {
			setOpen(false)
			setOpenView(true)
		}
		if (type === 'SignOut') {
			// handle sigout here
			setOpen(false)
			dispatch(logOutUser())
			history.push('/login')
			window.location.reload()
		}
		if (type === 'EditProfile') {
			setOpenView(false)
			setOpenEdit(true)
		}
	}

	const isOpen = Boolean(anchorEl)
	const { t } = useTranslation(TOP_NAVBAR_TRANSLATIONS)

	/**
	 * Getting the window location and adding the active class
	 * to the admin button when we select the child from menu
	 */
	const { pathname } = window.location
	const isAdminLinkActive = pathname.indexOf('/admin/') > -1 ? 'active' : ''

	/**
	 * handles the menu button clcick for open admin menu
	 * event- we will get currentTarget
	 */
	const handleMenuButtonClick = (event) => {
		setAnchorEl(event.currentTarget)
	}
	/**
	 * handles the menu button clcick for close admin menu
	 */
	const handleClose = () => {
		setAnchorEl(null)
	}
	/**
	 * handles the Top Navigation Header Name
	 */
	function topNavigationHeader() {
		return (
			<React.Fragment>
				<span className='topbar-heading-first'>{t(TOP_NAV_HEADING_FIRST)}</span>
				<span className='topbar-heading-mid'>{t(TOP_NAV_HEADING_MID)}</span>
				<span className='topbar-heading-last'>{t(TOP_NAV_HEADING_LAST)}</span>
			</React.Fragment>
		)
	}
	/**
	 * handles the top Navigation Items(Apps, Team, and Admin)
	 */
	function topNavigationItems() {
		return (
			<React.Fragment>
				{TopNav.topNavigation.map((item) => {
					if (!item.children || item.children.length <= 0) {
						return (
							<LinkContainer to={item.path} key={item.id}>
								<Link color='inherit' className='app-nav'>
									{item.title}
								</Link>
							</LinkContainer>
						)
					} else {
						return (
							<React.Fragment key={item.id}>
								<Button
									aria-controls={item.slug}
									aria-haspopup='true'
									color='inherit'
									onClick={handleMenuButtonClick}
									className={'app-nav ' + isAdminLinkActive}
								>
									{item.title}
									{<ArrowDropDownIcon />}
								</Button>
								<Menu
									className='admin-menu'
									id={item.slug}
									anchorEl={anchorEl}
									open={isOpen}
									onClose={handleClose}
								>
									{item.children.map((child, idx) => (
										<LinkContainer to={child.path} key={idx}>
											<MenuItem onClick={handleClose}>{child.title}</MenuItem>
										</LinkContainer>
									))}
								</Menu>
							</React.Fragment>
						)
					}
				})}
			</React.Fragment>
		)
	}
	/**
	 * handles the Top Navigation User Icon
	 */
	function topNavigationUser() {
		return (
			<Can
				action={permission.VIEW_USER_PROFILE}
				yes={() => (
					<div className='user-icon-div'>
						<IconButton onClick={(e) => handleClickOpen(e)}>
							<AvatarImage
								imageData={userDetails && userDetails.profilePic}
								defaultNameChar={firstName + LastName}
							/>
						</IconButton>
						{open && (
							<Profile
								open={open}
								onCancel={handleCancel}
								callback={handleAction}
							/>
						)}
						{openView && (
							<ViewProfile
								open={openView}
								onCancel={handleCancel}
								callback={handleAction}
								userData={userDetails}
							/>
						)}
						{openEdit && (
							<Can
								action={permission.UPDATE_USER_PROFILE}
								yes={() => (
									<EditProfile
										open={openEdit}
										onClose={handleCancel}
										editUserData={editUserDetails}
									/>
								)}
								no={() => ''}
							></Can>
						)}
					</div>
				)}
				no={() => ''}
			></Can>
		)
	}
	return (
		<div>
			<AppBar className='AppBar' position='static'>
				<Toolbar variant='dense' className='tool-bar'>
					{topNavigationHeader()}
					{topNavigationItems()}
					{topNavigationUser()}
				</Toolbar>
			</AppBar>
		</div>
	)
}
TopNavigation.propTypes = {
	handleMenuButtonClick: PropTypes.func,
	handleClose: PropTypes.func,
}
