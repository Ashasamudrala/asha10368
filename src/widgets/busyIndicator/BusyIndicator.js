import React from 'react'
import { useSelector } from 'react-redux'
import { getGlobalBusyIndicator } from './busyIndicator.selectors'
import CircularProgress from '@material-ui/core/CircularProgress'
import './busyIndicator.scss'

export default function BusyIndicator({ children }) {
	const show = useSelector(getGlobalBusyIndicator)

	const hasContentToDisplay =
		!show && children && (children.length === undefined || children.length > 0)

	return (
		<React.Fragment>
			{show ? (
				<div className='busy-indicator'>
					<CircularProgress />
				</div>
			) : (
				<React.Fragment>
					{hasContentToDisplay ? children : <ContentNotFound />}
				</React.Fragment>
			)}
		</React.Fragment>
	)
}

function ContentNotFound() {
	return <p>No content found</p>
}
