import React from 'react'
import Breadcrumbs from '@material-ui/core/Breadcrumbs'
import Link from '@material-ui/core/Link'
import { NavigateNext } from '@material-ui/icons'
import './breadcrumb.scss'

/**
 * To generate dymic breadCrumb
 * @param {sting} props - we will get url path.
 */
export default function Breadcrumb(props) {
	const { path } = props
	const pathToString = `${path}`
	const breadcrumbLinks = pathToString.split('/', 3).slice(1)
	return (
		<div className='breadCrum-conatainer'>
			<Breadcrumbs
				aria-label='breadcrumb'
				separator={<NavigateNext fontSize='small' />}
			>
				{breadcrumbLinks.map((link, i) => (
					<Link color='inherit' key={i}>
						{link === 'admin' ? 'administration' : link}
					</Link>
				))}
			</Breadcrumbs>
		</div>
	)
}
