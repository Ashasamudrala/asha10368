import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import DropDown from '../../../../dropDown'
import { TableCell, Checkbox } from '@material-ui/core'
import Avatar from '../../../../avatarImage/AvatarImage'
import Can from '../../../../../widgets/auth/Can'
import { permission } from '../../../../../utilities/constants'
import _ from 'lodash'

export function GridTableCell(props) {
	const { colName, item } = props

	const handleAction = (actionType, item, e) => {
		e.stopPropagation()
		props.handleAction(actionType, item, e)
	}
	const handleViewItem = (actionType, item, e) => {
		if (e.target.nodeName && e.target.nodeName !== 'LI') {
			props.handleViewItem(true, item)
		}
	}

	const renderAvatar = (column, item) => {
		let firstName = ''
		let lastName = ''
		const props = {}
		if (_.get(item, column.keyPath)) {
			firstName = _.get(item, column.keyPath)
		}
		if (_.get(item, 'lastName')) {
			lastName = _.get(item, 'lastName')
		}
		if (firstName) {
			props.defaultNameChar = firstName.charAt(0) + lastName.charAt(0)
		}
		return (
			<div className='parent-icon'>
				<Avatar
					imageClassName={'small'}
					imageData={item.profilePic}
					{...props}
				/>
				<div className='icon-name'>
					{firstName} {lastName}
				</div>
			</div>
		)
	}
	const renderDropdown = (column, item) => {
		return (
			<DropDown
				update={true}
				name={column.name}
				className={column.className}
				onChange={(e) => handleAction(column.type, item, e)}
				options={column.options}
				selected={_.get(item, column.keyPath)}
			/>
		)
	}
	const renderCheckbox = (column, item) => {
		return (
			<div className={` ${item.checked ? '' : 'isVisible'}`}>
				<Checkbox
					className={`checkbox-class`}
					checked={item.checked}
					onClick={(e) => handleAction(column.type, item, e)}
				/>
			</div>
		)
	}
	const renderCommaSeparetedValues = (column, item) => {
		if (column.dataType === 'string') {
			return _.get(item, column.keyPath).join(column.formatter)
		} else if (column.dataType === 'object') {
			return _.get(item, column.keyPath)
				.map((object) => _.get(object, column.objectKeyPath))
				.join(column.formatter)
		}
	}

	const renderIcon = (column, item) => {
		switch (column.icon) {
			case 'DeleteOutlinedIcon':
				return (
					<div className='icon'>
						<DeleteOutlinedIcon
							className='delete-icon'
							onClick={(e) => handleAction(column.action, item, e)}
						/>
					</div>
				)
		}
	}

	const renderText = (column, item) => {
		return _.get(item, column.keyPath)
	}
	const renderColumnData = (column, item) => {
		switch (column.type) {
			case 'text':
				return renderText(column, item)
			case 'iconWithText':
				return renderAvatar(column, item)
			case 'select':
				return (
					<Can
						action={column.authorization}
						yes={() => renderDropdown(column, item)}
						no={() => ''}
					></Can>
				)
			case 'comma-separeted':
				return renderCommaSeparetedValues(column, item)
			case 'checkbox':
				return (
					column.isHover &&
					column.type === 'checkbox' && (
						<Can
							action={column.authorization}
							yes={() => renderCheckbox(column, item)}
							no={() => ''}
						></Can>
					)
				)
			case 'icon':
				return (
					<Can
						action={permission.CREATE_PLATFORM}
						yes={() => renderIcon(column, item)}
						no={() => ''}
					></Can>
				)
		}
	}

	return (
		<Fragment>
			<TableCell
				className={`values ${colName.valueClass}`}
				onClick={(event) => handleViewItem(colName.type, item, event)}
			>
				{renderColumnData(colName, item)}
			</TableCell>
		</Fragment>
	)
}

GridTableCell.propTypes = {
	colName: PropTypes.object,
	item: PropTypes.object,
}
