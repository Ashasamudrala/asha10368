import React from 'react'
import PropTypes from 'prop-types'
import { TableRow } from '@material-ui/core'
import { GridTableCell } from './gridTableCell/GridTableCell'

export function GridTableRow(props) {
	const { properties, item } = props

	const handleAction = (actionType, item, e) => {
		props.handleAction(actionType, item, e)
	}
	return (
		<>
			<TableRow
				hover
				className={`table-row table-row-hover ${
					item && item.checked ? 'table-row-bgcolor' : ''
				}`}
				key={item.id}
			>
				{properties.columns.map((colName, i) =>
					colName.selected ? <img src={colName.path} /> : ''
				)}
				{properties.columns.map((colName, i) => {
					return (
						<GridTableCell
							key={i}
							colName={colName}
							item={item}
							handleAction={handleAction}
							handleViewItem={props.handleViewItem}
						/>
					)
				})}
			</TableRow>
		</>
	)
}

GridTableRow.propTypes = {
	properties: PropTypes.object,
	item: PropTypes.object,
}
