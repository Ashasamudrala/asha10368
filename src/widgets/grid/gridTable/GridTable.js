import React, { Fragment, useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import {
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TablePagination,
	TableRow,
} from '@material-ui/core'
import TableHeader from '../gridHeader/GridHeader'
import { PLATFORM_TRANSLATIONS, ASC, DESC } from '../../../utilities/constants'
import { GridTableRow } from './gridTableRow/GridTableRow'

/**
 * Represents a Grid Table.
 * @param {object} props - data from parent component.
 */
function GridTable(props) {
	const { properties, data } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)

	/**
	 * Declaring table header names for default sorting.
	 */
	const [orderBy, setOrderBy] = useState(properties.defaultSorting)

	/**
	 * If pagination is true
	 * Displaying the grid data based on the selected value from rowsPerPage selection.
	 */
	const [page, setPage] = useState(0)

	/**
	 * Declaring here 'asc' or 'desc'.
	 */
	const [order, setOrder] = useState(t(ASC))

	/**
	 * If pagination is true
	 * Displaying the grid data rows for Each page based on the json value.
	 */
	const [rowsPerPage, setRowsPerPage] = useState(
		properties.pagination ? properties.rowsPerPage : data.length
	)
	const handleViewItem = (isOpen, viewItem) => {
		props.handleViewItem && props.handleViewItem(isOpen, viewItem)
	}
	const handleAction = (actionType, item, e) => {
		props.handleAction(actionType, item, e)
	}

	/**
	 * Represents a descending and ascending order.
	 * @param {object} a - object of the column.
	 * @param {object} b - object of the column.
	 * @param {string} orderBy - type of the column.
	 */
	const descendingComparator = (a, b, orderBy) => {
		if (b[orderBy] < a[orderBy]) {
			return -1
		}
		if (b[orderBy] > a[orderBy]) {
			return 1
		}
		return 0
	}

	/**
	 * Represents a sorting the data.
	 * @param {string} order - passing desc or asc.
	 * @param {string} orderBy - passing grid header name.
	 */
	const getComparator = (order, orderBy) => {
		return order === t(DESC)
			? (a, b) => descendingComparator(a, b, orderBy)
			: (a, b) => -descendingComparator(a, b, orderBy)
	}

	/**
	 * Represents a sorting the data.
	 * @param {array} array - list of arrays.
	 * @param {function} comparator - callback function.
	 */
	const sortingData = (array, comparator) => {
		const sortingByOrder = array.map((el, index) => [el, index])
		sortingByOrder.sort((a, b) => {
			const order = comparator(a[0], b[0])
			if (order !== 0) return order
			return a[1] - b[1]
		})
		return sortingByOrder.map((el) => el[0])
	}

	/**
	 * Represents a handleRequestSort for a data.
	 * @param {event } event - events.
	 * @param {string} property - name of grid header.
	 */
	const handleRequestSort = (event, property) => {
		const isAsc = orderBy === property && order === t(ASC)
		setOrder(isAsc ? t(DESC) : t(ASC))
		setOrderBy(property)
	}

	/**
	 * Represents a handleChangePage.
	 * @param {event } event - events.
	 * @param {number} newPage - rows per page.
	 */
	const handleChangePage = (event, newPage) => {
		setPage(newPage)
	}

	/**
	 * Represents a handleChangeRowsPerPage.
	 * @param {event } event - selecte number from rowsPerPage dropdown.
	 */
	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(parseInt(event.target.value, 10))
		setPage(0)
	}

	useEffect(() => {
		setRowsPerPage(data.length)
	}, [data])
	return (
		<Fragment>
			<TableContainer>
				<Table>
					<TableHeader
						properties={properties}
						order={order}
						orderBy={orderBy}
						rowCount={data.length}
						onRequestSort={handleRequestSort}
						deleteBulkIds={props.deleteBulkIds}
						items={data}
						handleHeaderAction={props.handleHeaderAction}
					/>
					<TableBody>
						{data.length > 0 ? (
							sortingData(data, getComparator(order, orderBy))
								.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
								.map((item, index) => {
									return (
										<GridTableRow
											properties={properties}
											item={item}
											key={index}
											handleViewItem={handleViewItem}
											handleAction={handleAction}
										/>
									)
								})
						) : (
							<TableRow className='table-row'>
								<TableCell
									colSpan={properties.columns.length}
									className='no-data-found'
								>
									{properties.noDataMessage}
								</TableCell>
							</TableRow>
						)}
					</TableBody>
				</Table>
			</TableContainer>
			{properties.pagination ? (
				<TablePagination
					rowsPerPageOptions={[5, 10, 25]}
					component='div'
					count={data.length}
					rowsPerPage={rowsPerPage}
					page={page}
					onChangePage={handleChangePage}
					onChangeRowsPerPage={handleChangeRowsPerPage}
				/>
			) : (
				''
			)}
		</Fragment>
	)
}

GridTable.propTypes = {
	onRequestSort: PropTypes.func,
	order: PropTypes.string,
	orderBy: PropTypes.string,
	rowCount: PropTypes.number,
	handleChangeRowsPerPage: PropTypes.func,
	handleChangePage: PropTypes.func,
	rowsPerPage: PropTypes.number,
}
export default GridTable
