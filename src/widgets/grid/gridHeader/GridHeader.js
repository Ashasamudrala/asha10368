import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import {
	TableHead,
	TableRow,
	TableCell,
	TableSortLabel,
	Checkbox,
} from '@material-ui/core'
import { PLATFORM_TRANSLATIONS, ASC } from '../../../utilities/constants'
import Can from '../../../widgets/auth/Can'
import './gridHeader.scss'

/**
 * Represents a Grid Header.
 * @param {object} props
 */
export default function GridHeader(props) {
	const { properties, onRequestSort, order, orderBy, items } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)

	/**
	 * Represents a createSortHandler the data.
	 * @param {string} property - select header name on clicking sorting arrows.
	 */
	const createSortHandler = (property) => (event) => {
		onRequestSort(event, property)
	}
	let checkBoxVisible = false
	let rowCount = 0
	let numSelected = 0
	items.forEach((data) => {
		if (data.checked === true) {
			rowCount = rowCount + 1
			numSelected = numSelected + 1
		}
	})
	if (rowCount > 0) {
		rowCount = items.length
	}
	checkBoxVisible = items.some((data) => {
		if (data.checked === true) {
			return true
		}
	})

	return (
		<TableHead>
			<TableRow className='table-row table-row-hover'>
				{properties.columns.map((item, index) => {
					return (
						<TableCell key={index} className={`headers ${item.headerClass}`}>
							{item.type === 'checkbox' && item.permission === 'permission' ? (
								<Can
									action={item.authorization}
									yes={() => (
										<div
											className={`${
												items && checkBoxVisible ? '' : 'isVisible'
											}`}
										>
											<Checkbox
												className={`checkbox-class `}
												indeterminate={
													items && numSelected > 0 && numSelected < rowCount
												}
												checked={
													items && rowCount > 0 && numSelected === rowCount
												}
												onChange={(e) => props.handleHeaderAction(e, item)}
												inputProps={{ 'aria-label': 'indeterminate checkbox' }}
											/>
										</div>
									)}
									no={() => ''}
								></Can>
							) : item.permission === 'permission' ? (
								<Can
									action={item.authorization}
									yes={() => item.label}
									no={() => ''}
								></Can>
							) : (
								item.label
							)}

							{properties.sorting && (
								<TableSortLabel
									direction={orderBy === item.type ? order : t(ASC)}
									onClick={createSortHandler(item.type)}
								/>
							)}
						</TableCell>
					)
				})}
			</TableRow>
		</TableHead>
	)
}
GridHeader.propTypes = {
	createSortHandler: PropTypes.func,
	order: PropTypes.string,
	orderBy: PropTypes.string,
}
