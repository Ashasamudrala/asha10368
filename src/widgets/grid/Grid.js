import React, { useState, Fragment } from 'react'
import PropTypes from 'prop-types'

import { Paper } from '@material-ui/core'
import Search from '../search/Search'
import './grid.scss'
import GridTable from './gridTable/GridTable'
import _ from 'lodash'

export default function Grid(props) {
	const { properties, data } = props

	/**
	 * Searching grid table data based on given characters in search box.
	 */
	const [qdata, setQData] = useState('')

	const handleViewItem = (isOpen, viewItem) => {
		props.handleViewItem(isOpen, viewItem)
	}
	const handleDeleteItem = (isDelete, id) => {
		props.handleViewItem(isDelete, id)
	}

	/**
	 * Represents a search data from search bar.
	 * @param {string } searchingData - filter the data based on searching characters.
	 */
	const searchData = (searchingData) => {
		setQData(searchingData)
	}
	/**
	 * Represents a searching data.
	 * @param {array } rows - filter the datalist based on the searching characters.
	 */
	function search(rows) {
		return rows.filter((row) =>
			properties.columns.some(
				(column) =>
					column.keyPath &&
					_.get(row, column.keyPath)
						.toString()
						.toLowerCase()
						.indexOf(qdata.toLowerCase()) > -1
			)
		)
	}
	return (
		<Fragment>
			<Search searchData={searchData} searching={properties.searching} />
			<Paper className='grid-list'>
				<GridTable
					handleViewItem={handleViewItem}
					handleDeleteItem={handleDeleteItem}
					data={search(data)}
					properties={properties}
				/>
			</Paper>
		</Fragment>
	)
}
Grid.propTypes = {
	search: PropTypes.func,
	properties: PropTypes.object,
	data: PropTypes.any,
}
