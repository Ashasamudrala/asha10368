import React from 'react'
import ChipInput from 'material-ui-chip-input'
import './Versions.scss'
import Chip from '@material-ui/core/Chip'
import { ADD, PLATFORM_TRANSLATIONS, DELETE } from '../../utilities/constants'
import { useTranslation } from 'react-i18next'

/**
 *
 * @param {*} props from formbuilder component
 */
export default function Versions(props) {
	const { name, value, errors } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)

	// this is used for adding the versions into textfield
	const handleAddVersions = (version) => {
		props.handleVersions(version.split(' ').join(''), name, t(ADD))
	}

	// this is used for deleting the versions into textfield
	const handleDeleteVersions = (version, index) => {
		props.handleVersions(index, name, t(DELETE))
	}

	const chipRenderer = ({ chip, className, handleClick, isFocused }, key) => (
		<Chip
			className={className}
			key={key}
			label={chip}
			style={{
				backgroundColor: isFocused ? '#e34d28' : '',
				color: isFocused ? '#ffffff' : '',
			}}
			onClick={handleClick}
		/>
	)
	return (
		<div className='versions'>
			<label className='form-label'>
				{`${t(props.label)}`}
				{props.required ? (
					<span className='form-label-required'> * </span>
				) : null}
			</label>
			<ChipInput
				value={value}
				className={`${errors && errors.versions ? 'error' : ''}`}
				onAdd={(version) => handleAddVersions(version)}
				chipRenderer={chipRenderer}
				onDelete={(version, index) => handleDeleteVersions(version, index)}
				disableUnderline={true}
				InputProps={{
					inputProps: { maxLength: 10 },
					autoFocus: props.autoFocus,
				}}
				placeholder={t(props.placeholder)}
			/>
			{errors && errors.versions && (
				<span className='invalid-message'>{t(errors.versions)}</span>
			)}
		</div>
	)
}
