import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './autoCompleteSearch.scss'
import Search from '../search/Search'

/**
 * Represent a AutoCompleteSearch Component.
 * @param {string} searchData - enter characters from search box.
 * @param {boolean} searching - To show component.
 */
export default function AutoCompleteSearch(props) {
	const {
		searching,
		showSuggestions,
		suggestions,
		cancelIcon,
		iconPosition,
		searchIcon,
		handleSearch,
		searchPlaceholder,
		handleChange,
	} = props

	const [userInput, setUserInput] = useState('')

	/**
	 * Represents a handleSuggestions.
	 * @param {events } e - setting characters through input text box.
	 */

	const handleSuggestions = (e) => {
		setUserInput(e.target.innerText)
		props.handleSuggestions(
			e,
			searching && searching.name,
			searching && searching.actionType
		)
	}
	const handleCancel = () => {
		setUserInput('')
		props.handleCancel()
	}
	return (
		<div>
			<Search
				value={userInput}
				searching={searching}
				searchIcon={searchIcon}
				handleSearch={handleSearch}
				cancelIcon={cancelIcon}
				searchPlaceholder={searchPlaceholder}
				handleChange={handleChange}
				handleCancel={handleCancel}
				iconPosition={iconPosition}
			/>
			{showSuggestions && (
				<div>
					<ul className='listbox'>
						{suggestions &&
							suggestions.map((suggestion) => (
								<li key={suggestion.key} onClick={handleSuggestions}>
									{suggestion.value}
								</li>
							))}
					</ul>
				</div>
			)}
		</div>
	)
}
AutoCompleteSearch.propTypes = {
	suggestions: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
			value: PropTypes.string.isRequired,
		})
	),
	value: PropTypes.string,
	searching: PropTypes.bool.isRequired,
	showSuggestions: PropTypes.bool.isRequired,
	iconPosition: PropTypes.string,
	searchIcon: PropTypes.bool,
	cancelIcon: PropTypes.bool.isRequired,
	handleCancel: PropTypes.func,
	searchPlaceholder: PropTypes.string,
	handleSuggestions: PropTypes.func.isRequired,
	handleSearch: PropTypes.func.isRequired,
	handleChange: PropTypes.func,
}
