import Team from '../features/team/Team'
import Apps from '../features/apps/Apps'
import Devops from '../features/devops/Devops'
import Core from '../features/core/Core'

const topNavcomponents = {
	Apps: Apps,
	Team: Team,
	Core: Core,
	Devops: Devops,
}
export default topNavcomponents
