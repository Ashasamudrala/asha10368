import React from 'react'
import IsyButton from '../button/button'
import { useTranslation } from 'react-i18next'
import {
	BUTTON_CANCEL,
	BUTTON_SAVE,
	PLATFORM_TRANSLATIONS,
	modeType,
} from '../../utilities/constants'
/* 
this is component in right slider that it has save and close button
 */
export default function SliderFooter(props) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const { mode } = props
	return (
		<>
			<footer className='slider-footer'>
				{mode === modeType.WRITE ? (
					<IsyButton
						buttonClass='save-btn'
						handleButtonOnClick={() => {
							props.handleSave()
						}}
					>
						{t(BUTTON_SAVE)}
					</IsyButton>
				) : null}
				<IsyButton
					buttonClass='cancel-btn'
					handleButtonOnClick={() => {
						props.handleClose(false)
					}}
				>
					{t(BUTTON_CANCEL)}
				</IsyButton>
			</footer>
		</>
	)
}
