import React from 'react'
import Drawer from '@material-ui/core/Drawer'
import './rightSideSlider.scss'
/*
 this is used for getting the slider from right side and it gets contents as children prop
*/

export default function RightSideSlider(props) {
	// when ever close function is called this method executes
	const handleClose = () => {
		props.onCancel(false)
	}
	return (
		<Drawer
			className={props.drawerName}
			open={true}
			anchor='right'
			onClose={handleClose}
			disablePortal
			disableBackdropClick={true}
		>
			{props.children}
		</Drawer>
	)
}
