import React, { useState } from 'react'
import PropTypes from 'prop-types'
import SearchIcon from '@material-ui/icons/Search'
import {
	DEFAULT_SEARCH,
	PLATFORM_TRANSLATIONS,
} from '../../utilities/constants'
import CancelIcon from '@material-ui/icons/Cancel'
import { useTranslation } from 'react-i18next'
import _ from 'lodash'
import './search.scss'

/**
 * Represent a Search Component.
 * @param {string} searchData - enter characters from search box.
 * @param {boolean} searching - To show component.
 */
export default function Search(props) {
	const {
		searching,
		searchPlaceholder,
		iconPosition,
		searchIcon,
		cancelIcon,
		value,
	} = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const [q, setQ] = useState('')

	/**
	 * Represents a handleChange.
	 * @param {events } e - setting characters through input text box.
	 */
	const handleChange = (e) => {
		setQ(e.target.value)
		props.handleChange(
			e,
			searching && searching.name,
			searching && searching.actionType
		)
	}
	const handleSearch = () => {
		props.handleSearch(
			q,
			searching && searching.name,
			searching && searching.actionType
		)
	}
	const handleCancel = () => {
		setQ('')
		props.handleCancel()
	}
	const handleKeyDown = (e) => {
		if (e.keyCode === 13) {
			props.handleSearch(
				q,
				searching && searching.name,
				searching && searching.actionType
			)
		}
		if (e.keyCode === 8) {
			props.handleCancel()
		}
	}
	return (
		<>
			{searching ? (
				<SearchComponent
					handleChange={handleChange}
					handleKeyDown={handleKeyDown}
					handleSearch={handleSearch}
					value={value || q}
					handleCancel={handleCancel}
					searchIcon={searchIcon}
					cancelIcon={cancelIcon}
					searchPlaceholder={
						!_.isEmpty(searchPlaceholder)
							? searchPlaceholder
							: t(DEFAULT_SEARCH)
					}
					ref={props.searchRef}
					iconPosition={iconPosition}
				/>
			) : null}
		</>
	)
}
// eslint-disable-next-line react/display-name
const SearchComponent = React.forwardRef((props, ref) => {
	return (
		<div className='search-bar'>
			{props.iconPosition === 'right' && props.searchIcon && (
				<div className='search-icon'>
					<SearchIcon onClick={props.handleSearch} />
				</div>
			)}
			<input
				type='text'
				className='search-text'
				placeholder={props.searchPlaceholder}
				value={props.value}
				name='searchString'
				onChange={props.handleChange}
				onKeyDown={props.handleKeyDown}
				ref={ref}
			/>
			{props.cancelIcon && (
				<div className='cancel-icon'>
					<CancelIcon onClick={props.handleCancel} />
				</div>
			)}
			{props.iconPosition === 'left' && props.searchIcon ? (
				<div className='search-icon'>
					<SearchIcon onClick={props.handleSearch} />
				</div>
			) : (
				''
			)}
		</div>
	)
})
Search.propTypes = {
	handleChange: PropTypes.func,
	order: PropTypes.string,
	orderBy: PropTypes.string,
}
