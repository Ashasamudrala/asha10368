import React from 'react'
import PropTypes from 'prop-types'
import './textarea.scss'
/*
It is reusable input component and gets data as props
this component gets the required fields of textarea to display
 */
export default function Textarea(props) {
	return (
		<div className='dynamic-textarea'>
			{props.title && (
				<label className='form-label'>
					{`${props.title}`}
					{props.required ? (
						<span className='form-label-required'> *</span>
					) : null}
				</label>
			)}
			<textarea
				className={`form-control ${props.className ? props.className : ''}`}
				style={props.resize ? null : { resize: 'none' }}
				name={props.name}
				value={props.content}
				onChange={props.onChange}
				disabled={props.disabled}
				placeholder={props.placeholder}
			/>
		</div>
	)
}
Textarea.propTypes = {
	title: PropTypes.string,
	name: PropTypes.string.isRequired,
	content: PropTypes.string,
	resize: PropTypes.bool,
	placeholder: PropTypes.string,
	controlFunc: PropTypes.func,
	required: PropTypes.bool,
}
