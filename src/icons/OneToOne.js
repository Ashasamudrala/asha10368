import React from 'react'

function OneToOne(props) {
	return (
		<svg {...props}>
			<g
				id='Data-Modelling'
				stroke='none'
				strokeWidth='1'
				fill='none'
				fillRule='evenodd'
			>
				<g id='50_DB---Relationship-Detail-Panel-Normal-State'>
					<g id='Group-2'>
						<g id='one-to-one'>
							<rect id='Rectangle' x='0' y='0' width='37' height='32'></rect>
							<line
								x1='11'
								y1='15.5'
								x2='27'
								y2='15.5'
								id='Path-4'
								stroke='#232323'
								strokeWidth='1.6'
								strokeLinecap='round'
								strokeLinejoin='round'
							></line>
						</g>
					</g>
				</g>
			</g>
		</svg>
	)
}

export default OneToOne
