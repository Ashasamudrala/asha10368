import React from 'react'

function OneToMany(props) {
	return (
		<svg {...props}>
			<g
				id='Data-Modelling'
				stroke='none'
				strokeWidth='1'
				fill='none'
				fillRule='evenodd'
			>
				<g id='50_DB---Relationship-Detail-Panel-Normal-State'>
					<g id='Group-2'>
						<g id='one-to-many'>
							<rect id='Rectangle' x='0' y='0' width='32' height='32'></rect>
							<path
								d='M9,16.3846154 L25,16.3846154 M9,21 L18.9047619,16.3846154 M9,11 L18.9047619,16.3846154'
								id='Combined-Shape'
								stroke='#232323'
								strokeWidth='1.6'
								strokeLinecap='round'
								strokeLinejoin='round'
							></path>
						</g>
					</g>
				</g>
			</g>
		</svg>
	)
}

export default OneToMany
