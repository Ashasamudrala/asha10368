import React from 'react'
// import { SvgIcon } from '@material-ui/core'

function ManyToMany(props) {
	return (
		<svg {...props}>
			<g
				id='Data-Modelling'
				stroke='none'
				strokeWidth='1'
				fill='none'
				fillRule='evenodd'
			>
				<g id='50_DB---Relationship-Detail-Panel-Normal-State'>
					<g id='Group-2'>
						<g id='Many-to-many'>
							<rect id='Rectangle' x='0' y='0' width='32' height='32'></rect>
							<path
								d='M9,16.3846154 L25,16.3846154 M9,21 L17.3825675,16.3846154 M9,11 L17.3825675,16.3846154 M16.6168522,16.3846154 L24.9994198,21 M16.6168522,16.3846154 L24.9994198,11'
								id='Combined-Shape'
								stroke='#232323'
								strokeWidth='1.6'
								strokeLinecap='round'
								strokeLinejoin='round'
							></path>
						</g>
					</g>
				</g>
			</g>
		</svg>
	)
}

export default ManyToMany
