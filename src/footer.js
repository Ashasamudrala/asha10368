import React, { Component } from 'react';
import './second.css';
class Footer extends Component {
    render() {
        return (
            <div>
            <div>
                <h1 style={{ marginLeft: "150px", fontSize: "25px" }} ><b>Algorithms</b></h1>
                    <div class="grid-container1">
                        <div class="grid-item1">
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/search_55.png" class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down" >Search</p>

                            </div>
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/sorting_55.png " style={{ height: "50px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <p class="down" style={{ top: "50%" }} >Sorting</p>

                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/recursion_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Recursion</p>

                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/greedy_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">GREEDY ALGORITHM</p>

                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/bit_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Bit Pattern</p>
                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/dynamic_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Dynamic Programming</p>
                            </div>
                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/backtracking_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Back Tracking</p>
                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/mathematical_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Mathematical</p>
                            </div>
                        </div>


                    </div>
                </div>
                <div>
                    <h1 style={{ marginLeft: "150px", fontSize: "25px" }} ><b>
                        Company Interview Problems</b></h1>
                    <div class="grid-container1">
                        <div class="grid-item1">
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/comp_prep_amazon_55.png" style={{ height: "50px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />

                            </div>
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/adobe_interview_prep_55.png " style={{ height: "50px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />


                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/oracle_interview_prep_55.png" style={{ height: "30px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />


                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/goldman-sachs-logo.jpg" style={{ height: "30px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />


                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/microsoft_interview_prep_55.png" style={{ height: "30px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />

                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/directi-logo.jpg" style={{ height: "30px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />

                            </div>
                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/directi-logo.jpg" style={{ height: "30px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />

                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/Morgan-Stanley-logo_55.png" style={{ height: "30px", width: "130px", marginTop: "30px", marginLeft: "10px" }} class="btn-social" />
                            </div>
                        </div>


                    </div>
                </div>
                <div>
                    <div class="grid-container " style={{ marginTop: "10px" }}>
                        <div class="grid-item">
                            <ol>
                                <li><img src="https://d3qt53jevtbvi2.cloudfront.net/common/GeeksforGeeksLogoFooterSmall.png" /></li>
                                <li><p style={{ color: "white", fontSize: "20px" }}>710-B, Advant Navis Business Park,
                                Sector-142, Noida, Uttar Pradesh - 201305 feedback@geeksforgeeks.org</p></li>
                            </ol>
                        </div>
                        <div class="grid-item" >
                            <ol>
                                <li><b style={{ color: "white", fontSize: "15px" }}>Company</b></li>
                                <li><a href="https://www.geeksforgeeks.org/fundamentals-of-algorithms/">Algorithms</a></li>
                                <li><a href="https://www.geeksforgeeks.org/data-structures/">Data Structures</a></li>
                                <li><a href="https://www.geeksforgeeks.org/category/program-output/">Languages</a></li>
                                <li><a href="https://www.geeksforgeeks.org/articles-on-computer-science-subjects-gq/">CS Subjects</a></li>

                            </ol></div>
                        <div class="grid-item"><ol>
                            <li><b style={{ color: "white", fontSize: "15px" }}>LEARN</b></li>
                            <li><a href="https://www.geeksforgeeks.org/data-structures/">Alogrithms</a></li>
                            <li><a href="https://www.geeksforgeeks.org/category/program-output/">Data Structures</a></li>
                            <li><a href="https://www.geeksforgeeks.org/articles-on-computer-science-subjects-gq/">CS Subjects</a></li>

                        </ol></div>
                        <div class="grid-item"><ol>
                            <li><b style={{ color: "white", fontSize: "15px" }}>PRACTISE</b></li>
                            <li><a href="https://www.geeksforgeeks.org/fundamentals-of-algorithms/">Companywise </a></li>
                            <li><a href="https://www.geeksforgeeks.org/data-structures/">Topic-wise</a></li>
                            <li><a href="https://www.geeksforgeeks.org/category/program-output/">Contents</a></li>
                            <li><a href="https://www.geeksforgeeks.org/articles-on-computer-science-subjects-gq/">Subjective Questions</a></li>

                        </ol></div>
                        <div class="grid-item"><ol>
                            <li><b style={{ color: "white", fontSize: "15px" }}>Contribute</b></li>
                            <li><a href="https://www.geeksforgeeks.org/fundamentals-of-algorithms/" >write an article</a></li>
                            <li> <a href="https://www.geeksforgeeks.org/data-structures/" style={{ textAlign: "left" }}>Internships</a></li>
                            <li><a href="https://www.geeksforgeeks.org/category/program-output/" style={{ textAlign: "left" }}>Videos</a></li>
                            <li><a href="https://www.geeksforgeeks.org/articles-on-computer-science-subjects-gq/" style={{ textAlign: "left" }}>Interview Experiences</a></li>

                        </ol>
                        </div>

                    </div>

                </div>
                </div>
               
        );
    }
}
export default Footer;