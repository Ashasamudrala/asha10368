import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Switch, Route, Redirect, useHistory } from 'react-router-dom'
import { Container } from '@material-ui/core'
import RoutePaths from '../src/config/routes.json'
import Breadcrumb from '../src/widgets/breadcrumb/Breadcrumb'
import components from '../src/widgets/components'
import topNavigationComponents from '../src/widgets/topNavigationComponents'
import NotificationPopup from '../src/infrastructure/notificationPopup'
import loginAndSignUpComponents from './features/loginAndSignUp'
import TopNavigation from './widgets/topNavigation/TopNavigation'
import SideNavbar from './widgets/sideBar/SideBar'
import { selectGetLoginUserId } from './authAndPermissions/loginUserDetails.selectors'
import { isNil } from 'lodash'
import { logInUserOnRefresh } from './authAndPermissions/loginUserDetails.asyncActions'
import { Dialogs } from './infrastructure/dialogs/Dialogs'

export default function Routes() {
	const logInUserId = useSelector(selectGetLoginUserId)
	const history = useHistory()
	const dispatch = useDispatch()

	useEffect(() => {
		if (isNil(logInUserId)) {
			const jwtToken = sessionStorage.getItem('accessToken')
			if (isNil(jwtToken)) {
				history.push('/login')
			} else {
				dispatch(logInUserOnRefresh())
			}
		}
	})

	const renderTopNavigation = () => {
		if (!isNil(logInUserId)) {
			return <TopNavigation />
		}
	}

	const renderTopNavigationViews = () => {
		return RoutePaths.topNavigation.map((route) => {
			const RouteComponent = topNavigationComponents[route.componentName]
			/**
			 * If there is any child component, then the if part will execute
			 */
			if (route.children && route.children.length > 0) {
				return route.children.map((child) => {
					const ChildComponent = components[child.componentName]
					return (
						/**
						 * As per screens and what we discussed with UX team,
						 * we will have braedcrumb only for child components.
						 */
						<PageRoute path={child.path} key={child.id}>
							<div className='notification-container'>
								<Breadcrumb path={child.path} />
								<NotificationPopup />
							</div>
							<ChildComponent />
						</PageRoute>
					)
				})
			} else {
				/**
				 * If there is only parent component, then the else part will execute
				 */
				return (
					<PageRoute path={route.path} key={route.id}>
						<div className='route-without-breadcrum'>
							<NotificationPopup />
						</div>
						<RouteComponent />
					</PageRoute>
				)
			}
		})
	}

	const renderSideNavBar = () => {
		return RoutePaths.sideNavigation.map((route) => {
			/**
			 * If there is any child component, then the if part will execute
			 */
			if (route.children && route.children.length > 0) {
				return route.children.map((child) => {
					const ChildComponent = components[child.componentName]
					return (
						/**
						 * As per screens and what we discussed with UX team,
						 * we will have braedcrumb only for child components.
						 */
						<PageRoute path={child.path} key={child.id}>
							<Breadcrumb path={child.path} />
							<NotificationPopup />
							<ChildComponent />
						</PageRoute>
					)
				})
			} else {
				/**
				 * If there is only parent component, then the else part will execute
				 */
				return <Route path={route.path} component={SideNavbar} />
			}
		})
	}

	const renderLoginAndSignUpRouts = () => {
		return RoutePaths.loginAndSignUpRoutes.map((route) => {
			const RouteComponent = loginAndSignUpComponents[route.componentName]
			return (
				<PageRoute path={route.path} key={route.id}>
					<div className='login-alert'>
						<NotificationPopup />
					</div>
					<RouteComponent />
				</PageRoute>
			)
		})
	}

	const renderRoutes = () => {
		return (
			<div>
				<Switch>
					<Redirect exact from='/' to={RoutePaths.defaultRoute} />
					{renderLoginAndSignUpRouts()}
					{renderSideNavBar()}
					{renderTopNavigationViews()}
				</Switch>
			</div>
		)
	}

	return (
		<>
			{renderTopNavigation()}
			{renderRoutes()}
			<Dialogs />
		</>
	)
}

function PageRoute({ children, ...rest }) {
	return (
		<Route {...rest}>
			<Container fixed={false}>{children}</Container>
		</Route>
	)
}
