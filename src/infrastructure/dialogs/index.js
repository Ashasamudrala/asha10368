import { Dialogs } from './Dialogs'
import * as selectors from './base.selector'
import * as dialogs from './dialogs.slice'
import * as createDatabase from './createDatabase/createDatabase.slice'
import * as connectForeignKey from './connectForeignKey/connectForeignKey.slice'
import { combineReducers } from 'redux'

export const reducer = combineReducers({
	[dialogs.name]: dialogs.reducer,
	[connectForeignKey.name]: connectForeignKey.reducer,
	[createDatabase.name]: createDatabase.reducer,
})
export const name = selectors.sliceName

// we export the component most likely to be desired by default
export default Dialogs
