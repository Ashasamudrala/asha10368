import React from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector, useDispatch } from 'react-redux'
import { Typography } from '@material-ui/core'
import {
	DATABASE_NAME,
	DATABASE_TRANSLATIONS,
	DATABASE_NAME_PLACEHOLDER,
} from '../../../utilities/constants'
import { getName } from './createDatabase.selectors'
import { actions } from './createDatabase.slice'
import { IsyInput } from '../../../widgets/isyInput/IsyInput'
import './createDatabase.scss'

export function CreateDatabaseContent() {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const name = useSelector(getName)
	const dispatch = useDispatch()

	const handleNameChange = (value) => {
		dispatch(actions.setName(value))
	}

	return (
		<div className='createDataBase'>
			<Typography>{t(DATABASE_NAME)}</Typography>
			<IsyInput
				type='text'
				name='name'
				placeholder={t(DATABASE_NAME_PLACEHOLDER)}
				onBlur={handleNameChange}
				value={name}
			/>
		</div>
	)
}
