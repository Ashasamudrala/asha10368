import slice from './createDatabase.slice'
import { selectSlice as dialogsSelect } from '../base.selector'

export const selectSlice = (state) => dialogsSelect(state)[slice.name]

export const getName = (state) => selectSlice(state).name
