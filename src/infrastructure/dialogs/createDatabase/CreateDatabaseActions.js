import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	BUTTON_CANCEL,
	BUTTON_CREATE,
} from '../../../utilities/constants'
import Button from '../../../widgets/button'
import { getName } from './createDatabase.selectors'
import { actions } from './createDatabase.slice'
import { isEmpty } from 'lodash'

export function CreateDatabaseActions(props) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const name = useSelector(getName)

	const dispatch = useDispatch()

	const handleCancel = () => {
		props.onCancel()
		dispatch(actions.clearData())
	}

	const handleOkay = () => {
		props.onOkay({
			name,
		})
		dispatch(actions.clearData())
	}

	return (
		<>
			<Button handleButtonOnClick={handleCancel} buttonClass='cancel-btn'>
				{t(BUTTON_CANCEL)}
			</Button>
			<Button
				buttonClass='save-btn'
				disabled={isEmpty(name)}
				handleButtonOnClick={handleOkay}
			>
				{t(BUTTON_CREATE)}
			</Button>
		</>
	)
}
