import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	name: '',
}

const slice = createSlice({
	name: 'createDatabase',
	initialState,
	reducers: {
		// synchronous actions
		setName(state, action) {
			state.name = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
