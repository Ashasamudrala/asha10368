import slice from './dialogs.slice'
import { selectSlice as dialogsSelect } from './base.selector'

export const selectSlice = (state) => dialogsSelect(state)[slice.name]

export const selectOrderedDialogs = (state) => selectSlice(state).ordered

export const selectDialogsMap = (state) => selectSlice(state).map
