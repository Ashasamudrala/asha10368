import React from 'react'
import { DialogContentText } from '@material-ui/core'

export function ConfirmationDialogContent(props) {
	const { dialogData } = props
	return (
		<DialogContentText>
			<span className='content'>{dialogData.message}</span>
		</DialogContentText>
	)
}
