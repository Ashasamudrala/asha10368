import React from 'react'
import { isString } from 'lodash'
import { useTranslation } from 'react-i18next'
import {
	BUTTON_YES,
	BUTTON_NO,
	PLATFORM_TRANSLATIONS,
} from '../../../utilities/constants'
import Button from '../../../widgets/button'

export function ConfirmationDialogActions(props) {
	const { dialogData } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const getYesButtonLabel = () => {
		if (isString(dialogData.okayButtonLabel)) {
			return dialogData.okayButtonLabel
		}
		return t(BUTTON_YES)
	}
	const getCancelButtonLabel = () => {
		if (isString(dialogData.cancelButtonLabel)) {
			return dialogData.cancelButtonLabel
		}
		return t(BUTTON_NO)
	}

	const handleCancel = () => {
		props.onCancel()
	}

	const handleOkay = () => {
		props.onOkay()
	}

	return (
		<>
			<Button handleButtonOnClick={handleOkay} buttonClass='save-btn'>
				{getYesButtonLabel()}
			</Button>
			<Button handleButtonOnClick={handleCancel} buttonClass='cancel-btn'>
				{getCancelButtonLabel()}
			</Button>
		</>
	)
}
