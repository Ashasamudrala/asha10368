import { v4 } from 'uuid'
import { createSlice } from '@reduxjs/toolkit'
import { omit, filter, isFunction } from 'lodash'

const okayFns = {}
const cancelFns = {}

const initialState = {
	ordered: [],
	map: {},
}

const slice = createSlice({
	name: 'dialogsData',
	initialState,
	reducers: {
		// synchronous actions
		addDialog(state, action) {
			// action.payload must contain type, data, onCancel, onOkay, title
			const id = v4()
			okayFns[id] = action.payload.onOkay
			cancelFns[id] = action.payload.onCancel
			state.ordered = [id, ...state.ordered]
			state.map = {
				[id]: omit(action.payload, 'onCancel', 'onOkay'),
				...state.map,
			}
		},
		removeDialog(state, action) {
			const id = action.payload.id
			const isOkay = action.payload.isOkay
			setTimeout(() => {
				if (isOkay && isFunction(okayFns[id])) {
					okayFns[id](action.payload.data)
				} else if (isFunction(cancelFns[id])) {
					cancelFns[id]()
				}
				delete okayFns[id]
				delete cancelFns[id]
			})
			state.ordered = filter((innerId) => id !== innerId)
			state.map = omit(state.map, id)
		},
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
