import React from 'react'
import { isString } from 'lodash'
import { useTranslation } from 'react-i18next'
import { BUTTON_OK, PLATFORM_TRANSLATIONS } from '../../../utilities/constants'
import Button from '../../../widgets/button'

export function AlertActions(props) {
	const { dialogData } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const getYesButtonLabel = () => {
		if (isString(dialogData.okayButtonLabel)) {
			return dialogData.okayButtonLabel
		}
		return t(BUTTON_OK)
	}

	const handleOkay = () => {
		props.onOkay()
	}

	return (
		<Button handleButtonOnClick={handleOkay} buttonClass='save-btn'>
			{getYesButtonLabel()}
		</Button>
	)
}
