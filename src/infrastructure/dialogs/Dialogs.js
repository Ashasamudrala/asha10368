import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectOrderedDialogs, selectDialogsMap } from './dialogs.selectors'
import { actions } from './dialogs.slice'
import { isNil, isString } from 'lodash'
import * as dialogType from './dialogs.constants'
import { useTranslation } from 'react-i18next'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import {
	CONFIRMATION,
	PLATFORM_TRANSLATIONS,
	FOREIGN_KEY_DIALOG_HEADER,
	ERROR,
	CREATE_DATABASE,
} from '../../utilities/constants'
import './dialogs.scss'
import { ConfirmationDialogContent } from './confirmation/ConfirmationDialogContent'
import { ConfirmationDialogActions } from './confirmation/ConfirmationDialogActions'
import { ConnectForeignKeyContent } from './connectForeignKey/ConnectForeignKeyContent'
import { ConnectForeignKeyActions } from './connectForeignKey/ConnectForeignKeyActions'
import { AlertContent } from './alert/AlertContent'
import { AlertActions } from './alert/AlertActions'
import { CreateDatabaseContent } from './createDatabase/CreateDatabaseContent'
import { CreateDatabaseActions } from './createDatabase/CreateDatabaseActions'

export function Dialogs() {
	const dialogIds = useSelector(selectOrderedDialogs)
	const dialogsMap = useSelector(selectDialogsMap)
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()

	const handleClose = (id) => {
		dispatch(actions.removeDialog({ id, isOkay: false }))
	}
	const handleOkay = (id, data) => {
		dispatch(actions.removeDialog({ id, isOkay: true, data }))
	}

	const getTitle = (dialog) => {
		if (isString(dialog.title)) {
			return dialog.title
		}
		switch (dialog.type) {
			case dialogType.CONFIRMATION_DIALOG:
				return t(CONFIRMATION)
			case dialogType.CONNECT_FOREIGN_KEY:
				return t(FOREIGN_KEY_DIALOG_HEADER)
			case dialogType.ALERT:
				return t(ERROR)
			case dialogType.CREATE_DATABASE:
				return t(CREATE_DATABASE)
			default:
				return 'iSymphony Studio'
		}
	}

	const renderTitle = (id, dialog) => {
		return (
			<DialogTitle>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => handleClose(id)}
				/>
				<span className='dialog-heading'>{getTitle(dialog)}</span>
			</DialogTitle>
		)
	}

	const renderContent = (_, dialog) => {
		switch (dialog.type) {
			case dialogType.ALERT:
				return <AlertContent dialogData={dialog.data} />
			case dialogType.CONFIRMATION_DIALOG:
				return <ConfirmationDialogContent dialogData={dialog.data} />
			case dialogType.CONNECT_FOREIGN_KEY:
				return <ConnectForeignKeyContent dialogData={dialog.data} />
			case dialogType.CREATE_DATABASE:
				return <CreateDatabaseContent />
			default:
				return null
		}
	}

	const renderActions = (id, dialog) => {
		switch (dialog.type) {
			case dialogType.ALERT:
				return (
					<AlertActions
						dialogData={dialog.data}
						onOkay={(data) => handleOkay(id, data)}
						onCancel={() => handleClose(id)}
					/>
				)
			case dialogType.CONNECT_FOREIGN_KEY:
				return (
					<ConnectForeignKeyActions
						onOkay={(data) => handleOkay(id, data)}
						onCancel={() => handleClose(id)}
					/>
				)
			case dialogType.CONFIRMATION_DIALOG:
				return (
					<ConfirmationDialogActions
						dialogData={dialog.data}
						onOkay={(data) => handleOkay(id, data)}
						onCancel={() => handleClose(id)}
					/>
				)
			case dialogType.CREATE_DATABASE:
				return (
					<CreateDatabaseActions
						onOkay={(data) => handleOkay(id, data)}
						onCancel={() => handleClose(id)}
					/>
				)
			default:
				return null
		}
	}

	const renderDialog = (id) => {
		const dialog = dialogsMap[id]
		if (isNil(dialog)) {
			return null
		}
		return (
			<Dialog
				open={true}
				onClose={() => handleClose(id)}
				className='dialog-box-common'
				key={id}
			>
				{renderTitle(id, dialog)}
				<DialogContent>{renderContent(id, dialog)}</DialogContent>
				<DialogActions className='actions-btn'>
					{renderActions(id, dialog)}
				</DialogActions>
			</Dialog>
		)
	}

	return (
		<>
			{dialogIds.map((id) => {
				return renderDialog(id)
			})}
		</>
	)
}
