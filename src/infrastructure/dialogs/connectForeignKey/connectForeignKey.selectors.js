import slice from './connectForeignKey.slice'
import { selectSlice as dialogsSelect } from '../base.selector'

export const selectSlice = (state) => dialogsSelect(state)[slice.name]

export const getSelectedTableIndex = (state) =>
	selectSlice(state).selectedTableIndex

export const getSelectedAttributeIndex = (state) =>
	selectSlice(state).selectedAttributeIndex

export const getContainment = (state) => selectSlice(state).containment
export const getRelationshipType = (state) =>
	selectSlice(state).relationshipType
