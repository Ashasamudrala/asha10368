import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import Autocomplete from '@material-ui/lab/Autocomplete'
import {
	TableRow,
	TableBody,
	TableCell,
	Table,
	TextField,
} from '@material-ui/core'
import { useSelector, useDispatch } from 'react-redux'
import {
	FOREIGN_KEY_CONTENT_TITLE,
	FOREIGN_KEY_DIALOG_TRANSLATIONS,
	FOREIGN_KEY_PLACEHOLDER,
	FOREIGN_KEY_CONTENT_TO_POINT,
	FOREIGN_KEY_DIALOG_CONTAINMENT,
	FOREIGN_KEY_DIALOG_CONTAINMENT_AGGREGATION,
	FOREIGN_KEY_DIALOG_CONTAINMENT_COMPOSITION,
	ONE_TO_ONE,
	ONE_TO_MANY,
	MANY_TO_ONE,
	MANY_TO_MANY,
	RELATIONSHIP_TYPE,
} from '../../../utilities/constants'
import {
	getSelectedTableIndex,
	getSelectedAttributeIndex,
	getContainment,
	getRelationshipType,
} from './connectForeignKey.selectors'
import { actions } from './connectForeignKey.slice'
import './connectForeignKey.scss'
import { findIndex, isNil } from 'lodash'
import { IsyRadioGroup } from '../../../widgets/radioGroup/RadioGroup'
import DropDown from '../../../widgets/dropDown/dropDown'
import OneToOne from '../../../icons/OneToOne'
import OneToMany from '../../../icons/OneToMany'
import ManyToOne from '../../../icons/ManyToOne'
import ManyToMany from '../../../icons/ManyToMany'

export function ConnectForeignKeyContent(props) {
	const { dialogData } = props
	const { t } = useTranslation(FOREIGN_KEY_DIALOG_TRANSLATIONS)
	const selectedTableIndex = useSelector(getSelectedTableIndex)
	const selectedAttributeIndex = useSelector(getSelectedAttributeIndex)
	const containment = useSelector(getContainment)
	const relationShipType = useSelector(getRelationshipType)
	const dispatch = useDispatch()

	useEffect(() => {
		const reference =
			dialogData.tablesData[dialogData.tableIndex].attributes[
				dialogData.attrIndex
			].reference
		if (!isNil(reference)) {
			const tableIndex = findIndex(
				dialogData.tablesData,
				(table) => table.name === reference.targetModel
			)
			const attrIndex = findIndex(
				dialogData.tablesData[tableIndex].attributes,
				(attr) => attr.name === reference.column
			)
			dispatch(
				actions.setSelectedData({
					tableIndex,
					attrIndex,
				})
			)
			dispatch(actions.setContainment(reference.relation.containment))
			dispatch(actions.setRelationshipType(reference.relation.associationType))
		}
	}, [dialogData, dispatch])

	const containmentOptions = [
		{
			value: 'Composition',
			label: t(FOREIGN_KEY_DIALOG_CONTAINMENT_COMPOSITION),
		},
		{
			value: 'Aggregation',
			label: t(FOREIGN_KEY_DIALOG_CONTAINMENT_AGGREGATION),
		},
	]

	const relationshipOptions = [
		{
			id: 'oneToOne',
			name: t(ONE_TO_ONE),
			hasIcon: true,
			icon: <OneToOne className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
		{
			id: 'oneToMany',
			name: t(ONE_TO_MANY),
			hasIcon: true,
			icon: <OneToMany className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
		{
			id: 'manyToOne',
			name: t(MANY_TO_ONE),
			hasIcon: true,
			icon: <ManyToOne className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
		{
			id: 'manyToMany',
			name: t(MANY_TO_MANY),
			hasIcon: true,
			icon: <ManyToMany className='relation-ship-icons' />,
			labelClass: 'relation-ship-labels',
		},
	]

	const getOptionsAndSelectedElement = () => {
		const tableIndex = dialogData.tableIndex
		const attributeIndex = dialogData.attrIndex
		const tablesData = dialogData.tablesData
		const sourceAttrType =
			tablesData[tableIndex].attributes[attributeIndex].type
		const options = []
		let selectedElement = null
		for (let i = 0, iLen = tablesData.length; i < iLen; i++) {
			if (i !== tableIndex) {
				const table = tablesData[i]
				const primaryKeyIndex = findIndex(table.attributes, (attr) => {
					return attr.type === sourceAttrType && attr.constraints.primary
				})
				if (primaryKeyIndex !== -1) {
					options.push({
						tableIndex: i,
						tableName: table.name,
						attributeName: table.attributes[primaryKeyIndex].name,
						attributeIndex: primaryKeyIndex,
						dataType: sourceAttrType,
					})
					if (selectedTableIndex === i) {
						selectedElement = options[options.length - 1]
					}
				}
			}
		}
		return { options, selectedElement }
	}

	const getLabelValue = (option) => {
		const labelData = `${option.tableName}: ${option.attributeName} [${option.dataType}]`
		return labelData
	}

	const handleSelectedValue = (_, option, reason) => {
		if (reason === 'clear') {
			dispatch(
				actions.setSelectedData({
					tableIndex: null,
					attrIndex: null,
				})
			)
		} else if (reason === 'select-option') {
			dispatch(
				actions.setSelectedData({
					tableIndex: option.tableIndex,
					attrIndex: option.attributeIndex,
				})
			)
		}
	}

	const handleContainmentChange = (value) => {
		dispatch(actions.setContainment(value))
	}

	const handleRelationshipTypeChange = (e) => {
		dispatch(actions.setRelationshipType(e.target.value))
	}

	const renderListOfOptions = (option) => {
		return (
			<Table className='auto-complete-table'>
				<TableBody>
					<TableRow>
						<TableCell className='table-cell'>{option.tableName}</TableCell>
						<TableCell className='table-cell'>
							{option.attributeName} [{option.dataType}]
						</TableCell>
					</TableRow>
				</TableBody>
			</Table>
		)
	}

	const renderTextField = (params) => {
		return (
			<TextField
				{...params}
				placeholder={t(FOREIGN_KEY_PLACEHOLDER)}
				variant='outlined'
			/>
		)
	}

	const renderAutoCompleteComponent = () => {
		const optionsAndSelectedElement = getOptionsAndSelectedElement()
		return (
			<Autocomplete
				value={optionsAndSelectedElement.selectedElement}
				className='auto-search-input'
				options={optionsAndSelectedElement.options}
				getOptionLabel={getLabelValue}
				closeIcon={null}
				onChange={handleSelectedValue}
				renderOption={renderListOfOptions}
				renderInput={renderTextField}
			/>
		)
	}

	const renderContainment = () => {
		return (
			<IsyRadioGroup
				label={t(FOREIGN_KEY_DIALOG_CONTAINMENT)}
				labelClass='search-content'
				value={containment}
				options={containmentOptions}
				onChange={handleContainmentChange}
			/>
		)
	}

	const renderRelations = () => {
		return (
			<div className='relationShipType'>
				<DropDown
					options={relationshipOptions}
					onChange={handleRelationshipTypeChange}
					update={true}
					name='relationShipType'
					selected={relationShipType}
					title={t(RELATIONSHIP_TYPE)}
					className='input-base'
				/>
			</div>
		)
	}

	return (
		<div className='relation-ship'>
			<p className='search-content'>
				{!isNil(selectedTableIndex) && !isNil(selectedAttributeIndex)
					? t(FOREIGN_KEY_CONTENT_TO_POINT)
					: t(FOREIGN_KEY_CONTENT_TITLE)}
			</p>
			{renderAutoCompleteComponent()}
			{renderRelations()}
			{renderContainment()}
		</div>
	)
}
