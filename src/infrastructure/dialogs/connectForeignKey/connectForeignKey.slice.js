import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	selectedTableIndex: null,
	selectedAttributeIndex: null,
	containment: 'Composition',
	relationshipType: 'manyToOne',
}

const slice = createSlice({
	name: 'connectForeignKey',
	initialState,
	reducers: {
		// synchronous actions
		setSelectedData(state, action) {
			state.selectedTableIndex = action.payload.tableIndex
			state.selectedAttributeIndex = action.payload.attrIndex
		},
		setContainment(state, action) {
			state.containment = action.payload
		},
		setRelationshipType(state, action) {
			state.relationshipType = action.payload
		},
		clearData: () => initialState,
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
