import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import {
	PLATFORM_TRANSLATIONS,
	BUTTON_CANCEL,
	BUTTON_CONNECT,
} from '../../../utilities/constants'
import Button from '../../../widgets/button'
import {
	getSelectedTableIndex,
	getSelectedAttributeIndex,
	getContainment,
	getRelationshipType,
} from './connectForeignKey.selectors'
import { actions } from './connectForeignKey.slice'
import { isNil } from 'lodash'

export function ConnectForeignKeyActions(props) {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const selectedTableIndex = useSelector(getSelectedTableIndex)
	const selectedAttributeIndex = useSelector(getSelectedAttributeIndex)
	const containment = useSelector(getContainment)
	const relationShipType = useSelector(getRelationshipType)

	const dispatch = useDispatch()

	const handleCancel = () => {
		props.onCancel()
		dispatch(actions.clearData())
	}

	const handleOkay = () => {
		props.onOkay({
			targetTableIndex: selectedTableIndex,
			targetAttrIndex: selectedAttributeIndex,
			containment,
			relationShipType,
		})
		dispatch(actions.clearData())
	}

	return (
		<>
			<Button handleButtonOnClick={handleCancel} buttonClass='cancel-btn'>
				{t(BUTTON_CANCEL)}
			</Button>
			<Button
				buttonClass='save-btn'
				disabled={isNil(selectedTableIndex) || isNil(selectedAttributeIndex)}
				handleButtonOnClick={handleOkay}
			>
				{t(BUTTON_CONNECT)}
			</Button>
		</>
	)
}
