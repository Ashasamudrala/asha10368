import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectNotification } from './notificationPopup.selectors'
import Alert from '@material-ui/lab/Alert'
import './notificationPopup.scss'
import { actions } from './notificationPopup.slice'
const { closePopup } = actions

export default function NotificationPopupContainer() {
	const { errorMessage, successMessage } = useSelector(selectNotification)
	const message = errorMessage || successMessage
	const dispatch = useDispatch()
	// removing the pop up after 3 second of time and removing success message data after 3 seconds
	useEffect(() => {
		if (successMessage || errorMessage) {
			window.scrollTo(0, 0)
			setTimeout(() => dispatch(closePopup()), 3000)
		}
	}, [successMessage, errorMessage, dispatch])
	return (
		<div className={`notification-popup ${message ? 'visible' : ''}`}>
			{successMessage && (
				<div className='success-message'>
					<Alert icon={false}>
						<img
							src='/images/Success.svg'
							alt='error-icon'
							className='success-icon'
						/>
						{successMessage}
					</Alert>
				</div>
			)}
			{errorMessage && (
				<div className='error-message'>
					<Alert icon={false} severity='error'>
						<img
							src='/images/Error.svg'
							alt='error-icon'
							className='error-icon'
						/>
						{errorMessage.fieldErrors
							? errorMessage.fieldErrors.map((fieldError) => {
									return fieldError.errorMessage
							  })
							: errorMessage.errorMessage
							? errorMessage.errorMessage
							: errorMessage.message
							? errorMessage.message
							: errorMessage.error
							? errorMessage.error
							: ''}
					</Alert>
				</div>
			)}
		</div>
	)
}
