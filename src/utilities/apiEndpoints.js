export const GET_ALL_PLATFORMS = 'administration/platforms'
export const SAVE_PLATFORM = 'administration/platforms'
export const GET_ALL_FRAMEWORKS =
	'administration/platforms/5d57fc4c8a37cf5e626ad9ff/frameworks'
export const GET_PLATFORM_BY_ID = 'administration/platforms/platformId'

export const SAVE_FRAMEWORK_DETAILS = 'administration/platforms'

export const VIEW_PLATFORM_BY_ID = 'administration/platforms'
export const LOGIN_AUTH = 'auth/login'
export const LOGIN_REFRESH = 'users/me'
export const GET_ALL_REPOSITORIES =
	'administration/devops/repositories/configurations'
export const SAVE_REPOSITORY =
	'administration/devops/repositories/configurations'
export const GET_ALL_CONTINUOUSINTEGRATION =
	'administration/devops/continuousIntegration/configurations'
export const UPDATE_FRAMEWORK = 'administration/platforms'
export const GET_ALL_USERS = 'users'
export const GET_ACTIVE_MEMBERS = 'invitations?status=invited'
export const GET_PENDING_INVITES = 'invitations?status=draft'
export const UPDATE_REPOSITORIES_BY_ID =
	'administration/devops/repositories/configurations/'
export const UPDATE_CONTINUOUSDELIVERY_BY_ID =
	'administration/devops/continuousDelivery/configurations/'
export const USER_ROLES = 'roles'
export const GET_ACTIVE_SEARCH_USERS = 'users/search'
export const GET_PENDING_SEARCH_USERS = 'invitations/search'
export const CREATE_INVITE_USERS = 'invitations'
export const SAVE_CONTINUOUSDELIVERY =
	'administration/devops/continuousDelivery/configurations'
export const GET_ALL_CONTINUOUSDELIVERY =
	'administration/devops/continuousDelivery/configurations'
export const PASSWORD_RESET_REQUESTS = 'passwords/resetRequests'
export const PASSWORD_RESET_PASSWORD = 'passwords/resetPassword'
export const GET_TOKEN_FOR_RESET_PASSWORD = 'passwords/resetRequests'
export const UPDATE_INVITE_USER = 'invitations'
export const UPDATE_ACTIVE_USER = 'roles'
export const UPDATE_DB_CONFIG = (appid, databaseid) =>
	`applications/${appid}/databases/${databaseid}`
export const SIGNUP_USERS = 'users/signup'
export const GET_INVITATIONS_FOR_SIGNUP = 'invitations/verify'
export const GET_ALL_DATABASES = (id) => `applications/${id}/databases`
export const CREATE_DATABASE = (id) => `applications/${id}/databases`
export const SAVE_DATABASE = (appId, databaseId, schemaId) =>
	`applications/${appId}/databases/${databaseId}/schemas/${schemaId}`
export const DELETE_DATABASE = (applicationId, databaseId) =>
	`applications/${applicationId}/databases/${databaseId}`
export const GET_ATTRIBUTE_TYPES = 'lookups/attributeTypes'
export const GET_CONSTRAINT_KEY_TYPES = 'lookups/keyTypes'
