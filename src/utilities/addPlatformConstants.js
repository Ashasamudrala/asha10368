export const ADD_PLATFORM_TRANSLATIONS = ['addPlatform']

// exporting Add Platform label
export const ADD_PLATFORM = 'addPlatform:ADD_PLATFORM'

export const RIGHT_SIDE_SLIDER = 'addPlatform:RIGHT_SIDE_SLIDER'
