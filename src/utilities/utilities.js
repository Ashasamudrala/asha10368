import React from 'react'
import { isNil } from 'lodash'

/**
 * Is to render the sidebar Image.
 */
const getImagePath = (image) => {
	return <img src={`/images/${image}.svg`} />
}

export const getUniqueName = (names, name) => {
	let count = 1
	while (true) {
		if (names.indexOf(name + ' ' + count) === -1) {
			return name + ' ' + count
		} else {
			count++
		}
	}
}

const hasClass = (element, className) => {
	const classList = (' ' + element.classList.toString() + ' ').replace(
		/[\n\t]/g,
		' '
	)
	for (let i = 0, iLen = className.length; i < iLen; i++) {
		if (classList.indexOf(' ' + className[i] + ' ') > -1) {
			return true
		}
	}
	return false
}

export const isParentUntil = (element, className) => {
	if (hasClass(element, className)) {
		return true
	}
	if (isNil(element.parentElement)) {
		return false
	}
	return isParentUntil(element.parentElement, className)
}

export default getImagePath
