import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './addPlatform.asyncActions'

const initialState = {
	allAddPlatform: [],
	status: '',
}

const slice = createSlice({
	name: 'addPlatform',
	initialState,
	reducers: {
		removeStatus(state) {
			state.status = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.saveAddPlatform.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allAddPlatform = action.payload
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
