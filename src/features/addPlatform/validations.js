import _ from 'lodash'

const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,3}$/
const passwordRegex = /^(?=.*[\da-zA-Z]).{8,}$/
const nameRegex = /^(?!(?:[0-9]+|\s+)$).[-a-zA-z]+$/
const addNameValidRegex = /^[^\s]+(\s+[^\s]+)*$/
const versionValidationRegex = /[a-zA-Z~`!#$%&*+=\-\]\\';,/{}|\\":<>()]/
const versionNameValidRegex = /^[^\s]+(\s+[^\s]+)*$/
const moduleNameValidRegex = /^[^\s]+(\s+[^\s]+)*$/
const portNumberValidRegex = /^\d{4}$/
const dbCredentialsRegex = /^[a-zA-Z]+$/
const dbPasswordRegex = /^[a-zA-Z0-9]{4,}$/

function fieldIsEmpty(fieldName) {
	return _.isEmpty(fieldName) || fieldName.length === 0
}

function versionIsValid(versions) {
	return (
		versions.length > 0 &&
		versions.find((version) => {
			return versionValidationRegex.test(version)
		})
	)
}
function isVersionNameValid(name) {
	return !versionNameValidRegex.test(name)
}
function descriptionIsValid(description) {
	return description.length >= 120
}
function emailIsValid(email) {
	return email.match(emailRegex)
}
function addNameIsValid(name) {
	return !addNameValidRegex.test(name)
}
function passwordIsValid(password) {
	return password.match(passwordRegex)
}
function accountNameIsValid(accountName) {
	return /\d/.test(accountName)
}
function userNameIsValid(userName) {
	return /\d/.test(userName)
}
function continuousDeliveryFieldIsValid(name) {
	return !moduleNameValidRegex.test(name)
}
function nameIsValid(name) {
	return !nameRegex.test(name)
}
function isModuleVersionNameValid(name) {
	return !versionNameValidRegex.test(name)
}
function moduleNameIsValid(name) {
	return !moduleNameValidRegex.test(name)
}

function isNumberValid(number) {
	return portNumberValidRegex.test(number)
}

function isValidDbCredentials(string) {
	return dbCredentialsRegex.test(string)
}
function DbPasswordIsValid(string) {
	return dbPasswordRegex.test(string)
}

export {
	emailIsValid,
	passwordIsValid,
	fieldIsEmpty,
	versionIsValid,
	descriptionIsValid,
	nameIsValid,
	accountNameIsValid,
	userNameIsValid,
	isModuleVersionNameValid,
	isVersionNameValid,
	moduleNameIsValid,
	continuousDeliveryFieldIsValid,
	addNameIsValid,
	isNumberValid,
	isValidDbCredentials,
	DbPasswordIsValid,
}
