import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { saveAddPlatform } from './addPlatform.asyncActions'
import platformInput from '../../config/addPlatform/addPlatform.json'
import platformOutputData from '../../config/addPlatform/addPlatformOutput.json'
import AbstractPlatform from './AbstractPlatformForm'
import { getPlatformStatus } from './addPlatform.selectors'
import { fetchAllPlatform } from '../platform/platform.asyncActions'
import { actions } from './addPlatform.slice'
import './addPlatform.scss'
const { removeStatus } = actions

/* 
this compoent is used to provide add platform details whenever onclick of button
 */
export default function AddPlatform(props) {
	const dispatch = useDispatch()
	const status = useSelector(getPlatformStatus)

	// when ever addplatform is posted successfully and then dispatch to fetchallpltformsapi
	useEffect(() => {
		if (status !== '') {
			props.openDrawer(false)
			status && dispatch(fetchAllPlatform())
		}
	}, [status]) // eslint-disable-line react-hooks/exhaustive-deps

	// when ever save button is clciked dispatch to addplatform api
	const handleSave = (platformFields) => {
		dispatch(
			saveAddPlatform({
				...platformFields,
			})
		)
	}
	useEffect(() => {
		return () => {
			dispatch(removeStatus())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	return (
		<AbstractPlatform
			openDrawer={props.openDrawer}
			platformInput={platformInput}
			platformOutputData={platformOutputData}
			onSave={handleSave}
			className='add-platform'
		/>
	)
}
