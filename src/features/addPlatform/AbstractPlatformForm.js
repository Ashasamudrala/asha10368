import React, { useState, useEffect } from 'react'
import RightSlider from '../../widgets/rightSideSlider'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import ErrorMessages from '../../config/addPlatform/validations.json'
import metaDataInput from '../../config/metadata/metadata.json'
import metaDataOutput from '../../config/metadata/metadataOutput.json'
import _ from 'lodash'
import {
	fieldIsEmpty,
	versionIsValid,
	descriptionIsValid,
	addNameIsValid,
} from './validations'
import { useTranslation } from 'react-i18next'
import {
	ADD,
	ONCHANGE,
	PLATFORM_TRANSLATIONS,
	DELETE,
	METADATA,
	VERSION,
	modeType,
} from '../../utilities/constants'
import BusyIndicator from '../../widgets/busyIndicator'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'

/* 
this compoent is used to provide add platform details whenever onclick of button
 */
export default function AbstractPlatform(props) {
	const { platformInput, platformOutputData } = props
	// there are state values for showing dynamic and platform detials for storing the fields values
	const [platformFields, setPlatformFields] = useState({
		...platformOutputData,
	})
	const [errors, setErrovalues] = useState({})
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)

	useEffect(() => {
		setPlatformFields({ ...platformOutputData })
	}, [props]) // eslint-disable-line react-hooks/exhaustive-deps

	// this is used for when clicking on metadata to move to dynamic add component
	const renderRightSideSlider = () => {
		return (
			<RightSlider onCancel={props.openDrawer} drawerName='drawer'>
				<BusyIndicator>
					<CloseOutlinedIcon
						className='close-icon'
						onClick={() => {
							props.openDrawer()
						}}
					/>
					<div className='platform'>
						<div className={props.className}>
							<FormBuilder
								formBuilderInput={platformInput}
								onChange={handlePlatformFields}
								formBuilderOutput={platformFields}
								metaDataInput={metaDataInput}
								metaDataOutput={metaDataOutput}
								errors={errors}
							/>
						</div>
						<SliderFooter
							mode={modeType.WRITE}
							handleClose={props.openDrawer}
							handleSave={handleSave}
						/>
					</div>
				</BusyIndicator>
			</RightSlider>
		)
	}
	// this is used for setting the json fields
	const handlePlatformFields = (event, fieldName, action) => {
		/**
		 * if fieldname is defined then if condition excetues this is for metadata and version
		 * and then else executes when ever fieldname is undefined
		 */
		if (fieldName) {
			let processedfieldName = []
			/**
			 * based on action swtch case executes
			 */
			switch (action) {
				case t(ADD):
					if (fieldName === t(VERSION)) {
						/**
						 * It is used for adding new metadata into array
						 * and if type is metadata
						 */
						processedfieldName = platformFields[fieldName]
						processedfieldName = [...processedfieldName, event]
					} else if (fieldName === t(METADATA)) {
						/**
						 * It is used for adding new version into array
						 * and if type is version
						 */
						processedfieldName = platformFields[fieldName]
						processedfieldName = [event, ...processedfieldName]
					}
					break
				case t(ONCHANGE):
					/**
					 * It is used when onchange of metadata is called and push the change value
					 * based on id into array if type is (metadata)
					 */
					processedfieldName = platformFields[fieldName]
					processedfieldName[event.target.dataset.id][event.target.name] =
						event.target.value
					break
				case t(DELETE):
					/**
					 * It is used when delete of metadata and version  is called and removes the item
					 * based on id from array
					 */
					processedfieldName = platformFields[fieldName]
					processedfieldName = processedfieldName.filter(
						(version, id) => event !== id
					)
					break
				default:
			}
			setPlatformFields({
				...platformFields,
				[fieldName]: processedfieldName,
			})
		} else {
			setPlatformFields({
				...platformFields,
				[event.target.name]: event.target.value,
			})
		}
	}

	// on clicking save  it calls metdata to key value pairs
	const handleSave = () => {
		if (handleValidation()) {
			convertMetdataToKeyValue()
		}
	}

	// here it converts properties into ( key,value ) pairs and dispatch call to api
	const convertMetdataToKeyValue = () => {
		let processedMetadata = {}
		const metadata = platformFields.metadata
		if (metadata && metadata.length >= 0) {
			processedMetadata = _.mapValues(_.keyBy(metadata, 'key'), 'value')
			processedMetadata = _.omit(processedMetadata, [''])
		}
		props.onSave({ ...platformFields, metadata: { ...processedMetadata } })
	}

	// form validations
	const handleValidation = () => {
		const fields = { ...platformFields }
		const errors = {}
		const inputFields = platformInput.section[0].inputFields
		inputFields.forEach((inputField) => {
			if (inputField.required) {
				if (fieldIsEmpty(fields[inputField.name])) {
					errors[inputField.name] = ErrorMessages[inputField.name]
				}
			}
		})
		if (versionIsValid(fields.versions)) {
			errors.versions = ErrorMessages.invalidVersions
		}
		if (descriptionIsValid(fields.description)) {
			errors.description = ErrorMessages.invalidDescription
		}
		if (addNameIsValid(fields.name)) {
			errors.name = ErrorMessages.invalidName
		}
		setErrovalues({ ...errors })
		return _.isEmpty(errors)
	}
	// based on the condition the platform details and render metadata details are rendered
	return <div>{renderRightSideSlider()}</div>
}
