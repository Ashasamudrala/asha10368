import slice from './addPlatform.slice'

export const selectSlice = (state) => state[slice.name]

export const getPlatformStatus = (state) => selectSlice(state).status
