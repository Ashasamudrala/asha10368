import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { SAVE_PLATFORM } from '../../utilities/apiEndpoints'

export const saveAddPlatform = createAsyncThunk(
	'addPlatform/getAll',
	async (
		{ useCaching, noBusySpinner, metadata, name, description, versions } = {},
		thunkArgs
	) =>
		await doAsync({
			url: SAVE_PLATFORM,
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					metadata,
					name,
					description,
					versions,
				}),
			},
			successMessage: 'AddPlatform is saved successfully',
			...thunkArgs,
		})
)
