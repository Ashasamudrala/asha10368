import AddPlatform from './AddPlatform'
import * as asyncActions from './addPlatform.asyncActions'
import slice from './addPlatform.slice'

export const { name, reducer } = slice

export const { saveAddPlatform } = asyncActions

// we export the component most likely to be desired by default
export default AddPlatform
