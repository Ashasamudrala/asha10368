import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './activeMembers.asyncActions'

const initialState = {
	allActiveMembers: [],
	filter: '',
	records: '',
}

const slice = createSlice({
	name: 'activeMembers',
	initialState,
	confirmDelete: false,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
		updateCheckedValue(state, action) {
			// action.payload.item.checked = action.payload.e.target-
			const activeMembers = state.allActiveMembers
			activeMembers.forEach((item) => {
				if (item.id === action.payload.item.id) {
					item.checked = action.payload.value
				}
			})
			state.allActiveMembers = [...activeMembers]
		},
		updateSelectAll(state, action) {
			const activeMembers = state.allActiveMembers
			activeMembers.forEach((item) => {
				if (action.payload.value) {
					item.checked = true
				} else {
					item.checked = false
				}
			})
			state.allActiveMembers = [...activeMembers]
		},
		updateSearchData(state, action) {
			state.allActiveMembers = action.payload.content
			state.records = action.payload.records
		},
		confirmDelete(state, action) {
			state.confirmDelete = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllActiveMembers.fulfilled]: (state, action) => {
			if (action.payload) {
				action.payload.content.forEach((item) => {
					item.checked = false
				})
				state.allActiveMembers = action.payload.content
				state.records = action.payload
			}
		},
		[asyncActions.updateActiveMemberRole.fulfilled]: (state, action) => {},
	},
})

export default slice

export const { name, actions, reducer } = slice
