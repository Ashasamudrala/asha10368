import ActiveMembers from './ActiveMembers'
import * as selectors from './activeMembers.selectors'
import * as asyncActions from './activeMembers.asyncActions'
import slice from './activeMembers.slice'

export const {
	name,
	actions: { updateFilter, updateSearchTypePlaceholder },
	reducer,
} = slice

export const { fetchAllActiveMembers } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllActiveMembers, selectActiveMembersFilter } = selectors

// we export the component most likely to be desired by default
export default ActiveMembers
