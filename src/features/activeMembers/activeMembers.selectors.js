import slice from './activeMembers.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllActiveMembers = (state) =>
	selectSlice(state).allActiveMembers

export const selectActiveMembersFilter = (state) => selectSlice(state).filter
export const selectActiveMembersRecords = (state) => selectSlice(state).records

export const getSelectedActiveMembers = (state) =>
	selectSlice(state).allActiveMembers.filter((item) => item.checked === true)
export const getConfirmDelete = (state) => selectSlice(state).confirmDelete
