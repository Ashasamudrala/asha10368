import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectAllActiveMembers } from './activeMembers.selectors'
import {
	CONFIRMATION_MESSAGE_SELECT,
	TEAM_TRANSLATIONS,
	CONFIRMATION_MESSAGE_DELETE_USER,
	DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE,
	ROLE_SUCCESS_MESSAGE,
	permission,
} from '../../utilities/constants'
import ConfirmDialog from '../../widgets/confirmDialog/confirmDialog'
import GridTable from '../../widgets/grid/gridTable/GridTable'
import { useTranslation } from 'react-i18next'
import activeGridProp from '../../config/activeMember/activeMembers.json'
import {
	fetchAllActiveMembers,
	updateActiveMemberRole,
	deleteBulkUsers,
} from './activeMembers.asyncActions'
import _ from 'lodash'
import ViewProfile from '../profile/viewProfile'
import './activemembers.scss'
import { fetchActiveSearchResults } from '../team/team.asyncActions'
import {
	selectSearchStatus,
	selectSearchString,
	selectAllUsersFromSearchResults,
} from '../team/team.selectors'
import EditProfile from '../profile/editProfile'
import Can from '../../widgets/auth/Can'
import { actions } from './activeMembers.slice'
import { selectGetLogInUserInfo } from '../../authAndPermissions/loginUserDetails.selectors'
import { selectUserRoles } from '../inviteUser/inviteUser.selectors'
import { actions as teamActions } from '../team/team.slice'

const { updateCheckedValue, updateSelectAll } = actions
const {
	setActiveTab,
	updateSearchStatus,
	updateSearchString,
	removeSearchData,
} = teamActions

export default function ActiveMembers(props) {
	const activeMembers = useSelector(selectAllActiveMembers)
	const activeSearch = useSelector(selectAllUsersFromSearchResults)
	const searchStatus = useSelector(selectSearchStatus)
	const searchString = useSelector(selectSearchString)
	const roles = useSelector(selectUserRoles)
	const searchId = activeSearch.map((ele) => ele.id)
	const loginDetails = useSelector(selectGetLogInUserInfo)
	const handleData = searchStatus
		? searchId.length > 0
			? activeSearch
			: []
		: activeMembers

	const [activeUserFields, setActiveUserFields] = useState(
		_.cloneDeep(activeGridProp)
	)
	const [confirmDialog, setConfirmDialog] = useState(false)
	const [roleOnChangeValue, setRoleValue] = useState({})
	const [view, SetView] = useState(false)
	const [openEdit, setOpenEdit] = useState(false)
	const [userId, setUserId] = useState()
	const [openConfirmDialog, setOpenConfirmDialog] = useState(false)
	const [deleteUser, setDeleteUser] = useState({})

	const { t } = useTranslation(TEAM_TRANSLATIONS)
	const handleEdit = (type) => {
		if (type === 'EditProfile') {
			setOpenEdit(true)
			SetView(false)
		}
	}
	const handleSelectAllClick = (event) => {
		if (searchStatus) {
		} else {
			dispatch(updateSelectAll({ value: event.target.checked }))
		}
	}

	const handleHeaderAction = (event, item) => {
		if (item.type === 'checkbox') {
			handleSelectAllClick(event)
		}
	}

	const handleClickOpen = (e) => {
		SetView(true)
	}

	const handleCheckItem = (e, item) => {
		if (searchStatus) {
		} else {
			dispatch(updateCheckedValue({ item, value: e.target.checked }))
		}
	}
	const handleCancel = () => {
		SetView(false)
		setOpenEdit(false)
	}

	const dispatch = useDispatch()

	const handleOnChange = (item, e) => {
		setConfirmDialog(true)
		setRoleValue({ value: e.target.value, item: item })
	}

	const handleConfirmDialog = () => {
		const userIds = deleteUser.id
		const deleteSuccessMessage = deleteUser.email

		dispatch(
			deleteBulkUsers({
				userIds,
				deleteSuccessMessage: `${t(DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE, {
					user: deleteSuccessMessage,
				})}`,
			})
		).then(() => {
			setDeleteUser({})
			setOpenConfirmDialog(false)
			if (searchStatus) {
				dispatch(
					fetchActiveSearchResults({
						data: searchString,
					})
				)
			} else {
				dispatch(fetchAllActiveMembers())
			}
		})
	}

	const handleCloseConfirmDialog = () => {
		setOpenConfirmDialog(false)
	}

	const handleDeleteAction = (item, e) => {
		setOpenConfirmDialog(true)
		setDeleteUser(item)
	}

	const handleAction = (actionType, item, e) => {
		if (actionType === 'select') {
			handleOnChange(item, e)
		} else if (actionType === 'delete') {
			handleDeleteAction(item, e)
		} else if (actionType === 'checkbox') {
			handleCheckItem(e, item)
		}
	}

	const handleViewItem = (view, viewItem) => {
		SetView(view)
		setUserId(viewItem)
	}

	useEffect(() => {
		dispatch(setActiveTab(0))
		if (
			loginDetails &&
			loginDetails.permissions &&
			loginDetails.permissions.includes(permission.UPDATE_ROLE)
		) {
			const cloneActiveMembers = activeUserFields
			cloneActiveMembers.columns.splice(4, 1)
			setActiveUserFields({
				...cloneActiveMembers,
			})
		}
		dispatch(fetchAllActiveMembers())
	}, [dispatch])
	useEffect(() => {
		if (roles && roles.length !== 0) {
			setFormRoles(roles)
		}
	}, [roles])

	const saveRole = () => {
		setConfirmDialog(false)
		setRoleValue({})
		SetView(false)
		dispatch(
			updateActiveMemberRole({
				userId: roleOnChangeValue.item.id,
				roleId: roleOnChangeValue.value,
				roleSuccessMessage: `${t(ROLE_SUCCESS_MESSAGE)}`,
			})
		).then(() => {
			if (searchStatus) {
				dispatch(
					fetchActiveSearchResults({
						data: searchString,
					})
				)
			} else {
				dispatch(fetchAllActiveMembers())
			}
		})
	}

	const setFormRoles = (roles) => {
		let processedData
		const columns = activeUserFields.columns
		columns.map((inputField, index) => {
			if (inputField.name === 'roleId') {
				processedData = roles
				columns[index].options = [...processedData]
			}
		})
		setActiveUserFields({
			...activeUserFields,
			columns: columns,
		})
	}

	const handleConfirmClose = () => {
		setRoleValue({})
		SetView(false)
		setConfirmDialog(false)
	}

	useEffect(() => {
		return () => {
			dispatch(updateSearchStatus({ searchStatus: false }))
			dispatch(removeSearchData())
			dispatch(
				updateSearchString({
					value: '',
				})
			)
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<>
			<div className='active-content'>
				<GridTable
					properties={activeUserFields}
					data={handleData}
					handleViewItem={handleViewItem}
					handleAction={handleAction}
					handleHeaderAction={handleHeaderAction}
				/>
			</div>
			{view && _.isEmpty(roleOnChangeValue) && (
				<ViewProfile
					open={handleClickOpen}
					onCancel={handleCancel}
					callback={handleEdit}
					userData={userId}
				/>
			)}
			{openEdit && _.isEmpty(roleOnChangeValue) && (
				<Can
					action={permission.UPDATE_USER_PROFILE}
					yes={() => (
						<EditProfile
							open={openEdit}
							onClose={handleCancel}
							editUserData={userId}
						/>
					)}
					no={() => ''}
				></Can>
			)}
			{confirmDialog && (
				<ConfirmDialog
					open={true}
					onClose={() => handleConfirmClose()}
					onConfirm={saveRole}
					message={t(CONFIRMATION_MESSAGE_SELECT)}
				/>
			)}

			<ConfirmDialog
				open={openConfirmDialog}
				onClose={handleCloseConfirmDialog}
				onConfirm={handleConfirmDialog}
				message={t(CONFIRMATION_MESSAGE_DELETE_USER, {
					user: `${deleteUser.firstName} ${deleteUser.lastName}`,
				})}
			/>
		</>
	)
}
