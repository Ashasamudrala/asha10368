import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_USERS, UPDATE_ACTIVE_USER } from '../../utilities/apiEndpoints'

export const fetchAllActiveMembers = createAsyncThunk(
	'activeMembers/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ALL_USERS,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
export const updateActiveMemberRole = createAsyncThunk(
	'activeMembers/save',
	async (
		{ noBusySpinner, userId, roleId, roleSuccessMessage } = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${GET_ALL_USERS}/${userId}/${UPDATE_ACTIVE_USER}`,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify([roleId]),
			},
			...thunkArgs,
			successMessage: `${roleSuccessMessage}`,
		})
)

export const deleteBulkUsers = createAsyncThunk(
	'activeMembers/delete',
	async (
		{ noBusySpinner, useCaching, userIds, deleteSuccessMessage } = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${GET_ALL_USERS}/?userIds=${userIds}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'delete',
			...thunkArgs,
			successMessage: `${deleteSuccessMessage}`,
		})
)
