import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './profile.asyncActions'

const initialState = {
	filter: '',
	userDetails: {},
}

const slice = createSlice({
	name: 'profile',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
		removeProfileDetails(state) {
			state.userContactDetails = []
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchUserDetails.fulfilled]: (state, action) => {
			state.userDetails = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
