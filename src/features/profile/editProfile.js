import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
	updateProfileById,
	updateProfilePicById,
	fetchUserDetails,
} from './profile.asyncActions'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Button from '../../widgets/button/button'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import { useDispatch, useSelector } from 'react-redux'
import { selectGetLoginUserId } from '../../authAndPermissions/loginUserDetails.selectors'
import profileOutputData from '../../config/profile/viewProfileOutput.json'
import profileInputData from '../../config/profile/viewProfile.json'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import { useTranslation } from 'react-i18next'
import AvatarImage from '../../widgets/avatarImage'
import {
	BUTTON_SAVE,
	BUTTON_CANCEL,
	PROFILE_ERROR,
	PLATFORM_TRANSLATIONS,
} from '../../utilities/constants'
import './Profile.scss'

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: 'transparent',
		display: 'flex',
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
		margin: 'auto',
	},
	listItemText: {
		textAlign: 'center',
		fontSize: '24px',
	},
}))
export default function EditProfile(props) {
	const { onClose, open, editUserData } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const classes = useStyles()
	const [fileData, setFileData] = useState(null)
	const [fileUrl, setFileUrl] = useState(null)
	const hiddenFileInput = React.useRef(null)
	const [uploadError, setUploadError] = useState('')
	const logInUserId = useSelector(selectGetLoginUserId)
	const FirstName = editUserData.firstName
	const LastName = editUserData.lastName
	const defaultFirstName = FirstName && FirstName.charAt(0)
	const defaultLastName = LastName && LastName.charAt(0)
	const [profileFields, setProfileFields] = useState({
		...profileOutputData,
	})
	const dispatch = useDispatch()
	useEffect(() => {
		setUserDetails()
	}, [dispatch])
	const setUserDetails = () => {
		if (editUserData) {
			const rolesId = editUserData.roles.map((role) => {
				return role.id
			})
			setProfileFields({
				email: editUserData.email,
				firstName: editUserData.firstName,
				lastName: editUserData.lastName,
				title: editUserData.title,
				phoneNumber: editUserData.phoneNumber,
				address: editUserData.address,
				roleIds: rolesId,
				profilePic: editUserData.profilePic,
			})
		}
	}

	const handleInput = (event) => {
		setProfileFields({
			...profileFields,
			[event.target.name]: event.target.value,
		})
	}

	const handleSave = (e) => {
		const userId = editUserData.id
		dispatch(
			updateProfileById({
				...profileFields,
				userId: userId,
			})
		).then(() => {
			dispatch(fetchUserDetails({ userId: logInUserId }))
		})
		if (fileUrl) {
			dispatch(
				updateProfilePicById({
					fileData: fileData,
					userId: userId,
				})
			)
		}
		onClose(false)
	}

	/**
	 * Profile update onchange.
	 * @param {object} event
	 */
	const onFileChange = (event) => {
		if (event.target.files[0]) {
			if (event.target.files[0].size < 1000000) {
				setFileData(event.target.files[0])
				const url = URL.createObjectURL(event.target.files[0])
				setFileUrl(url)
				setUploadError('')
			} else {
				setUploadError(t(PROFILE_ERROR))
			}
		}
	}
	/**
	 * calling input file onchange through this function.
	 * @param {object} event
	 */
	const handleClick = (event) => {
		hiddenFileInput.current.click()
	}
	return (
		<div>
			<Dialog
				onClose={onClose}
				open={open}
				className='editprofile'
				BackdropProps={{
					classes: {
						root: classes.root,
					},
				}}
			>
				<DialogTitle>
					<input
						id='file'
						type='file'
						onChange={onFileChange}
						accept='image/*'
						ref={hiddenFileInput}
						className='file-input'
					/>
					<div className='profile' onClick={handleClick}>
						<AvatarImage
							imageClassName={classes.large}
							imageData={editUserData.profilePic}
							defaultNameChar={defaultFirstName + defaultLastName}
							fileUrl={fileUrl}
						/>
					</div>
					{uploadError && <p className='file-error'>{uploadError}</p>}
					<CloseOutlinedIcon className='close-icon' onClick={props.onClose} />
					<List>
						<ListItem alignItems='flex-start'>
							<ListItemText
								classes={{ primary: classes.listItemText }}
								primary={FirstName + ' ' + LastName}
								secondary={editUserData.title}
							/>
						</ListItem>
					</List>
				</DialogTitle>
				<DialogContent dividers>
					<div className='dynamic-form-input'>
						<FormBuilder
							formBuilderInput={profileInputData}
							formBuilderOutput={profileFields || ''}
							onChange={handleInput}
						/>
					</div>
				</DialogContent>
				<DialogActions className='actions-btn'>
					<Button handleButtonOnClick={handleSave} buttonClass='save-btn'>
						{t(BUTTON_SAVE)}
					</Button>
					<Button handleButtonOnClick={onClose} buttonClass='cancel-btn'>
						{t(BUTTON_CANCEL)}
					</Button>
				</DialogActions>
			</Dialog>
		</div>
	)
}
