import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Avatar from '../../widgets/avatarImage'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import Button from '../../widgets/button/button'
import { useDispatch } from 'react-redux'
import profileOutputData from '../../config/profile/viewProfileOutput.json'
import profileInputData from '../../config/profile/viewProfile.json'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Divider from '@material-ui/core/Divider'
import { useTranslation } from 'react-i18next'
import {
	BUTTON_EDITPROFILE,
	PLATFORM_TRANSLATIONS,
} from '../../utilities/constants'
import './Profile.scss'

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: 'transparent',
		display: 'flex',
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
		margin: 'auto',
	},
	listItemText: {
		textAlign: 'center',
		fontSize: '24px',
	},
}))
export default function ViewProfile(props) {
	const { onCancel, userData } = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const classes = useStyles()
	const [profileFields, setProfileFields] = useState({
		...profileOutputData,
	})
	const userDetails = profileInputData
	const FirstName = userData.firstName
	const LastName = userData.lastName
	const defaultName = FirstName.charAt(0) + LastName.charAt(0)

	const handleEditProfile = (e) => {
		props.callback('EditProfile')
	}
	const dispatch = useDispatch()
	useEffect(() => {
		setUserDetails()
	}, [dispatch])

	const setUserDetails = () => {
		if (userData) {
			setProfileFields({
				...profileFields,
				email: userData.email,
				phoneNumber: userData.phoneNumber,
				address: userData.address,
				firstName: userData.firstName,
				lastName: userData.lastName,
				title: userData.title,
			})
		}
	}

	return (
		<div>
			<Dialog
				onClose={onCancel}
				open={props.open}
				className='viewprofile'
				BackdropProps={{
					classes: {
						root: classes.root,
					},
				}}
			>
				<DialogTitle>
					<Avatar
						imageClassName={classes.large}
						imageData={userData.profilePic}
						defaultNameChar={defaultName}
					/>
					<CloseOutlinedIcon className='close-icon' onClick={onCancel} />
					<List>
						<ListItem alignItems='flex-start'>
							<ListItemText
								classes={{ primary: classes.listItemText }}
								primary={FirstName + ' ' + LastName}
								secondary={userData.title}
							/>
						</ListItem>
					</List>
				</DialogTitle>
				<Divider className='form-divider' variant='middle' />
				<Button
					buttonClass='edit-btn'
					handleButtonOnClick={(e) => handleEditProfile(e)}
				>
					{t(BUTTON_EDITPROFILE)}
				</Button>
				<Divider className='form-divider' variant='middle' />

				{userDetails.section[0].inputFields.map((profileData, index) => (
					<DialogContent key={index}>
						<Typography className='platform-name'>
							{profileData.label}
						</Typography>
						<Typography className='content'>
							{profileFields[profileData.name]}
						</Typography>
						<Divider className='form-divider' />
					</DialogContent>
				))}
			</Dialog>
		</div>
	)
}
