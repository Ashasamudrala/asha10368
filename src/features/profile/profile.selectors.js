import slice from './profile.slice'

export const selectSlice = (state) => state[slice.name]

export const selectProfilePicture = (state) => selectSlice(state).profilePicture

export const selectProfileFilter = (state) => selectSlice(state).filter

export const selectProfileData = (state) => selectSlice(state).profileData

export const selectProfileDetails = (state) => selectSlice(state).userDetails
