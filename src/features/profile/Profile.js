import React, { useEffect, useState } from 'react'
import Dialog from '@material-ui/core/Dialog'
import { makeStyles } from '@material-ui/core/styles'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import { fetchUserDetails } from './profile.asyncActions'
import {
	selectGetLoginUserId,
	selectGetLoginUserDisplayName,
	selectGetLoginUserTitle,
} from '../../authAndPermissions/loginUserDetails.selectors'
import Button from '../../widgets/button/button'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import { useSelector, useDispatch } from 'react-redux'
import AvatarImage from '../../widgets/avatarImage'
import {
	VIEW_PROFILE,
	VIEW_PROFILE_TRANSLATIONS,
	SIGN_OUT,
} from '../../utilities/constants'
import { useTranslation } from 'react-i18next'
import './Profile.scss'

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: 'transparent',
		display: 'flex',
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
	},
}))

export default function Profile(props) {
	const classes = useStyles()
	const logInUserId = useSelector(selectGetLoginUserId)
	const logInUserDisplayName = useSelector(selectGetLoginUserDisplayName)
	const longInUserTitle = useSelector(selectGetLoginUserTitle)
	const dispatch = useDispatch()
	const [userDetails, setUserDetails] = useState({})
	const { t } = useTranslation(VIEW_PROFILE_TRANSLATIONS)
	const defaultFirstName =
		userDetails.firstName && userDetails.firstName.charAt(0)
	const defaultLastName = userDetails.lastName && userDetails.lastName.charAt(0)
	const defaultName = defaultFirstName + defaultLastName

	const handleViewProfile = (e) => {
		props.callback('ViewProfile')
	}

	const handleSignOut = (e) => {
		props.callback('SignOut')
	}

	useEffect(() => {
		dispatch(fetchUserDetails({ userId: logInUserId })).then((response) => {
			setUserDetails(response.payload)
		})
	}, [dispatch])

	return (
		<div>
			<Dialog
				onClose={props.onCancel}
				open={props.open}
				BackdropProps={{
					classes: {
						root: classes.root,
					},
				}}
				className='profile-box'
			>
				<List>
					<ListItem alignItems='flex-start'>
						<CloseOutlinedIcon
							className='close-icon'
							onClick={props.onCancel}
						/>
						<ListItemAvatar>
							<AvatarImage
								imageClassName={classes.large}
								imageData={userDetails.profilePic}
								defaultNameChar={defaultName}
							/>
						</ListItemAvatar>
						<ListItemText
							primary={logInUserDisplayName}
							secondary={
								<>
									<Typography>{longInUserTitle}</Typography>
								</>
							}
						/>
					</ListItem>
				</List>
				<Divider className='form-divider' />
				<ButtonGroup variant='text'>
					<Button
						buttonClass='profile-btn'
						handleButtonOnClick={(e) => handleViewProfile(e)}
					>
						{t(VIEW_PROFILE)}
					</Button>
					<span>
						<Divider orientation='vertical' className='form-divider' flexItem />
					</span>
					<Button
						buttonClass='signout-btn'
						handleButtonOnClick={(e) => handleSignOut(e)}
					>
						{t(SIGN_OUT)}
					</Button>
				</ButtonGroup>
			</Dialog>
		</div>
	)
}
