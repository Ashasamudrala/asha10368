import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { doAsyncUpload } from '../../infrastructure/doAsync/doAsync'
import { EDIT_PROFILE_SUCCESS_MESSAGE } from '../../utilities/constants'

export const fetchUserDetails = createAsyncThunk(
	'users/{userId}',
	async ({ useCaching, noBusySpinner, userId } = {}, thunkArgs) =>
		await doAsync({
			url: `users/${userId}`,
			useCaching,
			noBusySpinner,
			errorMessage: 'Unable to load profile. Please try again later.',
			...thunkArgs,
		})
)

export const updateProfileById = createAsyncThunk(
	'users/{userId}',
	async (
		{
			useCaching,
			noBusySpinner,
			firstName,
			lastName,
			phoneNumber,
			address,
			roleIds,
			userId,
		} = {},
		thunkArgs
	) => {
		await doAsync({
			url: `users/${userId}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					firstName,
					lastName,
					phoneNumber,
					address,
					roleIds,
				}),
			},
			successMessage: `${EDIT_PROFILE_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
	}
)

export const updateProfilePicById = createAsyncThunk(
	'users',
	async ({ fileData, userId } = {}, thunkArgs) => {
		await doAsyncUpload(`users/${userId}/picture`, fileData)
	}
)
