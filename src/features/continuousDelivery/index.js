import ContinuousDelivery from './ContinuousDelivery'
import * as selectors from './continuousDelivery.selectors'
import * as asyncActions from './continuousDelivery.asyncActions'
import slice from './continuousDelivery.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllContinuousDelivery } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectAllContinuousDelivery,
	selectContinuousDeliveryFilter,
} = selectors

// we export the component most likely to be desired by default
export default ContinuousDelivery
