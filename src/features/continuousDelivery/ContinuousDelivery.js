import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectAllContinuousDelivery } from './continuousDelivery.selectors'
import { fetchAllContinuousDelivery } from './continuousDelivery.asyncActions'
import continuousDeliveryProp from '../../config/continuousDeliveryGrid.json'
import AddContinuousDelivery from '../../../src/features/addContinuousDelivery/AddContinuousDelivery'
import ViewContinuousDeliveryDrawer from '../viewContinuousDelivery'
import viewContinuousDeliveryData from '../../config/viewContinuousDelivery.json'
import Grid from '../../widgets/grid/Grid'
import IsyButton from '../../widgets/button/button'
import { useTranslation } from 'react-i18next'
import {
	CONTINUOUSDELIVERY_TRANSLATIONS,
	ADD_CONFIGURATION,
	permission,
} from '../../utilities/constants'
import Can from '../../widgets/auth/Can'
import './continuousDelivery.scss'

export default function ContinuousDelivery() {
	const continuousDelivery = useSelector(selectAllContinuousDelivery)
	const [isDrawerOpen, setIsDrawerOpen] = useState(false)
	const [continuousDeliveryDetails, setContinuousDeliveryDetails] = useState('')
	const [isCDDrawerOpen, setCDDrawerOpen] = React.useState(false)
	const dispatch = useDispatch()
	const { t } = useTranslation(CONTINUOUSDELIVERY_TRANSLATIONS)

	const handleViewItem = (isOpen, viewItem) => {
		setContinuousDeliveryDetails(viewItem)
		setIsDrawerOpen(isOpen)
	}
	useEffect(() => {
		dispatch(fetchAllContinuousDelivery())
	}, [dispatch])

	const handleCDButtonOnClick = () => {
		setCDDrawerOpen(true)
	}
	const handleIsDrawerOpen = (open) => {
		setCDDrawerOpen(open)
	}

	return (
		<>
			<div className='add-continuous-delivery-btn'>
				<Can
					action={permission.CREATE_CONTINUOUS_DELIVERY_CONFIGURATION}
					yes={() => (
						<IsyButton
							buttonClass={'add-continuous-delivery-class'}
							handleButtonOnClick={handleCDButtonOnClick}
						>
							{t(ADD_CONFIGURATION)}
						</IsyButton>
					)}
					no={() => ''}
				></Can>
			</div>
			{isCDDrawerOpen ? (
				<AddContinuousDelivery openDrawer={handleIsDrawerOpen} />
			) : null}
			<div>
				<Grid
					handleViewItem={handleViewItem}
					properties={continuousDeliveryProp}
					data={continuousDelivery}
				/>
				{isDrawerOpen && (
					<ViewContinuousDeliveryDrawer
						continuousDeliveryDetails={continuousDeliveryDetails}
						viewContinuousDeliveryJsonData={viewContinuousDeliveryData}
						onCancel={handleViewItem}
					/>
				)}
			</div>
		</>
	)
}
