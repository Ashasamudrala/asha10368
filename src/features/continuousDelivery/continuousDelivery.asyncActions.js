import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_CONTINUOUSDELIVERY } from '../../utilities/apiEndpoints'

export const fetchAllContinuousDelivery = createAsyncThunk(
	'continuousDelivery/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ALL_CONTINUOUSDELIVERY,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
