import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './continuousDelivery.asyncActions'

const initialState = {
	allContinuousDelivery: [],
	filter: '',
}

const slice = createSlice({
	name: 'continuousDelivery',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllContinuousDelivery.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allContinuousDelivery = action.payload.content
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
