import slice from './continuousDelivery.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllContinuousDelivery = (state) =>
	selectSlice(state).allContinuousDelivery

export const selectContinuousDeliveryFilter = (state) =>
	selectSlice(state).filter
