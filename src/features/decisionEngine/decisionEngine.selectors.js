import slice from './decisionEngine.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllDecisionEngine = (state) =>
	selectSlice(state).allDecisionEngine

export const selectDecisionEngineFilter = (state) => selectSlice(state).filter
