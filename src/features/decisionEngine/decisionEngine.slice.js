import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './decisionEngine.asyncActions'

const initialState = {
	allDecisionEngine: [],
	filter: '',
}

const slice = createSlice({
	name: 'decisionEngine',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllDecisionEngine.fulfilled]: (state, action) => {
			state.allDecisionEngine = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
