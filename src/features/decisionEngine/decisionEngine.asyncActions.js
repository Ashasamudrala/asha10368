import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllDecisionEngine = createAsyncThunk(
	'decisionEngine/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'decision-engine',
			useCaching,
			noBusySpinner,
			successMessage: 'DecisionEngine loaded',
			errorMessage: 'Unable to load decisionEngine. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
