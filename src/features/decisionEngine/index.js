import DecisionEngine from './DecisionEngine'
import * as selectors from './decisionEngine.selectors'
import * as asyncActions from './decisionEngine.asyncActions'
import slice from './decisionEngine.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllDecisionEngine } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllDecisionEngine, selectDecisionEngineFilter } = selectors

// we export the component most likely to be desired by default
export default DecisionEngine
