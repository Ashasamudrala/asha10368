import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './editContinuousDelivery.asyncActions'

const initialState = {
	status: '',
}

const slice = createSlice({
	name: 'editContinuousDelivery',
	initialState,
	reducers: {
		// synchronous actions
		updateStatus(state) {
			state.status = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.updateContinuousDeliveryById.fulfilled]: (state, action) => {
			if (action.payload) {
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
