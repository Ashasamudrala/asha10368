import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { UPDATE_CONTINUOUSDELIVERY_BY_ID } from '../../utilities/apiEndpoints'
import { CONTINUOUSDELIVERY_SUCCESS_MESSAGE } from '../../utilities/constants'

export const updateContinuousDeliveryById = createAsyncThunk(
	'editContinuousDelivery/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			name,
			description,
			apiConfiguration,
			metadata,
			channelConfiguration: { channelName },
			editContinuousDeliveryId,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${UPDATE_CONTINUOUSDELIVERY_BY_ID}/${editContinuousDeliveryId}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					name,
					metadata,
					description,
					apiConfiguration,
					channelConfiguration: { channelName },
				}),
			},
			successMessage: `${name} ${CONTINUOUSDELIVERY_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)
