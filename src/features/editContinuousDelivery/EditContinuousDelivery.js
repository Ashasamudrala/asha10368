import _ from 'lodash'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import continuousDelieveryInput from '../../config/continuousDelivery/addContinuousDelivery.json'
import AbstractContinuousDelivery from '../addContinuousDelivery/AbstractContinuousDeliveryForm'
import { updateContinuousDeliveryById } from './editContinuousDelivery.asyncActions'
import { getStatus } from './editContinuousDelivery.selectors'
import { fetchAllContinuousDelivery } from '../../features/continuousDelivery/continuousDelivery.asyncActions'
import { actions } from './editContinuousDelivery.slice'
import { useTranslation } from 'react-i18next'
import '../addContinuousDelivery/continuousDelivery.scss'
import {
	CONTINUOUSDELIVERY_TRANSLATIONS,
	ADD_CONTINUOUS_DELIVERY_CONFIGURATIONS,
	EDIT_CONTINUOUS_DELIVERY_CONFIGURATIONS,
} from '../../utilities/constants'
const { updateStatus } = actions

/**
 *This component is used for displaying editRepository details
 * @param {*} props
 */

export default function EditContinuousDelivery(props) {
	const {
		name,
		description,
		channelConfiguration,
		apiConfiguration,
		metadata,
	} = JSON.parse(JSON.stringify(props.continuousDeliveryDetails))
	// there are state values for showing dynamic and Repository detials for storing the fields values
	const ProcessedContinuousDelieveryInput = _.cloneDeep(
		continuousDelieveryInput
	)
	const dispatch = useDispatch()
	const status = useSelector(getStatus)
	const { t } = useTranslation(CONTINUOUSDELIVERY_TRANSLATIONS)

	// Clone the apiConfiguration and transform metadata property.
	if (apiConfiguration.metadata) {
		apiConfiguration.metadata = _.map(
			Object.keys(apiConfiguration.metadata),
			(key) => {
				return { key: key, value: apiConfiguration.metadata[key] }
			}
		)
		ProcessedContinuousDelieveryInput.formHeader.formHeading = ProcessedContinuousDelieveryInput.formHeader.formHeading.replace(
			t(ADD_CONTINUOUS_DELIVERY_CONFIGURATIONS),
			t(EDIT_CONTINUOUS_DELIVERY_CONFIGURATIONS)
		)
	}

	/**
	 *
	 * @param {*} continuousDeliveryFields
	 * gets the api data and dispatch to api call with form data
	 */
	const handleSave = (continuousDeliveryFields) => {
		dispatch(
			updateContinuousDeliveryById({
				...continuousDeliveryFields,
				editContinuousDeliveryId: props.continuousDeliveryDetails.id,
			})
		)
	}
	useEffect(() => {
		return () => {
			dispatch(updateStatus())
		}
	}, [dispatch])
	// eslint-disable-line react-hooks/exhaustive-deps
	useEffect(() => {
		if (status !== '') {
			status && dispatch(fetchAllContinuousDelivery())
			props.openDrawer(false)
		}
	}, [status])

	return (
		<AbstractContinuousDelivery
			openDrawer={props.openDrawer}
			continuousDelieveryInput={ProcessedContinuousDelieveryInput}
			className='continuous-delivery'
			onSave={handleSave}
			continuousDelieveryOutput={{
				name: name,
				description: description,
				channelConfiguration: channelConfiguration,
				apiConfiguration: apiConfiguration,
				metadata: _.map(Object.keys(metadata), (key) => {
					return { key: key, value: metadata[key] }
				}),
			}}
		/>
	)
}
