import slice from './editContinuousDelivery.slice'

export const selectSlice = (state) => state[slice.name]

export const getStatus = (state) => selectSlice(state).status
