import EditContinuousDelivery from './EditContinuousDelivery'
import * as asyncActions from './editContinuousDelivery.asyncActions'
import slice from './editContinuousDelivery.slice'

export const {
	name,
	actions: { updateStatus },
	reducer,
} = slice

export const { updateContinuousDeliveryById } = asyncActions

// we export the component most likely to be desired by default
export default EditContinuousDelivery
