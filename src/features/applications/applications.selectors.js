import slice from './applications.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllApplications = (state) =>
	selectSlice(state).allApplications

export const selectApplicationsFilter = (state) => selectSlice(state).filter
