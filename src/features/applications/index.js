import Applications from './Applications'
import * as selectors from './applications.selectors'
import * as asyncActions from './applications.asyncActions'
import slice from './applications.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllApplications } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllApplications, selectApplicationsFilter } = selectors

// we export the component most likely to be desired by default
export default Applications
