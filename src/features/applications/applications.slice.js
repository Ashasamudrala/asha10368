import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './applications.asyncActions'

const initialState = {
	allApplications: [],
	filter: '',
}

const slice = createSlice({
	name: 'applications',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllApplications.fulfilled]: (state, action) => {
			state.allApplications = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
