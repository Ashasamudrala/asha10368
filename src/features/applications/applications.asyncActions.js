import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllApplications = createAsyncThunk(
	'applications/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'applications',
			useCaching,
			noBusySpinner,
			successMessage: 'Applications loaded',
			errorMessage: 'Unable to load applications. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
