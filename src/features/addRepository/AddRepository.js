import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import RepositoryInput from '../../config/addRepository/addRepository.json'
import RepositoryOutputData from '../../config/addRepository/addRepositoryOutput.json'
import AbstractRepository from './AbstractRepository'
import './addRepository.scss'
import { actions } from './addRepository.slice'
import { saveAddRepository } from './addRepository.asyncActions'
import { selectAddRepositoryFilter } from './addRepository.selectors'
import { fetchAllRepositories } from '../repositories/repositories.asyncActions'
const { updateFilter } = actions
/* 
this component is used to provide add repository  details whenever onclick of button function
 */

export default function AddRepository(props) {
	const dispatch = useDispatch()
	const status = useSelector(selectAddRepositoryFilter)

	// when ever addplatform is posted successfully and then dispatch to fetchallpltformsapi
	useEffect(() => {
		if (status !== '') {
			status && dispatch(fetchAllRepositories())
			props.openDrawer(false)
		}
	}, [status])

	// when ever save button is clicked dispatch to addRepository  api
	const handleSave = (RepositoryFields) => {
		dispatch(saveAddRepository({ ...RepositoryFields }))
	}
	useEffect(() => {
		return () => {
			dispatch(updateFilter())
		}
	}, [])

	return (
		<div>
			<AbstractRepository
				openDrawer={props.openDrawer}
				RepositoryInput={RepositoryInput}
				RepositoryOutputData={JSON.parse(JSON.stringify(RepositoryOutputData))}
				className='add-repository'
				onSave={handleSave}
			/>
		</div>
	)
}
