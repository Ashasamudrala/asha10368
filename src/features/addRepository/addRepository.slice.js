import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './addRepository.asyncActions'

const initialState = {
	allAddRepository: [],
	filter: '',
}

const slice = createSlice({
	name: 'addRepository',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state) {
			state.filter = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.saveAddRepository.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allAddRepository = action.payload
				state.filter = true
			} else {
				state.filter = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
