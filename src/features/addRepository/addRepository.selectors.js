import slice from './addRepository.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllAddRepository = (state) =>
	selectSlice(state).allAddRepository

export const selectAddRepositoryFilter = (state) => selectSlice(state).filter
