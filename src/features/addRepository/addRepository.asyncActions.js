import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { SAVE_REPOSITORY } from '../../utilities/apiEndpoints'

export const saveAddRepository = createAsyncThunk(
	'addRepository/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			name,
			description,
			metadata,
			apiConfiguration,
			channelConfiguration: { channelName },
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: SAVE_REPOSITORY,
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name,
					description,
					metadata,
					apiConfiguration,
					channelConfiguration: {
						channelName,
					},
				}),
			},
			successMessage: 'AddRepository is saved successfully',
			errorMessage: 'Unable to load addRepository. Please try again later.',
			...thunkArgs,
		})
)
