import AddRepository from './AddRepository'
import * as selectors from './addRepository.selectors'
import * as asyncActions from './addRepository.asyncActions'
import slice from './addRepository.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { saveAddRepository } = asyncActions

export const { selectAllAddRepository, selectAddRepositoryFilter } = selectors
// we export the component most likely to be desired by default
export default AddRepository
