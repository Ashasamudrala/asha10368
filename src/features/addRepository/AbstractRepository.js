import React, { useState } from 'react'
import RightSlider from '../../widgets/rightSideSlider/RightSideSlider'
import BusyIndicator from '../../widgets/busyIndicator/BusyIndicator'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import {
	ADD,
	ONCHANGE,
	DELETE,
	modeType,
	REPOSITORY_TRANSLATIONS,
} from '../../utilities/constants'
import _ from 'lodash'
import { useTranslation } from 'react-i18next'
import {
	fieldIsEmpty,
	descriptionIsValid,
	addNameIsValid,
} from '../addPlatform/validations'
import ErrorMessages from '../../config/addRepository/validations.json'

/* 
this component is used to provide add Repository details whenever onclick of button
 */
export default function AbstractRepository(props) {
	// there are state values for showing dynamic and repository details for storing the fields values
	const { RepositoryInput, RepositoryOutputData } = props

	const [RepositoryFields, setRepositoryFields] = useState({
		...RepositoryOutputData,
	})

	const [errors, setErrorValues] = useState({})

	const { t } = useTranslation(REPOSITORY_TRANSLATIONS)
	/**
	 * On clicking the save button it converts the metadata to key, value pairs
	 */
	const handleSave = () => {
		if (handleValidation()) {
			convertMetdataToKeyValue()
		}
	}

	const convertMetdataToKeyValue = () => {
		// Clone the apiConfiguration and transform metadata property.
		const clonedFormValues = _.cloneDeep(RepositoryFields)
		const { apiConfiguration, metadata } = clonedFormValues
		let processedMetadata = {}
		if (metadata && metadata.length >= 0) {
			processedMetadata = _.mapValues(_.keyBy(metadata, 'key'), 'value')
			processedMetadata = _.omit(processedMetadata, [''])
		}
		if (
			apiConfiguration &&
			apiConfiguration.metadata &&
			apiConfiguration.metadata.length >= 0
		) {
			clonedFormValues.apiConfiguration.metadata = _.mapValues(
				_.keyBy(apiConfiguration.metadata, 'key'),
				'value'
			)
		}
		props.onSave({
			...clonedFormValues,
			metadata: { ...processedMetadata },
		})
	}

	/**
	 * Form Validations
	 */
	const handleValidation = () => {
		const fields = { ...RepositoryFields }
		const errors = {}
		const inputFields = RepositoryInput.section.map(
			(field) => field.inputFields
		)
		const inputElement = inputFields[0]
		const inputElement2 = inputFields[1]
		const inputSubElement = inputFields[2]

		inputElement.forEach((inputField) => {
			if (inputField.required) {
				if (fieldIsEmpty(_.get(fields, inputField.name))) {
					errors[inputField.name] = ErrorMessages[inputField.name]
				}
			}
		})

		inputElement2.forEach((inputField) => {
			if (inputField.required) {
				if (fieldIsEmpty(_.get(fields, inputField.name))) {
					errors[inputField.name] = ErrorMessages[inputField.name]
				}
			}
		})

		inputSubElement.forEach((inputField) => {
			if (inputField.required) {
				if (fieldIsEmpty(_.get(fields, inputField.name))) {
					errors[inputField.name] = ErrorMessages[inputField.name]
				}
			}
		})
		if (descriptionIsValid(fields.description)) {
			errors.description = ErrorMessages.invalidDescription
		}
		if (fields.name !== '' && addNameIsValid(fields.name)) {
			errors.name = ErrorMessages.invalidName
		}
		setErrorValues({ ...errors })
		return _.isEmpty(errors)
	}

	// this is used for setting the json fields
	const handleRepositoryFields = (event, fieldName, action) => {
		/**
		 * if fieldName is defined then if condition executes this is for apiD and version
		 * and then else executes when ever fieldName is undefined
		 */
		let processedfieldName = []
		if (fieldName) {
			/**
			 * based on action switch case executes
			 */
			switch (action) {
				case t(ADD):
					processedfieldName = RepositoryFields
					processedfieldName = _.set(processedfieldName, fieldName, [
						event,
						..._.get(processedfieldName, fieldName),
					])
					setRepositoryFields({ ...processedfieldName })
					break
				case t(ONCHANGE):
					/**
					 * It is used when onchange of metadata is called and push the change value
					 * based on id into array if type is (metadata)
					 */
					processedfieldName = RepositoryFields
					_.get(processedfieldName, fieldName)[event.target.dataset.id][
						event.target.name
					] = event.target.value
					setRepositoryFields({ ...processedfieldName })
					break
				case t(DELETE):
					/**
					 * It is used when delete of metadata and version  is called and removes the item
					 * based on id from array
					 */
					processedfieldName = RepositoryFields
					processedfieldName = _.set(
						processedfieldName,
						fieldName,
						_.get(processedfieldName, fieldName).filter(
							(version, id) => event !== id
						)
					)
					setRepositoryFields({ ...processedfieldName })
					break
				default:
			}
		} else {
			processedfieldName = RepositoryFields
			processedfieldName = _.set(
				processedfieldName,
				event.target.name,
				event.target.value
			)
			setRepositoryFields({ ...processedfieldName })
		}
	}
	/**
	 * this is used for when clicking on metadata to move to dynamic add component
	 */
	const renderRightSideSlider = () => {
		return (
			<RightSlider onCancel={props.openDrawer} drawerName='drawer'>
				<BusyIndicator>
					<CloseOutlinedIcon
						className='close-icon'
						onClick={() => {
							props.openDrawer()
						}}
					/>
					<div className='repository'>
						<div className={props.className}>
							<FormBuilder
								formBuilderInput={RepositoryInput}
								onChange={handleRepositoryFields}
								formBuilderOutput={RepositoryFields}
								errors={errors}
							/>
						</div>
						<SliderFooter
							mode={modeType.WRITE}
							handleClose={props.openDrawer}
							handleSave={handleSave}
						/>
					</div>
				</BusyIndicator>
			</RightSlider>
		)
	}
	return <div>{renderRightSideSlider()}</div>
}
