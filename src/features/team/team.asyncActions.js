import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import {
	GET_ACTIVE_SEARCH_USERS,
	GET_PENDING_SEARCH_USERS,
} from '../../utilities/apiEndpoints'

export const fetchActiveSearchResults = createAsyncThunk(
	'users/search?queryString={data}',
	async ({ useCaching, noBusySpinner, data } = {}, thunkArgs) =>
		await doAsync({
			url: `${GET_ACTIVE_SEARCH_USERS}?queryString=${data}`,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const fetchPendingSearchResults = createAsyncThunk(
	'invitations/search?queryString={data}',
	async ({ useCaching, noBusySpinner, data } = {}, thunkArgs) =>
		await doAsync({
			url: `${GET_PENDING_SEARCH_USERS}?queryString=${data}`,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
