import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './team.asyncActions'

const initialState = {
	searchResult: [],
	filter: '',
	searchStatus: false,
	searchFilter: '',
	searchString: '',
	activeTab: 0,
}

const slice = createSlice({
	name: 'team',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
		updateSearchStatus(state, action) {
			if (action.payload) {
				state.searchStatus = action.payload.searchStatus
			}
		},
		updateSearchString(state, action) {
			if (action.payload) {
				state.searchString = action.payload.value
			}
		},
		setActiveTab(state, action) {
			state.activeTab = action.payload
		},
		removeSearchData(state) {
			state.searchResult = []
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchActiveSearchResults.fulfilled]: (state, action) => {
			if (action.payload) {
				action.payload.content.forEach((item) => {
					item.checked = false
				})
				state.searchResult = action.payload.content
				state.searchFilter = action.payload
			}
		},
		[asyncActions.fetchPendingSearchResults.fulfilled]: (state, action) => {
			if (action.payload) {
				action.payload.content.forEach((item) => {
					item.checked = false
				})
				state.searchResult = action.payload.content
				state.searchFilter = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
