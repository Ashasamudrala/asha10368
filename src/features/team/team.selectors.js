import slice from './team.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllUsersFromSearchResults = (state) =>
	selectSlice(state).searchResult

export const selectTeamFilter = (state) => selectSlice(state).filter

export const selectSearchStatus = (state) => selectSlice(state).searchStatus

export const selectSearchFilter = (state) => selectSlice(state).searchFilter

export const selectSearchString = (state) => selectSlice(state).searchString

export const getActiveTab = (state) => selectSlice(state).activeTab
