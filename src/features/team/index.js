import Team from './Team'
import * as selectors from './team.selectors'
import * as asyncActions from './team.asyncActions'
import slice from './team.slice'

export const {
	name,
	actions: { updateSearchStatus, updateSearchString },
	reducer,
} = slice

export const { fetchActiveSearchResults } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllUsersFromSearchResults, selectTeamFilter } = selectors

// we export the component most likely to be desired by default
export default Team
