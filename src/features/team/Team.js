import React, { useState, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Typography } from '@material-ui/core'
import tabProps from '../../config/Team/teams.json'
import ConfirmDialog from '../../widgets/confirmDialog/confirmDialog'
import Tabs from '../../widgets/tabs'
import { useTranslation } from 'react-i18next'
import IsyButton from '../../widgets/button/button'
import './team.scss'
import {
	DELETE_ID_NUMBER,
	INVITE_MEMBER,
	TEAM_TRANSLATIONS,
	SEARCH_PENDING_PLACEHOLDER,
	SEARCH_ACTIVE_PLACEHOLDER,
	INVITE_USER_TRANSFORMATIONS,
	CONFIRMATION_MESSAGE_DELETE,
	DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE,
	USERS,
	USER,
	MY_TEAM,
	permission,
} from '../../utilities/constants'
import Can from '../../widgets/auth/Can'
import InviteUser from '../inviteUser'
import {
	fetchActiveSearchResults,
	fetchPendingSearchResults,
} from './team.asyncActions'
import UserGridProp from '../../config/teamGrid.json'
import Search from '../../widgets/search/Search'
import inviteUserFields from '../../config/inviteUsers/inviteUsers.json'
import { getSelectedActiveMembers } from '../activeMembers/activeMembers.selectors'
import { actions } from './team.slice'
import { actions as activeActions } from '../activeMembers/activeMembers.slice'
import { actions as pendingActions } from '../pendingInvites/pendingInvites.slice'
import {
	deleteBulkUsers,
	fetchAllActiveMembers,
} from '../activeMembers/activeMembers.asyncActions'
import { getSelectedPendingInvites } from '../pendingInvites/pendingInvites.selectors'
import {
	fetchAllPendingInvites,
	delteBulkPendingMembers,
} from '../pendingInvites/pendingInvites.asyncActions'
import { getActiveTab } from './team.selectors'
import { selectGetLogInUserInfo } from '../../authAndPermissions/loginUserDetails.selectors'
import { fetchAllInviteUserRoles } from '../../features/inviteUser/inviteUser.asyncActions'
import _ from 'lodash'
const { updateSearchStatus, updateSearchString } = actions
const { updateSelectAll } = activeActions
const { updateSelectAll: pendingselectAll } = pendingActions

export default function Team() {
	const { t } = useTranslation([TEAM_TRANSLATIONS, INVITE_USER_TRANSFORMATIONS])
	const [isDialogOpen, setIsDialogOpen] = useState(false)
	const [openConfirmDelete, setOpenConfirmDelete] = useState(false)
	const loginDetails = useSelector(selectGetLogInUserInfo)

	const selectedActiveMembers = useSelector(getSelectedActiveMembers)
	const selectedPendingInvites = useSelector(getSelectedPendingInvites)
	const activeTab = useSelector(getActiveTab)
	const dispatch = useDispatch()
	const searchInputRef = useRef()

	const handleDeleteUsers = () => {
		setOpenConfirmDelete(true)
	}
	useEffect(() => {
		if (
			loginDetails &&
			loginDetails.permissions &&
			loginDetails.permissions.includes(permission.UPDATE_ROLE)
		) {
			dispatch(fetchAllInviteUserRoles())
		}
	}, [dispatch])
	useEffect(() => {
		if (searchInputRef && searchInputRef.current) {
			searchInputRef.current.value = ''
		}
	}, [activeTab])

	const handleDeleteBulkUsers = () => {
		if (activeTab === 0) {
			handleDeleteBulkActiveUsers()
		} else {
			handleDeletePendingInvites()
		}
	}

	const handleDeleteBulkActiveUsers = () => {
		const payload = buildDeletePayload(selectedActiveMembers)
		dispatch(deleteBulkUsers(payload)).then(() => {
			dispatch(fetchAllActiveMembers())
		})
		setOpenConfirmDelete(false)
	}

	const handleDeletePendingInvites = () => {
		const payload = buildDeletePayload(selectedPendingInvites)
		dispatch(delteBulkPendingMembers(payload)).then(() => {
			dispatch(fetchAllPendingInvites())
		})
		setOpenConfirmDelete(false)
	}

	const buildDeletePayload = (selectedUsers) => {
		const userIds = []
		selectedUsers.forEach((member) => {
			userIds.push(member.id)
		})

		const deleteSuccessMessage = `${userIds.length} ${t(
			DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE,
			{
				user: userIds.length > 1 ? t(USERS) : t(USER),
			}
		)}`
		return { userIds, deleteSuccessMessage }
	}

	const handleCloseDeleteDialog = () => {
		if (activeTab === 0) {
			dispatch(updateSelectAll({ value: false }))
		} else {
			dispatch(pendingselectAll({ value: false }))
		}
		setOpenConfirmDelete(false)
	}

	const handleInviteUser = () => {
		setIsDialogOpen(true)
	}
	const handleClose = (isDialogOpen) => {
		setIsDialogOpen(isDialogOpen)
	}

	const handleChange = (e) => {
		if (_.isEmpty(e.target.value)) {
			dispatch(updateSearchStatus({ searchStatus: false }))
		}
	}

	const handleSearch = (searchString) => {
		dispatch(updateSearchStatus({ searchStatus: true }))

		if (activeTab === 0) {
			dispatch(
				fetchActiveSearchResults({
					data: searchString,
				})
			)
		} else {
			dispatch(
				fetchPendingSearchResults({
					data: searchString,
				})
			)
		}
		dispatch(
			updateSearchString({
				value: searchString,
			})
		)
	}

	const renderButton = () => {
		const selectedMembers = activeTab
			? selectedPendingInvites
			: selectedActiveMembers
		return (
			<Can
				action={permission.CREATE_INVITATION}
				yes={() =>
					selectedMembers.length > 0 ? (
						<IsyButton
							buttonClass={'team-btn'}
							handleButtonOnClick={handleDeleteUsers}
						>
							{t(DELETE_ID_NUMBER, { number: selectedMembers.length })}
						</IsyButton>
					) : (
						<IsyButton
							buttonClass={'team-btn'}
							handleButtonOnClick={handleInviteUser}
						>
							{t(INVITE_MEMBER)}
						</IsyButton>
					)
				}
				no={() => ''}
			></Can>
		)
	}
	function renderTeams() {
		return (
			<>
				<div className='team-container'>
					<div className='team-title'>
						<Typography>{t(MY_TEAM)}</Typography>
					</div>
					<div className='button-component'>
						<Search
							searching={UserGridProp}
							handleChange={handleChange}
							handleSearch={handleSearch}
							searchRef={searchInputRef}
							searchIcon={true}
							iconPosition={'left'}
							searchPlaceholder={
								activeTab
									? t(SEARCH_PENDING_PLACEHOLDER)
									: t(SEARCH_ACTIVE_PLACEHOLDER)
							}
						/>
						<div className='action-buttons'>{renderButton()}</div>
					</div>
					<div className='header'>
						<div className='tabs-component'>
							<Tabs tabProp={tabProps}></Tabs>
						</div>
					</div>
				</div>
			</>
		)
	}
	return (
		<>
			{renderTeams()}
			{isDialogOpen && (
				<InviteUser
					inviteUsers={inviteUserFields}
					handleClose={handleClose}
					isDialogOpen={isDialogOpen}
				/>
			)}

			<ConfirmDialog
				open={openConfirmDelete}
				onClose={handleCloseDeleteDialog}
				onConfirm={handleDeleteBulkUsers}
				message={t(CONFIRMATION_MESSAGE_DELETE)}
			/>
		</>
	)
}
