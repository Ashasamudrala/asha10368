import React from 'react'
import Tabs from '../../widgets/tabs'
import tabs from '../../config/coreTab.json'

export default function Core() {
	return <Tabs tabProp={tabs} />
}
