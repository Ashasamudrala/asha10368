import Core from './Core'
import * as selectors from './core.selectors'
import * as asyncActions from './core.asyncActions'
import slice from './core.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllCore } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllCore, selectCoreFilter } = selectors

// we export the component most likely to be desired by default
export default Core
