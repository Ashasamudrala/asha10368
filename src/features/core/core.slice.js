import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './core.asyncActions'

const initialState = {
	allCore: [],
	filter: '',
}

const slice = createSlice({
	name: 'core',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllCore.fulfilled]: (state, action) => {
			state.allCore = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
