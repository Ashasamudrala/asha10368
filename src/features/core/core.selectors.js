import slice from './core.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllCore = (state) => selectSlice(state).allCore

export const selectCoreFilter = (state) => selectSlice(state).filter
