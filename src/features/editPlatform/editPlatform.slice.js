import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './editPlatform.asyncActions'

const initialState = {
	platformDetails: [],
	status: '',
}

const slice = createSlice({
	name: 'editPlatform',
	initialState,
	reducers: {
		// synchronous actions
		removePlatformDetails(state) {
			state.platformDetails = []
		},
		removeStatus(state) {
			state.status = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.getPlatformById.fulfilled]: (state, action) => {
			if (action) {
				state.platformDetails = action.payload
			}
		},
		[asyncActions.updatePlatformById.fulfilled]: (state, action) => {
			if (action && action.payload) {
				state.platformDetails = action.payload
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
