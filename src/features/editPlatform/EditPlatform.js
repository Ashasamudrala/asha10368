import _ from 'lodash'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import platformInput from '../../config/editPlatform/editPlatform.json'
import {
	getPlatformById,
	updatePlatformById,
} from './editPlatform.asyncActions'
import { getPlatformDetails, getPlatformStatus } from './editPlatform.selectors'
import { actions } from './editPlatform.slice'
import './editPlatform.scss'
import { fetchAllPlatform } from '../platform/platform.asyncActions'
import AbstractPlatform from '../addPlatform/AbstractPlatformForm'
import platformOutputData from '../../config/addPlatform/addPlatformOutput.json'
const { removePlatformDetails, removeStatus } = actions

/**
 *This component is used for displaying editplatform details
 * @param {*} props
 */

export default function EditPlatform(props) {
	// there are state values for showing dynamic and platform detials for storing the fields values
	const [platformFields, setPlatformFields] = useState({
		...platformOutputData,
	})

	// get the data from selectors
	const platformDetails = useSelector(getPlatformDetails)
	const status = useSelector(getPlatformStatus)

	const dispatch = useDispatch()

	// whenever change in id then dispatch action to get details based on id and set data in selector
	useEffect(() => {
		props.platformId &&
			!_.isEmpty(props.platformId) &&
			dispatch(getPlatformById({ editPlatformId: props.platformId }))
	}, [props.platformId]) // eslint-disable-line react-hooks/exhaustive-deps

	// setting the platformfields whenever the details fetched based on id from selector
	const setPlatformDetails = (platformDetails) => {
		if (platformDetails && platformDetails.metadata) {
			platformInput.formHeader.formHeading = platformDetails.name
			setPlatformFields({
				...platformFields,
				name: platformDetails.name,
				description: platformDetails.description,
				versions: platformDetails.versions,
				metadata: convertMetadatatoKeyValueObj(),
			})
		}
	} // eslint-disable-line react-hooks/exhaustive-deps
	// whenever the details posted succesffully calling platform details api
	useEffect(() => {
		status && dispatch(fetchAllPlatform())
		if (status !== '') {
			props.openDrawer()
		}
	}, [status]) // eslint-disable-line react-hooks/exhaustive-deps

	// whenever componentwillmount  updating with empty array in for platformdetails and emptying status
	useEffect(() => {
		return () => {
			dispatch(removePlatformDetails())
			dispatch(removeStatus())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	// converts key value pair according to metadata componenet
	const convertMetadatatoKeyValueObj = () => {
		return _.without(
			_.map(Object.keys(platformDetails.metadata), (key) => {
				if (key !== '') {
					return { key: key, value: platformDetails.metadata[key] }
				}
			}),
			undefined,
			null,
			''
		)
	}
	useEffect(() => {
		setPlatformDetails(platformDetails)
	}, [platformDetails]) // eslint-disable-line react-hooks/exhaustive-deps

	const handleSave = (platformFields) => {
		dispatch(
			updatePlatformById({
				...platformFields,
				editPlatformId: props.platformId,
			})
		)
	}
	return (
		<AbstractPlatform
			openDrawer={props.openDrawer}
			platformInput={platformInput}
			platformOutputData={platformFields}
			onSave={handleSave}
			className='edit-platform'
		/>
	)
}
