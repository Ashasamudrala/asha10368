import EditPlatform from './EditPlatform'
import * as asyncActions from './editPlatform.asyncActions'
import slice from './editPlatform.slice'

export const {
	name,
	actions: { removePlatformDetails, removeStatus },
	reducer,
} = slice

export const { getPlatformById, updatePlatformById } = asyncActions

// we export the component most likely to be desired by default
export default EditPlatform
