import slice from './editPlatform.slice'

export const selectSlice = (state) => state[slice.name]

export const getPlatformDetails = (state) => selectSlice(state).platformDetails

export const getPlatformStatus = (state) => selectSlice(state).status
