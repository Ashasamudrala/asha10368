import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_PLATFORM_BY_ID } from '../../utilities/apiEndpoints'
import {
	EDIT_PLATFORM_SUCCESS_MESSAGE,
	PLATFORMID,
} from '../../utilities/constants'

export const getPlatformById = createAsyncThunk(
	'editPlatform/getAll',
	async ({ useCaching, noBusySpinner, editPlatformId } = {}, thunkArgs) =>
		await doAsync({
			url: GET_PLATFORM_BY_ID.replace(PLATFORMID, editPlatformId),
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const updatePlatformById = createAsyncThunk(
	'editPlatform/save',
	async (
		{
			useCaching,
			noBusySpinner,
			metadata,
			name,
			description,
			versions,
			editPlatformId,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: GET_PLATFORM_BY_ID.replace(`${PLATFORMID}`, editPlatformId),
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					metadata,
					name,
					description,
					versions,
				}),
			},
			successMessage: `${name} ${EDIT_PLATFORM_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)
