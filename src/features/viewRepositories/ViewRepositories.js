import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import BusyIndicator from '../../widgets/busyIndicator'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import GetImagePath from '../../utilities/utilities'
import RightSideSlider from '../../widgets/rightSideSlider'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import EditRepository from '../../features/editRepository'
import { modeType, permission } from '../../utilities/constants'
import Can from '../../widgets/auth/Can'
import './viewRepositories.scss'

export default function ViewRepositories(props) {
	const { repositoryDetails, viewRepositoriesJsonData } = props
	const [openEditFramework, setEditRespoitory] = useState(false)
	/**
	 * Render API Configuration Details.
	 * @param {object} apiConfigurationData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderApiConfiguration = (apiConfigurationData, index, jsonItem) => {
		return (
			<>
				{jsonItem.apiConfigChildren &&
					jsonItem.apiConfigChildren.map((item, i) =>
						renderLabel(item, i, apiConfigurationData)
					)}
			</>
		)
	}

	/**
	 * Render endpoints details.
	 * @param {object} apiConfigData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderEndPoints = (apiConfigData, index, jsonItem) => {
		if (apiConfigData.endpoints.length > 0) {
			return (
				<table>
					<thead>
						<tr>
							{jsonItem.endPointChildren.map((item, i) => (
								<th key={i} className={item.className}>
									{item.label}
								</th>
							))}
						</tr>
					</thead>
					<tbody>
						{apiConfigData &&
							apiConfigData.endpoints.map((item, i) => (
								<tr key={i}>
									<td className='content'>{item.name}</td>
									<td className='content table-value'>{item.endpoint}</td>
								</tr>
							))}
					</tbody>
				</table>
			)
		}
	}

	const renderMetaData = (metaData, index, jsonItem) => {
		const metaKeyValue = Object.entries(metaData)
		return (
			<>
				<table>
					<tbody>
						{metaKeyValue &&
							metaKeyValue.map(([key, value], i) => (
								<tr key={i} className={jsonItem.metaDataClass}>
									<td className={jsonItem.keyClass}>{key}</td>
									<td className={jsonItem.valueClass}>{value}</td>
								</tr>
							))}
					</tbody>
				</table>
			</>
		)
	}

	const renderApiMetaData = (apiConfig, index, jsonItem) => {
		if (!apiConfig.metadata.metadata) {
			const metaKeyValue = Object.entries(apiConfig.metadata)
			return (
				<table>
					<tbody>
						{metaKeyValue &&
							metaKeyValue.map(([key, value], i) => (
								<tr key={i} className={jsonItem.metaDataClass}>
									<td className={jsonItem.keyClass}>{key}</td>
									<td className={jsonItem.valueClass}>{value}</td>
								</tr>
							))}
					</tbody>
				</table>
			)
		}
	}

	const renderChannelConfiguration = (channelConfig, index, jsonItem) => {
		return (
			<>
				{jsonItem.channelConfigChildren &&
					jsonItem.channelConfigChildren.map((item, i) =>
						renderLabel(item, i, channelConfig)
					)}
			</>
		)
	}

	/**
	 * Render Icon's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderIcon = (item, index) => {
		return (
			<Can
				action={permission.UPDATE_VERSION_CONTROL_SYSTEM_CONFIGURATION}
				yes={() => (
					<div key={index} className={item.className} onClick={handleEditIcon}>
						<span>
							<span className={item.iconClass}>{GetImagePath(item.icon)}</span>
							<span className={item.labelClass}>{item.attributeName}</span>
						</span>
					</div>
				)}
				no={() => ''}
			></Can>
		)
	}

	/**
	 * Render label's.
	 * @param {object} item - Json data.
	 * @param {number} index
	 * @param {object} responseData
	 */
	const renderLabel = (item, index, responseData) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{item.label}
				</Typography>
				<Typography className={item.content_class}>
					{responseData[item.attributeName]}
				</Typography>
			</>
		)
	}

	/**
	 * Render name's of the heading's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderName = (item, index) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{repositoryDetails[item.attributeName]
						? repositoryDetails[item.attributeName]
						: item.attributeName}
				</Typography>
			</>
		)
	}

	/**
	 * Render field details.
	 * @param {object} viewRepositoryFields
	 */
	const renderFields = (viewRepositoryFields) => {
		return viewRepositoryFields.map((item, i) => (
			<>
				{item.icon ? renderIcon(item, i) : renderName(item, i)}
				{item.apiConfig
					? renderApiConfiguration(repositoryDetails[item.apiConfig], i, item)
					: null}
				{item.children
					? item.children.map((details, index) =>
							renderLabel(details, index, repositoryDetails)
					  )
					: null}
				{item.endPoints
					? renderEndPoints(repositoryDetails[item.endApiConfig], i, item)
					: null}
				{item.apiMetadata
					? renderApiMetaData(
							repositoryDetails[item.metaDataApiConfig],
							i,
							item
					  )
					: null}
				{item.channelConfig
					? renderChannelConfiguration(
							repositoryDetails[item.channelConfig],
							i,
							item
					  )
					: null}
				{item.metadata
					? renderMetaData(repositoryDetails[item.metadata], i, item)
					: null}
			</>
		))
	}

	/**
	 * Calling renderFields.
	 * @param {object} viewRepositoryFields
	 */
	const renderViewRepository = (viewRepositoryFields) => {
		return viewRepositoryFields ? renderFields(viewRepositoryFields) : null
	}

	/**
	 * to handle to cancel button while closing
	 */
	const handleButtonOnClick = () => {
		props.onCancel(false)
	}

	const handleEditIcon = () => {
		// props.onCancel(false)
		setEditRespoitory(true)
	}

	return openEditFramework ? (
		<EditRepository
			repositoryDetails={repositoryDetails}
			openDrawer={handleButtonOnClick}
		/>
	) : (
		<div className='view-repository'>
			<RightSideSlider onCancel={props.onCancel} drawerName='drawer'>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => {
						handleButtonOnClick()
					}}
				/>
				<BusyIndicator>
					<div className='right-side-drawer'>
						{repositoryDetails
							? renderViewRepository(viewRepositoriesJsonData.fields)
							: null}
					</div>
					<SliderFooter
						mode={modeType.READ}
						handleClose={handleButtonOnClick}
					/>
				</BusyIndicator>
			</RightSideSlider>
		</div>
	)
}
ViewRepositories.propTypes = {
	repositoryDetails: PropTypes.object,
	renderIcon: PropTypes.func,
	renderName: PropTypes.func,
	renderApiConfiguration: PropTypes.func,
	renderEndPoints: PropTypes.func,
	renderFields: PropTypes.func,
	renderChannelConfiguration: PropTypes.func,
	renderViewRepository: PropTypes.func,
}
