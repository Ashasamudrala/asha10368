import slice from './viewRepositories.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllViewRepositories = (state) =>
	selectSlice(state).allViewRepositories

export const selectViewRepositoriesFilter = (state) => selectSlice(state).filter
