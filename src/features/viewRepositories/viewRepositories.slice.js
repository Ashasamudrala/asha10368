import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './viewRepositories.asyncActions'

const initialState = {
	allViewRepositories: [],
	filter: '',
}

const slice = createSlice({
	name: 'viewRepositories',
	initialState,
	reducers: {
		// synchronous actions
		removeRepositoriesData(state) {
			state.allViewPlatform = []
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllViewRepositories.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allViewRepositories = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
