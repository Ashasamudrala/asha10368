import ViewRepositories from './ViewRepositories'
import * as selectors from './viewRepositories.selectors'
import * as asyncActions from './viewRepositories.asyncActions'
import slice from './viewRepositories.slice'

export const {
	name,
	actions: { removeRepositoriesData },
	reducer,
} = slice

export const { fetchAllViewRepositories } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectAllViewRepositories,
	selectViewRepositoriesFilter,
} = selectors

// we export the component most likely to be desired by default
export default ViewRepositories
