import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllViewRepositories = createAsyncThunk(
	'viewRepositories/byId',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: '',
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
