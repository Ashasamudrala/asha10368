import slice from './devops.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllDevops = (state) => selectSlice(state).allDevops

export const selectDevopsFilter = (state) => selectSlice(state).filter
