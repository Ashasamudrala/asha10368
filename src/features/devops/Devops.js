import React from 'react'
import Tabs from '../../widgets/tabs'
import tabs from '../../config/devopsTab.json'

export default function Devops() {
	return <Tabs tabProp={tabs} />
}
