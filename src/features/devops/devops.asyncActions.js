import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllDevops = createAsyncThunk(
	'devops/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'devops',
			useCaching,
			noBusySpinner,
			successMessage: 'Devops loaded',
			errorMessage: 'Unable to load devops. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
