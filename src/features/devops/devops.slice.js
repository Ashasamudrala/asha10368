import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './devops.asyncActions'

const initialState = {
	allDevops: [],
	filter: '',
}

const slice = createSlice({
	name: 'devops',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllDevops.fulfilled]: (state, action) => {
			state.allDevops = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
