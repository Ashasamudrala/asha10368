import Devops from './Devops'
import * as selectors from './devops.selectors'
import * as asyncActions from './devops.asyncActions'
import slice from './devops.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllDevops } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllDevops, selectDevopsFilter } = selectors

// we export the component most likely to be desired by default
export default Devops
