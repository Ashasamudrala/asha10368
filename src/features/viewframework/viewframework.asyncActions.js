import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllViewframework = createAsyncThunk(
	'viewframework/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'viewframework',
			useCaching,
			noBusySpinner,
			successMessage: 'Viewframework loaded',
			errorMessage: 'Unable to load viewframework. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
