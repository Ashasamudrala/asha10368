import Viewframework from './Viewframework'
import * as selectors from './viewframework.selectors'
import * as asyncActions from './viewframework.asyncActions'
import slice from './viewframework.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllViewframework } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllViewframework, selectViewframeworkFilter } = selectors

// we export the component most likely to be desired by default
export default Viewframework
