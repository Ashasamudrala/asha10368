import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './viewframework.asyncActions'

const initialState = {
	allViewframework: [],
	filter: '',
}

const slice = createSlice({
	name: 'viewframework',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllViewframework.fulfilled]: (state, action) => {
			state.allViewframework = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
