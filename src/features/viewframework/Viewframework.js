import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import BusyIndicator from '../../widgets/busyIndicator'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import GetImagePath from '../../utilities/utilities'
import RightSideSlider from '../../widgets/rightSideSlider'
import EditFramework from '../../features/editFramework'
import Can from '../../widgets/auth/Can'
import { modeType, permission } from '../../utilities/constants'
import './viewFramework.scss'

export default function Viewframework(props) {
	const { viewFrameworkDetails, viewFrameworkJsonData } = props
	const [openEditFramework, setEditFramework] = useState(false)

	/**
	 * Render channel configuration.
	 * @param {object} channelConfig
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderChannelConfiguration = (channelConfig, index, jsonItem) => {
		return (
			<>
				{jsonItem.channelConfigChildren &&
					jsonItem.channelConfigChildren.map((item, i) =>
						renderLabel(item, i, channelConfig)
					)}
			</>
		)
	}

	/**
	 * Render version modules.
	 * @param {object} modules
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderVersionModules = (modules, index, jsonItem) => {
		if (modules.length > 0) {
			return (
				<table>
					<thead>
						<tr>
							{jsonItem.versionModule.map((item, i) => (
								<th key={i} className={item.className}>
									{item.label}
								</th>
							))}
						</tr>
					</thead>
					<tbody>
						{modules.map((item, i) => (
							<tr key={i}>
								<td className='table-value'>{item.name}</td>
								<td className='table-value'>{item.description}</td>
								<td className='table-value'>{item.version}</td>
							</tr>
						))}
					</tbody>
				</table>
			)
		}
	}

	/**
	 * Render versions.
	 * @param {object} versions
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderVersions = (versions, index, jsonItem) => {
		return (
			<>
				{versions.map((version, i) => (
					<div key={i} className='version-border'>
						<p className={jsonItem.labelClass}>{jsonItem.versionName}</p>
						<p className={jsonItem.content_class}>{version.versionName}</p>
						<p className={jsonItem.labelClass}>{jsonItem.platformVersion}</p>
						<p className={jsonItem.content_class}>
							{version.supportedPlatformVersions}
						</p>
						<p className={jsonItem.labelClass}>{jsonItem.versionMetadata}</p>
						<p>
							{version.metadata &&
								renderMetadata(version.metadata, i, jsonItem)}
						</p>
						<p className='label'>{jsonItem.versionModules}</p>
						{version.modules &&
							renderVersionModules(version.modules, i, jsonItem)}
					</div>
				))}
			</>
		)
	}

	/**
	 * Render metadata.
	 * @param {object} metaData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderMetadata = (metaData, index, jsonItem) => {
		const metaKeyValue = Object.entries(metaData)
		return (
			<>
				{metaKeyValue &&
					metaKeyValue.map(([key, value], i) => (
						<p key={i}>
							<span className={jsonItem.keyClass}>{key}</span>
							<span className={jsonItem.valueClass}>{value}</span>
						</p>
					))}
			</>
		)
	}

	/**
	 * Render Icon's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderIcon = (item, index) => {
		return (
			<Can
				action={permission.UPDATE_FRAMEWORK}
				yes={() => (
					<div key={index} className={item.className} onClick={handleEditIcon}>
						<span>
							<span className={item.iconClass}>{GetImagePath(item.icon)}</span>
							<span className={item.labelClass}>{item.attributeName}</span>
						</span>
					</div>
				)}
				no={() => ''}
			></Can>
		)
	}

	/**
	 * Render label's.
	 * @param {object} item - Json data.
	 * @param {number} index
	 * @param {object} responseData
	 */
	const renderLabel = (item, index, responseData) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{item.label}
				</Typography>
				<Typography className={item.content_class}>
					{responseData[item.attributeName]}
				</Typography>
			</>
		)
	}

	/**
	 * Render name's of the heading's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderName = (item, index) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{viewFrameworkDetails[item.attributeName]
						? viewFrameworkDetails[item.attributeName]
						: item.attributeName}
				</Typography>
			</>
		)
	}

	/**
	 * Render field details.
	 * @param {object} viewFrameworkFields
	 */
	const renderFields = (viewFrameworkFields) => {
		return viewFrameworkFields.map((item, i) => (
			<>
				{item.icon ? renderIcon(item, i) : renderName(item, i)}
				{item.children
					? item.children.map((details, index) =>
							renderLabel(details, index, viewFrameworkDetails)
					  )
					: null}
				{item.versions
					? renderVersions(viewFrameworkDetails[item.versions], i, item)
					: null}
				{item.channelConfig
					? renderChannelConfiguration(
							viewFrameworkDetails[item.channelConfig],
							i,
							item
					  )
					: null}
				{item.metadata
					? renderMetadata(viewFrameworkDetails[item.metadata], i, item)
					: null}
			</>
		))
	}

	/**
	 * Calling renderFields.
	 * @param {object} viewFrameworkFields
	 */
	const renderViewFramework = (viewFrameworkFields) => {
		return viewFrameworkFields ? renderFields(viewFrameworkFields) : null
	}

	/**
	 * to handle to cancel button while closing
	 */
	const handleButtonOnClick = () => {
		props.onCancel(false)
	}

	const handleEditIcon = () => {
		// props.onCancel(false)
		setEditFramework(true)
	}
	return openEditFramework ? (
		<EditFramework
			frameworkDetails={viewFrameworkDetails}
			openDrawer={handleButtonOnClick}
		/>
	) : (
		<div className='view-framework'>
			<RightSideSlider onCancel={props.onCancel} drawerName='drawer'>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => {
						handleButtonOnClick()
					}}
				/>
				<BusyIndicator>
					<div className='right-side-drawer'>
						{viewFrameworkDetails
							? renderViewFramework(viewFrameworkJsonData.fields)
							: null}
					</div>
					<SliderFooter
						mode={modeType.READ}
						handleClose={handleButtonOnClick}
					/>
				</BusyIndicator>
			</RightSideSlider>
		</div>
	)
}
Viewframework.propTypes = {
	viewFrameworkDetails: PropTypes.object,
	renderIcon: PropTypes.func,
	renderName: PropTypes.func,
	renderChannelConfiguration: PropTypes.func,
	renderMetadata: PropTypes.func,
	renderFields: PropTypes.func,
	renderPipelineConfiguration: PropTypes.func,
	renderVersionModules: PropTypes.func,
	renderVersions: PropTypes.func,
	renderViewFramework: PropTypes.func,
}
