import slice from './viewframework.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllViewframework = (state) =>
	selectSlice(state).allViewframework

export const selectViewframeworkFilter = (state) => selectSlice(state).filter
