import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import BusyIndicator from '../../widgets/busyIndicator'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import GetImagePath from '../../utilities/utilities'
import { modeType, permission } from '../../utilities/constants'
import Can from '../../widgets/auth/Can'
import RightSideSlider from '../../widgets/rightSideSlider'
import EditContinuousDelivery from '../../features/editContinuousDelivery'
import './viewContinuousDelivery.scss'

export default function ViewContinuousDelivery(props) {
	const { continuousDeliveryDetails, viewContinuousDeliveryJsonData } = props
	const [openEditContinuousDelivery, setEditContinuousDelivery] = useState(
		false
	)

	/**
	 * Render API Configuration Details.
	 * @param {object} apiConfigurationData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderApiConfiguration = (apiConfigurationData, index, jsonItem) => {
		return (
			<>
				{jsonItem.apiConfigChildren &&
					jsonItem.apiConfigChildren.map((item, i) =>
						renderLabel(item, i, apiConfigurationData)
					)}
			</>
		)
	}

	/**
	 * Render endpoints details.
	 * @param {object} apiConfigData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderEndPoints = (apiConfigData, index, jsonItem) => {
		const endPointValue = Object.entries(apiConfigData.endpoints)

		return (
			<table>
				<thead>
					<tr>
						{endPointValue &&
							endPointValue.map(([key, value], i) => (
								<tr key={i}>
									<td className={jsonItem.keyClass}>{value.name}</td>
									<td className={jsonItem.valueClass}>{value.endpoint}</td>
								</tr>
							))}
					</tr>
				</thead>
			</table>
		)
	}
	/**
	 * Render renderMetadata details.
	 * @param {object} apiConfigData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderMetaData = (apiConfig, index, jsonItem) => {
		const metaKeyValue = Object.entries(apiConfig.metadata)
		return (
			<table>
				<thead>
					<tr>
						{metaKeyValue &&
							metaKeyValue.map(([key, value], i) => (
								<tr key={i}>
									<td className={jsonItem.keyClass}>{key}</td>
									<td className={jsonItem.valueClass}>{value}</td>
								</tr>
							))}
					</tr>
				</thead>
			</table>
		)
	}

	/**
	 * Render channelConfiguration details.
	 * @param {object} channelConfigData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderChannelConfiguration = (channelConfig, index, jsonItem) => {
		return (
			<>
				{jsonItem.channelConfigChildren &&
					jsonItem.channelConfigChildren.map((item, i) =>
						renderLabel(item, i, channelConfig)
					)}
			</>
		)
	}
	/**
	 * For setting edit icon
	 */
	const handleEditIcon = () => {
		// props.onCancel(false)
		setEditContinuousDelivery(true)
	}
	/**
	 * Render channelConfiguration details.
	 * @param {object} channelConfigData
	 * @param {number} index
	 * @param {object} jsonItem
	 */

	const renderCDData = (metaData, index, jsonItem) => {
		const metaKeyValue = Object.entries(metaData)
		return (
			<table>
				<thead>
					<tr>
						{metaKeyValue &&
							metaKeyValue.map(([key, value], i) => (
								<tr key={i} className={jsonItem.metaDataClass}>
									<td className={jsonItem.keyClass}>{key}</td>
									<td className={jsonItem.valueClass}>{value}</td>
								</tr>
							))}
					</tr>
				</thead>
			</table>
		)
	}
	/**
	 * Render Icon's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderIcon = (item, index) => {
		return (
			<Can
				action={permission.UPDATE_CONTINUOUS_DELIVERY_CONFIGURATION}
				yes={() => (
					<div key={index} className={item.className} onClick={handleEditIcon}>
						<span>
							<span className={item.iconClass}>{GetImagePath(item.icon)}</span>
							<span className={item.labelClass}>{item.attributeName}</span>
						</span>
					</div>
				)}
				no={() => ''}
			></Can>
		)
	}

	/**
	 * Render label's.
	 * @param {object} item - Json data.
	 * @param {number} index
	 * @param {object} responseData
	 */
	const renderLabel = (item, index, responseData) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{item.label}
				</Typography>
				<Typography className={item.content_class}>
					{responseData[item.attributeName]}
				</Typography>
			</>
		)
	}

	/**
	 * Render name's of the heading's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderName = (item, index) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{continuousDeliveryDetails[item.attributeName]
						? continuousDeliveryDetails[item.attributeName]
						: item.attributeName}
				</Typography>
			</>
		)
	}

	/**
	 * Render field details.
	 * @param {object} viewContinuousDeliveryFields
	 */
	const renderFields = (viewContinuousDeliveryFields) => {
		return viewContinuousDeliveryFields.map((item, i) => (
			<>
				{item.icon ? renderIcon(item, i) : renderName(item, i)}
				{item.apiConfig
					? renderApiConfiguration(
							continuousDeliveryDetails[item.apiConfig],
							i,
							item
					  )
					: null}
				{item.children
					? item.children.map((details, index) =>
							renderLabel(details, index, continuousDeliveryDetails)
					  )
					: null}
				{item.endPoints
					? renderEndPoints(
							continuousDeliveryDetails[item.endApiConfig],
							i,
							item
					  )
					: null}
				{item.apiMetadata
					? renderMetaData(
							continuousDeliveryDetails[item.metaDataApiConfig],
							i,
							item
					  )
					: null}
				{item.channelConfig
					? renderChannelConfiguration(
							continuousDeliveryDetails[item.channelConfig],
							i,
							item
					  )
					: null}
				{item.metadata
					? renderCDData(continuousDeliveryDetails[item.metadata], i, item)
					: null}
			</>
		))
	}

	/**
	 * Calling renderFields.
	 * @param {object} viewContinuousDeliveryFields
	 */
	const renderViewContinuousDelivery = (viewContinuousDeliveryFields) => {
		return viewContinuousDeliveryFields
			? renderFields(viewContinuousDeliveryFields)
			: null
	}

	/**
	 * to handle to cancel button while closing
	 */
	const handleButtonOnClick = () => {
		props.onCancel(false)
	}

	return openEditContinuousDelivery ? (
		<EditContinuousDelivery
			continuousDeliveryDetails={continuousDeliveryDetails}
			openDrawer={handleButtonOnClick}
		/>
	) : (
		<div className='view-continuous-delivery'>
			<RightSideSlider onCancel={props.onCancel} drawerName='drawer'>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => {
						handleButtonOnClick()
					}}
				/>
				<BusyIndicator>
					<div className='right-side-drawer'>
						{continuousDeliveryDetails
							? renderViewContinuousDelivery(
									viewContinuousDeliveryJsonData.fields
							  )
							: null}
					</div>
					<SliderFooter
						mode={modeType.READ}
						handleClose={handleButtonOnClick}
					/>
				</BusyIndicator>
			</RightSideSlider>
		</div>
	)
}
ViewContinuousDelivery.propTypes = {
	continuousDeliveryDetails: PropTypes.object,
	renderIcon: PropTypes.func,
	renderName: PropTypes.func,
	renderApiConfiguration: PropTypes.func,
	renderEndPoints: PropTypes.func,
	renderFields: PropTypes.func,
	renderPipelineConfiguration: PropTypes.func,
	renderViewContinuousDelivery: PropTypes.func,
}
