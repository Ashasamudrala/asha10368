import ViewContinuousDelivery from './ViewContinuousDelivery'
import * as selectors from './viewContinuousDelivery.selectors'
import * as asyncActions from './viewContinuousDelivery.asyncActions'
import slice from './viewContinuousDelivery.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllViewContinuousDelivery } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectAllViewContinuousDelivery,
	selectViewContinuousDeliveryFilter,
} = selectors

// we export the component most likely to be desired by default
export default ViewContinuousDelivery
