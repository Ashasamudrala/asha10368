import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllViewContinuousDelivery = createAsyncThunk(
	'viewContinuousDelivery/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: ' ',
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
