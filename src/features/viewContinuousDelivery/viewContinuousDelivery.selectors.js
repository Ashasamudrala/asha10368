import slice from './viewContinuousDelivery.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllViewContinuousDelivery = (state) =>
	selectSlice(state).allViewContinuousDelivery

export const selectViewContinuousDeliveryFilter = (state) =>
	selectSlice(state).filter
