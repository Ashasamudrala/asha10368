import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './viewContinuousDelivery.asyncActions'

const initialState = {
	allViewContinuousDelivery: [],
	filter: '',
}

const slice = createSlice({
	name: 'viewContinuousDelivery',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllViewContinuousDelivery.fulfilled]: (
			state,
			action
		) => {
			if (action.payload) {
				state.allViewContinuousDelivery = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
