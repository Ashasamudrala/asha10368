import _ from 'lodash'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import RepositoryInput from '../../config/editRepository/editRepository.json'
import AbstractRepository from '../addRepository/AbstractRepository'
import { updateRepositoryById } from './editRepository.asyncActions'
import { getRepositoryStatus } from './editRepository.selectors'
import { fetchAllRepositories } from '../repositories/repositories.asyncActions'
import { actions } from './editRepository.slice'
import './editRepository.scss'
const { updateStatus } = actions

/**
 *This component is used for displaying editRepository details
 * @param {*} props
 */

export default function EditRepository(props) {
	const {
		name,
		description,
		channelConfiguration,
		apiConfiguration,
		metadata,
	} = JSON.parse(JSON.stringify(props.repositoryDetails))
	// there are state values for showing dynamic and Repository detials for storing the fields values
	const dispatch = useDispatch()
	const status = useSelector(getRepositoryStatus)

	// Clone the apiConfiguration and transform metadata property.
	if (apiConfiguration.metadata) {
		apiConfiguration.metadata = _.map(
			Object.keys(apiConfiguration.metadata),
			(key) => {
				return { key: key, value: apiConfiguration.metadata[key] }
			}
		)
	}

	const handleSave = (RepositoryFields) => {
		dispatch(
			updateRepositoryById({
				...RepositoryFields,
				editRepositoryId: props.repositoryDetails.id,
			})
		)
	}
	useEffect(() => {
		return () => {
			dispatch(updateStatus())
		}
	}, [dispatch])
	// eslint-disable-line react-hooks/exhaustive-deps
	useEffect(() => {
		if (status !== '') {
			status && dispatch(fetchAllRepositories())
			props.openDrawer(false)
		}
	}, [status])

	return (
		<AbstractRepository
			openDrawer={props.openDrawer}
			RepositoryInput={RepositoryInput}
			RepositoryOutputData={{
				name: name,
				description: description,
				channelConfiguration: channelConfiguration,
				apiConfiguration: apiConfiguration,
				metadata: _.map(Object.keys(metadata), (key) => {
					return { key: key, value: metadata[key] }
				}),
			}}
			onSave={handleSave}
			className='add-repository'
		/>
	)
}
