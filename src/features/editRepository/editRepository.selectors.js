import slice from './editRepository.slice'

export const selectSlice = (state) => state[slice.name]

export const getRepositoryDetails = (state) =>
	selectSlice(state).RepositoryDetails

export const getRepositoryStatus = (state) => selectSlice(state).status
