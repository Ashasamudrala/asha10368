import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { UPDATE_REPOSITORIES_BY_ID } from '../../utilities/apiEndpoints'
import { EDIT_REPOSITORY_SUCCESS_MESSAGE } from '../../utilities/constants'

export const updateRepositoryById = createAsyncThunk(
	'editRepository/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			name,
			description,
			apiConfiguration,
			metadata,
			channelConfiguration: { channelName },
			editRepositoryId,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${UPDATE_REPOSITORIES_BY_ID}/${editRepositoryId}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					name,
					metadata,
					description,
					apiConfiguration,
					channelConfiguration: { channelName },
				}),
			},
			successMessage: `${name} ${EDIT_REPOSITORY_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)
