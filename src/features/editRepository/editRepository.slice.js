import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './editRepository.asyncActions'

const initialState = {
	status: '',
	RepositoryDetails: [],
}

const slice = createSlice({
	name: 'editRepository',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.status = action.payload
		},
		updateRepositoryDetails(state) {
			state.RepositoryDetails = []
		},
		updateStatus(state) {
			state.status = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.updateRepositoryById.fulfilled]: (state, action) => {
			if (action && action.payload) {
				state.RepositoryDetails = action.payload
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
