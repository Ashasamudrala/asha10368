import EditRepository from './EditRepository'
import slice from './editRepository.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

// we export the component most likely to be desired by default
export default EditRepository
