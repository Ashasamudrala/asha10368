import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AbstractFramework from './AbstractFrameworkForm'
import { getFrameworkStatus } from './addFramework.selectors'
import { saveAddFramework } from './addFramework.asyncActions'
import { fetchAllPlatform } from '../platform/platform.asyncActions'
import frameworkInput from '../../config/framework/addFramework.json'
import frameworkOutput from '../../config/framework/addFrameworkOutput.json'
import { actions } from './addFramework.slice'
const { removeStatus } = actions

export default function AddFramework(props) {
	const { supportedVersions, openDrawer, platformId } = props
	const dispatch = useDispatch()
	const status = useSelector(getFrameworkStatus)
	// when ever addplatform is posted successfully and then dispatch to fetchallpltformsapi
	useEffect(() => {
		if (status !== undefined) {
			openDrawer(false)
			status && dispatch(fetchAllPlatform())
		}
	}, [status]) // eslint-disable-line react-hooks/exhaustive-deps
	const handleSave = (frameworkFields) => {
		dispatch(
			saveAddFramework({
				...frameworkFields,
				platformId: platformId,
			})
		).then(() => dispatch(fetchAllPlatform()))
	}

	useEffect(() => {
		return () => {
			dispatch(removeStatus())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	return (
		<AbstractFramework
			openDrawer={openDrawer}
			frameworkInput={frameworkInput}
			frameworkOutput={JSON.parse(JSON.stringify(frameworkOutput))}
			supportedVersions={supportedVersions}
			onSave={handleSave}
		/>
	)
}
