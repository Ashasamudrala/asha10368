import React from 'react'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import Input from '../../../widgets/input/Input'
import TextArea from '../../../widgets/textarea/Textarea'
import IsyButton from '../../../widgets/button/button'
import {
	ADD,
	FRAMEWORK_VERSIONS,
	PLATFORM_TRANSLATIONS,
} from '../../../utilities/constants'
import addVersionsData from '../../../config/framework/addVersion.json'
import metaDataInput from '../../../config/framework/frameworkMetadata.json'
import metaDataOutput from '../../../config/metadata/metadataOutput.json'
import { Multiselect } from 'multiselect-react-dropdown'
import { useTranslation } from 'react-i18next'
import MetaData from '../../../widgets/metadata/Metadata'
import _, { isNil } from 'lodash'
import Grid from '@material-ui/core/Grid'
import ExpandMore from '@material-ui/icons/ExpandMore'
import '../addFramework.scss'
import './addVersion.scss'

export default function AddVersion(props) {
	const {
		handleVersiondata,
		framework,
		errors,
		supportedVersions,
		handleSupportedVersion,
		removeSupportedVersion,
	} = props
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)

	const renderDeleteIcon = (idx, buttonClassName, midx) => {
		const showDeleteIcon =
			idx === 0 && midx === undefined ? (
				''
			) : (
				<div className='version-delete'>
					<img
						src='/images/Delete.svg'
						className={buttonClassName}
						id={idx}
						onClick={() => {
							deleteVersions(idx, midx)
						}}
					/>
				</div>
			)

		return showDeleteIcon
	}

	// renders version form
	const renderVersionForm = (versionObj, idx) => {
		return (
			<>
				<Input
					{...versionObj}
					name={versionObj.name.replace('idx', idx)}
					dataId={idx}
					onChange={handleVersiondata}
					content={_.get(framework, versionObj.name.replace('idx', idx))}
					className={`${
						errors && _.get(errors, versionObj.name.replace('idx', idx))
							? versionObj.inputErrorClass
							: ''
					}`}
				/>
				<span className={versionObj.errorClass}>
					{t(errors && _.get(errors, versionObj.name.replace('idx', idx)))}
				</span>
			</>
		)
	}

	const onSelectMulitipleVersions = (selectedList, selectedItem, name, idx) => {
		handleSupportedVersion(selectedItem, name.replace('idx', idx))
	}
	const onRemove = (selectedList, removeItem, name, idx) => {
		removeSupportedVersion(selectedList, name.replace('idx', idx))
	}
	const renderMultiSelect = (versionObj, idx) => {
		return (
			<div className='multi-select'>
				<label className='form-label'>
					{versionObj.title}
					<span className='form-label-required'> *</span>
				</label>
				<Multiselect
					className='error'
					options={supportedVersions} // Options to display in the dropdown
					onSelect={(selectedList, selectedItem) =>
						onSelectMulitipleVersions(
							selectedList,
							selectedItem,
							versionObj.name,
							idx
						)
					} // Function will trigger on select event
					onRemove={(selectedList, removedItem) =>
						onRemove(selectedList, removedItem, versionObj.name, idx)
					} // Function will trigger on remove event
					placeholder={versionObj.placeholder}
					hidePlaceholder={true}
					selectedValues={_.get(framework, versionObj.name.replace('idx', idx))}
					isObject={false}
					id={
						errors && _.get(errors, versionObj.name.replace('idx', idx))
							? 'error'
							: ''
					}
				/>
				<ExpandMore className='expand-more' />
				<span className={versionObj.errorClass}>
					{t(errors && _.get(errors, versionObj.name.replace('idx', idx)))}
				</span>
			</div>
		)
	}

	// renders version form
	const renderForm = (moduleObj, versionId, moduleId) => {
		return (
			<>
				<Input
					{...moduleObj}
					name={moduleObj.name
						.replace('idx', versionId)
						.replace('moduleId', moduleId)}
					dataId={moduleId}
					onChange={handleVersiondata}
					content={_.get(
						framework,
						moduleObj.name
							.replace('idx', versionId)
							.replace('moduleId', moduleId)
					)}
					className={`${
						errors &&
						_.get(
							errors,
							moduleObj.name
								.replace('idx', versionId)
								.replace('moduleId', moduleId)
						)
							? moduleObj.inputErrorClass
							: ''
					}`}
				/>
				<span className={moduleObj.errorClass}>
					{errors &&
						_.get(
							errors,
							moduleObj.name
								.replace('idx', versionId)
								.replace('moduleId', moduleId)
						)}
				</span>
			</>
		)
	}
	// this is used for showing the textarea
	const renderTextArea = (moduleObj, versionId, moduleId) => {
		return (
			<>
				<TextArea
					{...moduleObj}
					name={moduleObj.name
						.replace('idx', versionId)
						.replace('moduleId', moduleId)}
					dataId={moduleId}
					onChange={handleVersiondata}
					content={_.get(
						framework,
						moduleObj.name
							.replace('idx', versionId)
							.replace('moduleId', moduleId)
					)}
				/>
				<span className={moduleObj.errorClass}>
					{errors &&
						_.get(
							errors,
							moduleObj.name
								.replace('idx', versionId)
								.replace('moduleId', moduleId)
						)}
				</span>
			</>
		)
	}
	// render version metadata header
	const renderVersionMetadata = (headerName, metadata, idx) => {
		return (
			<div className='version-metadata-box'>
				{renderMetadataWidget(metadata, idx, headerName)}
			</div>
		)
	}

	const renderAddButtons = (
		headerName,
		buttonName,
		callBackFunction,
		className
	) => {
		return (
			<div className='add-btn-box'>
				<div className='button-header'>
					<span>{headerName}</span>
				</div>
				{metadataButton(buttonName, callBackFunction, className)}
			</div>
		)
	}
	const addVersion = (versionFields, version, idx) => {
		return (
			<div className='add-version'>
				<div className='version-details'>
					<div className='version-header'>{t(FRAMEWORK_VERSIONS)}</div>
					{renderDeleteIcon(idx, 'delete-version-icon')}
				</div>
				<div className='add-version-subdata'>
					<Grid container spacing={3}>
						{versionFields.versions.map((versionField, i) => {
							return (
								<Grid item sm={5} xs={11} key={i}>
									{versionField.inputType === 'text' &&
										renderVersionForm(versionField, idx)}
									{versionField.inputType === 'multiselect' &&
										renderMultiSelect(versionField, idx)}
								</Grid>
							)
						})}
					</Grid>
				</div>
				{renderVersionMetadata('Version metadata', version.metadata, idx)}
				{renderAddButtons(
					'Modules',
					'Add Modules',
					() => addModules(idx),
					'version-metadata-btn'
				)}
				{version.modules &&
					version.modules.length > 0 &&
					addModule(versionFields.modules, version.modules, idx)}
			</div>
		)
	}
	const addModule = (moduleFields, modules, versionId) => {
		return (
			<div className='add-module'>
				{modules.map((module, midx) => {
					return (
						<div className='add-module-container' key={midx}>
							<div className='add-module-subdata'>
								<Grid container spacing={3}>
									{moduleFields.map((moduleField, id) => {
										return (
											<Grid key={id} item sm={6} xs={12}>
												<div className='module-field'>
													{moduleField.inputType === 'text' &&
														renderForm(moduleField, versionId, midx)}
													{moduleField.inputType === 'textarea' &&
														renderTextArea(moduleField, versionId, midx)}
												</div>
											</Grid>
										)
									})}
								</Grid>
								<div className='delete-container'>
									{renderDeleteIcon(versionId, 'delete-module-btn', midx)}
								</div>
							</div>
							<div>
								{renderModuleMetadataWidget(module.metadata, versionId, midx)}
							</div>
						</div>
					)
				})}
			</div>
		)
	}

	// this is used for showing metadata
	const renderMetadataWidget = (metadata, idx, headerName) => {
		return (
			<MetaData
				name={`versions.${idx}.metadata`}
				metaDataInput={metaDataInput}
				metaDataOutput={metaDataOutput}
				metadata={metadata}
				header={headerName}
				headerClassName={'header-classname'}
				handleMetadata={handleVersiondata}
			/>
		)
	}

	const renderModuleMetadataWidget = (metadata, versionId, moduleId) => {
		return (
			<div className='module-metadata'>
				<MetaData
					name={`versions.${versionId}.modules.${moduleId}.metadata`}
					metaDataInput={metaDataInput}
					metaDataOutput={metaDataOutput}
					metadata={metadata}
					handleMetadata={handleVersiondata}
				/>
			</div>
		)
	}
	const addVersions = () => {
		handleVersiondata(
			{
				...addVersionsData.versionOutput,
			},
			'versions',
			t(ADD)
		)
	}
	const addModules = (idx) => {
		handleVersiondata(
			{
				...addVersionsData.moduleOutput,
			},
			`versions.${idx}.modules`,
			t(ADD)
		)
	}
	const deleteVersions = (id, midx) => {
		if (!isNil(midx)) {
			handleVersiondata(midx, `versions.${id}.modules`, 'delete')
		} else {
			handleVersiondata(id, 'versions', 'delete')
		}
	}

	const metadataButton = (buttonName, callBackFunction, className) => {
		return (
			<div className={className}>
				<IsyButton
					buttonClass='add-metadata-btn'
					handleButtonOnClick={callBackFunction}
				>
					<AddOutlinedIcon />
					{buttonName}
				</IsyButton>
			</div>
		)
	}
	const renderVersionWidget = () => {
		return framework.versions.map((versionObj, idx) => {
			return <div key={idx}>{addVersion(addVersionsData, versionObj, idx)}</div>
		})
	}
	return (
		<div className='versions-data'>
			{framework &&
				framework.versions &&
				framework.versions.length > 0 &&
				renderVersionWidget()}
			{metadataButton('Add Version', addVersions, 'add-version-btn')}
		</div>
	)
}
