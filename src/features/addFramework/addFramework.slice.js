import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './addFramework.asyncActions'

const initialState = {
	allAddFramework: [],
	status: undefined,
}

const slice = createSlice({
	name: 'addFramework',
	initialState,
	reducers: {
		removeStatus(state) {
			state.status = undefined
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.saveAddFramework.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allAddFramework = action.payload
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
