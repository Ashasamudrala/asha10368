import AddFramework from './AddFramework'
import * as selectors from './addFramework.selectors'
import * as asyncActions from './addFramework.asyncActions'
import slice from './addFramework.slice'

export const {
	name,
	actions: { removeStatus },
	reducer,
} = slice

export const { saveAddFramework } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllAddFramework, getFrameworkStatus } = selectors

// we export the component most likely to be desired by default
export default AddFramework
