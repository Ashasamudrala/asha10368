import React, { useState, useEffect } from 'react'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import RightSlider from '../../widgets/rightSideSlider'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import metaDataInput from '../../config/framework/frameworkMetadata.json'
import metaDataOutput from '../../config/metadata/metadataOutput.json'
import {
	ADD,
	ONCHANGE,
	PLATFORM_TRANSLATIONS,
	DELETE,
	modeType,
} from '../../utilities/constants'
import {
	fieldIsEmpty,
	descriptionIsValid,
	isVersionNameValid,
	isModuleVersionNameValid,
	moduleNameIsValid,
} from '../../features/addPlatform/validations'
import versionInput from '../../config/framework/addVersion.json'
import ErrorMessages from '../../config/framework/frameworkValidations.json'
import { useTranslation } from 'react-i18next'
import './addFramework.scss'
import _ from 'lodash'

export default function AbstractFrameworkForm(props) {
	const { frameworkInput, frameworkOutput, supportedVersions } = props
	const [frameworkFields, setFrameworkFields] = useState({
		...frameworkOutput,
	})
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const [errors, setErrovalues] = useState({})

	useEffect(() => {
		setFrameworkFields({ ...frameworkOutput })
	}, [props]) // eslint-disable-line react-hooks/exhaustive-deps

	const handleSupportedVersion = (value, fieldName) => {
		let processedfieldName = []
		processedfieldName = frameworkFields
		processedfieldName = _.set(processedfieldName, fieldName, [
			..._.get(processedfieldName, fieldName),
			value,
		])
		setFrameworkFields({ ...processedfieldName })
	}

	const removeSupportedVersion = (value, fieldName) => {
		let processedfieldName = []
		processedfieldName = frameworkFields
		processedfieldName = _.set(processedfieldName, fieldName, value)
		setFrameworkFields({ ...processedfieldName })
	}
	// this is used for setting the json fields
	const handleFrameworkFields = (event, fieldName, action) => {
		let processedfieldName = []
		let errorFieldNames = []
		/**
		 * if fieldname is defined then if condition excetues this is for metadata and version
		 * and then else executes when ever fieldname is undefined
		 */
		if (fieldName) {
			/**
			 * based on action swtch case executes
			 */
			switch (action) {
				case t(ADD):
					if (fieldName.search('metadata') !== -1) {
						/**
						 * It is used for adding new metadata into array
						 * and if fieldname contains metadata and adds data at the starting
						 * of array
						 */
						processedfieldName = frameworkFields
						processedfieldName = _.set(processedfieldName, fieldName, [
							event,
							..._.get(processedfieldName, fieldName),
						])
					} else {
						/**
						 * It is used for adding modules,versions into array
						 */
						processedfieldName = frameworkFields
						processedfieldName = _.set(processedfieldName, fieldName, [
							..._.get(processedfieldName, fieldName),
							event,
						])
					}
					break
				case t(ONCHANGE):
					/**
					 * It is used when onchange of metadata , version ,module is called and push the change value
					 * based on id into array if type is (metadata)
					 */
					processedfieldName = frameworkFields
					_.get(processedfieldName, fieldName)[event.target.dataset.id][
						event.target.name
					] = event.target.value
					break
				case t(DELETE):
					/**
					 * It is used when delete of metadata , version ,module  is called and removes the item
					 * based on id from array
					 */
					processedfieldName = frameworkFields
					errorFieldNames = errors
					processedfieldName = _.set(
						processedfieldName,
						fieldName,
						_.get(processedfieldName, fieldName).filter(
							(version, id) => event !== id
						)
					)
					/**
					 * It is used for removig the errors when ever deleting module,version
					 */
					if (
						_.get(errorFieldNames, fieldName) &&
						!_.isEmpty(errorFieldNames)
					) {
						errorFieldNames = _.set(
							errorFieldNames,
							fieldName,
							_.get(errorFieldNames, fieldName).filter(
								(version, id) => event !== id
							),
							fieldName,
							_.get(errorFieldNames, fieldName).filter(
								(version, id) => event !== id
							)
						)
						setErrovalues({ ...errorFieldNames })
					}
					break
				default:
			}
			setFrameworkFields({ ...processedfieldName })
		} else {
			processedfieldName = frameworkFields
			processedfieldName = _.set(
				processedfieldName,
				event.target.name,
				event.target.value
			)
			setFrameworkFields({ ...processedfieldName })
		}
	}

	const handleSave = () => {
		if (handleValidation()) {
			convertMetdataToKeyValue()
		}
	}

	const convertMetdataToKeyValue = () => {
		// convertMetadataToKeyValue
		const clonedFormValues = JSON.parse(JSON.stringify(frameworkFields))
		const { metadata, versions } = clonedFormValues
		let processedMetadata = {}
		if (metadata && metadata.length >= 0) {
			processedMetadata = _.mapValues(_.keyBy(metadata, 'key'), 'value')
			processedMetadata = _.omit(processedMetadata, [''])
		}
		if (versions && versions.length >= 0) {
			versions.forEach((version, index) => {
				// Retrofit the version metadata.
				const versionMetadata = version.metadata
				if (versionMetadata && versionMetadata.length >= 0) {
					version.metadata = _.mapValues(
						_.keyBy(versionMetadata, 'key'),
						'value'
					)
					version.metadata = _.omit(version.metadata, [''])
				}

				// Module metadata
				const { modules } = version
				if (modules && modules.length >= 0) {
					modules.forEach((module, index) => {
						const moduleMetadata = module.metadata
						if (moduleMetadata && moduleMetadata.length >= 0) {
							module.metadata = _.mapValues(
								_.keyBy(moduleMetadata, 'key'),
								'value'
							)
							module.metadata = _.omit(module.metadata, [''])
						}
					})
				}
			})
		}
		props.onSave({ ...clonedFormValues, metadata: { ...processedMetadata } })
	}
	// ridht side popup data
	const renderFormBuilderData = (
		className,
		frameworkInput,
		frameworkOutput,
		metaDataInput,
		metaDataOutput,
		handleFrameworkFields,
		handleSupportedVersion,
		removeSupportedVersion
	) => {
		return (
			<RightSlider onCancel={props.openDrawer} drawerName='drawer'>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => {
						props.openDrawer()
					}}
				/>
				<div className='framework'>
					<div className={className}>
						<FormBuilder
							formBuilderInput={frameworkInput}
							onChange={handleFrameworkFields}
							formBuilderOutput={frameworkOutput}
							metaDataInput={metaDataInput}
							metaDataOutput={metaDataOutput}
							handleSupportedVersion={handleSupportedVersion}
							errors={errors}
							supportedVersions={supportedVersions}
							removeSupportedVersion={removeSupportedVersion}
						/>
					</div>
					<SliderFooter
						mode={modeType.WRITE}
						handleClose={props.openDrawer}
						handleSave={handleSave}
					/>
				</div>
			</RightSlider>
		)
	}

	// form validations
	const handleValidation = () => {
		const fields = { ...frameworkFields }
		const errors = {}
		// const inputFields = frameworkInput.section[0].inputFields
		frameworkInput.section.forEach(
			(formFields) =>
				formFields.inputFields &&
				formFields.inputFields.forEach((inputField) => {
					if (inputField.required) {
						if (fieldIsEmpty(_.get(fields, inputField.name))) {
							_.set(errors, inputField.name, ErrorMessages[inputField.name])
						}
					}
				})
		)
		if (descriptionIsValid(fields.description)) {
			errors.description = ErrorMessages.invalidDescription
		}
		if (fields.name !== '' && moduleNameIsValid(fields.name)) {
			errors.name = ErrorMessages.invalidName
		}
		return versionsValidations(errors)
	}
	// version validations
	const versionsValidations = (errors) => {
		frameworkFields.versions.forEach((versionFields, idx) => {
			Object.keys(versionFields).forEach((versionField) => {
				if (fieldIsEmpty(versionFields[versionField])) {
					versionInput.versions.forEach((versionFieldInfo) => {
						if (
							versionFieldInfo.errorName === versionField &&
							versionFieldInfo.required
						) {
							_.set(
								errors,
								versionFieldInfo.name.replace('idx', idx),
								ErrorMessages[versionField]
							)
						}
					})
				} else if (versionField === 'versionName') {
					if (isVersionNameValid(versionFields[versionField])) {
						_.set(
							errors,
							`versions.${idx}.${versionField}`,
							ErrorMessages.invalidVersion
						)
					}
				}
			})
		})
		return moduleValidations(errors)
	}
	// module validations
	const moduleValidations = (errors) => {
		frameworkFields.versions.forEach((version, idx) => {
			Object.values(version.modules).forEach((moduleFields, moduleId) => {
				Object.keys(moduleFields).forEach((moduleField) => {
					if (fieldIsEmpty(moduleFields[moduleField])) {
						versionInput.modules.forEach((moduleFieldInfo) => {
							if (
								moduleFieldInfo.errorName === moduleField &&
								moduleFieldInfo.required
							) {
								_.set(
									errors,
									moduleFieldInfo.name
										.replace('idx', idx)
										.replace('moduleId', moduleId),
									ErrorMessages[moduleField]
								)
							}
						})
					} else if (moduleField === 'name') {
						if (moduleNameIsValid(moduleFields[moduleField])) {
							_.set(
								errors,
								`versions.${idx}.modules.${moduleId}.${moduleField}`,
								ErrorMessages.invalidName
							)
						}
					} else if (moduleField === 'version') {
						if (isModuleVersionNameValid(moduleFields[moduleField])) {
							_.set(
								errors,
								`versions.${idx}.modules.${moduleId}.${moduleField}`,
								ErrorMessages.invalidModuleVersionName
							)
						}
					}
				})
			})
		})
		setErrovalues({ ...errors })
		return _.isEmpty(errors)
	}

	const renderRightSideSlider = () => {
		return renderFormBuilderData(
			'add-framework',
			frameworkInput,
			frameworkFields,
			metaDataInput,
			metaDataOutput,
			handleFrameworkFields,
			handleSupportedVersion,
			removeSupportedVersion
		)
	}
	return renderRightSideSlider()
}
