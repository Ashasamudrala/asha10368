import slice from './addFramework.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllAddFramework = (state) =>
	selectSlice(state).allAddFramework

export const getFrameworkStatus = (state) => selectSlice(state).status
