import { createAsyncThunk } from '@reduxjs/toolkit'
import { SAVE_FRAMEWORK_DETAILS } from '../../utilities/apiEndpoints'
import doAsync from '../../infrastructure/doAsync'

export const saveAddFramework = createAsyncThunk(
	'addFramework/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			platformId,
			metadata,
			name,
			description,
			channelConfiguration,
			versions,
		} = {},
		thunkArgs
	) => {
		const JsonObject = JSON.stringify({
			metadata,
			name,
			description,
			channelConfiguration,
			versions,
			platformId,
		})
		await doAsync({
			url: `${SAVE_FRAMEWORK_DETAILS}/${platformId}/frameworks`,
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JsonObject,
			},
			successMessage: 'AddFramework is saved successfully',
			...thunkArgs,
		})
	}
)
