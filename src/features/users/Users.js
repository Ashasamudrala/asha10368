import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchAllUsers } from './users.asyncActions'
import { selectAllUsers } from './users.selectors'
import BusyIndicator from '../../widgets/busyIndicator'
import { selectAllSettings } from '../settings'

export default function Users() {
	const users = useSelector(selectAllUsers)

	const dispatch = useDispatch()

	const settings = useSelector(selectAllSettings)

	useEffect(() => {
		dispatch(
			fetchAllUsers({
				useCaching: settings.useCaching,
				noBusySpinner: settings.noBusySpinner,
			})
		)
	}, [dispatch, settings.useCaching, settings.noBusySpinner])

	return (
		<div>
			<BusyIndicator>
				<ul>
					{users && users.map((user) => <li key={user.id}>{user.login}</li>)}
				</ul>
			</BusyIndicator>
		</div>
	)
}
