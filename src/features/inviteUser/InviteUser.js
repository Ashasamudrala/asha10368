import React, { useState, useEffect } from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import IsyButton from '../../widgets/button'
import { fetchAllPendingInvites } from '../../features/pendingInvites'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import { DialogActions } from '@material-ui/core'
import { emailIsValid, fieldIsEmpty } from '../addPlatform/validations'
import ErrorMesssages from '../../config/inviteUsers/inviteUserValidation.json'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import { useDispatch } from 'react-redux'
import {
	fetchAllInviteUserRoles,
	saveInviteUsers,
} from './inviteUser.asyncActions'
import {
	INVITE_USER_TRANSFORMATIONS,
	ADD_LINK,
	SEND_LINK,
	INVITE_MEMBER,
} from '../../utilities/constants'
import { useTranslation } from 'react-i18next'
import _ from 'lodash'
import './inviteUser.scss'
import DropDown from '../../widgets/dropDown/dropDown'

export default function InviteUser(props) {
	const { isDialogOpen, inviteUsers } = props
	const { t } = useTranslation(INVITE_USER_TRANSFORMATIONS)
	const [inviteUserMember, setInviteUserMember] = useState({
		email: '',
		roleId: '0',
	})
	const [listOfMembers, setListOfMembers] = useState([])
	const [errors, setErrors] = useState({})
	const inviteMemberFields = _.clone(inviteUsers)

	const [inviteUserFields, setInviteUserFields] = useState({
		...inviteMemberFields,
	})

	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(fetchAllInviteUserRoles()).then((response) => {
			renderFormBuilder(response.payload.content)
		})
	}, [dispatch])

	useEffect(() => {
		return () => {
			setInviteUserMember({
				email: '',
				roleId: '0',
			})
			setListOfMembers([])
			setErrors({})
			setInviteUserFields('')
		}
	}, [])

	const renderFormBuilder = (roles) => {
		const inputFields = inviteUserFields.section[0].inputFields
		inputFields.map((inputField, index) => {
			if (inputField.name === 'roleId') {
				inputFields[index].options = [inputFields[index].options[0], ...roles]
				setInviteUserFields({
					...inviteUserFields,
					inputFields,
				})
			}
		})
	}

	const handleClose = () => {
		props.handleClose(false)
	}

	const handleOnChange = (e) => {
		setInviteUserMember({
			...inviteUserMember,
			[e.target.name]: e.target.value,
		})
	}
	const handleValidations = () => {
		const errors = {}
		const userFields = inviteUserFields.section[0].inputFields
		userFields.forEach((user) => {
			if (user.required) {
				if (fieldIsEmpty(inviteUserMember[user.name])) {
					errors[user.name] = ErrorMesssages[user.name]
				}
			}
		})
		listOfMembers.forEach((invitedMembers) => {
			if (invitedMembers.email === inviteUserMember.email) {
				errors.email = ErrorMesssages.email_exists
			}
		})
		if (!emailIsValid(inviteUserMember.email)) {
			errors.email = ErrorMesssages.invalid_email
		}
		if (!inviteUserMember.roleId) {
			errors.roleId = ErrorMesssages.roleId
		}

		if (listOfMembers.includes(inviteUserMember.email)) {
			errors.email = ErrorMesssages.email_exists
		}
		setErrors({ ...errors })
		return _.isEmpty(errors)
	}
	const handleAddLink = () => {
		if (handleValidations()) {
			setListOfMembers([...listOfMembers, inviteUserMember])
			setInviteUserMember({
				email: '',
				roleId: '0',
			})
		}
	}
	const handleRemove = (e, index) => {
		setListOfMembers(listOfMembers.filter((user, i) => index !== i))
	}
	const handleSendInviteUsers = () => {
		dispatch(saveInviteUsers({ members: listOfMembers })).then(() => {
			setListOfMembers([])
			setInviteUserMember({
				email: '',
				roleId: '0',
			})
			dispatch(fetchAllPendingInvites())
			props.handleClose(false)
		})
	}
	const handleAction = (actionType, e, i) => {
		if (actionType === 'select') {
			const updatedArray = [...listOfMembers]
			const selectedValue = {
				...updatedArray[i],
				[e.target.name]: e.target.value,
			}
			updatedArray[i] = selectedValue
			setListOfMembers(updatedArray)
		}
	}
	const renderListOfMembers = (invitedUser, i) => {
		return (
			invitedUser && (
				<div className='invited-user-list' key={i}>
					<span className='email-list-class'>{invitedUser.email}</span>
					<span className='dropdown-class'>
						<DropDown
							options={inviteMemberFields.section[0].inputFields[1].options}
							onChange={(e) => handleAction('select', e, i)}
							update={true}
							name='roleId'
							className='role'
							selected={invitedUser.roleId}
						/>
					</span>
					<CloseOutlinedIcon
						className='close-icon-invite-user'
						onClick={(e) => handleRemove(e, i)}
					/>
				</div>
			)
		)
	}
	return (
		<div>
			<Dialog
				open={isDialogOpen}
				onClose={handleClose}
				aria-labelledby='form-dialog-title'
				disablePortal
			>
				<div className='title-container'>
					<DialogTitle id='form-dialog-title'>{t(INVITE_MEMBER)}</DialogTitle>
					<div className='close-icon-container'>
						<CloseOutlinedIcon className='close-icon' onClick={handleClose} />
					</div>
				</div>
				<DialogContent>
					<div className='invite-user-form'>
						<FormBuilder
							formBuilderInput={inviteUserFields}
							onChange={handleOnChange}
							formBuilderOutput={inviteUserMember}
							errors={errors}
						/>
						<div className={'add-link-container'}>
							<IsyButton
								buttonClass={'add-link'}
								handleButtonOnClick={handleAddLink}
							>
								<AddOutlinedIcon />
								{t(ADD_LINK)}
							</IsyButton>
						</div>
					</div>
					<div className='invited-user-list-container'>
						{listOfMembers &&
							listOfMembers.map((invitedUser, i) =>
								renderListOfMembers(invitedUser, i)
							)}
					</div>
				</DialogContent>
				{listOfMembers.length > 0 ? (
					<DialogActions className='dialog-actions'>
						<IsyButton
							buttonClass={'send-invite-btn'}
							handleButtonOnClick={handleSendInviteUsers}
						>
							{t(SEND_LINK)}
						</IsyButton>
					</DialogActions>
				) : null}
			</Dialog>
		</div>
	)
}
