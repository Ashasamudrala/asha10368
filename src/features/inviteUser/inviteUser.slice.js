import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './inviteUser.asyncActions'

const initialState = {
	allUserRoles: [],
	savedInvites: '',
	filter: '',
}

const slice = createSlice({
	name: 'inviteUser',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllInviteUserRoles.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allUserRoles = action.payload.content
			}
		},
		[asyncActions.saveInviteUsers.fulfilled]: (state, action) => {
			if (action.payload) {
				state.savedInvites = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
