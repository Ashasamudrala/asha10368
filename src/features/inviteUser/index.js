import InviteUser from './InviteUser'
import * as selectors from './inviteUser.selectors'
import * as asyncActions from './inviteUser.asyncActions'
import slice from './inviteUser.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllInviteUserRoles } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectUserRoles,
	selectSavedInvites,
	selectInvideUserDialogFilter,
} = selectors

// we export the component most likely to be desired by default
export default InviteUser
