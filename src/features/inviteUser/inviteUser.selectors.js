import slice from './inviteUser.slice'

export const selectSlice = (state) => state[slice.name]

export const selectUserRoles = (state) => selectSlice(state).allUserRoles

export const selectSavedInvites = (state) => selectSlice(state).savedInvites

export const selectInvideUserDialogFilter = (state) => selectSlice(state).filter
