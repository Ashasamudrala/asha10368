import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { USER_ROLES, CREATE_INVITE_USERS } from '../../utilities/apiEndpoints'

export const fetchAllInviteUserRoles = createAsyncThunk(
	'inviteUser/getAll',
	async ({ useCaching, noBusySpinner, tata } = {}, thunkArgs) =>
		await doAsync({
			url: `${USER_ROLES}`,
			useCaching,
			noBusySpinner,
			errorMessage: 'Unable to load inviteUser. Please try again later.',
			...thunkArgs,
		})
)

export const saveInviteUsers = createAsyncThunk(
	'inviteUser/saveUsers',
	async ({ useCaching, noBusySpinner, metadata, members } = {}, thunkArgs) =>
		await doAsync({
			url: CREATE_INVITE_USERS,
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					members,
				}),
			},
			successMessage: `${members.length} invitations sent successfully!`,
			...thunkArgs,
		})
)
