import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllAccelerators = createAsyncThunk(
	'accelerators/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'accelerators',
			useCaching,
			noBusySpinner,
			successMessage: 'Accelerators loaded',
			errorMessage: 'Unable to load accelerators. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
