import slice from './accelerators.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllAccelerators = (state) =>
	selectSlice(state).allAccelerators

export const selectAcceleratorsFilter = (state) => selectSlice(state).filter
