import Accelerators from './Accelerators'
import * as selectors from './accelerators.selectors'
import * as asyncActions from './accelerators.asyncActions'
import slice from './accelerators.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllAccelerators } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllAccelerators, selectAcceleratorsFilter } = selectors

// we export the component most likely to be desired by default
export default Accelerators
