import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './accelerators.asyncActions'

const initialState = {
	allAccelerators: [],
	filter: '',
}

const slice = createSlice({
	name: 'accelerators',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllAccelerators.fulfilled]: (state, action) => {
			state.allAccelerators = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
