import slice from './editContinuousIntegration.slice'

export const selectSlice = (state) => state[slice.name]

export const getContinuousIntegrationDetails = (state) =>
	selectSlice(state).continuousIntegrationDetails

export const getContinuousIntegrationStatus = (state) =>
	selectSlice(state).status
