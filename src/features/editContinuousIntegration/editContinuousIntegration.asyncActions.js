import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_CONTINUOUSINTEGRATION } from '../../utilities/apiEndpoints'
import { SAVED_SUCCESS_MESSAGE } from '../../utilities/constants'

export const updateContinuousIntegrationById = createAsyncThunk(
	'editContinuousIntegration/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			name,
			description,
			apiConfiguration,
			metadata,
			pipelineConfiguration,
			channelConfiguration: { channelName },
			editContinuousIntegrationId,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${GET_ALL_CONTINUOUSINTEGRATION}/${editContinuousIntegrationId}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					name,
					metadata,
					description,
					apiConfiguration,
					pipelineConfiguration,
					channelConfiguration: { channelName },
				}),
			},
			successMessage: `${name} ${SAVED_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)
