import EditContinuousIntegration from './EditContinuousIntegration'
import * as selectors from './editContinuousIntegration.selectors'
import * as asyncActions from './editContinuousIntegration.asyncActions'
import slice from './editContinuousIntegration.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { updateContinuousIntegrationById } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	getContinuousIntegrationDetails,
	getContinuousIntegrationStatus,
} = selectors

// we export the component most likely to be desired by default
export default EditContinuousIntegration
