import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './editContinuousIntegration.asyncActions'

const initialState = {
	status: '',
	continuousIntegrationDetails: [],
}

const slice = createSlice({
	name: 'editContinuousIntegration',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.status = action.payload
		},
		updateRepositoryDetails(state) {
			state.continuousIntegrationDetails = []
		},
		updateStatus(state) {
			state.status = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.updateContinuousIntegrationById.fulfilled]: (
			state,
			action
		) => {
			if (action && action.payload) {
				state.continuousIntegrationDetails = action.payload
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
