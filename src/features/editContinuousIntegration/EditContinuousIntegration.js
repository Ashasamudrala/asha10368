import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import {
	FORMBUILDER_TRANSLATIONS,
	ADD_CONTINUOUS_INTEGRATION_HEADING,
	EDIT_CONTINUOUS_INTEGRATION_HEADING,
} from '../../utilities/constants'
import { getContinuousIntegrationStatus } from './editContinuousIntegration.selectors'
import { actions } from './editContinuousIntegration.slice'
import { updateContinuousIntegrationById } from './editContinuousIntegration.asyncActions'
import continuousIntegrationInput from '../../config/addContinuousIntegration/addContinuousIntegration.json'
import AbstractContinuousIntegration from '../addContinuousIntegration/AbstractContinuousIntegration'
import { fetchAllContinuousIntegration } from './../continuousIntegration/continuousIntegration.asyncActions'
import _ from 'lodash'

const { updateStatus } = actions

/**
 * Represents of edit CI component.
 * @param {object} props
 */
export default function EditContinuousIntegration(props) {
	const { t } = useTranslation(FORMBUILDER_TRANSLATIONS)
	const {
		name,
		description,
		pipelineConfiguration,
		channelConfiguration,
		apiConfiguration,
		metadata,
	} = JSON.parse(JSON.stringify(props.continuousIntegrationDetails))

	const processedContinuousIntegrationInput = _.cloneDeep(
		continuousIntegrationInput
	)
	const dispatch = useDispatch()
	const status = useSelector(getContinuousIntegrationStatus)

	// Clone the apiConfiguration and transform metadata property.
	if (apiConfiguration.metadata) {
		apiConfiguration.metadata = _.map(
			Object.keys(apiConfiguration.metadata),
			(key) => {
				return { key: key, value: apiConfiguration.metadata[key] }
			}
		)
		processedContinuousIntegrationInput.formHeader.formHeading = processedContinuousIntegrationInput.formHeader.formHeading.replace(
			t(ADD_CONTINUOUS_INTEGRATION_HEADING),
			t(EDIT_CONTINUOUS_INTEGRATION_HEADING)
		)
	}

	const handleSave = (continuousIntegrationFields) => {
		dispatch(
			updateContinuousIntegrationById({
				...continuousIntegrationFields,
				editContinuousIntegrationId: props.continuousIntegrationDetails.id,
			})
		)
	}
	useEffect(() => {
		return () => {
			dispatch(updateStatus())
		}
	}, [dispatch])
	// eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		if (status !== '') {
			status && dispatch(fetchAllContinuousIntegration())
			props.openDrawer(false)
		}
	}, [status])

	return (
		<AbstractContinuousIntegration
			openDrawer={props.openDrawer}
			continuousIntegrationInput={processedContinuousIntegrationInput}
			continuousIntegrationOutputData={{
				name: name,
				description: description,
				channelConfiguration: channelConfiguration,
				apiConfiguration: apiConfiguration,
				pipelineConfiguration: pipelineConfiguration,
				metadata: _.map(Object.keys(metadata), (key) => {
					return { key: key, value: metadata[key] }
				}),
			}}
			onSave={handleSave}
			className='add-ci'
		/>
	)
}
EditContinuousIntegration.propTypes = {
	handleSave: PropTypes.func,
}
