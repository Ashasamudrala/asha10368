import React, { useEffect, useState } from 'react'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
// import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import BusyIndicator from '../../widgets/busyIndicator'
import Can from '../../widgets/auth/Can'
import { selectAllViewPlatform } from './viewPlatform.selectors'
import { fetchAllViewPlatform } from './viewPlatform.asyncActions'
import { useSelector, useDispatch } from 'react-redux'
import { actions } from './viewPlatform.slice'
import { isNull } from 'lodash'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import GetImagePath from '../../utilities/utilities'
import { modeType, permission } from '../../utilities/constants'
import RightSideSlider from '../../widgets/rightSideSlider'
import EditPlatform from '../../features/editPlatform'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import './viewPlatform.scss'
export default function ViewPlatform(props) {
	const { removePlatformData } = actions
	const { platform, viewPlatform } = props
	const [openEditPlatform, setEditPlatform] = useState(false)
	const dispatch = useDispatch()
	/**
	 * to dispatch the platform data b id
	 */
	useEffect(() => {
		if (!isNull(platform)) {
			dispatch(
				fetchAllViewPlatform({
					platformId: platform.id,
				})
			)
		}
	}, [props]) // eslint-disable-line react-hooks/exhaustive-deps
	/**
	 * to remove the platform data after closing page
	 */
	useEffect(() => {
		return () => {
			dispatch(removePlatformData())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	const platformDetails = useSelector(selectAllViewPlatform)

	/**
	 * @param {object} metadata - to display data in key value pair
	 * @param {number} index - index of the array of parent element
	 * this function is to display metadata form
	 */
	const renderMetaData = (metadata, index, metadataAttribute) => {
		let metadataValues = []
		if (metadata) {
			metadataValues = Object.entries(metadata)
		}
		return (
			<>
				{metadataValues &&
					metadataValues.map(([key, value], i) => (
						<>
							<Typography
								key={i}
								className={metadataAttribute.platformMetadataClass}
							>
								<Box className={metadataAttribute.metadataKeyClass}>{key}</Box>
							</Typography>
							<Typography
								key={i}
								className={metadataAttribute.platformMetadataClass}
							>
								<Box className={metadataAttribute.metadataValueClass}>
									{value}
								</Box>
							</Typography>
						</>
					))}
			</>
		)
	}

	/**
	 * to handle to cancel button while closing
	 */
	const handleButtonOnClick = () => {
		props.onCancel(false)
	}
	const handleEditIcon = () => {
		// props.onCancel(false)
		setEditPlatform(true)
	}
	const renderIcon = (attibute, index) => {
		return (
			<Can
				action={permission.UPDATE_PLATFORM}
				yes={() => (
					<div
						key={index}
						className={attibute.className}
						onClick={handleEditIcon}
					>
						<span className={attibute.iconClass}>
							{GetImagePath(attibute.icon)}
						</span>
						<span className={attibute.labelClass}>
							{attibute.attributeName}
						</span>
					</div>
				)}
				no={() => ''}
			></Can>
		)
	}
	const renderLabel = (attibute, index) => {
		return (
			<>
				<Typography key={index} className={attibute.className}>
					{attibute.attributeName}
				</Typography>
				<Typography className={attibute.content_class}>
					{platformDetails[attibute.attributeName]}
				</Typography>
			</>
		)
	}
	const renderName = (attibute, index) => {
		return (
			<>
				<Typography key={index} className={attibute.className}>
					{platformDetails[attibute.attributeName]
						? platformDetails[attibute.attributeName]
						: attibute.attributeName}
				</Typography>
			</>
		)
	}
	const renderFields = (viewPlatformFields) => {
		return viewPlatformFields.map((attibute, i) => (
			<>
				{attibute.icon ? renderIcon(attibute, i) : renderName(attibute, i)}
				{attibute.metadata
					? renderMetaData(platformDetails[attibute.metadata], i, attibute)
					: null}
				{attibute.children
					? attibute.children.map((details, index) =>
							renderLabel(details, index)
					  )
					: null}
			</>
		))
	}
	const renderViewPlatform = (viewPlatformFields) => {
		return viewPlatformFields ? renderFields(viewPlatformFields) : null
	}

	const renderViewPlatformDetails = () => {
		return openEditPlatform ? (
			<EditPlatform platformId={platform.id} openDrawer={handleButtonOnClick} />
		) : (
			<div className='view-platform'>
				<RightSideSlider onCancel={props.onCancel} drawerName='drawer'>
					<CloseOutlinedIcon
						className='close-icon'
						onClick={() => {
							handleButtonOnClick()
						}}
					/>
					<BusyIndicator>
						<div className='right-side-drawer'>
							{platformDetails ? renderViewPlatform(viewPlatform.fields) : null}
						</div>
						<SliderFooter
							mode={modeType.READ}
							handleClose={handleButtonOnClick}
						/>
					</BusyIndicator>
				</RightSideSlider>
			</div>
		)
	}
	return renderViewPlatformDetails()
}
