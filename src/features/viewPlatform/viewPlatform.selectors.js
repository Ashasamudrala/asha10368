import slice from './viewPlatform.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllViewPlatform = (state) =>
	selectSlice(state).allViewPlatform

export const selectViewPlatformFilter = (state) => selectSlice(state).filter
