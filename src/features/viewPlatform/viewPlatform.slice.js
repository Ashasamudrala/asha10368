import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './viewPlatform.asyncActions'

const initialState = {
	allViewPlatform: [],
}

const slice = createSlice({
	name: 'viewPlatform',
	initialState,
	reducers: {
		// synchronous actions
		removePlatformData(state) {
			state.allViewPlatform = []
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllViewPlatform.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allViewPlatform = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
