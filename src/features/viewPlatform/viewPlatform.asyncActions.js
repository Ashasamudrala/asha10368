import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { VIEW_PLATFORM_BY_ID } from '../../utilities/apiEndpoints'

export const fetchAllViewPlatform = createAsyncThunk(
	'viewPlatform/byId',
	async ({ useCaching, noBusySpinner, platformId } = {}, thunkArgs) =>
		await doAsync({
			url: `${VIEW_PLATFORM_BY_ID}/${platformId}`,
			useCaching,
			noBusySpinner,
			errorMessage: 'Unable to load platform. Please try again later.',
			...thunkArgs,
		})
)
