import ViewPlatform from './ViewPlatform'
import * as selectors from './viewPlatform.selectors'
import * as asyncActions from './viewPlatform.asyncActions'
import slice from './viewPlatform.slice'

export const {
	name,
	actions: { removePlatformData },
	reducer,
} = slice

export const { fetchAllViewPlatform } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllViewPlatform, selectViewPlatformFilter } = selectors

// we export the component most likely to be desired by default
export default ViewPlatform
