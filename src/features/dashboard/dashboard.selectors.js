import slice from './dashboard.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllDashboard = (state) => selectSlice(state).allDashboard

export const selectDashboardFilter = (state) => selectSlice(state).filter
