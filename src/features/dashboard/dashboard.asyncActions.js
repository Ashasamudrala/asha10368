import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllDashboard = createAsyncThunk(
	'dashboard/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'dashboard',
			useCaching,
			noBusySpinner,
			successMessage: 'Dashboard loaded',
			errorMessage: 'Unable to load dashboard. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
