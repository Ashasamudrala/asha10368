import Dashboard from './Dashboard'
import * as selectors from './dashboard.selectors'
import * as asyncActions from './dashboard.asyncActions'
import slice from './dashboard.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllDashboard } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllDashboard, selectDashboardFilter } = selectors

// we export the component most likely to be desired by default
export default Dashboard
