import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './dashboard.asyncActions'

const initialState = {
	allDashboard: [],
	filter: '',
}

const slice = createSlice({
	name: 'dashboard',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllDashboard.fulfilled]: (state, action) => {
			state.allDashboard = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
