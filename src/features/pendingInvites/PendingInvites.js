import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import pendingGridProp from '../../config/pendingInvites/pendingInvites.json'
import {
	fetchAllPendingInvites,
	updatePendingInvitationRole,
	delteBulkPendingMembers,
} from './pendingInvites.asyncActions'
import GridTable from '../../widgets/grid/gridTable/GridTable'
import './pendingInvites.scss'
import { useTranslation } from 'react-i18next'
import { selectAllPendingInvites } from './pendingInvites.selectors'
import ConfirmDialog from '../../widgets/confirmDialog/confirmDialog'
import _ from 'lodash'
import {
	CONFIRMATION_MESSAGE_SELECT,
	TEAM_TRANSLATIONS,
	DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE,
	CONFIRMATION_MESSAGE_DELETE_USER,
	permission,
} from '../../utilities/constants'
import { actions } from './pendingInvites.slice'
import { fetchPendingSearchResults } from '../team/team.asyncActions'
import { selectGetLogInUserInfo } from '../../authAndPermissions/loginUserDetails.selectors'
import { fetchAllInviteUserRoles } from '../inviteUser/inviteUser.asyncActions'
import {
	selectAllUsersFromSearchResults,
	selectSearchStatus,
	selectSearchString,
} from '../team/team.selectors'
import { actions as teamActions } from '../team/team.slice'
import BusyIndicator from '../../widgets/busyIndicator'
import { selectUserRoles } from '../inviteUser/inviteUser.selectors'

const { updateCheckedValue, updateSelectAll } = actions
const {
	setActiveTab,
	updateSearchStatus,
	updateSearchString,
	removeSearchData,
} = teamActions

export default function PendingInvites(props) {
	const pending = useSelector(selectAllPendingInvites)
	const [pendingUserFields, setPendingUserFields] = useState(
		_.cloneDeep(pendingGridProp)
	)
	const { t } = useTranslation(TEAM_TRANSLATIONS)
	const [confirmDialog, setConfirmDialog] = useState(false)
	const [roleOnChangeValue, setRoleValue] = useState({})
	const [openConfirmDialog, setOpenConfirmDialog] = useState(false)
	const [deleteUser, setDeleteUser] = useState({})
	const roles = useSelector(selectUserRoles)

	const pendingSearch = useSelector(selectAllUsersFromSearchResults)
	const searchString = useSelector(selectSearchString)
	const pendingSearchUserStatus = useSelector(selectSearchStatus)
	const searchId = pendingSearch.map((ele) => ele.id)
	const handleData = pendingSearchUserStatus
		? searchId.length > 0
			? pendingSearch
			: []
		: pending

	const dispatch = useDispatch()

	const loginDetails = useSelector(selectGetLogInUserInfo)

	const fetchInviteMembersData = () => {
		pendingSearchUserStatus
			? dispatch(
					fetchPendingSearchResults({
						data: searchString,
					})
			  )
			: dispatch(fetchAllPendingInvites())
	}

	useEffect(() => {
		dispatch(setActiveTab(1))
		if (
			loginDetails &&
			loginDetails.permissions &&
			loginDetails.permissions.includes(permission.UPDATE_ROLE)
		) {
			dispatch(fetchAllInviteUserRoles()).then((response) => {
				if (response.payload) {
					setFormRoles(response.payload.content)
				}
			})
		}
		dispatch(fetchAllPendingInvites())
	}, [dispatch])

	const handleOnChange = (item, e) => {
		setConfirmDialog(true)
		setRoleValue({ value: e.target.value, item: item })
	}
	const handleAction = (actionType, item, e) => {
		if (actionType === 'select') {
			handleOnChange(item, e)
		} else if (actionType === 'delete') {
			handleDeleteAction(item, e)
		} else if (actionType === 'checkbox') {
			handleCheckedItems(e, item)
		}
	}
	const handleConfirmDialog = () => {
		const userIds = deleteUser.id
		const deleteSuccessMessage = deleteUser.email

		dispatch(
			delteBulkPendingMembers({
				userIds,
				deleteSuccessMessage: `${t(DELETE_MULTIPLE_USERS_SUCCESS_MESSAGE, {
					user: deleteSuccessMessage,
				})}`,
			})
		).then(() => {
			setDeleteUser({})
			setOpenConfirmDialog(false)
			fetchInviteMembersData()
		})
	}
	useEffect(() => {
		if (roles && roles.length !== 0) {
			setFormRoles(roles)
		}
	}, [roles])

	const handleDeleteAction = (item, e) => {
		setOpenConfirmDialog(true)
		setDeleteUser(item)
	}

	const handleHeaderAction = (event, item) => {
		if (item.type === 'checkbox') {
			handleSelectAllClick(event)
		}
	}

	const handleSelectAllClick = (event) => {
		if (pendingSearchUserStatus) {
		} else {
			dispatch(updateSelectAll({ value: event.target.checked }))
		}
	}

	const handleCheckedItems = (e, item) => {
		if (pendingSearchUserStatus) {
		} else {
			dispatch(updateCheckedValue({ item, value: e.target.checked }))
		}
	}
	const saveRole = () => {
		setConfirmDialog(false)
		dispatch(
			updatePendingInvitationRole({
				metadata: roleOnChangeValue.item.metadata,
				invitationId: roleOnChangeValue.item.id,
				roleId: roleOnChangeValue.value,
			})
		).then(() => {
			fetchInviteMembersData()
		})
	}

	const setFormRoles = (roles) => {
		let processedData
		const columns = pendingUserFields.columns
		columns.map((inputField, index) => {
			if (inputField.name === 'roleId') {
				processedData = roles
				columns[index].options = [...processedData]
			}
		})
		setPendingUserFields({
			...pendingUserFields,
			columns: columns,
		})
	}

	const handleCloseConfirmDialog = () => {
		setOpenConfirmDialog(false)
	}

	useEffect(() => {
		return () => {
			dispatch(updateSearchStatus({ searchStatus: false }))
			dispatch(removeSearchData())
			dispatch(
				updateSearchString({
					value: '',
				})
			)
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps
	return (
		<BusyIndicator name='pendinginvite'>
			<div className='pendingInvites'>
				<div className='pending-data'>
					<GridTable
						properties={pendingUserFields}
						data={handleData}
						handleAction={handleAction}
						handleHeaderAction={handleHeaderAction}
					/>
				</div>
				{confirmDialog && (
					<ConfirmDialog
						open={true}
						onClose={() => setConfirmDialog(false)}
						onConfirm={saveRole}
						message={t(CONFIRMATION_MESSAGE_SELECT)}
					/>
				)}

				<ConfirmDialog
					open={openConfirmDialog}
					onClose={handleCloseConfirmDialog}
					onConfirm={handleConfirmDialog}
					message={t(CONFIRMATION_MESSAGE_DELETE_USER, {
						user: `${deleteUser.member ? deleteUser.member.email : ''}`,
					})}
				/>
			</div>
		</BusyIndicator>
	)
}
