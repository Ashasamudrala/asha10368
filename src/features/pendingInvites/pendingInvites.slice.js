import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './pendingInvites.asyncActions'

const initialState = {
	allPendingInvites: [],
	filter: '',
	records: null,
	confirmDelete: false,
}

const slice = createSlice({
	name: 'pendingInvites',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
		updateCheckedValue(state, action) {
			const pendingMembers = state.allPendingInvites
			pendingMembers.forEach((item) => {
				if (item.id === action.payload.item.id) {
					item.checked = action.payload.value
				}
			})
			state.allPendingInvites = [...pendingMembers]
		},
		updateSelectAll(state, action) {
			const pendingMembers = state.allPendingInvites
			pendingMembers.forEach((item) => {
				if (action.payload.value) {
					item.checked = true
				} else {
					item.checked = false
				}
			})
			state.allPendingInvites = [...pendingMembers]
		},
		confirmDelete(state, action) {
			state.confirmDelete = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllPendingInvites.fulfilled]: (state, action) => {
			if (action.payload) {
				action.payload.content.forEach((item) => {
					item.checked = false
				})
				state.allPendingInvites = action.payload.content
				state.records = action.payload
			}
		},
		[asyncActions.updatePendingInvitationRole.fulfilled]: (state, action) => {},
	},
})

export default slice

export const { name, actions, reducer } = slice
