import PendingInvites from './PendingInvites'
import * as selectors from './pendingInvites.selectors'
import * as asyncActions from './pendingInvites.asyncActions'
import slice from './pendingInvites.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllPendingInvites } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllPendingInvites, selectPendingInvitesFilter } = selectors

// we export the component most likely to be desired by default
export default PendingInvites
