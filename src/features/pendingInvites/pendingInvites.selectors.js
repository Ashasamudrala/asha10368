import slice from './pendingInvites.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllPendingInvites = (state) =>
	selectSlice(state).allPendingInvites

export const selectPendingInvitesFilter = (state) => selectSlice(state).filter
export const selectPendingInvitesRecords = (state) => selectSlice(state).records
export const getSelectedPendingInvites = (state) =>
	selectSlice(state).allPendingInvites.filter((item) => item.checked === true)
export const getConfirmDelete = (state) => selectSlice(state).confirmDelete
