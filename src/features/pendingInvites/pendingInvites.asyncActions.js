import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import {
	GET_ACTIVE_MEMBERS,
	UPDATE_INVITE_USER,
	CREATE_INVITE_USERS,
} from '../../utilities/apiEndpoints'

export const fetchAllPendingInvites = createAsyncThunk(
	'pendingInvites/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ACTIVE_MEMBERS,
			useCaching,
			noBusySpinner,
			busyIndicatorName: 'pendinginvite',
			...thunkArgs,
		})
)
export const updatePendingInvitationRole = createAsyncThunk(
	'pendingInvites/save',
	async ({ noBusySpinner, metadata, invitationId, roleId } = {}, thunkArgs) =>
		await doAsync({
			url: `${UPDATE_INVITE_USER}/${invitationId}`,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					metadata,
					invitationId,
					roleId,
				}),
			},
			...thunkArgs,
		})
)

export const delteBulkPendingMembers = createAsyncThunk(
	'pendingMembers/delete',
	async (
		{ noBusySpinner, useCaching, userIds, deleteSuccessMessage } = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${CREATE_INVITE_USERS}/?invitationIds=${userIds}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'delete',
			...thunkArgs,
			successMessage: `${deleteSuccessMessage}`,
		})
)
