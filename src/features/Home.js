import React from 'react'

export default function Home() {
	return (
		<>
			<h2>Innominds Software Boilerplate</h2>
			<p>
				Created by{' '}
				<a href='http://www.innominds.com' target='new'>
					innominds Software Pvt. Ltd.
				</a>{' '}
				.
			</p>
		</>
	)
}
