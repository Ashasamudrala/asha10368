import Apps from './Apps'
import * as selectors from './apps.selectors'
import * as asyncActions from './apps.asyncActions'
import slice from './apps.slice'

export const {
	name,
	actions: { updateFilter, updateSidebar },
	reducer,
} = slice

export const { fetchAllApps } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllApps, selectAppsFilter, selectSidebar } = selectors

// we export the component most likely to be desired by default
export default Apps
