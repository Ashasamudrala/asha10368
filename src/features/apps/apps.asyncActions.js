import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllApps = createAsyncThunk(
	'apps/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'apps',
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
