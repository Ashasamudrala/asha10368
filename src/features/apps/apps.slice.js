import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './apps.asyncActions'

const initialState = {
	allApps: [],
	filter: '',
	sidebarOpen: false,
}

const slice = createSlice({
	name: 'apps',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
		updateSidebar(state, action) {
			state.sidebarOpen = true
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllApps.fulfilled]: (state, action) => {
			state.allApps = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
