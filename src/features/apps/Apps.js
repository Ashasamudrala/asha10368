import React from 'react'
import Button from '@material-ui/core/Button'
import {
	APPS_TRANSLATIONS,
	APP_TEXT,
	APP_BUTTON,
} from '../../utilities/constants'
import { useTranslation } from 'react-i18next'
import './apps.scss'
import { useHistory } from 'react-router-dom'

export default function Projects() {
	const { t } = useTranslation(APPS_TRANSLATIONS)
	const history = useHistory()

	const handleSidebarButtonOnClick = () => {
		history.push('/database')
	}

	function renderEmptyProject() {
		return (
			<div className='app-page'>
				<div className='data-base'>
					<img className='data-base-icon' src='/images/Database.svg' />
				</div>

				<div className='data-base-text'>{t(APP_TEXT)}</div>
				<div>
					<Button className='new-app' onClick={handleSidebarButtonOnClick}>
						{t(APP_BUTTON)}
					</Button>
				</div>
			</div>
		)
	}
	return <React.Fragment>{renderEmptyProject()}</React.Fragment>
}
