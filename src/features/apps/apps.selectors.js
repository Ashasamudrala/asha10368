import slice from './apps.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllApps = (state) => selectSlice(state).allApps

export const selectAppsFilter = (state) => selectSlice(state).filter

export const selectSidebar = (state) => selectSlice(state).sidebarOpen
