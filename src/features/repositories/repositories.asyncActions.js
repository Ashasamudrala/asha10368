import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_REPOSITORIES } from '../../utilities/apiEndpoints'

export const fetchAllRepositories = createAsyncThunk(
	'repositories/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ALL_REPOSITORIES,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
