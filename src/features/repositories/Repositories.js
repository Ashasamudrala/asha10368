import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectAllRepositories } from './repositories.selectors'
import { fetchAllRepositories } from './repositories.asyncActions'
import repositoriesGridProp from '../../config/repositoriesGrid.json'
import Grid from '../../widgets/grid/Grid'
import IsyButton from '../../widgets/button/button'
import {
	REPOSITORY_TRANSLATIONS,
	ADD_REPOSITORY,
	permission,
} from '../../utilities/constants'
import AddRepository from '../../../src/features/addRepository/AddRepository'
import { useTranslation } from 'react-i18next'
import Can from '../../widgets/auth/Can'
import './repositories.scss'
import ViewRepositoriesDrawer from '../../features/viewRepositories'
import viewRepositoriesData from '../../config/viewRepositories.json'
import AuthProvider from '../../widgets/auth/Auth'

export default function Repositories() {
	const repositories = useSelector(selectAllRepositories)
	const dispatch = useDispatch()
	const { t } = useTranslation(REPOSITORY_TRANSLATIONS)
	const [isRepoDrawerOpen, setIsRepoDrawerOpen] = React.useState(false)

	const handleRepoButtonOnClick = (event) => {
		setIsRepoDrawerOpen(true)
	}
	const handleIsRepoDrawerOpen = (open) => {
		setIsRepoDrawerOpen(open)
	}

	useEffect(() => {
		dispatch(fetchAllRepositories())
	}, [dispatch])

	const [isDrawerOpen, setIsDrawerOpen] = useState(false)
	const [vcsId, setplatformId] = useState('')
	const handleViewItem = (isOpen, vcsId) => {
		setplatformId(vcsId)
		setIsDrawerOpen(isOpen)
	}
	return (
		<>
			<div className='add-repository-btn'>
				<Can
					action={permission.CREATE_VERSION_CONTROL_SYSTEM_CONFIGURATION}
					yes={() => (
						<IsyButton
							buttonClass={'add-repository-class'}
							handleButtonOnClick={handleRepoButtonOnClick}
						>
							{t(ADD_REPOSITORY)}
						</IsyButton>
					)}
					no={() => ''}
				></Can>
			</div>
			{isRepoDrawerOpen ? (
				<AddRepository openDrawer={handleIsRepoDrawerOpen} />
			) : null}
			<div>
				<AuthProvider userPermissions='view_repositories'></AuthProvider>
				<Grid
					handleViewItem={handleViewItem}
					properties={repositoriesGridProp}
					data={repositories}
				/>
			</div>
			{isDrawerOpen && (
				<ViewRepositoriesDrawer
					repositoryDetails={vcsId}
					viewRepositoriesJsonData={viewRepositoriesData}
					onCancel={handleViewItem}
				/>
			)}
		</>
	)
}
