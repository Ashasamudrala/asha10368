import Repositories from './Repositories'
import * as selectors from './repositories.selectors'
import * as asyncActions from './repositories.asyncActions'
import slice from './repositories.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllRepositories } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllRepositories, selectRepositoriesFilter } = selectors

// we export the component most likely to be desired by default
export default Repositories
