import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './repositories.asyncActions'

const initialState = {
	allRepositories: [],
	filter: '',
}

const slice = createSlice({
	name: 'repositories',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllRepositories.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allRepositories = action.payload.content
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
