import slice from './repositories.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllRepositories = (state) =>
	selectSlice(state).allRepositories

export const selectRepositoriesFilter = (state) => selectSlice(state).filter
