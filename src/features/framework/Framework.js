import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import Avatar from '@material-ui/core/Avatar'
import PropTypes from 'prop-types'
import { selectAllPlatform } from '../platform/platform.selectors'
import Button from '../../widgets/button'
import Grid from '../../widgets/grid/Grid'
import frameworkGridProp from '../../config/frameworkGrid.json'
import viewFrameworkJsonData from '../../config/viewFramework.json'
import AddFramework from '../../features/addFramework'
import {
	PLATFORM_TRANSLATIONS,
	ADD_FRAMEWORK,
	FRAMEWORK_ALL,
	permission,
} from '../../utilities/constants'
import Viewframework from '../../features/viewframework'
import Can from '../../widgets/auth/Can'
import './framework.scss'

export default function Framework() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const allPlatforms = useSelector(selectAllPlatform)
	const [selectedPlatform, setSelectedPlatform] = useState([])
	const [isFilterApplied, setIsFilterApplied] = useState(false)
	const [isDrawerOpen, setIsDrawerOpen] = useState(false)
	const [frameworkDetails, setFrameworkDetails] = useState({})
	const [openDrawer, setOpenDrawer] = useState(false)
	const [supportedVersions, setSupportedVersions] = useState([])
	const [platformId, setPlaformId] = useState('')
	const [filteredPlatformId, setFilteredPlatformId] = useState(t(FRAMEWORK_ALL))
	const filteredPlatforms = isFilterApplied ? selectedPlatform : allPlatforms

	// Calling func after adding framework to specific platform with filter.
	useEffect(() => {
		getFilterPlatformWithFrameworks(filteredPlatformId)
	}, [allPlatforms])

	/**
	 * Dropdown selection filter platforms with frameworks
	 * @param {object} event
	 */
	const getFilterPlatform = (event) => {
		if (event.target && event.target.value !== t(FRAMEWORK_ALL)) {
			setIsFilterApplied(true)
			const selectedPlatform = allPlatforms.find(
				(platform) => platform.id === event.target.value
			)
			setFilteredPlatformId(event.target.value)
			setSelectedPlatform([selectedPlatform])
		} else {
			setFilteredPlatformId(t(FRAMEWORK_ALL))
			setIsFilterApplied(false)
		}
	}
	const getFilterPlatformWithFrameworks = (platformId) => {
		if (platformId !== t(FRAMEWORK_ALL)) {
			setIsFilterApplied(true)
			const selectedPlatform = allPlatforms.find(
				(platform) => platform.id === platformId
			)
			setSelectedPlatform([selectedPlatform])
		} else {
			setFilteredPlatformId(t(FRAMEWORK_ALL))
			setIsFilterApplied(false)
		}
	}

	/**
	 * Represents handle view framework.
	 * @param {bool} isOpen
	 * @param {object} frameworkDetails
	 */
	const handleViewFramework = (isOpen, frameworkDetails) => {
		setFrameworkDetails(frameworkDetails)
		setIsDrawerOpen(isOpen)
	}

	/**
	 * Represents handle add framework.
	 * @param {event} e
	 * @param {object} platformDetails
	 */
	const handleAddFramework = (e, platformDetails) => {
		openSideDrawer(true)
		setSupportedVersions(platformDetails.versions)
		setPlaformId(platformDetails.id)
	}

	/**
	 * Represents to open side drawer when we click on add or view.
	 * @param {bool} open
	 */
	const openSideDrawer = (open) => {
		setOpenDrawer(open)
	}
	return (
		<div className='list-platform-container'>
			<div className='dropdown-filter'>
				<select onChange={getFilterPlatform}>
					<option value={t(FRAMEWORK_ALL)}>{t(FRAMEWORK_ALL)}</option>
					{allPlatforms.map((item) => (
						<option key={item.id} value={item.id}>
							{item.name}
						</option>
					))}
				</select>
			</div>
			{filteredPlatforms &&
				filteredPlatforms.map((item, index) => (
					<div key={index} className='framework-container'>
						<div className='framework-grid'>
							<div className='content'>
								<div className='platform-icon'>
									<Avatar>
										{item.metadata.icon &&
										(item.metadata.icon === 'java' ||
											item.metadata.icon === 'nodejs') ? (
											<img
												src={`/images/platformIcons/${item.metadata.icon}.svg`}
												alt={item.name.charAt(0)}
											/>
										) : (
											<img
												src={`/images/platformIcons/${'defaultIcon'}.svg`}
												alt={item.name.charAt(0)}
											/>
										)}
									</Avatar>
								</div>
								<div className='description'>
									<div className='platform-name'>{item.name}</div>
									<div className='platform-desc'>{item.description}</div>
								</div>
								<Can
									action={permission.CREATE_FRAMEWORK}
									yes={() => (
										<div>
											<div className='add-framework-btn'>
												<Button
													handleButtonOnClick={(e) =>
														handleAddFramework(e, item)
													}
													className='btn'
												>
													{t(ADD_FRAMEWORK)}
												</Button>
											</div>
										</div>
									)}
									no={() => ''}
								></Can>
							</div>
						</div>
						<Grid
							handleViewItem={(isOpen, frameworkDetails) =>
								handleViewFramework(isOpen, frameworkDetails)
							}
							properties={frameworkGridProp}
							data={Object.values(item.frameworks)}
						/>
					</div>
				))}
			{isDrawerOpen && (
				<Can
					action={permission.VIEW_FRAMEWORK}
					yes={() => (
						<Viewframework
							viewFrameworkDetails={frameworkDetails}
							viewFrameworkJsonData={viewFrameworkJsonData}
							onCancel={handleViewFramework}
						/>
					)}
					no={() => ''}
				></Can>
			)}
			{openDrawer && (
				<AddFramework
					supportedVersions={supportedVersions}
					platformId={platformId}
					openDrawer={openSideDrawer}
				/>
			)}
		</div>
	)
}
Framework.propTypes = {
	platform: PropTypes.arrayOf(Object),
	getFilterPlatform: PropTypes.func,
	platforms: PropTypes.arrayOf(Object),
	dataFilter: PropTypes.bool.isRequired,
}
