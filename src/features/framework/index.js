import Framework from './Framework'
import * as selectors from './framework.selectors'
import * as asyncActions from './framework.asyncActions'
import slice from './framework.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllFramework } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllFramework, selectFrameworkFilter } = selectors

// we export the component most likely to be desired by default
export default Framework
