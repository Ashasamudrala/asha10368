import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_FRAMEWORKS } from '../../utilities/apiEndpoints'

export const fetchAllFramework = createAsyncThunk(
	'framework/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ALL_FRAMEWORKS,
			useCaching,
			noBusySpinner,
			errorMessage: 'Unable to load  framework. Please try again later.',
			...thunkArgs,
		})
)
