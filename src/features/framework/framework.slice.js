import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './framework.asyncActions'

const initialState = {
	allFramework: [],
	filter: '',
}
const slice = createSlice({
	name: 'framework',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllFramework.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allFramework = action.payload.content
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
