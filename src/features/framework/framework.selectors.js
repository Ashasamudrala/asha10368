import slice from './framework.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllFramework = (state) => selectSlice(state).allFramework

export const selectFrameworkFilter = (state) => selectSlice(state).filter
