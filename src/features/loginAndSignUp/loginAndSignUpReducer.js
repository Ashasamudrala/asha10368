import { combineReducers } from 'redux'
import * as login from './login'
import * as forgotPassword from './forgotPassword'
import * as resetPassword from './resetPassword'
import * as activeInviteSignup from './activeInviteSignup'

export default combineReducers({
	[login.name]: login.reducer,
	[forgotPassword.name]: forgotPassword.reducer,
	[resetPassword.name]: resetPassword.reducer,
	[activeInviteSignup.name]: activeInviteSignup.reducer,
})
