import Login from './Login'
import slice from './login.slice'

export const { name, reducer, actions } = slice

// we export the component most likely to be desired by default
export default Login
