import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { logInUser } from '../../../authAndPermissions/loginUserDetails.asyncActions'
import {
	PLATFORM_TRANSLATIONS,
	REMEMBER_ME,
	FORGOT_PASSWORD,
} from '../../../utilities/constants'
import FormBuilder from '../../../widgets/formBuilder/FormBuilder'
import loginInput from '../../../config/login.json'
import ErrorMessages from '../../../config/login/loginValidation.json'
import {
	fieldIsEmpty,
	emailIsValid,
	passwordIsValid,
} from '../../addPlatform/validations'
import _, { isNil } from 'lodash'
import Button from '../../../widgets/button/button'
import LogoHeader from '../../../widgets/LogoHeader'
import NeedAnAccount from '../needAnAccount/NeedAnAccount'
import './login.scss'
import { useHistory, Link } from 'react-router-dom'
import { selectGetLogInUserInfo } from '../../../authAndPermissions/loginUserDetails.selectors'

/** Representation of login page */
export default function Login() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()
	const login = useSelector(selectGetLogInUserInfo)
	const [loginFields, setLoginFields] = useState({
		username: '',
		password: '',
	})
	const [isChecked, setIsChecked] = useState(false)
	const [errors, setErrovalues] = useState({})
	const history = useHistory()

	useEffect(() => {
		redirectToHome()
	}, [login])

	const redirectToHome = () => {
		!isNil(login) && login.accessToken && history.push('/apps')
	}
	/**
	 * username and password values updating.
	 * @param {object} event
	 */
	const handleLoginFields = (event) => {
		setLoginFields({
			...loginFields,
			[event.target.name]: event.target.value,
		})
	}

	// handle validations of required fields
	const handleValidation = () => {
		const fields = { ...loginFields }
		const errors = {}
		loginInput.section[0].inputFields.map((inputField) =>
			inputField.required
				? fieldIsEmpty(fields[inputField.name])
					? (errors[inputField.name] = ErrorMessages[inputField.name])
					: null
				: null
		)
		if (!emailIsValid(fields.username)) {
			errors.username = ErrorMessages.invalid_email
		}
		if (!passwordIsValid(fields.password)) {
			errors.password = ErrorMessages.invalid_password
		}
		setErrovalues({ ...errors })
		return _.isEmpty(errors)
	}
	// Clicking Login btn this function calls and dispatching.
	const sendLoginData = () => {
		if (handleValidation()) {
			dispatch(
				logInUser({
					...loginFields,
				})
			)
		}
		// Saving username in sessionStorage if user can click on "remember me" check.
		if (isChecked) {
			sessionStorage.setItem('username', loginFields.username)
		}
	}

	// Checkbox updating after checkin or not.
	const onChangeCheckbox = () => {
		setIsChecked(!isChecked)
	}

	return (
		<div className='login-container'>
			<LogoHeader />
			<div className='form'>
				<div className='login-form'>
					<FormBuilder
						formBuilderInput={loginInput}
						onChange={handleLoginFields}
						formBuilderOutput={loginFields}
						errors={errors}
					/>
					<Button handleButtonOnClick={sendLoginData}>Login</Button>
					<div className='remember-forgot'>
						<div className='check-remember'>
							<input
								type='checkbox'
								id='checkbox'
								checked={isChecked}
								onChange={onChangeCheckbox}
								className='checkbox'
							/>
							<label htmlFor='checkbox'>
								<span>{t(REMEMBER_ME)}</span>
							</label>
						</div>
						<div>
							<Link className='forgot-text' to='/forgotpassword'>
								{t(FORGOT_PASSWORD)}
							</Link>
						</div>
					</div>
				</div>
				<NeedAnAccount />
			</div>
		</div>
	)
}
