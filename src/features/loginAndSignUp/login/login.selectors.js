import slice from './login.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUpSelector'

export const selectSlice = (state) => loginAndRecoverySlice(state)[slice.name]
