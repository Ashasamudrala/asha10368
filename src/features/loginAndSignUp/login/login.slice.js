import { createSlice } from '@reduxjs/toolkit'

const initialState = {}
const slice = createSlice({
	name: 'login',
	initialState,

	reducers: {},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
