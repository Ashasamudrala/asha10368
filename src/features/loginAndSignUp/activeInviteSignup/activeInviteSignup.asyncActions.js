import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import {
	SIGNUP_USERS,
	GET_INVITATIONS_FOR_SIGNUP,
} from '../../../utilities/apiEndpoints'
import { SIGNUP_ACCOUNT_SUCCESS_MESSAGE } from '../../../utilities/constants'

export const validateInvitationId = createAsyncThunk(
	'signUp/getAll',
	async (
		{ useCaching, noBusySpinner, InvitationIdFromCurrentPath } = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${GET_INVITATIONS_FOR_SIGNUP}?token=${InvitationIdFromCurrentPath}`,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const fetchAllSignUp = createAsyncThunk(
	'activeInviteSignup',
	async (
		{
			useCaching,
			noBusySpinner,
			firstName,
			lastName,
			email,
			title,
			password,
			invitationId,
			metadata: { additionalProp1, additionalProp2, additionalProp3 },
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: SIGNUP_USERS,
			useCaching,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					firstName,
					lastName,
					title,
					email,
					password,
					invitationId,
					metadata: {
						additionalProp1: {},
						additionalProp2: {},
						additionalProp3: {},
					},
				}),
			},
			noBusySpinner,
			successMessage: SIGNUP_ACCOUNT_SUCCESS_MESSAGE,
			errorMessage:
				'Unable to load activeInviteSignup. Please try again later.',
			...thunkArgs,
		})
)
