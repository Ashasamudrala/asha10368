import slice from './activeInviteSignup.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUpSelector'

export const selectSlice = (state) => loginAndRecoverySlice(state)[slice.name]

export const selectAllActiveInviteSignup = (state) =>
	selectSlice(state).allInviteDetails

export const selectActiveInviteSignupFilter = (state) =>
	selectSlice(state).filter
