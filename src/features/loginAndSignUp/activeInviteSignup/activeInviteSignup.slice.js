import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './activeInviteSignup.asyncActions'

const initialState = {
	allInviteDetails: [],
	allActiveInviteSignup: [],
	filter: '',
}

const slice = createSlice({
	name: 'activeInviteSignup',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.validateInvitationId.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allInviteDetails = action.payload
			}
		},
		[asyncActions.fetchAllSignUp.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allActiveInviteSignup = action.payload
				state.filter = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
