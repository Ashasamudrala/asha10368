import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import activeInviteSignUpInput from '../../../config/signUp/signUp.json'
import {
	fetchAllSignUp,
	validateInvitationId,
} from './activeInviteSignup.asyncActions'
import activeInviteSignupOutput from '../../../config/signUp/signUpOutput.json'
import FormBuilder from '../../../widgets/formBuilder/FormBuilder'
import LogoHeader from '../../../widgets/LogoHeader'
import IsyButton from '../../../widgets/button/button'
import { useTranslation } from 'react-i18next'
import ErrorMessages from '../../../config/signUpValidations.json'
import {
	CREATE_ACCOUNT,
	SIGNUP_TRANSLATIONS,
	AGREE_ALL,
	TERMS_AND_CONDITIONS,
} from '../../../utilities/constants'
import {
	fieldIsEmpty,
	emailIsValid,
	passwordIsValid,
} from '../../addPlatform/validations'
import './activeInviteSignup.scss'
import _ from 'lodash'
import {
	selectActiveInviteSignupFilter,
	selectAllActiveInviteSignup,
} from './activeInviteSignup.selectors'
import { useHistory } from 'react-router-dom'

export default function ActiveInviteSignup() {
	const details = useSelector(selectAllActiveInviteSignup)
	const history = useHistory()

	const [isChecked, setIsChecked] = useState(false)
	const { t } = useTranslation(SIGNUP_TRANSLATIONS)
	const [errors, setErrorValues] = useState({})
	const status = useSelector(selectActiveInviteSignupFilter)
	const [activeSignUpDetails, setActiveSignUpDetails] = useState(
		activeInviteSignupOutput
	)

	const dispatch = useDispatch()
	useEffect(() => {
		if (status !== '') {
			status && history.push('/login')
		}
	}, [status])

	useEffect(() => {
		if (details && details.member) {
			setActiveSignUpDetails({
				...activeSignUpDetails,
				email: details.member.email,
			})
		}
	}, [details.member])
	const onChangeCheckbox = () => {
		setIsChecked(!isChecked)
	}
	const handleSignUpInviteFields = (event) => {
		setActiveSignUpDetails({
			...activeSignUpDetails,
			[event.target.name]: event.target.value,
		})
	}
	const handleValidation = () => {
		const fields = { ...activeSignUpDetails }
		const errors = {}
		activeInviteSignUpInput.section[0].inputFields.map((inputField) =>
			inputField.required
				? fieldIsEmpty(fields[inputField.name])
					? (errors[inputField.name] = ErrorMessages[inputField.name])
					: null
				: null
		)
		if (!emailIsValid(fields.email)) {
			errors.email = ErrorMessages.invalid_email
		}
		if (!passwordIsValid(fields.password)) {
			errors.password = ErrorMessages.invalid_password
		}
		if (fields.password !== fields.confirmPassword) {
			errors.confirmPassword = ErrorMessages.invalid_confirm_password
		}
		setErrorValues({ ...errors })
		return _.isEmpty(errors)
	}
	const handleSave = (e) => {
		if (handleValidation()) {
			dispatch(
				fetchAllSignUp({
					...activeSignUpDetails,
					invitationId: details.id,
				})
			)
		}
	}

	let InvitationIdFromCurrentPath
	useEffect(() => {
		var currentPath = window.location.href
		var url = new URL(currentPath)
		InvitationIdFromCurrentPath = url.searchParams.get('invitationId')
		dispatch(
			validateInvitationId({
				InvitationIdFromCurrentPath,
			})
		)
	}, [])

	return (
		<div className='signUp-container'>
			<LogoHeader />
			<div className='form'>
				<div className='signup-form'>
					<FormBuilder
						formBuilderInput={activeInviteSignUpInput}
						formBuilderOutput={activeSignUpDetails}
						onChange={handleSignUpInviteFields}
						errors={errors}
					/>
					<div className='agree-all-terms'>
						<div className='check-agree'>
							<input
								type='checkbox'
								id='checkbox'
								checked={isChecked}
								onChange={onChangeCheckbox}
								className='checkbox'
							/>
							<label htmlFor='checkbox'>
								<span className='check-text'>{t(AGREE_ALL)}</span>
							</label>
							<span className='terms-conditions'>
								{' '}
								{t(TERMS_AND_CONDITIONS)}
							</span>
						</div>
						<IsyButton
							handleButtonOnClick={handleSave}
							buttonClass='create-btn'
						>
							{t(CREATE_ACCOUNT)}
						</IsyButton>
					</div>
				</div>
			</div>
		</div>
	)
}
