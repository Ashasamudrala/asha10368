import * as selectors from './activeInviteSignup.selectors'
import ActiveInviteSignup from './ActiveInviteSignup'
import * as asyncActions from './activeInviteSignup.asyncActions'
import slice from './activeInviteSignup.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllSignUp } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectAllActiveInviteSignup,
	selectActiveInviteSignupFilter,
} = selectors

// we export the component most likely to be desired by default
export default ActiveInviteSignup
