import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import {
	GET_TOKEN_FOR_RESET_PASSWORD,
	PASSWORD_RESET_PASSWORD,
} from '../../../utilities/apiEndpoints'

export const validateResetToken = createAsyncThunk(
	'resetPassword/getAll',
	async ({ useCaching, noBusySpinner, tokenFromCurrentPath } = {}, thunkArgs) =>
		await doAsync({
			url: `${GET_TOKEN_FOR_RESET_PASSWORD}?token=${tokenFromCurrentPath}`,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const postEmailAndToken = createAsyncThunk(
	'resetPassword',
	async ({ useCaching, noBusySpinner, token, newPassword } = {}, thunkArgs) => {
		var newpasswordDetails = {
			token: token,
			newPassword: newPassword,
		}
		await doAsync({
			url: PASSWORD_RESET_PASSWORD,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify(newpasswordDetails),
			},
			successMessage: 'Password changed successfully',
			...thunkArgs,
		})
	}
)
