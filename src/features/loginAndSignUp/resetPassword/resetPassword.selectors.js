import slice from './resetPassword.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUpSelector'

export const selectSlice = (state) => loginAndRecoverySlice(state)[slice.name]

export const selectAllResetPassword = (state) =>
	selectSlice(state).allResetPassword

export const selectResetPasswordFilter = (state) => selectSlice(state).filter
