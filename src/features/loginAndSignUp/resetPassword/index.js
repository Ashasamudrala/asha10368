import ResetPassword from './ResetPassword'
import * as selectors from './resetPassword.selectors'
import * as asyncActions from './resetPassword.asyncActions'
import slice from './resetPassword.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { validateResetToken, postEmailAndToken } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllResetPassword, selectResetPasswordFilter } = selectors

// we export the component most likely to be desired by default
export default ResetPassword
