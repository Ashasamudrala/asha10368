import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './resetPassword.asyncActions'

const initialState = {
	allResetPassword: [],
	postEmailAndTokens: '',
	filter: '',
}

const slice = createSlice({
	name: 'resetPassword',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.validateResetToken.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allResetPassword = action.payload
			}
		},

		[asyncActions.postEmailAndToken.fulfilled]: (state, action) => {
			if (action.payload) {
				state.postEmailAndTokens = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
