import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectAllResetPassword } from './resetPassword.selectors'
import {
	validateResetToken,
	postEmailAndToken,
} from './resetPassword.asyncActions'

import _ from 'lodash'
import FormBuilder from '../../../widgets/formBuilder/FormBuilder'
import ResetPasswordInput from '../../../config/resetpassword.json'
import ResetPasswordOutput from '../../../config/resetPasswordOutput.json'
import ErrorMessages from '../../../config/resetPasswordValidations.json'
import { useTranslation } from 'react-i18next'
import NeedAnAccount from '../needAnAccount/NeedAnAccount'
import {
	PLATFORM_TRANSLATIONS,
	ISYMPHONY,
	STUDIO,
	REMEMBER_YOUR_PASSWORD,
	LOGIN,
	CHANGE_YOUR_PASSWORD,
} from '../../../utilities/constants'
import {
	fieldIsEmpty,
	emailIsValid,
	passwordIsValid,
} from '../../addPlatform/validations'
import IsyButton from '../../../widgets/button/button'
import './ResetPassword.scss'
import { useHistory } from 'react-router-dom'

export default function ResetPassword() {
	const userMailId = useSelector(selectAllResetPassword)
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()
	const [resetPasswordData, setResetPasswordDetails] = useState(
		ResetPasswordOutput
	)
	const [errors, setErrovalues] = useState({})
	const history = useHistory()
	const [tokenDetails, setToken] = useState('')

	useEffect(() => {
		setResetPasswordDetails({
			...resetPasswordData,
			email: userMailId.email,
		})
	}, [userMailId])

	// this is used for getting the json of fields
	const handlePlatformFields = (event) => {
		setResetPasswordDetails({
			...resetPasswordData,
			[event.target.name]: event.target.value,
		})
	}

	// handle validations of required fields
	const handleValidation = () => {
		const fields = { ...resetPasswordData }
		const errors = {}
		ResetPasswordInput.section[0].inputFields.map((inputField) =>
			inputField.required
				? fieldIsEmpty(fields[inputField.name])
					? (errors[inputField.name] = ErrorMessages[inputField.name])
					: null
				: null
		)
		if (!emailIsValid(fields.email)) {
			errors.email = ErrorMessages.invalid_email
		}
		if (!passwordIsValid(fields.password)) {
			errors.password = ErrorMessages.invalid_password
		}
		if (fields.password !== fields.confirm_password) {
			errors.confirm_password = ErrorMessages.invalid_confirm_password
		}
		setErrovalues({ ...errors })
		return _.isEmpty(errors)
	}

	// on clicking save  it calls metdata to key value pairs
	const handleSave = (e) => {
		if (handleValidation()) {
			dispatch(
				postEmailAndToken({
					token: tokenDetails,
					newPassword: resetPasswordData.password,
				})
			)
		}
	}

	const navigateToLogin = () => {
		history.push('/login')
	}

	let tokenFromCurrentPath
	useEffect(() => {
		var currentPath = window.location.href
		var url = new URL(currentPath)
		tokenFromCurrentPath = url.searchParams.get('token')

		setToken(tokenFromCurrentPath)
		dispatch(
			validateResetToken({
				tokenFromCurrentPath,
			})
		)
	}, [])
	return (
		<div className='login-container'>
			<div className='logo-text'>
				<span className='logo-left'>{t(ISYMPHONY)}</span>
				<span className='logo-right'>{t(STUDIO)}</span>
			</div>
			<div className='form'>
				<div className='login-form'>
					<FormBuilder
						formBuilderInput={ResetPasswordInput}
						onChange={handlePlatformFields}
						formBuilderOutput={resetPasswordData}
						errors={errors}
					/>

					<IsyButton handleButtonOnClick={handleSave} buttonClass='create-btn'>
						{t(CHANGE_YOUR_PASSWORD)}
					</IsyButton>
					<div className='remember-password'>
						<div className='message-class'>
							<div className='remember-tag'>
								<span>{t(REMEMBER_YOUR_PASSWORD)}</span>
							</div>
						</div>
						<div className='log-in'>
							<a onClick={navigateToLogin}>{t(LOGIN)}</a>
						</div>
					</div>
				</div>
				<div className='footer-class'>
					<NeedAnAccount />
				</div>
			</div>
		</div>
	)
}
