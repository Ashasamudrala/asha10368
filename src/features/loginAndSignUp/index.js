import reducer from './loginAndSignUpReducer'
import { name } from './loginAndSignUpSelector'
import component from './LoginAndSignUp'
export { reducer, name }
export default component
