import Login from './login/Login'
import ForgotPassword from './forgotPassword/ForgotPassword'
import ResetPassword from './resetPassword/ResetPassword'
import ActiveInviteSignUp from './activeInviteSignup/ActiveInviteSignup'

export default {
	Login: Login,
	ForgotPassword: ForgotPassword,
	ResetPassword: ResetPassword,
	ActiveInviteSignUp: ActiveInviteSignUp,
}
