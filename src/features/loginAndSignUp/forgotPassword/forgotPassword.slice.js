import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './forgotPassword.asyncActions'

const initialState = {
	allForgotPassword: [],
	filter: '',
}

const slice = createSlice({
	name: 'forgotPassword',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllForgotPassword.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allForgotPassword = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
