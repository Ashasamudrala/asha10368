import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../../infrastructure/doAsync'
import { PASSWORD_RESET_REQUESTS } from '../../../utilities/apiEndpoints'

export const fetchAllForgotPassword = createAsyncThunk(
	'forgotPassword/getAll',
	async ({ useCaching, noBusySpinner, email } = {}, thunkArgs) => {
		var userEmail = { email: email }
		return await doAsync({
			url: PASSWORD_RESET_REQUESTS,
			httpMethod: 'post',
			errorMessage: 'Please enter valid mail Id',
			httpConfig: {
				body: JSON.stringify(userEmail),
			},
			successMessage: 'Reset link sent successfully',
			...thunkArgs,
		})
	}
)
