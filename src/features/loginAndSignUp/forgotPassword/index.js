import ForgotPassword from './ForgotPassword'
import * as selectors from './forgotPassword.selectors'
import * as asyncActions from './forgotPassword.asyncActions'
import slice from './forgotPassword.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllForgotPassword } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllForgotPassword, selectForgotPasswordFilter } = selectors

// we export the component most likely to be desired by default
export default ForgotPassword
