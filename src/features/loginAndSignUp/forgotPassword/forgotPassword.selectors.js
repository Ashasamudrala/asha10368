import slice from './forgotPassword.slice'
import { selectSlice as loginAndRecoverySlice } from '../loginAndSignUpSelector'

export const selectSlice = (state) => loginAndRecoverySlice(state)[slice.name]

export const selectAllForgotPassword = (state) =>
	selectSlice(state).allForgotPassword

export const selectForgotPasswordFilter = (state) => selectSlice(state).filter
