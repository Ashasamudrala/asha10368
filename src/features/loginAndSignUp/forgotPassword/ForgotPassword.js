import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { fetchAllForgotPassword } from './forgotPassword.asyncActions'

import _ from 'lodash'
import './forgotPassword.scss'
import {
	PLATFORM_TRANSLATIONS,
	ISYMPHONY,
	STUDIO,
	RESET_PASSWORD,
	REMEMBER_YOUR_PASSWORD,
	LOGIN,
} from '../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import FormBuilder from '../../../widgets/formBuilder/FormBuilder'
import forgetPasswordInput from '../../../config/forgotPassword.json'
import NeedAnAccount from '../needAnAccount/NeedAnAccount'
import forgotPasswordOutput from '../../../config/forgotPasswordOutput.json'
import ErrorMessages from '../../../config/forgotPasswordValidations.json'
import { fieldIsEmpty, emailIsValid } from '../../addPlatform/validations'
import IsyButton from '../../../widgets/button/button'
import { useHistory } from 'react-router-dom'

export default function ForgotPassword() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const dispatch = useDispatch()
	const [forgotPasswordData, setforgotPasswordDetails] = useState(
		forgotPasswordOutput
	)
	const [errors, setErrorValues] = useState({})
	const history = useHistory()
	const handleFields = (event) => {
		setforgotPasswordDetails({
			...forgotPasswordData,
			[event.target.name]: event.target.value,
		})
	}
	const handleValidation = () => {
		const fields = { ...forgotPasswordData }
		const errors = {}
		forgetPasswordInput.section[0].inputFields.map((inputField) =>
			inputField.required
				? fieldIsEmpty(fields[inputField.name])
					? (errors[inputField.name] = ErrorMessages[inputField.name])
					: null
				: null
		)
		if (!emailIsValid(fields.email)) {
			errors.email = ErrorMessages.invalid_email
		}

		setErrorValues({ ...errors })
		return _.isEmpty(errors)
	}

	const navigateToLogin = () => {
		history.push('/login')
	}

	// Clicking Reset Password btn this function calls and dispatching.
	const sendResetLinkToMail = () => {
		if (handleValidation()) {
			dispatch(
				fetchAllForgotPassword({
					...forgotPasswordData,
				})
			)
		}
	}

	return (
		<div className='forgot-password-container'>
			<div className='logo-text'>
				<span className='logo-left'>{t(ISYMPHONY)}</span>
				<span className='logo-right'>{t(STUDIO)}</span>
			</div>
			<div className='form'>
				<div className='forgot-password-form'>
					<FormBuilder
						formBuilderInput={forgetPasswordInput}
						onChange={handleFields}
						formBuilderOutput={forgotPasswordData}
						errors={errors}
					/>

					<IsyButton handleButtonOnClick={sendResetLinkToMail}>
						{t(RESET_PASSWORD)}
					</IsyButton>
					<div className='remember-password'>
						<div className='message-class'>
							<div className='remember-tag'>
								<span>{t(REMEMBER_YOUR_PASSWORD)}</span>
							</div>
						</div>
						<div className='log-in'>
							<a onClick={navigateToLogin}>{t(LOGIN)}</a>
						</div>
					</div>
				</div>
				<div className='footer'>
					<NeedAnAccount />
				</div>
			</div>
		</div>
	)
}
