import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import {
	PLATFORM_TRANSLATIONS,
	NEED_AN_ACCOUNT,
	SIGN_UP,
} from '../../../utilities/constants'
import './needAnAccount.scss'

function NeedAnAccount() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	return (
		<div className='login-footer'>
			<div className='footer-text'>
				<p className='left-text'>{t(NEED_AN_ACCOUNT)}</p>
				<Link to='/signup' className='right-text'>
					{t(SIGN_UP)}
				</Link>
			</div>
		</div>
	)
}

export default NeedAnAccount
