import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { selectAllContinuousIntegration } from './continuousIntegration.selectors'
import { fetchAllContinuousIntegration } from './continuousIntegration.asyncActions'
import continuousIntegrationGridProp from '../../config/continuousIntegrationGrid.json'
import ViewContiniousIntegrationDrawer from '../viewContinuousIntegration'
import viewContiniousIntegrationData from '../../config/viewContinuousIntegration.json'
import AddContinuousIntegration from '../addContinuousIntegration/AddContinuousIntegration'
import IsyButton from '../../widgets/button/button'
import Grid from '../../widgets/grid/Grid'
import Can from '../../widgets/auth/Can'
import {
	ADD_CONFIGURATION,
	CONTINUOUSINTEGRATION_TRANSLATIONS,
	permission,
} from '../../utilities/constants'
import './continuousIntegration.scss'

export default function ContinuousIntegration() {
	const { t } = useTranslation(CONTINUOUSINTEGRATION_TRANSLATIONS)
	const continuousIntegration = useSelector(selectAllContinuousIntegration)
	const [isDrawerOpen, setIsDrawerOpen] = useState(false)
	const [
		continuousIntegrationDetails,
		setContinuousIntegrationDetails,
	] = useState('')
	const [isCIDrawerOpen, setCIDrawerOpen] = React.useState(false)
	const dispatch = useDispatch()
	const handleViewItem = (isOpen, viewItem) => {
		setContinuousIntegrationDetails(viewItem)
		setIsDrawerOpen(isOpen)
	}

	const handleCIButtonOnClick = (event) => {
		setCIDrawerOpen(true)
	}
	const handleContinuousIntegrationDrawerOpen = (open) => {
		setCIDrawerOpen(open)
	}
	useEffect(() => {
		dispatch(fetchAllContinuousIntegration())
	}, [dispatch])
	return (
		<>
			<div className='add-configuration-btn'>
				<Can
					action={permission.CREATE_CONTINUOUS_INTEGRATION_CONFIGURATION}
					yes={() => (
						<IsyButton
							buttonClass={'add-configuration-class'}
							handleButtonOnClick={handleCIButtonOnClick}
						>
							{t(ADD_CONFIGURATION)}
						</IsyButton>
					)}
					no={() => ''}
				></Can>
			</div>
			{isCIDrawerOpen ? (
				<AddContinuousIntegration
					openDrawer={handleContinuousIntegrationDrawerOpen}
				/>
			) : null}
			<Grid
				handleViewItem={handleViewItem}
				properties={continuousIntegrationGridProp}
				data={continuousIntegration}
			/>
			{isDrawerOpen && (
				<ViewContiniousIntegrationDrawer
					continuousIntegrationDetails={continuousIntegrationDetails}
					viewContiniousIntegrationJsonData={viewContiniousIntegrationData}
					onCancel={handleViewItem}
				/>
			)}
		</>
	)
}
