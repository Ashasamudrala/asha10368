import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_CONTINUOUSINTEGRATION } from '../../utilities/apiEndpoints'

export const fetchAllContinuousIntegration = createAsyncThunk(
	'continuousIntegration/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ALL_CONTINUOUSINTEGRATION,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
