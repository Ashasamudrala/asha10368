import slice from './continuousIntegration.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllContinuousIntegration = (state) =>
	selectSlice(state).allContinuousIntegration

export const selectContinuousIntegrationFilter = (state) =>
	selectSlice(state).filter
