import ContinuousIntegration from './Continuousintegration'
import * as selectors from './continuousIntegration.selectors'
import * as asyncActions from './continuousIntegration.asyncActions'
import slice from './continuousIntegration.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllContinuousIntegration } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectAllContinuousIntegration,
	selectContinuousIntegrationFilter,
} = selectors

// we export the component most likely to be desired by default
export default ContinuousIntegration
