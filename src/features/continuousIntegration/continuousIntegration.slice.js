import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './continuousIntegration.asyncActions'

const initialState = {
	allContinuousIntegration: [],
	filter: '',
}

const slice = createSlice({
	name: 'continuousIntegration',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllContinuousIntegration.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allContinuousIntegration = action.payload.content
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
