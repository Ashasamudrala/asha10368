import AddContinuousDelivery from './AddContinuousDelivery'
import * as selectors from './addContinuousDelivery.selectors'
import slice from './addContinuousDelivery.slice'

export const {
	name,
	actions: { removeStatus },
	reducer,
} = slice

// we prefix all selectors with the the "select" prefix
export const { getContinuousDeliveryStatus } = selectors

// we export the component most likely to be desired by default
export default AddContinuousDelivery
