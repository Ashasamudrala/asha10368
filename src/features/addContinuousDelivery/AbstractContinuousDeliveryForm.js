import React, { useState } from 'react'
import RightSlider from '../../widgets/rightSideSlider/RightSideSlider'
import BusyIndicator from '../../widgets/busyIndicator/BusyIndicator'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import {
	ADD,
	ONCHANGE,
	DELETE,
	modeType,
	REPOSITORY_TRANSLATIONS,
} from '../../utilities/constants'
import {
	fieldIsEmpty,
	continuousDeliveryFieldIsValid,
} from '../addPlatform/validations'
import _ from 'lodash'
import { useTranslation } from 'react-i18next'
import ErrorMessages from '../../config/continuousDelivery/continuousDeliveryValidations.json'

/* 
this component is used to provide ContinuousDelivery details whenever onclick of button
 */
export default function AbstractContinuousDelivery(props) {
	// there are state values for showing dynamic and ContinuousDelivery details for storing the fields values
	const { continuousDelieveryInput, continuousDelieveryOutput } = props

	const [continuousDeliveryFields, setContinuousDeliveryFields] = useState({
		...continuousDelieveryOutput,
	})

	const [errors, setErrorValues] = useState({})

	const { t } = useTranslation(REPOSITORY_TRANSLATIONS)
	/**
	 * On clicking the save button it converts the metadata to key, value pairs
	 */
	const handleSave = () => {
		if (handleValidation()) {
			convertMetdataToKeyValue()
		}
	}

	const convertMetdataToKeyValue = () => {
		// Clone the apiConfiguration and transform metadata property.
		const clonedFormValues = _.cloneDeep(continuousDeliveryFields)
		const { apiConfiguration, metadata } = clonedFormValues
		let processedMetadata = {}
		if (metadata && metadata.length >= 0) {
			processedMetadata = _.mapValues(_.keyBy(metadata, 'key'), 'value')
			processedMetadata = _.omit(processedMetadata, [''])
		}
		if (
			apiConfiguration &&
			apiConfiguration.metadata &&
			apiConfiguration.metadata.length >= 0
		) {
			apiConfiguration.metadata = _.mapValues(
				_.keyBy(apiConfiguration.metadata, 'key'),
				'value'
			)
			apiConfiguration.metadata = _.omit(apiConfiguration.metadata, [''])
		}
		props.onSave({
			...clonedFormValues,
			metadata: { ...processedMetadata },
		})
	}

	// this is used for setting the json fields
	const handlecontinuousDeliveryFields = (event, fieldName, action) => {
		/**
		 * if fieldName is defined then if condition executes this is for apiD and version
		 * and then else executes when ever fieldName is undefined
		 */
		let processedfieldName = []
		if (fieldName) {
			/**
			 * based on action switch case executes
			 */
			switch (action) {
				case t(ADD):
					processedfieldName = continuousDeliveryFields
					processedfieldName = _.set(processedfieldName, fieldName, [
						event,
						..._.get(processedfieldName, fieldName),
					])
					setContinuousDeliveryFields({ ...processedfieldName })
					break
				case t(ONCHANGE):
					/**
					 * It is used when onchange of metadata is called and push the change value
					 * based on id into array if type is (metadata)
					 */
					processedfieldName = continuousDeliveryFields
					_.get(processedfieldName, fieldName)[event.target.dataset.id][
						event.target.name
					] = event.target.value
					setContinuousDeliveryFields({ ...processedfieldName })
					break
				case t(DELETE):
					/**
					 * It is used when delete of metadata and version  is called and removes the item
					 * based on id from array
					 */
					processedfieldName = continuousDeliveryFields
					processedfieldName = _.set(
						processedfieldName,
						fieldName,
						_.get(processedfieldName, fieldName).filter(
							(version, id) => event !== id
						)
					)
					setContinuousDeliveryFields({ ...processedfieldName })
					break
				default:
			}
		} else {
			processedfieldName = continuousDeliveryFields
			processedfieldName = _.set(
				processedfieldName,
				event.target.name,
				event.target.value
			)
			setContinuousDeliveryFields({ ...processedfieldName })
		}
	}
	/**
	 * Form validations
	 */
	const handleValidation = () => {
		const fields = { ...continuousDeliveryFields }
		const errors = {}
		continuousDelieveryInput.section.forEach(
			(formFields) =>
				formFields.inputFields &&
				formFields.inputFields.forEach((inputField) => {
					if (inputField.required) {
						if (fieldIsEmpty(_.get(fields, inputField.name))) {
							_.set(errors, inputField.name, ErrorMessages[inputField.name])
						}
					}
				})
		)
		if (fields.name !== '' && continuousDeliveryFieldIsValid(fields.name)) {
			errors.name = ErrorMessages.invalidName
		}
		setErrorValues({ ...errors })
		return _.isEmpty(errors)
	}
	/**
	 * this is used for when clicking on metadata to move to dynamic add component
	 */
	const renderRightSideSlider = () => {
		return (
			<RightSlider onCancel={props.openDrawer} drawerName='drawer'>
				<BusyIndicator>
					<CloseOutlinedIcon
						className='close-icon'
						onClick={() => {
							props.openDrawer()
						}}
					/>
					<div className='continuous-delivery-system'>
						<div className={props.className}>
							<FormBuilder
								formBuilderInput={continuousDelieveryInput}
								onChange={handlecontinuousDeliveryFields}
								formBuilderOutput={continuousDeliveryFields}
								errors={errors}
							/>
						</div>
						<SliderFooter
							mode={modeType.WRITE}
							handleClose={props.openDrawer}
							handleSave={handleSave}
						/>
					</div>
				</BusyIndicator>
			</RightSlider>
		)
	}
	return <div>{renderRightSideSlider()}</div>
}
