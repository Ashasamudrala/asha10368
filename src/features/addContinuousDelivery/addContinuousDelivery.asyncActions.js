import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { SAVE_CONTINUOUSDELIVERY } from '../../utilities/apiEndpoints'
import { CONTINUOUSDELIVERY_SUCCESS_MESSAGE } from '../../utilities/constants'
export const saveContinuousDelivery = createAsyncThunk(
	'addContinuousDelivery/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			name,
			description,
			metadata,
			apiConfiguration,
			channelConfiguration,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: SAVE_CONTINUOUSDELIVERY,
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name,
					description,
					metadata,
					apiConfiguration,
					channelConfiguration,
				}),
			},
			successMessage: `${name}${CONTINUOUSDELIVERY_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)
