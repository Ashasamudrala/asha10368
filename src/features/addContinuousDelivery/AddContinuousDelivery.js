import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import continuousDelieveryInput from '../../config/continuousDelivery/addContinuousDelivery.json'
import continuousDelieveryOutput from '../../config/continuousDelivery/continuousDeliveryOutput.json'
import AbstractContinuousDelivery from './AbstractContinuousDeliveryForm'
import { saveContinuousDelivery } from './addContinuousDelivery.asyncActions'
import { fetchAllContinuousDelivery } from '../../features/continuousDelivery/continuousDelivery.asyncActions'
import { getContinuousDeliveryStatus } from './addContinuousDelivery.selectors'
import { actions } from './addContinuousDelivery.slice'
import './continuousDelivery.scss'
const { removeStatus } = actions

export default function AddContinuousDelivery(props) {
	const dispatch = useDispatch()
	const status = useSelector(getContinuousDeliveryStatus)

	// when ever save button is clicked dispatch to addContinuousdeliveryapi
	const handleSave = (continuousdelivery) => {
		dispatch(saveContinuousDelivery({ ...continuousdelivery }))
	}

	/**
	 * when ever post is successfull it calls fetchAllcontinuousdelievery api
	 */
	useEffect(() => {
		if (status !== '') {
			status && dispatch(fetchAllContinuousDelivery())
			props.openDrawer(false)
		}
	}, [status])

	/**
	 * When ever componentwillmount remove status
	 */
	useEffect(() => {
		return () => {
			dispatch(removeStatus())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<div>
			<AbstractContinuousDelivery
				openDrawer={props.openDrawer}
				continuousDelieveryInput={continuousDelieveryInput}
				continuousDelieveryOutput={JSON.parse(
					JSON.stringify(continuousDelieveryOutput)
				)}
				className='continuous-delivery'
				onSave={handleSave}
			/>
		</div>
	)
}
