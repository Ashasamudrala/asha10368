import slice from './addContinuousDelivery.slice'

export const selectSlice = (state) => state[slice.name]

export const getContinuousDeliveryStatus = (state) => selectSlice(state).status
