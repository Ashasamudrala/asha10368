import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './addContinuousDelivery.asyncActions'

const initialState = {
	status: '',
}

const slice = createSlice({
	name: 'addContinuousDelivery',
	initialState,
	reducers: {
		// synchronous actions
		removeStatus(state) {
			state.status = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.saveContinuousDelivery.fulfilled]: (state, action) => {
			if (action.payload) {
				state.status = true
			} else {
				state.status = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
