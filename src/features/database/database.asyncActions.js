import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import {
	GET_ALL_DATABASES,
	CREATE_DATABASE,
	SAVE_DATABASE,
	DELETE_DATABASE,
	GET_ATTRIBUTE_TYPES,
	GET_CONSTRAINT_KEY_TYPES,
	UPDATE_DB_CONFIG,
} from '../../utilities/apiEndpoints'
import { selectAllDatabases } from './database.selectors'
import {
	DATABASE_DELETE_MESSAGE,
	DATABASE_DELETE_MESSAGE_SUCCESS,
	DATABASE_SAVE_MESSAGE_SUCCESS,
	DATABASE_CREATE_MESSAGE_SUCCESS,
	DATABASE_LOADING_MESSAGE_ERROR,
	DATABASE_CONFIG_SUCCESS,
} from '../../utilities/constants'
import { actions as dialogActions } from '../../infrastructure/dialogs/dialogs.slice'
import * as dialogTypes from '../../infrastructure/dialogs/dialogs.constants'
import i18n from 'i18next'
import { find } from 'lodash'

export const fetchAttributeTypes = createAsyncThunk(
	'database/dataType',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ATTRIBUTE_TYPES,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const fetchConstrainsKeyTypes = createAsyncThunk(
	'database/keyTypes',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: GET_CONSTRAINT_KEY_TYPES,
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const fetchAllDatabases = createAsyncThunk(
	'database/getAll',
	async ({ useCaching, noBusySpinner, appId } = {}, thunkArgs) =>
		await doAsync({
			url: GET_ALL_DATABASES(appId),
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)

export const createDatabase = createAsyncThunk(
	'database/createDatabase',
	async ({ useCaching, noBusySpinner, appId, name } = {}, thunkArgs) =>
		await doAsync({
			url: CREATE_DATABASE(appId),
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name,
				}),
			},
			successMessage: i18n.t(DATABASE_CREATE_MESSAGE_SUCCESS, { name }),
			...thunkArgs,
		})
)

export const createDatabaseAsync = createAsyncThunk(
	'database/createDatabaseAsync',
	async (data, { dispatch } = {}) => {
		const okayFunction = (name) =>
			dispatch(createDatabase({ appId: data.appId, ...name })).then(
				(response) => {
					if (response && response.payload) {
						dispatch(fetchAllDatabases({ appId: '5fc1eb3ca15859e036aadc1d' }))
					}
				}
			)
		return await dispatch(
			dialogActions.addDialog({
				type: dialogTypes.CREATE_DATABASE,
				onOkay: okayFunction,
				data: {},
			})
		)
	}
)

export const saveDatabase = createAsyncThunk(
	'database/save',
	async (
		{
			useCaching,
			noBusySpinner,
			appId,
			databaseId,
			schemaId,
			metadata,
			name,
			displayName,
			description,
			tables,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: SAVE_DATABASE(appId, databaseId, schemaId),
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					metadata,
					name,
					displayName,
					description,
					tables,
				}),
			},
			successMessage: i18n.t(DATABASE_SAVE_MESSAGE_SUCCESS),
			...thunkArgs,
		})
)
export const deleteDatabase = createAsyncThunk(
	'database/delete',
	async (
		{ useCaching, noBusySpinner, applicationId, databaseId } = {},
		thunkArgs
	) =>
		await doAsync({
			url: DELETE_DATABASE(applicationId, databaseId),
			useCaching,
			noBusySpinner,
			httpMethod: 'delete',
			successMessage: i18n.t(DATABASE_DELETE_MESSAGE_SUCCESS),
			...thunkArgs,
		})
)

export const confirmDatabaseDelete = createAsyncThunk(
	'database/confirmDatabaseDelete',
	async (data, { dispatch, getState } = {}) => {
		const dbs = selectAllDatabases(getState())
		const db = find(dbs, (db) => db.id === data.databaseId) || {}
		const dbName = db.name
		const conformationCallBack = () => {
			dispatch(deleteDatabase(data)).then((response) => {
				if (response && response.payload) {
					dispatch(fetchAllDatabases({ appId: '5fc1eb3ca15859e036aadc1d' }))
				}
			})
		}
		return await dispatch(
			dialogActions.addDialog({
				type: dialogTypes.CONFIRMATION_DIALOG,
				onOkay: conformationCallBack,
				data: {
					message: i18n.t(DATABASE_DELETE_MESSAGE, { name: dbName }),
				},
			})
		)
	}
)

export const updateDBConfig = createAsyncThunk(
	'connectDb/update',
	async (
		{
			useCaching,
			noBusySpinner,
			appId,
			databaseId,
			configurations,
			environmentType,
			platform,
			host,
			port,
			username,
			password,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: UPDATE_DB_CONFIG(appId, databaseId),
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					configurations,
					environmentType,
					platform,
					host,
					port,
					userName: username,
					password,
				}),
			},
			successMessage: i18n.t(DATABASE_CONFIG_SUCCESS),
			...thunkArgs,
		})
)

export const fetchDBConfiguration = createAsyncThunk(
	'databases/{databaseId}',
	async ({ useCaching, noBusySpinner, databaseId, appId } = {}, thunkArgs) =>
		await doAsync({
			url: UPDATE_DB_CONFIG(appId, databaseId),
			useCaching,
			noBusySpinner,
			errorMessage: i18n.t(DATABASE_LOADING_MESSAGE_ERROR),
			...thunkArgs,
		})
)
