import { combineReducers } from 'redux'
import Database from './Database'
import * as selectors from './database.selectors'
import * as asyncActions from './database.asyncActions'
import database from './database.slice'
import tables from './tables/tables.slice'
import * as baseSelector from './base.selector'

export const { fetchAllDatabases } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllDatabases, selectDatabaseFilter } = selectors

export const reducer = combineReducers({
	[database.name]: database.reducer,
	[tables.name]: tables.reducer,
})

export const { name } = baseSelector

// we export the component most likely to be desired by default
export default Database
