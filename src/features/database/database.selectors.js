import { isNil, isEqual, differenceWith, find } from 'lodash'
import { selectSlice as baseSelector } from './base.selector'

export const sliceName = 'database'

export const selectSlice = (state) => baseSelector(state)[sliceName]

export const selectAllDatabases = (state) => selectSlice(state).databases

export const selectDatabaseFilter = (state) => selectSlice(state).filter

export const selectDbAttributeTypes = (state) =>
	selectSlice(state).attributeTypes

export const selectDbConstraintsByAttributeType = (state, type) => {
	const attrTypes = selectDbAttributeTypes(state)
	const attr = find(attrTypes, (a) => a.value === type)
	if (!isNil(attr)) {
		return attr.constraints
	}
	return []
}

export const selectSelectedDatabaseIndex = (state) => {
	return selectSlice(state).selectedDatabaseIndex
}

export const selectSelectedTableIndex = (state) => {
	return selectSlice(state).selectedTableIndex
}

export const selectSelectedTable = (state) => {
	const tables = selectDatabaseTables(state)
	const tableIndex = selectSelectedTableIndex(state)
	if (!isNil(tableIndex) && tables.length > tableIndex) {
		return tables[tableIndex]
	}
	return null
}

export const selectDatabaseTables = (state) => {
	const dbIndex = selectSelectedDatabaseIndex(state)
	const allDbs = selectAllDatabases(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return allDbs[dbIndex].schemas[0].tables
	}
	return []
}

export const selectConstrainsKeyTypes = (state) => selectSlice(state).keyTypes

export const selectisDBModified = (state) => {
	const dbIndex = selectSlice(state).selectedDatabaseIndex
	const allDbs = selectAllDatabases(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return differenceWith(
			[selectSlice(state).databases[dbIndex]],
			[selectSlice(state).prevAllDatabase[dbIndex]],
			isEqual
		)
	}
	return {}
}

export const selectDatabase = (state) => {
	const dbIndex = selectSelectedDatabaseIndex(state)
	const allDbs = selectAllDatabases(state)
	if (!isNil(dbIndex) && allDbs.length > dbIndex) {
		return allDbs[dbIndex]
	}
	return []
}

export const getDBConfiguration = (state) => selectSlice(state).dbConfiguration
