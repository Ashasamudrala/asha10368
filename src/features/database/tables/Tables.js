import React, { Fragment, useEffect } from 'react'
import { isNil, findIndex, isEmpty, find } from 'lodash'
import TableChartIcon from '@material-ui/icons/TableChart'
import Button from '@material-ui/core/Button'
import Toolbar from '../components/dbtoolbar/DBToolbar'
import { useTranslation } from 'react-i18next'
import {
	DATABASE_TRANSLATIONS,
	TABLE_TEXT,
	TABLE_BUTTON,
} from '../../../utilities/constants'
import { useSelector, useDispatch } from 'react-redux'
import { selectEditItem } from './tables.selectors'
import { actions } from './tables.slice'
import { actions as dbActions } from '../database.slice'
import {
	selectDatabaseTables,
	selectSelectedTable,
	selectDbAttributeTypes,
	selectisDBModified,
	selectDatabase,
} from '../database.selectors'
import DBTable from '../components/DBTable/DBTable'
import { DbCanvas } from '../components/canvas/DBCanvas'
import DBTablePanel from '../components/DBTablePanel/DBTablePanel'
import { isParentUntil } from '../../../utilities/utilities'
import { saveDatabase } from '../database.asyncActions'
import {
	conformTableDelete,
	conformTableAttributeDelete,
	setAttributeKey,
	updateTableAttributeAsync,
	updateTableAsync,
} from './tables.asyncActions'
import './tables.scss'

const { addTable, addAttribute, updateTableAttributeConstraints } = dbActions

export function Tables(props) {
	const selectedTable = useSelector(selectSelectedTable)
	const editItem = useSelector(selectEditItem)
	const attributeTypes = useSelector(selectDbAttributeTypes)
	const tablesData = useSelector(selectDatabaseTables)
	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	const isDBModified = useSelector(selectisDBModified)
	const selectedDatabase = useSelector(selectDatabase)

	const dispatch = useDispatch()

	useEffect(() => {
		const listener = (event) => {
			if (
				!isNil(event.srcElement) &&
				!isParentUntil(event.srcElement, [
					'MuiPopover-root',
					'database-container',
					'database-container-edit',
					'MuiDialog-root',
				])
			) {
				dispatch(actions.setEditTableView(null))
			}
		}
		document.addEventListener('click', listener)
	}, [])

	const getSelectedTableId = () => {
		if (!isNil(selectedTable)) {
			return selectedTable.id
		}
		return null
	}

	const getTableHeight = (table) => {
		const rowsHeight = table.attributes.length * 31
		const headerHeight = 33
		const buttonHeight = 32 + (table.attributes.length - 1)
		if (editItem === table.id) {
			return rowsHeight + headerHeight + buttonHeight
		}
		return rowsHeight + headerHeight
	}

	const getLinksOfTable = (table) => {
		const links = []
		for (let i = 0, iLen = table.attributes.length; i < iLen; i++) {
			if (!isNil(table.attributes[i].reference)) {
				const targetTable = find(
					tablesData,
					(t) => t.name === table.attributes[i].reference.targetModel
				)
				const targetAttrIndex = findIndex(
					targetTable.attributes,
					(a) => a.name === table.attributes[i].reference.column
				)
				const targetTableId = targetTable.id
				links.push({
					targetTableId,
					targetAttrIndex,
					sourceTableId: table.id,
					sourceAttrIndex: i,
					targetTableName: targetTable.name,
					targetAttrName: targetTable.attributes[targetAttrIndex].name,
					sourceTableName: table.name,
					sourceAttrName: table.attributes[i].name,
				})
			}
		}
		return links
	}

	const getTooltipForRelation = (data) => {
		return (
			data.sourceTableName +
			':' +
			data.sourceAttrName +
			' -> ' +
			data.targetTableName +
			':' +
			data.targetAttrName
		)
	}

	const getCanvasTables = () => {
		return tablesData.map((item, tableIndex) => {
			return {
				id: item.id,
				width: 230,
				height: getTableHeight(item),
				x: (tableIndex + 1) * 300,
				y: 20,
				links: getLinksOfTable(item),
				data: item,
			}
		})
	}

	/**
	 * Delete attributes from database table when we click delete icon.
	 */
	const handleDeleteAttribute = (tableIndex, attrIndex) => {
		dispatch(conformTableAttributeDelete({ tableIndex, attrIndex }))
	}

	/**
	 * Add attributes to the database table based on selecting.
	 */
	const handleAddAttribute = (tableIndex) => {
		dispatch(addAttribute({ tableIndex }))
	}

	const handleTableChange = (table, tableIndex) => {
		// dispatch update table
		dispatch(updateTableAsync({ table, tableIndex }))
	}

	const onTableAttributeChange = (attr, tableIndex, attrIndex) => {
		// dispatch update table attribute
		dispatch(updateTableAttributeAsync({ attr, tableIndex, attrIndex }))
	}

	const handleSelectTable = (tb) => {
		const index = findIndex(tablesData, (table) => table.id === tb.id)
		props.setSelectedTableDetails(index)
	}

	const handleSelectRelation = (data) => {
		// todo
	}

	const handleCanvasClick = () => {
		dispatch(actions.setEditTableView(null))
	}

	const handleSetEditTable = (id) => {
		dispatch(actions.setEditTableView(id))
	}

	const handlePanelClose = () => {
		props.setSelectedTableDetails(null)
	}

	const handleAddNewTable = () => {
		dispatch(addTable())
	}

	const handleDeleteTable = (tableIndex) => {
		dispatch(conformTableDelete({ tableIndex }))
	}

	const handleSetKey = (tableIndex, attrIndex, keyType) => {
		dispatch(setAttributeKey({ tableIndex, attrIndex, keyType }))
	}

	const handleSaveDatabase = () => {
		dispatch(
			saveDatabase({
				appId: selectedDatabase.applicationId,
				databaseId: selectedDatabase.id,
				schemaId: selectedDatabase.schemas[0].id,
				name: selectedDatabase.schemas[0].name,
				displayName: selectedDatabase.schemas[0].displayName,
				description: selectedDatabase.schemas[0].description,
				tables: selectedDatabase.schemas[0].tables,
			})
		)
	}

	const handleUpdateTableAttributeConstraints = (
		constraints,
		tableIndex,
		attrIndex
	) => {
		dispatch(
			updateTableAttributeConstraints({ constraints, tableIndex, attrIndex })
		)
	}

	const renderToolbarComponent = () => {
		return (
			<Toolbar
				drawer={!isNil(selectedTable)}
				handleAddNewTable={handleAddNewTable}
				tableData={tablesData}
				isDBModified={isEmpty(isDBModified)}
				handleSaveDatabase={handleSaveDatabase}
			/>
		)
	}

	const renderTableComponent = (table, index) => {
		return (
			<foreignObject
				width={table.width + 'px'}
				height={table.height + 'px'}
				x={'0px'}
				y={'0px'}
			>
				<DBTable
					table={table.data}
					tableIndex={index}
					attributeTypes={attributeTypes}
					isEditMode={editItem === table.id}
					setEditMode={handleSetEditTable}
					onChange={handleTableChange}
					onAttrChange={onTableAttributeChange}
					onAddAttr={handleAddAttribute}
					onDeleteAttr={handleDeleteAttribute}
					onDelete={handleDeleteTable}
					onSetKey={handleSetKey}
				/>
			</foreignObject>
		)
	}

	const renderCanvas = () => {
		return (
			<div className='DbCanvasParentContainer'>
				<DbCanvas
					list={getCanvasTables()}
					selected={getSelectedTableId()}
					onSelectNode={handleSelectTable}
					onSelectLink={handleSelectRelation}
					onCanvasClick={handleCanvasClick}
					onRenderItem={renderTableComponent}
					getLinkTooltip={getTooltipForRelation}
				/>
			</div>
		)
	}

	const renderNoTablesView = () => {
		return (
			<div className='table-page'>
				<span className='table' role='button'>
					<TableChartIcon />
				</span>
				<div className='table-text'>{t(TABLE_TEXT)}</div>
				<div>
					<Button className='add-new-table' onClick={handleAddNewTable}>
						{t(TABLE_BUTTON)}
					</Button>
				</div>
			</div>
		)
	}

	const renderProperties = () => {
		if (!isNil(selectedTable)) {
			const tableIndex = findIndex(
				tablesData,
				(table) => table.id === selectedTable.id
			)
			return (
				<DBTablePanel
					drawerName='table-properties-panel'
					tableData={selectedTable}
					drawer={!isNil(selectedTable)}
					attributeTypes={attributeTypes}
					onTableDelete={() => handleDeleteTable(tableIndex)}
					onTableChange={(data) => handleTableChange(data, tableIndex)}
					onAttributeChange={(data, attrIndex) =>
						onTableAttributeChange(data, tableIndex, attrIndex)
					}
					onAddAttribute={() => handleAddAttribute(tableIndex)}
					onClose={handlePanelClose}
					onAttributeConstraintChange={(d, attrIndex) =>
						handleUpdateTableAttributeConstraints(d, tableIndex, attrIndex)
					}
					onDeleteTableAttribute={(attrIndex) =>
						handleDeleteAttribute(tableIndex, attrIndex)
					}
					onAttributeKeyChange={(key, attrIndex) =>
						handleSetKey(tableIndex, attrIndex, key)
					}
				/>
			)
		}
		return null
	}

	const renderTablesView = () => {
		return (
			<Fragment>
				{renderToolbarComponent()}
				{renderCanvas()}
				{renderProperties()}
			</Fragment>
		)
	}

	const renderView = () => {
		if (tablesData.length === 0) {
			return renderNoTablesView()
		}
		return renderTablesView()
	}

	return renderView()
}
