import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	editItem: null,
}

const slice = createSlice({
	name: 'tables',
	initialState,
	reducers: {
		// synchronous actions
		setEditTableView(state, action) {
			state.editItem = action.payload
		},
	},
	extraReducers: {},
})

export default slice

export const { name, actions, reducer } = slice
