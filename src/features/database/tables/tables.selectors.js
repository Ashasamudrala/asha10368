import slice from './tables.slice'
import { selectSlice as baseSelector } from '../base.selector'

export const selectSlice = (state) => baseSelector(state)[slice.name]

export const selectEditItem = (state) => selectSlice(state).editItem
