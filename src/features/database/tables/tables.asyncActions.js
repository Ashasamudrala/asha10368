import { createAsyncThunk } from '@reduxjs/toolkit'
import { actions as dialogActions } from '../../../infrastructure/dialogs/dialogs.slice'
import * as dialogTypes from '../../../infrastructure/dialogs/dialogs.constants'
import { actions as dbActions } from '../database.slice'
import {
	CONFIRM_MESSAGE_DELETE_TABLE,
	CONFIRM_MESSAGE_DELETE_ATTRIBUTE,
	FOREIGN_KEY_NO_PRIMARY_KEY,
	PRIMARY_KEY_UPDATE_ERROR,
	PRIMARY_KEY_CHANGED_TO_OTHER,
	PRIMARY_KET_DELETE_ERROR,
	PRIMARY_KEY_UPDATE_TYPE,
	DUPLICATE_COLUMN_NAME_ERROR_TITLE,
	DUPLICATE_TABLE_NAME_ERROR_TITLE,
	DUPLICATE_COLUMN_NAME_ERROR_CONTENT,
	DUPLICATE_TABLE_NAME_ERROR_CONTENT,
	PRIMARY_KEY_UPDATE_TYPE_ERROR,
	UNIQUE_KEY_UPDATE_TYPE_CONFIRMATION,
	FOREIGN_KEY_UPDATE_TYPE_CONFIRMATION,
} from '../../../utilities/constants'
import i18n from 'i18next'
import {
	selectDatabaseTables,
	selectDbConstraintsByAttributeType,
} from '../database.selectors'
import { findIndex, isNil } from 'lodash'

export const conformTableDelete = createAsyncThunk(
	'tables/confirmTableDelete',
	async (data, { dispatch, getState } = {}) => {
		const tables = selectDatabaseTables(getState())
		const tableName = tables[data.tableIndex].name
		return await dispatch(
			dialogActions.addDialog({
				type: dialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => dispatch(dbActions.deleteTable(data)),
				data: {
					message: i18n.t(CONFIRM_MESSAGE_DELETE_TABLE, { name: tableName }),
				},
			})
		)
	}
)

export const conformTableAttributeDelete = createAsyncThunk(
	'tables/confirmTableAttributeDelete',
	async (data, { dispatch, getState } = {}) => {
		const tables = selectDatabaseTables(getState())
		const attr = tables[data.tableIndex].attributes[data.attrIndex]
		if (attr.constraints.primary) {
			return await dispatch(
				dialogActions.addDialog({
					type: dialogTypes.ALERT,
					data: {
						message: i18n.t(PRIMARY_KET_DELETE_ERROR),
					},
				})
			)
		}
		return await dispatch(
			dialogActions.addDialog({
				type: dialogTypes.CONFIRMATION_DIALOG,
				onOkay: () => dispatch(dbActions.deleteTableAttribute(data)),
				data: {
					message: i18n.t(CONFIRM_MESSAGE_DELETE_ATTRIBUTE, {
						name: attr.name,
					}),
				},
			})
		)
	}
)

const setAttributeForeignKey = createAsyncThunk(
	'tables/setAttributeForeignKey',
	async (data, { dispatch, getState } = {}) => {
		const tables = selectDatabaseTables(getState())
		const primaryKeyIndex = findIndex(
			tables[data.tableIndex].attributes,
			(attr) => attr.constraints.primary
		)
		if (primaryKeyIndex === -1) {
			return await dispatch(
				dialogActions.addDialog({
					type: dialogTypes.ALERT,
					data: {
						message: i18n.t(FOREIGN_KEY_NO_PRIMARY_KEY),
					},
				})
			)
		} else {
			const okayCallBack = (result) => {
				dispatch(
					dbActions.setTableAttributeForeignKey({
						...result,
						...data,
						primaryKeyIndex,
					})
				)
			}
			return await dispatch(
				dialogActions.addDialog({
					type: dialogTypes.CONNECT_FOREIGN_KEY,
					onOkay: okayCallBack,
					data: {
						tablesData: tables,
						...data,
					},
				})
			)
		}
	}
)

export const setAttributeKey = createAsyncThunk(
	'tables/setAttributeKey',
	async (data, { dispatch, getState } = {}) => {
		const key = data.keyType
		const tables = selectDatabaseTables(getState())
		const attributes = tables[data.tableIndex].attributes
		const oldPrimaryKey = findIndex(attributes, (a) => a.constraints.primary)
		if (oldPrimaryKey === data.attrIndex && key !== 'PrimaryKey') {
			return await dispatch(
				dialogActions.addDialog({
					type: dialogTypes.ALERT,
					data: {
						message: i18n.t(PRIMARY_KEY_UPDATE_ERROR),
					},
				})
			)
		}

		switch (key) {
			case 'None':
				return dispatch(
					dbActions.updateTableAttributeConstraints({
						tableIndex: data.tableIndex,
						attrIndex: data.attrIndex,
						constraints: {
							unique: false,
							primary: false,
						},
					})
				)
			case 'PrimaryKey':
				if (oldPrimaryKey !== data.attrIndex) {
					const okayFunction = () =>
						dispatch(
							dbActions.updateTableAttributeConstraints({
								tableIndex: data.tableIndex,
								attrIndex: data.attrIndex,
								constraints: {
									unique: false,
									primary: true,
									required: true,
								},
							})
						)
					return await dispatch(
						dialogActions.addDialog({
							type: dialogTypes.CONFIRMATION_DIALOG,
							onOkay: okayFunction,
							data: {
								message: i18n.t(PRIMARY_KEY_CHANGED_TO_OTHER),
							},
						})
					)
				}
				break
			case 'UniqueKey':
				return dispatch(
					dbActions.updateTableAttributeConstraints({
						tableIndex: data.tableIndex,
						attrIndex: data.attrIndex,
						constraints: {
							unique: true,
							primary: false,
						},
					})
				)
			case 'ForeignKey':
				return dispatch(
					setAttributeForeignKey({
						tableIndex: data.tableIndex,
						attrIndex: data.attrIndex,
					})
				)
			default:
		}
	}
)

export const updateTableAsync = createAsyncThunk(
	'tables/updateTableAsync',
	async (data, { dispatch, getState } = {}) => {
		const tables = selectDatabaseTables(getState())
		const okayFunction = () => dispatch(dbActions.updateTable(data))
		if (!isNil(data.table.name)) {
			const sameNameIndex = findIndex(tables, (t) => t.name === data.table.name)
			if (sameNameIndex !== -1 && sameNameIndex !== data.tableIndex) {
				return await dispatch(
					dialogActions.addDialog({
						type: dialogTypes.ALERT,
						title: i18n.t(DUPLICATE_TABLE_NAME_ERROR_TITLE),
						data: {
							message: i18n.t(DUPLICATE_TABLE_NAME_ERROR_CONTENT),
						},
					})
				)
			}
		}
		okayFunction()
	}
)

export const updateTableAttributeAsync = createAsyncThunk(
	'tables/updateTableAttributeAsync',
	async (data, { dispatch, getState } = {}) => {
		const tables = selectDatabaseTables(getState())
		const attribute = tables[data.tableIndex].attributes[data.attrIndex]
		const okayFunction = () => dispatch(dbActions.updateTableAttribute(data))
		if (!isNil(data.attr.name)) {
			const sameNameIndex = findIndex(
				tables[data.tableIndex].attributes,
				(a) => a.name === data.attr.name
			)
			if (sameNameIndex !== -1 && sameNameIndex !== data.attrIndex) {
				return await dispatch(
					dialogActions.addDialog({
						type: dialogTypes.ALERT,
						title: i18n.t(DUPLICATE_COLUMN_NAME_ERROR_TITLE),
						data: {
							message: i18n.t(DUPLICATE_COLUMN_NAME_ERROR_CONTENT),
						},
					})
				)
			}
		}

		if (!isNil(data.attr.type)) {
			const constraintsConfig = selectDbConstraintsByAttributeType(
				getState(),
				data.attr.type
			)
			let hasPrimary = false
			let hasUnique = false
			for (let i = 0, iLen = constraintsConfig.length; i < iLen; i++) {
				if (constraintsConfig[i].name === 'primary') {
					hasPrimary = true
				} else if (constraintsConfig[i].name === 'unique') {
					hasUnique = true
				}
			}

			if (attribute.constraints.primary) {
				if (!hasPrimary) {
					return await dispatch(
						dialogActions.addDialog({
							type: dialogTypes.ALERT,
							data: {
								message: i18n.t(PRIMARY_KEY_UPDATE_TYPE_ERROR),
							},
						})
					)
				}
				return await dispatch(
					dialogActions.addDialog({
						type: dialogTypes.CONFIRMATION_DIALOG,
						onOkay: okayFunction,
						data: {
							message: i18n.t(PRIMARY_KEY_UPDATE_TYPE),
						},
					})
				)
			}
			if (attribute.constraints.unique) {
				if (!hasUnique) {
					return await dispatch(
						dialogActions.addDialog({
							type: dialogTypes.CONFIRMATION_DIALOG,
							onOkay: () => {
								dispatch(
									dbActions.updateTableAttributeConstraints({
										tableIndex: data.tableIndex,
										attrIndex: data.attrIndex,
										constraints: {
											unique: false,
										},
									})
								)
								okayFunction()
							},
							data: {
								message: i18n.t(UNIQUE_KEY_UPDATE_TYPE_CONFIRMATION),
							},
						})
					)
				}
			}
			if (!isNil(attribute.reference)) {
				if (!hasPrimary) {
					return await dispatch(
						dialogActions.addDialog({
							type: dialogTypes.CONFIRMATION_DIALOG,
							onOkay: () => {
								data.reference = null
								okayFunction()
							},
							data: {
								message: i18n.t(FOREIGN_KEY_UPDATE_TYPE_CONFIRMATION),
							},
						})
					)
				}
			}
		}
		okayFunction()
	}
)
