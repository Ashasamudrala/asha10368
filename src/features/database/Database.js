import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Button from '@material-ui/core/Button'
import TableChartIcon from '@material-ui/icons/TableChart'
import {
	DATABASE_TRANSLATIONS,
	DATABASE_TEXT,
	DATABASE_BUTTON,
} from '../../utilities/constants'
import './Database.scss'
import { useTranslation } from 'react-i18next'
import {
	fetchAllDatabases,
	fetchAttributeTypes,
	confirmDatabaseDelete,
	createDatabaseAsync,
} from './database.asyncActions'

import {
	selectAllDatabases,
	selectSelectedDatabaseIndex,
	selectSelectedTableIndex,
} from './database.selectors'
import { actions } from './database.slice'
import { DBAccordion } from './components/DBAccordion/DBAccordion'
import { Tables } from './tables/Tables'

export default function Database() {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const dispatch = useDispatch()
	const databases = useSelector(selectAllDatabases)
	const selectedDatabaseIndex = useSelector(selectSelectedDatabaseIndex)
	const selectedTableIndex = useSelector(selectSelectedTableIndex)

	const onAddDatabase = () => {
		dispatch(createDatabaseAsync({ appId: '5fc1eb3ca15859e036aadc1d' }))
	}

	useEffect(() => {
		dispatch(fetchAttributeTypes())
		dispatch(
			fetchAllDatabases({
				appId: '5fc1eb3ca15859e036aadc1d',
			})
		)
	}, [dispatch]) // eslint-disable-line react-hooks/exhaustive-deps

	const setSelectedDatabaseIndex = (index) => {
		dispatch(actions.updateSelectedDatabaseIndex(index))
	}

	const setSelectedTableIndex = (index) => {
		dispatch(actions.updateSelectedTableIndex(index))
	}

	const deleteDatabaseById = (databaseId) => {
		dispatch(
			confirmDatabaseDelete({
				applicationId: '5fc1eb3ca15859e036aadc1d',
				databaseId: databaseId,
			})
		)
	}

	return (
		<>
			{databases && databases.length !== 0 && (
				<div className='db-container'>
					<div className='right'>
						<DBAccordion
							databases={databases}
							selectedDatabaseIndex={selectedDatabaseIndex}
							selectedTableIndex={selectedTableIndex}
							setSelectedDatabaseIndex={setSelectedDatabaseIndex}
							setSelectedTableIndex={setSelectedTableIndex}
							onDeleteDatabase={(databaseId) => deleteDatabaseById(databaseId)}
							onAddDatabase={onAddDatabase}
						/>
					</div>
					<div className='left'>
						<Tables setSelectedTableDetails={setSelectedTableIndex} />
					</div>
				</div>
			)}
			{databases.length === 0 && (
				<div className='database-page'>
					<span className='data-base' role='button'>
						<TableChartIcon />
					</span>
					<div className='data-base-text'>{t(DATABASE_TEXT)}</div>
					<div>
						<Button className='add-new-database' onClick={onAddDatabase}>
							{t(DATABASE_BUTTON)}
						</Button>
					</div>
				</div>
			)}
		</>
	)
}
