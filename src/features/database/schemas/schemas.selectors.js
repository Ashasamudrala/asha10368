import slice from './schemas.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllSchemas = (state) => selectSlice(state).allSchemas

export const selectSchemasFilter = (state) => selectSlice(state).filter
