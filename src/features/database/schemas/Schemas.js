import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectAllSchemas, selectSchemasFilter } from './schemas.selectors'
import { actions } from './schemas.slice'
import { fetchAllSchemas } from './schemas.asyncActions'
import BusyIndicator from '../../widgets/busyIndicator'

const { updateFilter } = actions

export default function Schemas() {
	const schemas = useSelector(selectAllSchemas)
	const filter = useSelector(selectSchemasFilter)

	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(fetchAllSchemas())
	}, [dispatch])

	return (
		<div>
			<h2>Schemas</h2>
			<input
				type='text'
				value={filter}
				onChange={(e) => dispatch(updateFilter(e.target.value))}
				placeholder='Filter by...'
			/>
			<BusyIndicator>
				<ul>
					{schemas &&
						schemas
							.filter((item) => (filter ? item.includes(filter) : true))
							.map((item) => <li key={item}>{item}</li>)}
				</ul>
			</BusyIndicator>
		</div>
	)
}
