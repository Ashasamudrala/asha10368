import Schemas from './Schemas'
import * as selectors from './schemas.selectors'
import * as asyncActions from './schemas.asyncActions'
import slice from './schemas.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllSchemas } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllSchemas, selectSchemasFilter } = selectors

// we export the component most likely to be desired by default
export default Schemas
