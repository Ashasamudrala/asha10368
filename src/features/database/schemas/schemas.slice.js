import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './schemas.asyncActions'

const initialState = {
	allSchemas: [],
	filter: '',
}

const slice = createSlice({
	name: 'schemas',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllSchemas.fulfilled]: (state, action) => {
			state.allSchemas = action.payload
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
