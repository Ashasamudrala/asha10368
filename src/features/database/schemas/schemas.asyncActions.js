import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllSchemas = createAsyncThunk(
	'schemas/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: 'schemas',
			useCaching,
			noBusySpinner,
			successMessage: 'Schemas loaded',
			errorMessage: 'Unable to load schemas. Please try again later.',
			stubSuccess: ['Dummy item 1', 'Dummy item 2'],
			...thunkArgs,
		})
)
