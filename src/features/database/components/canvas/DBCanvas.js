import React from 'react'
import PropTypes from 'prop-types'
import './DBCanvas.scss'

import { isFunction, isNil, isArray } from 'lodash'

import * as d3 from 'd3'

const canvasDefaults = {
	graphSize: 100000,
	gridDotSize: 5,
	gridDotOffset: 100,
	gridDotColor: 'lightgray',
	graphMinZoom: 0.15,
	graphMaxZoom: 1.5,
	nodeBoundary: 40,
}

export class DbCanvas extends React.Component {
	constructor(props) {
		super(props)
		this.transform = isNil(props.transform) ? d3.zoomIdentity : props.transform
		this.state = {
			selectedNode: null,
		}
		this.zoom = d3
			.zoom()
			.scaleExtent([canvasDefaults.graphMinZoom, canvasDefaults.graphMaxZoom])
		this.handleItemSelect = this.handleItemSelect.bind(this)
		this.handleItemDragStart = this.handleItemDragStart.bind(this)
		this.handleItemDrag = this.handleItemDrag.bind(this)
		this.handleItemDragEnd = this.handleItemDragEnd.bind(this)
		this.handleStartTransform = this.handleStartTransform.bind(this)
		this.handleSetTransform = this.handleSetTransform.bind(this)
		this.handleZoomEnd = this.handleZoomEnd.bind(this)
		this.handleSetZoom = this.handleSetZoom.bind(this)
		this.handleCanvasClick = this.handleCanvasClick.bind(this)
		this.handleSelectLink = this.handleSelectLink.bind(this)
	}

	componentDidMount() {
		this.bindGraphEventListeners()
		this.bindItemRepositionEvents()
	}

	componentDidUpdate() {
		this.bindGraphEventListeners()
		this.bindItemRepositionEvents()
	}

	bindGraphEventListeners() {
		d3.select(this.viewContainer)
			.on('touchstart', this.handleContainZoom)
			.on('touchmove', this.handleContainZoom)

		d3.select(this.svg)
			.on('mousedown', this.handleCanvasMouseDown)
			.call(this.zoom)

		this.zoom.on('start', this.handleStartTransform)
		this.zoom.on('zoom', this.handleSetTransform)
		this.zoom.on('end', this.handleZoomEnd)
	}

	bindItemRepositionEvents() {
		const entities = d3.select(this.entities)
		const list = entities.selectAll('.ITEM_REF')
		list
			// .on('click', this.handleItemMouseDown)
			.call(
				d3
					.drag()
					.on('start', this.handleItemDragStart)
					.on('drag', this.handleItemDrag)
					.on('end', this.handleItemDragEnd)
			)
	}

	buildSubData() {
		const arrivals = {}
		const ref = {}
		const getItemForId = (id) => {
			return ref[id]
		}
		const getArrivalsListForId = (id) => {
			return arrivals[id].list
		}

		const getTargetXY = (sourceId, targetId) => {
			const arrival = arrivals[targetId]
			const targetItem = getItemForId(targetId)
			const arrivalsGap = targetItem.height / (arrival.list.length + 1)

			const topIndex = arrival.topList.indexOf(sourceId)
			if (topIndex !== -1) {
				const topGap =
					canvasDefaults.nodeBoundary / (arrival.topList.length + 1)
				return {
					x: targetItem.x - topGap * (topIndex + 1),
					y: targetItem.y + arrivalsGap * (topIndex + 1),
				}
			}
			const bottomIndex = arrival.bottomList.indexOf(sourceId)
			const bottomGap =
				canvasDefaults.nodeBoundary / (arrival.bottomList.length + 1)
			return {
				x:
					targetItem.x -
					bottomGap * (arrival.bottomList.length - bottomIndex + 1),
				y:
					targetItem.y +
					arrivalsGap * (bottomIndex + arrival.topList.length + 1),
			}
		}

		const list = isArray(this.props.list) ? this.props.list : []
		for (let i = 0, iLen = list.length; i < iLen; i++) {
			const item = list[i]
			const dependencies = isArray(item.links) ? item.links : []
			ref[item.id] = item
			for (let j = 0, jLen = dependencies.length; j < jLen; j++) {
				if (isNil(arrivals[dependencies[j].targetTableId])) {
					arrivals[dependencies[j].targetTableId] = {
						list: [],
						idToTypeMap: {},
						topList: [],
						bottomList: [],
					}
				}
				arrivals[dependencies[j].targetTableId].list.push(item.id)
			}
		}

		for (const id in arrivals) {
			if (!isNil(arrivals[id])) {
				const targetItem = getItemForId(id)
				const items = arrivals[id].list
				for (let i = 0, iLen = items.length; i < iLen; i++) {
					const sourceItem = getItemForId(items[i])
					if (
						sourceItem.y + sourceItem.height / 2 <
						targetItem.y + targetItem.height / 2
					) {
						// towards top
						arrivals[id].topList.push(sourceItem.id)
					} else {
						// towards bottom
						arrivals[id].bottomList.push(sourceItem.id)
					}
				}
			}
		}

		return { getItemForId, getArrivalsListForId, getTargetXY }
	}

	buildDependentsFlow(sourceItem, getTargetXY) {
		const data = {
			idToTypeMap: {},
			bottomRight: 0,
			bottomLeft: 0,
			topRight: 0,
			topLeft: 0,
		}
		const bottomRight = []
		const bottomLeft = []
		const topRight = []
		const topLeft = []
		const dependencies = isArray(sourceItem.links) ? sourceItem.links : []
		for (let i = 0, iLen = dependencies.length; i < iLen; i++) {
			const targetItem = getTargetXY(
				sourceItem.id,
				dependencies[i].targetTableId
			)
			if (sourceItem.y + sourceItem.height / 2 < targetItem.y) {
				// target item towards bottom
				if (sourceItem.x + sourceItem.width <= targetItem.x) {
					// target item towards right
					data.idToTypeMap[dependencies[i].targetTableId] = 'BOTTOM_RIGHT'
					bottomRight.push(dependencies[i])
					data.bottomRight++
				} else {
					// target item towards left
					data.idToTypeMap[dependencies[i].targetTableId] = 'BOTTOM_LEFT'
					bottomLeft.push(dependencies[i])
					data.bottomLeft++
				}
			} else {
				// target item towards top
				if (sourceItem.x + sourceItem.width <= targetItem.x) {
					// target item towards right
					data.idToTypeMap[dependencies[i].targetTableId] = 'TOP_RIGHT'
					topRight.push(dependencies[i])
					data.topRight++
				} else {
					// target item towards left
					data.idToTypeMap[dependencies[i].targetTableId] = 'TOP_LEFT'
					topLeft.push(dependencies[i])
					data.topLeft++
				}
			}
		}
		topRight.sort((a, b) => {
			const aTarget = getTargetXY(sourceItem.id, a.targetTableId)
			const bTarget = getTargetXY(sourceItem.id, b.targetTableId)
			return aTarget.x > bTarget.x ? -1 : 1
		})
		bottomRight.sort((a, b) => {
			const aTarget = getTargetXY(sourceItem.id, a.targetTableId)
			const bTarget = getTargetXY(sourceItem.id, b.targetTableId)
			return aTarget.x > bTarget.x ? -1 : 1
		})
		topLeft.sort((a, b) => {
			const aTarget = getTargetXY(sourceItem.id, a.targetTableId)
			const bTarget = getTargetXY(sourceItem.id, b.targetTableId)
			return aTarget.x < bTarget.x ? -1 : 1
		})
		bottomLeft.sort((a, b) => {
			const aTarget = getTargetXY(sourceItem.id, a.targetTableId)
			const bTarget = getTargetXY(sourceItem.id, b.targetTableId)
			return aTarget.x < bTarget.x ? -1 : 1
		})
		data.orderedList = topLeft.concat(topRight, bottomRight, bottomLeft)
		return data
	}

	getLines() {
		const linesList = []
		const subDataFunctions = this.buildSubData()
		const list = isArray(this.props.list) ? this.props.list : []
		for (let i = 0, iLen = list.length; i < iLen; i++) {
			const sourceItem = list[i]
			const dependenciesData = this.buildDependentsFlow(
				sourceItem,
				subDataFunctions.getTargetXY
			)
			const dependencies = dependenciesData.orderedList
			const gap = sourceItem.height / (dependencies.length + 1)
			const topGap =
				canvasDefaults.nodeBoundary /
				(dependenciesData.topLeft + dependenciesData.topRight + 1)
			const bottomGap =
				canvasDefaults.nodeBoundary /
				(dependenciesData.bottomLeft + dependenciesData.bottomRight + 1)
			const bottomRightGap =
				canvasDefaults.nodeBoundary / (dependenciesData.bottomRight + 1)
			const bottomLeftGap =
				canvasDefaults.nodeBoundary / (dependenciesData.bottomLeft + 1)
			const topRightGap =
				canvasDefaults.nodeBoundary / (dependenciesData.topRight + 1)
			const topLeftGap =
				canvasDefaults.nodeBoundary / (dependenciesData.topLeft + 1)
			let topCount = 0
			let bottomCount = 0
			let bottomRightCount = 0
			let bottomLeftCount = 0
			let topRightCount = 0
			let topLeftCount = 0
			for (let j = 0, jLen = dependencies.length; j < jLen; j++) {
				let path = ''
				const targetItem = subDataFunctions.getItemForId(
					dependencies[j].targetTableId
				)
				const targetXY = subDataFunctions.getTargetXY(
					list[i].id,
					dependencies[j].targetTableId
				)
				const targetX = targetXY.x
				const targetY = targetXY.y

				// source start line
				let xGap = 0
				const type = dependenciesData.idToTypeMap[dependencies[j].targetTableId]
				if (type === 'TOP_RIGHT' || type === 'TOP_LEFT') {
					xGap = topGap * (topCount + 1)
					topCount++
				} else {
					xGap =
						bottomGap *
						(dependenciesData.bottomLeft +
							dependenciesData.bottomRight -
							bottomCount +
							1)
					bottomCount++
				}
				const ySource = sourceItem.y + gap * (j + 1)
				let x1 = sourceItem.x + sourceItem.width
				const y1 = ySource
				const x2 = sourceItem.x + sourceItem.width + xGap
				let y2 = ySource
				path += 'M' + x1 + ' ' + y1 + ' L' + x2 + ' ' + y2 + ' '

				x1 = x2
				switch (type) {
					case 'TOP_RIGHT':
						y2 = sourceItem.y - topRightGap * (topRightCount + 1)
						if (targetY <= y1 && y2 <= targetY) {
							y2 = targetY
						}
						topRightCount++
						break
					case 'TOP_LEFT':
						y2 = sourceItem.y - topLeftGap * (topLeftCount + 1)
						topLeftCount++
						break
					case 'BOTTOM_LEFT':
						y2 =
							sourceItem.y +
							sourceItem.height +
							bottomLeftGap * (bottomLeftCount + 1)
						bottomLeftCount++
						break
					case 'BOTTOM_RIGHT':
						y2 =
							sourceItem.y +
							sourceItem.height +
							bottomRightGap * (bottomRightCount + 1)
						if (targetY >= y1 && y2 >= targetY) {
							y2 = targetY
						}
						bottomRightCount++
						break
					default:
				}
				path += 'M' + x1 + ' ' + y1 + ' L' + x2 + ' ' + y2 + ' '

				if (y2 === targetY) {
					path += 'M' + x2 + ' ' + y2 + ' L' + targetX + ' ' + targetY + ' '
				} else {
					path += 'M' + x2 + ' ' + y2 + ' L' + targetX + ' ' + y2 + ' '
					path +=
						'M' + targetX + ' ' + targetY + ' L' + targetX + ' ' + y2 + ' '
				}

				// destination end line
				path +=
					'M' +
					targetItem.x +
					' ' +
					targetY +
					' L' +
					targetX +
					' ' +
					targetY +
					'Z'

				linesList.push({
					data: dependencies[j],
					path,
				})
			}
		}
		return linesList
	}

	getRenderRectangleContent(item, i) {
		if (isFunction(this.props.onRenderItem)) {
			return this.props.onRenderItem(item, i)
		}
		return null
	}

	getLinkTooltip(data) {
		if (isFunction(this.props.getLinkTooltip)) {
			return this.props.getLinkTooltip(data)
		}
		return null
	}

	handleItemSelect(e, item) {
		e.stopPropagation()
		e.preventDefault()
		if (isFunction(this.props.onSelectNode)) {
			this.props.onSelectNode(item)
		}
	}

	handleSelectLink(e, data) {
		e.stopPropagation()
		e.preventDefault()
		if (isFunction(this.props.onSelectLink)) {
			this.props.onSelectLink(data)
		}
	}

	handleItemDragStart(_, i) {
		this.setState({
			selectedNode: this.props.list[i],
		})
	}

	handleItemDrag() {
		if (!isNil(this.state.selectedNode)) {
			this.setState({
				selectedNode: {
					...this.state.selectedNode,
					x: this.state.selectedNode.x + d3.event.dx,
					y: this.state.selectedNode.y + d3.event.dy,
				},
			})
		}
	}

	handleItemDragEnd() {
		if (
			!isNil(this.state.selectedNode) &&
			isFunction(this.props.onReposition)
		) {
			this.props.onReposition(
				this.state.selectedNode.id,
				this.state.selectedNode.x,
				this.state.selectedNode.y
			)
		}
		this.setState({
			selectedNode: null,
		})
	}

	handleStartTransform() {
		if (
			d3.event.transform.x !== this.transform.x &&
			d3.event.transform.y !== this.transform.y
		) {
			this.handleSetZoom(this.transform.k, this.transform.x, this.transform.y)
		}
	}

	handleSetTransform() {
		this.transform = d3.event.transform
		d3.select(this.view).attr('transform', d3.event.transform.toString())
	}

	handleZoomEnd() {
		if (isFunction(this.props.onTransform)) {
			this.props.onTransform(this.transform)
		}
	}

	handleSetZoom(k = 1, x = 0, y = 0) {
		const t = d3.zoomIdentity.translate(x, y).scale(k)
		d3.select(this.svg).call(this.zoom.transform, t)
	}

	handleCanvasClick(event) {
		if (isFunction(this.props.onCanvasClick)) {
			this.props.onCanvasClick(event)
		}
	}

	renderGraphDefinitions() {
		return (
			<defs data-radium='true'>
				<pattern
					id='grid'
					key='grid'
					width={canvasDefaults.gridDotOffset}
					height={canvasDefaults.gridDotOffset}
					patternUnits='userSpaceOnUse'
				>
					<circle
						cx={canvasDefaults.gridDotOffset / 2}
						cy={canvasDefaults.gridDotOffset / 2}
						r={canvasDefaults.gridDotSize}
						fill={canvasDefaults.gridDotColor}
					/>
				</pattern>
			</defs>
		)
	}

	renderGraphBackground() {
		return (
			<rect
				x={-canvasDefaults.graphSize / 4}
				y={-canvasDefaults.graphSize / 4}
				width={canvasDefaults.graphSize}
				height={canvasDefaults.graphSize}
				fill='url(#grid)'
			/>
		)
	}

	renderItems() {
		const children = []
		const list = isArray(this.props.list) ? this.props.list : []
		for (let i = 0, iLen = list.length; i < iLen; i++) {
			let item = list[i]
			if (
				!isNil(this.state.selectedNode) &&
				this.state.selectedNode.id === item.id
			) {
				item = this.state.selectedNode
			}
			children.push(
				<svg
					key={item.id}
					id={item.id}
					width={item.width + 'px'}
					height={item.height + 'px'}
					x={item.x + 'px'}
					y={item.y + 'px'}
					className='ITEM_REF'
					onClick={(e) => this.handleItemSelect(e, item)}
				>
					<rect
						width={item.width + 'px'}
						height={item.height + 'px'}
						x={'0px'}
						y={'0px'}
						className='DbCanvasItemRect'
					/>
					{this.getRenderRectangleContent(item, i)}
				</svg>
			)
		}
		return children
	}

	renderLines() {
		if (!isNil(this.state.selectedNode)) {
			return null
		}
		const lines = this.getLines()
		return lines.map((item) => {
			const data = item.data
			const key =
				data.targetTableId +
				'_' +
				data.sourceTableId +
				'_' +
				data.sourceAttrIndex
			return (
				<path
					d={item.path}
					onClick={(e) => this.handleSelectLink(e, item.data)}
					className='DbCanvasLine LINE_REF'
					key={key}
				>
					<title>{this.getLinkTooltip(data)}</title>
				</path>
			)
		})
	}

	render() {
		return (
			<div
				className='DbCanvasContainer'
				ref={(el) => (this.viewContainer = el)}
				onClick={this.handleCanvasClick}
			>
				<svg className='DbCanvasSVGContainer' ref={(el) => (this.svg = el)}>
					{this.renderGraphDefinitions()}
					<g ref={(el) => (this.view = el)}>
						{this.renderGraphBackground()}
						<g ref={(el) => (this.connections = el)}>{this.renderLines()}</g>
						<g ref={(el) => (this.entities = el)}>{this.renderItems()}</g>
					</g>
				</svg>
			</div>
		)
	}
}

DbCanvas.propTypes = {
	transform1: PropTypes.shape({
		x: PropTypes.number.isRequired,
		y: PropTypes.number.isRequired,
		k: PropTypes.number.isRequired,
	}),
	list: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
			x: PropTypes.number.isRequired,
			y: PropTypes.number.isRequired,
			width: PropTypes.number.isRequired,
			height: PropTypes.number.isRequired,
			links: PropTypes.arrayOf(
				PropTypes.shape({
					targetTableId: PropTypes.string.isRequired,
					targetAttrIndex: PropTypes.number.isRequired,
					sourceTableId: PropTypes.string.isRequired,
					sourceAttrIndex: PropTypes.number.isRequired,
				})
			).isRequired,
		})
	).isRequired,
	selected: PropTypes.string,

	onTransform: PropTypes.func,
	onSelectNode: PropTypes.func.isRequired,
	onSelectLink: PropTypes.func.isRequired,
	onReposition: PropTypes.func.isRequired,
	onCanvasClick: PropTypes.func,

	onRenderItem: PropTypes.func.isRequired,
	getLinkTooltip: PropTypes.func,
}
