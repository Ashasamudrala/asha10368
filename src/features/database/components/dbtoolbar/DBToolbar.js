import React, { useState } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import ToolbarWidget from '../../../../widgets/toolbar/Toolbar'
import toolbarConfig from '../../../../config/toolbar.json'
import './dbtoolbar.scss'
import { findIndex } from 'lodash'

export default function DBToolbar(props) {
	const { isDBModified, handleSaveDatabase } = props
	const searchSuggestions = props.tableData.map((table) => ({
		key: table.id,
		value: table.name,
	}))
	const [options, setOptions] = useState(false)
	const [cancelIcon, setCancelIcon] = useState(false)
	const [searchTerm, setSearchTerm] = useState('')

	const toolbarInput = toolbarConfig.config

	const getFilterSuggestions = () => {
		const searchString = searchTerm.toLowerCase()
		return searchSuggestions.filter(
			(option) => option.value.toLowerCase().indexOf(searchString) > -1
		)
	}
	const handleToolbarFields = (event, name) => {
		switch (name) {
			case 'redo':
			case 'undo':
			case 'publish':
				break
			case 'save':
				handleSaveDatabase()
				break
			case 'search':
				setSearchTerm(event.target.value)
				setOptions(true)
				setCancelIcon(true)

				break
			case 'newTable':
				props.handleAddNewTable()
				break
			default:
		}
	}

	const handleCancel = () => {
		setOptions(false)
		setCancelIcon(false)
	}

	/**
	 * Represents a Search
	 * @todo Implement handleSearch to highlight the table.
	 */

	const handleSearch = () => {}

	const handleSuggestions = (event, fieldName, action) => {
		if (fieldName) {
			switch (action) {
				case 'search':
					if (fieldName === 'search') {
						setSearchTerm(event.target.innerText)
						setOptions(false)
					}
					break
				default:
			}
		}
	}

	const getToolbarInput = () => {
		const index = findIndex(toolbarInput, { name: 'save' })
		toolbarInput[index].disable = isDBModified
		return toolbarInput
	}

	return (
		<div className='tool-bar'>
			<div className={props.drawer ? 'shiftToolBar' : ''}>
				<AppBar position='static'>
					<Toolbar>
						<ToolbarWidget
							toolbarInput={getToolbarInput()}
							options={options}
							cancelIcon={cancelIcon}
							handleCancel={handleCancel}
							handleSuggestions={handleSuggestions}
							handleSearch={handleSearch}
							searchSuggestions={getFilterSuggestions()}
							onChange={handleToolbarFields}
						/>
					</Toolbar>
				</AppBar>
			</div>
		</div>
	)
}
