import React, { useState, useEffect } from 'react'
import FormDialog from '../../../../widgets/formDialog/formDialog'
import {
	DbPasswordIsValid,
	fieldIsEmpty,
	isValidDbCredentials,
	isNumberValid,
	nameIsValid,
} from '../../../addPlatform/validations'
import ErrorMessages from '../../../../config/database/connectDb/connectDbValidations.json'
import FormBuilder from '../../../../widgets/formBuilder/FormBuilder'
import dbConfigOutput from '../../../../config/database/connectDb/connectDbOutput.json'
import _ from 'lodash'
import './DBConfig.scss'

export default function DBConfiguration(props) {
	const { dbConfig, data, updateDBConfig } = props
	const [connectDbConfig, setConnectDbConfig] = useState({
		...dbConfigOutput,
	})

	const [errors, setErrors] = useState({})

	const handleValidations = () => {
		const fields = { ...connectDbConfig }
		const errors = {}
		dbConfig.section[0].inputFields.map((inputField) =>
			inputField.required
				? fieldIsEmpty(fields[inputField.name])
					? (errors[inputField.name] = ErrorMessages[inputField.name])
					: null
				: null
		)
		if (!isValidDbCredentials(fields.username)) {
			errors.username = ErrorMessages.invalid_username
		}
		if (!DbPasswordIsValid(fields.password)) {
			errors.password = ErrorMessages.invalid_password
		}

		if (!nameIsValid(fields.host)) {
			errors.host = ErrorMessages.invalid_host
		}
		if (!isNumberValid(fields.port)) {
			errors.port = ErrorMessages.invalid_port
		}
		setErrors({ ...errors })
		return _.isEmpty(errors)
	}
	useEffect(() => {
		setDbDetails(data)
	}, [])

	const setDbDetails = (dbDetails) => {
		if (dbDetails) {
			setConnectDbConfig({
				name: dbDetails.name,
				environmentType: dbDetails.configurations.Development.environmentType,
				platform: dbDetails.configurations.Development.platform,
				host: dbDetails.configurations.Development.host,
				port: dbDetails.configurations.Development.port,
				username: dbDetails.configurations.Development.userName,
				password: dbDetails.configurations.Development.password,
			})
		}
	}

	const handleClose = () => {
		props.onCancel()
	}

	const handleSave = () => {
		if (handleValidations()) {
			updateDBConfig()
			handleClose()
		}
	}
	const handleOnChange = (event) => {
		setConnectDbConfig({
			...connectDbConfig,
			[event.target.name]: event.target.value,
		})
	}

	const dialogActions = [
		{
			buttonName: 'Cancel',
			buttonClass: 'cancel-btn',
			buttonClick: handleClose,
		},
		{
			buttonName: 'Connect',
			buttonClass: 'save-btn',
			buttonClick: handleSave,
		},
	]

	return (
		<div className={'connectdb-dialogue'}>
			<FormDialog
				open={true}
				title={'Connect To Your Database'}
				onCancel={handleClose}
				dialogActions={dialogActions}
				onSave={handleSave}
				submitText={'Connect'}
				content={
					<FormBuilder
						formBuilderInput={dbConfig}
						onChange={handleOnChange}
						formBuilderOutput={connectDbConfig}
						errors={errors}
					/>
				}
			/>
		</div>
	)
}
