import React from 'react'
import AddIcon from '@material-ui/icons/Add'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined'
import Attribute from './DBAttribute/DBAttribute'
import { useTranslation } from 'react-i18next'
import {
	ADD_COLUMN,
	DATABASE_TRANSLATIONS,
} from '../../../../utilities/constants'
import './DBTable.scss'
import { IsyInput } from '../../../../widgets/isyInput/IsyInput'

export default function DBTable(props) {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const { table, attributeTypes, isEditMode, tableIndex } = props

	const handleOnNameChange = (name) => {
		props.onChange({ name: name }, tableIndex)
	}
	const handleOnAttrChange = (attr, index) => {
		props.onAttrChange(attr, tableIndex, index)
	}

	const handleClick = (e) => {
		if (isEditMode) {
			e.stopPropagation()
		}
	}

	const handleEnableEdit = (e) => {
		e.stopPropagation()
		props.setEditMode(table.id)
	}

	const handleOnAddColumn = (e) => {
		e.stopPropagation()
		props.onAddAttr(tableIndex)
	}

	const handleOnDelete = (e) => {
		e.stopPropagation()
		props.onDelete(tableIndex)
	}

	const handleOnAttrDelete = (attrIndex) => {
		props.onDeleteAttr(tableIndex, attrIndex)
	}

	const handleOnSetKey = (attrIndex, keyType) => {
		props.onSetKey(tableIndex, attrIndex, keyType)
	}

	const renderAttributes = () => {
		return table.attributes.map((item, index) => (
			<Attribute
				key={index + item}
				index={index}
				data={item}
				attributeTypes={attributeTypes}
				isEditMode={isEditMode}
				onChange={handleOnAttrChange}
				onDelete={handleOnAttrDelete}
				onSetKey={handleOnSetKey}
			/>
		))
	}

	return (
		<div className='database-container'>
			<div className='database-table'>
				<table>
					<thead>
						<th colSpan='2' className='table-name' onClick={handleClick}>
							{isEditMode ? (
								<IsyInput
									type='text'
									name='name'
									onBlur={handleOnNameChange}
									value={table.name}
								/>
							) : (
								<span>{table.name}</span>
							)}
							<span className='header-icon'>
								{isEditMode ? (
									<DeleteOutlineIcon onClick={handleOnDelete} />
								) : (
									<EditOutlinedIcon
										className={'database-container-edit'}
										onClick={handleEnableEdit}
									/>
								)}
							</span>
						</th>
					</thead>
					<tbody>{renderAttributes()}</tbody>
				</table>
				{isEditMode && (
					<button className='add-column-btn' onClick={handleOnAddColumn}>
						<AddIcon className='add-icon' />
						<span className='add-column'>{t(ADD_COLUMN)}</span>
					</button>
				)}
			</div>
		</div>
	)
}
