import React, { Fragment, useState, useEffect } from 'react'
import Popover from '../../../../../widgets/popover/popover'
import { useTranslation } from 'react-i18next'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import { IsyInput } from '../../../../../widgets/isyInput/IsyInput'
import { isNil, find } from 'lodash'
import {
	DATABASE_TRANSLATIONS,
	NONE,
	PRIMARY_KEY,
	FOREIGN_KEY,
	UNIQUE_KEY,
} from '../../../../../utilities/constants'
import './DBAttribute.scss'

function DBAttribute(props) {
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const { data, index, attributeTypes, isEditMode } = props
	const [anchorEl, setAnchorEl] = useState(null)

	useEffect(() => {
		setAnchorEl(null)
	}, [isEditMode])

	const getSvgTypeClassName = (canNull) => {
		if (!isNil(data.reference)) {
			return 'foreign_key_svg'
		}
		if (data.constraints.primary) {
			return 'primary_key_svg'
		}
		if (data.constraints.unique) {
			return 'unique_key_svg'
		}
		if (!canNull) {
			return 'none_key_svg'
		}
		return 'no_key_svg'
	}

	const getAttributeTypeConstraints = () => {
		const attributeType = find(attributeTypes, (a) => a.value === data.type)
		if (!isNil(attributeType)) {
			return attributeType.constraints.map((c) => c.name)
		}
		return []
	}

	const getPopoverActions = () => {
		const options = []
		const constraints = getAttributeTypeConstraints()
		if (constraints.indexOf('primary') !== -1) {
			options.push({
				key: 'PrimaryKey',
				hasIcon: true,
				icon: <VpnKeyIcon className='key-icon primary_key_svg' />,
				name: t(PRIMARY_KEY),
				labelClass: 'key_label',
			})
		}
		if (!data.constraints.primary) {
			options.unshift({
				key: 'None',
				hasIcon: true,
				icon: <VpnKeyIcon className='key-icon none_key_svg' />,
				name: t(NONE),
				labelClass: 'key_label',
			})
			if (constraints.indexOf('primary') !== -1) {
				options.push({
					key: 'ForeignKey',
					hasIcon: true,
					icon: <VpnKeyIcon className='key-icon foreign_key_svg' />,
					name: t(FOREIGN_KEY),
					labelClass: 'key_label',
				})
			}
			if (constraints.indexOf('unique') !== -1) {
				options.push({
					key: 'UniqueKey',
					hasIcon: true,
					icon: <VpnKeyIcon className='key-icon unique_key_svg' />,
					name: t(UNIQUE_KEY),
					labelClass: 'key_label',
				})
			}
		}
		return options
	}

	const handleOnClick = (e) => {
		e.stopPropagation()
	}

	const handleOnNameChange = (name) => {
		props.onChange({ name: name }, index)
	}

	const handleOnTypeChange = (evt) => {
		props.onChange({ type: evt.target.value }, index)
	}

	const handleClickOnKeyIcon = (event) => {
		event.stopPropagation()
		setAnchorEl(event.currentTarget)
	}
	const handlePopoverClose = (event) => {
		event.stopPropagation()
		setAnchorEl(null)
	}
	const handleMenuItems = (event, key) => {
		event.stopPropagation()
		props.onSetKey(index, key)
		setAnchorEl(null)
	}
	const renderPopover = () => {
		return (
			<Popover
				anchorEl={anchorEl}
				actions={getPopoverActions()}
				className={'key-popover'}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
			></Popover>
		)
	}

	return (
		<Fragment>
			{isEditMode ? (
				<tr onClick={handleOnClick}>
					<td>
						<span className='icon'>
							<DeleteOutlineIcon
								className='delete-icon'
								onClick={() => props.onDelete(index)}
							/>
						</span>
						<IsyInput
							type='text'
							name='name'
							onBlur={handleOnNameChange}
							value={data.name}
						/>
					</td>
					<td>
						<select
							name='type'
							value={data.type}
							onChange={handleOnTypeChange}
							disabled={!isNil(data.reference)}
						>
							<option disabled selected hidden value=''>
								Type
							</option>
							{attributeTypes.map((type) => (
								<option key={type.value} value={type.value}>
									{type.value}
								</option>
							))}
						</select>
						<span className='icon key-icon'>
							<VpnKeyIcon
								className={getSvgTypeClassName(false)}
								onClick={handleClickOnKeyIcon}
							/>
						</span>
						{anchorEl && renderPopover()}
					</td>
				</tr>
			) : (
				<tr key={index}>
					<td>{data.name}</td>
					<td>
						<span>{data.type}</span>
						<span className='icon key-icon'>
							<VpnKeyIcon className={getSvgTypeClassName(true)} />
						</span>
					</td>
				</tr>
			)}
		</Fragment>
	)
}

export default DBAttribute
