import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './dBAccordion.scss'
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Accordion from '@material-ui/core/Accordion'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import IconButton from '@material-ui/core/IconButton'
import {
	DATABASE_TRANSLATIONS,
	DATABASES,
	SETTINGS,
	DELETE_DATABASE,
} from '../../../../utilities/constants'
import {
	updateDBConfig,
	fetchDBConfiguration,
} from '../../database.asyncActions'
import { getDBConfiguration } from '../../database.selectors'
import DBConfiguration from '../DBConfig/DBConfig'
import dbConfig from '../../../../config/database/connectDb/connectDb.json'
import { useTranslation } from 'react-i18next'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import AccountTreeOutlinedIcon from '@material-ui/icons/AccountTreeOutlined'
import Popover from '../../../../widgets/popover/popover'

export function DBAccordion(props) {
	const { databases, selectedDatabaseIndex, selectedTableIndex } = props
	const dbConfiguration = useSelector(getDBConfiguration)
	const [openDialogConfig, setOpenConfig] = useState(false)
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const [popoverAnchorEl, setPopoverAnchorEl] = useState(null)
	const [selectedPopoverDbDetails, setPopoverDbDetails] = useState(null)
	const heightValue = databases.length * 48 + databases.length * 1
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(
			fetchDBConfiguration({
				appId: '5fc1eb3ca15859e036aadc1d',
				databaseId: databases[selectedDatabaseIndex].id,
			})
		)
	}, [dispatch])

	const updateDBConfiguration = (data) => {
		dispatch(
			updateDBConfig({
				appId: '5fc1eb3ca15859e036aadc1d',
				databaseId: databases[selectedDatabaseIndex].id,
				databaseVendor: data,
			})
		)
		setOpenConfig(false)
	}

	const renderDbConfigDialog = () => {
		return (
			<DBConfiguration
				dbConfig={dbConfig}
				onCancel={handleCancel}
				data={dbConfiguration}
				updateDBConfig={updateDBConfiguration}
			/>
		)
	}

	const handleCancel = () => {
		setOpenConfig(false)
	}

	const handleChange = (panel, index, isExpanded) => {
		props.setSelectedDatabaseIndex(index)
	}
	const handleListItem = (index) => {
		props.setSelectedTableIndex(index)
	}

	const accordionExpansion = {
		height: `calc(100vh - ${heightValue}px)`,
		overFlow: `auto`,
		backgroundColor: `#faf9f8`,
	}

	const handleClick = (event, database) => {
		event.stopPropagation()
		setPopoverDbDetails(database)
		setPopoverAnchorEl(event.currentTarget)
	}

	const handlePopoverClose = (event) => {
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const handleMenuItems = (event, selectedItem) => {
		if (selectedItem === 'settings') {
			// todo
			setOpenConfig(true)
		} else if (selectedItem === 'delete') {
			props.onDeleteDatabase(selectedPopoverDbDetails.id)
		}
		event.stopPropagation()
		setPopoverAnchorEl(null)
	}

	const popoverActions = [
		{
			name: t(SETTINGS),
			key: 'settings',
		},
		{
			name: t(DELETE_DATABASE),
			key: 'delete',
		},
	]

	const renderPopover = () => {
		return (
			<Popover
				anchorEl={popoverAnchorEl}
				actions={popoverActions}
				onSelect={handleMenuItems}
				onClose={handlePopoverClose}
			></Popover>
		)
	}

	const renderAccordionDetails = () => {
		return (
			<div className='database-parent'>
				<div className='database-accordion'>
					{databases.length !== 0 && (
						<div className='database-parent-accordion'>
							<div className='database'>{t(DATABASES)}</div>
							<IconButton
								aria-label='Add Dabase'
								onClick={props.onAddDatabase}
								component='span'
							>
								<AddOutlinedIcon />
							</IconButton>
						</div>
					)}
					{databases.map((database, index) => {
						return (
							<div
								className={`menu-header ${
									index === selectedDatabaseIndex ? 'selected' : ''
								}`}
								key={index}
							>
								<Accordion
									expanded={index === selectedDatabaseIndex}
									onChange={(_, isExpanded) =>
										handleChange(`panel${index}`, index, isExpanded)
									}
								>
									<AccordionSummary
										expandIcon={<ExpandMoreIcon />}
										aria-controls={`panel${index}bh-content`}
										id={`panel${index}bh-header`}
										className='accordion-parent'
									>
										<Typography className='database-name'>
											{database.name}
										</Typography>

										<div
											className='hide-settings-icon'
											onClick={(event) => handleClick(event, database)}
										>
											<MoreHorizIcon />
										</div>
										{popoverAnchorEl && renderPopover()}
									</AccordionSummary>
									<AccordionDetails style={accordionExpansion}>
										{renderSubMenuAccordionDetails(database, index)}
									</AccordionDetails>
								</Accordion>
							</div>
						)
					})}
				</div>
			</div>
		)
	}

	const renderSubListDetails = (schemaData, dbIndex) => {
		return (
			<List>
				{schemaData.tables.map((tableData, index) => {
					return (
						<ListItem
							button
							key={index}
							onClick={() => handleListItem(index)}
							className={`${
								dbIndex === selectedDatabaseIndex &&
								selectedTableIndex === index
									? 'list-selected'
									: ''
							} list-item`}
						>
							<ListItemIcon>
								<TableChartOutlinedIcon />
							</ListItemIcon>
							<ListItemText
								className='list-item-text'
								primary={tableData.name}
								title={tableData.name}
							/>
						</ListItem>
					)
				})}
			</List>
		)
	}

	const renderSubMenuAccordionDetails = (database, index) => {
		return database.schemas.map((schemaData, id) => {
			return (
				<div className={`subMenu-menu-header`} key={id}>
					<Accordion defaultExpanded={true}>
						<AccordionSummary expandIcon={<ExpandMoreIcon />}>
							<div className='account-tree-icon'>
								<AccountTreeOutlinedIcon />
							</div>
							<Typography className='schema-display-name'>
								{schemaData.displayName}
							</Typography>
						</AccordionSummary>
						<AccordionDetails className='tables-list-container'>
							{renderSubListDetails(schemaData, index)}
						</AccordionDetails>
					</Accordion>
					{openDialogConfig && renderDbConfigDialog()},
				</div>
			)
		})
	}
	return renderAccordionDetails()
}
