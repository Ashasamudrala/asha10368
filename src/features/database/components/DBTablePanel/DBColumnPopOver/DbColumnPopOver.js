import React from 'react'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import { withStyles } from '@material-ui/core/styles'
import Popover from '@material-ui/core/Popover'
import {
	TableRow,
	TableBody,
	TableCell,
	Table,
	Switch,
} from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import {
	KEY_TYPE,
	DATABASE_TRANSLATIONS,
	NONE,
	PRIMARY_KEY,
	FOREIGN_KEY,
	UNIQUE_KEY,
} from '../../../../../utilities/constants'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import IsyButton from '../../../../../widgets/button'
import './dbColumnPopOver.scss'
import { IsyInput } from '../../../../../widgets/isyInput/IsyInput'
import DropDown from '../../../../../widgets/dropDown/dropDown'
import { isNil } from 'lodash'

const RequiredSwitch = withStyles({
	switchBase: {
		color: '#e34d28',
		float: 'right',
		'&$checked': {
			color: '#e34d28',
		},
		'&$checked + $track': {
			backgroundColor: '#e34d28',
		},
	},
	checked: {},
	track: {},
})(Switch)

export default function DBColumnPopOver(props) {
	const { constraints, constraintsConfig, anchorEl, reference } = props

	const { t } = useTranslation(DATABASE_TRANSLATIONS)

	const getSelectedKeyType = () => {
		if (!isNil(reference)) {
			return 'ForeignKey'
		}
		if (constraints.primary) {
			return 'PrimaryKey'
		}
		if (constraints.unique) {
			return 'UniqueKey'
		}
		return 'None'
	}

	const getPopoverActions = (hasPrimary, hasUnique) => {
		const options = []
		if (hasPrimary) {
			options.push({
				id: 'PrimaryKey',
				name: t(PRIMARY_KEY),
				hasIcon: true,
				icon: (
					<VpnKeyIcon className='key-icon-popover-attribute primary_key_svg' />
				),
				labelClass: 'popover-attribute-key-labels',
			})
		}
		if (!constraints.primary) {
			options.unshift({
				id: 'None',
				hasIcon: true,
				icon: (
					<VpnKeyIcon className='key-icon-popover-attribute none_key_svg' />
				),
				name: t(NONE),
				labelClass: 'popover-attribute-key-labels',
			})
			if (hasPrimary) {
				options.push({
					id: 'ForeignKey',
					hasIcon: true,
					icon: (
						<VpnKeyIcon className='key-icon-popover-attribute foreign_key_svg' />
					),
					name: t(FOREIGN_KEY),
					labelClass: 'popover-attribute-key-labels',
				})
			}
			if (hasUnique) {
				options.push({
					id: 'UniqueKey',
					hasIcon: true,
					icon: (
						<VpnKeyIcon className='key-icon-popover-attribute unique_key_svg' />
					),
					name: t(UNIQUE_KEY),
					labelClass: 'popover-attribute-key-labels',
				})
			}
		}
		return options
	}

	const handleClosePopOver = () => {
		props.onClose(false)
	}

	const handleConstraintChange = (name, value) => {
		props.onChangeOfConstraint({ [name]: value })
	}

	const handleDeleteAttribute = () => {
		props.onDelete()
	}

	const handleRelationshipTypeChange = (e) => {
		props.onKeyChange(e.target.value)
	}

	const renderStringType = (constraint) => {
		return (
			<TableRow key={constraint.name}>
				<TableCell className='table-cell-pop-over'>{constraint.name}</TableCell>
				<TableCell>
					<IsyInput
						type='text'
						name='name'
						onBlur={(value) => handleConstraintChange(constraint.name, value)}
						value={constraints[constraint.name]}
					/>
				</TableCell>
			</TableRow>
		)
	}

	const renderIntegerType = (constraint) => {
		return (
			<TableRow key={constraint.name}>
				<TableCell className='table-cell-pop-over'>{constraint.name}</TableCell>
				<TableCell>
					<IsyInput
						type='number'
						name='name'
						onBlur={(value) => handleConstraintChange(constraint.name, value)}
						value={constraints[constraint.name]}
					/>
				</TableCell>
			</TableRow>
		)
	}

	const renderBooleanType = (constraint) => {
		return (
			<TableRow key={constraint.name}>
				<TableCell className='table-cell-pop-over'>
					{constraint.name}?
				</TableCell>
				<TableCell className='require-switch'>
					<RequiredSwitch
						checked={constraints[constraint.name]}
						onChange={(e) =>
							handleConstraintChange(constraint.name, e.target.checked)
						}
						name={constraint.name}
					/>
				</TableCell>
			</TableRow>
		)
	}

	const renderKeyDropdown = (hasPrimary, hasUnique) => {
		return (
			<TableRow key='key_type'>
				<TableCell className='table-cell-pop-over'>{t(KEY_TYPE)}</TableCell>
				<TableCell>
					<div className='keyTypeDropdown'>
						<DropDown
							options={getPopoverActions(hasPrimary, hasUnique)}
							onChange={handleRelationshipTypeChange}
							update={true}
							name='keyTypeDropdown'
							selected={getSelectedKeyType()}
							className='input-base'
						/>
					</div>
				</TableCell>
			</TableRow>
		)
	}

	const renderConfig = () => {
		const children = []
		let requiredChild = null
		let hasPrimary = false
		let hasUnique = false
		for (let i = 0, iLen = constraintsConfig.length; i < iLen; i++) {
			const name = constraintsConfig[i].name
			if (name === 'required') {
				requiredChild = renderBooleanType(constraintsConfig[i])
			} else if (name === 'primary') {
				hasPrimary = true
			} else if (name === 'unique') {
				hasUnique = true
			} else {
				if (constraintsConfig[i].dataType === 'Boolean') {
					children.push(renderBooleanType(constraintsConfig[i]))
				} else if (constraintsConfig[i].dataType === 'Integer') {
					children.push(renderIntegerType(constraintsConfig[i]))
				} else if (constraintsConfig[i].dataType === 'String') {
					children.push(renderStringType(constraintsConfig[i]))
				}
			}
		}
		children.push(requiredChild)
		if (hasPrimary || hasUnique) {
			children.push(renderKeyDropdown(hasPrimary, hasUnique))
		}
		console.log(hasPrimary, hasUnique)
		return children
	}
	const renderPopOverContent = () => {
		return (
			<Table className='table-pop-over-container'>
				<TableBody>
					{renderConfig()}
					<TableRow>
						<TableCell colSpan={2} className={'delete-column-container'}>
							<IsyButton
								buttonClass={'delete-column'}
								handleButtonOnClick={handleDeleteAttribute}
							>
								<DeleteOutlineIcon className='delete-icon' />
								{'Delete Column'}
							</IsyButton>
						</TableCell>
					</TableRow>
				</TableBody>
			</Table>
		)
	}

	return (
		<Popover
			open={true}
			onClose={handleClosePopOver}
			anchorEl={anchorEl}
			anchorOrigin={{
				vertical: 'bottom',
				horizontal: 'right',
			}}
			transformOrigin={{
				vertical: 'top',
				horizontal: 'right',
			}}
			className='pop-over-container'
		>
			{renderPopOverContent()}
		</Popover>
	)
}
