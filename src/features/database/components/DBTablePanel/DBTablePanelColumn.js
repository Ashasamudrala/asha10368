import React, { useState, useEffect } from 'react'
import './DBTablePanel.scss'
import { isNil, find } from 'lodash'
import { IsyInput } from '../../../../widgets/isyInput/IsyInput'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import DBColumnPopOver from './DBColumnPopOver/DbColumnPopOver'

export default function DBTablePanelColumn(props) {
	const { data, attributeTypes } = props
	const [anchorEl, setAnchorEl] = useState(null)

	useEffect(() => {
		setAnchorEl(null)
	}, [data.name])

	const handleClosePopOver = () => {
		setAnchorEl(null)
	}

	const handleClickOptions = (event) => {
		event.stopPropagation()
		setAnchorEl(event.currentTarget)
	}

	const handleNameChange = (value) => {
		props.onUpdateTableAttribute({ name: value })
	}

	const handleTypeChange = (e) => {
		props.onUpdateTableAttribute({ type: e.target.value })
	}

	const getAttributeTypeConstraints = () => {
		const attributeType = find(attributeTypes, (a) => a.value === data.type)
		if (!isNil(attributeType)) {
			return attributeType.constraints
		}
		return []
	}

	const renderOptions = () => {
		return (
			<div className='moreContainer'>
				<MoreHorizIcon onClick={handleClickOptions} className='more' />
				{!isNil(anchorEl) && (
					<DBColumnPopOver
						anchorEl={anchorEl}
						constraints={data.constraints}
						reference={data.reference}
						constraintsConfig={getAttributeTypeConstraints()}
						onClose={handleClosePopOver}
						onChangeOfConstraint={props.onChangeOfTableAttributeConstraint}
						onDelete={props.onDeleteTableAttribute}
						onKeyChange={props.onAttributeKeyChange}
					/>
				)}
			</div>
		)
	}

	const renderType = () => {
		return (
			<div className='dataTypeContainer'>
				<select
					value={data.type}
					className='dataType'
					onChange={handleTypeChange}
					disabled={!isNil(data.reference)}
				>
					<option disabled selected hidden value=''>
						Type
					</option>
					{attributeTypes.map((item) => (
						<option key={item.value} value={item.value}>
							{item.value}
						</option>
					))}
				</select>
			</div>
		)
	}

	const renderName = () => {
		return (
			<div className='dataNameContainer'>
				<IsyInput
					type='text'
					name='name'
					onBlur={handleNameChange}
					value={data.name}
				/>
			</div>
		)
	}

	return (
		<div className='columnData' key={data}>
			{renderName()}
			{renderType()}
			{renderOptions()}
		</div>
	)
}
