import React, { useState } from 'react'
import Drawer from '@material-ui/core/Drawer'
import { isEmpty } from 'lodash'
import { makeStyles } from '@material-ui/core/styles'
import './DBTablePanel.scss'
import { IsyInput } from '../../../../widgets/isyInput/IsyInput'
import AddOutlinedIcon from '@material-ui/icons/AddOutlined'
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined'
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined'
import SearchIcon from '@material-ui/icons/Search'
import Divider from '@material-ui/core/Divider'
import Search from '../../../../widgets/search/Search'
import { Typography } from '@material-ui/core'
import {
	COLUMNS,
	TABLE_NAME,
	SEARCH_COLUMN,
	DATABASE_TRANSLATIONS,
} from '../../../../utilities/constants'
import { useTranslation } from 'react-i18next'
import DBTablePanelColumn from './DBTablePanelColumn'

const useStyles = makeStyles({
	root: {
		backgroundColor: 'transparent',
	},
})
export default function DBTablePanel(props) {
	const { tableData, attributeTypes } = props
	const { t } = useTranslation(DATABASE_TRANSLATIONS)
	const classes = useStyles()
	const [searchEnabled, setSearchEnabled] = useState(false)
	const [searchTerm, setSearchTerm] = useState('')

	const handleTableNameChange = (value) => {
		props.onTableChange({ name: value })
	}

	const handleSearchChange = (event) => {
		setSearchTerm(event.target.value)
	}
	const handlePanelClose = () => {
		props.onClose(false)
	}
	const handleSearchColumn = () => {
		setSearchEnabled(true)
	}
	const handleSearchClose = () => {
		if (isEmpty(searchTerm)) {
			setSearchEnabled(false)
		} else {
			setSearchTerm('')
		}
	}
	const handleDeleteTable = () => {
		props.onTableDelete()
	}

	const handleAddAttribute = () => {
		props.onAddAttribute()
	}

	const renderTableHeader = () => {
		return (
			<div className='table-heading'>
				<TableChartOutlinedIcon className='table-icon' />
				<label className='form-heading' title={tableData && tableData.name}>
					{tableData && tableData.name}
				</label>
				<DeleteOutlinedIcon
					className='delete-icon'
					onClick={handleDeleteTable}
				/>
			</div>
		)
	}

	const renderTableName = () => {
		return (
			<div className='table-section'>
				<Typography>{t(TABLE_NAME)}</Typography>
				<IsyInput
					type='text'
					name='name'
					onBlur={handleTableNameChange}
					value={tableData.name}
				/>
			</div>
		)
	}
	const renderAttribute = (data, index) => {
		return (
			<DBTablePanelColumn
				data={data}
				attributeTypes={attributeTypes}
				onUpdateTableAttribute={(data) => props.onAttributeChange(data, index)}
				onChangeOfTableAttributeConstraint={(data) =>
					props.onAttributeConstraintChange(data, index)
				}
				onDeleteTableAttribute={() => props.onDeleteTableAttribute(index)}
				onAttributeKeyChange={(key) => props.onAttributeKeyChange(key, index)}
			/>
		)
	}

	const renderAttributesHeader = () => {
		return (
			<>
				<label className='section'>{t(COLUMNS)}</label>
				<AddOutlinedIcon className='add-icon' onClick={handleAddAttribute} />
				<SearchIcon className='searchIcon' onClick={handleSearchColumn} />
			</>
		)
	}

	const renderAttributesSearch = () => {
		return (
			<Search
				value={searchTerm}
				searching={searchEnabled}
				searchIcon={false}
				searchPlaceholder={t(SEARCH_COLUMN)}
				iconPosition={'left'}
				cancelIcon={true}
				handleCancel={handleSearchClose}
				handleChange={handleSearchChange}
			/>
		)
	}

	const renderAttributes = () => {
		const children = []
		const searchString = searchTerm.toLowerCase()
		for (let i = 0, iLen = tableData.attributes.length; i < iLen; i++) {
			const attr = tableData.attributes[i]
			if (
				isEmpty(searchString) ||
				attr.name.toLowerCase().indexOf(searchString) !== -1
			) {
				children.push(renderAttribute(attr, i))
			}
		}
		return children
	}

	const renderAttributesSection = () => {
		return (
			<div className='table-second-section'>
				{renderAttributesHeader()}
				{renderAttributesSearch()}
				<Divider />
				{renderAttributes()}
			</div>
		)
	}

	return (
		<div>
			<Drawer
				BackdropProps={{
					classes: {
						root: classes.root,
					},
				}}
				className={props.drawerName}
				open={true}
				anchor='right'
				onClose={handlePanelClose}
			>
				{renderTableHeader()}
				<Divider />
				{renderTableName()}
				<Divider />
				{renderAttributesSection()}
				<Divider />
			</Drawer>
		</div>
	)
}
