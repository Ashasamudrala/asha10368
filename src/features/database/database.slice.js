import { v4 } from 'uuid'
import { isNil, find, pick } from 'lodash'
import { sliceName } from './database.selectors'
import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './database.asyncActions'
import newTable from '../../config/database/newTable.json'
import newAttribute from '../../config/database/newAttribute.json'
import { getUniqueName } from '../../utilities/utilities'

const defaultConstraints = {
	min: 10,
	max: 20,
	pattern: '',
	length: 20,
	unique: false,
	required: true,
	primary: false,
}

const initialState = {
	databases: [],
	selectedDatabaseIndex: null,
	selectedTableIndex: null,
	attributeTypes: [],
	keyTypes: [],
	prevAllDatabase: [],
	dbConfiguration: null,
}

const slice = createSlice({
	name: sliceName,
	initialState,
	reducers: {
		// synchronous actions
		updateSelectedDatabaseIndex(state, action) {
			if (state.databases.length > action.payload) {
				state.selectedDatabaseIndex = action.payload
				state.selectedTableIndex = null
			}
		},
		updateSelectedTableIndex(state, action) {
			state.selectedTableIndex = action.payload
		},
		addTable(state) {
			const index = state.selectedDatabaseIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			const names = tables.map((table) => table.name)
			const table = Object.assign({}, newTable, {
				name: getUniqueName(names, 'Untitled table'),
				id: v4(),
			})
			const attribute = JSON.parse(JSON.stringify(newAttribute))
			attribute.name = 'id'
			attribute.constraints.primary = true
			table.attributes.push(attribute)
			tables.push(table)

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		updateTable(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex
			const data = action.payload.table

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			if (!isNil(data.name)) {
				// on updating the table name updating the reference names
				const oldName = tables[tableIndex].name
				const newName = data.name
				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
						const dataToChange = {}
						if (!isNil(tables[i].attributes[j].reference)) {
							if (tables[i].attributes[j].reference.sourceModel === oldName) {
								dataToChange.sourceModel = newName
							}
							if (tables[i].attributes[j].reference.targetModel === oldName) {
								dataToChange.targetModel = newName
							}
						}
						if (Object.keys(dataToChange).length > 0) {
							const attributes = [...tables[i].attributes]
							const reference = Object.assign(
								{},
								attributes[j].reference,
								dataToChange
							)
							attributes[j] = Object.assign({}, attributes[j], { reference })
							tables[i] = Object.assign({}, tables[i], {
								attributes,
							})
						}
					}
				}
			}

			tables[tableIndex] = Object.assign({}, tables[tableIndex], data)

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		deleteTable(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			// need to remove relative foreign keys
			const tableName = tables[tableIndex].name
			for (let i = 0, iLen = tables.length; i < iLen; i++) {
				for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
					if (!isNil(tables[i].attributes[j].reference)) {
						const ref = tables[i].attributes[j].reference
						if (ref.targetModel === tableName) {
							const attrs = [...tables[i].attributes]
							attrs[j] = Object.assign({}, attrs[j], { reference: null })
							tables[i] = Object.assign({}, tables[i], {
								attributes: attrs,
							})
						}
					}
				}
			}

			tables.splice(tableIndex, 1)

			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		addAttribute(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			const attribute = JSON.parse(JSON.stringify(newAttribute))
			const names = attributes.map((attr) => attr.name)
			attribute.name = getUniqueName(names, 'Attr')
			if (attributes.length === 0) {
				attribute.constraints.primary = true
			}
			attributes.push(attribute)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		updateTableAttribute(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex
			const data = action.payload.attr

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]

			if (tables[tableIndex].attributes[attrIndex].constraints.primary) {
				const tableName = tables[tableIndex].name
				const oldName = tables[tableIndex].attributes[attrIndex].name
				for (let i = 0, iLen = tables.length; i < iLen; i++) {
					for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
						if (!isNil(tables[i].attributes[j].reference)) {
							const dataToChange = {}
							const attrDatToChange = {}
							const ref = tables[i].attributes[j].reference
							if (
								ref.sourceModel === tableName &&
								ref.sourceModelPrimaryKeyColumn === oldName &&
								!isNil(data.name)
							) {
								// on updating the attribute name updating the reference names
								dataToChange.sourceModelPrimaryKeyColumn = data.name
							}
							if (ref.targetModel === tableName && ref.column === oldName) {
								if (!isNil(data.name)) {
									// on updating the attribute name updating the reference names
									dataToChange.column = data.name
								}
								if (!isNil(data.type)) {
									attrDatToChange.type = data.type
								}
							}
							if (
								Object.keys(dataToChange).length > 0 ||
								Object.keys(attrDatToChange).length > 0
							) {
								const attrs = [...tables[i].attributes]
								const reference = Object.assign(
									{},
									attrs[j].reference,
									dataToChange
								)
								attrs[j] = Object.assign({}, attrs[j], {
									reference,
									...attrDatToChange,
								})
								tables[i] = Object.assign({}, tables[i], {
									attributes: attrs,
								})
							}
						}
					}
				}
			}

			const attributes = [...tables[tableIndex].attributes]
			if (!isNil(data.type)) {
				const attributeTypes = state.attributeTypes
				const attribute = find(attributeTypes, (a) => a.value === data.type)
				if (!isNil(attribute)) {
					const pathsAllowed = attribute.constraints.map((c) => c.name)
					const filteredAdjConstraints = pick(
						attributes[attrIndex].constraints,
						pathsAllowed
					)
					const filteredDefaultConstraints = pick(
						defaultConstraints,
						pathsAllowed
					)
					data.constraints = Object.assign(
						{},
						filteredDefaultConstraints,
						filteredAdjConstraints
					)
				}
			}
			attributes[attrIndex] = Object.assign({}, attributes[attrIndex], data)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		deleteTableAttribute(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			attributes.splice(attrIndex, 1)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		updateTableAttributeConstraints(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex
			const data = action.payload.constraints

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			if (data.primary) {
				// if primary key is true need to remove other primary keys in table
				let primaryKeyName = null
				for (let i = 0, iLen = attributes.length; i < iLen; i++) {
					if (attributes[i].constraints.primary) {
						primaryKeyName = attributes[i].name
						attributes[i] = Object.assign({}, attributes[i], {
							constraints: Object.assign({}, attributes[i].constraints, {
								primary: false,
							}),
						})
					}
				}
				if (!isNil(primaryKeyName)) {
					// need to remove foreign relations for all the tables
					// need to update the primary
					const tableName = tables[tableIndex].name
					for (let i = 0, iLen = tables.length; i < iLen; i++) {
						for (let j = 0, jLen = tables[i].attributes.length; j < jLen; j++) {
							if (!isNil(tables[i].attributes[j].reference)) {
								const ref = tables[i].attributes[j].reference
								if (
									ref.sourceModel === tableName &&
									ref.sourceModelPrimaryKeyColumn === primaryKeyName
								) {
									// need to update the primary
									const attrs = [...tables[i].attributes]
									const reference = Object.assign({}, attrs[j].reference, {
										sourceModelPrimaryKeyColumn: attributes[attrIndex].name,
									})
									attrs[j] = Object.assign({}, attrs[j], { reference })
									tables[i] = Object.assign({}, tables[i], {
										attributes: attrs,
									})
								}
								if (
									ref.targetModel === tableName &&
									ref.column === primaryKeyName
								) {
									// need to remove foreign relations for all the tables
									const attrs = [...tables[i].attributes]
									attrs[j] = Object.assign({}, attrs[j], { reference: null })
									tables[i] = Object.assign({}, tables[i], {
										attributes: attrs,
									})
								}
							}
						}
					}
				}
			}

			// porting the changes
			const constraints = Object.assign(
				{},
				attributes[attrIndex].constraints,
				data
			)

			const attributeData = {}
			// setting the reference to null if primary or unique key is trying to set.
			if (!isNil(data.unique) || !isNil(data.primary)) {
				attributeData.reference = null
			}

			attributes[attrIndex] = Object.assign(
				{},
				attributes[attrIndex],
				attributeData,
				{
					constraints,
				}
			)

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
		setTableAttributeForeignKey(state, action) {
			const index = state.selectedDatabaseIndex
			const tableIndex = action.payload.tableIndex
			const attrIndex = action.payload.attrIndex
			const primaryKeyIndex = action.payload.primaryKeyIndex
			const targetTableIndex = action.payload.targetTableIndex
			const targetAttrIndex = action.payload.targetAttrIndex
			const relationShipType = action.payload.relationShipType
			const containment = action.payload.containment

			const databases = [...state.databases]
			const schemas = [...databases[index].schemas]
			const tables = [...schemas[0].tables]
			const attributes = [...tables[tableIndex].attributes]

			const reference = {
				sourceModel: tables[tableIndex].name,
				sourceModelPrimaryKeyColumn:
					tables[tableIndex].attributes[primaryKeyIndex].name,
				targetModel: tables[targetTableIndex].name,
				column: tables[targetTableIndex].attributes[targetAttrIndex].name,
				relation: {
					associationType: relationShipType,
					containment: containment,
				},
			}
			const constraints = Object.assign({}, attributes[attrIndex].constraints, {
				unique: false,
				primary: false,
			})

			attributes[attrIndex] = Object.assign({}, attributes[attrIndex], {
				reference,
				constraints,
			})

			tables[tableIndex] = Object.assign({}, tables[tableIndex], { attributes })
			schemas[0] = Object.assign({}, schemas[0], { tables })
			databases[index] = Object.assign({}, databases[index], { schemas })
			state.databases = databases
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllDatabases.fulfilled]: (state, action) => {
			if (action && action.payload) {
				state.prevAllDatabase = action.payload.content
				state.databases = action.payload.content
				if (action.payload.content.length > 0) {
					state.selectedDatabaseIndex = 0
					state.selectedTableIndex = null
				}
			}
		},
		[asyncActions.fetchAttributeTypes.fulfilled]: (state, action) => {
			state.attributeTypes = action.payload.content
		},
		[asyncActions.fetchConstrainsKeyTypes.fulfilled]: (state, action) => {
			state.keyTypes = action.payload.content
		},
		[asyncActions.fetchDBConfiguration.fulfilled]: (state, action) => {
			state.dbConfiguration = action.payload
		},
		[asyncActions.saveDatabase.fulfilled]: (state, action) => {
			if (action && action.payload) {
				let allDatabaseRef = state.prevAllDatabase
				if (!isNil(state.selectedDatabaseIndex)) {
					allDatabaseRef = allDatabaseRef.splice(state.selectedDatabaseIndex, 1)
				}
				allDatabaseRef.splice(
					state.selectedDatabaseIndex,
					0,
					state.databases[state.selectedDatabaseIndex]
				)
				state.prevAllDatabase = allDatabaseRef
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
