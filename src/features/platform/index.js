import Platform from './Platform'
import * as selectors from './platform.selectors'
import * as asyncActions from './platform.asyncActions'
import slice from './platform.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllPlatform } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAllPlatform, selectPlatformFilter } = selectors

// we export the component most likely to be desired by default
export default Platform
