import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectAllPlatform } from './platform.selectors'
import { fetchAllPlatform } from './platform.asyncActions'
import platformGridProp from '../../config/platformGrid.json'
import Grid from '../../widgets/grid/Grid'
import ViewPlatformDrawer from '../../features/viewPlatform'
import Can from '../../widgets/auth/Can'
import viewPlatformData from '../../config/viewPlatform.json'
import {
	permission,
	PLATFORM_TRANSLATIONS,
	ADD_PLATFORM,
} from '../../utilities/constants'
import IsyButton from '../../widgets/button'
import AddPlatform from '../../features/addPlatform'
import { useTranslation } from 'react-i18next'

import './platform.scss'

export default function Platform() {
	const { t } = useTranslation(PLATFORM_TRANSLATIONS)
	const platform = useSelector(selectAllPlatform)
	const dispatch = useDispatch()
	useEffect(() => {
		dispatch(fetchAllPlatform())
	}, [dispatch])

	const [isDrawerOpen, setIsDrawerOpen] = useState(false)
	const [isAddOpenDrawer, setIsAddOpenDrawer] = useState(false)
	const [platformData, setplatformData] = useState('')
	const handleViewItem = (isOpen, viewItem) => {
		setplatformData(viewItem)
		setIsDrawerOpen(isOpen)
	}

	const handleButtonOnClick = (event) => {
		setIsAddOpenDrawer(true)
	}

	const handleIsDrawerOpen = (open) => {
		setIsAddOpenDrawer(open)
	}

	return (
		<>
			<div>
				<div className='add-platform-button-container'>
					<Can
						action={permission.CREATE_PLATFORM}
						yes={() => (
							<IsyButton
								buttonClass={'add-platform-class'}
								handleButtonOnClick={handleButtonOnClick}
							>
								{t(ADD_PLATFORM)}
							</IsyButton>
						)}
						no={() => ''}
					></Can>
				</div>
				<Grid
					handleViewItem={handleViewItem}
					properties={platformGridProp}
					data={platform}
				/>
			</div>

			{isDrawerOpen && (
				<Can
					action={permission.VIEW_PLATFORM}
					yes={() => (
						<ViewPlatformDrawer
							platform={platformData}
							viewPlatform={viewPlatformData}
							onCancel={handleViewItem}
						/>
					)}
					no={() => ''}
				></Can>
			)}
			{isAddOpenDrawer ? <AddPlatform openDrawer={handleIsDrawerOpen} /> : null}
		</>
	)
}
