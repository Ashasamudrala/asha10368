import slice from './platform.slice'
export const selectSlice = (state) => state[slice.name]

export const selectAllPlatform = (state) => selectSlice(state).allPlatform

export const selectPlatformFilter = (state) => selectSlice(state).filter
