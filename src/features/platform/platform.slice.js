import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './platform.asyncActions'

const initialState = {
	allPlatform: [],
	filter: '',
}
const slice = createSlice({
	name: 'platform',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllPlatform.fulfilled]: (state, action) => {
			if (action.payload) {
				state.allPlatform = action.payload.content
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
