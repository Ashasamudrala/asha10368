import _ from 'lodash'
import React, { useEffect } from 'react'
import AbstractFramework from '../../features/addFramework/AbstractFrameworkForm'
import frameworkInput from '../../config/framework/editFramework.json'
import {
	updateFrameworkById,
	getPlatformById,
} from './editFramework.asyncActions'
import { useDispatch, useSelector } from 'react-redux'
import { getVersions, getStatus } from './editFramework.selectors'
import { fetchAllPlatform } from '../platform/platform.asyncActions'
import { actions } from './editFramework.slice'
const { removeStatus } = actions

export default function EditFramework(props) {
	const {
		name,
		description,
		channelConfiguration,
		versions,
		metadata,
	} = JSON.parse(JSON.stringify(props.frameworkDetails))

	const dispatch = useDispatch()
	useEffect(() => {
		props.frameworkDetails &&
			dispatch(
				getPlatformById({ platformId: props.frameworkDetails.platformId })
			)
	}, [props.frameworkDetails]) // eslint-disable-line react-hooks/exhaustive-deps

	const status = useSelector(getStatus)

	// when ever addplatform is posted successfully and then dispatch to fetchallpltformsapi
	useEffect(() => {
		if (status !== undefined) {
			props.openDrawer(false)
			status && dispatch(fetchAllPlatform())
		}
	}, [status]) // eslint-disable-line react-hooks/exhaustive-deps

	useEffect(() => {
		return () => {
			dispatch(removeStatus())
		}
	}, []) // eslint-disable-line react-hooks/exhaustive-deps

	//  get the platform details of particular platform from selectors
	const platformDetails = useSelector(getVersions)
	// convert version metadata,module metadata as per json
	if (versions && versions.length >= 0) {
		versions.forEach((version, index) => {
			const clonedVersion = version ? _.clone(version) : {}
			if (clonedVersion.metadata) {
				version.metadata = _.map(Object.keys(clonedVersion.metadata), (key) => {
					return { key: key, value: clonedVersion.metadata[key] }
				})
			}
			const { modules } = version
			if (modules && modules.length >= 0) {
				modules.forEach((module, moduleIndex) => {
					const clonedModule = module ? _.clone(module) : {}
					if (clonedModule.metadata) {
						module.metadata = _.map(
							Object.keys(clonedModule.metadata),
							(key) => {
								return { key: key, value: clonedModule.metadata[key] }
							}
						)
					}
				})
			}
		})
	}
	const setOpenDrawer = () => {
		props.openDrawer(false)
	}
	const handleSave = (frameworkDetails) => {
		dispatch(
			updateFrameworkById({
				...frameworkDetails,
				platformId: props.frameworkDetails.platformId,
				frameworkId: props.frameworkDetails.id,
			})
		)
	}
	return (
		<AbstractFramework
			openDrawer={setOpenDrawer}
			frameworkInput={frameworkInput}
			frameworkOutput={{
				name: name,
				description: description,
				channelConfiguration: channelConfiguration,
				versions: versions,
				metadata: _.map(Object.keys(metadata), (key) => {
					return { key: key, value: metadata[key] }
				}),
			}}
			onSave={handleSave}
			supportedVersions={platformDetails.versions}
		/>
	)
}
