import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './editFramework.asyncActions'

const initialState = {
	platformDetails: [],
	status: undefined,
}

const slice = createSlice({
	name: 'editFramework',
	initialState,
	reducers: {
		// synchronous actions
		removeStatus(state) {
			state.status = undefined
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.updateFrameworkById.fulfilled]: (state, action) => {
			if (action.payload) {
				state.status = true
			} else {
				state.status = false
			}
		},
		[asyncActions.getPlatformById.fulfilled]: (state, action) => {
			if (action.payload) {
				state.platformDetails = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
