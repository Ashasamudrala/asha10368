import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { UPDATE_FRAMEWORK } from '../../utilities/apiEndpoints'
import { EDIT_FRAMEWORK_SUCCESS_MESSAGE } from '../../utilities/constants'

export const updateFrameworkById = createAsyncThunk(
	'editFramework/update',
	async (
		{
			useCaching,
			noBusySpinner,
			platformId,
			frameworkId,
			metadata,
			name,
			description,
			channelConfiguration,
			versions,
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: `${UPDATE_FRAMEWORK}/${platformId}/frameworks/${frameworkId}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'put',
			httpConfig: {
				body: JSON.stringify({
					metadata,
					name,
					description,
					channelConfiguration,
					versions,
					platformId,
				}),
			},
			successMessage: `${name} ${EDIT_FRAMEWORK_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)

export const getPlatformById = createAsyncThunk(
	'editFramework/getAll',
	async ({ useCaching, noBusySpinner, platformId } = {}, thunkArgs) =>
		await doAsync({
			url: `${UPDATE_FRAMEWORK}/${platformId}`,
			useCaching,
			noBusySpinner,
			httpMethod: 'get',
			...thunkArgs,
		})
)
