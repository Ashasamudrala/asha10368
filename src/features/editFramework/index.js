import EditFramework from './EditFramework'
import slice from './editFramework.slice'

export const { name, reducer } = slice

// we export the component most likely to be desired by default
export default EditFramework
