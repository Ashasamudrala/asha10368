import slice from './editFramework.slice'

export const selectSlice = (state) => state[slice.name]

export const getVersions = (state) => selectSlice(state).platformDetails

export const getStatus = (state) => selectSlice(state).status
