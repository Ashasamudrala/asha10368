import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './addContinuousIntegration.asyncActions'

const initialState = {
	allAddContinuousIntegration: [],
	filter: '',
}

const slice = createSlice({
	name: 'addContinuousIntegration',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = ''
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.saveAddContinuousIntegration.fulfilled]: (state, action) => {
			if (action.payload) {
				state.filter = true
			} else {
				state.filter = false
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
