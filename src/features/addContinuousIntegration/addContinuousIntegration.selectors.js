import slice from './addContinuousIntegration.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAddContinuousIntegrationFilter = (state) =>
	selectSlice(state).filter
