import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import ContinuousIntegrationInput from '../../config/addContinuousIntegration/addContinuousIntegration.json'
import ContinuousIntegrationOutputData from '../../config/addContinuousIntegration/addContinuousIntegrationOutput.json'
import AbstractContinuousIntegration from './AbstractContinuousIntegration'
import { fetchAllContinuousIntegration } from './../continuousIntegration/continuousIntegration.asyncActions'

import { selectAddContinuousIntegrationFilter } from './addContinuousIntegration.selectors'
import { actions } from './addContinuousIntegration.slice'
import { saveAddContinuousIntegration } from './addContinuousIntegration.asyncActions'
import BusyIndicator from '../../widgets/busyIndicator'
import './addContinuousIntegration.scss'
const { updateFilter } = actions

/**
 * Represents the add ci details.
 * @param {object} props
 */
export default function AddContinuousIntegration(props) {
	const status = useSelector(selectAddContinuousIntegrationFilter)
	const dispatch = useDispatch()

	// when ever save button is clicked dispatch to addCI  api
	const handleSave = (continuousIntegrationFields) => {
		dispatch(saveAddContinuousIntegration({ ...continuousIntegrationFields }))
	}

	// when ever add CI posted successfully and then dispatch to fetchAllContinuousIntegration.
	useEffect(() => {
		if (status !== '') {
			status && dispatch(fetchAllContinuousIntegration())
			props.openDrawer(false)
		}
	}, [status])

	useEffect(() => {
		return () => {
			dispatch(updateFilter())
		}
	}, [])

	return (
		<BusyIndicator>
			<AbstractContinuousIntegration
				openDrawer={props.openDrawer}
				continuousIntegrationInput={ContinuousIntegrationInput}
				continuousIntegrationOutputData={JSON.parse(
					JSON.stringify(ContinuousIntegrationOutputData)
				)}
				className='add-ci'
				onSave={handleSave}
			/>
		</BusyIndicator>
	)
}
AddContinuousIntegration.propTypes = {
	handleSave: PropTypes.func,
}
