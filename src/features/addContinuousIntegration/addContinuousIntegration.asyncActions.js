import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'
import { GET_ALL_CONTINUOUSINTEGRATION } from '../../utilities/apiEndpoints'
import { SAVED_SUCCESS_MESSAGE } from '../../utilities/constants'

export const saveAddContinuousIntegration = createAsyncThunk(
	'addContinuousIntegration/getAll',
	async (
		{
			useCaching,
			noBusySpinner,
			name,
			description,
			pipelineConfiguration,
			metadata,
			apiConfiguration,
			channelConfiguration: { channelName },
		} = {},
		thunkArgs
	) =>
		await doAsync({
			url: GET_ALL_CONTINUOUSINTEGRATION,
			useCaching,
			noBusySpinner,
			httpMethod: 'post',
			httpConfig: {
				body: JSON.stringify({
					name,
					description,
					metadata,
					apiConfiguration,
					pipelineConfiguration,
					channelConfiguration: {
						channelName,
					},
				}),
			},
			successMessage: `${name} ${SAVED_SUCCESS_MESSAGE}`,
			...thunkArgs,
		})
)
