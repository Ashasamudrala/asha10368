import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import RightSlider from '../../widgets/rightSideSlider/RightSideSlider'
import BusyIndicator from '../../widgets/busyIndicator/BusyIndicator'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import FormBuilder from '../../widgets/formBuilder/FormBuilder'
import {
	ADD,
	ONCHANGE,
	DELETE,
	modeType,
	REPOSITORY_TRANSLATIONS,
} from '../../utilities/constants'
import _ from 'lodash'
import { fieldIsEmpty, addNameIsValid } from '../addPlatform/validations'
import ErrorMessages from '../../config/addContinuousIntegration/validations.json'

/**
 * Represents of add ci details after click on button.
 * @param {object} props
 */
export default function AbstractContinuousIntegration(props) {
	const { continuousIntegrationInput, continuousIntegrationOutputData } = props

	const [
		continuousIntegrationFields,
		setContinuousIntegrationFields,
	] = useState({
		...continuousIntegrationOutputData,
	})

	const [errors, setErrorValues] = useState({})

	const { t } = useTranslation(REPOSITORY_TRANSLATIONS)

	/**
	 * On clicking the save button it converts the metadata to key, value pairs
	 */
	const handleSave = () => {
		if (handleValidation()) {
			convertMetdataToKeyValue()
		}
	}

	const convertMetdataToKeyValue = () => {
		// Clone the apiConfiguration and transform metadata property.
		const clonedFormValues = _.cloneDeep(continuousIntegrationFields)
		const { apiConfiguration, metadata } = clonedFormValues
		let processedMetadata = {}
		if (metadata && metadata.length >= 0) {
			processedMetadata = _.mapValues(_.keyBy(metadata, 'key'), 'value')
			processedMetadata = _.omit(processedMetadata, [''])
		}
		if (
			apiConfiguration &&
			apiConfiguration.metadata &&
			apiConfiguration.metadata.length >= 0
		) {
			clonedFormValues.apiConfiguration.metadata = _.mapValues(
				_.keyBy(apiConfiguration.metadata, 'key'),
				'value'
			)
		}
		props.onSave({
			...clonedFormValues,
			metadata: { ...processedMetadata },
		})
	}

	/**
	 * Form Validations
	 */
	const handleValidation = () => {
		const fields = { ...continuousIntegrationFields }
		const errors = {}
		continuousIntegrationInput.section.forEach(
			(formFields) =>
				formFields.inputFields &&
				formFields.inputFields.forEach((inputField) => {
					if (inputField.required) {
						if (fieldIsEmpty(_.get(fields, inputField.name))) {
							_.set(errors, inputField.name, ErrorMessages[inputField.name])
						}
					}
				})
		)
		if (fields.name !== '' && addNameIsValid(fields.name)) {
			errors.name = ErrorMessages.invalidName
		}
		setErrorValues({ ...errors })
		return _.isEmpty(errors)
	}

	// this is used for setting the json fields
	const handleContinuousIntegrationFields = (event, fieldName, action) => {
		/**
		 * if fieldName is defined then if condition executes this is for metadata and version
		 * and then else executes when ever fieldName is undefined
		 */
		let processedfieldName = []
		if (fieldName) {
			/**
			 * based on action switch case executes
			 */
			switch (action) {
				case t(ADD):
					processedfieldName = continuousIntegrationFields
					processedfieldName = _.set(processedfieldName, fieldName, [
						event,
						..._.get(processedfieldName, fieldName),
					])
					setContinuousIntegrationFields({ ...processedfieldName })
					break
				case t(ONCHANGE):
					/**
					 * It is used when onchange of metadata is called and push the change value
					 * based on id into array if type is (metadata)
					 */
					processedfieldName = continuousIntegrationFields
					_.get(processedfieldName, fieldName)[event.target.dataset.id][
						event.target.name
					] = event.target.value
					setContinuousIntegrationFields({ ...processedfieldName })
					break
				case t(DELETE):
					/**
					 * It is used when delete of metadata and version  is called and removes the item
					 * based on id from array
					 */
					processedfieldName = continuousIntegrationFields
					processedfieldName = _.set(
						processedfieldName,
						fieldName,
						_.get(processedfieldName, fieldName).filter(
							(version, id) => event !== id
						)
					)
					setContinuousIntegrationFields({ ...processedfieldName })
					break
				default:
			}
		} else {
			processedfieldName = continuousIntegrationFields
			processedfieldName = _.set(
				processedfieldName,
				event.target.name,
				event.target.value
			)
			setContinuousIntegrationFields({ ...processedfieldName })
		}
	}

	/**
	 * render add ci in rightside slider.
	 */
	const renderRightSideSlider = () => {
		return (
			<RightSlider onCancel={props.openDrawer} drawerName='drawer'>
				<BusyIndicator>
					<CloseOutlinedIcon
						className='close-icon'
						onClick={() => {
							props.openDrawer()
						}}
					/>
					<div className='continuous-integration'>
						<div className={props.className}>
							<FormBuilder
								formBuilderInput={continuousIntegrationInput}
								onChange={handleContinuousIntegrationFields}
								formBuilderOutput={continuousIntegrationFields}
								errors={errors}
							/>
						</div>
						<SliderFooter
							mode={modeType.WRITE}
							handleClose={props.openDrawer}
							handleSave={handleSave}
						/>
					</div>
				</BusyIndicator>
			</RightSlider>
		)
	}
	return <div>{renderRightSideSlider()}</div>
}
AbstractContinuousIntegration.propTypes = {
	handleSave: PropTypes.func,
	handleContinuousIntegrationFields: PropTypes.func,
	handleValidation: PropTypes.func,
}
