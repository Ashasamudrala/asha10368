import AddContinuousIntegration from './AddContinuousIntegration'
import * as selectors from './addContinuousIntegration.selectors'
import * as asyncActions from './addContinuousIntegration.asyncActions'
import slice from './addContinuousIntegration.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { saveAddContinuousIntegration } = asyncActions

// we prefix all selectors with the the "select" prefix
export const { selectAddContinuousIntegrationFilter } = selectors

// we export the component most likely to be desired by default
export default AddContinuousIntegration
