import { createAsyncThunk } from '@reduxjs/toolkit'
import doAsync from '../../infrastructure/doAsync'

export const fetchAllViewContinuousIntegration = createAsyncThunk(
	'viewContinuousIntegration/getAll',
	async ({ useCaching, noBusySpinner } = {}, thunkArgs) =>
		await doAsync({
			url: '',
			useCaching,
			noBusySpinner,
			...thunkArgs,
		})
)
