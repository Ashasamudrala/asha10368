import ViewContinuousIntegration from './ViewContinuousIntegration'
import * as selectors from './viewContinuousIntegration.selectors'
import * as asyncActions from './viewContinuousIntegration.asyncActions'
import slice from './viewContinuousIntegration.slice'

export const {
	name,
	actions: { updateFilter },
	reducer,
} = slice

export const { fetchAllViewContinuousIntegration } = asyncActions

// we prefix all selectors with the the "select" prefix
export const {
	selectAllViewContinuousIntegration,
	selectViewContinuousIntegrationFilter,
} = selectors

// we export the component most likely to be desired by default
export default ViewContinuousIntegration
