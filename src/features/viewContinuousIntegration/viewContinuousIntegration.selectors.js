import slice from './viewContinuousIntegration.slice'

export const selectSlice = (state) => state[slice.name]

export const selectAllViewContinuousIntegration = (state) =>
	selectSlice(state).allViewContinuousIntegration

export const selectViewContinuousIntegrationFilter = (state) =>
	selectSlice(state).filter
