import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined'
import BusyIndicator from '../../widgets/busyIndicator'
import SliderFooter from '../../widgets/rightSideSlider/SliderFooter'
import GetImagePath from '../../utilities/utilities'
import RightSideSlider from '../../widgets/rightSideSlider'
import EditContinuousIntegration from '../../features/editContinuousIntegration'
import { modeType, permission } from '../../utilities/constants'
import Can from '../../widgets/auth/Can'
import './viewContinuousIntegration.scss'

export default function ViewContinuousIntegration(props) {
	const {
		continuousIntegrationDetails,
		viewContiniousIntegrationJsonData,
	} = props
	const [openEditCI, setOpenEditCI] = useState(false)

	/**
	 * Render API Configuration Details.
	 * @param {object} apiConfigurationData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderApiConfiguration = (apiConfigurationData, index, jsonItem) => {
		return (
			<>
				{jsonItem.apiConfigChildren &&
					jsonItem.apiConfigChildren.map((item, i) =>
						renderLabel(item, i, apiConfigurationData)
					)}
			</>
		)
	}

	/**
	 * Render Pipeline Configuration Details.
	 * @param {object} pipelineConfigurationData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderPipelineConfiguration = (
		pipelineConfigurationData,
		index,
		jsonItem
	) => {
		return (
			<>
				{jsonItem.pipelineConfigChildren &&
					jsonItem.pipelineConfigChildren.map((item, i) =>
						renderLabel(item, i, pipelineConfigurationData.sonarConfiguration)
					)}
			</>
		)
	}

	/**
	 * Render endpoints details.
	 * @param {object} apiConfigData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderEndPoints = (apiConfigData, index, jsonItem) => {
		if (apiConfigData.endpoints.length > 0) {
			return (
				<table>
					<thead>
						<tr>
							{jsonItem.endPointChildren.map((item, i) => (
								<th key={i} className={item.className}>
									{item.label}
								</th>
							))}
						</tr>
					</thead>
					<tbody>
						{apiConfigData &&
							apiConfigData.endpoints.map((item, i) => (
								<tr key={i}>
									<td className='content'>{item.name}</td>
									<td className='content table-value'>{item.endpoint}</td>
								</tr>
							))}
					</tbody>
				</table>
			)
		}
	}

	/**
	 * Render channel configuration details.
	 * @param {object} channelConfig
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderChannelConfiguration = (channelConfig, index, jsonItem) => {
		return (
			<>
				{jsonItem.channelConfigChildren &&
					jsonItem.channelConfigChildren.map((item, i) =>
						renderLabel(item, i, channelConfig)
					)}
			</>
		)
	}

	/**
	 * Render api metadata details.
	 * @param {object} apiConfig
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderApiMetaData = (apiConfig, index, jsonItem) => {
		const metaKeyValue = Object.entries(apiConfig.metadata)
		return (
			<table>
				<tbody>
					{metaKeyValue &&
						metaKeyValue.map(([key, value], i) => (
							<tr key={i} className={jsonItem.metaDataClass}>
								<td className={jsonItem.keyClass}>{key}</td>
								<td className={jsonItem.valueClass}>{value}</td>
							</tr>
						))}
				</tbody>
			</table>
		)
	}

	/**
	 * Render CI metadata details.
	 * @param {object} metaData
	 * @param {number} index
	 * @param {object} jsonItem
	 */
	const renderMetaData = (metaData, index, jsonItem) => {
		const metaKeyValue = Object.entries(metaData)
		return (
			<>
				<table>
					<tbody>
						{metaKeyValue &&
							metaKeyValue.map(([key, value], i) => (
								<tr key={i} className={jsonItem.metaDataClass}>
									<td className={jsonItem.keyClass}>{key}</td>
									<td className={jsonItem.valueClass}>{value}</td>
								</tr>
							))}
					</tbody>
				</table>
			</>
		)
	}

	/**
	 * Render Icon's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderIcon = (item, index) => {
		return (
			<Can
				action={permission.UPDATE_CONTINUOUS_INTEGRATION_CONFIGURATION}
				yes={() => (
					<div key={index} className={item.className} onClick={handleEditIcon}>
						<span>
							<span className={item.iconClass}>{GetImagePath(item.icon)}</span>
							<span className={item.labelClass}>{item.attributeName}</span>
						</span>
					</div>
				)}
				no={() => ''}
			></Can>
		)
	}

	/**
	 * Render label's.
	 * @param {object} item - Json data.
	 * @param {number} index
	 * @param {object} responseData
	 */
	const renderLabel = (item, index, responseData) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{item.label}
				</Typography>
				<Typography className={item.content_class}>
					{responseData[item.attributeName]}
				</Typography>
			</>
		)
	}

	/**
	 * Render name's of the heading's.
	 * @param {object} item
	 * @param {number} index
	 */
	const renderName = (item, index) => {
		return (
			<>
				<Typography key={index} className={item.className}>
					{continuousIntegrationDetails[item.attributeName]
						? continuousIntegrationDetails[item.attributeName]
						: item.attributeName}
				</Typography>
			</>
		)
	}

	/**
	 * Render field details.
	 * @param {object} viewContinuousIntegrationFields
	 */
	const renderFields = (viewContinuousIntegrationFields) => {
		return viewContinuousIntegrationFields.map((item, i) => (
			<>
				{item.icon ? renderIcon(item, i) : renderName(item, i)}
				{item.apiConfig
					? renderApiConfiguration(
							continuousIntegrationDetails[item.apiConfig],
							i,
							item
					  )
					: null}
				{item.children
					? item.children.map((details, index) =>
							renderLabel(details, index, continuousIntegrationDetails)
					  )
					: null}
				{item.endPoints
					? renderEndPoints(
							continuousIntegrationDetails[item.endApiConfig],
							i,
							item
					  )
					: null}
				{item.apiMetadata
					? renderApiMetaData(
							continuousIntegrationDetails[item.metaDataApiConfig],
							i,
							item
					  )
					: null}
				{item.pipelineConfig
					? renderPipelineConfiguration(
							continuousIntegrationDetails[item.pipelineConfig],
							i,
							item
					  )
					: null}
				{item.channelConfig
					? renderChannelConfiguration(
							continuousIntegrationDetails[item.channelConfig],
							i,
							item
					  )
					: null}
				{item.metadata
					? renderMetaData(continuousIntegrationDetails[item.metadata], i, item)
					: null}
			</>
		))
	}

	/**
	 * Calling renderFields.
	 * @param {object} viewContinuousIntegrationFields
	 */
	const renderViewContinuousIntegration = (viewContinuousIntegrationFields) => {
		return viewContinuousIntegrationFields
			? renderFields(viewContinuousIntegrationFields)
			: null
	}

	/**
	 * to handle to cancel button while closing
	 */
	const handleButtonOnClick = () => {
		props.onCancel(false)
	}

	const handleEditIcon = () => {
		setOpenEditCI(true)
	}

	return openEditCI ? (
		<EditContinuousIntegration
			continuousIntegrationDetails={continuousIntegrationDetails}
			openDrawer={handleButtonOnClick}
		/>
	) : (
		<div className='view-continuous-integration'>
			<RightSideSlider onCancel={props.onCancel} drawerName='drawer'>
				<CloseOutlinedIcon
					className='close-icon'
					onClick={() => {
						handleButtonOnClick()
					}}
				/>
				<BusyIndicator>
					<div className='right-side-drawer'>
						{continuousIntegrationDetails
							? renderViewContinuousIntegration(
									viewContiniousIntegrationJsonData.fields
							  )
							: null}
					</div>
					<SliderFooter
						mode={modeType.READ}
						handleClose={handleButtonOnClick}
					/>
				</BusyIndicator>
			</RightSideSlider>
		</div>
	)
}
ViewContinuousIntegration.propTypes = {
	continuousIntegrationDetails: PropTypes.object,
	renderIcon: PropTypes.func,
	renderName: PropTypes.func,
	renderApiConfiguration: PropTypes.func,
	renderEndPoints: PropTypes.func,
	renderFields: PropTypes.func,
	renderPipelineConfiguration: PropTypes.func,
	renderViewContinuousIntegration: PropTypes.func,
}
