import { createSlice } from '@reduxjs/toolkit'
import * as asyncActions from './viewContinuousIntegration.asyncActions'

const initialState = {
	allViewContinuousIntegration: [],
	filter: '',
}

const slice = createSlice({
	name: 'viewContinuousIntegration',
	initialState,
	reducers: {
		// synchronous actions
		updateFilter(state, action) {
			state.filter = action.payload
		},
	},
	extraReducers: {
		// asynchronous actions
		[asyncActions.fetchAllViewContinuousIntegration.fulfilled]: (
			state,
			action
		) => {
			if (action.payload) {
				state.allViewContinuousIntegration = action.payload
			}
		},
	},
})

export default slice

export const { name, actions, reducer } = slice
