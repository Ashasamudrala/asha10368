import React, { Component } from 'react';
import './second.css';
import Footer from './footer';
class Second extends Component {
    render() {
        return (
            <div>
                <div>
                    <ul class="nav">
                        <li style={{ marginLeft: "200px" }} ><a href="#home" style={{ fontSize: "23px", color: "green", fontFamily: "serif" }}><b>Practise</b></a></li>
                        <li><a href="#news" style={{ fontSize: "20px", color: "black", fontFamily: "serif" }}>Explore</a></li>
                        <li><a href="#contact" style={{ fontSize: "20px", color: "green", fontFamily: "serif" }}>Courses</a></li>
                        <li style={{ float: "right", marginTop: "10px", marginRight: "50px" }}><img src=" https://d3qt53jevtbvi2.cloudfront.net/auth/avatar.png" style={{ height: "30px" }} /></li>
                        <li style={{ float: "right", marginTop: "20px", marginRight: "30px" }}><i class="fa fa-search" ></i></li>

                    </ul>

                </div>
                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/courses/AKTU-Banner-Desktop.png" alt="Los Angeles" style={{ width: "100" }} />
                            </div>

                            <div class="item">
                                <img src="https://cdncontribute.geeksforgeeks.org/wp-content/uploads/sudo_placement_banner-2-min.jpg" alt="Chicago" style={{ width: "100" }} />
                            </div>

                            <div class="item">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/courses/1200x300GeeksClassesMar.png" alt="New york" style={{ width: "100" }} />
                            </div>
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <h1 style={{ marginLeft: "150px", fontSize: "25px" }} ><b>DataStructures</b></h1>
                <div>
                    <div class="grid-container1">
                        <div class="grid-item1">
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/array_55.png" class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down" > Array</p>

                            </div>
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/tree_55.png " style={{ height: "50px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <p class="down" style={{ top: "50%" }} >Tree</p>

                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/linked_list_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Linkedlist</p>

                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/bst_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">BST</p>

                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/stack_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Stack</p>
                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/bst_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Heap</p>
                            </div>
                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/queue_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Queue</p>
                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/hashing_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Hashing</p>
                            </div>
                        </div>


                    </div>
                </div>
                <div>
                    <h1 style={{ marginLeft: "150px", fontSize: "25px" }} ><b>Algorithms</b></h1>
                    <div class="grid-container1">
                        <div class="grid-item1">
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/search_55.png" class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down" >Search</p>

                            </div>
                            <div class="child7">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/sorting_55.png " style={{ height: "50px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <p class="down" style={{ top: "50%" }} >Sorting</p>

                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/recursion_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Recursion</p>

                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/greedy_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">GREEDY ALGORITHM</p>

                            </div>

                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/bit_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Bit Pattern</p>
                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/dynamic_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Dynamic Programming</p>
                            </div>
                        </div>
                        <div class="grid-item1">
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/backtracking_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Back Tracking</p>
                            </div>
                            <div class="child8">
                                <img src="https://cdnpractice.geeksforgeeks.org/images/mathematical_55.png" style={{ height: "30px", width: "130px" }} class="btn-social" />
                                <br></br>
                                <br></br>
                                <br></br>
                                <p class="down">Mathematical</p>
                            </div>
                        </div>


                    </div>
                </div>
                <div>
                   <Footer/> 
                  
              </div>
            </div>
        );
    }
}

export default Second;