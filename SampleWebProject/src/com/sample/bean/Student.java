package com.sample.bean;

/**
 * This Student class is used to declare, set the values for the variables and
 * retrieve them
 * 
 * @author IMVIZAG
 *
 */

public class Student {

	// declaring the variables
	private int id;
	private  String name;
	private int age;
	private Address address;

	// getter and setter methods
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	
}
