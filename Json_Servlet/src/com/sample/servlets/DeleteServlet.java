package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteStudent
 */
@WebServlet("/DeleteStudent")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * this servlet method is used for deleting and to get the request from the html
	 * page and sends back the response to the user in the form of html
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		StudentService service = new StudentServiceImpl();
		boolean result = service.removeStudent(id);
		PrintWriter pw = response.getWriter();
		pw.println("<html><body>");
		if (result) {
			pw.println("<h1>successfully deleted</h2>");
		} else {
			pw.println("<h1> Deletion Failed </h1>");
		}

		pw.println("</body></html>");

	}

}
