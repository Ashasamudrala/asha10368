package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	/**
	 * this servlet method is used for insertion and to get the request from the
	 * html page and sends back the response to the user in the form of html
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
			
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
        
        PrintWriter out = response.getWriter();
		StudentService service = new StudentServiceImpl();
		Student st = service.findById(studentId);
		
		if(st == null)
		{
			boolean result = service.insertStudent(student);
			out.print("<html><body>");
			if(result) {
				out.print("<h2>Student Data Inserted</h2>");
			}
			else {
				out.print("<h2>Something wrong</h2>");
			}
		}
		else {
			out.println("<h2>Id already Exist. \nData can't be Inserted</h2>");
		}
		
		out.print("</body></html>");
		out.close();
	}
}
