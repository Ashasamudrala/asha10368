import React from 'react';
import { BrowserRouter, Route, Switch ,Redirect} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import Dashboard from './components/Dashboard/Dashboard';
import Login from './components/Login/Login';
import masterAdminLogin from './components/masterAdminLogin/masterAdminLogin';
import './App.css';
import store from './redux/store';
const App = props => {
  return (
    <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Redirect path='/' exact to='/adminConsole' />
        <Route path='/adminConsole' exact component={Login} />
        <Route path='/masterAdminConsole' exact component={masterAdminLogin} />
        <Route path="/users" component={Dashboard} />
        <Route path="/org" component={Dashboard} />
        <Route path="/server-config" component={Dashboard} />
        <Route path="/myProfile" component={Dashboard} />
      </Switch>
    </BrowserRouter>
    </Provider>
  )
}
export default App;
