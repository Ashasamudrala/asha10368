import {FETCH_POSTS,NEW_POST} from './types';
export const localUrl = "http://localhost:3002";
export const fetchPosts =()=>dispatch=>{
    console.log('fetching');
        fetch(`https://jsonplaceholder.typicode.com/posts`)
        .then(res=>res.json())
        .then(posts=>dispatch({
            type:FETCH_POSTS,
            payload:posts
        }));
}