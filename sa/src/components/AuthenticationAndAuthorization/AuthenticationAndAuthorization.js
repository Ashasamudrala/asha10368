import React, { Component } from 'react';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
class AuthenticationAndAuthorization extends Component {
  render() {
    return <div className="main">
    <h2 className="title">Enable Authentication and Authorization For Web Services</h2>
    <p className="desc">Enable authentication and authorization for web services. To enable, move the selected web services to right. To disable move to left.</p>
    <p className="desc">
      <b>Note :</b>You must refresh the system cache of product servers for this change to take effect.</p>
    <div className="col-md-12 form-group row">
      <label className="col-md-3 col-form-label vertical-center">Web Services</label>
      <div className="col-md-9">
        <SwappingSelectBox
          firstHeader={'Available Attributes for encryption'}
          secondHeader={'	Attributes Selected for encryption'}
        />
      </div>
    </div>
    <div className="form-group form-submit-button row">
        <input className="secondary-btn" id="encryButton" type="submit" value="Save"></input>
      </div>
    </div>;
  }
}

export default AuthenticationAndAuthorization;
