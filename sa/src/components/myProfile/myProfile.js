import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import ReactTooltip from 'react-tooltip';
import './myProfile.css';

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: []
    }

  }
  render() {
    return <div className="main">
      <div className="row">
        <div className="col-md-4">Username: GLADMIN</div>
        <div className="col-md-4">Organization: DEFAULTORG</div>
        <div className="col-md-4">Role: Global Administrator</div>
      </div>
      <h2 className="title">My Profile</h2>
      <p className="desc">Update your personal details and preferences.</p>
      <div className="col-sm-7">
        <span className="ecc-h1 row">Personal Information</span>
        <SingleInput
          title={'First Name'}
          inputType={'text'}
          required={true}
          name={'first-name'} />
        <SingleInput
          title={'Middle Name'}
          inputType={'text'}
          name={'middle-name'} />
        <SingleInput
          title={'Last Name'}
          required={true}
          inputType={'text'}
          name={'last-name'} />
        <div className="div-seperator">
          <span className="ecc-h1 row">Email Address(es) Telephone Number(s)</span>
          <SingleInput
            title={'Email'}
            inputType={'text'}
            required={true}
            name={'email'} />
          <SingleInput
            title={'Phone Number'}
            required={true}
            inputType={'text'}
            name={'phone-number'} />
        </div>
        <div className="div-seperator">
          <span className="ecc-h1 row">Change Password</span>
          <SingleInput
            title={'Current Password'}
            inputType={'text'}
            name={'curr-password'} />
          <span data-tip data-for='password-criteria'>
            <SingleInput
              title={'New Password'}
              inputType={'text'}
              name={'new-password'} />
            <SingleInput
              title={'Confirm Password'}
              inputType={'text'}
              name={'confirm-password'} />
          </span>
          <ReactTooltip id='password-criteria' place="right" type="dark" >
            <ul className="password-criteria">
              Password must be at least 6 characters long, and must include
              <li>At least 1 numeric characters</li>
              <li>At least 4 alphabetic characters( 0 LowerCase, 0 UpperCase)</li>
              <li>At least 1 special characters  ( Allowed special characters are    !@#$%^&amp;*()_+ )</li></ul>
          </ReactTooltip>
        </div>
        <div className="div-seperator">
          <span className="ecc-h1 row">Administrator Preferences</span>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Enable Preferred Organization</label>
            <div className="col-sm-8">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="customCheck1" />
                <label className="custom-control-label" htmlFor="customCheck1"></label>
              </div>
            </div>
          </div>
          <Select
            name={'pref-org'}
            title={'Preferred Organization'}
            required={true}
            options={this.state.roleOptions}
            placeholder={'Select'} />
          <SingleInput
            title={'Date Time Format'}
            inputType={'text'}
            name={'date-time'}
            content={'MM/dd/yyyy HH:mm:ss (z)'} 
            imgActivate={true}
            />
            <Select
            name={'Locale'}
            title={'Locale'}
            required={true}
            options={this.state.roleOptions}
            placeholder={'Select'} />
          <Select
            name={'pref-org'}
            title={'Preferred Organization'}
            required={true}
            options={this.state.roleOptions}
            placeholder={'Select'} />
        </div>
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Save"></input>
      </div>
    </div>;
  }
}

export default MyProfile;
