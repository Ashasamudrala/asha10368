import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import TextArea from '../../shared/components/TextArea/TextArea';

class CreateOrganization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: '',
      roleOptions: [],
      petSelections: ["praveen"],
      selectedPets: ["praveen"]
    }
  }
  render() {
    return <div className="main"><h2 className="title">Create Organization</h2>
      <p className="desc">In addition to the basic organization information, specify the authentication mechanism to be used for logging in administrators and the location of the repository where the user data is available.</p>
      <span><b>Note:</b>You must refresh the system cache of product servers for the newly created organization to take effect.</span>
      <div className="col-sm-6">
        <div className="div-seperator">
          <span className="ecc-h1 row orgSectionLabels">Organization Information</span>
          <SingleInput
            title={'Organization Name'}
            inputType={'text'}
            required={true}
            name={'org-name'} />
          <SingleInput
            title={'Display Name'}
            required={true}
            inputType={'text'}
            name={'display-name'} />
          <SingleInput
            title={'Description'}
            inputType={'text'}
            name={'description'} />
          <Select
            name={'admin-auth'}
            title={'Administrator Authentication Mechanism'}
            required={true}
            options={this.state.roleOptions}
            placeholder={'Select'} />
        </div>
        <div className="div-seperator">
          <span className="ecc-h1 row orgSectionLabels">Key Label Configuration</span>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Use Global Key</label>
            <div className="col-sm-8">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" defaultChecked id="customCheck1" />
                <label className="custom-control-label" htmlFor="customCheck1"></label>
              </div>
            </div>
          </div>
          <SingleInput
            title={'Key Label'}
            required={true}
            disabled={true}
            inputType={'text'}
            name={'key-label'} />
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Storage Type</label>
            <div className="col-sm-8 form-inline">
              <RadioGroup
                title={'Current Users'}
                setName={'users'}
                type={'radio'}
                options={this.state.petSelections}
                selectedOptions={this.state.selectedPets} />
              <div className="px-md-2">
                <RadioGroup
                  title={'Current Users'}
                  setName={'users'}
                  type={'radio'}
                  options={this.state.petSelections}
                  selectedOptions={this.state.selectedPets} />
              </div>
            </div>
          </div>

          <div className="div-seperator">
            <span className="ecc-h1 row orgSectionLabels">Localization Configuration</span>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">Use Global Configuration</label>
              <div className="col-sm-8">
                <div className="custom-control custom-checkbox">
                  <input type="checkbox" className="custom-control-input" defaultChecked id="customCheck1" />
                  <label className="custom-control-label" htmlFor="customCheck1"></label>
                </div>
              </div>
            </div>
            <SingleInput
              title={'Date Time Format'}
              required={true}
              disabled={true}
              inputType={'text'}
              name={'Date-Time'} />
            <Select
              name={'preffered'}
              title={'Preferred Locale'}
              required={true}
              options={this.state.roleOptions}
              placeholder={'Select'} />
          </div>

          <div className="div-seperator">
            <span className="ecc-h1 row orgSectionLabels">User Data Location</span>
            <Select
              name={'rep-type'}
              title={'Repository Type'}
              required={true}
              options={this.state.roleOptions}
              placeholder={'Select'} />
          </div>
          <div className="div-seperator">
            <span className="ecc-h1 row orgSectionLabels">Session Configuration</span>
            <SingleInput
              title={'Idle Timeout (in seconds)'}
              required={true}
              inputType={'text'}
              name={'idle-time'} />
          </div>
          <div className="div-seperator">
            <span className="ecc-h1 row orgSectionLabels">Multi-Factor Authentication Configuration</span>
            <div className="form-group row">
              <label className="col-sm-4 col-form-label">Enable Multi-Factor Authentication</label>
              <div className="col-sm-8">
                <div className="custom-control custom-checkbox">
                  <input type="checkbox" className="custom-control-input" defaultChecked id="customCheck1" />
                  <label className="custom-control-label" htmlFor="customCheck1"></label>
                </div>
              </div>
            </div>
            <TextArea
              title={'Accepted Domains for Email Factor (.com)'}
              resize={false}
              content={this.state.description}
              name={'currentPetInfo'}
              placeholder={''} />
            <Select
              name={'2FA'}
              title={'Select the 2FA method'}
              required={true}
              options={this.state.roleOptions}
              placeholder={'Select'} />
            <Select
              name={'Delivery-channel'}
              title={'Delivery channel'}
              required={true}
              options={this.state.roleOptions}
              placeholder={'Select'} />
          </div>
        </div>
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="NEXT"></input>
      </div>
    </div>;
  }
}

export default CreateOrganization;
