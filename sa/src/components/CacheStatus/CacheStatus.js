import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';

class CacheStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      petSelections: ["praveen"],
      selectedPets: ["praveen"],
      roleOptions: []
    };
  }
  render() {
    return <div className="main"><h2 className="title">Search Cache Refresh Request</h2>
      <p className="desc">Select a Request ID to check the status of the cache refresh request.</p>
      <div className="col-sm-6 div-seperator">
        <Select
          name={'req-id'}
          title={'Request ID'}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <Select
          name={'status'}
          title={'Status'}
          options={this.state.roleOptions}
          placeholder={'Select'} />
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Search"></input>
      </div>
    </div>;
  }
}

export default CacheStatus;
