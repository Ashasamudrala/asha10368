import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import './RiskAnalyticsConn.css';
import Select from '../../shared/components/Select/Select';
class RiskAnalyticsConn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: [],
    }
  }
  render() {
    return <div className="main">
      <h2 className="title">Risk Analytics Transaction Server Management Connectivity</h2>
      <p className="desc">Specify the configurations that will be used by the Administration Console to connect to the Risk Analytics Transaction Server Management.</p>
      <div className="col-sm-6">
        <SingleInput
          title={'Server'}
          inputType={'text'}
          name={'Server-name'} />
        <SingleInput
          title={'Server Management Port'}
          inputType={'text'}
          name={'port'} />
        <Select
          name={'transport'}
          title={'Transport'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Server CA Root Certificate</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Client Certificate-Key Pair in PKCS#12</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <SingleInput
          title={'Client PKCS#12 Password'}
          inputType={'text'}
          name={'client-pass'} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE"></input>
        </div>
      </div>
      <h2 className="title">Case Management Server Management Connectivity</h2>
      <p className="desc">Specify the configurations that will be used by the Administration Console to connect to the Case Management Server Management.</p>
      <div className="col-sm-6">
        <SingleInput
          title={'Server'}
          inputType={'text'}
          name={'Server-name'} />
        <SingleInput
          title={'Server Management Port'}
          inputType={'text'}
          name={'port'} />
        <Select
          name={'transport'}
          title={'Transport'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Server CA Root Certificate</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Client Certificate-Key Pair in PKCS#12</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <SingleInput
          title={'Client PKCS#12 Password'}
          inputType={'text'}
          name={'client-pass'} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE"></input>
        </div>
      </div>
      <h2 className="title">Risk Analytics Administration Connectivity</h2>
      <p className="desc">Specify the configurations that will be used by the Administration Console to connect to the Risk Analytics Transaction Server Administration.</p>
      <div className="col-sm-6">
        <SingleInput
          title={'Server'}
          inputType={'text'}
          name={'Server-name'} />
        <SingleInput
          title={'Server Management Port'}
          inputType={'text'}
          name={'port'} />
        <Select
          name={'transport'}
          title={'Transport'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Server CA Root Certificate</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Client Certificate-Key Pair in PKCS#12</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <SingleInput
          title={'Client PKCS#12 Password'}
          inputType={'text'}
          name={'client-pass'} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE"></input>
        </div>
      </div>
      <h2 className="title">Case Management Server Connectivity</h2>
      <p className="desc">Specify the configurations that will be used by the Administration Console to connect to the Case Management Server instance.</p>
      <div className="col-sm-6">
      <SingleInput
          title={'Server'}
          inputType={'text'}
          name={'Host'} />
        <SingleInput
          title={'Host'}
          inputType={'text'}
          name={'Backup-Host'} />
        <SingleInput
          title={'Backup Host'}
          inputType={'text'}
          name={'port'} />
        <Select
          name={'transport'}
          title={'Transport'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Server CA Root Certificate</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Client Certificate-Key Pair in PKCS#12</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`ca-cert`}
            />
          </div></div>
        <SingleInput
          title={'Client PKCS#12 Password'}
          inputType={'text'}
          name={'client-pass'} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE"></input>
        </div>
      </div>
    </div>
  }
}

export default RiskAnalyticsConn;
