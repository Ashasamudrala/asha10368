import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';

class UDSConnectivity extends Component {
  constructor(props) {
    super(props);
    this.state = { roleOptions: [] }
  }
  render() {
    return <div className="main">
      <h2 className="title">User Data Service Connectivity Configuration</h2>
      <p className="desc">Configure the User Data Service (UDS) to access user information.</p>
      <p className="desc">
        <b>Note 1:</b>It is optional to configure SSL between UDS and CA Products.</p>
      <p className="desc">
        <b>Note 2:</b>You must refresh the system cache of product servers for this change to take effect.</p>
      <span class="ecc-h1">User Data
Service Connectivity Configuration</span>
      <div className="col-sm-7">
        <Select
          name={'Protocol '}
          title={'Protocol '}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <SingleInput
          title={'Host'}
          required={true}
          inputType={'text'}
          name={'Host'} />
        <SingleInput
          title={'Port'}
          required={true}
          inputType={'text'}
          name={'port'} />
        <SingleInput
          title={'Application Context Root'}
          required={true}
          inputType={'text'}
          name={'Application-Context'} />
        <SingleInput
          title={'Connection Timeout (in milliseconds)'}
          required={true}
          inputType={'text'}
          name={'Connection-Timeout'} />
        <SingleInput
          title={'Read Timeout (in milliseconds)'}
          required={true}
          inputType={'text'}
          name={'Read-Timeout'} />
        <SingleInput
          title={'Idle Timeout (in milliseconds)'}
          required={true}
          inputType={'text'}
          name={'Idle-Timeout'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Server Root Certificate</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`root-cert`}
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Client Certificate</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`client-cert`}
            />
          </div></div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Client Private Key</label>
          <div className="col-md-8">
            <input
              type="file"
              name={`private-key`}
            />
          </div></div>
        <SingleInput
          title={'Minimum Connections'}
          inputType={'text'}
          required={true}
          name={'min-conn'} />
        <SingleInput
          title={'Maximum Connections'}
          inputType={'text'}
          required={true}
          name={'max-conn'} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE"></input>
        </div>
      </div></div>;
  }
}

export default UDSConnectivity;
