import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';

class AMDSConnectivity extends Component {
  render() {
    return <div className="main">
      <h2 className="title">AMDS Connectivity</h2>
      <p className="desc">Configure AMDS Profiles for Email/SMS OTP/Voice OTP</p>
      <div className="col-sm-6">
        <SingleInput
          title={'AMDS URL'}
          required={true}
          inputType={'text'}
          name={'AMDS URL'} />
        <SingleInput
          title={'API Name'}
          required={true}
          inputType={'text'}
          name={'API Name'} />
        <SingleInput
          title={'Profile Name'}
          required={true}
          inputType={'text'}
          name={'Profile Name'} />
        <SingleInput
          title={'Profile Password'}
          required={true}
          inputType={'text'}
          name={'Profile Password'} />
        <SingleInput
          title={'Connection Timeout'}
          inputType={'text'}
          name={'Connection Timeout'} />
        <SingleInput
          title={'Read Timeout'}
          inputType={'text'}
          name={'Read Timeout'} />
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="caseButton" type="submit" value="SAVE"></input>
        </div>
      </div>
    </div>;
  }
}

export default AMDSConnectivity;
