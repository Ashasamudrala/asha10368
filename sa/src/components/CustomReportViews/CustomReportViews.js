import React, { Component } from 'react';
import Checkbox from '../../shared/components/Checkbox/Checkbox';

class CustomReportViews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterNames: [],
      storeSelected: [],
      petSelections: ["praveen"],
      selectedPets: ["praveen"],
      allChecked: false,
      list: [
        { id: 1, name: "item1", isChecked: false },
        { id: 2, name: "item2", isChecked: false },
        { id: 3, name: "item3", isChecked: false },
        { id: 4, name: "Machine", isChecked: false },
        { id: 5, name: "item5", isChecked: false },
        { id: 6, name: "item6", isChecked: false },
        { id: 7, name: "item7", isChecked: false },
        { id: 8, name: "item8", isChecked: false },
        { id: 9, name: "item9", isChecked: true },
        { id: 10, name: "Machine 1", isChecked: false },
        { id: 11, name: "item10", isChecked: true },
        { id: 12, name: "item11", isChecked: false }
      ]
    };
  }
  manipulatedList(getList){
    var getfilterNames = getList.map((eachlist) => {
      return eachlist.name
    })
    var getTrueNames = getList.filter((eachlist) => {
      return eachlist.isChecked
    })
    var getchecked = getTrueNames.map((eachlist) => {
      return eachlist.name
    })
    this.setState({ filterNames: getfilterNames, storeSelected: getchecked });
  }
  componentWillMount() {
   this.manipulatedList(this.state.list);
  }
  handleChange = e => {
    let itemName = e.target.name;
    let checked = e.target.checked;
      let { list, allChecked } = this.state;
      if (itemName === "checkAll") {
        this.setState({allChecked:checked});
        list = list.map(item => ({ ...item, isChecked: checked }));
        this.manipulatedList(list);
      } else {
        list = list.map(item =>
          item.name === itemName ? { ...item, isChecked: checked } : item
        );
        allChecked = list.every(item => item.isChecked);
      }
      return { list, allChecked };
  };
  render() {
    console.log('allChecked',this.state.allChecked);
    return <div className="main">
      <h2 className="title">Custom Report Views</h2>
      <p className="desc">Select the table columns that must be made available to all organizations to display in the Analyze Transaction report.</p>
      <hr></hr>
      <div className="div-seperator">
        <div className="col-md-6 form-inline row">
          <h6 className="apply-bold col-md-6 row">TRANSACTION DETAILS</h6>
          <span className="col-md-6">
            <Checkbox
              type={'checkbox'}
              setName="checkAll"
              controlFunc={this.handleChange}
              options={["Selectall"]}
              selectedOptions={this.state.allChecked ? ["Selectall"]:[]} />
          </span>
        </div>
        <Checkbox
          type={'checkbox'}
          checked={this.state.allChecked}
          options={this.state.filterNames}
          selectedOptions={this.state.storeSelected} />
      </div>
      <hr></hr>
      <div className="div-seperator">
        <div className="col-md-8 form-inline row">
          <h6 className="apply-bold col-md-6 row">BASIC TRANSACTION DETAILS</h6>
          <span className="col-md-6">
            <Checkbox
              type={'checkbox'}
              setName="checkAll"
              controlFunc={this.handleChange}
              options={["Selectall"]}
              selectedOptions={this.state.allChecked ? ["Selectall"]:[]} />
          </span>
        </div>
        <Checkbox
          type={'checkbox'}
          checked={this.state.allChecked}
          options={this.state.filterNames}
          selectedOptions={this.state.storeSelected} />
      </div>
      <hr></hr>
      <div className="div-seperator">
        <div className="col-md-8 form-inline row">
          <h6 className="apply-bold col-md-6 row">OTHER TRANSACTION DETAILS</h6>
          <span className="col-md-6">
            <Checkbox
              type={'checkbox'}
              setName="checkAll"
              controlFunc={this.handleChange}
              options={["Selectall"]}
              selectedOptions={this.state.allChecked ? ["Selectall"]:[]} />
          </span>
        </div>
        <Checkbox
          type={'checkbox'}
          checked={this.state.allChecked}
          options={this.state.filterNames}
          selectedOptions={this.state.storeSelected} />
      </div>
      <hr></hr>
      <div className="div-seperator">
        <div className="col-md-6 form-inline row">
          <h6 className="apply-bold col-md-6 row">LOCATION DETAILS</h6>
          <span className="col-md-6">
            <Checkbox
              type={'checkbox'}
              setName="checkAll"
              controlFunc={this.handleChange}
              options={["Selectall"]}
              selectedOptions={this.state.allChecked ? ["Selectall"]:[]} />
          </span>
        </div>
        <Checkbox
          type={'checkbox'}
          checked={this.state.allChecked}
          options={this.state.filterNames}
          selectedOptions={this.state.storeSelected} />
      </div>
      <hr></hr>
      <div className="div-seperator">
        <div className="col-md-6 form-inline row">
          <h6 className="apply-bold col-md-6 row">DEVICE DETAILS</h6>
          <span className="col-md-6">
            <Checkbox
              type={'checkbox'}
              setName="checkAll"
              controlFunc={this.handleChange}
              options={["Selectall"]}
              selectedOptions={this.state.allChecked ? ["Selectall"]:[]} />
          </span>
        </div>
        <Checkbox
          type={'checkbox'}
          checked={this.state.allChecked}
          options={this.state.filterNames}
          selectedOptions={this.state.storeSelected} />
      </div>
    </div>;
  }
}

export default CustomReportViews;
