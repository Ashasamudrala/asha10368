import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import './CreateCustomRole.css';

class CreateCustomRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: []
    }

  }
  render() {
    return <div className="main"><h2 className="title">Create Custom Role</h2>
      <p className="desc">Create a custom administrator role. This role must be derived from one of the predefined roles and can have a subset of the base role's privileges.</p>
      <div className="col-sm-5">
        <span className="ecc-h1 row">Role Details</span>
        <SingleInput
          title={'Role Name'}
          inputType={'text'}
          required={true}
          name={'role-name'} />
        <SingleInput
          title={'Role Display Name'}
          required={true}
          inputType={'text'}
          name={'role-display-name'} />
        <SingleInput
          title={'Role Description'}
          inputType={'text'}
          name={'role-description'} />
        <Select
          name={'role-based'}
          title={'Role Based On'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <span className="ecc-h1 row">Set Privileges</span>
      </div>
      <SwappingSelectBox/>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Create"></input>
      </div>
    </div>;
  }
}

export default CreateCustomRole;
