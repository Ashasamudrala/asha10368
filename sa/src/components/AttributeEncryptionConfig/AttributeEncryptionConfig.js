import React, { Component } from 'react';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import Select from '../../shared/components/Select/Select';

class AttributeEncryptionConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: []
    };
  }
  render() {
    return <div className="main">
      <h2 className="title">Attribute Encryption Set Configuration</h2>
      <p className="desc">Configure sensitive attribute(s) that need encryption and masking.</p>
      <p className="desc">
        <b>Note :</b>You must refresh the system cache of product servers for this change to take effect.</p>
      <div className="col-md-12 form-group row">
        <label className="col-md-3 col-form-label vertical-center">Select Attribute(s) for Encryption</label>
        <div className="col-md-9">
          <SwappingSelectBox
            firstHeader={'Available Attributes for encryption'}
            secondHeader={'	Attributes Selected for encryption'}
          />
        </div>
      </div>
      <div className="col-md-5">
        <span class="ecc-h1 row">Data Masking Configuration</span>
        <Select
          name={'req-id'}
          title={'Request ID'}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <div className="form-group row">
          <label className="col-md-4 col-form-label">Start Length:</label>
          <div className="col-md-4">
              <input type="text" class="form-control"></input>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-md-4 col-form-label">End Length:</label>
          <div className="col-md-4">
              <input type="text" class="form-control"></input>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-md-4 col-form-label">Masking Character:</label>
          <div className="col-md-4">
              <input type="text" class="form-control"></input>
          </div>
        </div>
        <div className="form-group form-submit-button row">
        <input className="secondary-btn" id="encryButton" type="submit" value="Save"></input>
      </div>
      </div>
    </div>;
  }
}

export default AttributeEncryptionConfig;
