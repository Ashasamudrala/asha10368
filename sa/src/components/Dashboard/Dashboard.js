import React, { Component } from 'react';
import './Dashboard.css';
import DynamicNavbar from '../DynamicNavbar/DynamicNavbar';
import { Route, Link ,Redirect ,Switch} from 'react-router-dom';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openLogout: false,
    }
  }
  openLogoutDropdown = () => {
    this.setState({ openLogout: !this.state.openLogout });
  }
  Logout=()=>{
    this.props.history.push('/');
  }
  activateMyProfile=()=>{
    this.props.history.push('/myProfile');
  }
  render() {
    const match= this.props.match.url;
    return <div className="wrapper">
      <nav className="navbar navbar-expand-lg">
        <a className="navbar-brand"><img src="/images/ca_logo.png" height="35"></img></a>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item  ecc-h1">
              CA Administration Console
            </li>
          </ul>
          <div className="user-logout" onClick={this.openLogoutDropdown}>
            <span className="navbar-text tertiary-link">
              MASTERADMIN
           </span>
            {this.state.openLogout ? <img src="/images/icon_up_arrow.png"></img> : <img src="/images/icon_down_arrow.png"></img>}
            {this.state.openLogout ?
              <div className="user-dropdown-menu">
                <div className="text-center">Last Login Time	08/01/2019 15:18:24 (GMT+05:30)</div>
                <div className="text-center logout-buttons">
                  <button className="button profile-button" onClick={this.activateMyProfile}>My Profile</button>
                </div>
                <div className="text-center logout-buttons">
                  <button className="button sign-out" onClick={this.Logout}>Sign out</button>
                </div>
              </div> : ''}
          </div>
        </div>
      </nav>
      <div className="second-nav">
        <nav className="second-nav-items">
          <Link to='/users'><span className={`${match ==='/users' ? 'second-nav-text active' : 'second-nav-text'}`}>Users and Administrators</span></Link>
          <Link to='/org'><span className={`${match ==='/org' ? 'second-nav-text active' : 'second-nav-text'}`}>Organizations</span></Link>
          <Link to='/server-config'><span className={`${match ==='/server-config' ? 'second-nav-text active' : 'second-nav-text'}`}>Services and Server Configurations</span></Link>
          <Link to='/reports'><span className={`${match ==='/reports' ? 'second-nav-text active' : 'second-nav-text'}`}>Reports</span></Link>
        </nav>
      </div>
      <Switch>
       <Route path={`/users`}  component={DynamicNavbar} />
       <Route path={`/org`}  component={DynamicNavbar} />
       <Route path={`/server-config`}  component={DynamicNavbar} />
       <Route path={`/myProfile`}  component={DynamicNavbar} />
      </Switch>
      <div id="footer">
        <div className="edl-footer" id="edlCommonFooter">
          Copyright © <span id="footerCurrentYear">2019</span> CA. All rights reserved.
      </div>
      </div>
    </div>;
  }
}

export default Dashboard;

