import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';

class UpdateCustomRole extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: []
    }
  }
  render() {
    return <div className="main"><h2 className="title">Update Custom Role</h2>
      <p className="desc">Update a custom role.</p>
      <div className="col-sm-5">
        <span className="ecc-h1 row">Role Details</span>
        <Select
          name={'role-name'}
          title={'Role Name'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <SingleInput
          title={'Role Display Name'}
          required={true}
          disabled={true}
          inputType={'text'}
          name={'role-display-name'} />
        <SingleInput
          title={'Role Description'}
          disabled={true}
          inputType={'text'}
          name={'role-description'} />
        <SingleInput
          title={'Role Based On'}
          required={true}
          disabled={true}
          inputType={'text'}
          name={'role-based-on'} />
        <span className="ecc-h1 row">Set Privileges</span>
      </div>
      <SwappingSelectBox />
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Create"></input>
      </div>
    </div>;
  }
}

export default UpdateCustomRole;
