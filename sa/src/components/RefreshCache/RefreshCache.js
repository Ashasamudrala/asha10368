import React, { Component } from 'react';

class RefreshCache extends Component {
  render() {
    return <div className="main"><h2 className="title">Refresh Cache</h2>
      <p className="desc">Refresh the cache configuration of all the product servers. If 'Refresh Organization Configuration' is selected, the cache of all the Organizations managed by you is refreshed.</p>
      <div className="col-sm-7 div-seperator">
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">
            Refresh System Configuration
                      </label>
          <div className="col-sm-8">
            <div className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id="customCheck1" />
              <label className="custom-control-label" htmlFor="customCheck1"></label>
            </div>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">
          Refresh Organization Configuration	
                      </label>
          <div className="col-sm-8">
            <div className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id="customCheck2" disabled="disabled"/>
              <label className="custom-control-label" htmlFor="customCheck2"></label>
            </div>
          </div>
        </div>
        <div className="form-group form-submit-button row">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="OK"></input>
      </div>
      </div>
    </div>;
  }
}

export default RefreshCache;
