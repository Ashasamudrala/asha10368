import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl } from '../../shared/utlities/constants';
import { connect } from 'react-redux';
import {fetchPosts} from '../../redux/actions/postActions';
import './Login.css';
const orgUrl = "arcotadmin/organization";
const adminUrl = "arcotadmin/adminlogin";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgName: '',
      loginError: false,
      orgError: false,
      hideDiv: false,
      hideCredentials: false,
      usernameError: false,
      passwordError: false,
      username: '',
      password: '',
      userAuth:false
    };
  }
  componentWillMount=()=>{
    console.log('this.props',this.props);
    this.props.fetchPosts(adminUrl);
  }
  handleChange = (e) => {
    this.setState({ orgName: e.target.value, loginError: false, orgError: false });
  }
  onNextClick = () => {
    const { orgName } = this.state;
    if (orgName === "" && orgName.trimLeft().length === 0) {
      this.setState({ orgName: '', loginError: true, orgError: false });
    } else if (orgName !== "" && orgName.toLowerCase().trim() === "defaultorg") {
      const checkOrg = {
        method: 'GET',
        url: `${serverUrl}/${orgUrl}`,
        data: { "organizationName": orgName }
      };
      const checkOrgStatus = getService(checkOrg);
      checkOrgStatus.then((getstatus) => {
        // if(getstatus.data.errorList.length && getstatus.data.errorList[0].errorCode === 401){
        //  alert(getstatus.data.errorList[0].errorMsg);
        // }
        if (getstatus.status === 200) {
          alert(getstatus.data.message);
        }
      })
      this.setState({ hideCredentials: true });
    } else if (orgName !== "" && orgName.toLowerCase().trim() !== "defaultorg") {
      this.setState({ loginError: false, orgError: true });
    }
  }
  hideCurrentDiv = () => {
    this.setState({ hideDiv: true });
  }
  handleCredentials = (e,msg) => {
    if(msg === "username"){
       this.setState({username:e.target.value});
       if(e.target.value.length){
        this.setState({usernameError:false});
       }else{
        this.setState({usernameError:true});
       }
    }else if(msg === "password"){
      this.setState({password:e.target.value});
      if(e.target.value.length){
        this.setState({passwordError:false});
       }else{
        this.setState({passwordError:true});
       }
    }
  }
  onRedirectNext = () => {
    if (this.state.username && this.state.password) {
      if(this.state.username !== 'gladmin' && this.state.password !=='dost1234#'){
          this.setState({userAuth:true});
          setTimeout(() => {
            this.setState({userAuth:false});
          }, 3000);
          return;
      }else{
      const checkAdmin = {
        method: 'GET',
        url: `${serverUrl}/${adminUrl}`,
        data: {
          "organizationName": this.state.orgName,
          "username": this.state.username,
          "password": this.state.password
        }
      };
      const checkAdminStatus = getService(checkAdmin);
      checkAdminStatus.then((getAdminStatus) => {
        if (getAdminStatus.status === 200) {
          console.log(getAdminStatus.data);
          this.props.history.push('/users');
        }
      })
    }
    } else {
      if (this.state.username === '') {
        this.setState({ usernameError: true });
      }
      if (this.state.password === '') {
        this.setState({ passwordError: true });
      }
      return
    }
  }
  render() {
    console.log('posts',this.props.posts);
    const { orgName = '', loginError, orgError, usernameError, passwordError,userAuth } = this.state;
    return (
      <div className="login-parent">
        <div className="eupopup-parent">
          {!this.state.hideDiv ?
            <div className="eupopup-container">
              <p className="eupopup-body">Access to this system may result in sensitive data such as cookies, cached data, or downloaded reports being stored on your computer.</p>
              <center><input type="submit" onClick={this.hideCurrentDiv} className="secondary-btn" value="Click here" />
                <span className="hide-message">if you do not want to see this message again</span>
              </center>
            </div> : ''}
        </div>
        <div className="wrapper">
          <div className="">
            <div className="clearfloat edl-login-box">
              <div id="brand-holder">
                <div>
                  <img src="/images/ca_logo.png"></img>
                  <p className="ecc-h1">Administration Console</p>
                </div>
              </div>
              <div id="form-holder">
                <div className="form-header" id="signin-header">SIGN IN</div>
                <form name="bamForm" method="post" >
                </form>

                <div id="loginbox">
                  {!this.state.hideCredentials ?
                    <div>
                      <p className="body-font margin-top-8">Organization Name</p>
                      <div className="edl_login_input">
                        <input
                          name="orgName"
                          title="Please enter your organization name."
                          className={`loginTextBox ${loginError ? ' error' : ''}`}
                          autoFocus=""
                          required=""
                          onChange={this.handleChange}
                          value={orgName}
                        />
                        <div className={`${loginError ? 'error-msg' : 'no-msg'}`}>Please enter your organization name.</div>
                      </div>
                      <div>
                        <div className={`${orgError ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">The Organization Name is not found. Please check if the Organization Name is correct or contact your administrator.
                    </div>
                      </div></div> : this.state.orgName.toUpperCase()}
                  {this.state.hideCredentials ?
                    <div>
                      <div className="body-font">Username</div>
                      <div className="edl_login_input">
                        <input name="userName" title="Please enter username." className={`loginTextBox ${usernameError ? ' error' : ''}`} autoComplete="off" type="text" onChange={e => this.handleCredentials(e, "username")} />
                        <div className={`${usernameError ? 'error-msg' : 'no-msg'}`}>Please enter username.</div>
                      </div>
                      <div className="body-font">Password</div>
                      <div className="edl_login_input">
                        <input name="userPassword" title="Please enter password." className={`loginTextBox ${passwordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, "password")} />
                        <div className={`${passwordError ? 'error-msg' : 'no-msg'}`}>Please enter password.</div>
                      </div>
                      <div className={`${userAuth ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">Authentication failed.</div>
                      </div> : ''}
                  {!this.state.hideCredentials ?
                    <input className="button edl_btn_main left_align margin-top-8 user-sign-in" onClick={this.onNextClick} type="submit" value="NEXT" /> :
                    <input className="button edl_btn_main left_align margin-top-8 user-sign-in" onClick={this.onRedirectNext} type="submit" value="SIGN IN" />
                  }
                </div>
                <br className="clearfloat" />
                <br />
                <div className="eupopup"></div>

              </div>
            </div>
          </div>
          <div className="push"></div>

        </div>
        <div id="footer">

          <div className="edl-footer">
            Copyright ©
           <span id="footerCurrentYear"> 2019 </span>
            CA. All rights reserved.
        </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps=state=>({
  posts:state.posts.items
})
export default connect(mapStateToProps,{fetchPosts})(Login);
