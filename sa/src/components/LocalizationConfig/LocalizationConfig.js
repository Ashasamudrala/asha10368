import React, { Component } from 'react';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import Select from '../../shared/components/Select/Select';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import './LocalizationConfig.css';
class LocalizationConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: [],
      isoLangs: [
        "Abkhaz",
        "Afar",
        "Afrikaans",
        "Akan",
        "Albanian",
        "Amharic",
        "Arabic",
        "Aragonese",
        "Armenian",
        "Assamese",
        "Avaric",
        "Avestan",
        "Aymara",
        "Azerbaijani",
        "Bambara",
        "Bashkir",
        "Basque",
        "Belarusian",
        "Bengali",
        "Bihari",
        "Bislama",
        "Bosnian",
        "Breton",
        "Bulgarian",
        "Burmese",
        "Catalan; Valencian",
        "Chamorro",
        "Chechen",
        "Chichewa; Chewa; Nyanja",
        "Chinese",
        "Chuvash",
        "Cornish",
        "Corsican",
        "Cree",
        "Croatian",
        "Czech",
        "Danish",
        "Divehi; Dhivehi; Maldivian;",
        "Dutch",
        "English",
        "Esperanto",
        "Estonian",
        "Ewe",
        "Faroese",
        "Fijian",
        "Finnish",
        "French",
        "Fula; Fulah; Pulaar; Pular",
        "Galician",
        "Georgian",
        "German",
        "Greek, Modern",
        "Guaraní",
        "Gujarati",
        "Haitian; Haitian Creole",
        "Hausa",
        "Hebrew (modern)",
        "Herero",
        "Hindi",
        "Hiri Motu",
        "Hungarian",
        "Interlingua",
        "Indonesian",
        "Interlingue",
        "Irish",
        "Igbo",
        "Inupiaq",
        "Ido",
        "Icelandic",
        "Italian",
        "Inuktitut",
        "Japanese",
        "Javanese",
        "Kalaallisut, Greenlandic",
        "Kannada",
        "Kanuri",
        "Kashmiri",
        "Kazakh",
        "Khmer",
        "Kikuyu, Gikuyu",
        "Kinyarwanda",
        "Kirghiz, Kyrgyz",
        "Komi",
        "Kongo",
        "Korean",
        "Kurdish",
        "Kwanyama, Kuanyama",
        "Latin",
        "Luxembourgish, Letzeburgesch",
        "Luganda",
        "Limburgish, Limburgan, Limburger",
        "Lingala",
        "Lao",
        "Lithuanian",
        "Luba-Katanga",
        "Latvian",
        "Manx",
        "Macedonian",
        "Malagasy",
        "Malay",
        "Malayalam",
        "Maltese",
        "Māori",
        "Marathi (Marāṭhī)",
        "Marshallese",
        "Mongolian",
        "Nauru",
        "Navajo, Navaho",
        "Norwegian Bokmål",
        "North Ndebele",
        "Nepali",
        "Ndonga",
        "Norwegian Nynorsk",
        "Norwegian",
        "Nuosu",
        "South Ndebele",
        "Occitan",
        "Ojibwe, Ojibwa",
        "Oromo",
        "Oriya",
        "Ossetian, Ossetic",
        "Panjabi, Punjabi",
        "Pāli",
        "Persian",
        "Polish",
        "Pashto, Pushto",
        "Portuguese",
        "Quechua",
        "Romansh",
        "Kirundi",
        "Romanian, Moldavian, Moldovan",
        "Russian",
        "Sanskrit (Saṁskṛta)",
        "Sardinian",
        "Sindhi",
        "Northern Sami",
        "Samoan",
        "Sango",
        "Serbian",
        "Scottish Gaelic; Gaelic",
        "Shona",
        "Sinhala, Sinhalese",
        "Slovak",
        "Slovene",
        "Somali",
        "Southern Sotho",
        "Spanish; Castilian",
        "Sundanese",
        "Swahili",
        "Swati",
        "Swedish",
        "Tamil",
        "Telugu",
        "Tajik",
        "Thai",
        "Tigrinya",
        "Tibetan Standard, Tibetan, Central",
        "Turkmen",
        "Tagalog",
        "Tswana",
        "Tonga (Tonga Islands)",
        "Turkish",
        "Tsonga",
        "Tatar",
        "Twi",
        "Zhuang, Chuang"]
    }
  }
  onchange() {

  }
  render() {
    return <div className="main">
      <h2 className="title">Localization Configuration</h2>
      <p className="desc">Configure supported Locales, default Locale and default Date Time formats.</p>
      <p className="desc"><b>Note:</b> You must refresh the system cache of product servers for this change to take effect.</p>
      <div className="col-md-6">
        <table className="edl_table_form">
          <tbody>
            <tr>
              <td className="label col-sm-4">Supported Locale</td>
              <td>
                <Checkbox
                  lisActivated={true}
                  type={'checkbox'}
                  options={this.state.isoLangs}
                  controlFunc={this.onchange}
                  selectedOptions={["Abkhaz"]} />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="col-md-8">
        <Select
          name={'def-loc'}
          title={'Default Locale'}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <SingleInput
          title={'Date Time Format'}
          inputType={'text'}
          name={'date-time'}
          content={'MM/dd/yyyy HH:mm:ss (z)'}
          imgActivate={true}
        />
        </div>
        <div className="form-group form-submit-button row">
        <input className="secondary-btn" id="encryButton" type="submit" value="Save"></input>
      </div>
    </div>;
  }
}

export default LocalizationConfig;
