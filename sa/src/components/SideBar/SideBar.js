import React, { Component } from 'react';
import { Route, Link, Switch, Redirect } from 'react-router-dom';
import SearchUser from '../SearchUsers/SearchUsers';
import CreateCustomRole from '../CreateCustomRole/CreateCustomRole';
import updateCustomRole from '../updateCustomRole/updateCustomRole';
import DeleteCustomRole from '../DeleteCustomRole/DeleteCustomRole';
import CreateOrg from '../CreateOrganization/CreateOrganization';
import SearchOrg from '../searchOrg/searchOrg';
import CacheStatus from '../CacheStatus/CacheStatus';
import RiskAnalytics from '../RiskAnalyticsConn/RiskAnalyticsConn';
import TrustedCertificate from '../TrustedCertificate/TrustedCertificate';
import ManageTokenSite from '../ManageTokenSites/ManageTokenSites';
import ManageToken from '../ManageToken/ManageToken';
import EndpointManagement from '../EndpointManagement/EndpointManagement';
import AMDSConnectivity from '../AMDSConnectivity/AMDSConnectivity';
import ProtocolConfig from '../ProtocolConfig/ProtocolConfig';
import CustomReportViews from '../CustomReportViews/CustomReportViews';
import UDSConnectivity from '../UDSConnectivity/UDSConnectivity';
import RefreshCache from '../RefreshCache/RefreshCache';
import AttributeEncryptionConfig from '../AttributeEncryptionConfig/AttributeEncryptionConfig';
import LocalizationConfig from '../LocalizationConfig/LocalizationConfig';
import ConfigAccount from '../ConfigureAccount/ConfigureAccount';
import SetDefaultOrg from '../SetDefaultOrg/SetDefaultOrg';
import AuthenticationAuthorization from '../AuthenticationAndAuthorization/AuthenticationAndAuthorization';
import MyProfile from '../myProfile/myProfile';
import Create from '../Create/Create';
import { mapRoutes } from '../routesPath';
import './SideBar.css';
class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openLeftNav: true
    }
  }
  toogleLeftNav = () => {
    this.setState({ openLeftNav: !this.state.openLeftNav });
  }
  render() {
    const match = this.props.match;
    return <div>
      {this.state.openLeftNav ?
        <div className="side-toogle-menu">
          <div id="reportTaskPane">
            <ul className="side-menu-items">
              {mapRoutes[match.url] && mapRoutes[match.url].multilevel ? Object.keys(mapRoutes[match.url]).map(keyOuter => {
                return [keyOuter === 'multilevel' ? '':<h6 className="side_menu_l4_header" key={keyOuter}>{keyOuter}</h6>,
                Object.keys(mapRoutes[match.url][keyOuter]).map(keyInner => {
                  return (
                    <Link to={`${match.url}${keyInner}`} key={keyInner}>
                    <li className={`${match.url}${keyInner}`=== window.location.pathname ? 'activated-route':''}>{mapRoutes[match.url][keyOuter][keyInner]}</li></Link>
                  );
                })]
              }) : mapRoutes[match.url] && Object.keys(mapRoutes[match.url]).map((eaachRoute, key) => {
                return <Link to={`${match.url}${eaachRoute}`} key={key}>
                <li className={`${match.url}${eaachRoute}`=== window.location.pathname ? 'activated-route':''}>{mapRoutes[match.url][eaachRoute]}</li></Link>
              })}
            </ul>
          </div>
        </div> : ''}
      <div className="main-content">
        <span className="left-nav-buttons" onClick={this.toogleLeftNav}>
          {this.state.openLeftNav ? <img src="/images/closeLeftPanel.png"></img> : <img src="/images/openLeftPanel.png"></img>}
        </span>
        {/* <div className="loadingOverlay"></div> */}
        <Switch>
          <Route path={`/myProfile`} component={MyProfile} />
          <Redirect path='/users/manageUsers' to='manageUsers/search' exact />
          <Redirect path='/users/manageRoles' to='manageRoles/createCustomRole' exact />
          <Redirect path='/org/RA' to='RA/createOrg' exact />
          <Redirect path='/server-config/RA' to='RA/risk-conn' exact />
          <Redirect path='/server-config/AC' to='AC/uds-conn' exact />
          <Route path={`/users/manageUsers/search`} component={SearchUser} />
          <Route path={`/users/manageUsers/create`} component={Create} />
          <Route path={`/users/manageRoles/createCustomRole`} component={CreateCustomRole} />
          <Route path={`/users/manageRoles/updateCustomRole`} component={updateCustomRole} />
          <Route path={`/users/manageRoles/deleteCustomRole`} component={DeleteCustomRole} />
          <Route path={`/org/createOrg`} component={CreateOrg} />
          <Route path={`/org/searchOrg`} component={SearchOrg} />
          <Route path={`/org/cacheStatus`} component={CacheStatus} />
          <Route path={`/server-config/RA/risk-conn`} component={RiskAnalytics} />
          <Route path={`/server-config/RA/trusted-cert-auth`} component={TrustedCertificate} />
          <Route path={`/server-config/RA/manage-token-sites`} component={ManageTokenSite} />
          <Route path={`/server-config/RA/manage-token`} component={ManageToken} />
          <Route path={`/server-config/RA/endpoint-manage`} component={EndpointManagement} />
          <Route path={`/server-config/RA/amds-conn`} component={AMDSConnectivity} />
          <Route path={`/server-config/RA/protocol-config`} component={ProtocolConfig} />
          <Route path={`/server-config/RA/custom-report-view`} component={CustomReportViews} />
          <Route path={`/server-config/AC/uds-conn`} component={UDSConnectivity} />
          <Route path={`/server-config/AC/refresh-cache`} component={RefreshCache} />
          <Route path={`/server-config/AC/cacheStatus`} component={CacheStatus} />
          <Route path={`/server-config/AC/attr-encry-config`} component={AttributeEncryptionConfig} />
          <Route path={`/server-config/AC/local-config`} component={LocalizationConfig} />
          <Route path={`/server-config/AC/default-org`} component={SetDefaultOrg}/>
          <Route path={`/server-config/AC/config-account`} component={ConfigAccount}/>
          <Route path={`/server-config/AC/auth`} component={AuthenticationAuthorization}/>
        </Switch>
      </div>
    </div>
  }
}

export default SideBar;
