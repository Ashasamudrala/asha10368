import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';

class SetDefaultOrg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: []
    };
  }
  render() {
    return <div className="main"><h2 className="title">Set Default Organization</h2>
    <p className="desc">Set the default organization from the available list of organizations.</p>
    <p className="desc"><b>Note:</b> You must refresh the system cache of product servers for this change to take effect.</p> 
    <div className="col-sm-6 div-seperator">
      <Select
        name={'def-org'}
        title={'Default Organization'}
        options={this.state.roleOptions}
        placeholder={'Select'} />
    </div>
    <div className="form-group form-submit-button">
      <input className="secondary-btn" id="createRoleButton" type="submit" value="Save"></input>
    </div>
  </div>;;
  }
}

export default SetDefaultOrg;
