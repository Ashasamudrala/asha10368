import React, { Component } from 'react';
import './DynamicNavbar.css';
import { Route, Link, Switch ,Redirect } from 'react-router-dom';
import SideBar from '../SideBar/SideBar';
import { mapRoutes,dynamicNavbar } from '../routesPath';
class DynamicNavbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openLeftNav: true,
    }
  }
  render() {
    const match = this.props.match;
    console.log('path', match);
    var index = window.location.pathname.lastIndexOf("/");
    if(index != -1)
{
    var newStr = window.location.pathname.substring(0,index);
}
    return <div>
      <div  className={dynamicNavbar[match.url] ? 'third-nav':''}>
      {dynamicNavbar[match.url] && Object.keys(dynamicNavbar[match.url]).map((eachRoute,key) => {
                return <Link to={`${match.url}${eachRoute}`} key={key}>
                <span className={`${match.url}${eachRoute}`=== newStr ? 'third-nav-text third-nav-active':'third-nav-text'}>{dynamicNavbar[match.url][eachRoute]}</span></Link>
              })}
      </div>
      {this.state.openLeftNav ?
        <div>
          <Switch>
            <Redirect path='/users' exact to='users/manageUsers'/>
            <Redirect path='/org' exact to='org/searchOrg'/>
            <Redirect path='/server-config' exact to='server-config/RA'/>
            <Route path={`/users/manageUsers`}  component={SideBar} />
            <Route path={`/users/manageRoles`}  component={SideBar} />
            <Route path={`/org`}  component={SideBar} />
            <Route path={`/myProfile`}  component={SideBar} />
            <Route path={`/server-config/AC`}  component={SideBar} />
            <Route path={`/server-config/RA`}  component={SideBar} />
          </Switch>
        </div>
        : ''}
    </div>
  }
}

export default DynamicNavbar;
