import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
class DocumentInput extends React.Component {
  render() {
    return <input
      type="file"
      class="dynaic-file-input"
      name={`document-${this.props.index}-document`}
    />;
  }
}
class TrustedCertificate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      documents: []
    }
    this.add = this.add.bind(this);
  }
  componentWillMount() {
    let loopdocuments = [];
    for (let i = 0; i < 2; i++) {
      loopdocuments = loopdocuments.concat(DocumentInput);
    }
    this.setState({ documents: loopdocuments });
  }
  add() {
    const documents = this.state.documents.concat(DocumentInput);
    this.setState({ documents });
  }
  render() {
    const documents = this.state.documents.map((Element, index) => {
      return <Element key={index} index={index} />
    });
    return <div className="main">
      <h2 className="title">Risk Analytics Transaction Server Trusted Certificate Authorities</h2>
      <p className="desc">Upload multiple CAs and group them to create an SSL Trust Store for Risk Analytics Transaction Server Instance.</p>
      <div className="col-sm-5">
        <SingleInput
          title={'Name'}
          inputType={'text'}
          name={'role-display-name'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Root CAs</label>
          <div className="col-md-8">
            {documents}
            <input type="button" className="custom-secondary-btn" onClick={this.add} value="Add More" />
          </div></div>
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="riskButton" type="submit" value="SAVE"></input>
        </div>
      </div>
      <h2 className="title">Case Management Server Trusted Certificate Authorities</h2>
      <p className="desc">Upload multiple CAs and group them to create an SSL Trust Store for Case Management Server Instance.</p>
      <div className="col-sm-5">
        <SingleInput
          title={'Name'}
          inputType={'text'}
          name={'role-display-name'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Root CAs</label>
          <div className="col-md-8">
            {documents}
            <input type="button" className="custom-secondary-btn" onClick={this.add} value="Add More" />
          </div></div>
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="caseButton" type="submit" value="SAVE"></input>
        </div>
      </div>
      <h2 className="title">Other Trusted Certificate Authorities</h2>
      <p className="desc">Add certificate for any part other than Risk Analytics and Case Management server.</p>
      <div className="col-sm-5">
        <SingleInput
          title={'Name'}
          inputType={'text'}
          name={'role-display-name'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">Root CAs</label>
          <div className="col-md-8">
            <input
              type="file"
              class="dynaic-file-input"
              name={`trust-cert`}
            />
          </div></div>
        <div className="form-group form-submit-button row">
          <input className="secondary-btn" id="caseButton" type="submit" value="SAVE"></input>
        </div>
      </div>
    </div>;
  }
}

export default TrustedCertificate;
