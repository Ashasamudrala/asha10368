import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';

class ManageTokenSites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roleOptions: []
    }
  }
  render() {
    return <div className="main">
      <h2 className="title">Manage Tokenization Sites</h2>
      <p className="desc">Manage Tokenization Sites</p>
      <div className="col-sm-6">
        <Select
          name={'Site'}
          title={'Site'}
          required={true}
          options={this.state.roleOptions}
          placeholder={'Select'} />
        <SingleInput
          title={'Site Name'}
          inputType={'text'}
          name={'Site-name'} />
        <span class="ecc-h1 row">Primary</span>
        <SingleInput
          title={'Site Url'}
          inputType={'text'}
          name={'site-url'} />
        <SingleInput
          title={'Root CA Path(PEM)(Path relative to ARCOT_HOME)'}
          inputType={'text'}
          name={'path-ca'} />
        <span class="ecc-h1 row">Backup</span>
        <SingleInput
          title={'Backup Site Url'}
          inputType={'text'}
          name={'backup-site-url'} />
        <SingleInput
          title={'Backup Root CA Path(PEM)(Path relative to ARCOT_HOME)'}
          inputType={'text'}
          name={'backup-ca'} />
        <div className="form-group form-submit-button row">
          <input className="custom-secondary-btn" id="RESET" type="button" value="RESET"></input>
          <input className="secondary-btn" id="manageTokenButton" type="submit" value="SUBMIT"></input>
        </div>
      </div></div>;
  }
}

export default ManageTokenSites;
