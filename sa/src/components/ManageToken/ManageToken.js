import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';

class ManageToken extends Component {
  constructor(props){
    super(props);
    this.state={roleOptions:[]}
  }
  render() {
    return <div className="main">
      <h2 className="title">Manage Tokenization For Organization</h2>
      <p className="desc">Manage Tokenization For Organization</p>
      <div className="col-sm-6">
        <Select
          name={'Organization'}
          title={'Organization'}
          options={this.state.roleOptions}
          placeholder={'Select'} />
      </div>
    </div>;
  }
}

export default ManageToken;
