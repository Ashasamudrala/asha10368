import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Checkbox from '../../shared/components/Checkbox/Checkbox';

class SearchOrg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      petSelections: ["praveen"],
      selectedPets: ["praveen"],
      roleOptions: []
    };
  }
  render() {
    return <div className="main"><h2 className="title">Search Organization</h2>
      <p className="desc">Enter the full Display Name or first few characters of the Organization Display Name that you want to search.</p>
      <div className="col-sm-9 div-seperator">
        <SingleInput
          title={'Organization'}
          inputType={'text'}
          name={'Organization-name'} />
        <div className="form-group row">
          <label className="col-sm-4 col-form-label">
            Status
                        </label>
          <div className="col-sm-8 form-inline">
            <Checkbox
              title={'Initial'}
              setName={'Initial'}
              type={'checkbox'}
              options={this.state.petSelections}
              selectedOptions={''} />
            <Checkbox
              title={'Active'}
              setName={'Active'}
              type={'checkbox'}
              options={this.state.petSelections}
              selectedOptions={''} />
            <Checkbox
              title={'Inactive'}
              setName={'Inactive'}
              type={'checkbox'}
              options={this.state.petSelections}
              selectedOptions={this.state.selectedPets} />
           <Checkbox
              title={'Deleted'}
              setName={'Deleted'}
              type={'checkbox'}
              options={this.state.petSelections}
              selectedOptions={this.state.selectedPets} />
          </div>
        </div>
      </div>
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createRoleButton" type="submit" value="Search"></input>
      </div>
    </div>;
  }
}

export default SearchOrg;
