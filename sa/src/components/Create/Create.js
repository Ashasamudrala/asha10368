import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import Select from '../../shared/components/Select/Select';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';
import './Create.css';
class Create extends Component {
  constructor(props) {
    super(props);
    this.dynamicInputs = React.createRef();
    this.state = {};
    this.state.orgOptions = ["test"];
    this.state.products = [
      {
        id: 1,
        category: 'Sporting Goods',
        price: '49.99',
        qty: 12,
        name: 'football'
      }
    ];
  }
  handleRowDel(product) {
    if (this.state.products.length === 1) {
      return
    }
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      name: "",
      price: "",
      category: "",
      qty: 0
    }
    this.state.products.push(product);
    this.setState(this.state.products);
  }
  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var products = this.state.products.slice();
    var newProducts = products.map(function (product) {
      for (var key in product) {
        if (key == item.name && product.id == item.id) {
          product[key] = item.value;
        }
      }
      return product;
    });
    this.setState({ products: newProducts });
  };
  componentDidMount() {
    if (this.refs.dynamicInputs) {
      this.refs.dynamicInputs.innerHTML = 'Hello World!';
    }
  }
  render() {
    return <div className="main"><h2 className="title">Create Administrator</h2>
      <p className="desc">Enter the details for the administrator that you want to create.</p>
      <span className="ecc-h1">Administrator Details</span>
      <div className="col-sm-5 create">
        <SingleInput
          title={'Username'}
          inputType={'text'}
          required={true}
          name={'username'} />
        <Select
          name={'org-name'}
          title={'Organization'}
          required={true}
          options={this.state.orgOptions}
          placeholder={'--Select--'} />
        <SingleInput
          title={'First Name'}
          required={true}
          inputType={'text'}
          name={'first_name'} />
        <SingleInput
          title={'Middle Name'}
          inputType={'text'}
          name={'middle_name'} />
        <SingleInput
          title={'Last Name'}
          required={true}
          inputType={'text'}
          name={'last_name'} />
      </div>

      <div className="div-seperator">
        <span className="ecc-h1">Email Address(es)</span>
        <div className="col-sm-5 create">
          <SingleInput
            title={'Email'}
            required={true}
            inputType={'text'}
            name={'email'} />
        </div>
      </div>
      <div className="div-seperator">
        <span className="ecc-h1">Telephone Number(s)</span>
        <div className="col-sm-5 create">
          <SingleInput
            title={'Phone Number'}
            inputType={'number'}
            required={true}
            name={'phone_number'} />
        </div>
      </div>
          <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.products} />
      <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createButton" type="submit" value="Next"></input>
      </div>
    </div>;
  }
}

export default Create;
