import React, { Component } from 'react';
import { getService } from '../../shared/utlities/RestAPI';
import { serverUrl } from '../../shared/utlities/constants';
import './masterAdminLogin.css';
const adminUrl = "arcotadmin/masteradminlogin";
class masterAdminLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginError: false,
      orgError: false,
      usernameError: false,
      passwordError: false,
      username: '',
      password: '',
      userAuth:false
    };
  }
  handleCredentials = (e,msg) => {
    if(msg === "username"){
       this.setState({username:e.target.value});
       if(e.target.value.length){
        this.setState({usernameError:false});
       }else{
        this.setState({usernameError:true});
       }
    }else if(msg === "password"){
      this.setState({password:e.target.value});
      if(e.target.value.length){
        this.setState({passwordError:false});
       }else{
        this.setState({passwordError:true});
       }
    }
  }
  onRedirectNext = () => {
    if (this.state.username && this.state.password) {
      if(this.state.username !== 'masteradmin' || this.state.password !=='dost1234#'){
          this.setState({userAuth:true});
          setTimeout(() => {
            this.setState({userAuth:false});
          }, 3000);
          return;
      }else{
      const checkAdmin = {
        method: 'GET',
        url: `${serverUrl}/${adminUrl}`,
        data: {
          "username": this.state.username,
          "password": this.state.password
        }
      };
      const checkAdminStatus = getService(checkAdmin);
      checkAdminStatus.then((getAdminStatus) => {
        if (getAdminStatus.status === 200) {
          console.log(getAdminStatus.data);
          this.props.history.push('/users');
        }
      })
    }
    } else {
      if (this.state.username === '') {
        this.setState({ usernameError: true });
      }
      if (this.state.password === '') {
        this.setState({ passwordError: true });
      }
      return
    }
  }
  render() {
    const { usernameError, passwordError,userAuth } = this.state;
    return (
      <div className="login-parent">
        <div className="eupopup-parent">
        </div>
        <div className="wrapper">
          <div className="">
            <div className="clearfloat edl-login-box">
              <div id="brand-holder">
                <div>
                  <img src="/images/ca_logo.png"></img>
                  <p className="ecc-h1">Administration Console</p>
                </div>
              </div>
              <div id="form-holder">
                <div className="form-header" id="signin-header">SIGN IN</div>
                <form name="bamForm" method="post" >
                </form>
                <div id="loginbox">
                    <div>
                      <div className="body-font">Username</div>
                      <div className="edl_login_input">
                        <input name="userName" title="Please enter username." className={`loginTextBox ${usernameError ? ' error' : ''}`} autoComplete="off" type="text" onChange={e => this.handleCredentials(e, "username")} />
                        <div className={`${usernameError ? 'error-msg' : 'no-msg'}`}>Please enter username.</div>
                      </div>
                      <div className="body-font">Password</div>
                      <div className="edl_login_input">
                        <input name="userPassword" title="Please enter password." className={`loginTextBox ${passwordError ? ' error' : ''}`} autoComplete="off" type="password" onChange={e => this.handleCredentials(e, "password")} />
                        <div className={`${passwordError ? 'error-msg' : 'no-msg'}`}>Please enter password.</div>
                      </div>
                      <div className={`${userAuth ? 'edl_error_feedback' : 'no-msg'}`} id="ssErrorMessagesTable">Authentication failed.</div>
                      </div>
                    <input className="button edl_btn_main left_align margin-top-8 user-sign-in" onClick={this.onRedirectNext} type="submit" value="SIGN IN" />
                </div>
                <br className="clearfloat" />
                <br />
                <div className="eupopup"></div>

              </div>
            </div>
          </div>
          <div className="push"></div>

        </div>
        <div id="footer">
          <div className="edl-footer">
            Copyright ©
           <span id="footerCurrentYear"> 2019 </span>
            CA. All rights reserved.
        </div>
        </div>
      </div>
    );
  }
}

export default masterAdminLogin;
