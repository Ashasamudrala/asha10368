import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';
class EndpointManagement extends Component {
  constructor(props){
    super(props);
    this.state={roleOptions:[]}
  }
  render() {
    return <div className="main">
    <h2 className="title">Protocol Configuration</h2>
    <p className="desc">Configure the protocols for any Risk Analytics Transaction/Case Management Server instance.</p>
    <div className="col-sm-6">
      <Select
        name={'Server-Instance'}
        title={'Server Instance'}
        options={this.state.roleOptions}
        placeholder={'Select'} />
    </div>
  </div>;
  }
}

export default EndpointManagement;
