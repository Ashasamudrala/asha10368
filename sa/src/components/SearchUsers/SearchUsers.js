import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import AdvancedSearch from './AdvancedSearch';

class SearchUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openLeftNav: true,
      advancedSearch:false,
      ageOptions: ["praveen", "Adaabala"],
      
    }
  }
  activateAdvancedSearch=()=>{
    this.setState({advancedSearch:true});
  }
  render() {
    return (<div>
      {!this.state.advancedSearch ?
    <div className="main">
      <table >
        <tbody>
          <tr>
            <td>
              <span className="pageTitle">
                <h2 className="title">Search Users and Administrators</h2>
              </span>
            </td>
          </tr>
          <tr>
            <td>
              <p className="desc">Specify the search criteria to display the list of users.
              You can specify the Organization's Display Name to search for the user in
              an organization. Use the Advanced Search link to additionally search on
  Status or Role.
              </p>
              <p className="desc">
                <b>Note:</b>
                You need to enter the complete value in the
                            field if it is marked for encryption, else partial value would
suffice.</p>
            </td>
            <td >
              <a className="regular-link" onClick={this.activateAdvancedSearch}>Advanced Search</a></td></tr>
        </tbody>
      </table>
      <div className="col-sm-5 user-details">
        <SingleInput
          title={'First Name'}
          inputType={'text'}
          name={'first_name'} />
        <SingleInput
          title={'Last Name'}
          inputType={'text'}
          name={'last_name'} />
        <SingleInput
          title={'Username'}
          inputType={'text'}
          name={'username'} />
        <SingleInput
          title={'Organization'}
          inputType={'text'}
          name={'org'} />
        <SingleInput
          title={'Email'}
          inputType={'text'}
          name={'email'} />
        <SingleInput
          title={'Phone Number'}
          inputType={'number'}
          name={'phone_number'} />
        <div className="form-group row">
          <input className="secondary-btn" id="searchButton" type="submit" value="Search"></input>
        </div>
      </div>
    </div>:
    <AdvancedSearch/>
  }
  </div>)
  }
}

export default SearchUsers;
