import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import RadioGroup from '../../shared/components/RadioGroup/RadioGroup';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
class AdvancedSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            petSelections: ["praveen"],
            selectedPets: ["praveen"]
        };
    }
    render() {
        return <div className="advanced-search main">
            <span className="pageTitle">
                <h2 className="title">Advanced Search</h2>
            </span>
            <p className="desc">Specify the advanced search criteria to additionally search on Status or Role.
              </p>
            <p className="desc">
                <b>Note:</b>
                You need to enter the complete value in the field if it is marked for encryption, else partial value would suffice.</p>
            <div className="col-sm-6">
                <span className="ecc-h1 row">User Details</span>
                <SingleInput
                    title={'Username'}
                    inputType={'text'}
                    name={'username'} />
                <div className="form-group row">
                    <label className="col-sm-4 col-form-label">Enable search by Accounts</label>
                    <div className="col-sm-8">
                        <div className="custom-control custom-checkbox">
                            <input type="checkbox" className="custom-control-input" id="customCheck1" />
                            <label className="custom-control-label" htmlFor="customCheck1"></label>
                        </div>
                    </div>
                </div>
                <SingleInput
                    title={'First Name'}
                    inputType={'text'}
                    name={'first_name'} />
                <SingleInput
                    title={'Last Name'}
                    inputType={'text'}
                    name={'last_name'} />
                <SingleInput
                    title={'Username'}
                    inputType={'text'}
                    name={'username'} />
                <SingleInput
                    title={'Organization'}
                    inputType={'text'}
                    name={'org'} />
                <SingleInput
                    title={'Email'}
                    inputType={'text'}
                    name={'email'} />
                <SingleInput
                    title={'Phone Number'}
                    inputType={'number'}
                    name={'phone_number'} />
                <div className="div-seperator">
                    <span className="ecc-h1 row">User Status</span>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">
                            <RadioGroup
                                title={'Current Users'}
                                setName={'users'}
                                type={'radio'}
                                options={this.state.petSelections}
                                selectedOptions={this.state.selectedPets} />
                        </label>
                        <div className="col-sm-8 form-inline">
                            <Checkbox
                                title={'Active'}
                                setName={'Active'}
                                type={'checkbox'}
                                options={this.state.petSelections}
                                selectedOptions={this.state.selectedPets} />
                            <Checkbox
                                title={'Inactive'}
                                setName={'Inactive'}
                                type={'checkbox'}
                                options={this.state.petSelections}
                                selectedOptions={this.state.selectedPets} />
                            <Checkbox
                                title={'Initial'}
                                setName={'Initial'}
                                type={'checkbox'}
                                options={this.state.petSelections}
                                selectedOptions={this.state.selectedPets} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label className="col-sm-4 col-form-label">
                            <RadioGroup
                                title={'Deleted Users'}
                                setName={'delete-users'}
                                type={'radio'}
                                options={this.state.petSelections}
                                selectedOptions={''} />
                        </label>
                    </div>
                    <div className="div-seperator">
                        <span className="ecc-h1 row">Available Rows</span>
                        <div className="form-group row">
                        <label className="col-sm-4 col-form-label">Roles</label>
                        <div className="col-sm-8">
                            <select className="custom-select" multiple>
                                <option >Open this select menu</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        </div>
                    </div>
                </div>
                <div className="form-group row">
                    <input className="secondary-btn" id="searchButton" type="submit" value="Search"></input>
                </div>
            </div>
        </div>
    }
}
export default AdvancedSearch;