import React, { Component } from 'react';
import SingleInput from '../../shared/components/SingleInput/SingleInput';
import DynamicAddInput from '../../shared/components/DynamicAddInput/DynamicAddInput';
import SwappingSelectBox from '../../shared/components/SwappingSelectBox/SwappingSelectBox';
import Checkbox from '../../shared/components/Checkbox/Checkbox';
import './ConfigureAccount.css';
class ConfigureAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isoLangs:["Apply to all Organizations"],
      products: [
        {
          id: 1,
          category: 'Sporting Goods',
          price: '49.99',
          qty: 12,
          name: 'football'
        }
      ]
    }
  }
  handleRowDel(product) {
    if (this.state.products.length === 1) {
      return
    }
    var index = this.state.products.indexOf(product);
    this.state.products.splice(index, 1);
    this.setState(this.state.products);
  };

  handleAddEvent(evt) {
    var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
    var product = {
      id: id,
      name: "",
      price: "",
      category: "",
      qty: 0
    }
    this.state.products.push(product);
    this.setState(this.state.products);
  }
  handleProductTable(evt) {
    var item = {
      id: evt.target.id,
      name: evt.target.name,
      value: evt.target.value
    };
    var products = this.state.products.slice();
    var newProducts = products.map(function (product) {
      for (var key in product) {
        if (key == item.name && product.id == item.id) {
          product[key] = item.value;
        }
      }
      return product;
    });
    this.setState({ products: newProducts });
  };
  render() {
    return <div className="main">
      <h2 className="title">Configure Account Type</h2>
      <p className="desc">Add or Update Account Type.</p>
      <p className="desc"><b>Note:</b> You must refresh the system cache of product servers for the add/update/delete account type & its custom attribute changes to take effect. If organizations are assigned/un-assigned, organization cache refresh is also needed for the organization changes to take effect.</p>
      <div className="div-seperator col-md-5">
        <SingleInput
          title={'Name'}
          inputType={'text'}
          required={true}
          name={'Name'} />
        <SingleInput
          title={'Display Name'}
          inputType={'text'}
          required={true}
          name={'Display Name'} />
        <DynamicAddInput onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.products} filterText={this.state.filterText} />
      </div>
      <div className="div-seperator">
        <span className="ecc-h1">Assign to Organizations</span>
        <div className="col-md-12 form-group row no-padding">
          <Checkbox
            type={'checkbox'}
            options={this.state.isoLangs}
            controlFunc={this.onchange}
            selectedOptions={["Abkhaz"]} />
          <span className="seperatText">OR</span>
          <div className="col-md-9">
            <SwappingSelectBox
              firstHeader={'Available Attributes for encryption'}
              secondHeader={'	Attributes Selected for encryption'}
            />
          </div>
        </div>
        <div className="form-group form-submit-button">
        <input className="secondary-btn" id="createButton" type="submit" value="CREATE"></input>
      </div>
      </div>
    </div>;
  }
}
export default ConfigureAccount;
