import React, { Component } from 'react';
import Select from '../../shared/components/Select/Select';

class ProtocolConfig extends Component {
  constructor(props){
    super(props);
    this.state={roleOptions:[]}
  }
  render() {
    return <div className="main">
    <h2 className="title">Endpoint Management</h2>
    <p className="desc">Add Risk Analytics endpoints</p>
    <div className="col-sm-6">
      <Select
        name={'Category'}
        title={'Category'}
        options={this.state.roleOptions}
        placeholder={'Select'} />
    </div>
  </div>;
  }
}

export default ProtocolConfig;
