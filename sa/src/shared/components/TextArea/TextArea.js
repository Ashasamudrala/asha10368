import React from 'react';
import PropTypes from 'prop-types';
import './TextArea.css';

const TextArea = (props) => (  
  <div className="dynamic-form-input form-group row dynamic-textarea">
    <label className="col-sm-4 col-form-label">{`${props.title}:`}{props.required ?<span>*</span>:""}</label>
    <div className="col-sm-8">
    <textarea
      className="form-input"
      style={props.resize ? null : {resize: 'none'}}
      name={props.name}
      value={props.content}
      onChange={props.controlFunc}
      placeholder={props.placeholder} />
      </div>
  </div>
);

TextArea.propTypes = {  
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  resize: PropTypes.bool,
  placeholder: PropTypes.string,
  controlFunc: PropTypes.func
};

export default TextArea; 