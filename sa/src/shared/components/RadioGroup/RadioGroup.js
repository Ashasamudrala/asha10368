import React from 'react';
import PropTypes from 'prop-types';
import './RadioGroup.css';

const RadioGroup = (props) => (  
<div className="custom-control custom-radio">
      {props.options.map(opt => {
        return (
          <span key={opt} className="form-label capitalize">
            <input
              className="form-radio custom-control-input"
              id={props.title}
              name={props.setName}
              onChange={props.controlFunc}
              value={opt}
              defaultChecked={ props.selectedOptions.indexOf(opt) > -1 }
              type={props.type} /><label className="form-label custom-control-label" htmlFor={props.title}>{props.title}</label>
          </span>
        );
      })}
  </div>
);

RadioGroup.propTypes = {  
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['checkbox', 'radio']).isRequired,
  setName: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOptions: PropTypes.array,
  controlFunc: PropTypes.func
};

export default RadioGroup;