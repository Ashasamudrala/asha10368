import React from 'react';
import PropTypes from 'prop-types';
import './Checkbox.css';

const Checkbox = (props) => (  
<div className="custom-control custom-checkbox">
      {props.options.map(opt => {
        return (
         <label key={opt} className={props.lisActivated ? 'form-label capitalize col-sm-12 edl_checkbox':'form-label capitalize col-sm-2'} >
            <input
              className="form-checkbox custom-control-input"
              id={opt}
              name={props.setName}
              onChange={props.controlFunc}
              value={opt}
              defaultChecked={ props.selectedOptions.indexOf(opt) > -1 }
              type={props.type} /><label className="form-label custom-control-label" htmlFor={opt}>{opt}</label>
          </label>
        );
      })}
  </div>
);

Checkbox.propTypes = {  
  title: PropTypes.string,
  type: PropTypes.oneOf(['checkbox', 'radio']),
  setName: PropTypes.string,
  options: PropTypes.array.isRequired,
  selectedOptions: PropTypes.array,
  controlFunc: PropTypes.func
};

export default Checkbox;