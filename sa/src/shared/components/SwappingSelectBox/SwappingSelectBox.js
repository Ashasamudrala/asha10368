import React, { Component } from 'react';
import './SwappingSelectBox.css';

const SwappingSelectBox = (props) => (
  <div className="form-group row">
    <div className="">
      <div className="text-center available-header">{props.firstHeader}</div>
      <select className="custom-select available-privilages" multiple>
        <optgroup label="Administration Console">
          <option title="Update Administrator" value="AAC.UpdateAdmin">Update Administrator</option>
          <option title="View Administrator Activity Report" value="AAC.ViewActivityReport">View Administrator Activity Report</option>
          <option title="View My Activity Report" value="AAC.ViewMyActivityReport">View My Activity Report</option>
        </optgroup>
        <optgroup label="Risk Analytics"><option title="Acquire Case (webservice)" value="RF.ACQUIRECASE">Acquire Case (webservice)</option>
          <option title="Acquire Next Case (webservice)" value="RF.ACQUIRENEXTCASE">Acquire Next Case (webservice)</option>
          <option title="Add User to Exception List (web service)" value="RF.ADDEXCEPTIONUSER">Add User to Exception List (web
      service)</option> 																										<option title="Analyze Transactions" value="RF.ViewTransactionSummary">Analyze Transactions</option>
          <option title="Create User (deprecated web service)" value="RF.CREATEUSER">Create User (deprecated web service)</option>
          <option title="Decrypt Sensitive Information" value="RF.DECRYPTSENSITIVEINFO">Decrypt Sensitive Information</option>
        </optgroup>
        <optgroup label="User Data Service"><option title="Get PAM" value="UDS.GetPAM">Get PAM</option> 																										<option title="Get QnA Values" value="UDS.GetQnAValues">Get QnA
      Values</option> 																										<option title="Get User Status" value="UDS.GetUserStatus">Get User Status</option>
          <option title="List Organizations" value="UDS.ListOrgs">List
      Organizations</option>
          <option title="List User Accounts" value="UDS.ListUserAccounts">List User Accounts</option>
          <option title="List Users" value="UDS.ListUsers">List Users</option>
        </optgroup>
      </select>
    </div>
    <div className="col-sm-2">
      <ul className="arrow-group">
        <li ><div className="arrow_btn tertiary-btn"> > </div></li>
        <li><div className="arrow_btn tertiary-btn"> >> </div></li>
        <li><div className="arrow_btn tertiary-btn"> &#60;</div></li>
        <li><div className="arrow_btn tertiary-btn"> &#60;&#60;</div></li>
      </ul>
    </div>
    <div className="">
      <div className="text-center unavailable-header">{props.secondHeader}</div>
      <select className="custom-select unavailable-privilages" multiple>
        <option >Open this select menu</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </select>
    </div>
  </div>
);

export default SwappingSelectBox;
