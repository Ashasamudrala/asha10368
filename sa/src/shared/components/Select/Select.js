import React from 'react';
import PropTypes from 'prop-types';
import './Select.css';

const Select = (props) => (  
  <div className="form-group dynamic-form-select">
      <div className="form-group row">
          <label className="col-sm-4 col-form-label">{`${props.title}:`}{props.required ?<span>*</span>:""}</label>
          <div className="col-sm-8">
    <select
      name={props.name}
      value={props.selectedOption}
      onChange={props.controlFunc}
      className="form-select form-control">
      <option value="">{props.placeholder}</option>
      {props.options.map(opt => {
        return (
          <option
            key={opt}
            value={opt}>{opt}</option>
        );
      })}
    </select>
    </div>
    </div>
  </div>
);

Select.propTypes = {  
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOption: PropTypes.string,
  controlFunc: PropTypes.func,
  placeholder: PropTypes.string
};

export default Select;  