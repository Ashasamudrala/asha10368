import React from 'react';
import PropTypes from 'prop-types';
import './SingleInput.css';

const SingleInput = (props) => (
  <div className="dynamic-form-input form-group row">
    <label className="col-sm-4 col-form-label">{`${props.title}:`}{props.required ?<span>*</span>:""}</label>
    <div className="col-sm-8 input-group input-group-unstyled">
      <input
        className="form-input form-control"
        autoComplete="off"
        name={props.name}
        disabled={props.disabled}
        type={props.inputType}
        value={props.content}
        onChange={props.controlFunc}
        placeholder={props.placeholder} />
        {props.imgActivate ?
        <span class="input-group-addon">
  <img align="top" name="helpImg" src="/images/help.gif" title="Supported date formats are &quot;dd/MM/yyyy HH:mm:ss (z)&quot; and &quot;MM/dd/yyyy HH:mm:ss (z)&quot;
Format Description : y=year, M=month, d=day, H=hour (24-hour clock), m=minute, s=second, z=time zone. 
For example : &quot;dd/MM/yyyy HH:mm:ss (z)&quot; produces &quot;26/10/2016 10:54:52 (GMT+05:30)&quot;"></img></span>:''}
    </div>
   </div>
);

SingleInput.propTypes = {
  inputType: PropTypes.oneOf(['text', 'number']).isRequired,
  name: PropTypes.string.isRequired,
  controlFunc: PropTypes.func,
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  placeholder: PropTypes.string,
};

export default SingleInput;  