var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send({
    "message": "User validated",
    "username": "masteradmin",
    "tokenId": "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJnbG9iYWwiLCJpYXQiOjE1NjMyNzE5OTUsImV4cCI6MTU2MzI3NTU5NX0.meDQPmEdXqWJEKnk_OfsNUEMjkrhynWGIAjmYjf_o0I",
    "enablePreferredOrganization": false,
    "preferredOrganization": null,
    "dateTimeFormat": "MM/dd/yyyy HH:mm:ss (z)",
    "locale": "en_US",
    "timeZone": "GMT",
    "errorList": []
   });
});

module.exports = router;
